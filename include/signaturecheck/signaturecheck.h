#ifndef QI_SIGNATURECHECK_H
#define QI_SIGNATURECHECK_H

// The following line enables unicode support for C function calls, e.g. <tchar.h>. Maybe separate the C code from the C++ code to suppress the warning this generates?
// #define _UNICODE 1
// #include <tchar.h>

// The following line enables unicode support for C++ function calls
#define UNICODE 1

#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <SoftPub.h>
#include <wincrypt.h>
#include <WinTrust.h>

#define ENCODING (X509_ASN_ENCODING | PKCS_7_ASN_ENCODING)

#include <string>
#include <QString>
#include <map>
//#include <QDebug>
#include <QDir>

// Link with the Wintrust.lib file.
#pragma comment (lib, "wintrust")
#pragma comment (lib, "crypt32.lib")

namespace  {

struct SPROG_PUBLISHERINFO {
	LPWSTR lpszProgramName;
	LPWSTR lpszPublisherLink;
	LPWSTR lpszMoreInfoLink;
};
using PSPROG_PUBLISHERINFO = SPROG_PUBLISHERINFO*;

/* BOOL GetProgAndPublisherInfo(PCMSG_SIGNER_INFO pSignerInfo,
							 PSPROG_PUBLISHERINFO Info); */
static BOOL GetDateOfTimeStamp(PCMSG_SIGNER_INFO pSignerInfo, SYSTEMTIME *st);
static std::map<QString, QString> PrintCertificateInfo(PCCERT_CONTEXT pCertContext);
static BOOL GetTimeStampSignerInfo(PCMSG_SIGNER_INFO pSignerInfo,
                            PCMSG_SIGNER_INFO *pCounterSignerInfo);

/*!
 * \brief verifyEmbeddedSignature Check for a valid code signature in a compatible file
 * \param pwszSourceFile A wchar string that points to a file
 * \pre A compatible verison of Windows OS that supports code signing
 * \post The code signature for the file is checked
 * \return The HRESULT from WinVerifyTrust. If the trust provider verifies that the subject is trusted for the specified action, the return value is zero.
 * \note This function uses and requires the Windows API
 */
__forceinline static LONG verifyEmbeddedSignature(LPCWSTR pwszSourceFile)
{
	LONG lStatus;
	HRESULT dwLastError;

	// Initialize the WINTRUST_FILE_INFO structure.

	WINTRUST_FILE_INFO FileData;
	memset(&FileData, 0, sizeof(FileData));
	FileData.cbStruct = sizeof(WINTRUST_FILE_INFO);
	FileData.pcwszFilePath = pwszSourceFile;
	FileData.hFile = nullptr;
	FileData.pgKnownSubject = nullptr;

	/*
	WVTPolicyGUID specifies the policy to apply on the file
	WINTRUST_ACTION_GENERIC_VERIFY_V2 policy checks:

	1) The certificate used to sign the file chains up to a root
	certificate located in the trusted root certificate store. This
	implies that the identity of the publisher has been verified by
	a certification authority.

	2) In cases where user interface is displayed (which this example
	does not do), WinVerifyTrust will check for whether the
	end entity certificate is stored in the trusted publisher store,
	implying that the user trusts content from this publisher.

	3) The end entity certificate has sufficient permission to sign
	code, as indicated by the presence of a code signing EKU or no
	EKU.
	*/

	GUID WVTPolicyGUID = WINTRUST_ACTION_GENERIC_VERIFY_V2;
	WINTRUST_DATA WinTrustData;

	// Initialize the WinVerifyTrust input data structure.

	// Default all fields to 0.
	memset(&WinTrustData, 0, sizeof(WinTrustData));

	WinTrustData.cbStruct = sizeof(WinTrustData);

	// Use default code signing EKU.
	WinTrustData.pPolicyCallbackData = nullptr;

	// No data to pass to SIP.
	WinTrustData.pSIPClientData = nullptr;

	// Disable WVT UI.
	WinTrustData.dwUIChoice = WTD_UI_NONE;

	// No revocation checking.
	WinTrustData.fdwRevocationChecks = WTD_REVOKE_NONE;

	// Verify an embedded signature on a file.
	WinTrustData.dwUnionChoice = WTD_CHOICE_FILE;

	// Verify action.
	WinTrustData.dwStateAction = WTD_STATEACTION_VERIFY;

	// Verification sets this value.
	WinTrustData.hWVTStateData = nullptr;

	// Not used.
	WinTrustData.pwszURLReference = nullptr;

	// This is not applicable if there is no UI because it changes
	// the UI to accommodate running applications instead of
	// installing applications.
	WinTrustData.dwUIContext = 0;

	// Set pFile.
	WinTrustData.pFile = &FileData;

	// WinVerifyTrust verifies signatures as specified by the GUID
	// and Wintrust_Data.
	lStatus = WinVerifyTrust(
	    nullptr,
	    &WVTPolicyGUID,
	    &WinTrustData);

	switch (lStatus)
	{
		case ERROR_SUCCESS:
			/*
			Signed file:
				- Hash that represents the subject is trusted.

				- Trusted publisher without any verification errors.

				- UI was disabled in dwUIChoice. No publisher or
					time stamp chain errors.

				- UI was enabled in dwUIChoice and the user clicked
					"Yes" when asked to install and run the signed
					subject.
			*/
			wprintf_s(L"The file \"%s\" is signed and the signature "
			    L"was verified.\n",
			    pwszSourceFile);
			break;

		case TRUST_E_NOSIGNATURE:
			// The file was not signed or had a signature
			// that was not valid.

			// Get the reason for no signature.
			dwLastError = static_cast<HRESULT>( GetLastError() );
			if (TRUST_E_NOSIGNATURE == dwLastError ||
			        TRUST_E_SUBJECT_FORM_UNKNOWN == dwLastError ||
			        TRUST_E_PROVIDER_UNKNOWN == dwLastError)
			{
				// The file was not signed.
				wprintf_s(L"The file \"%s\" is not signed.\n",
				    pwszSourceFile);
			}
			else
			{
				// The signature was not valid or there was an error
				// opening the file.
				wprintf_s(L"An unknown error occurred trying to "
				    L"verify the signature of the \"%s\" file.\n",
				    pwszSourceFile);
			}

			break;

		case TRUST_E_EXPLICIT_DISTRUST:
			// The hash that represents the subject or the publisher
			// is not allowed by the admin or user.
			wprintf_s(L"The signature is present, but specifically "
			    L"disallowed.\n");
			break;

		case TRUST_E_SUBJECT_NOT_TRUSTED:
			// The user clicked "No" when asked to install and run.
			wprintf_s(L"The signature is present, but not "
			    L"trusted.\n");
			break;

		case CRYPT_E_SECURITY_SETTINGS:
			/*
			The hash that represents the subject or the publisher
			was not explicitly trusted by the admin and the
			admin policy has disabled user trust. No signature,
			publisher or time stamp errors.
			*/
			wprintf_s(L"CRYPT_E_SECURITY_SETTINGS - The hash "
			    L"representing the subject or the publisher wasn't "
			    L"explicitly trusted by the admin and admin policy "
			    L"has disabled user trust. No signature, publisher "
			    L"or timestamp errors.\n");
			break;
		case CRYPT_E_FILE_ERROR:
			wprintf_s(L"An error occurred while reading file \"%s\". "
			          L"Check that the file exists and that it's not locked by another process.\n",
			          pwszSourceFile);
			break;

		default:
			// The UI was disabled in dwUIChoice or the admin policy
			// has disabled user trust. lStatus contains the
			// publisher or time stamp chain error.
			wprintf_s(L"Error is: 0x%x.\n",
			    lStatus);
			break;
	}

	// Any hWVTStateData must be released by a call with close.
	WinTrustData.dwStateAction = WTD_STATEACTION_CLOSE;

	[[maybe_unused]] auto lStatus2 = WinVerifyTrust(
	    nullptr,
	    &WVTPolicyGUID,
	    &WinTrustData);

	return lStatus;
}

/*!
 * \brief getSignatureDetails Retrieve details of the embedded signature for a compatible file
 * \param filename A wstring that points to a file
 * \pre A compatible version of Windows that supports code signatures
 * \post Details of the signed file is returned
 * \return A map of relevant code signature details. If an error occurs, it will contain an key labeled "Error"
 */
__forceinline  static std::map<QString, QString> getSignatureDetails(std::wstring filename){
	auto lStatus = verifyEmbeddedSignature(filename.data());
	std::map<QString, QString> sigmap;

	if(lStatus == ERROR_SUCCESS){
		//WCHAR szFileName[MAX_PATH];
		HCERTSTORE hStore = nullptr;
		HCRYPTMSG hMsg = nullptr;
		PCCERT_CONTEXT pCertContext = nullptr;
		BOOL fResult;
		DWORD dwEncoding, dwContentType, dwFormatType;
		PCMSG_SIGNER_INFO pSignerInfo = nullptr;
		PCMSG_SIGNER_INFO pCounterSignerInfo = nullptr;
		DWORD dwSignerInfo;
		CERT_INFO CertInfo;
		SPROG_PUBLISHERINFO ProgPubInfo;
		[[maybe_unused]] SYSTEMTIME st; // This is used for finding timestamps relative to system time

		//lstrcpynW(szFileName, filename.data(), MAX_PATH);

		ZeroMemory(&ProgPubInfo, sizeof(ProgPubInfo));

		        // Get message handle and store handle from the signed file.
		        fResult = CryptQueryObject(CERT_QUERY_OBJECT_FILE,
				                           filename.data(),//szFileName,
				                           CERT_QUERY_CONTENT_FLAG_PKCS7_SIGNED_EMBED,
				                           CERT_QUERY_FORMAT_FLAG_BINARY,
				                           0,
				                           &dwEncoding,
				                           &dwContentType,
				                           &dwFormatType,
				                           &hStore,
				                           &hMsg,
				                           nullptr);
				if (!fResult)
				{
					sigmap["Error"] = QString("CryptQueryObject failed with %1").arg(GetLastError());
					return sigmap;
				}

				// Get signer information size.
				fResult = CryptMsgGetParam(hMsg,
				                           CMSG_SIGNER_INFO_PARAM,
				                           0,
				                           nullptr,
				                           &dwSignerInfo);
				if (!fResult)
				{
					sigmap["Error"] = QString("CryptMsgGetParam failed with %1").arg(GetLastError());
					return sigmap;
				}

				// Allocate memory for signer information.
				pSignerInfo = static_cast<PCMSG_SIGNER_INFO>(LocalAlloc(LPTR, dwSignerInfo));
				if (!pSignerInfo)
				{
					sigmap["Error"] = "Unable to allocate memory for Signer Info.";
					return sigmap;
				}

				// Get Signer Information.
				fResult = CryptMsgGetParam(hMsg,
				                           CMSG_SIGNER_INFO_PARAM,
				                           0,
				                           static_cast<PVOID>(pSignerInfo),
				                           &dwSignerInfo);
				if (!fResult)
				{
					sigmap["Error"] = QString("CryptMsgGetParam failed with %1").arg(GetLastError());
					return sigmap;
				}

#if 0
				// Get program name and publisher information from
				// signer info structure.
				if (GetProgAndPublisherInfo(pSignerInfo, &ProgPubInfo))
				{
					if (ProgPubInfo.lpszProgramName != nullptr)
					{
						wprintf(L"Program Name : %s\n",
						    ProgPubInfo.lpszProgramName);
					}

					if (ProgPubInfo.lpszPublisherLink != nullptr)
					{
						wprintf(L"Publisher Link : %s\n",
						    ProgPubInfo.lpszPublisherLink);
					}

					if (ProgPubInfo.lpszMoreInfoLink != nullptr)
					{
						wprintf(L"MoreInfo Link : %s\n",
						    ProgPubInfo.lpszMoreInfoLink);
					}
				}

				wprintf(L"\n");
#endif

				// Search for the signer certificate in the temporary
				// certificate store.
				CertInfo.Issuer = pSignerInfo->Issuer;
				CertInfo.SerialNumber = pSignerInfo->SerialNumber;

				pCertContext = CertFindCertificateInStore(hStore,
				                                          ENCODING,
				                                          0,
				                                          CERT_FIND_SUBJECT_CERT,
				                                          static_cast<PVOID>(&CertInfo),
				                                          nullptr);
				if (!pCertContext)
				{
					sigmap["Error"] = QString("CertFindCertificateInStore failed with %1").arg(GetLastError());
					return sigmap;
				}

				// Print Signer certificate information.
				sigmap = PrintCertificateInfo(pCertContext);

				// Clean up.
				if (ProgPubInfo.lpszProgramName != nullptr)
					LocalFree(ProgPubInfo.lpszProgramName);
				if (ProgPubInfo.lpszPublisherLink != nullptr)
					LocalFree(ProgPubInfo.lpszPublisherLink);
				if (ProgPubInfo.lpszMoreInfoLink != nullptr)
					LocalFree(ProgPubInfo.lpszMoreInfoLink);

				if (pSignerInfo != nullptr) LocalFree(pSignerInfo);
				if (pCounterSignerInfo != nullptr) LocalFree(pCounterSignerInfo);
				if (pCertContext != nullptr) CertFreeCertificateContext(pCertContext);
				if (hStore != nullptr) CertCloseStore(hStore, 0);
				if (hMsg != nullptr) CryptMsgClose(hMsg);
	    }
	    /*
		std::wstring signInfoDisplayName = winTrustData.pSgnr->pcwszDisplayName;
		sigmap["Display Name"] = QString::fromStdWString(signInfoDisplayName);

		auto issuerBlob = winTrustData.pSgnr->psSignerInfo->Issuer;
		QByteArray issuer(reinterpret_cast<char *>(issuerBlob.pbData), issuerBlob.cbData);
		sigmap["Issuer"] = issuer;

		auto serialNumberBlob = winTrustData.pSgnr->psSignerInfo->SerialNumber;
		QByteArray serialNumber(reinterpret_cast<char *>(serialNumberBlob.pbData), serialNumberBlob.cbData);
		sigmap["Serial Number"] = QByteArray::fromHex(serialNumber);
		*/

	return sigmap;
}

/*!
 * \brief PrintCertificateInfo Generates a map of strings that contain information about a code signing certificate
 * \param pCertContext Signing certificate info that has been loaded
 * \pre Certificate info from a valid file has been loaded
 * \post Details of the certificate info is returned
 * \return A map of the certificate details. If an error occurs, it will contain a key labeled "Error"
 */
__forceinline static std::map<QString, QString> PrintCertificateInfo(PCCERT_CONTEXT pCertContext)
{
	BOOL fReturn = FALSE;
	LPTSTR szName = nullptr;
	DWORD dwData;
	std::map<QString, QString> sigmap;

	do
	{
		// Print Serial Number.
		QString serialNumber;
		dwData = pCertContext->pCertInfo->SerialNumber.cbData;
		for (DWORD n = 0; n < dwData; n++)
		{
			serialNumber += QString::number(pCertContext->pCertInfo->SerialNumber.pbData[dwData - (n + 1)], 16).rightJustified(2, '0');
		}
		sigmap["Serial Number"] = serialNumber;

		// Get Issuer name size.
		if (!(dwData = CertGetNameString(pCertContext,
		                                 CERT_NAME_SIMPLE_DISPLAY_TYPE,
		                                 CERT_NAME_ISSUER_FLAG,
		                                 nullptr,
		                                 nullptr,
		                                 0)))
		{
			sigmap["Error"] = "CertGetNameString failed.";
			break;
		}

		// Allocate memory for Issuer name.
		szName = static_cast<LPTSTR>(LocalAlloc(LPTR, dwData * sizeof(WCHAR)));
		if (!szName)
		{
			sigmap["Error"] = "Unable to allocate memory for issuer name.";
			break;
		}

		// Get Issuer name.
		if (!(CertGetNameString(pCertContext,
		                        CERT_NAME_SIMPLE_DISPLAY_TYPE,
		                        CERT_NAME_ISSUER_FLAG,
		                        nullptr,
		                        szName,
		                        dwData)))
		{
			sigmap["Error"] = "CertGetNameString failed.";
			break;
		}

		// print Issuer name.
		sigmap["Issuer Name"] = QString::fromWCharArray(szName);
		LocalFree(szName);
		szName = nullptr;

		// Get Subject name size.
		if (!(dwData = CertGetNameString(pCertContext,
		                                 CERT_NAME_SIMPLE_DISPLAY_TYPE,
		                                 0,
		                                 nullptr,
		                                 nullptr,
		                                 0)))
		{
			sigmap["Error"] = "CertGetNameString failed.\n";
			break;
		}

		// Allocate memory for subject name.
		szName = static_cast<LPTSTR>(LocalAlloc(LPTR, dwData * sizeof(WCHAR)));
		if (!szName)
		{
			sigmap["Error"] = "Unable to allocate memory for subject name.\n";
			break;
		}

		// Get subject name.
		if (!(CertGetNameString(pCertContext,
		                        CERT_NAME_SIMPLE_DISPLAY_TYPE,
		                        0,
		                        nullptr,
		                        szName,
		                        dwData)))
		{
			sigmap["Error"] = "CertGetNameString failed.\n";
			break;
		}

		// Print Subject Name.
		sigmap["Subject Name"] = QString::fromWCharArray(szName);

		fReturn = TRUE;
	}
	while(false);

	if (szName != nullptr) LocalFree(szName);

	return sigmap;
}

/*
LPWSTR AllocateAndCopyWideString(LPCWSTR inputString)
{
	LPWSTR outputString = nullptr;

	outputString = (LPWSTR)LocalAlloc(LPTR,
		(wcslen(inputString) + 1) * sizeof(WCHAR));
	if (outputString != nullptr)
	{
		lstrcpyW(outputString, inputString);
	}
	return outputString;
}

BOOL GetProgAndPublisherInfo(PCMSG_SIGNER_INFO pSignerInfo,
							 PSPROG_PUBLISHERINFO Info)
{
	BOOL fReturn = FALSE;
	PSPC_SP_OPUS_INFO OpusInfo = nullptr;
	DWORD dwData;
	BOOL fResult;

	__try
	{
		// Loop through authenticated attributes and find
		// SPC_SP_OPUS_INFO_OBJID OID.
		for (DWORD n = 0; n < pSignerInfo->AuthAttrs.cAttr; n++)
		{
			if (lstrcmpA(SPC_SP_OPUS_INFO_OBJID,
						pSignerInfo->AuthAttrs.rgAttr[n].pszObjId) == 0)
			{
				// Get Size of SPC_SP_OPUS_INFO structure.
				fResult = CryptDecodeObject(ENCODING,
							SPC_SP_OPUS_INFO_OBJID,
							pSignerInfo->AuthAttrs.rgAttr[n].rgValue[0].pbData,
							pSignerInfo->AuthAttrs.rgAttr[n].rgValue[0].cbData,
							0,
							nullptr,
							&dwData);
				if (!fResult)
				{
					wprintf(L"CryptDecodeObject failed with %x\n",
						GetLastError());
					__leave;
				}

				// Allocate memory for SPC_SP_OPUS_INFO structure.
				OpusInfo = (PSPC_SP_OPUS_INFO)LocalAlloc(LPTR, dwData);
				if (!OpusInfo)
				{
					wprintf(L"Unable to allocate memory for Publisher Info.\n");
					__leave;
				}

				// Decode and get SPC_SP_OPUS_INFO structure.
				fResult = CryptDecodeObject(ENCODING,
							SPC_SP_OPUS_INFO_OBJID,
							pSignerInfo->AuthAttrs.rgAttr[n].rgValue[0].pbData,
							pSignerInfo->AuthAttrs.rgAttr[n].rgValue[0].cbData,
							0,
							OpusInfo,
							&dwData);
				if (!fResult)
				{
					wprintf(L"CryptDecodeObject failed with %x\n",
						GetLastError());
					__leave;
				}

				// Fill in Program Name if present.
				if (OpusInfo->pwszProgramName)
				{
					Info->lpszProgramName =
						AllocateAndCopyWideString(OpusInfo->pwszProgramName);
				}
				else
					Info->lpszProgramName = nullptr;

				// Fill in Publisher Information if present.
				if (OpusInfo->pPublisherInfo)
				{

					switch (OpusInfo->pPublisherInfo->dwLinkChoice)
					{
						case SPC_URL_LINK_CHOICE:
							Info->lpszPublisherLink =
								AllocateAndCopyWideString(OpusInfo->pPublisherInfo->pwszUrl);
							break;

						case SPC_FILE_LINK_CHOICE:
							Info->lpszPublisherLink =
								AllocateAndCopyWideString(OpusInfo->pPublisherInfo->pwszFile);
							break;

						default:
							Info->lpszPublisherLink = nullptr;
							break;
					}
				}
				else
				{
					Info->lpszPublisherLink = nullptr;
				}

				// Fill in More Info if present.
				if (OpusInfo->pMoreInfo)
				{
					switch (OpusInfo->pMoreInfo->dwLinkChoice)
					{
						case SPC_URL_LINK_CHOICE:
							Info->lpszMoreInfoLink =
								AllocateAndCopyWideString(OpusInfo->pMoreInfo->pwszUrl);
							break;

						case SPC_FILE_LINK_CHOICE:
							Info->lpszMoreInfoLink =
								AllocateAndCopyWideString(OpusInfo->pMoreInfo->pwszFile);
							break;

						default:
							Info->lpszMoreInfoLink = nullptr;
							break;
					}
				}
				else
				{
					Info->lpszMoreInfoLink = nullptr;
				}

				fReturn = TRUE;

				break; // Break from for loop.
			} // lstrcmp SPC_SP_OPUS_INFO_OBJID
		} // for
	}
	__finally
	{
		if (OpusInfo != nullptr) LocalFree(OpusInfo);
	}

	return fReturn;
}
*/

/*!
 * \brief GetDateOfTimeStamp Gets the date of a timestamp that is part of an embedded signature
 * \param pSignerInfo Signature info from a file
 * \param st A pointer to the system time info
 * \return True if the time was able to be found
 * \note This is not currently used, but could be useful to make sure the timestamp of the signature is reasonable
 */
[[maybe_unused]] __forceinline static BOOL GetDateOfTimeStamp(PCMSG_SIGNER_INFO pSignerInfo, SYSTEMTIME *st)
{
	BOOL fResult;
	FILETIME lft, ft;
	DWORD dwData;
	BOOL fReturn = FALSE;

	// Loop through authenticated attributes and find
	// szOID_RSA_signingTime OID.
	for (DWORD n = 0; n < pSignerInfo->AuthAttrs.cAttr; n++)
	{
		if (lstrcmpA(szOID_RSA_signingTime,
		            pSignerInfo->AuthAttrs.rgAttr[n].pszObjId) == 0)
		{
			// Decode and get FILETIME structure.
			dwData = sizeof(ft);
			fResult = CryptDecodeObject(ENCODING,
			            szOID_RSA_signingTime,
			            pSignerInfo->AuthAttrs.rgAttr[n].rgValue[0].pbData,
			            pSignerInfo->AuthAttrs.rgAttr[n].rgValue[0].cbData,
			            0,
			            static_cast<PVOID>(&ft),
			            &dwData);
			if (!fResult)
			{
				wprintf(L"CryptDecodeObject failed with %x\n",
				    GetLastError());
				break;
			}

			// Convert to local time.
			FileTimeToLocalFileTime(&ft, &lft);
			FileTimeToSystemTime(&lft, st);

			fReturn = TRUE;

			break; // Break from for loop.

		} //lstrcmp szOID_RSA_signingTime
	} // for

	return fReturn;
}

/*!
 * \brief GetTimeStampSignerInfo Helper function to get a timestampt of a signature
 * \param pSignerInfo A pointer to signature information
 * \param pCounterSignerInfo A pointer to the countersign info from the signature chain
 * \return True if the timestamp could be retrieved
 * \note This is not currently used, but could be useful if we want to check timestamps of file signatures
 */
[[maybe_unused]] __forceinline static BOOL GetTimeStampSignerInfo(PCMSG_SIGNER_INFO pSignerInfo, PCMSG_SIGNER_INFO *pCounterSignerInfo)
{
	PCCERT_CONTEXT pCertContext = nullptr;
	BOOL fReturn = FALSE;
	BOOL fResult;
	DWORD dwSize;

	do {
		*pCounterSignerInfo = nullptr;

		// Loop through unathenticated attributes for
		// szOID_RSA_counterSign OID.
		for (DWORD n = 0; n < pSignerInfo->UnauthAttrs.cAttr; n++)
		{
			if (lstrcmpA(pSignerInfo->UnauthAttrs.rgAttr[n].pszObjId,
			             szOID_RSA_counterSign) == 0)
			{
				// Get size of CMSG_SIGNER_INFO structure.
				fResult = CryptDecodeObject(ENCODING,
				           PKCS7_SIGNER_INFO,
				           pSignerInfo->UnauthAttrs.rgAttr[n].rgValue[0].pbData,
				           pSignerInfo->UnauthAttrs.rgAttr[n].rgValue[0].cbData,
				           0,
				           nullptr,
				           &dwSize);
				if (!fResult)
				{
					wprintf(L"CryptDecodeObject failed with %x\n",
					    GetLastError());
					break; // We actually want to exit the do-while loop here, but nothing comes after the for loop. Not sure if do-while is even necessary. Note: rewritten from Windows API C Code which uses SEH
				}

				// Allocate memory for CMSG_SIGNER_INFO.
				*pCounterSignerInfo = static_cast<PCMSG_SIGNER_INFO>(LocalAlloc(LPTR, dwSize));
				if (!*pCounterSignerInfo)
				{
					wprintf(L"Unable to allocate memory for timestamp info.\n");
					break; // We actually want to exit the do-while loop here, but nothing comes after the for loop. Not sure if do-while is even necessary. Note: rewritten from Windows API C Code which uses SEH
				}

				// Decode and get CMSG_SIGNER_INFO structure
				// for timestamp certificate.
				fResult = CryptDecodeObject(ENCODING,
				           PKCS7_SIGNER_INFO,
				           pSignerInfo->UnauthAttrs.rgAttr[n].rgValue[0].pbData,
				           pSignerInfo->UnauthAttrs.rgAttr[n].rgValue[0].cbData,
				           0,
				           static_cast<PVOID>(*pCounterSignerInfo),
				           &dwSize);
				if (!fResult)
				{
					wprintf(L"CryptDecodeObject failed with %x\n",
					    GetLastError());
					break; // We actually want to exit the do-while loop here, but nothing comes after the for loop. Not sure if do-while is even necessary. Note: rewritten from Windows API C Code which uses SEH
				}

				fReturn = TRUE;

				break; // Break from for loop.
			}
		}
	} while (false);
	// Clean up.
	if (pCertContext != nullptr) CertFreeCertificateContext(pCertContext);

	return fReturn;
}

/*!
 * \brief signatureCheck This will check a file to see if it is signed using the Qlarity Imaging, LLC code signing certificate
 * \param filename A wstring that points to the location of a file to check for a valid signature
 * \pre A Windows OS that supports code signatures
 * \post A result of the signature check
 * \return True if the file is signed by Qlarity Imaging, LLC
 */
__forceinline static bool signatureCheck(std::wstring filename)
{
	auto result = getSignatureDetails(filename);
	auto success = result["Serial Number"] == "00d453d71e7bc963cd30161fc68fa6b767";
	return success;
}

/*!
 * \brief signatureCheckAllFilesInFolder Scan a directory and all its subdirectories for valid Qlarity Imaging, LLC code signatures
 * \param dirPath A path to the directory which to scan all files for valid signatures
 * \param filter A filter for the files to be validated
 * \pre A Windows OS that supports code signatures
 * \post A result of the signature check
 * \return True if all files are signed by Qlarity Imaging, LLC
 */
#pragma inline_recursion(on)
[[maybe_unused]] __forceinline static bool signatureCheckAllFilesInFolder(QString dirPath, QStringList filter = {"*.dll", "*.exe"})
{
	bool result = true;
	QDir topDir(dirPath);
	const auto subdirs = topDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
	for(const auto & subdir : subdirs){
		result = result && signatureCheckAllFilesInFolder(dirPath + QDir::separator() + subdir);
	}
	const auto fileinfos = QDir(dirPath).entryInfoList(filter, QDir::Files | QDir::NoDotAndDotDot);
	for(const auto & fileinfo : fileinfos){
		result = result && signatureCheck(fileinfo.filePath().toStdWString());
	}
	return result;
}

} // namespace

#endif //QI_SIGNATURECHECK_H
