#ifndef QITESTCASE_H
#define QITESTCASE_H

#include <QMetaEnum>
#include <QTest>

QT_BEGIN_NAMESPACE


//Copied from Quantx's qitestcase.h

class qfloat16;
class QRegularExpression;
void addCustomLogger(const char *filename);

#define QIVERIFY(statement) \
do {\
        if (!QITest::qVerify(static_cast<bool>(statement), #statement, nullptr, __FILE__, __LINE__))\
            return;\
} while (false)

#define QIFAIL(message) \
do {\
        QITest::qFail(static_cast<const char *>(message), __FILE__, __LINE__);\
        return;\
} while (false)

#define QIVERIFY2(statement, description) \
do {\
        if (!QITest::qVerify(static_cast<bool>(statement), #statement, static_cast<const char *>(description), __FILE__, __LINE__))\
            return;\
} while (false)

#define QICOMPARE(actual, expected) \
do {\
        if (!QITest::qCompare(actual, expected, nullptr, #actual, #expected, __FILE__, __LINE__))\
            return;\
} while (false)

#define QICOMPARE2(actual, expected, description) \
do {\
        if (!QITest::qCompare(actual, expected, static_cast<const char *>(description), #actual, #expected, __FILE__, __LINE__))\
            return;\
} while (false)

#ifndef QT_NO_EXCEPTIONS

#  define QIVERIFY_EXCEPTION_THROWN(expression, exceptiontype) \
        do {\
            QT_TRY {\
                QT_TRY {\
                    expression;\
                    QTest::qFail("Expected exception of type " #exceptiontype " to be thrown" \
                                 " but no exception caught", __FILE__, __LINE__);\
                    return;\
                } QT_CATCH (const exceptiontype &) {\
                }\
            } QT_CATCH (const std::exception &e) {\
                QByteArray msg = QByteArray() + "Expected exception of type " #exceptiontype \
                                 " to be thrown but std::exception caught with message: " + e.what(); \
                QTest::qFail(msg.constData(), __FILE__, __LINE__);\
                return;\
            } QT_CATCH (...) {\
                QTest::qFail("Expected exception of type " #exceptiontype " to be thrown" \
                             " but unknown exception caught", __FILE__, __LINE__);\
                return;\
            }\
        } while (false)

#else // QT_NO_EXCEPTIONS

/*
 * The expression passed to the macro should throw an exception and we can't
 * catch it because Qt has been compiled without exception support. We can't
 * skip the expression because it may have side effects and must be executed.
 * So, users must use Qt with exception support enabled if they use exceptions
 * in their code.
 */
#  define QIVERIFY_EXCEPTION_THROWN(expression, exceptiontype) \
        Q_STATIC_ASSERT_X(false, "Support of exceptions is disabled")

#endif // !QT_NO_EXCEPTIONS


#define QITRY_LOOP_IMPL(expr, timeoutValue, step) \
        if (!(expr)) { \
            QITest::qWait(0); \
        } \
        int qt_test_i = 0; \
        for (; qt_test_i < timeoutValue && !(expr); qt_test_i += step) { \
            QITest::qWait(step); \
        }

#define QITRY_TIMEOUT_DEBUG_IMPL(expr, timeoutValue, step)\
        if (!(expr)) { \
            QITRY_LOOP_IMPL((expr), (2 * timeoutValue), step);\
            if (expr) { \
                QString msg = QString::fromUtf8("QTestLib: This test case check (\"%1\") failed because the requested timeout (%2 ms) was too short, %3 ms would have been sufficient this time."); \
                msg = msg.arg(QString::fromUtf8(#expr)).arg(timeoutValue).arg(timeoutValue + qt_test_i); \
                QFAIL(qPrintable(msg)); \
            } \
        }

// Ideally we'd use qWaitFor instead of QTRY_LOOP_IMPL, but due
// to a compiler bug on MSVC < 2017 we can't (see QTBUG-59096)
#define QITRY_IMPL(expr, timeout)\
        const int qt_test_step = timeout < 350 ? timeout / 7 + 1 : 50; \
        const int qt_test_timeoutValue = timeout; \
    { QITRY_LOOP_IMPL((expr), qt_test_timeoutValue, qt_test_step); } \
        QITRY_TIMEOUT_DEBUG_IMPL((expr), qt_test_timeoutValue, qt_test_step)\

// Will try to wait for the expression to become true while allowing event processing
#define QITRY_VERIFY_WITH_TIMEOUT(expr, timeout) \
do { \
        QITRY_IMPL((expr), timeout);\
        QIVERIFY(expr); \
} while (false)

#define QITRY_VERIFY(expr) QITRY_VERIFY_WITH_TIMEOUT((expr), 5000)

// Will try to wait for the expression to become true while allowing event processing
#define QITRY_VERIFY2_WITH_TIMEOUT(expr, messageExpression, timeout) \
do { \
        QITRY_IMPL((expr), timeout);\
        QIVERIFY2(expr, messageExpression); \
} while (false)

#define QITRY_VERIFY2(expr, messageExpression) QTRY_VERIFY2_WITH_TIMEOUT((expr), (messageExpression), 5000)

// Will try to wait for the comparison to become successful while allowing event processing
#define QITRY_COMPARE_WITH_TIMEOUT(expr, expected, timeout) \
do { \
        QITRY_IMPL(((expr) == (expected)), timeout);\
        QICOMPARE((expr), expected); \
} while (false)

#define QITRY_COMPARE(expr, expected) QITRY_COMPARE_WITH_TIMEOUT((expr), expected, 5000)

#define QISKIP_INTERNAL(statement) \
do {\
        QITest::qSkip(static_cast<const char *>(statement), __FILE__, __LINE__);\
        return;\
} while (false)

#define QISKIP(statement, ...) QISKIP_INTERNAL(statement)

#define QIEXPECT_FAIL(dataIndex, comment, mode)\
do {\
        if (!QITest::qExpectFail(dataIndex, static_cast<const char *>(comment), QTest::mode, __FILE__, __LINE__))\
            return;\
} while (false)

#define QIFETCH(Type, name)\
        Type name = *static_cast<Type *>(QITest::qData(#name, ::qMetaTypeId<typename std::remove_cv<Type >::type>()))

#define QIFETCH_GLOBAL(Type, name)\
        Type name = *static_cast<Type *>(QITest::qGlobalData(#name, ::qMetaTypeId<typename std::remove_cv<Type >::type>()))

#define QITEST(actual, testElement)\
do {\
        if (!QITest::qTest(actual, testElement, #actual, #testElement, __FILE__, __LINE__))\
            return;\
} while (false)

#define QIWARN(msg)\
        QITest::qWarn(static_cast<const char *>(msg), __FILE__, __LINE__)

#ifdef QT_TESTCASE_BUILDDIR
# define QIFINDTESTDATA(basepath)\
        QITest::qFindTestData(basepath, __FILE__, __LINE__, QT_TESTCASE_BUILDDIR)
#else
# define QIFINDTESTDATA(basepath)\
        QITest::qFindTestData(basepath, __FILE__, __LINE__)
#endif

# define QIEXTRACTTESTDATA(resourcePath) \
        QITest::qExtractTestData(resourcePath)

class QObject;
class QTestData;

/*
#define QITEST_COMPARE_DECL(KLASS)\
        template<> char *toString<KLASS >(const KLASS &);
*/
namespace QTest
{
/*
        namespace Internal {

        template<typename T> // Output registered enums
        inline typename std::enable_if<QtPrivate::IsQEnumHelper<T>::Value, char*>::type toString(T e)
        {
                QMetaEnum me = QMetaEnum::fromType<T>();
                return qstrdup(me.valueToKey(int(e))); // int cast is necessary to support enum classes
        }

        template <typename T> // Fallback
        inline typename std::enable_if<!QtPrivate::IsQEnumHelper<T>::Value, char*>::type toString(const T &)
        {
                return nullptr;
        }

        template<typename F> // Output QFlags of registered enumerations
        inline typename std::enable_if<QtPrivate::IsQEnumHelper<F>::Value, char*>::type toString(QFlags<F> f)
        {
                const QMetaEnum me = QMetaEnum::fromType<F>();
                return qstrdup(me.valueToKeys(int(f)).constData());
        }

        template <typename F> // Fallback: Output hex value
        inline typename std::enable_if<!QtPrivate::IsQEnumHelper<F>::Value, char*>::type toString(QFlags<F> f)
        {
                const size_t space = 3 + 2 * sizeof(unsigned); // 2 for 0x, two hex digits per byte, 1 for '\0'
                char *msg = new char[space];
                qsnprintf(msg, space, "0x%x", unsigned(f));
                return msg;
        }

        } // namespace Internal

        template<typename T>
        inline char *toString(const T &t)
        {
                return Internal::toString(t);
        }

        inline char * toString( const QPointF &p )
        {
                char *msg = new char[128];
                qsnprintf(msg, 128, "QPointF(%f,%f)", p.x(), p.y());
                return msg;
        }

        inline char * toString( const QPoint &p )
        {
                char *msg = new char[128];
                qsnprintf(msg, 128, "QPoint(%d,%d)", p.x(), p.y());
                return msg;
        }
*/
    template<typename T>
    inline char * toString( const QVector<T> &v )
        {
                char *msg = new char[128];
                qsnprintf(msg, 128, "QVector<%s>(%d)", typeid(T).name(), v.size());
                return msg;
        }

        inline char * toString( const QRegion &r )
        {
                char *msg = new char[128];
                qsnprintf(msg, 128, "QRegion(%d)", r.rectCount());
                return msg;
        }
/*
        inline char * toString( const QTime &r )
        {
                char *msg = new char[128];
                qsnprintf(msg, 128, "%s", r.toString().toStdString().c_str());
                return msg;
        }
        template<typename T>
        inline char * toString( const QList<T> &v )
        {
                char *msg = new char[128];
                qsnprintf(msg, 128, "QList<%s>(%d)", typeid(T).name(), v.size());
                return msg;
        }

        template <typename T1, typename T2>
        inline char *toString(const QPair<T1, T2> &pair);

        template <typename T1, typename T2>
        inline char *toString(const std::pair<T1, T2> &pair);

        template <class... Types>
        inline char *toString(const std::tuple<Types...> &tuple);

        char *toHexRepresentation(const char *ba, int length);
        char *toPrettyCString(const char *unicode, int length);
        char *toPrettyUnicode(QStringView string);
        char *toString(const char *);
        char *toString(const void *); */
}

namespace QITest
{

    void qiInit(QObject *testObject, int argc = 0, char **argv = nullptr, const char *customXmlFile = nullptr);
        int qRun();
        void qCleanup();

        int qiExec(QObject *testObject, int argc = 0, char **argv = nullptr, const char *customXmlFile = nullptr);
        //int qiExec(QObject *testObject, int argc = 0, char **argv = nullptr);
        //int qiExec(QObject *testObject, const QStringList &arguments);

        void setMainSourcePath(const char *file, const char *builddir = nullptr);

        bool qVerify(bool statement, const char *statementStr, const char *description,
                                     const char *file, int line);
        void qFail(const char *statementStr, const char *file, int line);
        void qSkip(const char *message, const char *file, int line);
        bool qExpectFail(const char *dataIndex, const char *comment, QTest::TestFailMode mode,
                               const char *file, int line);
        void qWarn(const char *message, const char *file = nullptr, int line = 0);
        void ignoreMessage(QtMsgType type, const char *message);
#if QT_CONFIG(regularexpression)
        void ignoreMessage(QtMsgType type, const QRegularExpression &messagePattern);
#endif

#if QT_CONFIG(temporaryfile)
        QSharedPointer<QTemporaryDir> qExtractTestData(const QString &dirName);
#endif
        QString qFindTestData(const char* basepath, const char* file = nullptr, int line = 0, const char* builddir = nullptr);
        QString qFindTestData(const QString& basepath, const char* file = nullptr, int line = 0, const char* builddir = nullptr);

        void *qData(const char *tagName, int typeId);
        void *qGlobalData(const char *tagName, int typeId);
        void *qElementData(const char *elementName, int metaTypeId);
        QObject *testObject();

        const char *currentAppName();

        const char *currentTestFunction();
        const char *currentDataTag();
        bool currentTestFailed();

        Qt::Key asciiToKey(char ascii);
        char keyToAscii(Qt::Key key);

        bool compare_helper(bool success, const char *failureMsg,
                                             char *val1, char *val2,
                                             const char *actual, const char *expected,
                                             const char *file, int line);
        void qSleep(int ms);
        void addColumnInternal(int id, const char *name);

        template <typename T>
        inline void addColumn(const char *name, T * = nullptr)
        {
                using QIsSameTConstChar = std::is_same<T, const char*>;
                Q_STATIC_ASSERT_X(!QIsSameTConstChar::value, "const char* is not allowed as a test data format.");
                addColumnInternal(qMetaTypeId<T>(), name);
        }
        QTestData &newRow(const char *dataTag);
        QTestData &addRow(const char *format, ...) Q_ATTRIBUTE_FORMAT_PRINTF(1, 2);

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        // kept after adding implementation of <T1, T2> out of paranoia:
        template <typename T>
        inline bool qCompare(T const &t1, T const &t2, const char *description, const char *actual, const char *expected,
                            const char *file, int line)
        {
                return compare_helper(t1 == t2, description ? description : "Compared values are not the same",
                                      QTest::toString(t1), QTest::toString(t2), actual, expected, file, line);
        }
#endif

        bool qCompare(qfloat16 const &t1, qfloat16 const &t2, const char *description,
                      const char *actual, const char *expected, const char *file, int line);

        bool qCompare(float const &t1, float const &t2, const char *description,
                        const char *actual, const char *expected, const char *file, int line);

        bool qCompare(double const &t1, double const &t2, const char *description,
                        const char *actual, const char *expected, const char *file, int line);

        bool qCompare(int t1, int t2, const char *description, const char *actual, const char *expected,
                                       const char *file, int line);

        bool qCompare(unsigned t1, unsigned t2, const char *description, const char *actual, const char *expected,
                                       const char *file, int line);

        bool qCompare(QStringView t1, QStringView t2, const char *description,
                                       const char *actual, const char *expected,
                                       const char *file, int line);
        bool qCompare(QStringView t1, const QLatin1String &t2, const char *description,
                                       const char *actual, const char *expected,
                                       const char *file, int line);
        bool qCompare(const QLatin1String &t1, QStringView t2, const char *description,
                                       const char *actual, const char *expected,
                                       const char *file, int line);
        inline bool qCompare(const QString &t1, const QString &t2, const char *description,
                             const char *actual, const char *expected,
                             const char *file, int line)
        {
                return qCompare(QStringView(t1), QStringView(t2), description, actual, expected, file, line);
        }
        inline bool qCompare(const QString &t1, const QLatin1String &t2, const char *description,
                             const char *actual, const char *expected,
                             const char *file, int line)
        {
                return qCompare(QStringView(t1), t2, description, actual, expected, file, line);
        }
        inline bool qCompare(const QLatin1String &t1, const QString &t2, const char *description,
                             const char *actual, const char *expected,
                             const char *file, int line)
        {
                return qCompare(t1, QStringView(t2), description, actual, expected, file, line);
        }

        inline bool compare_ptr_helper(const volatile void *t1, const volatile void *t2, const char *description, const char *actual,
                                       const char *expected, const char *file, int line)
        {
                return compare_helper(t1 == t2, description ? description : "Compared pointers are not the same",
                                      QTest::toString(t1), QTest::toString(t2), actual, expected, file, line);
        }

        inline bool compare_ptr_helper(const volatile void *t1, std::nullptr_t, const char *description, const char *actual,
                                       const char *expected, const char *file, int line)
        {
                return compare_helper(t1 == nullptr, description ? description : "Compared pointers are not the same",
                                      QTest::toString(t1), QTest::toString(nullptr), actual, expected, file, line);
        }

        inline bool compare_ptr_helper(std::nullptr_t, const volatile void *t2, const char *description, const char *actual,
                                       const char *expected, const char *file, int line)
        {
                return compare_helper(nullptr == t2, description ? description : "Compared pointers are not the same",
                                      QTest::toString(nullptr), QTest::toString(t2), actual, expected, file, line);
        }

        bool compare_string_helper(const char *t1, const char *t2, const char *description, const char *actual,
                                          const char *expected, const char *file, int line);

        char *formatString(const char *prefix, const char *suffix, size_t numArguments, ...);

        /*
#ifndef Q_QDOC
        QITEST_COMPARE_DECL(short)
        QITEST_COMPARE_DECL(ushort)
        QITEST_COMPARE_DECL(int)
        QITEST_COMPARE_DECL(uint)
        QITEST_COMPARE_DECL(long)
        QITEST_COMPARE_DECL(ulong)
        QITEST_COMPARE_DECL(qint64)
        QITEST_COMPARE_DECL(quint64)

        QITEST_COMPARE_DECL(float)
        QITEST_COMPARE_DECL(double)
        QITEST_COMPARE_DECL(qfloat16)
        QITEST_COMPARE_DECL(char)
        QITEST_COMPARE_DECL(signed char)
        QITEST_COMPARE_DECL(unsigned char)
        QITEST_COMPARE_DECL(bool)
#endif
*/

        template <typename T1, typename T2>
        inline bool qCompare(const T1 &t1, const T2 &t2, const char *description, const char *actual, const char *expected,
                             const char *file, int line)
        {
                return compare_helper(t1 == t2, description ? description : "Compared values are not the same",
                                      QTest::toString(t1), QTest::toString(t2), actual, expected, file, line);
        }

        inline bool qCompare(double const &t1, float const &t2, const char *description, const char *actual,
                                     const char *expected, const char *file, int line)
        {
                return qCompare(qreal(t1), qreal(t2), description, actual, expected, file, line);
        }

        inline bool qCompare(float const &t1, double const &t2, const char *description, const char *actual,
                                     const char *expected, const char *file, int line)
        {
                return qCompare(qreal(t1), qreal(t2), description, actual, expected, file, line);
        }

        template <typename T>
        inline bool qCompare(const T *t1, const T *t2, const char *description, const char *actual, const char *expected,
                            const char *file, int line)
        {
                return compare_ptr_helper(t1, t2, description, actual, expected, file, line);
        }
        template <typename T>
        inline bool qCompare(T *t1, T *t2, const char *description, const char *actual, const char *expected,
                            const char *file, int line)
        {
                return compare_ptr_helper(t1, t2, description, actual, expected, file, line);
        }

        template <typename T>
        inline bool qCompare(T *t1, std::nullptr_t, const char *description, const char *actual, const char *expected,
                            const char *file, int line)
        {
                return compare_ptr_helper(t1, nullptr, description, actual, expected, file, line);
        }
        template <typename T>
        inline bool qCompare(std::nullptr_t, T *t2, const char *description, const char *actual, const char *expected,
                            const char *file, int line)
        {
                return compare_ptr_helper(nullptr, t2, description, actual, expected, file, line);
        }

        template <typename T1, typename T2>
        inline bool qCompare(const T1 *t1, const T2 *t2, const char *description, const char *actual, const char *expected,
                            const char *file, int line)
        {
                return compare_ptr_helper(t1, static_cast<const T1 *>(t2), description, actual, expected, file, line);
        }
        template <typename T1, typename T2>
        inline bool qCompare(T1 *t1, T2 *t2, const char *description, const char *actual, const char *expected,
                            const char *file, int line)
        {
                return compare_ptr_helper(const_cast<const T1 *>(t1),
                        static_cast<const T1 *>(const_cast<const T2 *>(t2)), description actual, expected, file, line);
        }
        inline bool qCompare(const char *t1, const char *t2, const char *description, const char *actual,
                                           const char *expected, const char *file, int line)
        {
                return compare_string_helper(t1, t2, description, actual, expected, file, line);
        }
        inline bool qCompare(char *t1, char *t2, const char *description, const char *actual, const char *expected,
                            const char *file, int line)
        {
                return compare_string_helper(t1, t2, description, actual, expected, file, line);
        }

        /* The next two overloads are for MSVC that shows problems with implicit
           conversions
         */
        inline bool qCompare(char *t1, const char *t2, const char *description, const char *actual,
                             const char *expected, const char *file, int line)
        {
                return compare_string_helper(t1, t2, description, actual, expected, file, line);
        }
        inline bool qCompare(const char *t1, char *t2, const char *description, const char *actual,
                             const char *expected, const char *file, int line)
        {
                return compare_string_helper(t1, t2, description, actual, expected, file, line);
        }

        template <class T>
        inline bool qTest(const T& actual, const char *elementName, const char *actualStr,
                         const char *expected, const char *file, int line)
        {
                return qCompare(actual, *static_cast<const T *>(QITest::qElementData(elementName,
                               qMetaTypeId<T>())), nullptr, actualStr, expected, file, line);
        }
}

#undef QTEST_COMPARE_DECL

QT_END_NAMESPACE

#endif // QITESTCASE_H
