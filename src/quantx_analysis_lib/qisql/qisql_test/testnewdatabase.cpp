#include "qisqltest.h"

#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/backend/Postgres.h>
#include <Wt/Dbo/backend/Sqlite3.h>

#include "model/acquisitionprotocol.h"
#include "model/auditlog.h"
#include "model/dicomdata.h"
#include "model/images2d.h"
#include "model/lesion.h"
#include "model/lesiondetails.h"
#include "model/motioncorrection.h"
#include "model/overlay.h"
#include "model/patient.h"
#include "model/plugindata.h"
#include "model/dicomseries.h"
#include "model/seriesthumbnails.h"
#include "model/dicomstudy.h"
#include "model/useraccess.h"

#include "databasesettings.h"
#include "testsqlmainwindow.h"
#include "sqldatabaseservice.h"
#include "patientmodel.h"
#include "qi2dimage.h"


#define useDebug 0
#if useDebug
#include <QDebug>
#endif

namespace {
#if DB_DRIVER == POSTGRES_DB
    constexpr auto driverType = "QPSQL";
    constexpr auto hostName = "localhost";
    constexpr auto port = "5432";
    constexpr auto databaseName = "blankqisqltest";
    constexpr auto userName = "quantx";
    constexpr auto password = "postgres";
#elif DB_DRIVER == SQLITE_DB
    constexpr auto driverType = "QSQLITE";
    constexpr auto hostName = "";
    constexpr auto port = "";
    constexpr auto databaseName = "C:\\Quantx\\qisqltest";
    constexpr auto userName = "";
    constexpr auto password = "";
#endif

    namespace TableNames {
	    constexpr const auto acquisitionProtocol = "qi_acquisition_protocols";
	    constexpr const char* auditLog = "qi_auditlog";
	    constexpr const char* dicomData = "dicom_data";
	    constexpr const char* images2d = "qi_2d_images";
	    constexpr const char* lesions = "qi_lesions";
	    constexpr const char* lesionDetails = "qi_lesiondetails";
	    constexpr const char* motionCorrection = "qi_motion_correction";
	    constexpr auto overlay = "qi_overlay";
	    constexpr const char* patients = "qi_patients";
	    constexpr auto plugins = "qi_plugin_data";
	    constexpr const char* series = "qi_series";
	    constexpr const char* seriesThumbnails = "qi_series_thumbnails";
	    constexpr const char* studies = "qi_studies";
	    constexpr const char* userAccess = "qi_useraccess";
	}
}

using namespace quantx::patient;
using namespace quantx::auditlog;
using namespace quantx::dicom;
using namespace quantx::mranalysis;
using namespace quantx::overlay;
using namespace quantx::acquisition;
using namespace quantx::sql;

void QISqlTest::testNewDatabase()
{
    QIWARN("TSQL 3");

	std::unique_ptr<Wt::Dbo::SqlConnection> connection;
	Wt::Dbo::Session session;

	DatabaseSettings dbSettings{ driverType, hostName, databaseName, userName, password, port };

#if DB_DRIVER == POSTGRES_DB
	try{
		auto params = QString{ "host=%1 port=%2 dbname=postgres user=%3 password=%4" }
		              .arg( dbSettings.hostName() )
		              .arg( dbSettings.port() )
		              .arg( dbSettings.userName() )
		              .arg( dbSettings.password() );
		connection = std::make_unique<Wt::Dbo::backend::Postgres>( params.toStdString() );
		connection->executeSql( QString{ "create database %1" }.arg( dbSettings.databaseName() ).toStdString() );
	} catch( const std::exception& e ) {
#if useDebug
		qDebug() << QString{ "Postgres database create new database failed: %1" }.arg( e.what() );
#endif
		QIWARN( QString{ "Postgres database create new database failed: %1" }.arg( e.what() ).toUtf8() );
	}

	try {
		auto params = QString{ "host=%1 port=%2 dbname=%3 user=%4 password=%5" }
		              .arg( dbSettings.hostName() )
		              .arg( dbSettings.port() )
		              .arg( dbSettings.databaseName() )
		              .arg( dbSettings.userName() )
		              .arg( dbSettings.password() );
		static_cast<Wt::Dbo::backend::Postgres*>( connection.get() )->connect( params.toStdString() );
	} catch ( const std::exception& e ) {
		QIVERIFY2( false, QString{ "Postgres database connect new database failed: %1" }.arg( e.what() ).toUtf8() );
	}
	QIVERIFY2( true, "Postgres connection to new database successful." );
#elif DB_DRIVER == SQLITE_DB
	try{
		connection = std::make_unique<Wt::Dbo::backend::Sqlite3>( dbSettings.databaseName().toStdString() );
	} catch( const std::exception& e ) {
		QIVERIFY2( false, QString{ "Create new SQLite database failed: %1" }.arg( e.what() ).toUtf8() );
	}
#endif
	try{
		session.setConnection( connection->clone() );
	} catch( const std::exception& e ) {
		QIVERIFY2( false, QString{ "Create new session failed: %1" }.arg( e.what() ).toUtf8() );
	}
	QIVERIFY2( true, "Create new session succeeded" );

	try {
		session.mapClass<AcquisitionProtocol>( TableNames::acquisitionProtocol );
		session.mapClass<AuditLog>( TableNames::auditLog );
		session.mapClass<DicomData>( TableNames::dicomData );
		session.mapClass<Images2D>( TableNames::images2d );
		session.mapClass<Lesion>( TableNames::lesions );
		session.mapClass<LesionDetails>( TableNames::lesionDetails );
		session.mapClass<MotionCorrection>( TableNames::motionCorrection );
		session.mapClass<DbOverlay>( TableNames::overlay );
		session.mapClass<Patient>( TableNames::patients );
		session.mapClass<PluginData>( TableNames::plugins );
		session.mapClass<DicomSeries>( TableNames::series );
		session.mapClass<SeriesThumbnails>( TableNames::seriesThumbnails );
		session.mapClass<DicomStudy>( TableNames::studies );
		session.mapClass<UserAccess>( TableNames::userAccess );
	} catch ( const std::exception& e ) {
		QIVERIFY2( false, QString{ "Mapping classes failed: %1" }.arg( e.what() ).toUtf8() );
	}
	QIVERIFY2( true, "Mapping classes succeeded" );

	try {
		session.createTables();
	} catch ( const std::exception& e ) {
#if useDebug
		qDebug() << QString{ "Test Db tables aleady created! %1" }.arg( e.what() );
#endif
		QIWARN( QString{ "Test Db tables aleady created! %1" }.arg( e.what() ).toUtf8() );
	}


//    QStringList dbSettings;
//    dbSettings << "QSQLITE" << "" << "tempdb.db" << "" << "";
//    QFile tempdbFile(dbSettings[2]);
//    if(tempdbFile.exists())
//        tempdbFile.remove();

//    QSqlDatabase db = QSqlDatabase::addDatabase(dbSettings.at(0),"CreateEmptyDB");
//    db.setDatabaseName(dbSettings.at(2));
//    db.open();
//    QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
//    QFile commandFile(":/sql/sqliteInitialize");
//    QStringList commands;
//    commandFile.open(QIODevice::ReadOnly);
//    while(!commandFile.atEnd())
//        commands << commandFile.readLine();
//    commandFile.close();
//	for(const auto& command : commands)
//        sqlQuery->exec(command);

//    QScopedPointer<QISql> qisql(new QISql(dbSettings));
//    QStringList tableList = db.tables();
//    QStringList viewList = db.tables(QSql::Views);

//    QIVERIFY2(tableList.contains(qisql->imageTable(QUANTX::MRI)), "Test whether database contains MR image table");
//    QIVERIFY2(tableList.contains(qisql->imageTable(QUANTX::MAMMO)), "Test whether database contains Mammo image table");
//    QIVERIFY2(tableList.contains(qisql->imageTable(QUANTX::US)), "Test whether database contains Ultrasound image table");
//    QIVERIFY2(tableList.contains(qisql->imageTable(QUANTX::NOIMAGE)), "Test whether database contains Unknown Type image table");
//    QIVERIFY2(tableList.contains(qisql->seriesTable()), "Test whether database contains Series table");
//    QIVERIFY2(tableList.contains(qisql->studiesTable()), "Test whether database contains Studies table");
//    QIVERIFY2(tableList.contains(qisql->patientsTable()), "Test whether database contains Patients table");
//    QIVERIFY2(tableList.contains(qisql->dicomTable()), "Test whether database contains DICOM header table");
//    QIVERIFY2(tableList.contains(qisql->thumbnailTable()), "Test whether database contains Thumbnail table");
//    QIVERIFY2(viewList.contains(qisql->seriesView()), "Test whether database contains Series view");
//    QIVERIFY2(viewList.contains(qisql->imageLocationsView()), "Test whether database contains Image Locations view");

	QScopedPointer<TestSqlMainWindow> sqlWindow(new TestSqlMainWindow);
	QStringList fileList;
	fileList << "sql_test/patient_00050_US_IM_0000" << "sql_test/patient_00050_US_IM_0002";
	sqlWindow->openFiles( fileList, dbSettings.getDBSettingsList() );
	QIVERIFY2(true, "Add Test Images To Database");
	sqlWindow.reset();

	QSettings settings{};
	QScopedPointer<SqlDatabaseService> dbService( new SqlDatabaseService( settings ) );
	dbService->databaseSettings_ = dbSettings;
	auto error = dbService->initialize( true );
	QIVERIFY2( error.bSuccess, "Database service connected to new database" );

	auto shallowPatientsModel = dbService->findAllPatientsShallow().get();

#if useDebug
	qDebug() << "tempDB rowCount:" << shallowPatientsModel->rowCount();
#endif
	QICOMPARE2( shallowPatientsModel->columnCount(), 5, "Test number of columns in shallow patient model" );
	QICOMPARE2( shallowPatientsModel->rowCount(), 1, "Test number of rows in shallow patient model" );

#if useDebug
	qDebug() << shallowPatientsModel->item(0,0)->text()
	         << shallowPatientsModel->item(0,1)->text()
	         << shallowPatientsModel->item(0,2)->text()
	         << shallowPatientsModel->item(0,3)->text();
#endif
	QICOMPARE2( shallowPatientsModel->item(0,0)->text(), QString("00050 "), "Test name of patient in shallow patient model" );
	QICOMPARE2( shallowPatientsModel->item(0,1)->text(), QString("00050 "), "Test MRN of patient in shallow patient model" );
	QICOMPARE2( shallowPatientsModel->item(0,2)->text(), QString("Tue Apr 18 2006"), "Test MR date of patient in shallow patient model" );
	QICOMPARE2( shallowPatientsModel->item(0,3)->text(), QString("00050_US_18-Apr-2006"), "Test Accession number in shallow patient model" );
	QICOMPARE2( shallowPatientsModel->item(0,4)->text(), QString("F "), "Test sex of patient in shallow patient model" );

	QScopedPointer<Qi2DImage> image( new Qi2DImage );
	auto returnval = image->openSeries( "1.3.46.670589.998.3.1320866118252.4848", QUANTX::US, dbSettings.getDBSettingsList() );

#if useDebug
	qDebug() << returnval;
#endif

	QICOMPARE2(returnval, 2, "Load image series from database");

#if useDebug
	qDebug() << image->windowWidth() <<
	            image->windowLevel() <<
	            image->maxPixval() <<
	            image->minPixval() <<
	            image->dx() <<
	            image->dy() <<
	            image->nx() <<
	            image->ny() <<
	            image->modality() <<
	            image->seriesUID() <<
	            image->scanOptions() <<
	            image->mrn() <<
	            image->seriesDesc() <<
	            image->time() <<
	            image->viewPosition() <<
	            image->imageLaterality() <<
	            image->patientOrientation() <<
	            image->photometricInterpretation() <<
	            image->manufacturer() <<
	            image->modelName();
#endif

	QICOMPARE2(image->windowWidth(), 256., "Test window width (contrast) of image");
	QICOMPARE2(image->windowLevel(), 128., "Test window level (brightness) of image");
	QICOMPARE2(image->maxPixval(), quint16(0), "Test Max Pixval (from DICOM header)");
	QICOMPARE2(image->minPixval(), quint16(0), "Test Min Pixval (from DICOM header)");
	QICOMPARE2(image->dx(), static_cast<float>(0.105), "Test pixel width");
	QICOMPARE2(image->dy(), static_cast<float>(0.105), "Test pixel height");
	QICOMPARE2(image->nx(), 640, "Test image width");
	QICOMPARE2(image->ny(), 476, "Test image height");
	QICOMPARE2(image->modality(), QUANTX::Modality::US, "Test modality of image");
	QICOMPARE2(image->seriesUID(), QString("1.3.46.670589.998.3.1320866118252.4848"), "Test Series UID of image");
	//QICOMPARE(image->scanOptions(), QStringList(""));
	QICOMPARE2(image->mrn(), QString("00050 "), "Test image MRN");
	QICOMPARE2(image->seriesDesc(), QString("L BIU CORE NB S&I US GUIDE"), "Test image series description");
	//QICOMPARE(image->time(), QTime(9,52,36));
	QICOMPARE2(image->viewPosition(), QString(""), "Test image view position");
	QICOMPARE2(image->imageLaterality(), QString(""), "Test image laterality");
	QICOMPARE2(image->patientOrientation(), QString(""), "Test patient orientation");
	QICOMPARE2(image->photometricInterpretation(), QString("MONOCHROME2 "), "Test pohotometric interpretation");
	QICOMPARE2(image->manufacturer(), QString("ATL "), "Test image acquisition device manufacturer");
	QICOMPARE2(image->modelName(), QString("HDI 5000"), "Test image acquisition device model");
	image.reset();

	QScopedPointer<Qi2DImage> image2( new Qi2DImage );
	returnval = image2->openSeries("1.3.46.670589.998.3.1320866118252.4848", QUANTX::US, dbSettings.getDBSettingsList(), 1);

#if useDebug
	qDebug() << returnval;
#endif

	QICOMPARE2(returnval, 2, "Load image series from database");

#if useDebug
	qDebug() << image2->windowWidth() <<
	            image2->windowLevel() <<
	            image2->maxPixval() <<
	            image2->minPixval() <<
	            image2->dx() <<
	            image2->dy() <<
	            image2->nx() <<
	            image2->ny() <<
	            image2->modality() <<
	            image2->seriesUID() <<
	            image2->scanOptions() <<
	            image2->mrn() <<
	            image2->seriesDesc() <<
	            image2->time() <<
	            image2->viewPosition() <<
	            image2->imageLaterality() <<
	            image2->patientOrientation() <<
	            image2->photometricInterpretation() <<
	            image2->manufacturer() <<
	            image2->modelName();
#endif

	QICOMPARE2(image2->windowWidth(), 256., "Test window width (contrast) of image");
	QICOMPARE2(image2->windowLevel(), 128., "Test window level (brightness) of image");
	QICOMPARE2(image2->maxPixval(), quint16(0), "Test Max Pixval (from DICOM header)");
	QICOMPARE2(image2->minPixval(), quint16(0), "Test Min Pixval (from DICOM header)");
	QICOMPARE2(image2->dx(),static_cast<float>(0.105), "Test pixel width");
	QICOMPARE2(image2->dy(),static_cast<float>(0.105), "Test pixel height");
	QICOMPARE2(image2->nx(), 640, "Test image width");
	QICOMPARE2(image2->ny(), 476, "Test image height");
	QICOMPARE2(image2->modality(), QUANTX::Modality::US, "Test modality of image");
	QICOMPARE2(image2->seriesUID(), QString("1.3.46.670589.998.3.1320866118252.4848"), "Test Series UID of image");
	//QICOMPARE(imag2e->scanOptions(), QStringList(""));
	QICOMPARE2(image2->mrn(), QString("00050 "), "Test image MRN");
	QICOMPARE2(image2->seriesDesc(), QString("L BIU CORE NB S&I US GUIDE"), "Test image series description");
	//QICOMPARE(imag2e->time(), QTime(9,53,33));
	QICOMPARE2(image2->viewPosition(), QString(""), "Test image view position");
	QICOMPARE2(image2->imageLaterality(), QString(""), "Test image laterality");
	QICOMPARE2(image2->patientOrientation(), QString(""), "Test patient orientation");
	QICOMPARE2(image2->photometricInterpretation(), QString("RGB "), "Test photometric interpretation");
	QICOMPARE2(image2->manufacturer(), QString("ATL "), "Test image acquisition device manufacturer");
	QICOMPARE2(image2->modelName(), QString("HDI 5000"), "Test image acquisition device model");

	// Close the connection to the new database.
	image2.reset();
	dbService.reset();

	try {
		session.dropTables();
	} catch ( const std::exception& e ) {
#if useDebug
		qDebug() << QString{ "Postgres database new database cleanup failed: %1" }.arg( e.what() ).toUtf8();
#endif
		QIWARN( QString{ "Postgres database new database cleanup failed: %1" }.arg( e.what() ).toUtf8() );
	}

//    db.close();
//    tempdbFile.remove();

}
