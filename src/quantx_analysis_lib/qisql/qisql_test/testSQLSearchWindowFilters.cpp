#include "qisqltest.h"
#include "sqlsearchwindow.h"
#include <QLineEdit>
#include <QPushButton>
#include "databasesettings.h"

#define useDebug 0
#if useDebug
#include <QDebug>
#define VISUAL_WAIT_TIME 1000
#endif

#define WAIT_TIME 500

//void QISqlTest::testSQLSearchWindowFilters_data()
//{
//	QIWARN("TSQL 4");

//	QTest::addColumn<QString>("searchBarText");
//	QTest::addColumn<int>("expectedNumRows");

//	QTest::addRow("PopulateTable") << "" << 27;
//	QTest::addRow("CaseName") << "r" << 3;
//	QTest::addRow("MRN") << "53" << 3;
//	QTest::addRow("AccessionNumber1") << "456" << 3;
//	QTest::addRow("AccessionNumber2") << "123456" << 2;
//	QTest::addRow("AccessionNumber2") << "123-" << 1;
//	QTest::addRow("DOB - Year") << "2011" << 11;
//	QTest::addRow("DOB - YearMonth") << "2010-11" << 6;
//	QTest::addRow("DOB - YearMonthDay") << "2010-11-15" << 2;
//}

//int checkVisibleRows(QTableWidget* tableWidget)
//{
//	int visibleRows = 0;
//	for(int row = 0; row < tableWidget->rowCount(); ++row)
//	{
//		if(!tableWidget->isRowHidden(row))
//		{
//			++visibleRows;
//		}
//	}

//	return visibleRows;
//}

//void QISqlTest::testSQLSearchWindowFilters()
//{
//	QIFETCH(QString, searchBarText);
//	QIFETCH(int, expectedNumRows);

//	// Open Search Window
//	DatabaseSettings dbSettings;
//        dbSettings.setDatabaseDriverType("QSQLITE");
//	// @NOTE: Grabbing the database from the Network Drive is very slow
//	dbSettings.setDatabaseName("C:/QuantX/quantx.db");
//	QScopedPointer<SqlSearchWindow, QScopedPointerDeleteLater> searchWindow(new SqlSearchWindow(dbSettings.getDBSettingsList()));

//	// Grab the SearchBar and TableView
//	auto searchBar = searchWindow->findChild<QLineEdit*>("sqlSearchBar");
//	auto tableWidget = searchWindow->findChild<QTableWidget*>("searchWindowTableWidget");

//#if useDebug
//	searchWindow->show();
//#endif

//	// Set SearchBar text
//	searchBar->setText(searchBarText);
//#if useDebug
//	qDebug() << "Text to search for:" << searchBarText;
//	QTest::qWait(VISUAL_WAIT_TIME);
//#endif

//	QICOMPARE2(checkVisibleRows(tableWidget), expectedNumRows, "Testing Number of Rows");

//#if useDebug
//	qDebug() << "Num Visible Rows:" << checkVisibleRows(tableWidget);
//	QTest::qWait(VISUAL_WAIT_TIME);
//	qDebug() << "Testing Clear Search and Rescan Buttons" << endl;
//#endif

//	searchBar->setText("Testing Clear Search Button");
//	auto clearButton = searchWindow->findChild<QPushButton*>("sqlClearSearchButton");

//#if useDebug
//	QTest::qWait(VISUAL_WAIT_TIME);
//#endif

//	clearButton->animateClick();
//	QTest::qWait(WAIT_TIME);

//#if useDebug
//	QTest::qWait(VISUAL_WAIT_TIME);
//#endif

//	QIVERIFY2(searchBar->text().isEmpty(), "Testing Clear Search Button Cleared SearchBar Text");

//	searchBar->setText("Testing Rescan Button");
//	auto rescanButton = searchWindow->findChild<QPushButton*>("sqlRescanButton");

//#if useDebug
//	QTest::qWait(VISUAL_WAIT_TIME);
//#endif

//	rescanButton->animateClick();
//	QTest::qWait(WAIT_TIME);

//#if useDebug
//	QTest::qWait(VISUAL_WAIT_TIME);
//#endif

//	QIVERIFY2(searchBar->text().isEmpty(), "Testing Rescan Button Cleared SearchBar Text");
//}
