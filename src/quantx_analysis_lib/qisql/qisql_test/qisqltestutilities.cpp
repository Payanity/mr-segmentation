#include "qisqltest.h"

namespace {
    constexpr auto driverType = "QPSQL";
    constexpr auto hostName = "localhost";
    constexpr auto port = "5432";
    constexpr auto databaseName = "qisqltest";
    constexpr auto userName = "quantx";
    constexpr auto password = "postgres";
}

void QISqlTest::initTestCase()
{
	QIWARN("TSQL 1");

	initializeCurrentDbSettings();

	QIVERIFY2(true, "Test Initialization");
}

void QISqlTest::initializeCurrentDbSettings()
{
	this->currentDbSettings_ = DatabaseSettings{ driverType, hostName, databaseName, userName, password, port };
}
