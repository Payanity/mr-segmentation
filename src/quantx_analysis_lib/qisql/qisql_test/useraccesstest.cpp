#include "qisqltest.h"

#include "useraccessdatabase.h"
#include "model/useraccess.h"

#define useDebug 0

#if useDebug
#include <QDebug>
#endif

void clearUserAccessEntriesWithDBName(const QStringList& dbSettings)
{
	bool bSuccess;
	std::unique_ptr<QuantXUserAccessDatabase> userAccessDB = std::make_unique<QuantXUserAccessDatabase>(dbSettings, &bSuccess);
	if(bSuccess)
	{
		userAccessDB->clearAllEntries();
	}
}

// This is for Cleaning up and the end
void QISqlTest::clearUserAccessEntries()
{
	clearUserAccessEntriesWithDBName( this->currentDbSettings_.getDBSettingsList() );
}

void QISqlTest::userAccessTest_data()
{
	QIWARN("TSQL 7");

	QStringList dbSettings = this->currentDbSettings_.getDBSettingsList();
	clearUserAccessEntriesWithDBName(dbSettings);

	// Input Actual data
	bool bSuccess;
	std::unique_ptr<QuantXUserAccessDatabase> userAccessDB = std::make_unique<QuantXUserAccessDatabase>(dbSettings, &bSuccess);
	if(!bSuccess)
	{
		QSKIP("Cannot connect to database.");
	}
	userAccessDB->addNewUser("nandre", "yes", "no");
	userAccessDB->addNewUser("rtomek", "no", "yes");
	userAccessDB->addNewUser("mchinander", "no", "no");
	userAccessDB->addNewUser("dlevinson", "yes", "yes");
	userAccessDB->addNewGroup("sales", "yes", "yes");
	userAccessDB->addNewGroup("operations", "no", "yes");
	userAccessDB->addNewGroup("chiefs", "yes", "no");

	userAccessDB->modifyUser("rtomek", "yes", "yes");
	userAccessDB->modifyGroup("chiefs", "yes", "yes");

	userAccessDB->removeUser("dlevinson");
	userAccessDB->removeGroup("sales");


	QTest::addColumn<QString>("expected_username");
	QTest::addColumn<QString>("expected_groupname");
	QTest::addColumn<QString>("expected_enabled");
	QTest::addColumn<QString>("expected_allowed");
	QTest::addColumn<QString>("actual_username");
	QTest::addColumn<QString>("actual_groupname");
	QTest::addColumn<QString>("actual_enabled");
	QTest::addColumn<QString>("actual_allowed");

	// Grab Database and corresponding values
	auto items = userAccessDB->getAllItems();
	const int actualNumItems = 6;

	QICOMPARE2(static_cast<int>(items.size()), actualNumItems, "Testing Audit Log DB Size");

	QStringList actualUserNames;
	QStringList actualGroupNames;
	QStringList actualEnabled;
	QStringList actualAllowed;

	for(auto item : items)
	{
		actualUserNames.append(item.userName.c_str());
		actualGroupNames.append(item.groupName.c_str());
		actualEnabled.append(item.enabled.c_str());
		actualAllowed.append(item.allow.c_str());
	}
#if useDebug
	qDebug() << "UserNames:" << actualUserNames;
	qDebug() << "GroupNames:" << actualGroupNames;
	qDebug() << "Enabled:" << actualEnabled;
	qDebug() << "Allowed:" << actualAllowed;
#endif

	QString rowName;
	QString expectedUserName;
	QString expectedGroupName;
	QString expectedEnabled;
	QString expectedAllowed;

	for(int itemInd = 0; itemInd < actualNumItems; ++itemInd)
	{
		if( actualUserNames.at( itemInd ) == QUANTX_DEFAULT_ACCESS_USER ){
			rowName = "Default User";
			expectedUserName = QUANTX_DEFAULT_ACCESS_USER;
			expectedGroupName = "";
			expectedEnabled = "yes";
			expectedAllowed = "yes";
		} else if( actualUserNames.at( itemInd ) == "nandre" ) {
			rowName = "User_Nandre";
			expectedUserName = "nandre";
			expectedGroupName = "";
			expectedEnabled = "yes";
			expectedAllowed = "no";
		} else if( actualUserNames.at( itemInd ) == "rtomek" ) {
			rowName = "User_RTomek";
			expectedUserName = "rtomek";
			expectedGroupName = "";
			expectedEnabled = "yes";
			expectedAllowed = "yes";
		} else if( actualUserNames.at( itemInd ) == "mchinander" ){
			rowName = "User_Mchinander";
			expectedUserName = "mchinander";
			expectedGroupName = "";
			expectedEnabled = "no";
			expectedAllowed = "no";
		} else if( actualGroupNames.at( itemInd ) == "operations" ) {
			rowName = "Group_Operations";
			expectedUserName = "";
			expectedGroupName = "operations";
			expectedEnabled = "no";
			expectedAllowed = "yes";
		} else if( actualGroupNames.at( itemInd ) == "chiefs" ) {
			rowName = "Group_Chiefs";
			expectedUserName = "";
			expectedGroupName = "chiefs";
			expectedEnabled = "yes";
			expectedAllowed = "yes";
		}

		QTest::addRow(rowName.toStdString().c_str())
		        << expectedUserName
		        << expectedGroupName
		        << expectedEnabled
		        << expectedAllowed
		        << actualUserNames[itemInd]
		        << actualGroupNames[itemInd]
		        << actualEnabled[itemInd]
		        << actualAllowed[itemInd];
	}




}

void QISqlTest::userAccessTest()
{
	QIFETCH(QString, expected_username);
	QIFETCH(QString, expected_groupname);
	QIFETCH(QString, expected_enabled);
	QIFETCH(QString, expected_allowed);
	QIFETCH(QString, actual_username);
	QIFETCH(QString, actual_groupname);
	QIFETCH(QString, actual_enabled);
	QIFETCH(QString, actual_allowed);

	QICOMPARE2(actual_username.toStdString().c_str(), expected_username.toStdString().c_str(), "Testing Username Match");
	QICOMPARE2(actual_groupname.toStdString().c_str(), expected_groupname.toStdString().c_str(), "Testing GroupName Match");
	QICOMPARE2(actual_enabled.toStdString().c_str(), expected_enabled.toStdString().c_str(), "Testing Enabled Match");
	QICOMPARE2(actual_allowed.toStdString().c_str(), expected_allowed.toStdString().c_str(), "Testing Allowed Match");


}
