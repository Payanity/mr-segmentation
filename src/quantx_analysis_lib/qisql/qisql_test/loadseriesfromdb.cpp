#include "qisqltest.h"
#include <QSettings>
#include <QCryptographicHash>
#include <QByteArray>
#include <QBuffer>
#include <QDataStream>
#include "qimriimage.h"
#include "quantx.h"
#include "../../quantxdata/quantxdata_lib/readwriteappdata.h"

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

Q_DECLARE_METATYPE(QVector<int>)
Q_DECLARE_METATYPE(QUANTX::Modality)

void QISqlTest::loadSeriesFromDB_data()
{
	QIWARN("TSQL 5");

	QITest::addColumn<QString>("seriesUID");
	QITest::addColumn<QUANTX::Modality>("modality");
	QITest::addColumn<QVector<int> >("expectedDims");
	QITest::addColumn<QString>("md5Sum");

	QVector<int> expectedDims;
	expectedDims << 400 << 400 << 215 << 5;

	//Should add a row for each modality/manufacturer/acquisition type
	QITest::newRow("Philips - dyneTHRIVE - 008") << "1.3.46.670589.11.326748640.1756260987.1570855328.2568365236"
	                                       << QUANTX::MRI
	                                       << expectedDims
	                                       << "d41d8cd98f00b204e9800998ecf8427e";

	expectedDims.clear();
	expectedDims << 420 << 448 << 176 << 1;
	QITest::newRow("Siemens - Dynamic 3D Post - ANON1ITDFA18O") << "1.3.12.2.1107.5.2.30.25609.2014091512113170633516824.0.0.1"
	                                               << QUANTX::MRI
	                                               << expectedDims
	                                               << "d41d8cd98f00b204e9800998ecf8427e";


	expectedDims.clear();
	expectedDims << 92 << 256 << 256 << 4;
	QITest::newRow("GE - T1 Sagittal post - 001 Breast MR") << "1.3.6.1.4.1.18047.1.11.10020461424147699177"
	                                               << QUANTX::MRI
	                                               << expectedDims
	                                               << "d41d8cd98f00b204e9800998ecf8427e";

}

void QISqlTest::loadSeriesFromDB()
{

	const QSettings::Format XmlFormat = QSettings::registerFormat("xml", QuantXSettingsFile::readXmlSettings, QuantXSettingsFile::writeXmlSettings);
	QScopedPointer<QSettings> settings(new QSettings(XmlFormat, QSettings::UserScope, "Quantitative Insights", "QuantXdata"));

	auto dbSettings = this->currentDbSettings_.getDBSettingsList();

	QStringList dynMultiSeriesDesc;
	QStringList fastDynamicSeriesDesc;
	settings->beginGroup("MRIseriesDescriptions");
	dynMultiSeriesDesc << "T1 AX VIBE 3D 1 PRE + 5 POST DYN";
	dynMultiSeriesDesc = settings->value("DCEmultipleSeries",dynMultiSeriesDesc).toStringList();
	fastDynamicSeriesDesc = settings->value("FastDynamicSeries", QStringList()).toStringList();
	settings->endGroup();

	//Fetch data
	QIFETCH(QString, seriesUID);
	QIFETCH(QUANTX::Modality, modality);
	QIFETCH(QVector<int>,expectedDims);
	QIFETCH(QString, md5Sum);

	if ( modality == QUANTX::MRI )
	{

#if useDebug
		qDebug() << "Loading MRI image from DB";
		qDebug() << "DBSettings: " << dbSettings;
		qDebug() << "DCEmultipleSeries: " << dynMultiSeriesDesc;
		qDebug() << "FastDynamicSeries: " << fastDynamicSeriesDesc;
#endif

		QScopedPointer<QiMriImage> image(new QiMriImage());
		image->openSeries(seriesUID, dbSettings, dynMultiSeriesDesc, fastDynamicSeriesDesc);

#if useDebug
		qDebug() << "nx:" << image->nx();
		qDebug() << "ny:" << image->ny();
		qDebug() << "nz:" << image->nz();
		qDebug() << "nt:" << image->nt();
#endif

		QICOMPARE2(image->nx(), expectedDims.at(0), "Verify X size of image");
		QICOMPARE2(image->ny(), expectedDims.at(1), "Verify Y size of image");
		QICOMPARE2(image->nz(), expectedDims.at(2), "Verify Z size of image");
		QICOMPARE2(image->nt(), expectedDims.at(3), "Verify Temporal size of image");

		QScopedPointer<QCryptographicHash> createImageHash(new QCryptographicHash(QCryptographicHash::Md5));
		QByteArray byteArray;
		QBuffer buffer(&byteArray);
		buffer.open(QIODevice::WriteOnly);
		QDataStream out(&buffer);
		//out << image->data();
		buffer.close();
		createImageHash->addData(byteArray);
#if useDebug
		qDebug() << "Bytearray Size: " << byteArray.size();
		qDebug() << "Hash Value:" << createImageHash->result().toHex().constData();
#endif
		//QIEXPECT_FAIL("GE DCEMRI 015 Breast MR", "Displaying upside down", Continue);
		QICOMPARE2(QString(createImageHash->result().toHex().constData()), md5Sum, "Verify MD5 hash of image data");
	}

}
