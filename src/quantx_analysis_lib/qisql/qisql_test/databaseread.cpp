#include "qisqltest.h"

#include "readwriteappdata.h"

#include "sqldatabaseservice.h"
#include "patientmodel.h"

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

namespace {
    constexpr auto currentDatabaseVersionID = 13;
}

using namespace quantx::sql;
using namespace quantx::patient;

void QISqlTest::databaseRead()
{
    QIWARN("TSQL 2");
    const QSettings::Format XmlFormat = QSettings::registerFormat("xml", QuantXSettingsFile::readXmlSettings, QuantXSettingsFile::writeXmlSettings);
	const QSettings settings{ XmlFormat, QSettings::UserScope, "Quantitative Insights", "QuantXdata" };

	QScopedPointer<SqlDatabaseService, QScopedPointerDeleteLater> dbService{ new SqlDatabaseService{ settings } };
	dbService->databaseSettings_ = this->currentDbSettings_;

	auto error = dbService->initialize( true );
#if useDebug
	qDebug() << "Db Service Initialization Error Message:" << error.msg();
#endif
	QIVERIFY2( error.bSuccess, "Database Service initialized correctly" );
	QICOMPARE2( dbService->databaseVersionId, currentDatabaseVersionID, "Check to see if the database is the current version" );

	auto shallowPatientsModel = dbService->findAllPatientsShallow().get();

#if useDebug
	qDebug() << "Patient cols,rows" << shallowPatientsModel->columnCount() << shallowPatientsModel->rowCount();
#endif
	QICOMPARE2( shallowPatientsModel->columnCount(), 5, "Test number of columns in patient model" );
	QICOMPARE2( shallowPatientsModel->rowCount(), 3, "Test number of rows in patient model" );



//	QIVERIFY2(tableList.contains(qisql->imageTable(QUANTX::MRI)), "Test whether database contains MR image table");
//	QIVERIFY2(tableList.contains(qisql->imageTable(QUANTX::MAMMO)), "Test whether database contains Mammo image table");
//	QIVERIFY2(tableList.contains(qisql->imageTable(QUANTX::US)), "Test whether database contains Ultrasound image table");
//	QIVERIFY2(tableList.contains(qisql->imageTable(QUANTX::NOIMAGE)), "Test whether database contains Unknown Type image table");
//	QIVERIFY2(tableList.contains(qisql->seriesTable()), "Test whether database contains Series table");
//	QIVERIFY2(tableList.contains(qisql->studiesTable()), "Test whether database contains Studies table");
//	QIVERIFY2(tableList.contains(qisql->patientsTable()), "Test whether database contains Patients table");
//	QIVERIFY2(tableList.contains(qisql->dicomTable()), "Test whether database contains DICOM header table");
//	QIVERIFY2(tableList.contains(qisql->thumbnailTable()), "Test whether database contains Thumbnail table");
//	QIVERIFY2(viewList.contains(qisql->seriesView()), "Test whether database contains Series view");
//	QIVERIFY2(viewList.contains(qisql->imageLocationsView()), "Test whether database contains Image Locations view");

}
