#ifndef QISQLTEST_H
#define QISQLTEST_H

#include <QtTest/QtTest>
#include <QObject>
#include "qitestcase.h"
#include "databasesettings.h"

#define POSTGRES_DB 0
#define SQLITE_DB 1

#define DB_DRIVER POSTGRES_DB

class QTableWidget;

class QISqlTest : public QObject
{
    Q_OBJECT

private:
	DatabaseSettings currentDbSettings_;

	void clearAuditLogs( const QStringList& dbSettings );
	void clearUserAccessEntries();

	void initializeCurrentDbSettings();

private slots:
	void initTestCase();
    void databaseRead();
    void testNewDatabase();
	void loadSeriesFromDB_data();
	void loadSeriesFromDB();
	void auditLogTest_data();
	void auditLogTest();
	void userAccessTest_data();
	void userAccessTest();
	void cleanupTestCase() { QIWARN("TSQL 8"); clearUserAccessEntries(); QIVERIFY2(true, "Test Cleanup"); }

};

#endif // QISQLTEST_H
