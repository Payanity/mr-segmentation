#include "qisqltest.h"
#include "qixmltestlogger.h"
#include "qitestcase.h"
#include "quantx.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setAttribute(Qt::AA_Use96Dpi, true);
    QTEST_DISABLE_KEYPAD_NAVIGATION
    QISqlTest tc;
    return QITest::qiExec(&tc, argc, argv, "sql_test/qi_sql_testoutput.xml");
}

//#include "moc_qisqltest.cpp"
