#ifndef TESTSQLMAINWINDOW_H
#define TESTSQLMAINWINDOW_H

#include <qisql_mainwindow.h>

class TestSqlMainWindow : public QISqlMainWindow
{
    Q_OBJECT

public:
	TestSqlMainWindow() = default;
    void openFiles(QStringList &fn,const QStringList &db){fileLoop(&fn,db);}
};

#endif // TESTSQLMAINWINDOW_H
