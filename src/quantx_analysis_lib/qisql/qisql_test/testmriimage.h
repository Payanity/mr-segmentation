#ifndef TESTMRIIMAGE_H
#define TESTMRIIMAGE_H

#include "qimriimage.h"
class Plm_image;

class TestMriImage : public QiMriImage
{
public:
	explicit TestMriImage() = default;
	void createSimulatedSphereMRData(int xDim, int yDim, int zDim, int tDim, float inPlaneResolution, float sliceSpacing, QVector3D center, float radiusMM, const QString& description, QUANTX::Orientation acquisitionOrientation);
	void createBlankData();
	void readMRTestData(const QString&);
	void setValue(int x, int y, int z, int t, quint16 value);
	QVector<QImage> getSlices(int startSlice, int endSlice, int timePt);
	QVector<QImage> getSlices(int timePt);
	QImage getSlice(int slice, int timePt);
	QVector<Plm_image *> createPlastimatchImages_() { return createPlastimatchImages(nullptr); }
	void setWarpedData_(Plm_image *image, int t){ setWarpedData(image, t); }

	void setWindowWidth(const double &w){windowWidth_=w;}
	void setWindowLevel(const double &l){windowLevel_=l;}
	void setMaxPixval(const quint16 &m){maxPixval_=m;}
	void setMinPixval(quint16 &m){minPixval_=m;}
	void setDx(const float &d){dx_=d;}
	void setDy(const float &d){dy_=d;}
	void setDz(const float &d){dz_=d;}
	void setTimestamps(const QVector<QTime> &t){timestamps_=t;}
	void setSeriesNums(const QStringList &s){seriesNums_=s;}
	void setNx(const int &n){nx_=n;}
	void setNy(const int &n){ny_=n;}
	void setNz(const int &n){nz_=n;}
	void setOrientation(const QVector<float> &o){orientation_=o;}
	void setAcquisitionOrientation(const QUANTX::Orientation &o){acquisitionOrientation_=o;}
	void setOrigin(const QVector3D &o){origin_=o;}
	void setPositions(const QVector<QVector3D> &p){positions_=p;}
	void setScanOptions(const QStringList &s){scanOptions_=s;}
	void setModality(const QUANTX::Modality &m){modality_=m;}
	void setMRN(const QString &m){mrn_=m;}

	QVector<int> dimVector() { QVector<int> d; return d << nx_ << ny_ << nz_ << nt_; }

};

#endif // TESTMRIIMAGE_H
