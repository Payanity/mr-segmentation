#-------------------------------------------------
#
# Project created by QtCreator 2012-07-20T15:17:47
#
#-------------------------------------------------

MRIV_ROOT = ../../..
QT       += gui sql core testlib
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets concurrent

TARGET = qisql_test

DEFINES += NO_QUANTXDATA_DLL

MOC_DIR           = moc
OBJECTS_DIR       = obj
DESTDIR           = $${MRIV_ROOT}/bin

include( $${MRIV_ROOT}/quantxconfig.pri )
include( $${MRIV_ROOT}/src/directoryconfig.pri )

macx {
    LIBS += -framework IOKit \
            -framework CoreFoundation
}

QI_TESTLIB_PATH = $${MRIV_ROOT}/src/global/testlib

INCLUDEPATH      += . \
                    $${GDCM_PATH}/include/gdcm-$${GDCM_VERSION} \
                    $${MRIV_ROOT}/src/qisql/qisql_lib \
                    $${MRIV_ROOT}/include/simplecrypt \
                    $${MRIV_ROOT}/src/quantxdata/quantxdata_lib \
                    $${QI_TESTLIB_PATH} \
                    $${WT_PATH}/include


CONFIG(debug, debug|release) {
    #TARGET = $$join(TARGET,,,_debug)
    win32 {
        LIBS += -L$${GDCM_PATH}/lib -lgdcmCommon \
            -L$${GDCM_PATH}/lib -lgdcmDICT \
            -L$${GDCM_PATH}/lib -lgdcmDSED \
            -L$${GDCM_PATH}/lib -lgdcmMSFF \
            -L$${MRIV_ROOT}/lib -lquantxData_debug0 \
            -L$${MRIV_ROOT}/lib -lqisql_debug \
            -L$${WT_PATH}/lib -lwtdbod -lwtdbosqlite3d -lwtdbopostgresd
    }
    else {
        LIBS += -L$${GDCM_PATH}/lib -lgdcmCommon \
            -L$${GDCM_PATH}/lib -lgdcmDICT \
            -L$${GDCM_PATH}/lib -lgdcmDSED \
            -L$${GDCM_PATH}/lib -lgdcmMSFF \
            -L$${MRIV_ROOT}/lib -lqisql_debug
    }
}

CONFIG(release, debug|release) {
    LIBS += -L$${GDCM_PATH}/lib -lgdcmCommon \
            -L$${GDCM_PATH}/lib -lgdcmDICT \
            -L$${GDCM_PATH}/lib -lgdcmDSED \
            -L$${GDCM_PATH}/lib -lgdcmMSFF \
            -L$${MRIV_ROOT}/lib -lquantxData0 \
            -L$${MRIV_ROOT}/lib -lqisql \
            -L$${WT_PATH}/lib -lwtdbo -lwtdbosqlite3 -lwtdbopostgres
}

SOURCES += \
    qisqltest.cpp \
    testSQLSearchWindowFilters.cpp \
    testmriimage.cpp \
    ../../quantxdata/quantxdata_lib/readwriteappdata.cpp \
    testnewdatabase.cpp \
    databaseread.cpp \
    loadseriesfromdb.cpp \
    auditlogtest.cpp \
    useraccesstest.cpp

HEADERS += \
    qisqltest.h \
    testmriimage.h \
    testsqlmainwindow.h

#Global testlib sources

SOURCES += \
    $${QI_TESTLIB_PATH}/qitestresult.cpp \
    $${QI_TESTLIB_PATH}/qitestcase.cpp \
    $${QI_TESTLIB_PATH}/qixmltestlogger.cpp \
    $${QI_TESTLIB_PATH}/qitestlog.cpp \
    $${QI_TESTLIB_PATH}/private/qabstracttestlogger.cpp \
    $${QI_TESTLIB_PATH}/private/qbenchmarkmetric.cpp \
    $${MRIV_ROOT}/include/simplecrypt/simplecrypt.cpp
