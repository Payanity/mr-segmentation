#include "qisqltest.h"

#include "auditlogdatabase.h"
#include "model/auditlog.h"
#include "quantxdata.h"
#include "testmriimage.h"
#include "qipatientinventory.h"

#define useDebug 0

#if useDebug
#include <QDebug>
#endif

void QISqlTest::auditLogTest_data()
{
	QIWARN("TSQL 6");

#if useDebug
	qDebug() << "Loading Images";
#endif
	auto dbSettings = this->currentDbSettings_.getDBSettingsList();

	clearAuditLogs( dbSettings );

	// Create QuantXdata
	QScopedPointer<QuantXData> qidata(new QuantXData{});
	qidata->patientinventory = new QiPatientInventory();
	qidata->patientinventory->setDBSettings(dbSettings);

	// Create test images
	QScopedPointer<TestMriImage> image(new TestMriImage());
	image->readMRTestData("sql_test/00050-Motion Corr. dyn_eTHRIVE_0-703_0-703_121-130.dat");
	qidata->openNewFileWithDBWindow(image.take());

	QTest::addColumn<QString>("date_time");
	QTest::addColumn<QString>("username");
	QTest::addColumn<QString>("expected_action");
	QTest::addColumn<QString>("expected_mrn");
	QTest::addColumn<QString>("actual_action");
	QTest::addColumn<QString>("actual_mrn");

	QStringList dateTimes;
	QStringList userNames;
	QStringList actualActions;
	QStringList actualMrns;

	// Grab Database and corresponding values
	bool bSuccess;
	auto auditLogDB = std::make_unique<QuantXAuditLogDatabase>(dbSettings, &bSuccess);
	if(!bSuccess)
	{
		QSKIP("Unable to connect to database");
	}
	Wt::Dbo::Transaction auditLogDBTransaction(auditLogDB->session());
	Wt::Dbo::collection<Wt::Dbo::ptr<quantx::auditlog::AuditLog> > auditLogs = auditLogDBTransaction.session().find<quantx::auditlog::AuditLog>();
	QICOMPARE2(static_cast<int>(auditLogs.size()), 2, "Testing Audit Log DB Size");

	for(auto Log : auditLogs)
	{
		dateTimes.append(QString(Log->DateAndTime.c_str()));
		userNames.append(QString(Log->UserName.c_str()));
		actualActions.append(QString(Log->ActionDesc.c_str()));
		actualMrns.append(QString(Log->MRN.c_str()).remove(" "));
	}

	QTest::addRow("OpenApp") << dateTimes[0]
	                         << userNames[0]
	                         << "Open QuantX Application"
	                         << ""
	                         << actualActions[0]
	                         << actualMrns[0];

	QTest::addRow("New Patient-50") << dateTimes[1]
	                                << userNames[1]
	                                << "Open New Patient"
	                                << "00050"
	                                << actualActions[1]
	                                << actualMrns[1];

}

void QISqlTest::auditLogTest()
{
	QFETCH(QString, date_time); // QIFETCH calls auditLogTest for each row of data
	QFETCH(QString, username);
	QFETCH(QString, expected_action);
	QFETCH(QString, expected_mrn);
	QFETCH(QString, actual_action);
	QFETCH(QString, actual_mrn);

#if useDebug
	qDebug() << "Action: Expected-" << expected_action << " Actual-" << actual_action;
	qDebug() << "MRN: Expected-" << expected_mrn << " Actual-" << actual_mrn;
#endif
	QIVERIFY2(!date_time.isEmpty(), QString("Testing Date and Time Field is filled: %1").arg(date_time).toStdString().c_str());
	QIVERIFY2(!username.isEmpty(), QString("Testing UserName Field is filled: %1").arg(username).toStdString().c_str());
	QICOMPARE2(actual_action.toStdString().c_str(), expected_action.toStdString().c_str(), "Testing Action Description");
	QICOMPARE2(actual_mrn.toStdString().c_str(), expected_mrn.toStdString().c_str(), "Testing MRN");
}

void QISqlTest::clearAuditLogs( const QStringList& dbSettings )
{
	bool bSuccess;
	auto auditLogDB = std::make_unique<QuantXAuditLogDatabase>(dbSettings, &bSuccess);
	if(bSuccess)
	{
		Wt::Dbo::Transaction auditLogDBTransaction(auditLogDB->session());
		Wt::Dbo::collection<Wt::Dbo::ptr<quantx::auditlog::AuditLog> > auditLogs = auditLogDBTransaction.session().find<quantx::auditlog::AuditLog>();
		for(auto audit : auditLogs)
		{
			audit.remove();
		}

		auditLogDBTransaction.commit();
	}
}
