#ifndef WRITETESTMRIIMAGE_H
#define WRITETESTMRIIMAGE_H

#include "qimriimage.h"
#include <QFile>
#include <QDataStream>
#include <QBuffer>
#include <QVector>
#include <QPoint>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

void writeTestMRData(QiMriImage* image, QVector<QPoint> boxBounds);

inline void writeTestMRData(QiMriImage* image, int startslice, int endslice){
    QVector<QPoint> boxBounds;
    boxBounds << QPoint(0,image->nx()-1);
    boxBounds << QPoint(0,image->ny()-1);
    boxBounds << QPoint(startslice,endslice);
    writeTestMRData(image,boxBounds);
}

inline void writeTestMRData(QiMriImage* image, QVector<QPoint> boxBounds){
    QFile datafile;
    datafile.setFileName("sql_test/" + image->mrn().trimmed()+QString("-")+image->seriesDesc().trimmed()+QString("_")+
                         QString::number(boxBounds[0].x())+QString("-")+QString::number(boxBounds[0].y())+QString("_")+
                         QString::number(boxBounds[1].x())+QString("-")+QString::number(boxBounds[1].y())+QString("_")+
                         QString::number(boxBounds[2].x())+QString("-")+QString::number(boxBounds[2].y())+QString(".dat"));
    datafile.open(QIODevice::WriteOnly);
#if useDebug
    qDebug()<<"Output Filename:"<<datafile.fileName();
#endif
    QDataStream out(&datafile);
    out << static_cast<quint16>(boxBounds[0].y()-boxBounds[0].x()+1);
    out << static_cast<quint16>(boxBounds[1].y()-boxBounds[1].x()+1);
    out << static_cast<quint16>(boxBounds[2].y()-boxBounds[2].x()+1);//number of slices written out
    out << image->nt();
    out << image->dx();
    out << image->dy();
    out << image->dz();
    out << image->minPixval();
    out << image->maxPixval();
    out << image->windowWidth();
    out << image->windowLevel();
    out << image->seriesUID();
    out << image->scanOptions();
    out << image->mrn();
    out << image->seriesDesc();
    out << image->positions();
    out << image->getNb();
    out << image->getTimestamps();
    out << static_cast<quint8>(image->acquisitionOrientation());
#if useDebug
    int endslice = boxBounds[2].y();
    int startslice = boxBounds[2].x();
    qDebug()<<"Image Dims (X,Y,Z,T)"<<image->nx()<<image->ny()<<(endslice-startslice+1)<<image->nt();
    qDebug()<<"Image spacing (Dx,Dy,Dz)"<<image->dx()<<image->dy()<<image->dz();
    qDebug()<<"Image Min,Max"<<image->minPixval()<<image->maxPixval();
    qDebug()<<"Window Width,Level"<<image->windowWidth()<<image->windowLevel();
    qDebug()<<"SeriesUID:"<<image->seriesUID();
    qDebug()<<"Scan Options, MRN, series desc:"<<image->scanOptions()<<image->mrn()<<image->seriesDesc();
#endif
    for (int t=0;t<image->nt();t++)
        for (int z=boxBounds[2].x();z<=boxBounds[2].y();z++){
            for (int y=boxBounds[1].x();y<=boxBounds[1].y();y++){
				std::vector<quint16> row(image->data()[t][z][y].begin()+ boxBounds[0].x(), image->data()[t][z][y].begin() + boxBounds[0].y() + 1);
				out << QVector<quint16>::fromStdVector(row);
            }
        }
    datafile.close();
}

#ifdef useDebug
#undef useDebug
#endif

#endif // WRITETESTMRIIMAGE_H
