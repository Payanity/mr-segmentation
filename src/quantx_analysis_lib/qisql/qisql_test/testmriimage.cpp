#include "testmriimage.h"
#include <QImage>
//#include <QtMath>
#include <cmath>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif
#include <QFile>

void TestMriImage::createSimulatedSphereMRData(int xDim, int yDim, int zDim, int tDim, float inPlaneRes, float sliceSpacing, QVector3D center, float radiusMM, const QString& description, QUANTX::Orientation acquisitionOrientation)
{
    nx_ = xDim;
    ny_ = yDim;
    nz_ = zDim;
    nt_ = tDim;
    dx_ = inPlaneRes;
    dy_ = inPlaneRes;
    dz_ = sliceSpacing;
    seriesDesc_ = description;
    mrn_ = description;
    acquisitionOrientation_ = acquisitionOrientation;
    scanOptions_ << "FS";
    QVector<double> timePtScaleFactor;
    timePtScaleFactor << 0.2 << 0.4 << 0.8 << 0.85 << 1.0 << 0.85 << 0.80;
    for(int i = 0; i < tDim; ++i)
        timestamps_ << QTime(0,i);
    injectionTime_ = QTime(0,0);
    createBlankData();
    auto nLesionVoxels = 0;
    qsrand(37);
    for (auto t = 0; t < nt_; t++)
        for (auto z = 0; z < nz_; z++)
            for(auto y = 0; y < ny_; y++)
                for(auto x = 0; x < nx_; x++){
                    if (sqrtf( (x - center.x()) * (x - center.x()) * dx_ * dx_ +
                               (y - center.y()) * (y - center.y()) * dy_ * dy_ +
                               (z - center.z()) * (z - center.z()) * dz_ * dz_ ) < radiusMM){
                        setValue(x, y, z, t, static_cast<quint16>((timePtScaleFactor[t] * 200) + (qrand() % 11) - 5)); // add some variation to the values (+/- 5)
                        if (t==0)
                            nLesionVoxels++;
                    }else{
                        setValue(x, y, z, t, static_cast<quint16>((timePtScaleFactor[t] * 40) + (qrand() % 11) - 5));
                    }
    }
#if useDebug
    qDebug() << "Dims:" << nx() << ny() << nz() << nt();
    qDebug() << "Number of Voxels in lesion:" << nLesionVoxels;
#endif
}


void TestMriImage::readMRTestData(const QString& filename)
{
    //not complete yet
    QFile file;
    file.setFileName(filename);
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);
    quint16 nx16,ny16,nz16;
    quint8 acquisitionOrientation8;
    in >> nx16 >> ny16 >> nz16;
    nx_ = nx16; ny_ = ny16; nz_ = nz16;
    in >> nt_;
    in >> dx_ >> dy_ >> dz_;
    in >> minPixval_ >> maxPixval_;
    in >> windowWidth_ >> windowLevel_;
    in >> seriesUID_ >> scanOptions_ >> mrn_ >> seriesDesc_;
    in >> positions_;
    in >> nb_;
#if useDebug && false
	quint8 byteWatcher1, byteWatcher2, byteWatcher3, byteWatcher4;
	for(auto i = 0; i < 16; ++i){
		in >> byteWatcher1;
		in >> byteWatcher2;
		in >> byteWatcher3;
		in >> byteWatcher4;
		qDebug() << QString::number(byteWatcher1, 16) << QString::number(byteWatcher2, 16) << QString::number(byteWatcher3, 16) << QString::number(byteWatcher4, 16);
	}
#endif
	in >> timestamps_;
	in >> acquisitionOrientation8;
	acquisitionOrientation_ = static_cast<QUANTX::Orientation>(acquisitionOrientation8);
    if (nt_ > 1){
        injectionTime_ = timestamps_[1].addSecs(-60);
    }
#if useDebug
    qDebug() << "MR parameters: XxYxZxT dims:" << nx() << ny() << nz() << nt();
    qDebug() << "MR parameters: Dx,Dy,Dz:" << dx() << dy() << dz();
    qDebug() << "MR min,max:" << minPixval() << maxPixval();
    qDebug() << "MR window Width,Level:" << windowWidth() << windowLevel();
    qDebug() << "seriesUID:" << seriesUID();
    qDebug() << "scanOption,MRN,seriesDesc:" << scanOptions() << mrn() << seriesDesc();
    qDebug() << "timestamps" << getTimestamps();
    //qDebug() << "positions" << positions;
#endif

    createBlankData();

    for (int t = 0; t < nt_; t++)
        for (int z = 0; z < nz_; z++){
            for (int y = 0; y < ny_; y++){
				QVector<quint16> row;
				in >> row;
                data_[t][z][y] = row.toStdVector();
            }
        }

    file.close();
}


void TestMriImage::createBlankData()
{
    data_.clear();
	data_.resize(nt_);
    for(auto t = 0; t < nt_; t++){
        data_.at(t) = (std::vector<std::vector<std::vector<quint16> > >(nz_));
        for(auto z = 0; z < nz_; z++){
            data_[t].at(z) = (std::vector<std::vector<quint16> >(ny_));
            for(auto y = 0; y < ny_; y++){
                data_[t][z].at(y) = (std::vector<quint16>(nx_));
            }
        }
    }
}

void TestMriImage::setValue(int x, int y, int z, int t, quint16 value)
{
    data_[t][z][y][x] = value;
}

QVector<QImage> TestMriImage::getSlices(int startSlice, int endSlice, int timePt)
{
    if (startSlice < 0)
        startSlice = 0;
    if (endSlice < 0)
        endSlice = 0;
    if (endSlice > (nz_ - 1))
        endSlice = nz_ - 1;
    if (startSlice > endSlice)
        startSlice = endSlice;
	QVector<QImage> sliceImages(endSlice - startSlice + 1);
    for (auto slice = startSlice; slice <= endSlice; slice++)
        sliceImages[slice - startSlice] = getSlice(slice, timePt);
    return sliceImages;

}

QVector<QImage> TestMriImage::getSlices(int timePt)
{
    return getSlices(0, nz_ - 1, timePt);
}

QImage TestMriImage::getSlice(int slice, int timePt)
{
    QImage image(nx_,ny_,QImage::Format_ARGB32);
    image.fill(Qt::black);
    image.setText("description",seriesDesc());
    image.setText("slice",QString("%1").arg(slice,3,10,QChar('0')));
    QVector<QVector<double> > imageData = QVector<QVector<double> >(ny_);
    double imageMin,imageMax;
    double dataValue;
    for (int i = 0; i < ny_; i++)
        imageData[i] = QVector<double>(nx_);
    if (timePt < nt_){//
        image.setText("time", "T" + QString::number(timePt));
#if useDebug
        qDebug()<<"Slice:"<<slice<<" Using timepoint"<<timePt;
#endif
        imageMin = data_[timePt][slice][0][0];
        imageMax = imageMin;
        for (int y = 0; y < ny_; y++){
            for(int x = 0; x < nx_; x++){
                dataValue = data_[timePt][slice][y][x];
                imageData[y][x] = dataValue;
                if (dataValue < imageMin)
                    imageMin = dataValue;
                if (dataValue > imageMax)
                    imageMax = dataValue;
            }
        }
    } else {// create subtraction image
        if (timePt > 2 * nt_ - 2)
            timePt = 2 * nt_ - 2;
        int subTimePt = timePt - (nt_ -1);
        image.setText("time", "Sub" + QString::number(subTimePt));
#if useDebug
        qDebug() << "Slice:" << slice << " Using timepoint" << subTimePt << " (subtraction)";
#endif
        imageMin = data_[subTimePt][slice][0][0] - data_[0][slice][0][0];
        imageMax = imageMin;
        for (int y = 0 ; y < ny_; y++){
            for(int x = 0; x < nx_; x++){
                dataValue = data_[subTimePt][slice][y][x] - data_[0][slice][y][x];
                imageData[y][x] = dataValue;
                if (dataValue < imageMin)
                    imageMin = dataValue;
                if (dataValue > imageMax)
                    imageMax = dataValue;
            }
        }
    }
    double scaleFactor = 255. / (imageMax-imageMin);
    if (qFuzzyCompare(imageMax, imageMin))
        scaleFactor = 0;
#if useDebug
    qDebug() << "Image min/max:" << imageMin << imageMax;
    qDebug() << "Scale factor:" << scaleFactor;
#endif
    for (int y = 0; y < ny_; y++){
        for(int x = 0; x < nx_; x++){
			auto pixelValue = static_cast<int>((imageData[y][x] - imageMin) * scaleFactor); // always floor the values (user can adjust window/level to compensate)
            if (pixelValue < 0)
                pixelValue = 0;
            if (pixelValue > 255)
                pixelValue = 255;
            image.setPixel(x, y, qRgb(pixelValue, pixelValue, pixelValue));
        }
    }
    return image;
}

