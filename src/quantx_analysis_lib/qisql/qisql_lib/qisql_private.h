#ifndef QISQL_PRIVATE_H
#define QISQL_PRIVATE_H

// Private header for QiSql library. Do not include except in qisql files

//!
//! \brief The SqlObjectType enum
//! \details Private enum used by getDbRowId to specify which object type is being queried
enum class QiDbObjectType {
	study,
	series,
	image
};

#endif // QISQL_PRIVATE_H
