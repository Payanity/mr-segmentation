#ifndef QIMRIIMAGE_H
#define QIMRIIMAGE_H

#include <utility>
#include <set>

#include "qi3dimage.h"

/*!
 * \class QiMriImage
 * \ingroup SQLModule
 * \brief Class for storing, retrieving, and interacting with MRI images.
 */
class QISQL_EXPORT QiMriImage : public Qi3DImage
{
	Q_OBJECT
public:
	explicit QiMriImage() { modality_ = QUANTX::MRI; precontrastIdx_ = -1; firstPostIdxFixed = -1; isFastDynamic_ = false; } //!< Default constructor. Sets modality to QUANTX::MRI, inits other members accordingly.
	int openSeries(QString uid, const QStringList &dbSettings, const QStringList &dynMultiSeriesDesc = QStringList(), const QStringList &fastDynamicSeriesDesc = QStringList());
	void updateDBWithWarpedData(const QStringList &dbSettings, QTextEdit * textEdit = nullptr , bool replace = false);
	virtual QVector<QTime> getTimestamps() const;
	virtual QMap<double, QString> getTimeAndSeriesNums() const;
	virtual QVector<double> timePointsMinutes() const;
	virtual QVector<QTime> getAllTimestamps() const;
	virtual QTime injectionTime() const {return injectionTime_; } //!< Getter for the contrast agent injection time.
	virtual int precontrastIdx()const ;
	virtual int firstPostIdx()const ;
	virtual int getIdx( int index ) const;
	int nt() const override;
	virtual int getNb()const ;
	virtual QVector<double> getBValues() const;
	virtual float magneticFieldStrength() const { return magneticFieldStrength_; }

	virtual void setNt(int t); //This function is for experimental purposes only

	// Be careful using these
	virtual void setSeriesDesc(QString d){seriesDesc_ = std::move(d);} //!< Setter for the series description.
	virtual void setSeriesUID(QString u){seriesUID_ = std::move(u);} //!< Setter for the series UID.
	virtual void setPrecontrastIdx( int index){precontrastIdx_ = index; } //!< Setter for the pre-contrast index.
	virtual void setInjectionTime(QTime t){injectionTime_ = t; } //!< Setter for the injection time.
	void setInjectionTimeFromInjectionToFirstPost( const float injectionToFirstPostTime );
	virtual void setFirstPostcontrastIdx( int index ){ firstPostIdxFixed = index; } //!< Setter for the first post-contrast index fixed value.
	QVector3D findOrigin();

protected:
	QVector<QTime> timestamps_; //!< Stores the collection of timestamps.
	QStringList seriesNums_; //!< \internal This holds the list of Dicom series numbers for this MR
	QTime injectionTime_; //!< Stores the injection time.
	int precontrastIdx_; //!< Stores the preconotrast index.
	int firstPostIdxFixed; //!< Stores the first post-contrast fixed index value.
	int nb_{}; //!< Stores the number of b-values.
	QVector<double> bValues_; //!< Stores the collection of b-values.
	QStringList associatedSeries_; //!< Stores the associated series list.
	float magneticFieldStrength_{}; //!< Stores the magnetic field strength used.
	bool isFastDynamic_;
	std::set<int> instanceNumsIndicesToRemove;

private:
	int getSeriesData(const QStringList &dbSettings, const QStringList &dynMultiSeriesDesc, const QStringList &fastDynamicSeriesDesc);
	int getGESeriesData(const QStringList &dbSettings, const QStringList &dynMultiSeriesDesc);
	bool hasDuplicateTimestamps();
	bool correctDuplicateTimestamps(const QStringList &dbSettings);

Q_SIGNALS:
	void startGetSeriesData(); //!< \brief Emitted at start of getSeriesData() \note Used for internal timing output.
	void determinedMRSize(); //!< \brief Emitted upon determination of MR size. \note Used for internal timing output.
	void builtMRBuffer(); //!< \brief Emitted once the MR buffer is built. \note Used for internal timing output.
	void finishedMRImageQuery(); //!< \brief Emitted once the MR image query has finished. \note Used for internal timing output.
	void finishedMRFillBuffer(); //!< \brief Emitted once the MR buffer is filled. \note Used for internal timing output.
};

#endif // QIMRIIMAGE_H
