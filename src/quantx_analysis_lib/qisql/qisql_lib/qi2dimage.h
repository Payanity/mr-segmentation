#ifndef QI2DIMAGE_H
#define QI2DIMAGE_H

#include "qiabstractimage.h"

/*!
 * Class for working storing, manipulating, and otherwise dealing with 2D modality images.
 * \class Qi2DImage
 * \ingroup SQLModule
 */
class QISQL_EXPORT Qi2DImage : public QiAbstractImage
{
public:
	Qi2DImage() = default;
	std::vector<std::vector<quint16> >  data; //!< Stores image data
	int openSeries(QString uid, QUANTX::Modality qmodality, const QStringList &dbSettings, int getImageNum = 0);
	std::vector<quint16> linearDataVector();
	virtual void addToSeriesDesc(const QString &postfix) { seriesDesc_ += postfix; } //!< Appends to the series description.

	virtual int numberOfFrames() const { return numberOfFrames_; } //!< Getter for number of frames in image.
	virtual QString viewPosition() const { return viewPosition_; } //!< Getter for the view position of the image.
	virtual QString imageLaterality() const { return imageLaterality_;  } //!< Getter for image laterality.
	virtual QString patientOrientation() const { return patientOrientation_; } //!< Getter for the orientation of the patient.
	virtual QString photometricInterpretation() const { return photometricInterpretation_; } //!< Getter for the photometric interpretation of the image.

private:
	int getSeriesData(const QStringList &dbSettings, QUANTX::Modality qmodality, int getImageNum = 0);

	int numberOfFrames_{}; //!< Stores the number of frames in the image.
	QString viewPosition_; //!< Stores the view position of the image.
	QString imageLaterality_; //!< Stores the image laterality of the image.
	QString patientOrientation_; //!< Stores the patient orientation.
	QString photometricInterpretation_; //!< Stores the photometric interpretation.

};

#endif // QI2DIMAGE_H
