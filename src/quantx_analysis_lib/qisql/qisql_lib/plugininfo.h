#ifndef PLUGININFO_H
#define PLUGININFO_H

#include <QString>

/*!
 * \brief Simple struct for storing plugin info.
 * \ingroup SQLModule
 */
struct PluginInfo
{
	PluginInfo() = default;
	~PluginInfo() = default;

	QString name{}; //!< Stores the name.
	QString sessionId{}; //!< Stores the session ID.
	QString mrn{}; //!< Stores the MRN.
	QString key{}; //!< Stores the key.
	QByteArray data{}; //!< Stores the binary data associated with this plugin.
};

#endif // PLUGININFO_H
