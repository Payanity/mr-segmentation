#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "qisqlDllCheck.h"
#include "qisql.h"
#include <QDialog>
#include <QList>
#include <QStringList>
#include <QProgressDialog>
/*!
 * \class QISqlMainWindow
 * \ingroup SQLModule
 * \brief Class for interacting with a DB through a UI, currently used mainly in the QuantXGui, in mriwindowmenubar.cpp.
 * \note Any new functionality written to access the database should be done through the new SQL service, and built to fit in an MVC architecture. This class will likely be refactored to reflect this at some point.
 */
class QISQL_EXPORT QISqlMainWindow : public QDialog
{
    Q_OBJECT

public:
    explicit QISqlMainWindow(QWidget *parent = nullptr);

public slots:
    void openFile(const QStringList &dbSettings);
    void openDir(const QStringList &dbSettings, bool recurse=true);
    void openDICOMDIR(const QStringList &dbSettings);
    void openSearchWindow();

signals:
	/*!
	 * \brief Emitted when a new iterator value is encountered for the purposes of computing progress.
	 */
    void iteratorValue(int);

protected:
    void fileLoop(QStringList *fileList, const QStringList &dbSettings);
   // QList<QStringList> makeData();

};

#endif // MAINWINDOW_H
