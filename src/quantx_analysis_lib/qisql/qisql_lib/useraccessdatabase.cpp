#include "useraccessdatabase.h"

#include "databasehelper.h"
#include "model/useraccess.h"
#include <Wt/Dbo/backend/Postgres.h>
#include <Wt/Dbo/backend/Sqlite3.h>
#include "databasesettings.h"

#define useDebug 0

#if useDebug
#include <QDebug>
#endif

class QuantXUserAccessDatabasePrivate
{
public:
	Wt::Dbo::Session session;
	std::unique_ptr<Wt::Dbo::SqlConnection> connection;
	DatabaseHelper::ConnectionType connectionType;
	QStringList dbSettings;

	void initializeSession()
	{
		session.setConnection(connection->clone());
		session.mapClass<UserAccess>("qi_useraccess");
		try {
			session.createTables();
			std::cerr << "Created database" << std::endl;
		} catch (std::exception& e) {
			std::cerr << e.what() << std::endl;
			std::cerr << "Using existing database" << std::endl;
		}
	}

	void setUsernameAndGroupExclusivity()
	{
		//Wt::Dbo::Transaction userAccessTransaction(session);
		//QString ExclusivitySQLStatement("alter ")
	}

	Wt::Dbo::ptr<UserAccess> findItemInDB(const std::string& inFieldName, const std::string& inFieldValue)
	{
		Wt::Dbo::Transaction userAccessTransaction(session);
		try {
			return userAccessTransaction.session().find<UserAccess>()
			        .where(QString("%1 = ?").arg(inFieldName.c_str()).toStdString()).bind(inFieldValue)
			        .resultValue();
		} catch (std::exception e) {
			std::cerr << e.what() << std::endl;
			return Wt::Dbo::ptr<UserAccess>();
		}
	}

	bool doesItemExist(const std::string& inFieldName, const std::string& inFieldValue)
	{
		auto possibleResult = findItemInDB(inFieldName, inFieldValue);
		if(possibleResult.get() != nullptr)
		{
#if useDebug
			qDebug() << inFieldValue.c_str() << "found in" << inFieldName.c_str();
#endif
			return true;
		}
#if useDebug
		    qDebug() << inFieldValue.c_str() << "not found in" << inFieldName.c_str();
#endif
		return false;
	}

	void addNewUserOrGroup(const QString& inName, bool bIsUserName,  const QString& isEnabled, const QString& isAllowed)
	{
		Wt::Dbo::Transaction transaction(session);
		auto item = std::make_unique<UserAccess>();

		if(bIsUserName)
		{
			item->userName = inName.toStdString();
		}
		else
		{
			item->groupName = inName.toStdString();
		}
		item->enabled = isEnabled.toStdString();
		item->allow = isAllowed.toStdString();

		session.add(std::move(item));
		transaction.commit();
	}

	void modifyUserOrGroup(const QString& inName, bool bIsUserName,  const QString& isEnabled, const QString& isAllowed)
	{
		Wt::Dbo::Transaction transaction(session);
		Wt::Dbo::ptr<UserAccess> item;
		if(bIsUserName)
		{
			item = findItemInDB("username", inName.toStdString());
		}
		else
		{
			item = findItemInDB("group_name", inName.toStdString());
		}

		if(item.get() != nullptr)
		{
			item.modify()->enabled = isEnabled.toStdString();
			item.modify()->allow = isAllowed.toStdString();
		}

		transaction.session().flush();
	}

	void removeUserOrGroup(const QString& inName, bool bIsUserName)
	{
		Wt::Dbo::Transaction transaction(session);
		Wt::Dbo::ptr<UserAccess> item;
		if(bIsUserName)
		{
			item = findItemInDB("username", inName.toStdString());
		}
		else
		{
			item = findItemInDB("group_name", inName.toStdString());
		}

		if(item.get() != nullptr)
		{
			item.remove();
		}

		transaction.session().flush();
	}
};

/*!
 * \brief Default constructor.
 * Initializes the private d-pointer implementation, creates default entry if need be, sets up Postgres connection.
 */
QuantXUserAccessDatabase::QuantXUserAccessDatabase()
    :d(std::make_unique<QuantXUserAccessDatabasePrivate>())
{
	d->connection = std::make_unique<Wt::Dbo::backend::Postgres>("host=localhost port=5432 dbname=quantinsights user=postgres password=postgres");
	d->connectionType = DatabaseHelper::ConnectionType::Postgres;
	d->initializeSession();

	createDefaultEntry();
}

/*!
 * \brief Constructor that takes the DB settings list, and pointer to a flag for setting success/fail.
 * \param databaseSettings DB settings list.
 * \param bSuccess Pointer to flag for success/fail reporting.
 */
QuantXUserAccessDatabase::QuantXUserAccessDatabase(const QStringList& databaseSettings, bool* bSuccess)
    :d(std::make_unique<QuantXUserAccessDatabasePrivate>())
{
	bool success = true;
	if(databaseSettings.first() == "QPSQL")
	{
		auto host = databaseSettings.at(1).toStdString();
		std::string port = "5432";
		if (databaseSettings.at(1).contains(":"))
		{
			host = databaseSettings.at(1).split(":").first().toStdString();
			port = databaseSettings.at(1).split(":").at(1).toStdString();
		}
		auto params = "host="     + host +
		             " port="     + port +
		             " dbname="   + databaseSettings.at(2).toStdString() +
		             " user="     + databaseSettings.at(3).toStdString() +
		             " password=" + databaseSettings.at(4).toStdString() ;
		try{
			d->connection = std::make_unique<Wt::Dbo::backend::Postgres>(params);
			d->connectionType = DatabaseHelper::ConnectionType::Postgres;
		} catch (std::exception e) {
			std::cerr << e.what() << std::endl;
#if useDebug
			qDebug() << "Postgres Exception:" << e.what();
#endif

			success = false;
		}
	}
	else // SQLite will be default
	{
		// create Default db for now
		QString databaseFilePath;
		if(!databaseSettings.at(2).isEmpty())
		{
			databaseFilePath = databaseSettings.at(2);
		}
		else
		{
			 databaseFilePath = DatabaseHelper::getTempSQLiteDatabasePath("quantx.db");
		}

		try {
			d->connection = std::make_unique<Wt::Dbo::backend::Sqlite3>(databaseFilePath.toStdString());
			d->connectionType = DatabaseHelper::ConnectionType::SQLite;
		} catch (std::exception e) {
			std::cerr << e.what() << std::endl;
#if useDebug
			qDebug() << "SQLite Exception:" << e.what();
#endif
			success = false;
		}
	}
	if(success)
	{
		d->initializeSession();
		d->dbSettings = databaseSettings;
		createDefaultEntry();
	}

	if(bSuccess)
	{
		*bSuccess = success;
	}
}

/*!
 * \brief Copy constructor.
 * \param other Other user db interface instance.
 */
QuantXUserAccessDatabase::QuantXUserAccessDatabase(const QuantXUserAccessDatabase& other)
    : d(std::make_unique<QuantXUserAccessDatabasePrivate>())
{
	d->connection = other.connection();
	d->initializeSession();

	createDefaultEntry();
}

/*!
 * \brief Defaulted destructor.
 */
QuantXUserAccessDatabase::~QuantXUserAccessDatabase() = default; // Need to default here instead of .h to avoid inlining the destructor

std::unique_ptr<Wt::Dbo::SqlConnection> QuantXUserAccessDatabase::connection() const
{
	return d->connection->clone();
}

/*!
 * \brief Getter for the session.
 * \return Session reference on d-pointer implementation.
 */
Wt::Dbo::Session& QuantXUserAccessDatabase::session() const
{
	return d->session;
}

/*!
 * \brief If the default entry doesn't exist, create it.
 */
void QuantXUserAccessDatabase::createDefaultEntry() const
{
	// Create Default User
	if(!d->doesItemExist("username", QUANTX_DEFAULT_ACCESS_USER))
	{
		addNewUser(QUANTX_DEFAULT_ACCESS_USER, "yes", "yes");
	}
}

/*!
 * \brief Test user ability to access.
 * \param reasonForNotAllow Pointer to string where error reporting can be done.
 * \return True if successful, false otherwise.
 */
bool QuantXUserAccessDatabase::allowCurrentUserAccess(QString * reasonForNotAllow) const
{
	// If no database is set, then allow users to access GUI of Quantx
	DatabaseSettings currentDBSettings(d->dbSettings);
	if(currentDBSettings.databaseDriverType().isEmpty() || currentDBSettings.databaseName().isEmpty())
	{
		return true;
	}
	else
	{
		QString currentUserName = DatabaseHelper::getLoggedInUserName();
		auto user = d->findItemInDB("username", currentUserName.toStdString());
		// Check if username is in DB and enabled and allowed access
		if(user.get() != nullptr)
		{
			if(user->enabled == "yes")
			{
				if(user->allow == "yes")
				{
					return true;
				}
				else
				{
					*reasonForNotAllow = QString("UserName: \"%1\" access is denied.").arg(currentUserName);
					return false;
				}
			}
			else
			{
				*reasonForNotAllow = QString("UserName: \"%1\" is not enabled.").arg(currentUserName);
				return false;
			}
		}
		else
		{
			// Look for if their group is in DB and enabled and allowed access
			auto currentUserGroups = DatabaseHelper::getUserGroups(currentUserName);
			bool bAllow = false;
			QStringList groupsFound;
			for(auto userGroup : currentUserGroups)
			{
				auto group = d->findItemInDB("group_name", userGroup.toStdString());
				if(group.get() != nullptr)
				{
					groupsFound << group->groupName.c_str();
					if(group->enabled == "yes")
					{
						if(group->allow == "yes")
						{
							bAllow = true;
							break;
						}
					}
				}
			}

			// If at least one group they are apart of is found, and enabled and allowed
			if(!groupsFound.isEmpty() && bAllow)
			{
				return true;
			}
			else if(!groupsFound.isEmpty() && !bAllow) // If at least one group they are apart of is found, but not allowed
			{
				*reasonForNotAllow = "Group(s): ";
				for(auto group : groupsFound)
				{
					*reasonForNotAllow += "\"" + group + "\" ";
				}
				*reasonForNotAllow += "access is(are) denied.";
				return false;
			}
		}

		// If we are down here, then this current users username and groups were not found in the DB
		// So we will resort to the default
		auto defaultUser = d->findItemInDB("username", QUANTX_DEFAULT_ACCESS_USER);
		if(defaultUser.get() != nullptr)
		{
			if(defaultUser->enabled == "yes")
			{
				if(defaultUser->allow == "yes")
				{
					return true;
				}
				else
				{
					*reasonForNotAllow = "User and their groups were not found. Default User access is denied.";
					return false;
				}
			}
			else
			{
				*reasonForNotAllow = "User and their groups were not found. Default User access is disabled.";
				return false;
			}
		}
	}


	// If we return here, something when wrong;
	// ie, the default user was not found in the DB
	*reasonForNotAllow = "QuantXUserAccess Error.";
	return false;
}

/*!
 * \brief Add a new user.
 * \param inUserName User name to add.
 * \param isEnabled Whether the user is enabled.
 * \param isAllowed Whether user access is allowed.
 */
void QuantXUserAccessDatabase::addNewUser(const QString& inUserName, const QString& isEnabled, const QString& isAllowed) const
{
	d->addNewUserOrGroup(inUserName, true, isEnabled, isAllowed);
}

/*!
 * \brief Add a new group.
 * \param inGroupName Group name to add.
 * \param isEnabled Whether the group is enabled.
 * \param isAllowed Whether group access is allowed.
 */
void QuantXUserAccessDatabase::addNewGroup(const QString& inGroupName, const QString& isEnabled, const QString& isAllowed) const
{
	d->addNewUserOrGroup(inGroupName, false, isEnabled, isAllowed);
}

/*!
 * \brief Modify an existing user.
 * \param inUserName User name to modify.
 * \param isEnabled Whether the user is enabled.
 * \param isAllowed Whether user access is allowed.
 */
void QuantXUserAccessDatabase::modifyUser(const QString& inUserName, const QString& isEnabled, const QString& isAllowed) const
{
	d->modifyUserOrGroup(inUserName, true, isEnabled, isAllowed);
}

/*!
 * \brief Modify an existing group.
 * \param inUserName Group name to modify.
 * \param isEnabled Whether the group is enabled.
 * \param isAllowed Whether group access is allowed.
 */
void QuantXUserAccessDatabase::modifyGroup(const QString& inUserName, const QString& isEnabled, const QString& isAllowed) const
{
	d->modifyUserOrGroup(inUserName, false, isEnabled, isAllowed);
}

/*!
 * \brief Remove a user.
 * \param inUserName User name to remove.
 */
void QuantXUserAccessDatabase::removeUser(const QString& inUserName) const
{
	d->removeUserOrGroup(inUserName, true);
}

/*!
 * \brief Remove a group.
 * \param inUserName Group name to remove.
 */
void QuantXUserAccessDatabase::removeGroup(const QString& inUserName) const
{
	d->removeUserOrGroup(inUserName, false);
}

/*!
 * \brief Get all UserAccess items.
 * \return Vector of all items.
 */
QVector<UserAccess> QuantXUserAccessDatabase::getAllItems() const
{
	QVector<UserAccess> users;
	Wt::Dbo::Transaction userAccessTransaction(d->session);
	auto items = userAccessTransaction.session().find<UserAccess>().resultList();
	for(auto item : items)
	{
		UserAccess user;

		user.userName = item->userName;
		user.groupName = item->groupName;
		user.enabled = item->enabled;
		user.allow = item->allow;

		users.append(user);
	}

	return users;
}

/*!
 * \brief Wipe all entries from DB.
 */
void QuantXUserAccessDatabase::clearAllEntries()
{
	Wt::Dbo::Transaction userAccessTransaction(d->session);
	auto items = userAccessTransaction.session().find<UserAccess>().resultList();
	for(auto item : items)
	{
		item.remove();
	}

	userAccessTransaction.commit();
}
