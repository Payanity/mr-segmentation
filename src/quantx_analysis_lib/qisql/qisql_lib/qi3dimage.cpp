#include <QTextEdit>
#include <QElapsedTimer>
#include "qi3dimage.h"
#include "motioncorrection_mr.h"
#include "plm_image_header.h"
#include "qisql.h"
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSharedMemory>
#include <QApplication>

#define useDebug 0
#if useDebug
#include <QDebug>
#include <QSqlError>
#endif

#include <QCryptographicHash>


namespace
{

QString mcSharedMemorySalt = "RJL7GCOwqXS19gixCNnHRWfi6CWESDfeEyqPMZLehF3T52FXMSw3zkk9ZTbc";

QString generateKeyHash( const QString& key, const QString& salt )
{
	QByteArray data;

	data.append( key.toUtf8() );
	data.append( salt.toUtf8() );
	data = QCryptographicHash::hash( data, QCryptographicHash::Sha3_224 ).toBase64();

	return data;
}

}

/*!
 * \brief Convert a PointVector3D<int> to QVector3D real world coordinates.
 * \param index The 3D position data to convert.
 * \return The QVector3D in real world coordinates.
 */
QVector3D Qi3DImage::toRealworld(PointVector3D<int> index)
{
	return toRealworld(index.x(), index.y(), index.z());
}

/*!
 * \brief Convert individual dimension 3D machine coordinates to real world ones.
 * \param x The machine x dimension coordinate.
 * \param y The machine y dimension coordinate.
 * \param z The machine z dimension coordinate.
 * \return A QVector3D of real world coordinates.
 * \note This method is bounds-checked on the machine coordinate side.
 */
QVector3D Qi3DImage::toRealworld(int x, int y, int z)
{
	return origin_ + QVector3D(x * dx_, y * dy_, z * dz_);
}

/*!
 * \brief Convert a real world coordinate position to a machine-space index.
 * \param realworldPos The coordinates in real world space.
 * \return The index they were converted to.
 */
PointVector3D<int> Qi3DImage::toIndex(QVector3D realworldPos)
{
	return toIndex(realworldPos.x(), realworldPos.y(), realworldPos.z());
}
/*!
 * \brief Convert a real world coordinate position to a machine-space index.
 * \param xpos The x component of the real world position.
 * \param ypos The y component of the real world position.
 * \param zpos The z component of the real world position.
 * \return The index they were converted to.
 */
PointVector3D<int> Qi3DImage::toIndex(float xpos, float ypos, float zpos)
{
	return PointVector3D<int>(toIndexX(xpos), toIndexY(ypos), toIndexZ(zpos));
}

/*!
 * \brief Convert a real world x position to a machine-space index value.
 * \param x The real world x position.
 * \return The machine-space index value x dimension.
 */
int Qi3DImage::toIndexX(float x)
{
	return qRound( ( x - origin_.x() ) / dx_ );
}

/*!
 * \brief Convert a real world y position to a machine-space index value.
 * \param y The real world y position.
 * \return The machine-space index value y dimension.
 */
int Qi3DImage::toIndexY(float y)
{
	return qRound( ( y - origin_.y() ) / dy_ );
}

/*!
 * \brief Convert a real world z position to a machine-space index value.
 * \param z The real world z position.
 * \return The machine-space index value z dimension.
 */
int Qi3DImage::toIndexZ(float z)
{
	return qRound( ( z - origin_.z() ) / dz_ );
}

/*!
 * \brief Get the number of slices for a given orientation.
 * \param orientation The orientation to get the number of slices for.
 * \return Number of slices.
 */
int Qi3DImage::numSlices(QUANTX::Orientation orientation) const
{
	if(orientation == QUANTX::FULLMIP || orientation == QUANTX::FULLMIP) {
		return 1;
	}
	return imageSize().reoriented(orientation).z();
}

/*!
 * \brief Create plastimatch images from this Qi3DImage.
 * \param textEdit A text editor that we would normally use as part of the naming, but that is currently more or less unused due to commented code.
 * \return A QVector<Plm_image *> representing all volumetrically registered image slices, if we use the nt() assumption of there being only one pre-contrast timepoint.
 */
QVector<Plm_image *> Qi3DImage::createPlastimatchImages(QTextEdit * textEdit)
{
	plm_long dim[3] = {nx_, ny_, nz_};
	float offset[3] = {0, 0, 0};                                    // Note: Offset is set to 0,0,0. If we need to register two different scans in the future (i.e. T1 & T2) then this will need to be fixed
	float spacing[3] = {dx_, dy_, dz_};
	float direction_cosines[9] = {1, 0, 0, 0, 1, 0, 0, 0, 1};       // QuantX always store images in x,y,z format
	Volume_pixel_type vox_type = PT_FLOAT;                          // Image registration requires data stored in float
	int vox_planes = 1;                                             // This option is if we want to hold vecotrs instead of scalars in the volume

	QVector<Plm_image *> plmImages;
#if useDebug
	    qDebug() << "Number of timepoints in motion correction image:" << nt_;
#endif
		for(int t = 0; t < nt_; ++t){
		MotionCorrection_MR::textUpdate(QString("Generating Deformable Registration Volume %1/%2").arg(t + 1).arg(nt_), textEdit);
		auto volume = new Volume(dim, offset, spacing, direction_cosines, vox_type, vox_planes);
		plm_long i = 0;
		auto img = volume->get_raw<float>();
#if useDebug
		//qDebug() << QString("Volume Location: 0x%1").arg( (qulonglong) ( volume->get_raw<float>() ) , QT_POINTER_SIZE * 2, 16, QChar('0') );
#endif
		for(const auto & slice : std::as_const(data().at(t)))
			for(const auto & line : std::as_const(slice))
				for(const auto & voxel : std::as_const(line))
					img[i++] = static_cast<float>(voxel);
		plmImages << new Plm_image(volume); // Plm_image takes ownership of volume
	}
	return plmImages;
}

/*!
 * \brief Allows setting a plastimatch image to populate the current Qi3DImage instance.
 * \param image Plastimatch image pointer to set.
 * \param t Timepoint as following standard protocol assumptionss (e.g. like nt()) to set the warped image data on.
 */
void Qi3DImage::setWarpedData(Plm_image *image, int t)
{
	if(image == nullptr)
		return;
	plm_long i = 0;
	auto img = image->get_vol()->get_raw<float>();
	for(int z = 0; z < nz_; ++z)
		for(int y = 0; y < ny_; ++y)
			for(int x = 0; x < nx_; ++x)
				data_[t][z][y][x] = static_cast<quint16>(qRound(img[i++]));
}

/*!
 * \brief Query the DB for transformed data.
 * \param seriesUID_moving The UID of the series to check for motion-corrected data.
 * \param dbSettings DB settings to use to connect to the db.
 * \return SQL record output of the query.
 */
QISQL_EXPORT QSqlRecord checkForXformData(const QString & seriesUID_moving, const QStringList & dbSettings)
{
	QScopedPointer<QISql> qisql(new QISql(dbSettings)); // Just use this to get settings
	QSqlDatabase db = qisql->createDatabase("QIMotionCorrectionDatabase");
	db.open();

	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
	QString mcTable = "qi_motion_correction";
	QSqlRecord mcRecord  = db.record(mcTable);
	QSqlRecord uidRecord = mcRecord;
	uidRecord.setValue("series_uid_moving", seriesUID_moving);
	for(int i=uidRecord.count()-1;i>=0;i--)
		if(uidRecord.isNull(i))
			uidRecord.remove(i);

	QString sqlStatement;
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,mcTable,mcRecord,false) + QString(' ')
	             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,mcTable,uidRecord,false);
	sqlQuery->exec(sqlStatement);
	sqlQuery->first();
	mcRecord = sqlQuery->record();
	db.close();
	return mcRecord;
}

/*!
 * \brief Query the DB for transformed data.
 * \param mcRecord SQL record to store the data with.
 * \param dbSettings DB settings to use to connect to the db.
 */
QISQL_EXPORT void storeXformData(const QSqlRecord & mcRecord, const QStringList & dbSettings)
{
	QScopedPointer<QISql> qisql(new QISql(dbSettings)); // Just use this to get settings
	QSqlDatabase db = qisql->createDatabase("QIMotionCorrectionDatabase");
	db.open();

	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
	QString sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement, "qi_motion_correction", mcRecord, true);
	sqlQuery->prepare(sqlStatement);
	for(int i = 0; i < mcRecord.count(); ++i)
		sqlQuery->bindValue(i, mcRecord.value(i));
	sqlQuery->exec();
#if useDebug
	qDebug() << "Motion Correction Database Insert Result:" << sqlQuery->lastError().text();
#endif
	db.close();
}

void Qi3DImage::motionCorrectImage(const QStringList& databaseSettings, int fixedIdx, QTextEdit * textEdit)
{
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit startMotionCorrection();
	qApp->processEvents();
#endif
#if useDebug
	QElapsedTimer timer;
	timer.start();
#endif
	const auto imagePointers = createPlastimatchImages(textEdit);
	emit finishedMCCreatePlastimatchImages();
	qApp->processEvents();

	QVector<Plm_image::Pointer> images;
	for(const auto & p : imagePointers)
		images << Plm_image::Pointer(p);

	int t_fixed = fixedIdx;
#if useDebug
	qDebug() << QString("Using index %1 for the fixed image").arg(t_fixed);
#endif

	QSharedMemory sharedMemory(generateKeyHash(seriesUID_, mcSharedMemorySalt));
	// Note: To create QSharedMemory data, you must request at least 1 byte. I'm not sure if we actually need to create the data block in order to lock it though.
	if(!sharedMemory.create(1, QSharedMemory::ReadOnly)){
#if useDebug
		qDebug() << "SharedMemory create error:" << sharedMemory.errorString();
#endif
		if(sharedMemory.attach(QSharedMemory::ReadOnly)){
#if useDebug
			qDebug() << "Attached to SharedMemory with key" << sharedMemory.key();
#endif
			MotionCorrection_MR::textUpdate("Another process is already performing motion correction.\nWaiting for process to finish...", textEdit);
		}
#if useDebug
		else
			qDebug() << "SharedMemory attach error:" << sharedMemory.errorString();
#endif
	}
#if useDebug
	else
		qDebug() << "SharedMemory created with key" << sharedMemory.key();
	bool sharedMemoryCheck =
#endif
	sharedMemory.lock();
#if useDebug
	qDebug() << (sharedMemoryCheck ? "SharedMemory locked" : "SharedMemory lock failed");
#endif

	QSqlRecord mcRecord = checkForXformData(seriesUID_, databaseSettings);
	QSqlDatabase::removeDatabase("QIMotionCorrectionDatabase");
	QVector<Xform::Pointer> xforms;
	Plm_image_header pih;

	if(mcRecord.value("series_uid_moving").toString().trimmed() != seriesUID_.trimmed()){
		// Generate new Registration Data
#if useDebug
		qDebug() << "Generating New Xforms";
#endif
		xforms = MotionCorrection_MR::generateXforms(images, acquisitionOrientation_, t_fixed, textEdit);
#if useDebug
		MotionCorrection_MR::textUpdate(QString("Transform Creation Time: %1 seconds").arg(static_cast<float>(timer.restart() / 10) / 100), textEdit);
#endif
		pih.set_from_plm_image(images[t_fixed]);
		// Store Xform Data in Database
		QVector<QByteArray> xfList;
		for(const auto & xf : xforms)
			xfList << MotionCorrection_MR::serializeXform(xf);
		QByteArray buf;
		QDataStream stream(&buf, QIODevice::WriteOnly);
		stream << xfList;
		mcRecord.setValue("xform", buf);
		mcRecord.setValue("version", 0);
		buf = MotionCorrection_MR::serializeHeader(pih);
		mcRecord.setValue("image_header", buf);
		mcRecord.setValue("series_uid_moving", seriesUID_);
		mcRecord.setValue("series_uid_fixed", seriesUID_);
		mcRecord.setValue("t_fixed", t_fixed);
		mcRecord.setValue("default_value", 0.0);
		mcRecord.remove(mcRecord.indexOf("id"));
		int rowID = QISql::getSeriesDbRowId(databaseSettings, seriesUID_);
		if (rowID >= 0)
			mcRecord.setValue("qi_series_id", rowID);
		storeXformData(mcRecord, databaseSettings);
		QSqlDatabase::removeDatabase("QIMotionCorrectionDatabase");
	}
	else{
		QByteArray buf = mcRecord.value("xform").toByteArray();
		QDataStream stream(buf);
		QVector<QByteArray> xfList;
		stream >> xfList;
		for(const auto & xf : std::as_const(xfList))
			xforms << MotionCorrection_MR::deserializeXform(xf);

#if useDebug
		//for(const auto & xf : xforms)
		//    if( xf.get() != NULL )
		//        qDebug() << QString("Bspline Location list creation: 0x%1").arg( (qulonglong) (xf->get_gpuit_bsp()) , QT_POINTER_SIZE * 2, 16, QChar('0') );
#endif

		buf = mcRecord.value("image_header").toByteArray();
		pih = MotionCorrection_MR::deserializeHeader(buf);
		t_fixed = mcRecord.value("t_fixed").toInt();
	}
	emit finishedMCXForms();
	qApp->processEvents();

#if useDebug
	sharedMemoryCheck =
#endif
	sharedMemory.unlock();
#if useDebug
	qDebug() << (sharedMemoryCheck ? "SharedMemory unlocked" : "SharedMemory unlock failed");
#endif
	sharedMemory.detach();

	QVector<Plm_image_header> headers;
	QVector<float> d;
	for(int i = 0; i < images.size(); ++i){
		headers << pih;
		d << 0.0;
	}

#if useDebug
	//for(const auto & xf : xforms)
	//    if( xf.get() != NULL )
	//        qDebug() << QString("Bspline Location before warp: 0x%1").arg( (qulonglong) (xf->get_gpuit_bsp()) , QT_POINTER_SIZE * 2, 16, QChar('0') );
#endif

	auto im_warped = MotionCorrection_MR::warpImages(images, xforms, headers, d, textEdit);

	emit finishedMCWarping();
	qApp->processEvents();

#if useDebug
	MotionCorrection_MR::textUpdate(QString("Image Warp Time: %1 seconds").arg(static_cast<float>(timer.restart() / 10) / 100), textEdit);
#endif

#if useDebug
	//for(const auto & xf : xforms)
	//    if( xf.get() != NULL )
	//        qDebug() << QString("Bspline Location after warp:  0x%1").arg( (qulonglong) (xf->get_gpuit_bsp()) , QT_POINTER_SIZE * 2, 16, QChar('0') );
	//for( int xfNum = 0; xfNum < xforms.size(); xfNum++)
	//    xforms[xfNum]->clear();
#endif

	MotionCorrection_MR::textUpdate("Getting Ready to Display Corrected DCE MR Image", textEdit);

	for(int i = 0; i < im_warped.size(); ++i)
		setWarpedData(im_warped[i].get(), i);
#if useDebug
	MotionCorrection_MR::textUpdate(QString("Memory Relocation Time: %1 seconds").arg(static_cast<float>(timer.restart() / 10) / 100), textEdit);
#endif
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit finishedMCQuantXImages();
	qApp->processEvents();
#endif
	MotionCorrection_MR::textUpdate(" ", textEdit);
	MotionCorrection_MR::textUpdate("Done!", textEdit);
}
