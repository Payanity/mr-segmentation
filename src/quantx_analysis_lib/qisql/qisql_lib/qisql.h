#ifndef QISQL_H
#define QISQL_H

#include "qisqlDllCheck.h"
#include "gdcmImageReader.h"
#include "../../quantxdata/quantxdata_lib/quantx.h"
#include <QList>
#include <QString>
#include <QMultiHash>
#include <QPoint>
#include <QMap>
#include <QSqlDatabase>
#include <QVariantList>

#define NEWSTEADNAMECHECK 1

/*!
 * \defgroup SQLModule SQL Module
 * This class contains the functions needed for the data and UI modules to
 * communicate with the QuantX database.
 *
 * \class QISql
 * \ingroup SQLModule
 *
 * \brief The QuantX SQL Module
 *
 * This class contains the functions needed for the data and UI modules
 * to communicate with the QuantX database.
 *
 * ## Functional Requirements
 *
 * These functional requirements are based on the SRS and architecture,
 * detailing all primary goals of the module design.
 *
 * Requirement Name | SRS | Description
 * ---------------- | --- | -----------
 * Initialize Software | 3.2.1 | Initialize attributes to default values, present user with default interface
 * Read/Write Application Settings | 3.2.2 | Save and load customizable attributes from disk, trigger a refresh of the UI based on updated attributes
 * User-Initiated File Input Handling | 3.2.3 | Read data from files for insertion into the QuantX Image Database.
 * PACS-Initiated File Input Handling | 3.2.4 | Reads data sent by a PACS for insertion into the QuantX Image Database.
 * Store Image Data in Database | 3.2.5 | Store image data in the QuantX Image Database.
 * Read Image Data from Database | 3.2.6 | Read stored image data from the QuantX Image Database.
 * Display Patient Selection Window | 3.2.7 | Display a table with a list of patients
 * Query Patient List from Database | 3.2.8 | Return a list of available patients and other associated metadata.
 * Open Patient for Viewing | 3.2.9 | After selecting a patient from the list, open the relevant images for display
 * Query Patient Series List from Database | 3.2.10 | The data module communicates with the SQL module to store a list of available series for a given patient.
 * Load Image Data in Memory | 3.2.11 | The data module holds the raw image data from the database via the SQL module
 * Generate Thumbnails from Patient Series List | 3.2.12 | The data module stores the data provided by the SQL module from the patient series list.
 * Update Image Display | 3.2.14 | Make any visual changes to how or what is being displayed in an image cell as requested by the user
 * Process Motion Correction Algorithm | 3.2.51 | The SQL module is responsible for initiating the motion correction algorithm, as well as storing/retrieving the parameters in the database.
 *
 */

namespace quantx {
namespace mranalysis {
class DicomSeriesInfo;
}
}

class QISQL_EXPORT QISql
{
public:
	QISql(const QString & dbDriver = "QSQLITE",const QString & dbHostName = "",const QString & dbDatabaseName = "quantx.db",const QString & dbUserName = "",const QString & dbPassword = "");
	QISql(const QStringList & dbSettings);
	int addFile(const QString &filename, const QMap<gdcm::Tag, QByteArray> &tagmap = (QMap<gdcm::Tag, QByteArray>()) );
	int addItem(gdcm::ImageReader &imagereader, const QMap<gdcm::Tag, QByteArray> &tagmap = (QMap<gdcm::Tag, QByteArray >()) );
	//Dicom pullItem(int itemNumber);
	bool setDatabaseType(QString dbDriver = "QSQLITE");
	bool setHostName(QString dbHostName = QString());
	bool setDatabaseName(QString dbDatabaseName = "quantx.db");
	bool setUserName(QString dbUserName = QString());
	bool setPassword(QString dbPassword = QString());

	QString databaseType();
	QString hostName();
	QString databaseName();
	QString userName();
	QString password();

	QString imageTable(QUANTX::Modality modality = QUANTX::NOIMAGE);
	QString seriesTable();
	QString studiesTable();
	QString patientsTable();
	QString dicomTable();
	QString thumbnailTable();
	QString seriesView();
	QString imageLocationsView();
	QMultiHash<QString,QString> extractAllDicomFields(const QString& imageUID);
	QVector<QVariantList> getDicomValues(const QString& seriesUID, const QStringList &dicomTags);
	static QString getDicomTag(const QString&);
	static QString getDicomValue(QString);
	static int getStudyDbRowId(const QStringList & dbSettings, QString studyUid);
	static int getSeriesDbRowId(const QStringList & dbSettings, QString seriesUid);
	static int getImageDbRowId(const QStringList & dbSettings, QString imageUid);
	static QMultiMap<int, quantx::mranalysis::DicomSeriesInfo> getSeriesInfo(QStringList dbSettings, QString studyUID);

	QSqlDatabase createDatabase(const QString & connectionName);

private:
	QString sqlDatabaseType; //!< This holds the data representing the database driver to use.
	QString sqlHostName; //!< This holds the data representing the database host name to use.
	QString sqlDatabaseName; //!< This holds the data representing the database name to use.
	QString sqlUserName; //!< This holds the data representing the database user to identify as.
	QString sqlPassword; //!< This holds the data representing the database password to use.

	QString sqlImageTable; //!< This holds the name of the fallback image table.
	QString sqlImageMGTable; //!< This holds the name of the MG image table.
	QString sqlImageMRTable; //!< This holds the name of the MR image table.
	QString sqlImageUSTable; //!< This holds the name of the US image table.
	QString sqlSeriesTable; //!< This holds the name of the series table.
	QString sqlStudiesTable; //!< This holds the name of the studies table.
	QString sqlPatientsTable; //!< This holds the name of the patients table.
	QString sqlDicomTable; //!< This holds the name of the DICOM table \deprecated May be removed in future version
	QString sqlThumbnailTable; //!< This holds the name of the series thumbnail table.
	QString sqlSeriesView; //!< This holds the name of the series view. \details Same as the SeriesTable plus column with numimages in series
	QString sqlImageLocationsView; //!< This holds the name of the image locations view.


	QVector<QPoint> tagList; //!< A list of DICOM tags that are relevant to MR image acquisition and display, which will be stored in the DICOM table of the QuantX image database.
	void createTagList();

};

#endif // QISQL_H
