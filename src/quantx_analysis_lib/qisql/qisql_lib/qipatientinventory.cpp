#include "qipatientinventory.h"
#include "qiseriesinfo.h"
#include "qisql.h"
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlField>
#include <QImage>
#include <QBuffer>
#include <QMessageBox>
#include <QMap>
#include <QEventLoop>
#include <QTimer>
#include <QElapsedTimer>
#include <QThread>
#include <QVector>
#include <utility>

#define IPS4BATCH 0
#define useDebug 0
#if useDebug
#include <QSqlError>
#include <QDebug>
#include <QImageWriter>
#define DEBUG_WAITTIME_ 100
#endif

#if NEWSTEADNAMECHECK
QString newsteadNameCheck(QString name);
#endif //NEWSTEADNAMECHECK

/*!
 * \brief Constructor taking some required params to know the patient for whom we're building the inventory.
 * \param patientMRN Patient MRN (medical record number).
 * \param dbSettings DB settings used to connect to DB and load data.
 * \param parent Parent object for cleanup and signal/slots functionality.
 */
QiPatientInventory::QiPatientInventory(QString patientMRN, const QStringList &dbSettings, QObject *parent) :
    QObject(parent)
{
	mrn = std::move(patientMRN);
	databaseSettings = dbSettings;
}

/*!
 * \brief Builds the patient inventory incl. data.
 * \param patientMRN The MRN for the patient to build.
 * \return Number of studies in the patient inventory.
 */
int QiPatientInventory::buildInventory(QString patientMRN){
#if useDebug
	qDebug() << "Begin Building Patient Inventory";
#endif
	mrn = std::move(patientMRN);

	buildPatientInfo();

#if useDebug
	qDebug() << "Begin Building Patient Inventory Studies List";
#endif

	numStudies = buildStudiesList();

#if useDebug
	qDebug() << "Begin Building Patient Inventory Series List";
#endif

	for (const auto& study : studies) {
		series << buildSeriesList(study);
	}
#if useDebug
	qDebug() << "Finish Building Patient Inventory";
#endif
	emit inventoryListFinished();

	//buildThumbnails(thumbSize);

	//emit inventoryFinished();
	return numStudies;
}

/*!
 * \brief Build patient info from DB.
 */
int QiPatientInventory::buildPatientInfo(){
	int returnval = 0;
	{
		QScopedPointer<QISql> qisql(new QISql(databaseSettings));
		QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabase");
		bool ok = db.open();  //if ok is false, we should error and quit
		if (ok){
			QString sqlPatientsTable = qisql->patientsTable();
			QSqlRecord patientRecord = db.record(sqlPatientsTable);

			QSqlRecord thispatientRecord = patientRecord;
			thispatientRecord.setValue("mrn",mrn);
			for(int i = thispatientRecord.count() - 1; i >= 0; i--)
				if(thispatientRecord.isNull(i))
					thispatientRecord.remove(i);

			QString sqlStatement;
			QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));

			// New query to get patient info for this mrn
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlPatientsTable,patientRecord,false) + QString(' ')
			             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlPatientsTable,thispatientRecord,true);
			sqlQuery->prepare(sqlStatement);
			for(int i = 0; i < thispatientRecord.count(); i++)
				sqlQuery->bindValue(i,thispatientRecord.value(i));
			sqlQuery->exec();
			if(sqlQuery->next()){
				patientName = sqlQuery->value(patientRecord.indexOf("patient_name")).toString();
#if NEWSTEADNAMECHECK
				patientName = newsteadNameCheck(patientName);
#endif //NEWSTEADNAMECHECK
				patientDOB =  sqlQuery->value(patientRecord.indexOf("dob")).toDate();
			}
			returnval = 1;
		}
	}
	QSqlDatabase::removeDatabase("QIpatientInventoryDatabase");

	return returnval; //return 1 for success
}

/*!
 * \brief Build list of studies.
 * \return 1 if successful, 0 otherwise.
 */
int QiPatientInventory::buildStudiesList(){
	QScopedPointer<QISql> qisql(new QISql(databaseSettings));
	{
#if useDebug
		qDebug() << "Patient Inventory Studies List Database Creation";
#endif
	QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabaseStudiesList");
	    studies.clear();
		series.clear();
		numSeries.clear();
		numStudies = 0;
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			return 0;
		}

#if useDebug
		qDebug() << "Patient Inventory Studies List Database Open";
#endif

		QString sqlStudiesTable = qisql->studiesTable();

		QSqlRecord studiesRecord  = db.record(sqlStudiesTable);
		QSqlRecord thisMrnStudiesRecord = studiesRecord;
		thisMrnStudiesRecord.setValue("mrn",mrn);
		for(int i = thisMrnStudiesRecord.count() - 1; i >= 0; i--)
			if(thisMrnStudiesRecord.isNull(i))
				thisMrnStudiesRecord.remove(i);

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlStudiesTable,studiesRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlStudiesTable,thisMrnStudiesRecord,true)
		             + QString(" ORDER BY study_date DESC");
		sqlQuery->prepare(sqlStatement);
#if useDebug
		qDebug() << "Patient Inventory Studies List Statement Prepared";
#endif
		for(int i = 0; i < thisMrnStudiesRecord.count(); i++)
			sqlQuery->bindValue(i, thisMrnStudiesRecord.value(i));
		sqlQuery->exec();
		if(!sqlQuery->next())
			return 0;

		    //sqlQuery->first();
		    do{
			    studies << sqlQuery->value(studiesRecord.indexOf("study_uid")).toString();
				accessions << sqlQuery->value(studiesRecord.indexOf("accession_number")).toString();

				QScopedPointer<QSqlQuery> sqlQuery2(new QSqlQuery(db));
				QString sqlStatement2{R"(SELECT * FROM "dicom_data" WHERE "0020,000d" = ? LIMIT 1)"}; // Note that LIMIT works with SQLite and PostgreSQL, but not necessarily other SQL databases
				sqlQuery2->prepare(sqlStatement2);
        #if useDebug
				qDebug() << "Patient Referring Physician List Statement Prepared";
        #endif
				sqlQuery2->bindValue(0, studies.last());
				sqlQuery2->exec();
				sqlQuery2->first();
				referringPhysicians << sqlQuery2->value("0008,0090").toString();
				requestingPhysicians << sqlQuery2->value("0032,1032").toString();

				numStudies++;
		    }while(sqlQuery->next());

#if useDebug
		qDebug() << "Patient Inventory Studies List Statement Executed";
#endif

	}
	QSqlDatabase::removeDatabase("QIpatientInventoryDatabaseStudiesList");

	return studies.count();
}

/*!
 * \brief Updates the studies list.
 * \pre The studies list is built out.
 * \return Whether an update was needed or not.
 */
bool QiPatientInventory::updateStudiesList(){
	QScopedPointer<QISql> qisql(new QISql(databaseSettings));
	bool neededUpdate = false;
	{
#if useDebug
		qDebug() << "Updating Patient Inventory Studies List Database Creation";
#endif
		if (mrn.isEmpty())
			return false;
	QStringList newStudiesList;
	    QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabaseStudiesList");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			return false;
		}

#if useDebug
		qDebug() << "Patient Inventory Studies List Database Open";
#endif

		QString sqlStudiesTable = qisql->studiesTable();

		QSqlRecord studiesRecord  = db.record(sqlStudiesTable);
		QSqlRecord thisMrnStudiesRecord = studiesRecord;
		thisMrnStudiesRecord.setValue("mrn" ,mrn);
		for(int i = thisMrnStudiesRecord.count() - 1; i >= 0; i--)
			if(thisMrnStudiesRecord.isNull(i))
				thisMrnStudiesRecord.remove(i);

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlStudiesTable,studiesRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlStudiesTable,thisMrnStudiesRecord,true)
		             + QString(" ORDER BY study_date DESC");
		sqlQuery->prepare(sqlStatement);
#if useDebug
		qDebug() << "Updating Patient Inventory Studies List Statement Prepared";
#endif
		for(int i = 0; i < thisMrnStudiesRecord.count(); i++)
			sqlQuery->bindValue(i, thisMrnStudiesRecord.value(i));
		sqlQuery->exec();

		while (sqlQuery->next()){
			newStudiesList << sqlQuery->value(studiesRecord.indexOf("study_uid")).toString();
		}
		if (newStudiesList != studies){
			studies = newStudiesList;
			numStudies = studies.count();
			neededUpdate = true;
		}
#if useDebug
		qDebug() << "Updating Patient Inventory Studies List Statement Executed";
#endif
	}
	QSqlDatabase::removeDatabase("QIpatientInventoryDatabaseStudiesList");

	return neededUpdate;
}

/*!
 * \brief Builds the list of series for a given study.
 * \param studyUID UID of study to build series for.
 * \return Vector of all series info for a study, empty if none exist.
 */
QVector<QiSeriesInfo> QiPatientInventory::buildSeriesList(const QString& studyUID)
{
	QScopedPointer<QISql> qisql(new QISql(databaseSettings));
	QVector<QiSeriesInfo> seriesList;
	{
		QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabaseSeriesList");
		int numSeriesinStudy = 0;
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 60) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(nullptr, "Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
			return QVector<QiSeriesInfo>();
		}
		QString sqlSeriesTable = qisql->seriesTable();
		QiSeriesInfo newseries("");
		QSqlRecord seriesRecord  = db.record(sqlSeriesTable);
		QSqlRecord thisSeriesRecord = seriesRecord;
		thisSeriesRecord.setValue("study_uid",studyUID);
		for(int i = thisSeriesRecord.count() - 1; i >= 0; i--)
			if(thisSeriesRecord.isNull(i))
				thisSeriesRecord.remove(i);

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));

		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) +QString(' ')
		        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,thisSeriesRecord,true)
		        + QString(" ORDER BY CAST(series_number AS INT)");

		sqlQuery->prepare(sqlStatement);
		for(int i=0;i<thisSeriesRecord.count();i++)
			sqlQuery->bindValue(i,thisSeriesRecord.value(i));
		sqlQuery->exec();
		if(!sqlQuery->next())
			seriesList = QVector<QiSeriesInfo>();
		else{
			//sqlQuery->first();
			do{
				newseries.setSeriesUID(sqlQuery->value(seriesRecord.indexOf("series_uid")).toString());
				newseries.setStudyUID(sqlQuery->value(seriesRecord.indexOf("study_uid")).toString());
				newseries.setDate(sqlQuery->value(seriesRecord.indexOf("series_date")).toDate());
				newseries.setModality(sqlQuery->value(seriesRecord.indexOf("modality")).toString());
				newseries.setTime(sqlQuery->value(seriesRecord.indexOf("series_time")).toTime());
				newseries.setSeriesDesc(sqlQuery->value(seriesRecord.indexOf("series_desc")).toString());
				newseries.setNumTimePts(sqlQuery->value(seriesRecord.indexOf("num_temporal_positions")).toInt());
				//newseries.setNumImages(sqlQuery->value(seriesRecord.indexOf("numimages")).toInt());
				QStringList orientationString = sqlQuery->value(seriesRecord.indexOf("image_orientation")).toString().split('\\');
				QVector<int> imageOrientation;
				for(int i = 0; i<orientationString.size(); i++ )
					imageOrientation << orientationString.at(i).toInt();
				newseries.setImageOrientation(imageOrientation);

				if(newseries.modality() == QUANTX::US || newseries.modality() == QUANTX::MAMMO)
					if(sqlQuery->value(seriesRecord.indexOf("space_between_slices")).toFloat() > 0.0f)
						newseries.setModality(newseries.modalityString() + "3D");
				bool postTest = false;
				//This would be better to put in QSettings
				QString seriesDesc = newseries.seriesDesc();
				if(seriesDesc.trimmed().endsWith("POST")){
					QString seriesDescPre = seriesDesc;
					seriesDescPre.replace("POST ","PRE");
					seriesDescPre.replace("POST","PRE ");
					for(const auto & oldseries : std::as_const(seriesList))
						if(oldseries.seriesDesc() == seriesDescPre)
							postTest = true;
				}
				if(seriesDesc.trimmed().endsWith("post") || seriesDesc.trimmed().endsWith("pre")){
					QString seriesDescPre = seriesDesc;
					seriesDescPre.replace("post ","pre");
					seriesDescPre.replace("post","pre ");
					for(const auto & oldseries : std::as_const(seriesList))
						if(oldseries.seriesDesc() == seriesDescPre)
							postTest = true;
				}
				if( studyUID.startsWith("1.3.6.1.4.1.18047") && ( seriesDesc.contains("POST") || seriesDesc.contains("Post") ) )
					postTest = true;
				// This should actually test against quantxData->dynMultiSeriesDesc
				else if(seriesDesc.contains("POST")){
					for(int i = 0; i < seriesList.size(); ++i)
						if(seriesList[i].seriesDesc() == seriesDesc){
							if(newseries.time() < seriesList[i].time()){
								seriesList.removeAt(i);
								numSeriesinStudy -= 1;
							}
							break; //This seriesDesc should only match once
						}
				}
				if(!postTest){
					seriesList << newseries;
					numSeriesinStudy++;
				}


			}while(sqlQuery->next());

			numSeries << numSeriesinStudy;
		}
	}
	QSqlDatabase::removeDatabase("QIpatientInventoryDatabaseSeriesList");

	return seriesList;
}

/*!
 * \brief Refreshes the inventory.
 * \return 1 if successful, 0 otherwise.
 */
int QiPatientInventory::updateInventory(){
	QScopedPointer<QISql> qisql(new QISql(databaseSettings));
	{
		QSqlDatabase db = qisql->createDatabase("QIupdateInventory");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 60) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(nullptr, "Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
			return 0;
		}
		bool newStudies = updateStudiesList();
		if (!newStudies){
#if useDebug
			qDebug() << "No new studies";
#endif
		}
		for (const auto & study : studies){
			const auto seriesList = buildSeriesList(study);
			const auto oldSeriesList = getSeriesInStudy(study);
			if (seriesList == getSeriesInStudy(study)){
#if useDebug
				qDebug() << "No change in the series for study " << study;
				qDebug() << getSeriesUIDInStudy(study);
#endif
			} else {
#if useDebug
				qDebug() << "Change in the series for study " << study;
				qDebug() << getSeriesUIDInStudy(study);
#endif
				int studyIndex = studies.indexOf(study);
				auto oldList = getSeriesInStudy(study);
				if (newStudies && !oldList.isEmpty()){//there are new studies but the current study isn't one of them
					for (int i=0; i<seriesList.count(); i++){
						if (series.at(i) == oldList){
							studyIndex = i;
							break;
						}
					}
				}
				bool newStudy = false;
				if (oldList.isEmpty()){ // insert new study
					newStudy = true;
					series.insert(studyIndex, seriesList);
					thumbnails.insert(studyIndex, QVector<QImage>());
					numSeries.insert(studyIndex, seriesList.count());
				} else { // update existing study
					series.replace(studyIndex, seriesList);
					numSeries.replace(studyIndex, seriesList.count());
				}
				//studyIndex = studies.indexOf(study); // this should correct for either new or previously existing studies
				for (auto series : seriesList){
					if (!oldList.contains(series)){
#if useDebug
						qDebug() << "Missing series: " << series.seriesUID();
#endif
						auto seriesIndex = seriesList.indexOf(series);
						auto imageUID = chooseImageUIDForThumbnail(series);
						auto thumbnail = getCachedThumbnail(series.seriesUID(), thumbnails.first().first().width());
						if (thumbnail.isNull()){
							thumbnail = buildThumbnail(imageUID, thumbnails.first().first().width(), series.modality());
							insertThumbnail(series.seriesUID(), thumbnail);
						}
						thumbnails[studyIndex].insert(seriesIndex,thumbnail);
						emit newSeriesAdded(studyIndex, seriesIndex, newStudy);
						newStudy = false; // for new studies, only want this to be true the first time we emit newSeriesAdded
					}
				}
			}
		}
		db.close();
	}
	QSqlDatabase::removeDatabase("QIupdateInventory");

	return 1;
}

/*!
 * \brief Build thumbnails for all inventory images. It first checks for cached thumbnails, and then creates them as needed.
 * \param thumbSize Size to make the thumbnails, specifically their width.
 * \return success/fail, although only returns success at the moment...
 */
int QiPatientInventory::buildThumbnails(int thumbSize)
{
#if useDebug
	qDebug() << "Building Patient Inventory Thumbnails";
	QElapsedTimer whole, loop;
	whole.start();
	int seriesi{};
	int seriesj{};
#endif

	thumbnails.clear();

	for (auto & serie : series) {
		QVector<QImage> seriesthumbs;
#if useDebug
		++seriesi;
		seriesj = 0;
#endif
		for (auto & j : serie) {
#if useDebug
			qDebug() << "Get cached thumbnail for series" << seriesi << "," << ++seriesj;
#endif
			QImage tempthumbnail = getCachedThumbnail(j.seriesUID(), thumbSize);
			if (!tempthumbnail.isNull()){
				seriesthumbs<<tempthumbnail;
#if useDebug
				qDebug()<<"Thumbnail in Database";
#endif
			}else {
#if useDebug
				loop.start();

				qDebug()<<"Thumbnail NOT in Database, generating...";
#endif
				QString uid = chooseImageUIDForThumbnail(j);
#if useDebug
				int loopTime1 = loop.elapsed();
				QEventLoop eventLoop;
				qDebug()<<"Waiting " << DEBUG_WAITTIME_ / 1000.0 << " Seconds...";
				QTimer::singleShot(DEBUG_WAITTIME_, &eventLoop, SLOT(quit()));
				eventLoop.exec();
#endif
				QImage tempthumbnail = buildThumbnail(uid, thumbSize, j.modality());
				if(!tempthumbnail.isNull()){
					bool flipX = false;
					bool flipY = false;
					if ( (j.modality() == QUANTX::MRI) && (j.imageOrientation().size() == 6) ){
						QVector<int> orientation = j.imageOrientation();
						for (int direction = 0; direction < 3; direction++ ){
							if ((direction < 2 && orientation[direction] < 0 ) || (direction == 2 && orientation[direction] > 0 )){
								flipX = true;
								break;
							}
						}
						for (int direction = 3; direction < 6; direction++ ){
							if ((direction < 5 && orientation[direction] < 0 ) || (direction == 5 && orientation[direction] > 0 )){
								flipY = true;
								break;
							}
						}
#if useDebug
						qDebug() << "Series: " << j.seriesDesc();
						qDebug() << "\tOrientation: " << orientation;
						qDebug() << "\tFlip X: "<< flipX << "FlipY: " << flipY;
#endif
					}
					seriesthumbs << tempthumbnail.mirrored(flipY, flipX);
#if useDebug
					//int loopTime2 = loop.elapsed();
#endif
					insertThumbnail(j.seriesUID(),seriesthumbs.last());
				}
				else {
					seriesthumbs.append(tempthumbnail); // just insert the null image
				}
#if useDebug
				int loopTime3 = loop.elapsed();
				qDebug() << j.seriesDesc() << " SeriesUID: " << j.seriesUID() << "Thumb ImageUID:" << uid <<
				          "\n\tChoose Image UID time: " << loopTime1 <<
				          //"\n\tBuild Thumbnail time: " << loopTime2 - loopTime1 - DEBUG_WAITTIME_ <<
				          "\n\tBuild & Insert Thumbnail time: " << loopTime3 - loopTime1 <<
				          "\n\tTotal time for series: " << loop.elapsed() - DEBUG_WAITTIME_ <<
				          "\n========================================";
#endif
			}
		}

		thumbnails << seriesthumbs;
	}
	emit inventoryFinished();
	return 1;
}

/*!
 * \brief Gets the patient name.
 * \pre For this to mean anything, the inventory must be initialized and built.
 * \return Patient name.
 */
QString QiPatientInventory::getPatientName()
{
   return patientName;
}

/*!
 * \brief Gets the patient date of birth.
 * \pre For this to mean anything, the inventory must be initialized and built.
 * \return Date of birth.
 */
QDate QiPatientInventory::getPatientDOB()
{
   return patientDOB;
}

/*!
 * \brief Gets the MRN of the patient.
 * \pre For this to mean anything, the inventory must be initialized and built.
 * \return
 */
QString QiPatientInventory::getMRN()
{
   return mrn;
}

/*!
 * \brief Gets studies for the patient.
 * \pre For this to mean anything, the inventory must be initialized and built.
 * \return List of studies.
 */
QStringList const QiPatientInventory::getStudies()
{
   return studies;
}

/*!
 * \brief Gets the accessions for the patient.
 * \pre For this to mean anything, the inventory must be initialized and built.
 * \return List of accessions.
 */
QStringList const QiPatientInventory::getAccessions()
{
   return accessions;
}

/*!
 * \brief Gets the referring physicians.
 * \pre For this to mean anything, the inventory must be initialized and built.
 * \return List of referring physicians.
 */
QStringList const QiPatientInventory::getReferringPhysicians()
{
   return referringPhysicians;
}

/*!
 * \brief Gets the requesting physicians.
 * \pre For this to mean anything, the inventory must be initialized and built.
 * \return List of requesting physicians.
 */
QStringList const QiPatientInventory::getRequestingPhysicians()
{
   return requestingPhysicians;
}

/*!
 * \brief Gets a list of all series UIDs.
 * \return List of all series UIDs, empty if none found.
 */
QStringList QiPatientInventory::getAllSeries() {

	QStringList serieslist;
	for (auto & serie : series) {
		for (auto & j : serie) {
			serieslist<<j.seriesUID();
		}
	}
	return serieslist;
}

/*!
 * \brief Get series in a given study.
 * \param studyUID UID of study to query for series.
 * \return Vector of all series represented by QiSeriesInfo objects, empty if none found.
 */
QVector<QiSeriesInfo> QiPatientInventory::getSeriesInStudy(const QString& studyUID)
{
	for (auto series : series){
		if (series.first().studyUID() == studyUID)
			return series;
	}
	return QVector<QiSeriesInfo>();
}

/*!
 * \brief Get series in a given study.
 * \overload
 * \param index Index of the study to get series info on.
 * \return Vector of series in study, represented by QiSeriesInfo objects, empty if none found.
 */
QVector<QiSeriesInfo> QiPatientInventory::getSeriesInStudy(int index)
{
	if ((index >=0) && (index < series.size()))
		return series[index];

	    return QVector<QiSeriesInfo>();
}

/*!
 * \brief Get list of series UIDs in a study.
 * \param studyUID UID of study to get series for.
 * \return List of UIDs, empty if none found.
 */
QStringList QiPatientInventory::getSeriesUIDInStudy(const QString& studyUID)
{
	QStringList seriesUIDs;
	for (auto study : series)
		if (study.first().studyUID() == studyUID)
			for(const auto & s : study)
				seriesUIDs << s.seriesUID();
	return seriesUIDs;
}

/*!
 * \brief Get list of series UIDs in a study.
 * \param index Index of study to get series for.
 * \overload
 * \return List of series UIDs. Empty if none found.
 */
QStringList QiPatientInventory::getSeriesUIDInStudy(int index)
{
	QStringList seriesUIDs;
	if ((index >=0) && (index < series.size()))
		for(const auto & s : series.at(index))
			seriesUIDs << s.seriesUID();
	return seriesUIDs;
}

/*!
 * \brief Get number of series in a study.
 * \param studyUID UID of study to query on.
 * \return Number of series found, -1 if none found.
 */
int QiPatientInventory::getNumSeriesinStudy(const QString& studyUID)
{
	int index=studies.indexOf(studyUID);
	if (index !=-1)
		return numSeries.at(index);

	    return(-1);
}

/*!
 * \brief Get number of series in a study.
 * \overload
 * \param index Index of study to query.
 * \return Number of series, -1 if none found.
 */
int QiPatientInventory::getNumSeriesinStudy(int index)
{
	if ((index >=0) && (index < series.size()))
		return numSeries.at(index);

	    return(-1);
}

/*!
 * \brief Get number of studies in patient inventory.
 * \return Number of studies.
 */
int QiPatientInventory::getNumStudies()
{
	return numStudies;
}

/*!
 * \brief Build a thumbnail.
 * \param imageUID Image UID for the thumbnail to build.
 * \param thumbwidth Width of the thumbnail.
 * \param modality Modality of the image.
 * \return Thumbnail image, blank QImage if none the UID is empty, or no image is found for the given UID and modality.
 */
QImage QiPatientInventory::buildThumbnail(const QString& imageUID,int thumbwidth, QUANTX::Modality modality)
{
	if(imageUID.isEmpty())
		return QImage();

	QImage thumbnail;
	{
#if useDebug
		QElapsedTimer loop;
		loop.start();
		int initializationTime, sqlQueryTime = 0, createImageDataTime = 0, scaleImageTime;
#endif

		QScopedPointer<QISql> qisql(new QISql(databaseSettings));
		QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabaseBuildThumbnail");
    #if useDebug
		qDebug()<<"Opening DB (buildthumbnail)";
    #endif
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 60) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(nullptr, "Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
			return QImage();
		}

		QString sqlImageTable = qisql->imageTable(modality);

		QSqlRecord imageRecord   = db.record(sqlImageTable);

		//quint16 maxPixval;
		//quint16 minPixval;
		int width;
		int height;
		double windowWidth;
		double windowLevel;
		QString manufacturer;
		QString modelName;
		QString photometricInterpretation;

		QByteArray imageData;
		quint16* dataptr = nullptr;
		quint8* dataptr8bit = nullptr;

		QSqlRecord thumbnailRecord = imageRecord;
		thumbnailRecord.setValue("image_uid", imageUID);
		for(int i=thumbnailRecord.count()-1;i>=0;i--)
			if(thumbnailRecord.isNull(i))
				thumbnailRecord.remove(i);

		QStringList returnedFields;
		returnedFields<<"image_uid"<<"image_data"<<"width"<<"height"<<"window_width"<<"window_center"<<
		                "photometricInterpretation"<<"manufacturer"<<"modelName";
		for (int i = imageRecord.count();i>=0;i--){
			if (!returnedFields.contains(imageRecord.fieldName(i)))
				imageRecord.remove(i);
		}
		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));


		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
		        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,thumbnailRecord,true);
		sqlQuery->prepare(sqlStatement);
		for(int i=0;i<thumbnailRecord.count();i++)
			sqlQuery->bindValue(i,thumbnailRecord.value(i));
#if useDebug
		initializationTime = loop.elapsed();
		qDebug()<<"SQL Statement: "<<sqlStatement<<" imageUID: "<<imageUID;
#endif

		sqlQuery->exec();

		if(sqlQuery->next()){
			//sqlQuery->first();

			int index_width         = imageRecord.indexOf("width");
			int index_height        = imageRecord.indexOf("height");
			int index_windowWidth   = imageRecord.indexOf("window_width");
			int index_windowCenter  = imageRecord.indexOf("window_center");
			//int index_minPixval     = imageRecord.indexOf("min_pixval");
			//int index_maxPixval     = imageRecord.indexOf("max_pixval");
			int index_image_data    = imageRecord.indexOf("image_data");
			int index_photometricInterpretation = imageRecord.indexOf("photometric_interpretation");
			int index_manufacturer  = imageRecord.indexOf("manufacturer");
			int index_modelName     = imageRecord.indexOf("model_name");
			int temppixval;
			//maxPixval = sqlQuery->value(index_maxPixval).toUInt();
			//minPixval = sqlQuery->value(index_minPixval).toUInt();
			width = sqlQuery->value(index_width).toInt();
			height = sqlQuery->value(index_height).toInt();
			windowWidth = sqlQuery->value(index_windowWidth).toDouble();
			windowLevel = sqlQuery->value(index_windowCenter).toDouble();
			photometricInterpretation = sqlQuery->value(index_photometricInterpretation).toString();
			manufacturer = sqlQuery->value(index_manufacturer).toString();
			modelName = sqlQuery->value(index_modelName).toString();

			//fix for multiple window/level settings in DICOM header
			if (modality == QUANTX::MAMMO){
				if(windowWidth <= 0)
					windowWidth = sqlQuery->value(index_windowWidth).toString().split('\\').first().toDouble();
				if(windowLevel <= 0)
					windowLevel = sqlQuery->value(index_windowCenter).toString().split('\\').first().toDouble();
			}
#if useDebug
			sqlQueryTime = loop.elapsed();
#endif
			int planarConfiguration = 0;
	if(manufacturer.contains("Philips") && photometricInterpretation.contains("RGB",Qt::CaseInsensitive)){
#if useDebug
		qDebug() << "Found RGB image from manufacturer Philips: assuming planar configuration is 0";
#endif
		planarConfiguration = 0;
	}
	else if(manufacturer.contains("ATL") && photometricInterpretation.contains("RGB",Qt::CaseInsensitive)){
#if useDebug
		qDebug() << "Found RGB image from manufacturer ATL: assuming planar configuration is 1";
#endif
		planarConfiguration = 1;
	}


	        QScopedPointer<QImage> image(new QImage(width,height,QImage::Format_RGB32));
			//if(databaseSettings.at(0) == QString("QPSQL"))
			//    imageData = sqlQuery->value(index_image_data).toByteArray();
			//else
			    imageData = qUncompress(sqlQuery->value(index_image_data).toByteArray());

				int numPixels = width*height;
			if (imageData.size() == width*height)         //each pixel stored using 8 bits
				dataptr8bit = reinterpret_cast<quint8 *>(imageData.data());
			else if (imageData.size() == 2*width*height)  //each pixel stored using 16 bits
				dataptr = reinterpret_cast<quint16 *>(imageData.data());
			else if (imageData.size() == 3*width*height)  //each pixel stored using 8 bits for each RGB value
				dataptr8bit = reinterpret_cast<quint8 *>(imageData.data());
			else                                          //strange size, we might crash
				dataptr8bit = reinterpret_cast<quint8 *>(imageData.data());
			double scalefactor = 255./(windowWidth);
			double shiftamount = windowLevel-windowWidth/2;
			bool flipBrightness = photometricInterpretation.startsWith("MONOCHROME1");
			if (photometricInterpretation.contains("RGB",Qt::CaseInsensitive)) {
				for (int i=0;i<height;i++)
					for(int j=0;j<width;j++) {
						if (planarConfiguration == 0)
							image->setPixel(j,i,qRgb(dataptr8bit[3*width*i+3*j],
							                         dataptr8bit[3*width*i+3*j +1],
							                         dataptr8bit[3*width*i+3*j +2]));
						else if (planarConfiguration == 1)
							image->setPixel(j,i,qRgb(dataptr8bit[(width*i) +j],
							                         dataptr8bit[(width*i) + numPixels + j],
							                         dataptr8bit[(width*i) + 2 * numPixels + j]));
					}
			}
			else if(dataptr8bit != nullptr){
				for (int i=0;i<height;i++)
					for(int j=0;j<width;j++) {

						temppixval = static_cast<int>((dataptr8bit[width*i+j]-shiftamount)*scalefactor);
						if (temppixval < 0)
							temppixval = 0;
						if (temppixval > 255)
							temppixval = 255;
						if(flipBrightness)
							temppixval = 255 - temppixval;
						image->setPixel(j,i,qRgb(temppixval,temppixval,temppixval));
					}

			}
			else {
				for (int i=0;i<height;i++)
					for(int j=0;j<width;j++) {

						temppixval = static_cast<int>((dataptr[width*i+j]-shiftamount)*scalefactor);
						if (temppixval < 0)
							temppixval = 0;
						if (temppixval > 255)
							temppixval = 255;
						if(flipBrightness)
							temppixval = 255 - temppixval;
						image->setPixel(j,i,qRgb(temppixval,temppixval,temppixval));
					}
			}
#if useDebug
			createImageDataTime = loop.elapsed();
#endif

			if (thumbwidth >0)
				thumbnail=image->scaled(thumbwidth,thumbwidth,Qt::KeepAspectRatio,Qt::SmoothTransformation);
			else {
				thumbnail=*(image.data());
			}

		}
		//   QImageWriter writer("thumnailtest.png");
		//   writer.write(image->scaledToWidth(thumbwidth));
#if useDebug
		scaleImageTime = loop.elapsed();
		qDebug()<<"Closing DB (buildthumbnail)";
#endif

		sqlQuery.reset();
		db.close();
#if useDebug
		qDebug()<<"Build Thumbnail Timing:"<<
		          "\n\t\tInitialization:"<<initializationTime<<
		          "\n\t\tSQL Query:"<<sqlQueryTime-initializationTime<<
		          "\n\t\tCreateImageData:"<<createImageDataTime-sqlQueryTime<<
		          "\n\t\tScaleImage:"<<scaleImageTime-createImageDataTime<<
		          "\n\t\tTotal:"<<loop.elapsed();
#endif
	}
	QSqlDatabase::removeDatabase("QIpatientInventoryDatabaseBuildThumbnail");
	return thumbnail;
}

/*!
 * \brief Determine through a set of heuristics, and given a passed QiSeriesInfo object, which UID to use for a thumbnail.
 * \param series Info object passed about the series in question.
 * \return UID to use.
 */
QString QiPatientInventory::chooseImageUIDForThumbnail(QiSeriesInfo series)
{
	// decide which image from an image series to use for the thumbnail
	// for DCEMRI we'll use the middle slice from the first postcontrast time-point (for now)
#if useDebug
	QElapsedTimer loop;
	loop.start();
	int resultsMappingTime,chooseImageTime;
#endif

	QUANTX::Modality modality = series.modality();
	QString thumbnailImageUID;
	int thumbtimepoint=2; //right now, we choose the first postcontrast timept
	int numtimepoints = 0;
	int thumbslicenum = 0;
	int numslicespertimept;
	QMap<float,QString> imageMap;
	QScopedPointer<QISql> qisql(new QISql(databaseSettings));


	if (modality == QUANTX::MRI) {
		// Find the number of images here
		{
			QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabaseSeriesNumImages");
			bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 60) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(nullptr, "Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
			return QString();
		}
		QString sqlSeriesView = qisql->seriesView();
		QSqlRecord seriesRecord  = db.record(sqlSeriesView);
		QSqlRecord thisSeriesRecord = seriesRecord;
		thisSeriesRecord.setValue("study_uid",series.studyUID());
		thisSeriesRecord.setValue("series_uid",series.seriesUID());
		for(int i = thisSeriesRecord.count() - 1; i >= 0; i--)
			if(thisSeriesRecord.isNull(i))
				thisSeriesRecord.remove(i);

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));

		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesView,seriesRecord,false) +QString(' ')
		        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesView,thisSeriesRecord,true)
		        + QString(" ORDER BY CAST(series_number AS INT)");

		sqlQuery->prepare(sqlStatement);
		for(int i=0;i<thisSeriesRecord.count();i++)
			sqlQuery->bindValue(i,thisSeriesRecord.value(i));
		sqlQuery->exec();
		if(sqlQuery->next()){
			series.setNumImages(sqlQuery->value(seriesRecord.indexOf("numimages")).toInt());
		} else {
#if useDebug
			qDebug() << "Series View query returned no results";
#endif
		}

		numtimepoints = series.numTimePts();
		if(numtimepoints < 1)
			numtimepoints = 1;
		numslicespertimept = series.numImages() / numtimepoints;
		if (numtimepoints < 2)//check for non-dynamic series
			thumbtimepoint = 1;
		thumbslicenum = static_cast<int>( numslicespertimept / 2);
#if useDebug
		qDebug() << "Num. Time Points:" << numtimepoints;
		qDebug() << "Thumbtimepoint:" << thumbtimepoint;
		qDebug() << "Thumb Slice Num.:" << thumbslicenum;
#endif
		}
		QSqlDatabase::removeDatabase("QIpatientInventoryDatabaseSeriesNumImages");
	}
	{
		QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabaseChooseUID");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 60) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(nullptr, "Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
			return "NO IMAGE";
		}
		//QString sqlImageTable = qisql->imageLocationsView();
		QString sqlImageTable = qisql->imageTable(modality);
		QSqlRecord imageRecord = db.record(sqlImageTable);
		QSqlRecord seriesimageRecord = imageRecord;
		for (int i = 0;i<imageRecord.count();i++){
			        imageRecord.setGenerated(i,false);
		}
		imageRecord.setGenerated("series_uid",true);
		imageRecord.setGenerated("image_uid",true);
		seriesimageRecord.setValue("series_uid",series.seriesUID());
#if IPS4BATCH
		thumbtimepoint=-1;
#endif

		if (modality == QUANTX::MRI){
			imageRecord.setGenerated("slice_location",true);
			if (numtimepoints > 1){
				imageRecord.setGenerated("temporal_pos_id",true);
				//if(qisql->databaseType() != "QMYSQL" && qisql->databaseType() != "QSQLITE" && thumbtimepoint < 10)
				//    seriesimageRecord.setValue("temporal_pos_id",QString::number(thumbtimepoint) + QString(" "));
				//else
				    seriesimageRecord.setValue("temporal_pos_id",QString::number(thumbtimepoint));
			}
		}
		for(int i=seriesimageRecord.count()-1;i>=0;i--)
			if(seriesimageRecord.isNull(i))
				seriesimageRecord.remove(i);
		for(int i=imageRecord.count()-1;i>=0;i--)
			if(!(imageRecord.isGenerated(i)))
				imageRecord.remove(i);

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
		        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,seriesimageRecord,true);
		sqlQuery->prepare(sqlStatement);
#if useDebug
		qDebug() << "chooseImageUIDForThumbnail SQL Statement: " << sqlStatement;
#endif

		for(int i=0;i<seriesimageRecord.count();i++)
			sqlQuery->bindValue(i,seriesimageRecord.value(i));
#if useDebug
		int initializationTime = loop.elapsed();
#endif
		ok = sqlQuery->exec();
		if(ok && sqlQuery->driver()->hasFeature(QSqlDriver::QuerySize) && sqlQuery->size() == 0){
#if useDebug
		QStringList recordValues;
		for(int i=0;i<seriesimageRecord.count();i++)
			recordValues << seriesimageRecord.fieldName(i) << seriesimageRecord.value(i).toString() << QString(';');
		qDebug() << "No Valid Results For Record:" << recordValues;
#endif
		ok = false;
		/* This can take hours with a large database, it's grabbing the middle when selecting ALL images. We should probably make sure this has temporal pos id before removing it
		seriesimageRecord.remove(seriesimageRecord.indexOf("temporal_pos_id"));
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
				+ db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,seriesimageRecord,true);
		sqlQuery->prepare(sqlStatement);
#if useDebug
	qDebug() << "2nd chooseImageUIDForThumbnail SQL Statement: " << sqlStatement;
#endif

		for(int i=0;i<seriesimageRecord.count();i++)
			sqlQuery->bindValue(i,seriesimageRecord.value(i));
		ok = sqlQuery->exec();
		*/
	}
#if useDebug
	if(!ok){
		qDebug() << "Thumbnail ImageRecord Search Error";
		qDebug() << sqlQuery->lastError().text();
	}
	else{
		qDebug() << "Thumbnail ImageRecord Search Success";
	}

	    int sqlQueryTime = loop.elapsed();

#endif

		int index_sliceLocation = imageRecord.indexOf("slice_location");
		int index_imageUID      = imageRecord.indexOf("image_uid");
		if(thumbslicenum != 0) {
			sqlQuery->first();
			do{
				imageMap.insert(sqlQuery->value(index_sliceLocation).toFloat(),sqlQuery->value(index_imageUID).toString());
			}while(sqlQuery->next());
#if useDebug
			resultsMappingTime = loop.elapsed();
#endif

			if(thumbslicenum < imageMap.size()) {
				thumbnailImageUID = imageMap.values()[thumbslicenum];
			} else {
				thumbnailImageUID = imageMap.values()[(imageMap.size() - 1)/2];
			}
		} else {
#if useDebug
			resultsMappingTime = loop.elapsed();
#endif
			sqlQuery->first();
			thumbnailImageUID = sqlQuery->value(index_imageUID).toString();
		}
#if useDebug
		chooseImageTime = loop.elapsed();
#endif
		sqlQuery.reset();
		db.close();
#if useDebug
		qDebug()<<"ChooseImageUID Timing:" <<
		          "\n\t\tInitialization:" << initializationTime <<
		          "\n\t\tSQL Query:" << sqlQueryTime-initializationTime <<
		          "\n\t\tResultsMapping:" << resultsMappingTime-sqlQueryTime <<
		          "\n\t\tChooseImage:" << chooseImageTime-resultsMappingTime <<
		          "\n\t\tTotal:" << loop.elapsed();
		qDebug()<<"Closing DB (ChooseUID)";
#endif

	}
	QSqlDatabase::removeDatabase("QIpatientInventoryDatabaseChooseUID");

	return thumbnailImageUID;
}

/*!
 * \brief Get the list of image UIDs for a series.
 * \param series Series info object to use in getting the list.
 * \return A QStringList of the UIDs to use.
 */
QStringList QiPatientInventory::getImageUIDsForSeries(const QiSeriesInfo& series){

	QUANTX::Modality modality = series.modality();
	QStringList imageUIDs;

	{
		QScopedPointer<QISql> qisql(new QISql(databaseSettings));
		QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabaseChooseUID");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 60) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(nullptr, "Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
			return QStringList();
		}
		//QString sqlImageTable = qisql->imageLocationsView();
		QString sqlImageTable = qisql->imageTable(modality);
		QSqlRecord imageRecord = db.record(sqlImageTable);
		QSqlRecord seriesimageRecord = imageRecord;
		for (int i = 0;i<imageRecord.count();i++){
			imageRecord.setGenerated(i,false);
		}
		imageRecord.setGenerated("series_uid",true);
		imageRecord.setGenerated("image_uid",true);
		seriesimageRecord.setValue("series_uid",series.seriesUID());
		for(int i=seriesimageRecord.count()-1;i>=0;i--)
			if(seriesimageRecord.isNull(i))
				seriesimageRecord.remove(i);
		for(int i=imageRecord.count()-1;i>=0;i--)
			if(!(imageRecord.isGenerated(i)))
				imageRecord.remove(i);

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
		        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,seriesimageRecord,true);
		sqlQuery->prepare(sqlStatement);
		for(int i=0;i<seriesimageRecord.count();i++)
			sqlQuery->bindValue(i,seriesimageRecord.value(i));
		ok = sqlQuery->exec();
#if useDebug
		if(!ok){
			qDebug() << "ImageRecord Search Error";
			qDebug() << sqlQuery->lastError().text();
		}
		else{
			qDebug() << "ImageRecord Search Success";
		}
#endif
		int index_imageUID      = imageRecord.indexOf("image_uid");
		sqlQuery->first();
		do{
			imageUIDs << sqlQuery->value(index_imageUID).toString();
		}while(sqlQuery->next());
		sqlQuery.reset();
		db.close();
#if useDebug
		qDebug()<<"Closing DB (ChooseUID)";
#endif

	}
	QSqlDatabase::removeDatabase("QIpatientInventoryDatabaseChooseUID");

	return imageUIDs;
}

/*!
 * \brief Gets cached thumbnail for series, if it exists.
 * \param seriesUID UID of series to get.
 * \param thumbSize Requested thumbnail size, in pixels. (width)
 * \return Thumbnail if it exists in the cache, blank otherwise.
 */
QImage QiPatientInventory::getCachedThumbnail(const QString & seriesUID, int thumbSize){
#if useDebug
	qDebug() << "Getting Cached Thumbnail";
#endif
	QScopedPointer<QISql> qisql(new QISql(getDBSettings()));
	QImage thumbnail;
#if useDebug
	qDebug() << "Creating DB Object";
#endif
	{
		QSqlDatabase db = qisql->createDatabase("QIpatientInventoryDatabaseGetCachedThumbnail");
		if(!db.isValid()){
#if useDebug
		qDebug() << "Critical: Database is not valid!!!";
#endif
		return QImage();
	}
#if useDebug
	qDebug() << "Opening DB Object";
#endif
	bool ok = false;
	try{
		ok = db.open();  //if ok is false, we should error and quit
	}
	catch(...){
#if useDebug
		qDebug() << "Opening DB Object CRASHED!!!";
#endif
		std::cout << "WTF?????";
		ok = false;
	}
#if useDebug
	qDebug() << "Database Open:" << ok;
#endif
	if(!ok){
		int retries = 0;
		while( (!ok) && (retries++ < 0) ){
			QThread::sleep(1); // Wait one second, and retry
		}
#if useDebug
		qDebug() << "Critical";
#endif
		QMessageBox::critical(nullptr, "Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		return QImage();
	}
#if useDebug
	qDebug() << "Cached Thumbnail Database Open";
#endif

	QString sqlThumbnailTable = qisql->thumbnailTable();
	QSqlRecord seriesRecord  = db.record(sqlThumbnailTable);
	QSqlRecord thisSeriesRecord = seriesRecord;
	thisSeriesRecord.setValue("series_uid",seriesUID);
	thisSeriesRecord.setValue("max_dim",thumbSize);
	for(int i=thisSeriesRecord.count()-1;i>=0;i--){
		seriesRecord.setGenerated(i,false);
		if(thisSeriesRecord.isNull(i))
			thisSeriesRecord.remove(i);
	}
	seriesRecord.setGenerated("series_uid",true);
	seriesRecord.setGenerated("thumbnail",true);

	QString sqlStatement;
	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlThumbnailTable,seriesRecord,false) +QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlThumbnailTable,thisSeriesRecord,true);
	        sqlQuery->prepare(sqlStatement);
			for(int i=0;i<thisSeriesRecord.count();i++)
		sqlQuery->bindValue(i,thisSeriesRecord.value(i));

	ok = sqlQuery->exec();
#if useDebug
	if(!ok){
		qDebug() << "Thumbnail Search Error";
		qDebug() << sqlQuery->lastError().text();
	}
	else{
		qDebug() << "Thumbnail Search Success";
	}
#endif

	if(sqlQuery->first()){
		seriesRecord = sqlQuery->record();
		QByteArray thumbData=sqlQuery->value(seriesRecord.indexOf("thumbnail")).toByteArray();
		thumbnail = QImage::fromData(thumbData);
	}
	sqlQuery.reset();
	db.close();
	}
	QSqlDatabase::removeDatabase("QIpatientInventoryDatabaseGetCachedThumbnail");
	return thumbnail;
}

/*!
 * \brief Inserts a new thumbnail for a given series.
 * \param seriesUID UID of series to insert on.
 * \param thumbnail Thumbnail image to use.
 * \return True if successful, false otherwise.
 */
bool QiPatientInventory::insertThumbnail(const QString& seriesUID, const QImage& thumbnail){
	QScopedPointer<QISql> qisql(new QISql(databaseSettings));
	bool result;
	{
		QSqlDatabase db = qisql->createDatabase("QIpatientInventoryThumbnailDatabase");
#if useDebug
		qDebug()<<"Opening DB (insertthumbnail)";
#endif

	bool ok = db.open();  //if ok is false, we should error and quit
	if(!ok){
		int retries = 0;
		while( (!ok) && (retries++ < 60) ){
			QThread::sleep(1); // Wait one second, and retry
		}
		QMessageBox::critical(nullptr, "Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		return false;
	}
	QString sqlThumbnailTable = qisql->thumbnailTable();
	int width = thumbnail.width();
	int height = thumbnail.height();
	int maxDim = qMax(width, height);
	int minDim = qMin(width,height);
	QSqlRecord seriesRecord  = db.record(sqlThumbnailTable);
	seriesRecord.setValue("series_uid",seriesUID);
	seriesRecord.setValue("version", 0);
	seriesRecord.setValue("width",width);
	seriesRecord.setValue("height",height);
	seriesRecord.setValue("max_dim",maxDim);
	seriesRecord.setValue("min_dim",minDim);
	QByteArray thumbnailData;
	QBuffer buffer(&thumbnailData);
	buffer.open(QIODevice::WriteOnly);
	ok = thumbnail.save(&buffer,"JPG",85);
#if useDebug
	if(!ok)
		qDebug() << "Thumbnail conversion to jpeg failed" << thumbnail.size() << QImageWriter::supportedImageFormats();
#endif
	buffer.close();
	seriesRecord.setValue("thumbnail",thumbnailData);
	//Remove all null values before insertion into database
	for(int i=seriesRecord.count()-1;i>=0;i--)
		if(seriesRecord.isNull(i))
			seriesRecord.remove(i);

	QString sqlStatement;
	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
#if useDebug
	//qDebug()<<"Thumbnail record size:"<< seriesRecord.field("thumbnail").value().toByteArray().size();
#endif
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement,sqlThumbnailTable,seriesRecord,true);
	       sqlQuery->prepare(sqlStatement);
		for(int i=0;i<seriesRecord.count();i++)
		sqlQuery->bindValue(i,seriesRecord.value(i));

	result = sqlQuery->exec();
#if useDebug
	qDebug()<<"SQL Statement: "<<sqlStatement;
	qDebug()<<"Thumbdata size: "<<thumbnailData.size();
#endif
	while (!result){
		QEventLoop eventLoop;
#if useDebug
		qDebug()<<"Waiting " << 500 / 1000.0 << " Seconds...";
#endif
		QTimer::singleShot(500, &eventLoop, SLOT(quit()));
		eventLoop.exec();

		result = sqlQuery->exec();
#if useDebug
		if (result)
		qDebug()<<"Insert Successful!";
	else {
		qDebug()<<"Insert Failed!";
		if (sqlQuery->lastError().isValid())
			qDebug()<< sqlQuery->lastError().text();
		else
			qDebug()<<"Error is not valid";

	}
#endif
	}


#if useDebug
	qDebug()<<"Closing DB (insertThumbnail)";
#endif

	sqlQuery.reset();
	db.close();
	}
	QSqlDatabase::removeDatabase("QIpatientInventoryThumbnailDatabase");
	return result;
}

/*!
 * \brief Get thumbnail by study number and series number.
 * \param studyNum Study number.
 * \param seriesNum Series number.
 * \pre QiPatientInventory is initialized and populated with patient data.
 * \return QImage of the thumbnail.
 */
  QImage QiPatientInventory::getThumbnail(int studyNum, int seriesNum) {
	  return thumbnails[studyNum][seriesNum];

}

/*!
 * \brief Setter for DB settings.
 * \param dbSettings DB settings list to set.
 */
  void QiPatientInventory::setDBSettings(const QStringList &dbSettings){
	  databaseSettings = dbSettings;
  }

