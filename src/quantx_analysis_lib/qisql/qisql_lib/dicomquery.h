#ifndef DICOMQUERY_H
#define DICOMQUERY_H

#include <QObject>
#include "qisqlDllCheck.h"
#include "gdcmBaseRootQuery.h"
#include <QPointF>

namespace quantx {
namespace mranalysis {

/*!
 * \brief A small class for moving DICOM series data around.
 * \ingroup SQLModule
 */
class QISQL_EXPORT DicomSeriesInfo {
public:
	QString seriesUID; //!< Stores the series UID value for identifying the series within an organization.
	QString studyUID; //!< Stores the study UID value for identifying the study within an organization and series.
	QString seriesDescription; //!< Stores the description of a given series.
	QStringList imageType; //!< Stores the types of images for the series.
	QStringList scanOptions; //!< Stores the scan options for the series.
	QPointF pixelSpacing; //!< Stores the pixel spacing value.
	int seriesNumber; //!< Stores the series number.
	int numImages; //!< Stores the number of images.
	operator QString() const { return QString("UID: %1 (%2) Series %3: %4, %5 images, resxy: (%6,%7) ScanOp: %8").arg(seriesUID).arg(imageType.join("\\")).arg(seriesNumber).arg(seriesDescription).arg(numImages).arg(pixelSpacing.x()).arg(pixelSpacing.y()).arg(scanOptions.join("\\"));} //!< Returns a custom formatted QString representation of the series info.
	bool operator <(const DicomSeriesInfo &otherSeries) const {return seriesNumber < otherSeries.seriesNumber;} //!< Series comparator operator to see which one has the larger series number.
};

/*!
 * \brief Class to encapsulate the DICOM query.
 */
class QISQL_EXPORT DicomQuery : public QObject
{
	Q_OBJECT
public:
	explicit DicomQuery(QString dicomHost = QString(), uint dicomPort = 0, QString aeTitle = QString(), QString callingAeTitle = QString(), QObject *parent = nullptr);
	bool verifyConnection(); // C-ECHO
	int findNumImages(QString studyUID, QString seriesUID);
	QMap<QString, int> findNumImages(QString studyUID, QStringList seriesList, bool quick = false);
	QMap<QString, int> findNumObjects(QString studyUID, QStringList seriesList);
	QStringList findObjects(QString studyUID, QString seriesUID, QString sopClass = "1.2.840.10008*");
	DicomSeriesInfo getSeriesInfo(QString studyUID, QString seriesUID);
	QVector<DicomSeriesInfo> findSeriesInStudy(QString studyUID, bool primaryOnly = true, QString seriesDesc = QString(), QStringList seriesDescExcludeEndings = QStringList(),  QStringList seriesDescExcludeContains = QStringList());
	QVector<QMap<QString, QString> > worklistQuery(QString accession = QString(), QString patientID = QString(), QString patientName = QString(), QString reqProcedureID = QString());

signals:

public slots:

private:
	QString dicomHost_; //!< The DICOM host to query.
	uint16_t dicomPort_; //!< The port to use for DICOM host communication.
	QString aeTitle_; //!< The AE title of the query user we are querying with.
	QString callAeTitle_; //!< Stores the AE title of the provider (server) we are calling.
};

}
}

#endif // DICOMQUERY_H
