#include "qi3dmgimage.h"
#include "qisql.h"
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QtAlgorithms>
#include <QByteArray>
//#include <QProgressDialog>
#include <QPushButton>
#include <QCoreApplication>
#include <QFuture>
#include <QFutureWatcher>
#include <QtConcurrentRun>
#include <QEventLoop>
#include <utility>
//#include <QMessageBox>

//For internal testing of Query times. Remove prior to deployment.
#define useDebug 0
#if useDebug
#include <QElapsedTimer>
#include <QDebug>
#endif

#define testFullHeader 0

/*!
 * \brief Default constructor. Initializes the modality as QUANTX::Modality::MG3D.
 */
Qi3DMGImage::Qi3DMGImage()
{
	modality_ = QUANTX::Modality::MG3D;
}

/*!
 * \brief Open a 3D mammo series.
 * \param uid UID of series to open.
 * \param dbSettings DB settings to use to connect.
 * \return Success or lack thereof of the query. Success returns 1, failures can return other values, although at the moment, only -1 is returned in the case that no records match.
 */
int Qi3DMGImage::openSeries(QString uid,const QStringList &dbSettings){
	seriesUID_ = std::move(uid);
	data_.clear();
	windowWidth_ = 0;
	windowLevel_ = 0;
	maxPixval_ = 0;
	minPixval_ = 0;
	dx_ = 0.0;
	dy_ = 0.0;
	dz_ = 0.0;
	nx_ = 0;
	ny_ = 0;
	nz_ = 0;
	nt_ = 0;
	orientation_.clear();
	acquisitionOrientation_ = QUANTX::XYPLANE;
	int returnVal = getSeriesData(dbSettings);
	QSqlDatabase::removeDatabase("QIopenDatabase");
	return returnVal;
}


/*!
 * \brief Get series data.
 * \param dbSettings DB settings to use to connect.
 * \return Success or lack thereof of the query. Success returns 1, failures can return other values, although at the moment, only -1 is returned in the case that no records match.
 * \note This is an internal method called by Qi3DMGImage::openSeries.
 */
int Qi3DMGImage::getSeriesData(const QStringList &dbSettings){
#if useDebug
	QElapsedTimer whole, loop;
	whole.start();
#endif
	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	QSqlDatabase db = qisql->createDatabase("QIopenDatabase");

#if useDebug
	    qDebug()<<"Opening DB Qi3DMGImage";
#endif
		bool ok = db.open();  //if ok is false, we should error and quit
	if(!ok){
		int retries = 0;
		while( (!ok) && (retries++ < 5) ){
			QThread::sleep(1); // Wait one second, and retry
		}
		qWarning("Database Error!\nThe specified database could not be opened.\nCheck your database settings and try again.");
	}

	QString sqlImageTable = qisql->imageTable(QUANTX::MG3D);
	QString sqlSeriesTable = qisql->seriesTable();

	QSqlRecord seriesRecord  = db.record(sqlSeriesTable);
	QSqlRecord imageRecord   = db.record(sqlImageTable);
	QSqlRecord uidRecord = seriesRecord;
	uidRecord.setValue("series_uid",seriesUID_);
	for(int i=uidRecord.count()-1;i>=0;i--)
		if(uidRecord.isNull(i))
			uidRecord.remove(i);

	QString sqlStatement;
	QFuture<bool> sqlFuture;
	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));

	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,uidRecord,false);
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlFuture.waitForFinished();
	if(!sqlQuery->next())
		return -1; //no records match series UID
	//if(sqlQuery->size() == 0)
	//    return -1; //no records match series UID
	//if(sqlQuery->size() > 1)
	//    return -2; //more than one record with series UID requested

	sqlQuery->first();
	seriesRecord = sqlQuery->record(); //Set series record data to result instead of empty
	mrn_ = seriesRecord.value("mrn").toString();
	seriesDesc_ = seriesRecord.value("series_desc").toString();
	// New query to get the image data for this series
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);

	emit openProgressNewMaximum(2);// (n_TimePts*250*2) is an estimate of twice the number of slices we are going
	                                               // to read. Will be exact if each timept has 250 slices. We only
	                                               // have the n_TimePts after reading the series table. It isn't critical
	                                               // if it isn't exact since it's only for the progressbardialog
	                                               // Also, note that the SQLITE driver doesn't support sqlQuery->size();
	/*QProgressDialog *fileOpenProgress = new QProgressDialog("Opening MRI Case (time depends on speed of disk or network)","Please Wait...",0,100,parent);
	fileOpenProgress->setWindowTitle("Opening Image...");
	QPushButton* abortProgressButton = new QPushButton("Please Wait...");
	fileOpenProgress->setCancelButton(abortProgressButton);
	fileOpenProgress->setWindowModality(modalProgress);
	abortProgressButton->setEnabled(false);
	fileOpenProgress->setMinimumDuration(0);
	fileOpenProgress->setValue(1);
	fileOpenProgress->show();
	qApp->processEvents();*/

	//Run the SQL Query in a separate thread so that the UI still updates
	//QEventLoop sqlWaitLoop;

	//QFutureWatcher<bool> sqlWatcher;
	//QObject::connect(&sqlWatcher,SIGNAL(finished()),&sqlWaitLoop,SLOT(quit()));
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	//sqlWatcher.setFuture(sqlFuture);
	//sqlWaitLoop.exec();
	sqlFuture.waitForFinished();
#if useDebug
	int queryTime = whole.elapsed();
#endif

	//int index_imageUID      = imageRecord.indexOf("image_uid");
	int index_time          = imageRecord.indexOf("image_time");
    int index_date          = imageRecord.indexOf("image_date");
    int index_width         = imageRecord.indexOf("width");
	int index_height        = imageRecord.indexOf("height");
	int index_pixelsize_x   = imageRecord.indexOf("pixelsize_x");
	int index_pixelsize_y   = imageRecord.indexOf("pixelsize_y");
	int index_windowWidth   = imageRecord.indexOf("window_width");
	int index_windowCenter  = imageRecord.indexOf("window_center");
	int index_minPixval     = imageRecord.indexOf("min_pixval");
	int index_maxPixval     = imageRecord.indexOf("max_pixval");

	int index_image_data    = imageRecord.indexOf("image_data");
	int index_scanOptions   = imageRecord.indexOf("scan_options");
	int index_viewPosition  = imageRecord.indexOf("view_position");
	int index_imageLaterality = imageRecord.indexOf("image_laterality");
	int index_patientOrientation = imageRecord.indexOf("patient_orientation");
	int index_photometricInterpretation = imageRecord.indexOf("photometric_interpretation");
	int index_manufacturer  = imageRecord.indexOf("manufacturer");
	int index_modelName     = imageRecord.indexOf("model_name");
	int index_numberOfFrames      = imageRecord.indexOf("number_of_frames");

	sqlQuery->first();

	time_ =  sqlQuery->value(index_time).toTime();
    date_ = sqlQuery->value(index_date).toDate();
    windowWidth_ = sqlQuery->value(index_windowWidth).toUInt();
	windowLevel_ = sqlQuery->value(index_windowCenter).toUInt();
	minPixval_ = sqlQuery->value(index_minPixval).toUInt();
	maxPixval_ = sqlQuery->value(index_maxPixval).toUInt();
	dx_ = sqlQuery->value(index_pixelsize_x).toFloat();
	dy_ = sqlQuery->value(index_pixelsize_y).toFloat();
	dz_ = seriesRecord.value("space_between_slices").toFloat();
	nx_ = sqlQuery->value(index_width).toInt();
	ny_ = sqlQuery->value(index_height).toInt();
	nz_ = sqlQuery->value(index_numberOfFrames).toInt();
	if(index_scanOptions >= 0)
		scanOptions_ = sqlQuery->value(index_scanOptions).toString().trimmed().split('\\');
	else
		scanOptions_.clear();

	viewPosition = sqlQuery->value(index_viewPosition).toString();
	imageLaterality = sqlQuery->value(index_imageLaterality).toString();
	patientOrientation = sqlQuery->value(index_patientOrientation).toString();
	photometricInterpretation = sqlQuery->value(index_photometricInterpretation).toString();
	manufacturer_ = sqlQuery->value(index_manufacturer).toString();
	modelName_ = sqlQuery->value(index_modelName).toString();

	QByteArray imageData;
	imageData = qUncompress(sqlQuery->value(index_image_data).toByteArray());
	quint16 *imgptr,*dataptr;
	quint8 *imgptr8bit;
	imgptr = reinterpret_cast<quint16 *>(imageData.data());
	imgptr8bit = reinterpret_cast<quint8 *>(imageData.data());

#if useDebug
	qDebug() << "numberOfFrames index:" << index_numberOfFrames << "numberOfFrames" << sqlQuery->value(index_numberOfFrames).toInt();
	qDebug() << "seriesUID" << seriesUID_;
	qDebug() << "nx:" << nx_ << "ny:" << ny_ << "nz:" << nz_ << "imageData size:" << imageData.size();
	qDebug() << "spaceBetweenSlices:" << dz_;
	qDebug() << "Window Width:" << windowWidth_ << "WIndow Level:" << windowLevel_;
#endif

	//locations isn't used at all
	//for(int i = 0; i < nz_; i++){
	//   locations_ << i * dz_;
	//}while(sqlQuery->next());
	//qSort(locations);
	//sqlQuery->first();
	emit openProgressNewMaximum(2);
	emit openProgressUpdated(1);
#if testFullHeader
	QString imageUID = sqlQuery->value(imageRecord.indexOf("image_uid")).toString();
	QMultiHash<QString, QString> header = qisql->extractAllDicomFields(imageUID);
	QVector<QString> patientID = header.values("0010,0010");
	qDebug()<<"Header Size:"<<header.count();
	qDebug()<<"Patient ID:"<<patientID.first();
#endif

	if(nt_ < 1)
		nt_ = 1;
#if useDebug
	if (nt_ > 1)
		qDebug() << "Dynamic MG Series!";
	else
		qDebug() << "Standard 3DMG Series";
#endif

	//Fix for images that have multiple window/level values
	if(windowWidth_ <= 0)
		windowWidth_ = sqlQuery->value(index_windowWidth).toString().split('\\').first().toUInt();
	if(windowLevel_ <= 0)
		windowLevel_ = sqlQuery->value(index_windowCenter).toString().split('\\').first().toUInt();

	int dim4Size; // fourth dimension of the data set, nb if DWI, nt otherwise
	dim4Size = nt_;
#if useDebug
	qDebug() << "4th Dimension Size: " << dim4Size;
	loop.start();
#endif

	data_.clear();

	bool is16bit = (imageData.size() == (nx_ * ny_ * 2) * nz_);
	bool is8bit  = (imageData.size() == (nx_ * ny_) * nz_);

	data_.resize(dim4Size);
	for(int t=0;t<dim4Size;t++){
		data_.at(t) = (std::vector<std::vector<std::vector<quint16> > >(nz_));
		for(int z=0;z<nz_;z++){
			data_[t].at(z) = (std::vector<std::vector<quint16> >(ny_));
			for(int y=0;y<ny_;y++){
				data_[t][z].at(y) = (std::vector<quint16>(nx_));
				dataptr = data_[t][z][y].data();
				long ptrloc = nx_ * ((z * ny_) + y);
				if(is16bit) {
					for(int x=0;x<nx_;x++){
						dataptr[x] = imgptr[ptrloc + x];
					}
				} else if (is8bit) { // 8-bit image
					for(int x=0;x<nx_;x++){
						dataptr[x] = imgptr8bit[ptrloc + x];
					}
				}
			}
		}
	}

#if useDebug
	qDebug() << "Partial Image Data:" << data_[0][nz_/2][ny_/2].mid(nx_/2 - 50,100);
	qDebug() << "Closing DB Qi3DMGImage";
#endif

	sqlQuery.reset();
	db.close();

#if useDebug
	qDebug() << "Query Time: " << queryTime;
	qDebug() << "Loop Time : " << loop.elapsed();
	qDebug() << "Whole Time: " << whole.elapsed();
#endif

	return 1; //success!

}


