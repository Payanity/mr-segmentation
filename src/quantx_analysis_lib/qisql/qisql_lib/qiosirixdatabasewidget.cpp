#include <QtGlobal>
#ifdef Q_OS_MAC

#include "qiosirixdatabasewidget.h"
#include "qisql.h"
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QSqlTableModel>
#include <QMessageBox>
#include <QFileInfo>
#include <QDir>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlDriver>
#include <QProgressDialog>
#include <QDateTime>
#include <QApplication>
#include <QFuture>
#include <QtConcurrent>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

QiOsirixDatabaseWidget::QiOsirixDatabaseWidget(QWidget *parent) :
    QWidget(parent)
{
	QLabel* nameSearchLabel = new QLabel("Name");
	nameLineEdit = new QLineEdit;
	QLabel* mrnSearchLabel  = new QLabel("MRN");
	mrnLineEdit  = new QLineEdit;
	QPushButton* searchButton = new QPushButton("Clear Search");
	QPushButton* rescanButton = new QPushButton("ReScan");
	QPushButton* importButton = new QPushButton("Import");

	QObject::connect(rescanButton, &QPushButton::clicked, this, &QiOsirixDatabaseWidget::initializeTableWidget);
	QObject::connect(nameLineEdit, QOverload<const QString &>::of(&QLineEdit::textChanged), this, &QiOsirixDatabaseWidget::searchFilterUpdate);
	QObject::connect(mrnLineEdit, QOverload<const QString &>::of(&QLineEdit::textChanged), this, &QiOsirixDatabaseWidget::searchFilterUpdate);
	QObject::connect(searchButton, &QPushButton::clicked, mrnLineEdit, &QLineEdit::clear);
	QObject::connect(searchButton, &QPushButton::clicked, nameLineEdit, &QLineEdit::clear);
	QObject::connect(importButton, &QPushButton::clicked, this, &QiOsirixDatabaseWidget::importStudiesFromColumnSelection);

	QGridLayout* searchGrid = new QGridLayout;
	searchGrid->addWidget(nameSearchLabel,0,0);
	searchGrid->addWidget(nameLineEdit,1,0);
	searchGrid->addWidget(mrnSearchLabel,0,1);
	searchGrid->addWidget(mrnLineEdit,1,1);
	searchGrid->addWidget(searchButton,1,2);
	searchGrid->addWidget(rescanButton,1,3);
	searchGrid->addWidget(importButton,1,4);

	QVBoxLayout* vbox = new QVBoxLayout;
	tableWidget = new QTableWidget;
	tableWidget->setObjectName("osirixWindowTableWidget");
	setDatabaseSettings(QStringList());
	vbox->addLayout(searchGrid);
	vbox->addWidget(tableWidget);

	setLayout(vbox);
	setWindowFlags(Qt::Window);
	setWindowTitle("Osirix Patient Database  -  Import to QuantX Application");
	resize(1000,500);

	tableWidget->setColumnWidth(0,380);
	tableWidget->setColumnWidth(4,160);

#if useDebug
	qDebug() << tableWidget->rowCount() << "total rows";
#endif
}

void QiOsirixDatabaseWidget::setDatabaseSettings(const QStringList &dbSettings)
{
	databaseSettingsQI = dbSettings;
	if(databaseSettingsQI.isEmpty()){
		databaseSettingsQI << "QSQLITE" <<
		                    "" <<
		                    "/Applications/QuantX.app/Contents/Database/quantx.db" <<
		                    "" <<
		                    "" ;
	}
	if(databaseSettingsOSIRIX.isEmpty()){
		databaseSettingsOSIRIX << "QSQLITE" <<
		                    "" <<
		                    QDir::homePath() + "/Documents/Osirix Data/Database.sql" <<
		                    "" <<
		                    "" ;
	}
	initializeTableWidget();
}

QStringList QiOsirixDatabaseWidget::getDatabaseSettings()
{
	return databaseSettingsOSIRIX;
}

void QiOsirixDatabaseWidget::initializeTableWidget(){
	{
		tableWidget->setSortingEnabled(false);
		if(databaseSettingsOSIRIX.first() == "QSQLITE" && !databaseSettingsOSIRIX.at(2).isEmpty()){
			QFileInfo sqlDBfileInfo(databaseSettingsOSIRIX.at(2));
			if(!sqlDBfileInfo.exists()){
				if(QMessageBox::warning(this,"Osirix Database File Missing","Cannot find Osirix Database File In Default Location" + databaseSettingsOSIRIX.at(2),QMessageBox::Yes | QMessageBox::No,QMessageBox::No) == QMessageBox::Yes){
					//We should probably use a file dialog here to find the file
				}
			}
		}

		QSqlDatabase dbOsirix = QSqlDatabase::addDatabase(databaseSettingsOSIRIX[0],"OSIRIXsearchDatabaseInitializeTree");
		dbOsirix.setHostName(databaseSettingsOSIRIX[1]);
		dbOsirix.setDatabaseName(databaseSettingsOSIRIX[2]);
		dbOsirix.setUserName(databaseSettingsOSIRIX[3]);
		dbOsirix.setPassword(databaseSettingsOSIRIX[4]);
		bool ok = dbOsirix.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(this,"Osirix Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		}

		tableWidget->clear();
		tableWidget->setColumnCount(7);
		tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

		QScopedPointer<QSqlTableModel> sqlModelOsirix(new QSqlTableModel(this,dbOsirix));
		sqlModelOsirix->setTable("zstudy");
		sqlModelOsirix->select();
		while(sqlModelOsirix->canFetchMore())
			sqlModelOsirix->fetchMore();
		int numRows = sqlModelOsirix->rowCount();
		tableWidget->setRowCount(numRows);

		int index_name                  = sqlModelOsirix->fieldIndex("ZNAME");
		int index_MRN                   = sqlModelOsirix->fieldIndex("ZPATIENTID");
		int index_study_desc            = sqlModelOsirix->fieldIndex("ZSTUDYNAME");
		int index_modality              = sqlModelOsirix->fieldIndex("ZMODALITY");
		int index_date                  = sqlModelOsirix->fieldIndex("ZDATE");
		int index_studyUID              = sqlModelOsirix->fieldIndex("ZSTUDYINSTANCEUID");

#if useDebug
		qDebug() << "Begin tableWidget Update";
#endif

	    QDateTime baseTime;
		for(int i=0;i<numRows;i++){
			baseTime = QDateTime(QDate(2001,01,01),QTime(0,0),Qt::UTC).toLocalTime();

			tableWidget->setItem(i,0, new QTableWidgetItem(sqlModelOsirix->index(i,index_name).data().toString()));
			tableWidget->setItem(i,1, new QTableWidgetItem(sqlModelOsirix->index(i,index_MRN).data().toString()));
			tableWidget->setItem(i,2, new QTableWidgetItem(sqlModelOsirix->index(i,index_study_desc).data().toString()));
			tableWidget->setItem(i,3, new QTableWidgetItem(sqlModelOsirix->index(i,index_modality).data().toString()));
			tableWidget->setItem(i,4, new QTableWidgetItem(baseTime.addSecs(sqlModelOsirix->index(i,index_date).data().toInt()).toString("yyyy-MM-dd hh:mm:ss")));
			tableWidget->setItem(i,6, new QTableWidgetItem(sqlModelOsirix->index(i,index_studyUID).data().toString()));

		}

		sqlModelOsirix->clear();
		sqlModelOsirix.reset();
		dbOsirix.close();

		// Done Using Osirix DB
		// Time to cross-reference with QuantX DB

		QSqlDatabase dbQI = QSqlDatabase::addDatabase(databaseSettingsQI[0],"QIsearchDatabaseInitializeTree");
		dbQI.setHostName(databaseSettingsQI[1]);
		dbQI.setDatabaseName(databaseSettingsQI[2]);
		dbQI.setUserName(databaseSettingsQI[3]);
		dbQI.setPassword(databaseSettingsQI[4]);
		ok = dbQI.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(this,"QuantX Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		}

		QString studyTableQI = "qi_studies";
		QSqlRecord studyRecordQI = dbQI.record(studyTableQI);
		studyRecordQI.setValue("study_uid","temp");
		for(int i=studyRecordQI.count()-1;i>=0;i--)
			if(studyRecordQI.isNull(i))
				studyRecordQI.remove(i);

		QString sqlStatement = dbQI.driver()->sqlStatement(QSqlDriver::SelectStatement,studyTableQI,studyRecordQI,false) + QString(' ')
		                     + dbQI.driver()->sqlStatement(QSqlDriver::WhereStatement,studyTableQI,studyRecordQI,true);
		QScopedPointer<QSqlQuery> sqlQueryQI(new QSqlQuery(dbQI));
		for(int i=0; i < numRows; ++i){
			sqlQueryQI->prepare(sqlStatement);
			sqlQueryQI->bindValue(0,tableWidget->model()->index(i,6).data().toString());
			ok = sqlQueryQI->exec();
			tableWidget->setItem(i,5, new QTableWidgetItem( sqlQueryQI->next() ? "YES" : "NO" ));
		}

		sqlQueryQI->finish();
		sqlQueryQI.reset();
		dbQI.close();

		//Done Using QuantX DB

		tableWidget->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));
		tableWidget->setHorizontalHeaderItem(1,new QTableWidgetItem("MRN"));
		tableWidget->setHorizontalHeaderItem(2,new QTableWidgetItem("Study Description"));
		tableWidget->setHorizontalHeaderItem(3,new QTableWidgetItem("Modality"));
		tableWidget->setHorizontalHeaderItem(4,new QTableWidgetItem("Date Acquired"));
		tableWidget->setHorizontalHeaderItem(5,new QTableWidgetItem("In QuantX DB"));
		tableWidget->setColumnHidden(6,true);

		tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
		tableWidget->setAutoScroll(true);
		tableWidget->setAutoScrollMargin(64);
	}

	QSqlDatabase::removeDatabase("OSIRIXsearchDatabaseInitializeTree");
	QSqlDatabase::removeDatabase("QIsearchDatabaseInitializeTree");
	tableWidget->setSortingEnabled(true);
	searchFilterUpdate();
}

void QiOsirixDatabaseWidget::importStudiesFromColumnSelection(){
	if(tableWidget->selectedRanges().isEmpty())
		return;
	else{
		QSqlDatabase dbOsirix = QSqlDatabase::addDatabase(databaseSettingsOSIRIX[0],"OSIRIXimageDatabaseInitializeTree");
		dbOsirix.setHostName(databaseSettingsOSIRIX[1]);
		dbOsirix.setDatabaseName(databaseSettingsOSIRIX[2]);
		dbOsirix.setUserName(databaseSettingsOSIRIX[3]);
		dbOsirix.setPassword(databaseSettingsOSIRIX[4]);
		bool ok = dbOsirix.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(this,"Osirix Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		}

		QProgressDialog progressDialog(this);
		progressDialog.setCancelButton(0);
		QLabel *progressLabel = new QLabel;
		progressLabel->setAlignment(Qt::AlignLeft);
		progressDialog.setFixedSize(400,102);
		progressDialog.setLabel(progressLabel);
		progressDialog.setLabelText("Importing Studies...\nPreparing Study List...");
		progressDialog.show();
		qApp->processEvents();

		QString studyTableOSIRIX = "zstudy";
		QSqlRecord studyRecordOSIRIX= dbOsirix.record(studyTableOSIRIX);
		QSqlRecord studyUIDRecordOSIRIX= dbOsirix.record(studyTableOSIRIX);
		studyUIDRecordOSIRIX.setValue("ZSTUDYINSTANCEUID","temp");
		for(int i=studyUIDRecordOSIRIX.count()-1;i>=0;i--)
			if(studyUIDRecordOSIRIX.isNull(i))
				studyUIDRecordOSIRIX.remove(i);

		QString sqlStatement = dbOsirix.driver()->sqlStatement(QSqlDriver::SelectStatement,studyTableOSIRIX,studyRecordOSIRIX,false) + QString(' ')
		                     + dbOsirix.driver()->sqlStatement(QSqlDriver::WhereStatement,studyTableOSIRIX,studyUIDRecordOSIRIX,true);
		QScopedPointer<QSqlQuery> sqlQueryOSIRIX(new QSqlQuery(dbOsirix));

		uint totalImages = 0;
		QVector<uint> studyPKs;
		QStringList patientNames;
		QStringList studyDescriptions;
		const auto ranges = tableWidget->selectedRanges();
		for(const auto & range : ranges){
			for(int i=range.topRow(); i<=range.bottomRow(); ++i){
				sqlQueryOSIRIX->prepare(sqlStatement);
				sqlQueryOSIRIX->bindValue(0,tableWidget->model()->index(i,6).data().toString());
				ok = sqlQueryOSIRIX->exec();
				//qDebug() << "Sql Query " << (ok ? "Success" : "Failed");
				sqlQueryOSIRIX->first();
				studyPKs << sqlQueryOSIRIX->value("Z_PK").toUInt();
				patientNames << sqlQueryOSIRIX->value("ZNAME").toString();
				studyDescriptions << sqlQueryOSIRIX->value("ZSTUDYNAME").toString();
				totalImages += sqlQueryOSIRIX->value("ZNUMBEROFIMAGES").toUInt();
			}
		}
		// Now we have all of the study UIDs, and the number of Images
		progressDialog.setLabelText("Importing Studies...\nPreparing Series List...");
		progressDialog.setMaximum(totalImages);
		uint progress = 0;
		QScopedPointer<QISql> qisql(new QISql(databaseSettingsQI));

		// Set up Series Records
		QString seriesTableOSIRIX = "zseries";
		QSqlRecord seriesRecordOSIRIX= dbOsirix.record(seriesTableOSIRIX);
		QSqlRecord seriesStudyRecordOSIRIX= dbOsirix.record(seriesTableOSIRIX);
		seriesStudyRecordOSIRIX.setValue("ZSTUDY",studyPKs.first());
		for(int i=seriesStudyRecordOSIRIX.count()-1;i>=0;i--)
			if(seriesStudyRecordOSIRIX.isNull(i))
				seriesStudyRecordOSIRIX.remove(i);

		sqlStatement = dbOsirix.driver()->sqlStatement(QSqlDriver::SelectStatement,seriesTableOSIRIX,seriesRecordOSIRIX,false) + QString(' ')
		             + dbOsirix.driver()->sqlStatement(QSqlDriver::WhereStatement,seriesTableOSIRIX,seriesStudyRecordOSIRIX,true);

		QVector<QStringList> seriesDescriptions;
		QVector<QVector<uint> > seriesPKs;
		QVector<QVector<uint> > seriesNumImages;
		for(int i=0; i<studyPKs.size(); ++i){
			sqlQueryOSIRIX->prepare(sqlStatement);
			sqlQueryOSIRIX->bindValue(0,studyPKs[i]);
			ok = sqlQueryOSIRIX->exec();
			QStringList seriesDescriptionsOneStudy;
			QVector<uint> seriesPKsOneStudy;
			QVector<uint> seriesNumImagesOneStudy;
			while(sqlQueryOSIRIX->next()){
				seriesDescriptionsOneStudy << sqlQueryOSIRIX->value("ZSERIESDESCRIPTION").toString();
				seriesPKsOneStudy << sqlQueryOSIRIX->value("Z_PK").toUInt();
				seriesNumImagesOneStudy << sqlQueryOSIRIX->value("ZNUMBEROFIMAGES").toUInt();
			}
			seriesDescriptions << seriesDescriptionsOneStudy;
			seriesPKs << seriesPKsOneStudy;
			seriesNumImages << seriesNumImagesOneStudy;
		}

		//Set Up Image Records
		QString imageTableOSIRIX = "zimage";
		QSqlRecord imageRecordOSIRIX= dbOsirix.record(imageTableOSIRIX);
		QSqlRecord imageSeriesRecordOSIRIX= dbOsirix.record(imageTableOSIRIX);
		imageSeriesRecordOSIRIX.setValue("ZSERIES",studyPKs.first());
		for(int i=imageSeriesRecordOSIRIX.count()-1;i>=0;i--)
			if(imageSeriesRecordOSIRIX.isNull(i))
				imageSeriesRecordOSIRIX.remove(i);

		sqlStatement = dbOsirix.driver()->sqlStatement(QSqlDriver::SelectStatement,imageTableOSIRIX,imageRecordOSIRIX,false) + QString(' ')
		             + dbOsirix.driver()->sqlStatement(QSqlDriver::WhereStatement,imageTableOSIRIX,imageSeriesRecordOSIRIX,true);

		uint imageNum;
		QFuture<int> sqlFuture;
		QString baseProgressText = QString("Importing Study %2/%1 : %3 - %4\n") +
		                                   "Series %6/%5 : %7\n" +
		                                   "Image %9/%8";
		baseProgressText = baseProgressText.arg(studyPKs.size());
		for(int i=0; i<seriesPKs.size(); ++i){
			QString studyProgressText = baseProgressText.arg(i+1).arg(patientNames[i]).arg(studyDescriptions[i]).arg(seriesPKs[i].size());
			for(int j=0; j<seriesPKs[i].size(); ++j){
				QString seriesProgressText = studyProgressText.arg(j+1).arg(seriesDescriptions[i][j]).arg(seriesNumImages[i][j]);
				imageNum = 0;
				sqlQueryOSIRIX->prepare(sqlStatement);
				sqlQueryOSIRIX->bindValue(0,seriesPKs[i][j]);
				ok = sqlQueryOSIRIX->exec();
				while(sqlQueryOSIRIX->next()){
					progressDialog.setLabelText(seriesProgressText.arg(++imageNum));
					QString filename = sqlQueryOSIRIX->value("ZPATHSTRING").toString();
					if(filename.isEmpty()){
						//File was copied to Osirix Database
						uint imageID = sqlQueryOSIRIX->value("ZPATHNUMBER").toUInt();
						uint imageFolder = ((imageID / 10000) + 1) * 10000;
						filename = QString(QDir::homePath() + "/Documents/Osirix Data/DATABASE.noindex/%1/%2.dcm").arg(imageFolder).arg(imageID);
					}
#if useDebug
					if(!(QFile(filename).exists()))
						qDebug() << "WARNING file is missing:" << filename;
#endif
					sqlFuture = QtConcurrent::run(qisql.data(), &QISql::addFile, filename, QMap<gdcm::Tag, QByteArray>());
					sqlFuture.waitForFinished();
					progressDialog.setValue(++progress);
					qApp->processEvents();
				}
			}
		}

		progressDialog.close();
		sqlQueryOSIRIX->finish();
		sqlQueryOSIRIX.reset();
		dbOsirix.close();
	}
	QSqlDatabase::removeDatabase("OSIRIXimageDatabaseInitializeTree");
	initializeTableWidget();
	emit casesImported();
}

void QiOsirixDatabaseWidget::searchFilterUpdate(){
	QString name = nameLineEdit->text();
	QString mrn  = mrnLineEdit->text();
	for(int i=0; i<tableWidget->rowCount(); i++){
		if(tableWidget->item(i,0)->text().contains(name,Qt::CaseInsensitive))
			tableWidget->showRow(i);
		else
			tableWidget->hideRow(i);
	}
	if(!mrn.isEmpty())
		for(int i=0; i<tableWidget->rowCount(); i++)
			if(!tableWidget->item(i,1)->text().contains(mrn,Qt::CaseInsensitive))
				tableWidget->hideRow(i);
}

void QiOsirixDatabaseWidget::sortByColumn(int c, Qt::SortOrder s)
{
	tableWidget->sortByColumn(c,s);

}

#endif // Q_OS_MAC
