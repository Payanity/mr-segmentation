#ifndef QI3DUSIMAGE_H
#define QI3DUSIMAGE_H

#include "qi3dimage.h"

/*!
 * \brief Class for interacting with, and storing and retrieving, 3D ultrasound images.
 * \ingroup SQLModule
 * \class Qi3DUSImage
 */
class QISQL_EXPORT Qi3DUSImage : public Qi3DImage
{
public:
    Qi3DUSImage();
    int openSeries(QString uid, const QStringList &dbSettings);

protected:
    QString viewPosition; //!< Stores view position of the image.
    QString imageLaterality; //!< Stores image laterality of the image.
    QString patientOrientation; //!< Stores the patient orientation.
    QString photometricInterpretation; //!< Stores the photometric interpretation of the image.


private:
    int getSeriesData(const QStringList &dbSettings);

};

#endif // QI3DUSIMAGE_H
