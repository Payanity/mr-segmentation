#include "sqldatabaseservice.h"

#include <algorithm>
#include <functional>

#include <QSettings>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QtConcurrent>
#include <QSqlDriver>
#include <QStandardItemModel>
#include <utility>
#include <iomanip>

#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/backend/Postgres.h>
#include <Wt/Dbo/backend/Sqlite3.h>

#include "model/acquisitionprotocol.h"
#include "model/auditlog.h"
#include "model/dicomdata.h"
#include "model/images2d.h"
#include "model/lesion.h"
#include "model/lesiondetails.h"
#include "model/motioncorrection.h"
#include "model/overlay.h"
#include "model/patient.h"
#include "model/plugindata.h"
#include "patientmodel.h"
#include "treeitem.h"
#include "model/dicomseries.h"
#include "model/seriesthumbnails.h"
#include "model/dicomstudy.h"
#include "model/useraccess.h"
#include "qimriimage.h"
#include "qi3dusimage.h"
#include "qi2dimage.h"
#include "qi3dmgimage.h"
#include "sqlseriesdeletewidget.h"
#include "overlayinfo.h"
#include "databasehelper.h"
#include "plugininfo.h"
#include "acquisitionprotocolinfo.h"
#include <utility>
#include <vector>

constexpr static auto QUANTX_CURRENT_DATABASE_VERSION = 320;

#define useDebug 0
#define showPatientTreeview 0

#if useDebug
#include <QImageWriter>
#include <QDebug>
#include <QtWidgets/QMessageBox>
#include <functional>
#endif

#if showPatientTreeview
#include <QTreeView>
#include <QApplication>
#include "patientmodeldelegate.h"
#endif

using namespace quantx::sql;
using namespace quantx::patient;
using namespace quantx::auditlog;
using namespace quantx::dicom;
using namespace quantx::mranalysis;
using namespace quantx::overlay;
using namespace quantx::acquisition;

#define useConcurrency 1

#define useTestDBSettings 0

#if useTestDBSettings
#define testDBDriver "QPSQL"
//#define testDBName "C:\\QuantX\\blankSqlStatements.db"
//#define testDBName "C:\\QuantX\\quantx_Nates_Copy.db"
#define testDBName "quantx"
#define testDBHostName "localhost"
#define testDBPort "5432"
#define testDBUserName "postgres"
#define testDBPassword "postgres"
#endif

namespace TableNames {
    constexpr const auto acquisitionProtocol = "qi_acquisition_protocols";
    constexpr const char* auditLog = "qi_auditlog";
    constexpr const char* dicomData = "dicom_data";
    constexpr const char* images2d = "qi_2d_images";
    constexpr const char* lesions = "qi_lesions";
    constexpr const char* lesionDetails = "qi_lesiondetails";
    constexpr const char* motionCorrection = "qi_motion_correction";
    constexpr auto overlay = "qi_overlay";
    constexpr const char* patients = "qi_patients";
    constexpr auto plugins = "qi_plugin_data";
    constexpr const char* series = "qi_series";
    constexpr const char* seriesThumbnails = "qi_series_thumbnails";
    constexpr const char* studies = "qi_studies";
    constexpr const char* userAccess = "qi_useraccess";
} // TableNames

// Try catch function
//!
//! \brief Helper function for handling Wt::Dbo function calls
//! \param error The Error object
//! \param exceptionMsg The type of function call (ie. "initializeConnection, queryDatabaseVersion, etc.)
//!
//! \prePost{ A SqlDatabaseServiceError message is set.}
//!
void handleException( SqlDatabaseServiceError* error, const QString& exceptionMsg = "" )
{
	try {
		throw; // Rethrows last exception
	} catch( const Wt::Dbo::NoUniqueResultException& e ) {
		error->bSuccess = false;
		error->setMsg( exceptionMsg + ": " + "No Unique Result - " + e.what() + " - Code:" + e.code().c_str() );
	} catch( const Wt::Dbo::Exception& e ) {
		error->bSuccess = false;
		error->setMsg( exceptionMsg + ": " + e.what() + " - Code:" + e.code().c_str() );
	} catch( const std::exception& e ) {
		error->bSuccess = false;
		error->setMsg( exceptionMsg + ": " + e.what() );
	} catch( ... ) {
		error->bSuccess = false;
		error->setMsg( exceptionMsg + ": Unknown Exception, Rethrow" );
		throw;
	}
}

//!
//! \brief Helper function to handle an error from the Qt Sql Module
//! \param error The Error Object
//! \param msg The type of function call (ie. "initializeConnection, queryDatabaseVersion, etc.)
//! \param sqlError Qt Sql's error class
//!
//! \prePost{ A SqlDatabaseServiceError message is set.}
//!
void handleQSqlError( SqlDatabaseServiceError* error, const QString& msg, const QSqlError& sqlError )
{
	error->bSuccess = false;
	error->setMsg( msg + " -- Error Text: " + sqlError.text() +
	               " -- Native Error Code:" + sqlError.nativeErrorCode() );
}

template<class T>
bool dicomDateTimeLessForwarded( const Wt::Dbo::ptr<T>& lhs, const Wt::Dbo::ptr<T>& rhs, const Wt::WDate & d, const Wt::WTime & t)
{
	return Wt::WDateTime{lhs.get()->date.value_or(d), lhs.get()->time.value_or(t)} <
	       Wt::WDateTime{rhs.get()->date.value_or(d), rhs.get()->time.value_or(t)};
}

template<class T>
bool dicomDateTimeGreaterForwarded( const Wt::Dbo::ptr<T>& lhs, const Wt::Dbo::ptr<T>& rhs, const Wt::WDate& d, const Wt::WTime& t )
{
	return Wt::WDateTime{lhs.get()->date.value_or(d), lhs.get()->time.value_or(t)} >
	       Wt::WDateTime{rhs.get()->date.value_or(d), rhs.get()->time.value_or(t)};
}

template<class T>
bool dicomDateTimeGreater( const Wt::Dbo::ptr<T>& lhs, const Wt::Dbo::ptr<T>& rhs) {
	return dicomDateTimeGreaterForwarded(lhs, rhs, Wt::WDate(1,1,1), Wt::WTime{0, 0, 0});
}

template<class T>
bool dicomDateTimeLess( const Wt::Dbo::ptr<T>& lhs, const Wt::Dbo::ptr<T>& rhs) {
	return dicomDateTimeLessForwarded(lhs, rhs, Wt::WDate::currentServerDate().addDays(1), Wt::WTime{23, 59, 59, 999});
}

// pImpl class
class SqlDatabaseServiceImpl
{
public: // Vars
	std::unique_ptr<Wt::Dbo::SqlConnection> connection;
	Wt::Dbo::Session session;
};

//!
//! \ctorBrief{Constructor for the Database Service Error}
//!
//! \ctorPrePost
//!
SqlDatabaseServiceError::SqlDatabaseServiceError()
    :bSuccess( true )
{}

//!
//! \brief Sets the error message and outputs to debug if possible
//! \param msg
//!
//! \preInitPost{The error message is set.}
//!
void SqlDatabaseServiceError::setMsg( const QString& msg )
{
	msg_ = msg;
#if useDebug
	qDebug() << msg_;
#endif
}

//!
//! \brief Returns the error message
//! \return
//!
//! \preInitPost{Error msg is returned from object.}
//!
QString SqlDatabaseServiceError::msg() const
{
	return msg_;
}

//!
//! \ctorBrief{Reads in the database settings}
//! \param dataSettings The Setttings file that contains the Database Settings.
//!
//! \ctorPrePost
//!
SqlDatabaseService::SqlDatabaseService( const QSettings& dataSettings )
    : impl( std::make_unique<SqlDatabaseServiceImpl>() ),
      databaseVersionId( 0 ), originalDbSettings_{dataSettings}
{
#if useTestDBSettings
	Q_UNUSED( dataSettings );
	databaseSettings_.setDatabaseDriverType( testDBDriver );
	databaseSettings_.setDatabaseName( testDBName );
	databaseSettings_.setHostName( testDBHostName );
	databaseSettings_.setPort( testDBPort );
	databaseSettings_.setUserName( testDBUserName );
	databaseSettings_.setPassword( testDBPassword );
#else
	databaseSettings_.readFromSettings( dataSettings );
#endif
}

//!
//! \brief Default Constructor
//!
//! \preInitPost{The Object is destroyed and memory cleaned up.}
//!
//! \note See here for a good explanation to why we need to define the destructor (as opposed to the compiler defining it)
//! and default it in the .cpp file:
//! https://www.fluentcpp.com/2017/09/22/make-pimpl-using-unique_ptr/
//! tl;dr ~~ unique_ptr's destructor checks if the pImpl type is defined and will not compile if it is not -
//!			Compiler inlines the destructor if it defines it, so it is in header file
//!			We need to define the constructor and put it after the pImpl class definition
//!
SqlDatabaseService::~SqlDatabaseService() = default;

//!
//! \brief Initializes the whole Service
//! \return Error Object
//!
//! Calls:
//! 1. initializeConnection
//! 2. checkDatabaseVersionAndUpradeIfNecessary
//! 3. initializeSession
//! 4. mapClassesToTables
//!
//! \preInitPost{The Service will be initialized and ready to use.}
//!
SqlDatabaseServiceError SqlDatabaseService::initialize(bool useResearchLicenseMode)
{
	useResearchLicenseMode_ = useResearchLicenseMode;

	QVector<std::function<SqlDatabaseServiceError( void )>> initializeFunctionList = {
	    // Remember that order matters!
	    std::bind( &SqlDatabaseService::initializeConnection, this ),
	    std::bind( &SqlDatabaseService::checkDatabaseVersionAndUpradeIfNecessary, this ),
	    std::bind( &SqlDatabaseService::initializeSession, this ),
	    std::bind( &SqlDatabaseService::mapClassesToTables, this )
    };

	SqlDatabaseServiceError error;
	for( auto func : initializeFunctionList )
	{
		error = func();
		if( !error.bSuccess )
		{
			break;
		}
	}

	initializeDescriptionMappings();

	return error;
}

/*!
 * \brief Finds all patient records, and returns them, but shallowly-- in other words, without a deep load of all patient model data.
 * \return Pointer to the QStandardItemModel of the shallowly loaded patients.
 */
std::unique_ptr<QStandardItemModel, decltype(&QScopedPointerDeleteLater::cleanup) > SqlDatabaseService::findAllPatientsShallow()
{
#if useDebug
	qDebug() << "findAllPatientsShallow";
#endif
	//auto my_lambda = [](QStandardItemModel* p) { if(p) p->deleteLater(); };
	std::unique_ptr<QStandardItemModel, decltype(&QScopedPointerDeleteLater::cleanup) > viewListPatients(new QStandardItemModel(), &QScopedPointerDeleteLater::cleanup );
	//viewListPatients.reset(new QStandardItemModel);

	Wt::Dbo::Transaction transaction { impl->session };
	Wt::Dbo::collection< Wt::Dbo::ptr<Patient> > patients;

	SqlDatabaseServiceError error;

	try {
		patients = transaction.session().find<Patient>().orderBy("mrn");
	} catch ( ... ) {
		handleException(&error, "Error finding all patients.");
	}

	for (const auto& patient : patients) {
#if useDebug
		qDebug() << "------------------------   Found Patient   ------------------------";
		qDebug() << "Patient MRN:" << patient->mrn.c_str();
#endif
		std::vector<Wt::Dbo::ptr<DicomStudy> > studies(patient->studies.begin(), patient->studies.end());
#if useDebug
		qDebug() << "Number of Studies:" << studies.size();
		if(studies.size() == 0){
			qDebug() << "Not adding patient to list!";
		}
#endif
		if(studies.size() > 0){
			std::sort(studies.begin(), studies.end(), dicomDateTimeGreater<DicomStudy>);
			auto studyFront = studies.front();

#if useDebug
			qDebug() << "Study MRN:" << studyFront->mrn->c_str();
			qDebug() << "Study Accession Number:" << studyFront->accessionNumber->c_str();
#endif

			viewListPatients->appendRow(QList<QStandardItem *> {
			                                new QStandardItem { QString::fromStdString(patient->name.value()) },
			                                new QStandardItem { QString::fromStdString(patient->mrn) },
			                                new QStandardItem { QDate::fromJulianDay( studyFront->date.value().toJulianDay() ).toString() } ,
			                                new QStandardItem { QString::fromStdString(studyFront->accessionNumber.value_or(std::string())) },
			                                new QStandardItem { QString::fromStdString(patient->sex.value()) }
			                            });
		}
	}

#if useDebug
	qDebug() << "Scanned" << patients.size() << "patients";
#endif

	return viewListPatients;
}

/*!
 * \brief Can be registered to listen to DB settings changes.
 * \param databaseSettings The new DB settings to use.
 */
void SqlDatabaseService::onDatabaseSettingsChanged( const DatabaseSettings& databaseSettings )
{
	databaseSettings_ = databaseSettings;
}

/*!
 * \brief SqlDatabaseService::rebuildIndexes
 *
 * This slot is meant to rebuild db indexes for sqlite... it may be deprecated soon however
 */
void SqlDatabaseService::rebuildIndexes()
{
	if(databaseSettings_.getDBSettingsList().first() == "QSQLITE"){
		QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE","RebuildDBIndex");
		db.setDatabaseName(databaseSettings_.getDBSettingsList().at(2));
		db.open();
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		QFile commandFile(":/sql/sqliteIndex");
		QStringList commands;
		commandFile.open(QIODevice::ReadOnly);
		while(!commandFile.atEnd())
			commands << commandFile.readLine();
		commandFile.close();
		for(const auto & command : std::as_const(commands))
			sqlQuery->exec(command);
		sqlQuery.reset();
		db.close();
	}
	QSqlDatabase::removeDatabase("RebuildDBIndex");
}

/*!
 * \brief Initializes the series description mappings.
 */
void SqlDatabaseService::initializeDescriptionMappings()
{
	seriesUtils.setT2SeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/T2").toStringList());
	seriesUtils.setDynSeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/DCE").toStringList());
	seriesUtils.setDynMultiSeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/DCEmultipleSeries").toStringList());
	seriesUtils.setAdcSeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/ADC").toStringList());
	seriesUtils.setDwiSeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/DWI").toStringList());
	seriesUtils.setMcT2SeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/MotionCorrectedT2").toStringList());
	seriesUtils.setMcDynSeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/MotionCorrectedDCE").toStringList());
	seriesUtils.setMcAdcSeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/MotionCorrectedADC").toStringList());
	seriesUtils.setMcDwiSeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/MotionCorrectedDWI").toStringList());
	seriesUtils.setFastDynamicSeriesDesc(originalDbSettings_.value("MRIseriesDescriptions/FastDynamicSeries").toStringList());
	int philipsPre = 1;
	if (useResearchLicenseMode_){
		philipsPre = originalDbSettings_.value("Research/PhilipsNumPre", 1).toInt();
	}
	seriesUtils.setPhilipsResearchNumPre(philipsPre);
}

//!
//! \brief Creates the specific Sql Database Driver Connection and intializes it.
//! \return Error Object
//!
//! \preInitPost{The Connection is initialized.}
//!
SqlDatabaseServiceError SqlDatabaseService::initializeConnection()
{
	SqlDatabaseServiceError error;

	auto databaseDriver = databaseSettings_.databaseDriverType();
	try {
		if( databaseDriver == "QPSQL" ) {
			sqlStatements = std::make_unique<SqlDatabaseServiceStatementsPostgres>();
			auto params = "host="     + databaseSettings_.hostName() +
			             (databaseSettings_.port().isEmpty() ? "" : (" port="     + databaseSettings_.port()) ) +
			             " dbname="   + databaseSettings_.databaseName() +
			             " user="     + databaseSettings_.userName() +
			             " password=" + databaseSettings_.password();
			impl->connection = std::make_unique<Wt::Dbo::backend::Postgres>( params.toStdString() );
		} else if( databaseDriver == "QSQLITE" ) {
			sqlStatements = std::make_unique<SqlDatabaseServiceStatementsSqlite>();
			impl->connection = std::make_unique<Wt::Dbo::backend::Sqlite3>( databaseSettings_.databaseName().toStdString() );
		} else {
			error.bSuccess = false;
			error.setMsg( "Database Connection Error: SQL Driver Type \"" + databaseDriver + "\" is not currently supported!" );
		}
#if useDebug
		impl->connection->setProperty( "show-queries", "true" );
#endif
	} catch( ... ) {
		handleException( &error, "Database Connection Error -- SQL Driver Type " + databaseDriver );
	}

	return error;
}

//!
//! \brief Check the current database version and if it is out of date, upgrade the database.
//! \return Error Object
//!
//! \preInitPost{The database version is checked, and if necessary, it is updated to the current version.}
//!
SqlDatabaseServiceError SqlDatabaseService::checkDatabaseVersionAndUpradeIfNecessary()
{
	SqlDatabaseServiceError error;

	int databaseVersion = 0;

	constexpr const char* connectionName = "check_and_upgrade_db_version";
	{
	auto db = QSqlDatabase::addDatabase( databaseSettings_.databaseDriverType(), connectionName );
	if( connectAndOpenQDatabase( &db ) ) {
		if( db.transaction() ){
			QSqlQuery versionQuery( db );
			if( versionQuery.exec( sqlStatements->currentDatabaseVersion() ) ) {
				if ( versionQuery.last() ) {
					databaseVersion = versionQuery.value( "version" ).toInt();
				databaseVersionId = versionQuery.value( "idqi_version" ).toInt();
				}
			} else {
				if( db.rollback() ) {
					handleQSqlError( &error, "Check Database Version Error: " + versionQuery.lastQuery(), versionQuery.lastError() );
				} else {
					handleQSqlError( &error, "Check DbVersion Rollback Error", db.lastError() );
				}
			}
			if( error.bSuccess ) {
				if( !db.commit() ) {
					handleQSqlError( &error, "Check DbVersion Commit Error", db.lastError() );
				}
			}
		} else {
			handleQSqlError( &error, "Check DbVersion Transaction Error", db.lastError() );
		}
	} else {
		handleQSqlError( &error, "Database Connection Error", db.lastError() );

	}
#if useDebug
	qDebug() << "Most Recent Database Version:" << QUANTX_CURRENT_DATABASE_VERSION << "vs Current Database Version:" << databaseVersion;
#endif

	db.close();
	}
	QSqlDatabase::removeDatabase( connectionName );

	if(databaseVersion == 0)
		return SqlDatabaseServiceError();

	if( error.bSuccess) {
		if( QUANTX_CURRENT_DATABASE_VERSION != databaseVersion ){
			error = updateDatabase( databaseVersion );
		}
	}

	return error;
}

//!
//! \brief Initializes the session to use Wt::Dbo
//! \return Error Object
//!
//! \preInitPost{Session becomes initialized.}
//!
SqlDatabaseServiceError SqlDatabaseService::initializeSession()
{
	SqlDatabaseServiceError error;
	try {
		impl->session.setConnection( impl->connection->clone() );
	} catch ( ... ) {
		handleException( &error, "Database Session Error" );
	}

	return error;
}

//!
//! \brief Maps the model classes to the actual database Tables.
//! \return Error Object
//!
//! \preInitPost{ Classes are mapped and ready to use. }
//!
SqlDatabaseServiceError SqlDatabaseService::mapClassesToTables()
{
	SqlDatabaseServiceError error;
	try {
		impl->session.mapClass<AcquisitionProtocol>( TableNames::acquisitionProtocol );
		impl->session.mapClass<AuditLog>( TableNames::auditLog );
		impl->session.mapClass<DicomData>( TableNames::dicomData );
		impl->session.mapClass<Images2D>( TableNames::images2d );
		impl->session.mapClass<Lesion>( TableNames::lesions );
		impl->session.mapClass<LesionDetails>( TableNames::lesionDetails );
		impl->session.mapClass<MotionCorrection>( TableNames::motionCorrection );
		impl->session.mapClass<DbOverlay>( TableNames::overlay );
		impl->session.mapClass<Patient>( TableNames::patients );
		impl->session.mapClass<PluginData>( TableNames::plugins );
		impl->session.mapClass<DicomSeries>( TableNames::series );
		impl->session.mapClass<SeriesThumbnails>( TableNames::seriesThumbnails );
		impl->session.mapClass<DicomStudy>( TableNames::studies );
		impl->session.mapClass<UserAccess>( TableNames::userAccess );


#if useDebug
	//qDebug() << "SQL for Table Creation: " << endl << impl->session.tableCreationSql().c_str();
	qDebug() << "SQL Database Service: Classes Mapped To Tables";
#endif
	} catch ( ... ) {
		handleException( &error, "Mapping Classes Error" );
	}

	return error;
}

//!
//! \brief Sets the Qt Sql Database object with the correct settings
//! \param db
//! \return True if it opened, false if it did not.
//!
//! \preInitPost{The Qt Database Object is set-up and opened.}
//!
bool SqlDatabaseService::connectAndOpenQDatabase( QSqlDatabase* db )
{
	db->setDatabaseName( databaseSettings_.databaseName() );
	db->setHostName( databaseSettings_.hostName() );
	if(!databaseSettings_.port().isEmpty()) {
		db->setPort( databaseSettings_.port().toInt() );
	}
	db->setUserName( databaseSettings_.userName() );
	db->setPassword( databaseSettings_.password() );

	return db->open();
}

//!
//! \brief Master Update Version.  This delegates at what version to start updating the database.
//! \param databaseVersion The currently read database's version
//! \return Error Object
//!
//! \preInitPost{The Database has been updated to the current version.}
//!
SqlDatabaseServiceError SqlDatabaseService::updateDatabase( int databaseVersion )
{
	SqlDatabaseServiceError error;
	emit updatingDatabaseBegin();

	std::vector < std::pair< int, const std::function<QStringList()> > > versions {
		{  1, [this](){return sqlStatements->updateFromVer0ToVer1();}},
		{101, [this](){return sqlStatements->updateFromVer1ToVer101();}},
		{110, [this](){return sqlStatements->updateFromVer101ToVer110();}},
		{200, [this](){return sqlStatements->updateFromVer110ToVer200();}},
		{201, [this](){return sqlStatements->updateFromVer200ToVer201();}},
		{202, [this](){return sqlStatements->updateFromVer201ToVer202();}},
		{300, [this](){return sqlStatements->updateFromVer202ToVer300();}},
		{301, [this](){return sqlStatements->updateFromVer300ToVer301();}},
		{302, [this](){return sqlStatements->updateFromVer301ToVer302();}},
		{303, [this](){return sqlStatements->updateFromVer302ToVer303();}},
		{304, [this](){return sqlStatements->updateFromVer303ToVer304();}},
		{310, [this](){return sqlStatements->updateFromVer304ToVer310();}},
		{320, [this](){return sqlStatements->updateFromVer310ToVer320();}}
	};

	for(auto version : versions){
		if(databaseVersion < version.first){
			error = updateDatabaseHelper(databaseVersion, version.first, version.second());

			// Special rules for certain database versions
			if(error.bSuccess){
				if(databaseVersion == 200) {
					error = updateFromVer200ToVer201();
				}
			}

			if( !error.bSuccess ) {
				break;
			}
			databaseVersion = version.first;
		}
	}

	emit updatingDatabaseFinish();

	return error;
}

SqlDatabaseServiceError SqlDatabaseService::updateDatabaseHelper(int prevDbVersion, int newDbVersion, QStringList statements)
{
	auto errorFuture = QtConcurrent::run(this, &SqlDatabaseService::updateFromVerXToVerY, prevDbVersion, newDbVersion, statements );
	while(!errorFuture.isFinished())
		qApp->processEvents();
	return errorFuture.result();
}

//!
//! \brief Extra functions to update the DB from version 200 to version 201.
//! \return Error Object
//!
//! \preInitPost{The Database is updated to version 201.}
//!
SqlDatabaseServiceError SqlDatabaseService::updateFromVer200ToVer201()
{

	SqlDatabaseServiceError error;

	if( error.bSuccess ) {
		const QString connectionName = "update_200_to_201_extra";
		auto db = QSqlDatabase::addDatabase( databaseSettings_.databaseDriverType(), connectionName );
		if( this->connectAndOpenQDatabase( &db ) ) {
			QSqlQuery query( db );
			if( query.exec( sqlStatements->studyList() ) ) {
				while( query.next() ) {
					if( !error.bSuccess ) {
						break;
					}
					QSqlQuery headerQuery( db );
#if useDebug
					qDebug() << "Executing SQL Statement:";
					qDebug() << "----------------------------------------------------------------------------------------------------------------------------------";
					qDebug() << sqlStatements->dicomHeader() + ( QString( R"( WHERE "0020,000d" = '%1')" ).arg(query.value( "study_uid" ).toString() ) + " LIMIT 1" );
					qDebug() << "----------------------------------------------------------------------------------------------------------------------------------";
#endif
					if ( headerQuery.exec( ( sqlStatements->dicomHeader() + ( QString( R"( WHERE "0020,000d" = '%1')" ).arg(query.value( "study_uid" ).toString() ) + " LIMIT 1" ) ) ) ) {
						headerQuery.first();
#if useDebug
						auto headerValue = headerQuery.value( "0008,0018" ).toString();
						qDebug() << "Query Value:" << headerValue;
						qDebug() << "Last Executed Query:" << headerQuery.executedQuery();
						qDebug() << "Is Active:" << headerQuery.isActive();
						qDebug() << "Is Select:" << headerQuery.isSelect();
						qDebug() << "Has Query Size Feature:" << headerQuery.driver()->hasFeature( QSqlDriver::DriverFeature::QuerySize );
						qDebug() << "Count:" << headerQuery.size();
						auto dicomFields = qSqlExtractAllDicomFields( headerValue, &error );
#else
						auto dicomFields = qSqlExtractAllDicomFields( headerQuery.value( "0008,0018" ).toString(), &error );
#endif
						if( error.bSuccess ) {
							QSqlQuery updateQuery( db );
							updateQuery.prepare( R"(UPDATE "qi_studies"
							                     SET accession_number = ?
							                     WHERE "idqi_studies" = ?)");
							updateQuery.addBindValue( dicomFields.value( "0008,0050" ) );
							updateQuery.addBindValue( query.value( "idqi_studies" ) );
							if( !updateQuery.exec()) {
								handleQSqlError( &error, "Update " + connectionName, updateQuery.lastError() );
							}
						}
					} else {
						handleQSqlError( &error, "Header Query " + connectionName, headerQuery.lastError() );
					}
				}
			} else {
				handleQSqlError( &error, "Query Error: " + connectionName, query.lastError() );
			}
		} else {
			handleQSqlError( &error, connectionName, db.lastError() );
		}
	}

	return error;
}

/*!
 * \brief extraSqliteStatements
 * \param db
 * \note This is probably only required for the update from version 202 to version 300, but should be quick
 * \return
 */
QStringList extraSqliteStatements(QSqlDatabase& db)
{
	QSqlQuery query( db );
	QStringList sqlStatementList;

	struct columnSearchParams{
		QString table;
		QString column;
		bool rename;
		QString newColumnNameOrType;
	};

	std::vector<columnSearchParams> columnList;

	//columnList.push_back({"qi_images_mr", "magnetic_field_strength", false, "real DEFAULT NULL"});
	columnList.push_back({"qi_images", "sliceLocation", true, "slice_location"});

	for(auto & c : columnList){
		query.exec(QString(R"(select COUNT(*) FROM pragma_table_info("%1") WHERE name = "%2" ;)").arg(c.table, c.column));
		query.first();
#if useDebug
		        qDebug() << "COLUMN" << c.column << "IN" << c.table << (query.value(0).toInt() ? "ALREADY EXISTS" : "DOES NOT EXIST") << (query.value(0).toBool() == c.rename ? ": BAD" : ": GOOD");
#endif
		if(query.value(0).toBool() == c.rename){
			if(c.rename){
				sqlStatementList.append(QString(R"(ALTER TABLE "%1" RENAME COLUMN "%2" TO "%3")").arg(c.table, c.column, c.newColumnNameOrType));
			} else {
				sqlStatementList.append(QString(R"(ALTER TABLE "%1" ADD COLUMN "%2" %3)").arg(c.table, c.column, c.newColumnNameOrType));
			}
		}
	}

	return sqlStatementList;
}

//!
//! \brief Generic update function. Updates the DB from version X to version Y.
//! \return Error Object
//!
//! \preInitPost{The Database is updated to version Y.}
//!
SqlDatabaseServiceError SqlDatabaseService::updateFromVerXToVerY( const int verX, const int verY, QStringList sqlStatementList )
{
	SqlDatabaseServiceError error;

	const QString connectionName = QString( "Update from %1 to %2" ).arg( verX ).arg( verY );
	{
		auto db = QSqlDatabase::addDatabase( databaseSettings_.databaseDriverType(), connectionName );
		if( connectAndOpenQDatabase( &db ) )
		{
			if( db.transaction() ) {
				QSqlQuery query( db );
				// some of our SQLite databases are missing this column
				if(databaseSettings_.databaseDriverType() == "QSQLITE") {
					for(auto & extraSqliteStatement : extraSqliteStatements(db)){
						sqlStatementList.prepend(extraSqliteStatement);
					}
				}
				sqlStatementList << sqlStatements->updateToVersion( ++databaseVersionId, verY );
				for( auto statement : sqlStatementList ) {
#if useDebug
					qDebug() << "Executing SQL Statement:";
					qDebug() << "----------------------------------------------------------------------------------------------------------------------------------";
					qDebug() << statement.simplified().toUtf8();
					qDebug() << "----------------------------------------------------------------------------------------------------------------------------------";
#endif
					if(!query.exec(statement.simplified())){
						if( db.rollback() ) {
							handleQSqlError( &error, connectionName + " Execution Error: " + query.lastQuery(), query.lastError() );
						} else {
							handleQSqlError( &error, connectionName + " Rollback Error", query.lastError() );
						}
						break;
					}
				}

				if( error.bSuccess ) {
					if( !db.commit() ) {
						handleQSqlError( &error, connectionName + " Commit Error", db.lastError() );
					}
				}
			} else {
				handleQSqlError( &error, connectionName + " Transaction Error", db.lastError() );
			}
		}
		else
		{
			handleQSqlError( &error, connectionName + " Database Open Error", db.lastError() );
		}
	}
	QSqlDatabase::removeDatabase( connectionName );
	return error;
}

/*!
 * \brief Finds a patient given an MRN.
 * \param mrn The MRN (Medical Record Number) to query on.
 * \return A Wt::Dbo::ptr<Patient> representing the patient record from the DB.
 */
Wt::Dbo::ptr<Patient> SqlDatabaseService::findPatient(QString mrn)
{
	SqlDatabaseServiceError error;
	Wt::Dbo::Transaction transaction { impl->session };
	Wt::Dbo::ptr<Patient> ptrPatient;

	try {
		ptrPatient = transaction.session().find<Patient>().where("mrn = ?").bind(mrn.toStdString()).resultValue();
	} catch ( ... ) {
		handleException(&error, "Error finding patient for MRN.");
	}

	return ptrPatient;
}

bool sortOverlays( const Wt::Dbo::ptr<DbOverlay>& first, const Wt::Dbo::ptr<DbOverlay>& second )
{
	return DatabaseHelper::dateTimeFromSessionId( first->sessionId.c_str() ) < DatabaseHelper::dateTimeFromSessionId( second->sessionId.c_str() );
}

/*!
 * \brief Populates a patient model for a given MRN.
 * \param mrn MRN of the patient to populate the model with.
 * \param model Pointer to model instance.
 * \param patientLevelOnly Whether we want to go only as deep as the patient, or also include all the images and so on as well.
 */
void SqlDatabaseService::populatePatientModelForMRN(QString mrn, quantx::patient::PatientModel *model,
                                                    bool patientLevelOnly)
{
	model->beginPopulateModel(this);

	if (model->hasChildren() && (mrn != model->patientMrn())){ //remove old data in model
		model->getRootItem(this)->deleteChildren();
	}
	auto dboPatient = findPatient(mrn);
	if (!dboPatient){
#if useDebug
		qDebug() << "Patient not found, " << mrn;
#endif
		model->endPopulateModel(this);
		return;
	}
#if useDebug
	qDebug() << QString("Query success! Patient name: %1").arg(QString::fromStdString(dboPatient->name.value()));
#endif
	QVector<TreeItem*> parents;
	parents << model->getRootItem(this);
	QDate dob;
	if (dboPatient->dateOfBirth.has_value()){
		dob = QDate::fromJulianDay(dboPatient->dateOfBirth->toJulianDay());
	}
	parents.last()->appendChild(new TreeItem({QString::fromStdString(dboPatient->mrn),
	                                          QString::fromStdString(dboPatient->name.value()),
	                                          dob,
	                                          QString::fromStdString(dboPatient->sex.value()), QString(), QString(), QString(), QString()}, parents.last()));
	parents << parents.last()->child(parents.last()->childCount()-1);
	if (patientLevelOnly){
		model->endPopulateModel(this);
		return;
	}

	SqlDatabaseServiceError error;
	Wt::Dbo::Transaction transaction { impl->session };

	std::vector<Wt::Dbo::ptr<DicomStudy> > studies(dboPatient->studies.begin(), dboPatient->studies.end());
	std::sort(studies.begin(), studies.end(), dicomDateTimeGreater<DicomStudy>);

	for (const auto& study : studies){
#if useDebug
		qDebug() << QString("Study: %1").arg(QString::fromStdString(study->studyuid.value()));
		qDebug() << "\tStudy UID:" << study->studyuid.value().c_str();
		qDebug() << "\tDate:" << study->date->toString().value();
		qDebug() << "\tTime has value? " << study->time.has_value();
		if (study->time.has_value()){
			qDebug() << "\tTime: " << study->time.value().toString().value();
		}
		qDebug() << "\tAccession:" << study->accessionNumber.value_or(std::string()).c_str();
		qDebug() << "\tModality: " << study->modality.value_or(std::string()).c_str();
#endif
		parents.last()->appendChild(new TreeItem({QString::fromStdString(study->studyuid.value()),
		                                          study->date.has_value() ? QDate::fromJulianDay(study->date.value().toJulianDay()) : QDate{},
		                                          study->time.has_value() ? QTime::fromMSecsSinceStartOfDay(study->time.value().toTimeDuration().count()) : QTime{},
		                                          QString::fromStdString(study->accessionNumber.value_or(std::string())),
		                                          QString::fromStdString(study->modality.value_or(std::string())), QString(), QString(), QString()}, parents.last()));

		parents << parents.last()->child(parents.last()->childCount()-1);

		std::vector<Wt::Dbo::ptr<DicomSeries> > series(study->series.begin(), study->series.end());
		std::sort(series.begin(), series.end(), dicomDateTimeLess<DicomSeries>);

		for (const auto& serie : series){
			QVector<int> imageOrientation;
			if (serie->imageOrientation.has_value()){
				QStringList orientationStringList = QString::fromStdString(serie->imageOrientation.value()).split('\\');
				bool ok;
				for (auto orientation : orientationStringList){
					imageOrientation << orientation.toInt(&ok);
					if (!ok)
						imageOrientation.removeLast();
				}
			}
			parents.last()->appendChild(new TreeItem({QString::fromStdString(serie->seriesuid),
			                                          QString::fromStdString(serie->seriesNumber.value_or(std::string())),
			                                          QString::fromStdString(serie->modality.value_or(std::string())),
			                                          QString::fromStdString(serie->seriesDesc.value_or(std::string())),
			                                          serie->date.has_value() ? QDate::fromJulianDay(serie->date.value().toJulianDay()) : QDate{},
			                                          serie->time.has_value() ? QTime::fromMSecsSinceStartOfDay(serie->time.value().toTimeDuration().count()) : QTime{},
			                                          serie->numTemporalPositions.value_or(0),
			                                          QVariant::fromValue(imageOrientation),
			                                          QString::fromStdString(serie->spaceBetweenSlices.value_or(std::string()))}, parents.last()));
			parents << parents.last()->child(parents.last()->childCount()-1);
			// Insert Thumbnail Parent
			parents.last()->appendChild( new TreeItem{ { "SeriesThumbnails", "", "", "", "" }, parents.last() } );
			parents << parents.last()->child(parents.last()->childCount()-1);
			for (auto thumb : serie->thumbnails){
				auto thumbnail = QImage::fromData(QByteArray(reinterpret_cast<const char *>(thumb->thumbnail.data()), static_cast<int>(thumb->thumbnail.size())));
				parents.last()->appendChild(new TreeItem({QString::number(thumb->width),
				                                          QString::number(thumb->height),
				                                          QString::number(thumb->minDim),
				                                          QString::number(thumb->maxDim),
				                                          thumbnail}, parents.last()));
			}
			parents.pop_back(); // Thumbnail

			// Insert Overlay Parent
			parents.last()->appendChild( new TreeItem{ { "Overlays" }, parents.last() } );
			parents << parents.last()->child(parents.last()->childCount()-1);
			if( !serie->overlays.empty() ) {
				QMultiHash<QString, Wt::Dbo::ptr<DbOverlay>> overlayHash{};
				for( const auto& overlay : serie->overlays ) {
					overlayHash.insert( overlay->UUID.c_str(), overlay );
				}

				for( const auto& uuid : overlayHash.uniqueKeys() ) {
					auto values = overlayHash.values( uuid );
					std::sort( values.begin(), values.end(), sortOverlays );
					auto overlay = values.last();

					QPixmap pixmap{};
					pixmap.loadFromData( overlay->overlayImage.data(), static_cast<unsigned int>( overlay->overlayImage.size() ) );
					QByteArray overlayData{ reinterpret_cast<const char *>( overlay->overlayData.data() ), static_cast<int>( overlay->overlayData.size() ) };
					parents.last()->appendChild( new TreeItem{ {
					                            QString::fromStdString( overlay->UUID ),
					                            QString::fromStdString( overlay->overlayTypeName ),
					                            overlay->imageNumber.value_or( -1 ),
					                            pixmap,
					                            overlayData
					                            }, parents.last() } );
				}
			}
			parents.pop_back(); // Overlay

			parents.pop_back(); // Series
		}
		parents.pop_back(); // Study
	}
	model->endPopulateModel(this);
#if showPatientTreeview
	auto patientModelView = new QTreeView(qApp->topLevelWidgets().first());
	patientModelView->setItemDelegate(new PatientModelDelegate);
	patientModelView->setModel(model);
	patientModelView->setWindowFlag(Qt::Window);
	patientModelView->show();
	//patientModelView->expandAll();
#endif
}

void SqlDatabaseService::updatePatientModel(quantx::patient::PatientModel *model, const QString &seriesUID)
{
	qDebug() << "Updating Patient Model" << seriesUID;
	Wt::Dbo::Transaction transaction { impl->session };
	auto series = transaction.session().find<DicomSeries>().where("series_uid = ?").bind(seriesUID.toStdString()).resultValue();
	if (series && QString::fromStdString(series->mrn.value()) == model->patientMrn()){
		model->insertNewSeries(series.get());
	}
}

/*!
 * \brief Saves a thumbnail to the DB, for a given series UID.
 * \param seriesUID UID of series to save to.
 * \param thumbnail Thumbnail image.
 * \return Success / fail -- true if successful, false otherwise.
 */
bool SqlDatabaseService::saveThumbnailToDB(QString seriesUID, const QImage &thumbnail)
{
	QByteArray thumbnailByteArray;
	QBuffer buffer(&thumbnailByteArray);
	buffer.open(QIODevice::WriteOnly);
	auto ok = thumbnail.save(&buffer, "JPG", 85);
#if !useDebug
	Q_UNUSED( ok );
#else
	if(!ok)
		qDebug() << "Thumbnail conversion to jpeg failed" << thumbnail.size() << QImageWriter::supportedImageFormats();
#endif
	buffer.close();
	std::vector<unsigned char> thumbData(thumbnailByteArray.begin(), thumbnailByteArray.end());

	SqlDatabaseServiceError error;
	Wt::Dbo::Transaction transaction { impl->session };
	Wt::Dbo::ptr<DicomSeries> series;
	Wt::Dbo::ptr<SeriesThumbnails> thumbPtr = transaction.session().add(std::unique_ptr<SeriesThumbnails> {new SeriesThumbnails()});
	thumbPtr.modify()->seriesUID = seriesUID.toStdString();
	thumbPtr.modify()->width = thumbnail.width();
	thumbPtr.modify()->height = thumbnail.height();
	auto minmaxDims = std::minmax(thumbnail.width(), thumbnail.height());
	thumbPtr.modify()->minDim = minmaxDims.first;
	thumbPtr.modify()->maxDim = minmaxDims.second;
	thumbPtr.modify()->thumbnail = std::move(thumbData);
	try {
		series = transaction.session().find<DicomSeries>().where("series_uid = ?").bind(seriesUID.toStdString()).resultValue();
	} catch ( ... ) {
		handleException(&error, "Error finding DICOM series for series UID.");
		return false;
	}
	if (series){
#if useDebug
		qDebug() << "Linking thumbnail to series";
#endif
		thumbPtr.modify()->series = series;
#if useDebug
	} else {
		qDebug() << "series not found to link to thumbnail";
#endif
	}
	return true;
}

/*!
 * \brief Creates the series thumbnails for the patient.
 * \param seriesUIDs List of series UIDs to populate thumbnails for.
 * \param maxDim Max dimension size for thumbnails.
 */
void SqlDatabaseService::createSeriesThumbnails(const QStringList &seriesUIDs, int maxDim)
{
	QtConcurrent::run(this, &SqlDatabaseService::createSeriesThumbnailThread, seriesUIDs, maxDim);
}

/*!
 * \brief Single-threadedly create thumbnails, e.g. for SQlite.
 * \param seriesUIDs List of series UIDs to create thumbnails for.
 * \param maxDim Max dimensional size for the thumbnails.
 */
void SqlDatabaseService::createSeriesThumbnailThread(QStringList seriesUIDs, int maxDim)
{
	//could possibly do several thumbnails concurrently, but Sqlite db seems to have some issues do so
	for (auto series : seriesUIDs)
		createSeriesThumbnail(series, maxDim);
}

/*!
 * \brief Create a series thumbnail.
 * \param seriesUID UID of series to create thumbnail for.
 * \param maxDim Max dimensional size for thumnail.
 */
void SqlDatabaseService::createSeriesThumbnail(QString seriesUID, int maxDim)
{
	SqlDatabaseServiceError error;
	Wt::Dbo::ptr<Images2D> imagePtr;
	auto thumbImageUIDs = chooseThumbnailImageUID(seriesUID);
	if (!thumbImageUIDs.second.empty()){
		auto saveFullSize = (thumbImageUIDs.first == "OT");
		auto firstThumbnail = true;
		for (auto thumbImageUID : thumbImageUIDs.second){
		int width;
		int height;
		double windowWidth;
		double windowLevel;
		QByteArray imageData;
		int planarConfiguration;
		QString photometricInterpretation;
		{
		Wt::Dbo::Transaction transaction { impl->session };
		imagePtr = transaction.session().find<Images2D>().where("image_uid = ?").bind(thumbImageUID).resultValue();
		width = imagePtr->width.value();
		height = imagePtr->height.value();
		QString windowWidthString = QString::fromStdString(imagePtr->windowWidth.value());
		QString windowLevelString = QString::fromStdString(imagePtr->windowCenter.value());
		// check for multi-valued window Width/Level
		windowWidth = windowWidthString.contains('\\') ? windowWidthString.split('\\').first().toDouble() : windowWidthString.toDouble();
		windowLevel = windowLevelString.contains('\\') ? windowLevelString.split('\\').first().toDouble() : windowLevelString.toDouble();
		QString manufacturer = QString::fromStdString(imagePtr->manufacturer.value_or(std::string()));
		QString modelName  = QString::fromStdString(imagePtr->modelName.value_or(std::string()));
		photometricInterpretation = QString::fromStdString(imagePtr->photometricInterpretation.value_or(std::string()));;
		imageData = qUncompress(QByteArray(reinterpret_cast<const char *>(imagePtr->image_data.value().data()), static_cast<int>(imagePtr->image_data.value().size())));
		planarConfiguration = 0;
		if(manufacturer.contains("Philips") && photometricInterpretation.contains("RGB",Qt::CaseInsensitive)){
#if useDebug
			qDebug() << "Found RGB image from manufacturer Philips: assuming planar configuration is 0";
#endif
			planarConfiguration = 0;
		}
		else if(manufacturer.contains("ATL") && photometricInterpretation.contains("RGB",Qt::CaseInsensitive)){
#if useDebug
			qDebug() << "Found RGB image from manufacturer ATL: assuming planar configuration is 1";
#endif
			planarConfiguration = 1;
		}
		} // end transaction

		QScopedPointer<QImage> image(new QImage(width,height,QImage::Format_RGB32));
		int numPixels = width*height;
		quint16* dataptr = nullptr;
		quint8* dataptr8bit = nullptr;
		if (imageData.size() == width*height)         //each pixel stored using 8 bits
			dataptr8bit = reinterpret_cast<quint8 *>(imageData.data());
		else if (imageData.size() == 2*width*height)  //each pixel stored using 16 bits
			dataptr = reinterpret_cast<quint16 *>(imageData.data());
		else if (imageData.size() == 3*width*height)  //each pixel stored using 8 bits for each RGB value
			dataptr8bit = reinterpret_cast<quint8 *>(imageData.data());
		else                                          //strange size, we might crash
			dataptr8bit = reinterpret_cast<quint8 *>(imageData.data());
		double scalefactor = 255./(windowWidth);
		double shiftamount = windowLevel-windowWidth/2;
		bool flipBrightness = photometricInterpretation.startsWith("MONOCHROME1");

		if (photometricInterpretation.contains("RGB",Qt::CaseInsensitive)) {
			for (int i=0;i<height;i++){
				for(int j=0;j<width;j++) {
					if (planarConfiguration == 0)
						image->setPixel(j , i, qRgb(dataptr8bit[3 * width * i+ 3 * j],
						                dataptr8bit[3 * width * i + 3 * j +1],
						        dataptr8bit[ 3 * width * i + 3 * j + 2]));
					else if (planarConfiguration == 1)
						image->setPixel(j, i, qRgb(dataptr8bit[(width * i) + j],
						                dataptr8bit[(width * i) + numPixels + j],
						        dataptr8bit[(width *i ) + 2 * numPixels + j]));
				}
			}
		} else if(dataptr8bit != nullptr){
			int temppixval;
			for (int i=0;i<height;i++)
				for(int j=0;j<width;j++) {

					temppixval = static_cast<int>((dataptr8bit[width*i+j]-shiftamount)*scalefactor);
					if (temppixval < 0)
						temppixval = 0;
					if (temppixval > 255)
						temppixval = 255;
					if(flipBrightness)
						temppixval = 255 - temppixval;
					image->setPixel(j,i,qRgb(temppixval,temppixval,temppixval));
				}

		} else {
			int temppixval;
			for (int i=0;i<height;i++)
				for(int j=0;j<width;j++) {
					temppixval = static_cast<int>((dataptr[width*i+j]-shiftamount)*scalefactor);
					if (temppixval < 0)
						temppixval = 0;
					if (temppixval > 255)
						temppixval = 255;
					if(flipBrightness)
						temppixval = 255 - temppixval;
					image->setPixel(j,i,qRgb(temppixval,temppixval,temppixval));
				}
		}
		if (firstThumbnail){
			firstThumbnail = false;
			auto thumbnail=image->scaled(maxDim, maxDim, Qt::KeepAspectRatio, Qt::SmoothTransformation);
			if (!thumbnail.isNull()){
			    emit seriesThumbnailCreated(seriesUID, thumbnail);
				saveThumbnailToDB(seriesUID, thumbnail);
		    }
		}
		if (saveFullSize){
			auto thumbnail=*(image.data());
			if (!thumbnail.isNull()){
			    emit seriesThumbnailCreated(seriesUID, thumbnail);
				saveThumbnailToDB(seriesUID, thumbnail);
		    }
		}
	}
	}
}

/*!
 * \brief Choose a UID for a thumbnail.
 * \param seriesUID UID of series the thumbnail is for.
 * \return UID of thumbnail.
 */
std::pair<std::string, std::vector<std::string>> SqlDatabaseService::chooseThumbnailImageUID(QString seriesUID)
{
	SqlDatabaseServiceError error;
	Wt::Dbo::ptr<DicomSeries> series;
	std::pair<std::string, std::vector<std::string>> thumbnailImageUID;
	Wt::Dbo::Transaction transaction { impl->session };
	Wt::Dbo::Query<Wt::Dbo::ptr<DicomSeries>> seriesQuery;
	try {
		seriesQuery = transaction.session().find<DicomSeries>().where("series_uid = ?").bind(seriesUID.toStdString());
	} catch ( ... ) {
		handleException(&error, "Error finding DICOM series for series UID.");
		return thumbnailImageUID;
	}

	auto resultList = seriesQuery.resultList();
	if (resultList.size() == 0){
#if useDebug
		qDebug() << "\tNo Results in series query";
#endif
		return thumbnailImageUID;
	}
	series = resultList.front();
	thumbnailImageUID.first = series->modality.value();
	if (series->modality != "MR" && series->modality != "OT"){
		thumbnailImageUID.second.push_back(transaction.session().query<std::string>("select image_uid from qi_2d_images").where("series_uid = ?").bind(seriesUID.toStdString()).limit(1).resultValue());
	} else if (series->modality == "OT"){
		auto uids = transaction.session().query<std::string>("select image_uid from qi_2d_images").where("series_uid = ?").bind(seriesUID.toStdString()).orderBy("instance_num").resultList();
		for (auto uid : uids){
			thumbnailImageUID.second.push_back(uid);
		}
	} else { // MR image
		int imageCount;
		try {
			imageCount = transaction.session().query<int>("select count(1) from qi_2d_images").where("series_uid = ?").bind(seriesUID.toStdString());
		} catch ( ... ) {
			handleException(&error, "Error finding number of images for series.");
			return thumbnailImageUID;
		}
		if (imageCount == 1){
			thumbnailImageUID.second.push_back(transaction.session().query<std::string>("select image_uid from qi_2d_images").where("series_uid = ?").bind(seriesUID.toStdString()).limit(1).resultValue());
		}
		int defaultThumbTimePoint = 2; // the default time point to use is the first post-contrast timepoint
		int numTimePoints = series->numTemporalPositions.value_or(1);
		int imagesPerTimePoint = imageCount/ numTimePoints;
		int thumbTimePoint = (numTimePoints < 2) ? 1 : defaultThumbTimePoint;
		int thumbSliceNum = static_cast<int>( imagesPerTimePoint / 2);
		QMap<float,std::string> imageMap;
		using imageQuery = std::tuple<std::string, double>; // this defines the fields data types for the query below
		Wt::Dbo::collection<imageQuery> imageResult;
		QMap<double, std::string> sliceMap;
		try {
			if (numTimePoints > 1){
				imageResult = transaction.session().query<imageQuery>("select image_uid, slice_location from qi_2d_images").where("series_uid = ? and temporal_pos_id = ?").bind(seriesUID.toStdString()).bind(thumbTimePoint).resultList();
				if (imageResult.size() == 0){ // series reported having multiple temporal positions but all temporal position weren't in this series
					thumbSliceNum = imageCount / 2;
					imageResult = transaction.session().query<imageQuery>("select image_uid, slice_location from qi_2d_images").where("series_uid = ?").bind(seriesUID.toStdString()).resultList();
				}
			} else {
				imageResult = transaction.session().query<imageQuery>("select image_uid, slice_location from qi_2d_images").where("series_uid = ?").bind(seriesUID.toStdString()).resultList();
			}
		} catch ( ... ) {
			handleException(&error, "Error finding Images for series UID.");
			return thumbnailImageUID;
		}
		for (auto row : std::as_const(imageResult)){
			sliceMap.insert(std::get<1>(row), std::get<0>(row));
		}
		const auto imageUIDs = sliceMap.values();
		if(thumbSliceNum < imageUIDs.size()) {
			thumbnailImageUID.second.push_back(imageUIDs[thumbSliceNum]);
		} else {
			thumbnailImageUID.second.push_back(imageUIDs[(sliceMap.size() - 1)/2]);
		}
	}
#if useDebug
	qDebug().noquote() << QString("Selected imageUID for %1: %2").arg(seriesUID, thumbnailImageUID.second.front().c_str());
#endif
	return thumbnailImageUID;
}

// Dicom Helper Functions
namespace  {
QString getDicomTag( const QString& line )
{
	int start;
	int end;
	start = line.indexOf( "(",0 );
	if (start == -1) {
		return QString();
	}

	    end = line.indexOf( ")",start );
		return line.mid( start + 1, end - start - 1 );
}

QString getDicomValue( QString line )
{
	QString dicomValue;
	while( line.startsWith( '\t' ) ) {
		line.remove( 0,1 );
	}
	auto parsedStrings = line.split( "\t" );
	if( parsedStrings.size() > 2 ) {
		if( parsedStrings.at( 1 ).startsWith( "SQ" ) ) {
			dicomValue += "SQ";
		}
		dicomValue += parsedStrings.last().trimmed();
	}
	if( dicomValue.startsWith( "Loaded:" ) ) {
		dicomValue = QString("<Binary Data>(%1 Bytes)").arg(dicomValue.split(':').last());
	}

	return dicomValue;
}
}

//!
//! \brief Gets all of the dicom fields based on a imageUID
//! \param imageUID
//! \return Field and Value
//!
//! \preInitPost{ The fields are returned for an image.}
//!
QMultiHash<QString, QString> SqlDatabaseService::extractAllDicomFields( const QString &imageUID )
{
	QMultiHash<QString, QString> hash;

	try {
		Wt::Dbo::Transaction transaction( impl->session );
		auto result = transaction.session().find<DicomData>().where( R"("0008,0018" = ?)" ).bind( imageUID.toStdString() ).resultValue();

		QByteArray fullHeader = QByteArray::fromRawData( reinterpret_cast<const char*>( result->fullHeader.data() ), static_cast<int>(result->fullHeader.size()) );
		QString line;
		QString tag, dicomValue;
		const auto lineList = fullHeader.split('\n');
		for(const auto & lineByteArray : lineList){
			line = QString(lineByteArray.data());
			tag = getDicomTag(line);
			dicomValue = getDicomValue(line);
#if useDebug
			qDebug()<<line;
			qDebug()<<"Tag:"<<tag<<" Value: "<<dicomValue;
#endif
			if (tag.length() > 0)
				hash.insertMulti(tag,dicomValue);
		}

//		transaction.commit();

	} catch ( ... ) {
		//handleException( &)
	}

	return hash;
}

//!
//! \brief Gets all of the dicom fields based on a imageUID
//! \param imageUID UID to query on.
//! \param error Pointer to error object to use for DB errors
//! \return Field and Value
//!
//! \preInitPost{ The fields are returned for an image.}
//!
QMultiHash<QString, QString> SqlDatabaseService::qSqlExtractAllDicomFields( const QString &imageUID, SqlDatabaseServiceError* error )
{
	QMultiHash<QString, QString> dicomOutput;
	auto connectionName = "QIsqlExtractDicomDatabase";
	QSqlDatabase db = QSqlDatabase::addDatabase( databaseSettings_.databaseDriverType(), connectionName );
	if( connectAndOpenQDatabase( &db ) ) {

		auto sqlDicomTable = "dicom_data";
		QSqlRecord dicomRecord = db.record( sqlDicomTable );
		QSqlRecord uidRecord = dicomRecord;
		uidRecord.setValue("0008,0018",imageUID);
		for (int i=uidRecord.count()-1;i>=0;i--)
			if(uidRecord.isNull(i)){
				uidRecord.remove(i);
				dicomRecord.setGenerated(i,false);
			}
		dicomRecord.setGenerated("full_header", true);

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		QMultiHash<QString, QString> dicomOutput;

		sqlStatement = db.driver()->sqlStatement( QSqlDriver::SelectStatement, sqlDicomTable ,dicomRecord,false) + QString(' ')
		        + db.driver()->sqlStatement( QSqlDriver::WhereStatement, sqlDicomTable, uidRecord,false);
		sqlQuery->exec(sqlStatement);
		if(!sqlQuery->next()){
#if useDebug
			qDebug()<<"No Records returned";
#endif
			return dicomOutput;
		}

		sqlQuery->first();
		dicomRecord = sqlQuery->record();
		QByteArray fullHeader = dicomRecord.value("full_header").toByteArray();
#if useDebug
		qDebug()<< "Header Size: "<<fullHeader.size();
#endif
		QString line;
		QString tag, dicomValue;
		const auto lineList = fullHeader.split('\n');
		for(const auto & lineByteArray : lineList){
			line = QString(lineByteArray.data());
			tag = getDicomTag(line);
			dicomValue = getDicomValue(line);
#if useDebug
			qDebug()<<line;
			qDebug()<<"Tag:"<<tag<<" Value: "<<dicomValue;
#endif
			if (tag.length() > 0)
				dicomOutput.insertMulti(tag,dicomValue);
		}
		sqlQuery.reset();
		db.close();
	} else {
		handleQSqlError( error, connectionName, db.lastError() );
	}

	return dicomOutput;
}

/*!
 * \brief Loads a series to be used by the new MVC framework.
 * \param seriesId UID of series to load.
 * \param modality Modality to load.
 */
void SqlDatabaseService::loadSeries(const QString& seriesId, const QUANTX::Modality& modality)
{
	emit beginImageLoad(modality);

	if (modality == QUANTX::MRI) {
		auto *mriImage = new QiMriImage;
#ifdef QI_INTERNAL_TIMING_OUTPUT
		emit startLoadingMRSeries(mriImage);
#endif
#if useDebug
		qDebug() << "Loading MRI from SqlSearchWindow object";
#endif
		connect(mriImage, &QiMriImage::openProgressUpdated, this, &SqlDatabaseService::openImageLoadProgressUpdated);
		connect(mriImage, &QiMriImage::openProgressNewMaximum, this,
		        &SqlDatabaseService::openImageLoadProgressNewMaximum);

		mriImage->openSeries(seriesId, databaseSettings_.getDBSettingsList(), seriesUtils.dynMultiSeriesDesc(), seriesUtils.fastDynamicSeriesDesc());

		if(mriImage->nz() == 0){
			auto possibleCorruptSeriesId = mriImage->seriesUID();
			emit corruptImageFound("Cannot Open MR Series","MR series failed to open.\nData is likely to be corrupt. Do you wish to delete this series?", [this, possibleCorruptSeriesId](QWidget *parentDeleteWidget) {
				SqlSeriesDeleteWidget deleteWidget(databaseSettings_.getDBSettingsList(), parentDeleteWidget);
				deleteWidget.deleteSeries(possibleCorruptSeriesId, QUANTX::MRI);
			});

			delete mriImage;
			return;
		}

		//mriImage should be deleted when the other program is done with it
		emit imageLoading(mriImage);
	} else if(modality == QUANTX::US3D){
//        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::US3D,this));
		qApp->processEvents();
		auto *us3DImage = new Qi3DUSImage;
		us3DImage->openSeries(seriesId, databaseSettings_.getDBSettingsList());
		emit imageLoading(us3DImage);
//        openDialog->close();
	} else if(modality == QUANTX::MG3D){
//        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::MG3D,this));
		qApp->processEvents();
		auto *mg3DImage = new Qi3DMGImage;
		mg3DImage->openSeries(seriesId, databaseSettings_.getDBSettingsList());
		emit imageLoading(mg3DImage);
//        openDialog->close();
	} else if (modality == QUANTX::MAMMO) {
		auto *image = new Qi2DImage;
//        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::MAMMO,this));
		int numImages = image->openSeries(seriesId, modality, databaseSettings_.getDBSettingsList());
		if(numImages == 1)
			emit imageLoading(image);
		else{
			QVector<Qi2DImage *> twoDImageList;
			twoDImageList << image;
			for(int i=1;i<numImages;i++){
				twoDImageList << new Qi2DImage;
				numImages = twoDImageList[i]->openSeries(seriesId, modality, databaseSettings_.getDBSettingsList(), i);
			}
			if(numImages == -1){ //error opening last image in list
				delete twoDImageList.last();
				twoDImageList.removeLast();
			}
			emit imageLoading(twoDImageList);
		}
//        openDialog->close();
	} else if(modality == QUANTX::US){
//        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::US,this));
		QVector<Qi2DImage *> twoDImageList;
		int numImages = 1;
		for(int i=0;i<numImages;i++){
			twoDImageList << new Qi2DImage;
			numImages = twoDImageList[i]->openSeries(seriesId, modality, databaseSettings_.getDBSettingsList(), i);
		}
		if(numImages == -1){ //error opening last image in list
			delete twoDImageList.last();
			twoDImageList.removeLast();
		}
		//mriImage should be deleted when the other program is done with it
		emit imageLoading(twoDImageList);
//        openDialog->close();
	}
}

/*!
 * \brief Saves plugin info to the DB.
 * \param info Plugin info object to save.
 * \return Error object that describes any error information about the save operation.
 */
SqlDatabaseServiceError SqlDatabaseService::savePluginDataToDb( const PluginInfo& info )
{
	auto pluginData = std::make_unique<PluginData>();
	pluginData->pluginName = info.name.toStdString();
	pluginData->sessionId = DatabaseHelper::createSessionId().toStdString();
	if( !info.mrn.isNull() ) {
		pluginData->mrn = info.mrn.toStdString();
	}
	pluginData->key = info.key.toStdString();

	pluginData->data = std::vector<unsigned char>{ info.data.begin(), info.data.end() };

	SqlDatabaseServiceError error;

	Wt::Dbo::Transaction transaction( impl->session );

	try {
		transaction.session().add( std::move( pluginData ) );
		transaction.commit();
	} catch ( ... ) {
		handleException( &error, QString{ "Save Plugin Data - Name: %1 - Key: %2 -Error" }.arg( info.name ).arg( info.key ) );
		transaction.rollback();
	}

	return error;
}

/*!
 * \brief Loads plugin data from the current patient.
 * \param patientMrn MRN of patient to load.
 * \param pluginInfo Pointer to vector of plugin info objects to load.
 * \return Error object that describes the status of the load operation.
 */
SqlDatabaseServiceError SqlDatabaseService::loadPluginDataFromCurrentPatientMrn( const QString &patientMrn, QVector<PluginInfo> *pluginInfo )
{
	SqlDatabaseServiceError error;

	Wt::Dbo::Transaction transaction( impl->session );

	try {
		auto dataCollection = transaction.session().find<PluginData>().where( "mrn = ?" ).bind( patientMrn.toStdString() ).resultList();
		for( auto dataPtr : dataCollection ) {
			PluginInfo info;
			info.name = dataPtr->pluginName.c_str();
			info.sessionId = dataPtr->sessionId.c_str();
			info.mrn = patientMrn;
			info.key = dataPtr->key.c_str();
			info.data = QByteArray{ reinterpret_cast<const char *>( dataPtr->data.data() ), static_cast<int>( dataPtr->data.size() ) };
			pluginInfo->append( info );
		}

		transaction.commit();
	} catch ( ... ) {
		handleException( &error, QString{ "Load Plugin Data for Patiern Mrn: %1" }.arg( patientMrn ) );
		transaction.rollback();
	}

	return error;
}

/*!
 * \brief Loads acquisition protocol information.
 * \param protocols Collection of AcquisitionProtocolInfo object pointers.
 * \return Error object describing how the operation went.
 */
SqlDatabaseServiceError SqlDatabaseService::loadAcquisitionProtocols(QVector<AcquisitionProtocolInfo> *protocols)
{
	SqlDatabaseServiceError error;

	Wt::Dbo::Transaction transaction( impl->session );

	try {
		auto dbProtocols = transaction.session().find<AcquisitionProtocol>().resultList();
		for( auto protocol : dbProtocols ) {
			AcquisitionProtocolInfo info;
			info.protocolNumber = protocol->protocolNumber;
			info.keyName = protocol->keyName.c_str();
			info.keyValue = protocol->keyValue.c_str();

			protocols->append( info );
		}

		transaction.commit();
	} catch ( ... ) {
		handleException( &error, "Load Acquisition Protocols" );
		transaction.rollback();
	}

	return error;
}

/*!
 * \brief Saves an overlay to the DB.
 * \param pixmap Snapshot of the viewport to save.
 * \param overlay Overlay info object to save.
 * \return An error object that shows if any error occurs.
 */
SqlDatabaseServiceError SqlDatabaseService::saveOverlayToDb( const QPixmap& pixmap, const OverlayInfo& overlay )
{
	auto dbOverlay = std::make_unique<DbOverlay>();

	dbOverlay->UUID = overlay.UUID.toStdString();

	dbOverlay->sessionId = DatabaseHelper::createSessionId().toStdString();

	dbOverlay->mrn = overlay.mrn.toStdString();
	dbOverlay->studyUID = overlay.studyUID.toStdString();
	dbOverlay->seriesUID = overlay.seriesUID.toStdString();
	dbOverlay->accession_number = overlay.accessionNumber.toStdString();
	dbOverlay->overlayTypeName = overlay.overlayTypeName.toStdString();
	dbOverlay->imageNumber = overlay.imageNumber;

	QByteArray snapshotByteArray;
	QBuffer buffer( &snapshotByteArray );
	buffer.open( QIODevice::WriteOnly );
#if !useDebug
	pixmap.save( &buffer, "JPG", 85 );
#else
	if( !pixmap.save( &buffer, "JPG", 85 ) ) {
		qDebug() << "OverlayImage conversion to jpeg failed" << pixmap.size() << QImageWriter::supportedImageFormats();
	}
#endif
	buffer.close();
	dbOverlay->overlayImage = std::vector<unsigned char>{ snapshotByteArray.begin(), snapshotByteArray.end() };

	dbOverlay->overlayData = std::vector<unsigned char>{ overlay.overlayData.begin(), overlay.overlayData.end() };

	SqlDatabaseServiceError error;

	Wt::Dbo::Transaction transaction( impl->session );

	try {
		auto wtOverlayPtr = transaction.session().add( std::move( dbOverlay ) );

		// Attach to Series
		auto series = transaction.session().find<DicomSeries>().where( "series_uid = ?" ).bind( overlay.seriesUID.toStdString() ).resultValue();
		series.modify()->overlays.insert( wtOverlayPtr );
		transaction.commit();
	} catch ( ... ) {
		handleException( &error, QString{ "Save Overlay of Type %1 Error" }.arg( overlay.overlayTypeName ) );
		transaction.rollback();
	}

	return error;
}

/*!
 * \brief Remove an overlay from the DB.
 * \param overlay Overlay info object to remove.
 * \return An error object that shows if any error occurs.
 */
SqlDatabaseServiceError SqlDatabaseService::removeOverlayFromDb( const quantx::overlay::OverlayInfo& overlay )
{
    SqlDatabaseServiceError error;

    try {
        Wt::Dbo::Transaction transaction(impl->session);
        impl->session.execute("DELETE FROM \"qi_overlay\" WHERE \"uuid\" = ?").bind(overlay.UUID.toStdString());
        transaction.commit();
    } catch ( ... ) {
        handleException( &error, QString{ "Remove Overlay of Type %1 Error" }.arg( overlay.overlayTypeName ) );
    }

    return error;
}

//QMultiHash<QString, QString> SqlDatabaseService::qSqlExtractAllDicomFields( const QString &imageUID, SqlDatabaseServiceError* error )
//{
//	QMultiHash<QString, QString> dicomOutput;
//	auto connectionName = "QIsqlExtractDicomDatabase";
//	QSqlDatabase db = QSqlDatabase::addDatabase( databaseSettings_.databaseDriverType(), connectionName );
//	if( connectAndOpenQDatabase( &db ) ) {

//		auto sqlDicomTable = "dicom_data";
//		QSqlRecord dicomRecord = db.record( sqlDicomTable );
//		QSqlRecord uidRecord = dicomRecord;
//		uidRecord.setValue("0008,0018",imageUID);
//		for (int i=uidRecord.count()-1;i>=0;i--)
//			if(uidRecord.isNull(i)){
//				uidRecord.remove(i);
//				dicomRecord.setGenerated(i,false);
//			}
//		dicomRecord.setGenerated("full_header", true);

//		QString sqlStatement;
//		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
//		QMultiHash<QString, QString> dicomOutput;

//		sqlStatement = db.driver()->sqlStatement( QSqlDriver::SelectStatement, sqlDicomTable ,dicomRecord,false) + QString(' ')
//		        + db.driver()->sqlStatement( QSqlDriver::WhereStatement, sqlDicomTable, uidRecord,false);
//		sqlQuery->exec(sqlStatement);
//		if(!sqlQuery->next()){
//#if useDebug
//			qDebug()<<"No Records returned";
//#endif
//			return dicomOutput;
//		}

//		sqlQuery->first();
//		dicomRecord = sqlQuery->record();
//		QByteArray fullHeader = dicomRecord.value("full_header").toByteArray();
//#if useDebug
//		qDebug()<< "Header Size: "<<fullHeader.size();
//#endif
//		QString line;
//		QString tag, dicomValue;
//		const auto lineList = fullHeader.split('\n');
//		for(const auto & lineByteArray : lineList){
//			line = QString(lineByteArray.data());
//			tag = getDicomTag(line);
//			dicomValue = getDicomValue(line);
//#if useDebug
//			qDebug()<<line;
//			qDebug()<<"Tag:"<<tag<<" Value: "<<dicomValue;
//#endif
//			if (tag.length() > 0)
//				dicomOutput.insertMulti(tag,dicomValue);
//		}
//		sqlQuery.reset();
//		db.close();
//	} else {
//		handleQSqlError( error, connectionName, db.lastError() );
//	}

//	return dicomOutput;
//}

//void SqlDatabaseService::loadSeries(const QString& seriesId, const QUANTX::Modality& modality)
//{
//	emit beginImageLoad(modality);

//	if (modality == QUANTX::MRI) {
//		auto *mriImage = new QiMriImage;
//#ifdef QI_INTERNAL_TIMING_OUTPUT
//		emit startLoadingMRSeries(mriImage);
//#endif
//#if useDebug
//		qDebug() << "Loading MRI from SqlSearchWindow object";
//#endif
//		connect(mriImage, &QiMriImage::openProgressUpdated, this, &SqlDatabaseService::openImageLoadProgressUpdated);
//		connect(mriImage, &QiMriImage::openProgressNewMaximum, this,
//		        &SqlDatabaseService::openImageLoadProgressNewMaximum);

//		mriImage->openSeries(seriesId, databaseSettings_.getDBSettingsList(), seriesUtils.dynMultiSeriesDesc(), seriesUtils.fastDynamicSeriesDesc());

//		if(mriImage->nz() == 0){
//			auto possibleCorruptSeriesId = mriImage->seriesUID();
//			emit corruptImageFound("Cannot Open MR Series","MR series failed to open.\nData is likely to be corrupt. Do you wish to delete this series?", [this, possibleCorruptSeriesId](QWidget *parentDeleteWidget) {
//				SqlSeriesDeleteWidget deleteWidget(databaseSettings_.getDBSettingsList(), parentDeleteWidget);
//				deleteWidget.deleteSeries(possibleCorruptSeriesId, QUANTX::MRI);
//			});

//			delete mriImage;
//			return;
//		}

//		//mriImage should be deleted when the other program is done with it
//		emit imageLoading(mriImage);
//	} else if(modality == QUANTX::US3D){
////        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::US3D,this));
//		qApp->processEvents();
//		auto *us3DImage = new Qi3DUSImage;
//		us3DImage->openSeries(seriesId, databaseSettings_.getDBSettingsList());
//		emit imageLoading(us3DImage);
////        openDialog->close();
//	} else if(modality == QUANTX::MG3D){
////        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::MG3D,this));
//		qApp->processEvents();
//		auto *mg3DImage = new Qi3DMGImage;
//		mg3DImage->openSeries(seriesId, databaseSettings_.getDBSettingsList());
//		emit imageLoading(mg3DImage);
////        openDialog->close();
//	} else if (modality == QUANTX::MAMMO) {
//		auto *image = new Qi2DImage;
////        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::MAMMO,this));
//		int numImages = image->openSeries(seriesId, modality, databaseSettings_.getDBSettingsList());
//		if(numImages == 1)
//			emit imageLoading(image);
//		else{
//			QVector<Qi2DImage *> twoDImageList;
//			twoDImageList << image;
//			for(int i=1;i<numImages;i++){
//				twoDImageList << new Qi2DImage;
//				numImages = twoDImageList[i]->openSeries(seriesId, modality, databaseSettings_.getDBSettingsList(), i);
//			}
//			if(numImages == -1){ //error opening last image in list
//				delete twoDImageList.last();
//				twoDImageList.removeLast();
//			}
//			emit imageLoading(twoDImageList);
//		}
////        openDialog->close();
//	} else if(modality == QUANTX::US){
////        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::US,this));
//		QVector<Qi2DImage *> twoDImageList;
//		int numImages = 1;
//		for(int i=0;i<numImages;i++){
//			twoDImageList << new Qi2DImage;
//			numImages = twoDImageList[i]->openSeries(seriesId, modality, databaseSettings_.getDBSettingsList(), i);
//		}
//		if(numImages == -1){ //error opening last image in list
//			delete twoDImageList.last();
//			twoDImageList.removeLast();
//		}
//		//mriImage should be deleted when the other program is done with it
//		emit imageLoading(twoDImageList);
////        openDialog->close();
//	}
//}
