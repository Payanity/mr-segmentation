#ifndef QISQLDLLCHECK_H
#define QISQLDLLCHECK_H

#include <QtGlobal>

#ifdef QISQL_DLL
#define QISQL_EXPORT  Q_DECL_EXPORT
#else
#define QISQL_EXPORT Q_DECL_IMPORT
#endif

#endif // QISQLDLLCHECK_H
