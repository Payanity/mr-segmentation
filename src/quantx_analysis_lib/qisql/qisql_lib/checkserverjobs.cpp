#include "checkserverjobs.h"
#include <QSqlDriver>
#include <QSqlRecord>
#include <QSqlQuery>
#include "qisql.h"
#include <QDateTime>
#include <QVariant>

#define useDebug 0
#if useDebug
#include<QDebug>
#include <QElapsedTimer>
#endif

QString checkServerJobs(const QString & seriesUID, const QStringList & dbSettings)
{
#if useDebug
	QElapsedTimer timer;
	timer.start();
#endif
	//check if the server is doing any processing on the series, empty string = no jobs, otherwise return string with job info
	QScopedPointer<QISql> qisql(new QISql(dbSettings)); // Just use this to get settings
	auto result = QString();
	{
		QSqlDatabase db = qisql->createDatabase("QIJobsDatabase");
		auto ok = db.open();
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		QString jobsTable = "qi_jobs_status";
		QSqlRecord jobRecord  = db.record(jobsTable);
		QSqlRecord uidRecord = jobRecord;
		uidRecord.setValue("series_uid", seriesUID);
		for(int i=uidRecord.count()-1;i>=0;i--)
			if(uidRecord.isNull(i))
				uidRecord.remove(i);

		QString sqlStatement;
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement, jobsTable,jobRecord,false) + QString(' ')
		               + db.driver()->sqlStatement(QSqlDriver::WhereStatement, jobsTable,uidRecord,false)
		               + " ORDER BY job_time DESC";
		ok = sqlQuery->exec(sqlStatement);
		if (ok && sqlQuery->first()){
			if (sqlQuery->record().value("status").toString() != "finished")
				result = sqlQuery->record().value("jobtype").toString() + "," + sqlQuery->record().value("status").toString();
		}
	}
	QSqlDatabase::removeDatabase("QIJobsDatabase");
#if useDebug
	qDebug() << "Time to check for database jobs (sec) " << timer.elapsed()/1000.;
	qDebug() << "Jobs status of " << seriesUID << " " << result;
#endif
	return result;
}
