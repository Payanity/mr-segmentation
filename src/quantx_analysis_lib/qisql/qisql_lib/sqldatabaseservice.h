#ifndef SQLDATABASESERVICE_H
#define SQLDATABASESERVICE_H

#include "qisqlDllCheck.h"
#include "databasesettings.h"
#include "sqldatabaseservicestatements.h"
#include "quantx.h"
#include "seriesdescriptionutils.h"
#include "qtsqltraits.h"
#include <Wt/Dbo/Dbo.h>
#include <QStandardItemModel>
#include <QSettings>

#include <QObject>

class QSettings;
class SqlDatabaseServiceImpl;
class QSqlDatabase;
class Qi2DImage;
class Qi3DImage;
class QiMriImage;
struct PluginInfo;

namespace quantx {
namespace patient {
class Patient;
class PatientModel;
}
namespace dicom {
class DicomStudy;
}
namespace overlay {
struct OverlayInfo;
}
namespace acquisition {
struct AcquisitionProtocolInfo;
}
namespace sql {

/*!
 * \brief Class that contains errors for the SqlDatabaseService
 * \class SqlDatabaseServiceError
 * \ingroup SQLModule
 */
class QISQL_EXPORT SqlDatabaseServiceError
{
public:
	SqlDatabaseServiceError();
	SqlDatabaseServiceError(const SqlDatabaseServiceError& other) = default;
	SqlDatabaseServiceError(SqlDatabaseServiceError&& other) = default;
	SqlDatabaseServiceError& operator=(const SqlDatabaseServiceError& rhs) = default;
	SqlDatabaseServiceError& operator=(SqlDatabaseServiceError&& rhs) = default;
	~SqlDatabaseServiceError() = default;

	bool bSuccess; //!< Shows if a function call was succesful or not
	void setMsg(const QString& msg);
	QString msg() const;
private:
	QString msg_; //!< The error output from the function call (if there is any).
};

/*!
 * \brief The SqlDatabaseService class
 * \class SqlDatabaseService
 * \ingroup SQLModule
 *
 * \funcReqBegin
 * \funcReq1
 * \funcReq2
 * \funcReq3
 * \funcReq4
 * \funcReq5
 * \funcReq6
 * \funcReq7
 * \funcReq8
 * \funcReq9
 * \funcReq10
 * \funcReq11
 * \funcReq12
 * \funcReq14
 * \funcReq51
 * \funcReqEnd
 *
 * \todo Change SqlDriver into an enum?  Or use better defined strings rather than Qt's version
 */
class QISQL_EXPORT SqlDatabaseService : public QObject
{
	Q_OBJECT
public:
	/*!
	 * \brief Deleted default constructor.
	 */
	SqlDatabaseService() = delete;
	explicit SqlDatabaseService( const QSettings& dataSettings );
	SqlDatabaseService(const SqlDatabaseService& other) = delete;
	SqlDatabaseService& operator=(const SqlDatabaseService& rhs) = delete;
	~SqlDatabaseService() override;

	SqlDatabaseServiceError initialize(bool useResearchLicenseMode);
	Wt::Dbo::ptr<quantx::patient::Patient> findPatient(QString mrn);
	void populatePatientModelForMRN(QString mrn, quantx::patient::PatientModel *model,
	                                bool patientLevelOnly = false);
	void updatePatientModel(quantx::patient::PatientModel *model, const QString &seriesUID);
	void createSeriesThumbnails(const QStringList &seriesUIDs, int maxDim);
	void createSeriesThumbnailThread(QStringList seriesUIDs, int maxDim);
	void createSeriesThumbnail(QString seriesUID, int maxDim);
	std::pair<std::string, std::vector<std::string> > chooseThumbnailImageUID(QString seriesUID);
	bool saveThumbnailToDB(QString seriesUID, const QImage &thumbnail);
	std::unique_ptr<QStandardItemModel, decltype(&QScopedPointerDeleteLater::cleanup) > findAllPatientsShallow();

	[[nodiscard]] quantx::series::SeriesDescriptionUtils *seriesDescUtil() { return &seriesUtils; } //!< Getter for the series description utils object associated with the current SQL service.

	void loadSeries(const QString& seriesId, const QUANTX::Modality& modality);

	SqlDatabaseServiceError saveOverlayToDb( const QPixmap& pixmap, const overlay::OverlayInfo& overlay );
    SqlDatabaseServiceError removeOverlayFromDb( const overlay::OverlayInfo& overlay );

	SqlDatabaseServiceError savePluginDataToDb( const PluginInfo& info );
	SqlDatabaseServiceError loadPluginDataFromCurrentPatientMrn( const QString& patientMrn, QVector<PluginInfo>* pluginInfo );

	SqlDatabaseServiceError loadAcquisitionProtocols( QVector<acquisition::AcquisitionProtocolInfo>* protocols );
signals:
	/*!
	 * \brief SqlDatabaseService::updatingDatabaseBegin
	 *
	 * Signals listeners of the beginning of the db upgrade migration.
	 */
	void updatingDatabaseBegin();

	/*!
	 * \brief SqlDatabaseService::updatingDatabaseFinish
	 *
	 * Signals listeners of the ending of the db upgrade migration.
	 */
	void updatingDatabaseFinish();

	void openImageLoadProgressUpdated(int); //!< Fires when load progress has been updated.
	void openImageLoadProgressNewMaximum(int); //!< Fires when a new max value for the computation of progress has been determined.

	/*!
	 * \brief Fires when a new series thumbnail is created.
	 * \param seriesUID UID of the series.
	 * \param thumbnail Reference to the thumbnail.
	 */
	void seriesThumbnailCreated(QString seriesUID, const QImage &thumbnail);

	/*!
	 * \brief Fires when an image load starts.
	 * \param modality_ Modality of the image in question.
	 */
	void beginImageLoad(QUANTX::Modality modality_);
#ifdef QI_INTERNAL_TIMING_OUTPUT
	void startLoadingMRSeries(QiMriImage *mriImage);
#endif

	/*!
	 * \brief Fires when loading an Qi3DImage.
	 */
	void imageLoading(Qi3DImage *);

	/*!
	 * \brief Fires when loading an Qi2DImage.
	 */
	void imageLoading(Qi2DImage *);

	/*!
	 * \brief Fires when loading a collection of 2D Images.
	 */
	void imageLoading(QVector<Qi2DImage *>);

	/*!
	 * \brief Fires when a corrupt image is found during the load.
	 * \param titleOfError Title of the error.
	 * \param textOfError Text of the error.
	 * \param onAcceptAction Any functor that can execute when the user accepts the action to be executed when such a corrupt image is found.
	 */
	void corruptImageFound(QString titleOfError, QString textOfError, std::function<void(QWidget*)> onAcceptAction);


public slots:
	void onDatabaseSettingsChanged( const DatabaseSettings& dbSettings );
	void rebuildIndexes();

private:
	// Series desc. text mapping util
	quantx::series::SeriesDescriptionUtils seriesUtils; //!< Stores the description info.
	DatabaseSettings databaseSettings_; //!< Stores the DB settings
	std::unique_ptr<SqlDatabaseServiceStatements> sqlStatements; //!< Stores the DB service statements.
	std::unique_ptr<SqlDatabaseServiceImpl> impl; //!< Stores the internal impl pointer.
	int databaseVersionId; //!< Stores the DB version.
	const QSettings &originalDbSettings_; //!< Stores the original DB settings
	bool useResearchLicenseMode_; //!< Stores the research license mode flag

	void initializeDescriptionMappings();

	SqlDatabaseServiceError initializeConnection();
	SqlDatabaseServiceError checkDatabaseVersionAndUpradeIfNecessary();
	SqlDatabaseServiceError initializeSession();
	SqlDatabaseServiceError mapClassesToTables();

	bool connectAndOpenQDatabase( QSqlDatabase* db );

	SqlDatabaseServiceError updateDatabase(int databaseVersion );
	SqlDatabaseServiceError updateDatabaseHelper(int prevDbVersion, int newDbVersion, QStringList statements);
	SqlDatabaseServiceError updateFromVerXToVerY( const int verX, const int verY, QStringList sqlStatementList );
	SqlDatabaseServiceError updateFromVer200ToVer201();

	QMultiHash<QString, QString> extractAllDicomFields( const QString& imageUID );
	QMultiHash<QString, QString> qSqlExtractAllDicomFields( const QString& imageUID, SqlDatabaseServiceError* error );

#if QUANTX_BUILD_WITH_TESTS
	friend class QISqlTest;
#endif
};

}
}

#include "model/patient.h"
#include "model/dicomstudy.h"

#endif // SQLDATABASESERVICE_H
