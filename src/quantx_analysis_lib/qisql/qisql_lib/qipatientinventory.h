#ifndef QIPATIENTINVENTORY_H
#define QIPATIENTINVENTORY_H

#include <QObject>
#include "qisqlDllCheck.h"
#include "../../quantxdata/quantxdata_lib/quantx.h"
#include <QVector>
#include <QStringList>
#include <QString>
#include <QList>
#include <QImage>
#include "qiseriesinfo.h"

/*!
 * \class QiPatientInventory
 * \ingroup SQLModule
 * \brief Class for loading and managing the inventory of the patient data.
 * \note This will likely be either deprecated in future, or at least refactored to use the sql service.
 */
class QISQL_EXPORT QiPatientInventory : public QObject
{
	Q_OBJECT

public:
	QiPatientInventory(QString patientMRN=QString(), const QStringList &dbSettings=QStringList(), QObject *parent = nullptr);
	int buildInventory(QString patientMRN);
	int updateInventory();
	const QStringList & getDBSettings(){return databaseSettings;} //!< Getter for DB settings.
	void setDBSettings(const QStringList &dbSettings);
	QImage getThumbnail(int studyNum, int seriesNum);
	QImage buildThumbnail(const QString&, int thumbwidth, QUANTX::Modality);
	QString chooseImageUIDForThumbnail(QiSeriesInfo);
	QStringList getImageUIDsForSeries(const QiSeriesInfo&);
	int buildThumbnails(int thumbSize);

Q_SIGNALS:
	/*!
	 * \brief Fired when the entire inventory build is complete. \note Currently DISABLED so do not use unless reconnected.
	 */
	void inventoryFinished();

	/*!
	 * \brief Fired when the inventory list is finished being built.
	 */
	void inventoryListFinished();

	/*!
	 * \brief Fires when a new series is added and the inventory is updated.
	 * \pre The inventory is built already, but a new series is being added now.
	 * \param studyIdx Index of the study added to.
	 * \param seriesIdx Index of the series added.
	 * \param newStudy Whether it's a new study or not.
	 */
	void newSeriesAdded(int studyIdx, int seriesIdx, bool newStudy);

public Q_SLOTS:
	QString getPatientName();
	QDate getPatientDOB();
	QString getMRN();
	QStringList const getStudies();
	QStringList const getAccessions();
	QStringList const getReferringPhysicians();
	QStringList const getRequestingPhysicians();
	QStringList getAllSeries();
	QVector<QiSeriesInfo> getSeriesInStudy(const QString&);
	QVector<QiSeriesInfo> getSeriesInStudy(int);
	QStringList getSeriesUIDInStudy(const QString&);
	QStringList getSeriesUIDInStudy(int);
	int getNumStudies();
	int getNumSeriesinStudy(const QString&);
	int getNumSeriesinStudy(int);
	//QImage getSeriesThumbnail(QiSeriesInfo);



private:
	QString patientName; //!< Stores patient name.
	QDate patientDOB; //!< Stores patient date of birth.
	QString mrn; //!< Stores MRN.
	QStringList studies; //!< Stores list of study UIDs.
	QStringList accessions; //!< Stores list of accessions.
	QStringList referringPhysicians; //!< Stores list of referring physicians.
	QStringList requestingPhysicians; //!< Stores list of requesting physicians.
	QVector< QVector<QiSeriesInfo> > series; //!< Stores all series.
	QVector< QVector< QImage> > thumbnails; //!< Stores all thumbnail images.
	int numStudies{}; //!< Stores the number of studies.
	QVector<int> numSeries; //!< Stores the number of series per study.
	int buildPatientInfo();
	int buildStudiesList();
	QVector<QiSeriesInfo> buildSeriesList(const QString&);
	bool updateStudiesList();
	QImage getCachedThumbnail(const QString &, int thumbwidth);
	bool insertThumbnail(const QString&, const QImage&);
	QStringList databaseSettings; //!< Stores the DB settings.




};

#endif // QIPATIENTINVENTORY_H
