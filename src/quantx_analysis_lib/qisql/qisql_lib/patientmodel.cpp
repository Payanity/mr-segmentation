#include "patientmodel.h"
#include "treeitem.h"
#include "qisql.h"
#include "sqldatabaseservice.h"
#include "quantx.h"
#include "overlayinfo.h"
#include "model/patient.h"
#include "model/dicomstudy.h"
#include "model/dicomseries.h"
#include <QDate>
#include <Wt/WDate.h>
#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/backend/Sqlite3.h>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

using namespace quantx::patient;
using namespace quantx::dicom;
using namespace quantx;
using namespace quantx::overlay;

/*!
 * \brief Constructor that takes a parent QObject*.
 * \param parent The parent object to parent this model with. Default is nullptr.
 */
PatientModel::PatientModel(QObject *parent)
    : QAbstractItemModel(parent),
     rootItem(std::make_unique<TreeItem>(TreeItem({"Series UID", "Series Num", "Modality", "Description", "Date", "Time", "# Timepoints", "Orientation", "Slice Spacing (mm)"})))
{
}

/*!
 * \brief Getter for the patient name value.
 * \return The patient name from the model, or an empty string if not yet populated with data.
 */
QString PatientModel::patientName() const
{

	if (hasIndex(0, 0, QModelIndex())){
		return data(index(0, 0, QModelIndex())).toString();
	}
	return QString();
}

/*!
 * \brief Getter for the patient MRN.
 * \return The patient MRN or an empty string if not yet populated with data.
 */
QString PatientModel::patientMrn() const
{

	if (hasIndex(0, 1, QModelIndex())){
		return data(index(0, 1, QModelIndex())).toString();
	}
	return QString();
}

/*!
 * \brief Getter for the patient DOB.
 * \return The patient DOB or an empty string if not yet populated with data.
 */
QDate PatientModel::patientDob() const
{

	if (hasIndex(0, 2, QModelIndex())){
		return data(index(0, 2, QModelIndex())).toDate();
	}
	return QDate();
}

/*!
 * \brief Getter for the patient studies.
 * \return The patient studies list, empty list if no data pouplated yet.
 */
QStringList PatientModel::studies() const
{

	QStringList studies;
	for (int studyIdx = 0; studyIdx < numStudies(); studyIdx++){
		studies << data(studyIndex(studyIdx)).toString();
	}
	return studies;
}

/*!
 * \brief Getter for the number of studies associated with this patient.
 * \return Number of studies.
 */
int PatientModel::numStudies() const
{
	return rowCount(patientIndex());
}

/*!
 * \brief Getter for the number of series in a given study for this patient.
 * \param studyUID The UID of the study in question.
 * \return The number of series in the study being checked against.
 */
int PatientModel::numSeriesInStudy(const QString &studyUID) const
{
	return numSeriesInStudy(studyRow(studyUID));
}

/*!
 * \brief Getter for the number of series in the study we are querying.
 * \param studyIdx Index of study to query.
 * \return Number of series associated with the study.
 */
int PatientModel::numSeriesInStudy(int studyIdx) const
{
	if (studyIdx >= 0 && studyIdx < numStudies()){
		return rowCount(studyIndex(studyIdx));
	}
	return 0;
}

/*!
 * \brief Getter for accessions associated with this patient.
 * \return List of accessions, empty if there aren't any.
 */
QStringList PatientModel::accessions() const
{

	QStringList studies;
	for (int studyIdx = 0; studyIdx < numStudies(); studyIdx++){
		studies << data(studyIndex(studyIdx).siblingAtColumn(3)).toString();
	}
	return studies;
}

/*!
 * \brief Getter for row index for a study of given UID.
 * \param studyUID UID of study to query on.
 * \return Index of study row, -1 if not found.
 */
int PatientModel::studyRow(QString studyUID) const
{
	int matchedStudy = -1;
	for (int row = 0; row < numStudies(); row++){
		if (studyUID == data(studyIndex(row)).toString()){
			matchedStudy = row;
			break;
		}
	}
	return matchedStudy;
}

/*!
 * \brief Getter for study date of a particular study.
 * \param studyUID UID of study to query for.
 * \return Date of study, empty QDate object if not found.
 */
QDate PatientModel::studyDate(QString studyUID) const
{
	int studyIdx = studyRow(studyUID);
	if (studyIdx == -1){
#if useDebug
		qDebug() << "Study not found..";
#endif
		return QDate();
	}
	return data(studyIndex(studyIdx).siblingAtColumn(1)).toDate();
}

/*!
 * \brief Getter for list of series UIDs in study.
 * \param studyUID UID of study to query on.
 * \return QStringList of series UIDs, empty QStringList if none found.
 */
QStringList PatientModel::seriesUidInStudy(const QString &studyUID) const
{
	return seriesUidInStudy(studyRow(studyUID));
}

/*!
 * \brief Getter for list of series UIDs in study.
 * \param studyIdx Index of study to query on.
 * \return QStringList of series UIDs, empty QStringList if none found.
 */
QStringList PatientModel::seriesUidInStudy(int studyIdx) const
{
	if ( (studyIdx >=0) && (studyIdx < numStudies()) ){
		QStringList seriesUIDs;
		for (int seriesIdx=0; seriesIdx < rowCount(studyIndex(studyIdx)); seriesIdx++){
			seriesUIDs << data(seriesIndex(studyIdx, seriesIdx)).toString();
		}
		return seriesUIDs;
	}
	return QStringList();
}

/*!
 * \brief Checks if the specified dicom series is in the specified study of the currently loaded patient model
 * \param studyUID UID of the study to check if the series exists
 * \param seriesUID UID of the series to check if it exists in the specified study
 * \return Returns true is the given seriesUID is in the specified study, otherwise returns false
 */
bool PatientModel::isSeriesInStudy(const QString &studyUID, const QString &seriesUID) const
{
	bool seriesExists = false;
	auto series = seriesInStudy(studyUID);
	for (const auto & serie : series){
		if (serie.seriesUID() == seriesUID){
			seriesExists = true;
			break;
		}
	}
	return seriesExists;
}

/*!
 * \brief Getter for vector of series info objects for a given study.
 * \param studyUID UID of study to query on.
 * \return Vector of series info objects, empty vector if none found.
 */
QVector<QiSeriesInfo> PatientModel::seriesInStudy(const QString &studyUID) const
{
	return seriesInStudy(studyRow(studyUID));
}

/*!
 * \brief Getter for series in a given study.
 * \param studyIdx Index of study to query.
 * \return Vector of QiSeriesInfo objects, empty if none found.
 */
QVector<QiSeriesInfo> PatientModel::seriesInStudy(int studyIdx) const
{
	if ( (studyIdx >=0) && (studyIdx < numStudies()) ){
		QVector<QiSeriesInfo> series;
		for (int seriesIdx=0; seriesIdx < rowCount(studyIndex(studyIdx)); seriesIdx++){
			QModelIndex seriesRoot = seriesIndex(studyIdx, seriesIdx);
			QiSeriesInfo serie(data(seriesRoot).toString());
			serie.setStudyUID(data(seriesRoot.parent()).toString());
			serie.setSeriesDesc(data(seriesRoot.siblingAtColumn(3)).toString());
			serie.setDate(data(seriesRoot.siblingAtColumn(4)).toDate());
			serie.setModality(data(seriesRoot.siblingAtColumn(2)).toString());
			serie.setNumTimePts(data(seriesRoot.siblingAtColumn(6)).toInt());
			serie.setTime(data(seriesRoot.siblingAtColumn(5)).toTime());
			serie.setImageOrientation(data(seriesRoot.siblingAtColumn(7)).value<QVector<int>>());
			series << serie;
		}
		return series;
	}
	return QVector<QiSeriesInfo>();
}

/*!
 * \brief Getter for series descriptions for a given study UID.
 * \param studyUID UID of study to query on.
 * \return QStringList of descriptions, empty if none found.
 */
QStringList PatientModel::seriesDescriptions(QString studyUID) const
{
	QStringList descriptions;

	int studyIdx = studyRow(studyUID);
	if (studyIdx == -1){
#if useDebug
		qDebug() << "Study not found..";
#endif
		return descriptions;
	}

	for (int seriesIdx=0; seriesIdx < numSeriesInStudy(studyIdx); seriesIdx++){
		descriptions << data(seriesIndex(studyIdx, seriesIdx).siblingAtColumn(3)).toString();
	}

	return descriptions;
}

/*!
 * \brief Get the most recent study UID of the specified modality in the patient model
 * \param The modality for which to find the most recent study (e.g., MR, MG, US)
 * \return The study UID of the most recent study of the specified modality in the patient model
 */
QString PatientModel::mostRecentStudy(QString modality) const
{
	QString studyUid;
	for (int i=0; i < numStudies(); i++){
		auto studyIdx = studyIndex(i);
		if (data(studyIdx.siblingAtColumn(4)).toString() == modality){
			studyUid = data(studyIdx).toString();
			break;
		}
	}
	return studyUid;
}

/*!
 * \brief Getter for vector of OverlayInfo objects for a given series.
 * \param seriesUID UID of series to query on.
 * \return Vector of overlay info objects, emprty if none found.
 */
QVector<OverlayInfo> PatientModel::seriesOverlays(const QString& seriesUID) const
{
	QVector<OverlayInfo> overlays;
	auto seriesModelIndex = seriesIndex( seriesUID );
	if( seriesModelIndex.isValid() ) {
		static constexpr auto overlayParentRow = 1;
		static constexpr auto overlayParentCol = 0;
		auto overlayParentIndex = index( overlayParentRow, overlayParentCol, seriesModelIndex );
		if( overlayParentIndex.isValid() ) {
			auto parentTreeItem = getItem( overlayParentIndex );
			if( parentTreeItem ) {
				for( auto overlayInd = 0; overlayInd < parentTreeItem->childCount(); ++overlayInd ) {
					auto overlayTreeItem = parentTreeItem->child( overlayInd );
					if( overlayTreeItem ) {
						static constexpr auto dataUUIDIndex = 0;
						static constexpr auto dataOverlayTypeNameIndex = 1;
						static constexpr auto dataImageNumberIndex = 2;
						static constexpr auto dataImageIndex = 3;
						static constexpr auto dataIndex = 4;

						OverlayInfo overlay;
						overlay.UUID = overlayTreeItem->data( dataUUIDIndex ).toString();
						overlay.mrn = this->patientMrn();
						auto seriesTreeItem = getItem( seriesModelIndex );
						if( seriesTreeItem ) {
							auto studyTreeItem = seriesTreeItem->parentItem();
							if( studyTreeItem ) {
								static constexpr auto studyUIDIndex = 0;
								static constexpr auto accessionNumberIndex = 3;
								overlay.studyUID = studyTreeItem->data( studyUIDIndex ).toString();
								overlay.accessionNumber = studyTreeItem->data( accessionNumberIndex ).toString();
							}
						}
						overlay.seriesUID = seriesUID;
						overlay.overlayTypeName = overlayTreeItem->data( dataOverlayTypeNameIndex ).toString();
						overlay.imageNumber = overlayTreeItem->data( dataImageNumberIndex ).toInt();
						overlay.overlayData = overlayTreeItem->data( dataIndex ).toByteArray();

						emit snapshotLoaded( overlayTreeItem->data( dataImageIndex ).value<QPixmap>() );

						overlays.append( overlay );
					}
				}
			}
		}
	}

	return overlays;
}

/*!
 * \brief Call this method to let the patient model know abouut a new overlay.
 * \param pixmap
 * \param overlay
 */
void PatientModel::newOverlayAdded( const QPixmap& pixmap, const overlay::OverlayInfo &overlay)
{
	auto seriesModelIndex = seriesIndex( overlay.seriesUID );
	if( seriesModelIndex.isValid() ) {
		static constexpr auto overlayParentRow = 1;
		static constexpr auto overlayParentCol = 0;
		auto overlayParentIndex = index( overlayParentRow, overlayParentCol, seriesModelIndex );
		if( overlayParentIndex.isValid() ) {
			auto overlayParentItem = getItem( overlayParentIndex );
			if( overlayParentItem ) {
				overlayParentItem->appendChild( new TreeItem{ {
				                                                  overlay.UUID,
				                                                  overlay.overlayTypeName,
				                                                  overlay.imageNumber,
				                                                  pixmap,
				                                                  overlay.overlayData
				                                              } } );
			}
		}
	}
}

/*!
 * \brief Getter for series QModelIndex on the model.
 * \param seriesUID UID of series to query on.
 * \return index for series, empty QModelIndex if not found.
 */
QModelIndex PatientModel::seriesIndex(QString seriesUID) const
{
	for (int studyIdx = 0; studyIdx < numStudies(); studyIdx++){
		if (auto seriesIdx = seriesUidInStudy(studyIdx).indexOf(seriesUID); seriesIdx !=-1)
			return seriesIndex(studyIdx, seriesIdx);
	}
	return QModelIndex();
}

/*!
 * \brief Getter for series thumbnail.
 * \param seriesUID UID of series to query on.
 * \param maxDim Max dimension value to query on.
 * \return QImage thumbnail, blank QImage if none found.
 */
QImage PatientModel::seriesThumbnail(const QString &seriesUID, int maxDim) const
{
	constexpr int maxDimColumn{3};
	constexpr int imageColumn{4};
	auto seriesModelIndex = seriesIndex(seriesUID);
	if (seriesModelIndex.isValid()){
		static constexpr auto thumbParentRow = 0;
		static constexpr auto thumbParentCol = 0;
		auto thumbParentIndex = index( thumbParentRow, thumbParentCol, seriesModelIndex );
		if( thumbParentIndex.isValid() ) {
			auto thumbParentItem = getItem( thumbParentIndex );
			for( auto childIndex = 0; childIndex < thumbParentItem->childCount(); ++childIndex ) {
				const auto& childItem = thumbParentItem->child( childIndex );
				const auto& childMaxDim = childItem->data( maxDimColumn ).toInt();
				if( childMaxDim == maxDim ) {
					return childItem->data( imageColumn ).value<QImage>();
				}
			}
//			auto matchIndexList = match( thumbParentIndex, Qt::DisplayRole, maxDim, 1, Qt::MatchExactly);
//			if (!matchIndexList.isEmpty()){
//				auto image = data(matchIndexList.first().siblingAtColumn(imageColumn)).value<QImage>();
//				return image;
//			}
		}
	}
	return QImage();
}
/*!
 * \brief Gets all the images in a series using the seriesUID and places them in a vector.
 * \param seriesUID UID of series to query on.
 * \return QVector<QImage> of the images in a series.
 */
QVector<QImage> PatientModel::seriesThumbnails(const QString &seriesUID) const
{
	QVector<QImage> fullSizedImages;
	constexpr int maxDimColumn{3};
	constexpr int imageColumn{4};
	auto seriesModelIndex = seriesIndex(seriesUID);
	if (seriesModelIndex.isValid()){
		static constexpr auto thumbParentRow = 0;
		static constexpr auto thumbParentCol = 0;
		auto thumbParentIndex = index( thumbParentRow, thumbParentCol, seriesModelIndex );
		if( thumbParentIndex.isValid() ) {
			auto thumbParentItem = getItem( thumbParentIndex );
			for( auto childIndex = 0; childIndex < thumbParentItem->childCount(); ++childIndex ) {
				const auto& childItem = thumbParentItem->child( childIndex );
				const auto& childMaxDim = childItem->data( maxDimColumn ).toInt();
				if(childMaxDim>100) {
					  fullSizedImages.append(childItem->data( imageColumn ).value<QImage>());
				}
			}
//			auto matchIndexList = match( thumbParentIndex, Qt::DisplayRole, maxDim, 1, Qt::MatchExactly);
//			if (!matchIndexList.isEmpty()){
//				auto image = data(matchIndexList.first().siblingAtColumn(imageColumn)).value<QImage>();
//				return image;
//			}
		}
	}
	return fullSizedImages;
}

/*!
 * \brief Call when new thumbnail added to patient.
 * \param seriesUID UID of series the thumbnail belongs to.
 * \param thumbnail QImage to add.
 */
void PatientModel::newThumbnailAdded(QString seriesUID, const QImage &thumbnail)
{
	auto seriesModelIndex = seriesIndex(seriesUID);
	if (seriesModelIndex.isValid()){
		static constexpr auto thumbParentRow = 0;
		static constexpr auto thumbParentCol = 0;
		auto thumbParentIndex = index( thumbParentRow, thumbParentCol, seriesModelIndex );
		if( thumbParentIndex.isValid() ) {
			bool success = insertRows(rowCount(thumbParentIndex), 1, thumbParentIndex);
			if (success){
				auto imageMinMaxDims = std::minmax(thumbnail.width(), thumbnail.height());
				auto newThumbIndex = index(rowCount(thumbParentIndex) - 1, 0, thumbParentIndex);
				success = setData(newThumbIndex, thumbnail.width());
				success = setData(newThumbIndex.siblingAtColumn(1), thumbnail.height());
				success = setData(newThumbIndex.siblingAtColumn(2), imageMinMaxDims.first);
				success = setData(newThumbIndex.siblingAtColumn(3), imageMinMaxDims.second);
				success = setData(newThumbIndex.siblingAtColumn(4), thumbnail);
				emit dataChanged(newThumbIndex, newThumbIndex.siblingAtColumn(4));
			}
		}
	}
}

/*!
 * \brief Getter for header data for the model.
 * \param section Section index to query on.
 * \param orientation Modality orientation to query on.
 * \param role Qt display role index to query on.
 * \return Data found, or empty QVariant if nothing found.
 */
QVariant PatientModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return rootItem->data(section);

	return QVariant();
}

/*!
 * \brief Getter for QModelIndex object for a given query.
 * \param row Row in the model to query on.
 * \param column Column in the model to query on.
 * \param parent Parent model index (model is a tree structure).
 * \return Either the index or an empty one if none found.
 */
QModelIndex PatientModel::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	TreeItem *parentItem;

	if (!parent.isValid())
		parentItem = rootItem.get();
	else
		parentItem = static_cast<TreeItem*>(parent.internalPointer());

	TreeItem *childItem = parentItem->child(row);
	if (childItem)
		return createIndex(row, column, childItem);
	return QModelIndex();
}

/*!
 * \brief Getter for parent by QModelIndex.
 * \param index Index to query on.
 * \return QModelIndex of the parent item, empty if either the passed index is not valid, or the parentItem found is the root item.
 */
QModelIndex PatientModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
	TreeItem *parentItem = childItem->parentItem();

	if (parentItem == rootItem.get())
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}

/*!
 * \brief Getter for the TreeItem of the model structure based on the passed index to query on.
 * \param index QModelIndex to query on.
 * \return Either the item, if the index is valid and the item is found, root item otherwise.
 */
TreeItem *PatientModel::getItem(const QModelIndex &index) const
{
	if (index.isValid()){
		TreeItem *item = static_cast<TreeItem *>(index.internalPointer());
		if (item)
			return item;
	}
	return rootItem.get();
}

/*!
 * \brief Get row count for given parent QModelIndex.
 * \param parent Index object to query on.
 * \return 0 if the parent column is > 0, the root item child count if there's no valid parent, otherwise the parent item child count.
 */
int PatientModel::rowCount(const QModelIndex &parent) const
{
	TreeItem *parentItem;
	if (parent.column() > 0)
		return 0;

	if (!parent.isValid())
		parentItem = rootItem.get();
	else
		parentItem = static_cast<TreeItem*>(parent.internalPointer());

	return parentItem->childCount();
}

/*!
 * \brief Getter for the column count on this model, querying by parent node.
 * \param parent QModelIndex to query for column count.
 * \return Either the column count for the parent passed in, or the root item column count if the parent is invalid.
 */
int PatientModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
	return rootItem->columnCount();
}

/*!
 * \brief Get data from the model based on an index and role.
 * \param index QModelIndex to query on.
 * \param role Role identifier to query on.
 * \return Empty QVariant if the index is invalid, or the role is not Qt::DisplayRole, otherwise returns the TreeItem::data for the provided index.
 */
QVariant PatientModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role != Qt::DisplayRole)
		return QVariant();

	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

	return item->data(index.column());
}

/*!
 * \brief Setter for data, mapped via QModelIndex and role.
 * \param index QModelIndex passed to map the data to.
 * \param value Data to store.
 * \param role Role to check before mapping.
 * \return Success of storing the data.
 * \note The role checking and signal emission of this method are commented out, and should probably be removed altogether, as well as refactoring the method signature.
 */
bool PatientModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
//    if (role != Qt::EditRole)
//        return false;

	TreeItem *item = getItem(index);
	bool result = item->setData(index.column(), value);

//    if (result)
//        emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});

	return result;
}

/*!
 * \brief Insert rows into the model.
 * \param row Index of row on which to begin the insert.
 * \param count Number of rows being inserted.
 * \param parent The parent QModelIndex upon which to start the insert.
 * \return Success of the insert operation.
 */
bool PatientModel::insertRows(int row, int count, const QModelIndex &parent)
{
	TreeItem *parentItem = getItem(parent);
	if (!parentItem)
		return false;
	beginInsertRows(parent, row, row + count - 1);
	const bool success = parentItem->insertChildren(row,
	                                                count,
	                                                rootItem->columnCount());
	endInsertRows();

	return success;
}

/*!
 * \brief Insert columns into the model.
 * \param column Column index to start insertion at.
 * \param count Count of columns being inserted.
 * \param parent QModelIndex upon which to begin the insertion.
 * \return False value, implying that success will never happen.
 * \note This method is likely not done being implemented, and should be used little, if at all, until more testing and dev work has been completed.
 */
bool PatientModel::insertColumns(int column, int count, const QModelIndex &parent)
{
	beginInsertColumns(parent, column, column + count - 1);
	// FIXME: Implement me!
	endInsertColumns();
	return false;
}

/*!
 * \brief PatientModel::beginPopulateModel
 * \param svcDb Pointer to the db service, checked to determine if we proceed or no-op.
 *
 * This method, like the end method and rootItem getter, are only intended to be called from the SERVICE, so that no one
 * will likely accidentally call them from anywhere else without a no-op occurring.
 */
void PatientModel::beginPopulateModel(sql::SqlDatabaseService *svcDb)
{
	if (svcDb == nullptr) {
		return; // We check for null here and return if it is, because we really only want these populate lifecycle methods being called from the service as needed
	}
	beginResetModel();
}

/*!
 * \brief PatientModel::endPopulateModel
 * \param svcDb Pointer to the db service, checked to determine if we proceed or no-op.
 *
 * This method, like the start method and rootItem getter, are only intended to be called from the SERVICE, so that no one
 * will likely accidentally call them from anywhere else without a no-op occurring.
 */
void PatientModel::endPopulateModel(sql::SqlDatabaseService *svcDb)
{
	if (svcDb == nullptr) {
		return; // We check for null here and return if it is, because we really only want these populate lifecycle methods being called from the service as needed
	}
	endResetModel();
}

/*!
 * \brief PatientModel::getRootItem
 * \param svcDb Pointer to the db service, checked to determine if we proceed or no-op.
 * \return Pointer to the rootItem if any.
 *
 * This method, like the start method and rootItem getter, are only intended to be called from the SERVICE, so that no one
 * will likely accidentally call them from anywhere else without a no-op occurring.
 */
TreeItem *PatientModel::getRootItem(sql::SqlDatabaseService *svcDb)
{
	if (svcDb == nullptr) {
		return nullptr; // We check for null here and return nullptr if it is, because we really only want these populate lifecycle methods being called from the service as needed
	}
	return rootItem.get();
}

/*!
 * \brief PatientModel::findModality
 * \param modality The modality we are lookning for.
 * \param seriesDesc The series description as mapped for a given brand of modality.
 * \param seriesMrDate The date on which the series was generated.
 * \param sortOnSeriesNumber Whether or not to sort by series number when searching for a given modality.
 * \return A bool that indicates whether a given modality search was productive.
 */
bool PatientModel::findModality(const QUANTX::Modality &modality, const QString &seriesDesc, const QString &seriesMrDate,
                           bool sortOnSeriesNumber)
{
	bool hasModality = false;

	for (const auto& study : studies()) {
		auto seriesForStudy = seriesInStudy(study);

		if (sortOnSeriesNumber) {
			// Sort the series list on a series number

		}

		for (const auto& series : seriesForStudy) {
			hasModality = series.modality() == modality;

			if (!seriesDesc.isEmpty()) {
				if (seriesDesc.contains("*")){
					hasModality = hasModality && series.seriesDesc().contains(QRegularExpression(seriesDesc));
				} else {
					hasModality = hasModality && series.seriesDesc().contains(seriesDesc);
				}
			}

			if (!seriesMrDate.isEmpty()) {
				hasModality = hasModality && series.date() == QDate::fromString(seriesMrDate);
			}

			if (hasModality) {
				seriesFound = series;
				goto foundModality; // This is normally really bad, but one of the few, if not only acceptable use cases
				                    // is breaking out of nested loops and keeping the label close to the goto.
			}
		}
	}

foundModality:
	return hasModality;
}

/*!
 * \brief PatientModel::findModalityFromList
 * \param modality The modality we are lookning for.
 * \param seriesDesc A list of mappings to a given brand of modality.
 * \param mrDate The date on which the series was generated.
 * \return A bool indicating success or lack thereof for the search.
 */
bool PatientModel::findModalityFromList(const QUANTX::Modality &modality, const QStringList &seriesDesc,
                                        const QString &mrDate)
{
	for(const auto & desc : seriesDesc)
		if(findModality(modality, desc, mrDate, true))
			return true;
	return false;
}

/*!
 * \brief PatientModel::is3DSeries
 * \param seriesInfo An object reference to the series info we are checking
 * \return A bool indicating whether the series in question is 3d or not.
 */
bool PatientModel::is3DSeries(const QiSeriesInfo& seriesInfo)
{
	return (seriesInfo.spaceBetweenSlices().toFloat() > 0.0f);
}

/*!
 * \brief PatientModel::getDefaultSeriesInfo
 * \param mrDate The date we should look for the series on.
 * \param svcDb A pointer to the SqlDatabaseService we are using to find out the mappings for series description.
 * \return A std::pair<QString, QUANTX::Modality> that indicates the series UID to load, and the modality we should load for that series.
 */
std::pair<QString, QUANTX::Modality> PatientModel::getDefaultSeriesInfo(const QString &mrDate, const std::unique_ptr<sql::SqlDatabaseService>& svcDb)
{
	if(findModalityFromList(QUANTX::Modality::MRI, svcDb->seriesDescUtil()->t2SeriesDesc(), mrDate)) //Load T2 MR Image
		return {seriesFound.seriesUID(), QUANTX::Modality::MRI};
	QStringList mcDynSeriesDescList;
	for (auto mcDesc : svcDb->seriesDescUtil()->mcDynSeriesDesc()){
		auto mcPrefix = QString("^") + mcDesc.replace(".", "\\.") + ".*";
		for (const auto &serieDesc : svcDb->seriesDescUtil()->dynMultiSeriesDesc() + svcDb->seriesDescUtil()->dynSeriesDesc()){
			mcDynSeriesDescList << mcPrefix + serieDesc;
		}
	}
	if(findModalityFromList(QUANTX::Modality::MRI, mcDynSeriesDescList, mrDate)) //Load motion-corrected DCE MR Image
		return {seriesFound.seriesUID(), QUANTX::Modality::MRI};
	if(findModalityFromList(QUANTX::Modality::MRI, svcDb->seriesDescUtil()->dynMultiSeriesDesc() + svcDb->seriesDescUtil()->dynSeriesDesc(), mrDate)) //Load DCE MR Image
		return {seriesFound.seriesUID(), QUANTX::Modality::MRI};
	if(findModality(QUANTX::Modality::MRI, QString(), mrDate)) //Load First MR Image on last MR date (fallback, may cause a crash)
		return {seriesFound.seriesUID(), QUANTX::Modality::MRI};
//	if( findModality(QUANTX::Modality::MRI) ) //Load First MR Image found (fallback, may cause a crash)
//		return {seriesFound.seriesUID(), QUANTX::Modality::MRI};
	if(findModality(QUANTX::Modality::MAMMO)) //Load Mammo Image
		return {seriesFound.seriesUID(), (is3DSeries(seriesFound) ? QUANTX::MG3D : QUANTX::MAMMO) };
	if(findModality(QUANTX::Modality::US)) //Load US Image
		return {seriesFound.seriesUID(), (is3DSeries(seriesFound) ? QUANTX::US3D : QUANTX::US) };
	return {"", QUANTX::Modality::NOIMAGE};
}

/*!
 * \brief Find an MR series.
 * \param mrSubtype Subtype string for the MR series being searched for.
 * \param svcDb Pointer to the DB service to use.
 * \return UID of series being searched for.
 */
QString PatientModel::findMrSeries(const QString &mrSubtype, const std::unique_ptr<sql::SqlDatabaseService> &svcDb, bool mostRecentStudyOnly)
{
	QString seriesUID;
	QStringList seriesDescriptions;

	if (mrSubtype.startsWith("T2", Qt::CaseInsensitive)){
		seriesDescriptions = svcDb->seriesDescUtil()->t2SeriesDesc();
	} else if (mrSubtype.startsWith("Dyn", Qt::CaseInsensitive)){
		for (auto mcDesc : svcDb->seriesDescUtil()->mcDynSeriesDesc()){
			auto mcPrefix = QString("^") + mcDesc.replace(".", "\\.") + ".*";
			for (const auto &serieDesc : svcDb->seriesDescUtil()->dynSeriesDesc() + svcDb->seriesDescUtil()->dynMultiSeriesDesc()){
				seriesDescriptions << mcPrefix + serieDesc;
			}
		}
		seriesDescriptions += svcDb->seriesDescUtil()->dynSeriesDesc() + 
		                      svcDb->seriesDescUtil()->dynMultiSeriesDesc();
	} else{
		seriesDescriptions = mrSubtype.split(";");
	}
	if (!seriesDescriptions.empty()){
		if(findModalityFromList(QUANTX::Modality::MRI, seriesDescriptions, mostRecentStudyOnly ? studyDate(mostRecentStudy("MR")).toString() : QString()))
			seriesUID = seriesFound.seriesUID();
	}
	return seriesUID;
}

void PatientModel::insertNewSeries(const dicom::DicomSeries *newSeries)
{
	QVector<int> imageOrientation;
	if (newSeries->imageOrientation.has_value()){
		QStringList orientationStringList = QString::fromStdString(newSeries->imageOrientation.value()).split('\\');
		bool ok;
		for (auto orientation : orientationStringList){
			imageOrientation << orientation.toInt(&ok);
			if (!ok)
				imageOrientation.removeLast();
		}
	}
	insertNewSeries(QString::fromStdString(newSeries->studyuid.value()),
	                QString::fromStdString(newSeries->seriesuid),
	                QString::fromStdString(newSeries->seriesNumber.value_or(std::string())),
	                QString::fromStdString(newSeries->modality.value_or(std::string())),
	                QString::fromStdString(newSeries->seriesDesc.value_or(std::string())),
	                newSeries->date.has_value() ? QDate::fromJulianDay(newSeries->date.value().toJulianDay()) : QDate{},
	                newSeries->time.has_value() ? QTime::fromMSecsSinceStartOfDay(newSeries->time.value().toTimeDuration().count()) : QTime{},
	                newSeries->numTemporalPositions.value_or(0),
	                imageOrientation,
	                QString::fromStdString(newSeries->spaceBetweenSlices.value_or(std::string())));
}

void PatientModel::insertNewSeries(QString studyUID, QString seriesUID, QString seriesNumber, QString modality, QString seriesDesc, QDate seriesDate, QTime seriesTime, int numTempPos, QVector<int> imageOrientation, QString spaceBetweenSlices)
{
	auto studyModelIndex = studyIndex(studyRow(studyUID));
	if (studyModelIndex.isValid()){
		auto series = seriesInStudy(studyUID);
		auto newDateTime = QDateTime(seriesDate, seriesTime);
		int newIndex = rowCount(studyModelIndex);
		for (auto serie : series){
			if (QDateTime(serie.date(), serie.time()) > newDateTime){
				newIndex = seriesIndex(serie.seriesUID()).row();
			}
		}
		bool success = insertRows(newIndex, 1, studyModelIndex);
		if (success){
			auto newSeriesModelIndex = index(newIndex, 0, studyModelIndex);
			int col = 1;
			success = setData(newSeriesModelIndex, seriesUID);
			success = setData(newSeriesModelIndex.siblingAtColumn(col++), seriesNumber);
			success = setData(newSeriesModelIndex.siblingAtColumn(col++), modality);
			success = setData(newSeriesModelIndex.siblingAtColumn(col++), seriesDesc);
			success = setData(newSeriesModelIndex.siblingAtColumn(col++), seriesDate);
			success = setData(newSeriesModelIndex.siblingAtColumn(col++), seriesTime);
			success = setData(newSeriesModelIndex.siblingAtColumn(col++), numTempPos);
			success = setData(newSeriesModelIndex.siblingAtColumn(col++), QVariant::fromValue(imageOrientation));
			success = setData(newSeriesModelIndex.siblingAtColumn(col), spaceBetweenSlices);
			emit dataChanged(newSeriesModelIndex, newSeriesModelIndex.siblingAtColumn(col));
			//insert 2 sub rows to the new series for the thumbnails and the overlays
			success = insertRows(0, 2, newSeriesModelIndex);
			if(success){
				success = setData(index(0, 0, newSeriesModelIndex), "SeriesThumbnails");
				success = setData(index(1, 0, newSeriesModelIndex), "Overlays");
				//add extra columns needed when adding thumbnail
				int numExtraCols = 4;
				for (auto col = 1; col <= numExtraCols; col++){
					success = setData(index(0, 0, newSeriesModelIndex).siblingAtColumn(col), "");
					success = setData(index(1, 0, newSeriesModelIndex).siblingAtColumn(col), "");
				}
				emit dataChanged(index(0, 0, newSeriesModelIndex), index(1, 0, newSeriesModelIndex).siblingAtColumn(numExtraCols));
			}
			emit newSeriesAdded(*this, studyModelIndex.row(), newIndex, false);
		}
	}
}
