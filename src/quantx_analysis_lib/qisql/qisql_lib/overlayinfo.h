#ifndef OVERLAYINFO_H
#define OVERLAYINFO_H

#include <QString>
#include <QtCore>

#include "qisqlDllCheck.h"

namespace quantx {
namespace overlay {

/*!
 * \brief Enum option to delete the overlays.
 * \ingroup SQLModule
 */
enum RemoveOption
{
    None,
    All,
    Last
};

/*!
 * \brief Small class to encapsulate data for overlays.
 * \ingroup SQLModule
 */
struct QISQL_EXPORT OverlayInfo
{
	OverlayInfo() = default; //!< Defaulted constructor.
	~OverlayInfo() = default; //!< Defaulted destructor.

	QString UUID{}; //!< UUID for identifying this overlay info.
	QString mrn{}; //!< MRN (Medical Record Number) identifying the records that this overlay depends on.
	QString studyUID{}; //!< Study UID for the study represented by this overlay.
	QString seriesUID{}; //!< Series UID for the series represented by this overlay.
	QString accessionNumber{}; //!< Accession number represented by this overlay.
	QString overlayTypeName{}; //!< Overlay type name.
	int imageNumber{ -1 }; //!< Image number represented by this overlay.
	QByteArray overlayData{}; //!< Arbitrary binary data that is associated with this overlay.
    RemoveOption removeOption;
};

}
}

Q_DECLARE_METATYPE( quantx::overlay::OverlayInfo );


#endif // OVERLAYINFO_H
