#ifndef SQLDATABASESERVICESTATEMENTS_H
#define SQLDATABASESERVICESTATEMENTS_H

#include <QStringList>

/*!
 * \brief The SqlDatabaseServiceStatements class stores the hardcoded SQL statements we use for e.g. upgrading DB versions.
 * \class SqlDatabaseServiceStatements
 * \ingroup SQLModule
 * \note We should consider moving these SQL statements to some loaded SQL files or some such, to avoid keeping them in compiled binary like this.
 */
class SqlDatabaseServiceStatements
{
public:
	/*!
	 * \brief Defaulted constructor.
	 */
	SqlDatabaseServiceStatements() = default;

	/*!
	 * \brief Defaulted destructor.
	 */
	virtual ~SqlDatabaseServiceStatements() = default;

	virtual const QString currentDatabaseVersion() const;
	virtual const QString studyList() const;
	virtual const QString dicomHeader() const;
	virtual const QString updateToVersion( const int versionId, const int versionNum ) const;

	virtual const QStringList updateFromVer0ToVer1() const;
	virtual const QStringList updateFromVer1ToVer101() const;
	virtual const QStringList updateFromVer101ToVer110() const = 0;
	virtual const QStringList updateFromVer110ToVer200() const = 0;
	virtual const QStringList updateFromVer200ToVer201() const;
	virtual const QStringList updateFromVer201ToVer202() const;
	virtual const QStringList updateFromVer202ToVer300() const = 0;
	virtual const QStringList updateFromVer300ToVer301() const = 0;
	virtual const QStringList updateFromVer301ToVer302() const = 0;
	virtual const QStringList updateFromVer302ToVer303() const = 0;
	virtual const QStringList updateFromVer303ToVer304() const = 0;
	virtual const QStringList updateFromVer304ToVer310() const = 0;
	virtual const QStringList updateFromVer310ToVer320() const = 0;
};

/*!
 * \brief The SqlDatabaseServiceStatementsSqlite class stores the hardcoded SQL statements we use for e.g. upgrading DB versions specifically for SQLite.
 * \class SqlDatabaseServiceStatementsSqlite
 * \ingroup SQLModule
 * \note We should consider moving these SQL statements to some loaded SQL files or some such, to avoid keeping them in compiled binary like this.
 */
class SqlDatabaseServiceStatementsSqlite : public SqlDatabaseServiceStatements
{
public:
	SqlDatabaseServiceStatementsSqlite() = default;
	~SqlDatabaseServiceStatementsSqlite() override = default;

	const QStringList updateFromVer101ToVer110() const override;
	const QStringList updateFromVer110ToVer200() const override;
	const QStringList updateFromVer202ToVer300() const override;
	const QStringList updateFromVer300ToVer301() const override;
	const QStringList updateFromVer301ToVer302() const override;
	const QStringList updateFromVer302ToVer303() const override;
	const QStringList updateFromVer303ToVer304() const override;
	const QStringList updateFromVer304ToVer310() const override;
	const QStringList updateFromVer310ToVer320() const override;
};

/*!
 * \brief The SqlDatabaseServiceStatementsPostgres class stores the hardcoded SQL statements we use for e.g. upgrading DB versions specifically for Postgres.
 * \class SqlDatabaseServiceStatementsPostgres
 * \ingroup SQLModule
 * \note We should consider moving these SQL statements to some loaded SQL files or some such, to avoid keeping them in compiled binary like this.
 */
class SqlDatabaseServiceStatementsPostgres : public SqlDatabaseServiceStatements
{
public:
	SqlDatabaseServiceStatementsPostgres() = default;
	~SqlDatabaseServiceStatementsPostgres() override = default;

	const QStringList updateFromVer101ToVer110() const override;
	const QStringList updateFromVer110ToVer200() const override;
	const QStringList updateFromVer202ToVer300() const override;
	const QStringList updateFromVer300ToVer301() const override;
	const QStringList updateFromVer301ToVer302() const override;
	const QStringList updateFromVer302ToVer303() const override;
	const QStringList updateFromVer303ToVer304() const override;
	const QStringList updateFromVer304ToVer310() const override;
	const QStringList updateFromVer310ToVer320() const override;
};

#endif // SQLDATABASESERVICESTATEMENTS_H
