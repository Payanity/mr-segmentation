#include "dicomquery.h"
#include "gdcmDataSet.h"
#include "gdcmCompositeNetworkFunctions.h"
#include "gdcmBaseRootQuery.h"
#include "gdcmQueryFactory.h"
#include "gdcmValidate.h"
#include <gdcmAttribute.h>
#include "gdcmDict.h"
#include "gdcmDicts.h"
#include "gdcmGlobal.h"
#include "gdcmDataElement.h"
#include "gdcmStringFilter.h"
#include "gdcmFile.h"
#include "gdcmTag.h"
#include <iostream>
#include <sstream>
#include <QMap>

#define useDebug 0

#if useDebug
#include <QDebug>
#endif

using namespace quantx::mranalysis;

/*!
 * \brief Constructor that takes various params to initialize a DicomQuery object.
 * \param dicomHost The host for the DICOM query to run on.
 * \param dicomPort The port through which to contact the host.
 * \param aeTitle The AE title of the DICOM the query user we are making the query with.
 * \param callAeTitle The AE title of the provider (server) we are querying.
 * \param parent The object that will be the QObject::parent of this query object.
 */
DicomQuery::DicomQuery(QString dicomHost, uint dicomPort, QString aeTitle, QString callAeTitle, QObject *parent) : QObject(parent)
{
	dicomHost_ = dicomHost;
	dicomPort_ = static_cast<uint16_t>(dicomPort);
	aeTitle_ = aeTitle;
	callAeTitle_ =  callAeTitle.isEmpty() ? "ANY-SCP" : callAeTitle;
}

/*!
 * \brief Verifies the validity of the connection to the DICOM provider.
 * \return Boolean indicating status good vs bad connection.
 */
bool DicomQuery::verifyConnection()
{
	return gdcm::CompositeNetworkFunctions::CEcho( dicomHost_.toUtf8(), dicomPort_,
	                                               aeTitle_.toUtf8(), callAeTitle_.toUtf8() );
}

/*!
 * \brief Finds the number of images on the server for a given study and series UID combination.
 * \param studyUID UID of the study to query for.
 * \param seriesUID UID of the series to query for.
 * \return Number of images found.
 */
int DicomQuery::findNumImages(QString studyUID, QString seriesUID)
{
	gdcm::ERootType root = gdcm::eStudyRootType;
	gdcm::EQueryLevel level = gdcm::eSeries;
	auto numSeriesObjTag = gdcm::Tag(0x0020, 0x1209); //Num. series related instances -- Query/Retrieve-only attribute
	std::vector< std::pair<gdcm::Tag, std::string> > keys;
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000d), studyUID.toStdString()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000e), seriesUID.toStdString()));
	keys.push_back(std::make_pair(numSeriesObjTag,std::string()));
	auto query = gdcm::CompositeNetworkFunctions::ConstructQuery(root, level, keys);
	int numImages = -1;
	if (query->ValidateQuery(false))
	{
		std::vector<gdcm::DataSet> dataSets;
		if (gdcm::CompositeNetworkFunctions::CFind(dicomHost_.toUtf8(), dicomPort_, query, dataSets,
		                                           aeTitle_.toUtf8(), callAeTitle_.toUtf8()) &&
		    dataSets.size() > 0){
			gdcm::Attribute<0x0020, 0x1209> attrib;
			attrib.SetFromDataSet(dataSets.front());
			numImages = attrib.GetValue();
		}
	}
	return numImages;
}

/*!
 * \brief Find number of objects for a given study and list of series.
 * \param studyUID UID of the study.
 * \param seriesList QStringList of the series to query for.
 * \return QMap<QString, int> of counts mapped to their series.
 */
QMap<QString, int> DicomQuery::findNumObjects(QString studyUID, QStringList seriesList)
{
	QMap<QString, int> result;
	gdcm::ERootType root = gdcm::eStudyRootType;
	gdcm::EQueryLevel level = gdcm::eSeries;
	auto numSeriesObjTag = gdcm::Tag(0x0020, 0x1209); //Num. series related instances -- Query/Retrieve-only attribute
	auto numTemporalPositionTag = gdcm::Tag(0x0020, 0x0105);
	std::vector< std::pair<gdcm::Tag, std::string> > keys;
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000d), studyUID.toStdString()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000e), seriesList.join("\\").toStdString()));
	keys.push_back(std::make_pair(numSeriesObjTag,std::string()));
	keys.push_back(std::make_pair(numTemporalPositionTag,std::string()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0008, 0x0070),std::string()));
	auto query = gdcm::CompositeNetworkFunctions::ConstructQuery(root, level, keys);
	if (query->ValidateQuery(false))
	{
		std::vector<gdcm::DataSet> dataSets;
		if (gdcm::CompositeNetworkFunctions::CFind(dicomHost_.toUtf8(), dicomPort_, query, dataSets,
		                                           aeTitle_.toUtf8(), callAeTitle_.toUtf8()) &&
		    dataSets.size() > 0){
			gdcm::Attribute<0x0020, 0x1209> numImagesAttrib;
			gdcm::Attribute<0x0020, 0x000E> seriesUIDAttrib;
			for (auto dataSet : dataSets){
				numImagesAttrib.SetFromDataSet(dataSet);
				seriesUIDAttrib.SetFromDataSet(dataSet);
				// using c_str() instead just fromStdString gets rid of null terminating char
				std::string seriesUID = seriesUIDAttrib.GetValue();
				result.insert(QString::fromUtf8(seriesUID.c_str()), numImagesAttrib.GetValue());
			}
		}
	}
	for (auto series : seriesList)
		if (!result.contains(series))
			result.insert(series, 0);
	return result;
}

/*!
 * \brief Find number of images by study UID and list of series,
 * \param studyUID UID of the study to query for.
 * \param seriesList List of the series to query for on a given study.
 * \param quick Flag for whether to use a quicker, but possibly less robust search method.
 * \return QMap<QString, int> of all the series UIDs and image counts associated for this search.
 */
QMap<QString, int> DicomQuery::findNumImages(QString studyUID, QStringList seriesList, bool quick)
{
	QMap<QString, int> result;
	if (quick){ // quicker method, but possibly less robust
		QMap<QString, int> nonImages;
		for (auto series : std::as_const(seriesList)){
			nonImages.insert(series, findObjects(studyUID, series, "1.3*").count());
		}
		result = findNumObjects(studyUID, seriesList);
		for (auto key : result.keys())
			result.insert(key, result.value(key) - nonImages.value(key));
	} else {
		for (auto series : std::as_const(seriesList)){
			result.insert(series, findObjects(studyUID, series).count());
		}
	}
	return result;
}

/*!
 * \brief Find objects for a given study UID, series UID, and SOP class.
 * \param studyUID UID of study.
 * \param seriesUID UID of series.
 * \param sopClass SOP class to query.
 * \return QStringList of the objects found.
 */
QStringList DicomQuery::findObjects(QString studyUID, QString seriesUID, QString sopClass)
{
	// this query doesn't work at UofC (and maybe all Stentor PACS)
	QStringList imageUids;
	gdcm::ERootType root = gdcm::eStudyRootType;
	gdcm::EQueryLevel level = gdcm::eImage;
	auto imageUidTag = gdcm::Tag(0x0008, 0x0018);
	std::vector< std::pair<gdcm::Tag, std::string> > keys;
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000d), studyUID.toStdString()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000e), seriesUID.toStdString()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x0013), std::string()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0008, 0x0016), sopClass.toStdString())); // SOP Class
	keys.push_back(std::make_pair(imageUidTag, std::string()));
	auto query = gdcm::CompositeNetworkFunctions::ConstructQuery(root, level, keys);

	if (query->ValidateQuery(false))
	{
		std::vector<gdcm::DataSet> dataSets;
		if (gdcm::CompositeNetworkFunctions::CFind(dicomHost_.toUtf8(), dicomPort_, query, dataSets,
		                                           aeTitle_.toUtf8(), callAeTitle_.toUtf8()) &&
		    dataSets.size() > 0){
			int instanceNum;
			for (auto dataSet : dataSets){
				instanceNum = -1;
				gdcm::Attribute<0x0008, 0x0018> attrib;
				gdcm::Attribute<0x0020, 0x0013> instanceNumAttrib;
				gdcm::Attribute<0x0008, 0x0016> sopClassAttrib;
				attrib.SetFromDataSet(dataSet);
				instanceNumAttrib.SetFromDataSet(dataSet);
				sopClassAttrib.SetFromDataSet(dataSet);
				if (instanceNumAttrib.GetNumberOfValues() == 0){
#if useDebug
					qDebug().noquote() << QString("Series %1 has no instance num").arg(QString(attrib.GetValue()));
#endif
				}
				imageUids << QString(attrib.GetValue());
			}
		}
	}
	return imageUids;
}

/*!
 * \brief Find series for a study.
 * \param studyUID UID of study.
 * \param primaryOnly Find only PRIMARY image type series.
 * \param seriesDesc Series description.
 * \param seriesDescExcludeEndings List of exclude endings to filter on for series descriptions.
 * \param seriesDescExcludeContains List of exclude contains to filter series descriptions.
 * \return QVector<DicomSeriesInfo> about the various series found.
 */
QVector<DicomSeriesInfo> DicomQuery::findSeriesInStudy(QString studyUID, bool primaryOnly, QString seriesDesc, QStringList seriesDescExcludeEndings, QStringList seriesDescExcludeContains)
{
	QVector<DicomSeriesInfo> queryResult;
	gdcm::ERootType root = gdcm::eStudyRootType;
	gdcm::EQueryLevel level = gdcm::eSeries;
	auto numSeriesObjTag = gdcm::Tag(0x0020, 0x1209); //Num. series related instances
	std::vector< std::pair<gdcm::Tag, std::string> > keys;
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000D), studyUID.toStdString())); // studyUID
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000E), std::string())); // seriesUID
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x0011), std::string())); // series Number
	keys.push_back(std::make_pair(gdcm::Tag(0x0018, 0x0022), std::string())); // scan option
	keys.push_back(std::make_pair(gdcm::Tag(0x0028, 0x0030), std::string())); // pixel spacing
	keys.push_back(std::make_pair(gdcm::Tag(0x0018, 0x1030), std::string())); // protocol name
	keys.push_back(std::make_pair(gdcm::Tag(0x0008, 0x103E), seriesDesc.toStdString())); // series Description
	if (primaryOnly)
		keys.push_back(std::make_pair(gdcm::Tag(0x0008, 0x0008), std::string("*PRIMARY*"))); // Image Type
	keys.push_back(std::make_pair(numSeriesObjTag,std::string()));
	auto query = gdcm::CompositeNetworkFunctions::ConstructQuery(root, level, keys);

	if (query->ValidateQuery(false))
	{
		std::vector<gdcm::DataSet> dataSets;
		if (gdcm::CompositeNetworkFunctions::CFind(dicomHost_.toUtf8(), dicomPort_, query, dataSets,
		                                           aeTitle_.toUtf8(), callAeTitle_.toUtf8()) &&
		    dataSets.size() > 0){
			for (auto dataset : dataSets){
				gdcm::Attribute<0x0020, 0x000D> studyUIDAttrib;
				gdcm::Attribute<0x0020, 0x000E> seriesUIDAttrib;
				gdcm::Attribute<0x0020, 0x0011> seriesNumAttrib;
				gdcm::Attribute<0x0018, 0x0022> scanOptionsAttrib;
				gdcm::Attribute<0x0008, 0x0008> imageTypeAttrib;
				gdcm::Attribute<0x0028, 0x0030> pixelSpacingAttrib;
				gdcm::Attribute<0x0008, 0x103E> seriesDescAttrib;
				gdcm::Attribute<0x0018, 0x1030> protocolNameAttrib;
				gdcm::Attribute<0x0020, 0x1209> numImagesAttrib;
				studyUIDAttrib.SetFromDataSet(dataset);
				seriesUIDAttrib.SetFromDataSet(dataset);
				seriesNumAttrib.SetFromDataSet(dataset);
				scanOptionsAttrib.SetFromDataSet(dataset);
				imageTypeAttrib.SetFromDataSet(dataset);
				pixelSpacingAttrib.SetFromDataSet(dataset);
				seriesDescAttrib.SetFromDataSet(dataset);
				protocolNameAttrib.SetFromDataSet(dataset);
				numImagesAttrib.SetFromDataSet(dataset);
				bool exclude = false;
				//don't include any found series whose series description ends with any of the strings in seriesDescExcludeEndings
				QString seriesDesc = QString(seriesDescAttrib.GetValue());
				for (auto const &excludeEnding : seriesDescExcludeEndings){
					if (seriesDesc.trimmed().endsWith(excludeEnding, Qt::CaseInsensitive)){
						exclude = true;
						break;
					}
				}
				if (!exclude){
					for (auto const &excludeContain : seriesDescExcludeContains){
						if (seriesDesc.trimmed().contains(excludeContain, Qt::CaseInsensitive)){
							exclude = true;
							break;
						}
					}
				}
				if (!exclude){
					queryResult << DicomSeriesInfo();
					queryResult.last().studyUID = studyUIDAttrib.GetValue();
					queryResult.last().seriesUID = seriesUIDAttrib.GetValue();
					queryResult.last().seriesNumber = seriesNumAttrib.GetValue();
					
					auto optionsArray = scanOptionsAttrib.GetValues();
					for (auto i = 0u; i< scanOptionsAttrib.GetNumberOfValues();i++){
						queryResult.last().scanOptions << QString(optionsArray[i]).trimmed();
					}
					auto imageTypeArray = imageTypeAttrib.GetValues();
					for (auto i = 0u; i< imageTypeAttrib.GetNumberOfValues();i++){
						queryResult.last().imageType << QString(imageTypeArray[i]).trimmed();
					}
					auto spacing = pixelSpacingAttrib.GetValues();
					queryResult.last().pixelSpacing = QPointF(spacing[1], spacing[0]); //x-spacing, y-spacing
					queryResult.last().seriesDescription = seriesDesc;
					if (queryResult.last().seriesDescription.isEmpty())
						queryResult.last().seriesDescription = protocolNameAttrib.GetValue();
					queryResult.last().numImages = numImagesAttrib.GetValue();
				}
			}
		}
	}
	return queryResult;
}

/*!
 * \brief Get series info for a specific series.
 * \param studyUID UID of study to query.
 * \param seriesUID UID of series to query.
 * \return Info for the series found.
 */
DicomSeriesInfo DicomQuery::getSeriesInfo(QString studyUID, QString seriesUID)
{
	DicomSeriesInfo result;
	gdcm::ERootType root = gdcm::eStudyRootType;
	gdcm::EQueryLevel level = gdcm::eSeries;
	auto numSeriesObjTag = gdcm::Tag(0x0020, 0x1209); //Num. series related instances
	std::vector< std::pair<gdcm::Tag, std::string> > keys;
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000D), studyUID.toStdString())); // studyUID
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x000E), seriesUID.toStdString())); // seriesUID
	keys.push_back(std::make_pair(gdcm::Tag(0x0020, 0x0011), std::string())); // series Number
	keys.push_back(std::make_pair(gdcm::Tag(0x0018, 0x0022), std::string())); // scan option
	keys.push_back(std::make_pair(gdcm::Tag(0x0028, 0x0030), std::string())); // pixel spacing
	keys.push_back(std::make_pair(gdcm::Tag(0x0008, 0x103E), std::string())); // series Description
	gdcm::Attribute<0x0018, 0x1030> protocolNameAttrib;
	keys.push_back(std::make_pair(numSeriesObjTag,std::string()));
	auto query = gdcm::CompositeNetworkFunctions::ConstructQuery(root, level, keys);
	if (query->ValidateQuery(false))
	{
		std::vector<gdcm::DataSet> dataSets;
		if (gdcm::CompositeNetworkFunctions::CFind(dicomHost_.toUtf8(), dicomPort_, query, dataSets,
		                                           aeTitle_.toUtf8(), callAeTitle_.toUtf8()) &&
		    dataSets.size() > 0){
			auto dataset = dataSets.front();

			gdcm::Attribute<0x0020, 0x000D> studyUIDAttrib;
			gdcm::Attribute<0x0020, 0x000E> seriesUIDAttrib;
			gdcm::Attribute<0x0020, 0x0011> seriesNumAttrib;
			gdcm::Attribute<0x0018, 0x0022> scanOptionsAttrib;
			gdcm::Attribute<0x0028, 0x0030> pixelSpacingAttrib;
			gdcm::Attribute<0x0008, 0x103E> seriesDescAttrib;
			gdcm::Attribute<0x0018, 0x1030> protocolNameAttrib;
			gdcm::Attribute<0x0020, 0x1209> numImagesAttrib;
			studyUIDAttrib.SetFromDataSet(dataset);
			seriesUIDAttrib.SetFromDataSet(dataset);
			seriesNumAttrib.SetFromDataSet(dataset);
			scanOptionsAttrib.SetFromDataSet(dataset);
			pixelSpacingAttrib.SetFromDataSet(dataset);
			seriesDescAttrib.SetFromDataSet(dataset);
			numImagesAttrib.SetFromDataSet(dataset);
			result.studyUID = studyUIDAttrib.GetValue();
			result.seriesUID = seriesUIDAttrib.GetValue();
			result.seriesNumber = seriesNumAttrib.GetValue();

			auto optionsArray = scanOptionsAttrib.GetValues();
			for (auto i = 0u; i< scanOptionsAttrib.GetNumberOfValues();i++){
				result.scanOptions << QString(optionsArray[i]).trimmed();
			}
			auto spacing = pixelSpacingAttrib.GetValues();
			result.pixelSpacing = QPointF(spacing[1], spacing[0]); //x-spacing, y-spacing
			result.seriesDescription = seriesDescAttrib.GetValue();
			if (result.seriesDescription.isEmpty())
				result.seriesDescription = protocolNameAttrib.GetValue();
			result.numImages = numImagesAttrib.GetValue();
		}
	}
	return result;
}

/*!
 * \brief Query for worklist for given accession, patient, and requested procedure ID.
 * \param accession Accession number to query for.
 * \param patientID ID of patient to query for.
 * \param patientName Name of patient.
 * \param reqProcedureID Requested procedure ID.
 * \return QVector<QMap<QString, QString>> listing maps of GDCM tag name to tag value.
 */
QVector<QMap<QString, QString>> DicomQuery::worklistQuery(QString accession, QString patientID, QString patientName, QString reqProcedureID)
{
	QVector<QMap<QString, QString>> queryResult;
	if (accession.isEmpty() && patientID.isEmpty() && patientName.isEmpty() && reqProcedureID.isEmpty()){
#if useDebug
		qDebug() << "At least one query key (accession, patientID, patientName, or reqProcedureID) must be specified";
#endif
		return queryResult;
	}
	gdcm::ERootType root =  gdcm::ePatientRootType;
	gdcm::EQueryLevel level = gdcm::eSeries;
	std::vector< std::pair<gdcm::Tag, std::string> > keys;
	keys.push_back(std::make_pair(gdcm::Tag(0x0008,0x0050), accession.toStdString()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0010,0x0010), patientName.toStdString()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0010,0x0020), patientID.toStdString()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0040,0x1001), reqProcedureID.toStdString()));
	keys.push_back(std::make_pair(gdcm::Tag(0x0010,0x0030), std::string())); //birth date
	keys.push_back(std::make_pair(gdcm::Tag(0x0010,0x0040), std::string())); //gender
	keys.push_back(std::make_pair(gdcm::Tag(0x0032,0x1032), std::string())); //requesting physician
	keys.push_back(std::make_pair(gdcm::Tag(0x0032,0x1060), std::string())); //requested procedure description
	auto query = gdcm::CompositeNetworkFunctions::ConstructQuery( root, level ,keys, gdcm::eWLMFind );
//	if (query->ValidateQuery(false))
//	{
	// this validation check seems to always return false for WL queries even when it is valid
//	}
	    std::vector<gdcm::DataSet> dataSets;
		if (gdcm::CompositeNetworkFunctions::CFind(dicomHost_.toUtf8(), dicomPort_, query, dataSets,
		                                           aeTitle_.toUtf8(), callAeTitle_.toUtf8()) &&
		    dataSets.size() > 0){
			auto sf = gdcm::StringFilter();
			for (gdcm::DataSet dataSet : dataSets){
				auto gdcmFile = gdcm::File();
				gdcmFile.SetDataSet(dataSet);
				sf.SetFile(gdcmFile);
				queryResult << QMap<QString, QString>();
				for (auto it = dataSet.Begin() ; it != dataSet.End(); ++it){
					gdcm::DataElement el = *it;
					auto tag = it->GetTag();
					auto tagName = QString::fromStdString(tag.PrintAsPipeSeparatedString()).replace("|",",");
					auto tagValue = sf.ToString(el);
					//auto values = sf.ToStringPair(el); // this contains the plain English tagname and value instead of numeric tagname pair
					queryResult.last().insert(tagName,QString::fromStdString(tagValue).trimmed());
				}
			}
#if useDebug
		} else {
			qDebug() << "WL Query returned no results";
#endif
		}
	return queryResult;
}

