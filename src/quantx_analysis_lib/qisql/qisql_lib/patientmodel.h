#ifndef PATIENTMODEL_H
#define PATIENTMODEL_H

#include "qisqlDllCheck.h"
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include "model/patient.h"
#include "model/dicomseries.h"
#include "sqldatabaseservice.h"
#include "qiseriesinfo.h"
#include "treeitem.h"

class QSqlQuery;

namespace quantx {
namespace sql {
class SqlDatabaseService;
}
namespace overlay {
struct OverlayInfo;
}
namespace patient {

/*!
 * \brief Class to encapsulate all data about a patient, in representation for the front end.
 * \ingroup SQLModule
 * \note This class is for the frontend component modeling only, and is not necessarily a 1:1 mapping with the
 * patient model on the DB side. There are probably also a few things that still need to be migrated into this class at
 * some point in the future.
 */
class QISQL_EXPORT PatientModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	explicit PatientModel(QObject *parent = nullptr);

	// Header:
	QStringList patientInfo();
	void beginPopulateModel(sql::SqlDatabaseService *svcDb);
	void endPopulateModel(sql::SqlDatabaseService *svcDb);
	TreeItem *getRootItem(sql::SqlDatabaseService *svcDb);
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	QString patientName() const ;
	QDate patientDob() const;
	QString patientMrn() const;
	QDate studyDate(QString studyUID) const;
	QTime studyTime(QString studyUID) const;
	QStringList accessions() const;
	QStringList studies() const;
	QStringList seriesUidInStudy(const QString &studyUID) const;
	QStringList seriesUidInStudy(int studyIdx) const;
	QVector<QiSeriesInfo>seriesInStudy(const QString &studyUID) const ;
	QVector<QiSeriesInfo>seriesInStudy(int studyIdx) const ;
	bool isSeriesInStudy(const QString &studyUID, const QString &seriesUID) const ;
	QImage seriesThumbnail(const QString &seriesUID, int maxDim) const;
	QVector<QImage> seriesThumbnails(const QString &seriesUID) const;
	int numStudies() const;
	int numSeriesInStudy(const QString &studyUID) const;
	int numSeriesInStudy(int studyIdx) const;
	QStringList seriesDescriptions(QString studyUID) const;
	QString mostRecentStudy(QString modality) const;
	QVector<overlay::OverlayInfo> seriesOverlays( const QString& seriesUID ) const;
	void newOverlayAdded( const QPixmap& pixmap, const overlay::OverlayInfo& overlay );

	// Basic functionality:
	QModelIndex index(int row, int column,
	                  const QModelIndex &parent = QModelIndex()) const override;
	QModelIndex parent(const QModelIndex &index) const override;

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	// Add data:
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
	bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

	// Load series
	std::pair<QString, QUANTX::Modality> getDefaultSeriesInfo(const QString &mrDate, const std::unique_ptr<sql::SqlDatabaseService>& svcDb);
	QString findMrSeries(const QString &mrSubtype, const std::unique_ptr<sql::SqlDatabaseService>& svcDb, bool mostRecentStudyOnly = true);
	void newThumbnailAdded(QString seriesUID, const QImage &thumbnail);
	//Update model
	void insertNewSeries(QString studyUID, QString seriesUID, QString seriesNumber, QString modality, QString seriesDesc, QDate seriesDate, QTime seriesTime, int numTempPos, QVector<int> imageOrientation, QString spaceBetweenSlices);
	void insertNewSeries(const dicom::DicomSeries *newSeries);

signals:
	void newSeriesAdded(const PatientModel &model, int studyIdx, int seriesIdx, bool newStudy);
	void snapshotLoaded( const QPixmap& snapshotData ) const;

private:
	std::unique_ptr<TreeItem> rootItem;
	TreeItem *getItem(const QModelIndex &index) const;
	QVector<QVariant> createPatientRow(QSqlQuery *query); //!< Currently unimplemented.
	QVector<QVariant> createStudyRow(QSqlQuery *query); //!< Currently unimplemented.
	QVector<QVariant> createSeriesRow(QSqlQuery *query); //!< Currently unimplemented.
	QModelIndex patientIndex() const { return index(0, 0, QModelIndex()); } //!< Getter for the QModelIndex for the patient. \return Index of patient.
	QModelIndex studyIndex(int ixStudy) const { return index(ixStudy, 0, patientIndex()); } //!< Getter for the study index object, based on an integer index. \param ixStudy Integer index to query on. \return Index of study.
	QModelIndex seriesIndex(int ixStudy, int ixSeries) const { return index(ixSeries, 0, studyIndex(ixStudy)); } //!< Getter for the series index object. \param ixStudy Integer index of study to query on. \param ixSeries Integer index of series to query on. \return Index of series.
	QModelIndex seriesIndex(QString seriesUID) const;
	int studyRow(QString studyUID) const;
	Wt::Dbo::ptr<Patient> dboPatient; //!< Stores the patient model as mapped via Wt::Dbo to the actual db tables.
	QiSeriesInfo seriesFound; //!< Stores found series info.

	bool findModality(const QUANTX::Modality &modality, const QString &seriesDesc = QString(), const QString &seriesMrDate = QString(),
	                  bool sortOnSeriesNumber = false);
	bool findModalityFromList(const QUANTX::Modality &modality, const QStringList &seriesDesc, const QString &mrDate = QString());
	bool is3DSeries(const QiSeriesInfo& seriesInfo);
};

}
}

#endif // PATIENTMODEL_H
