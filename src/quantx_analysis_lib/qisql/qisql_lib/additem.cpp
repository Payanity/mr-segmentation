#include "qisql.h"
#include <QSettings>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlField>
#include <QDate>
#include <QMessageBox>
#include <QApplication>
#include <QtEndian>
#include <QThread>
#include "gdcmGlobal.h"
#include "gdcmDicts.h"
#include "gdcmDict.h"
#include "gdcmStringFilter.h"
#include "gdcmImageReader.h"
//#include "gdcmPrinter.h"
#include "gdcmImageChangeTransferSyntax.h"
#include "hologictomo.h"

#define useDebug 0
#if useDebug
#define useDebugDWI 0
#include <QDebug>
#endif

/*!
 * \brief Add a gdcm object to the database.
 * \param imagereader Image to read
 * \param tagmap List of DICOM tags that are columns in database
 * \return 1 if image is added to database, 0 if it already exists in database, negative for error
 * \pre The QISql object is initialized.
 * \post The DICOM image is added to the database.
 */
int QISql::addItem(gdcm::ImageReader &imagereader, const QMap<gdcm::Tag, QByteArray> &tagmap){
	bool UIDskip = false;
	{
		if(!imagereader.Read()){
#if useDebug
			qDebug() << "Image Read Failed";
#endif
			return -101;
		}
		gdcm::File &file = imagereader.GetFile();
		gdcm::StringFilter sf;
		sf.SetFile(file);
		const gdcm::FileMetaInformation &dicomMetaFileHeader = file.GetHeader();
		const auto swapCode = dicomMetaFileHeader.GetDataSetTransferSyntax().GetSwapCode();
#if useDebug
		{
			const gdcm::TransferSyntax &ts = dicomMetaFileHeader.GetDataSetTransferSyntax();
			qDebug() << "DICOM Swap Code:" << ts.GetSwapCode() << swapCode;
			qDebug() << "DICOM Is JPEG?" << (ts == gdcm::TransferSyntax::JPEGLosslessProcess14 || ts == gdcm::TransferSyntax::JPEGLosslessProcess14_1);
		}
#endif
		const gdcm::DataSet &dicomHeader = file.GetDataSet();
		std::stringstream headerStream;
		//gdcm::Printer gdcmPrinter;
		headerStream << dicomMetaFileHeader;
		headerStream << dicomHeader;
		//gdcmPrinter.PrintDataSet(dicomHeader,headerStream);
		QByteArray headerByteArray = QByteArray::fromStdString(headerStream.str());
		//QByteArray imageData(buffer,gimage.GetBufferLength(),static_cast<char>(0));
		//buffer.clear();

		//gdcm::Tag tPatientName(0x0,0x0);
		//const DictEntry &de1 =
		//pubdict.GetDictEntryByName("Patient's Name", tPatientName); //Look at DICOMV3.xml for dictionary names

		const gdcm::Global& g = gdcm::Global::GetInstance();
		const gdcm::Dicts &dicts = g.GetDicts();
		const gdcm::Dict &pubdict = dicts.GetPublicDict();
		gdcm::Tag tag(0x0,0x0);
		QString temp;

		tag = gdcm::Tag(0x0008,0x0018);
		QString imageUID = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0008,0x0008);
		QString imageType = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0008,0x0020);
		QString studyDate = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0008,0x0021);
		QString seriesDate = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		if (seriesDate.isEmpty()){
			tag=gdcm::Tag(0x0018,0x1012); // secondary capture date
			seriesDate = QString::fromStdString(sf.ToString(tag));
		}

		tag = gdcm::Tag(0x0008,0x0022);
		QString imageDate = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		if(imageDate.isEmpty()){
			tag = gdcm::Tag(0x0008,0x0023);
			imageDate = QString::fromStdString(sf.ToString(tag));
		}

		tag = gdcm::Tag(0x0008,0x0030);
		QString studyTime = QString::fromStdString(sf.ToString(tag));
		if(studyTime.contains('.'))
			studyTime = studyTime.split('.').first();

		tag = gdcm::Tag(0x0008,0x0031);
		QString seriesTime = QString::fromStdString(sf.ToString(tag));

		if (seriesTime.isEmpty()){
			tag=gdcm::Tag(0x0018,0x1014);//secondary capture time
			seriesTime = QString::fromStdString(sf.ToString(tag));
		}

		if(seriesTime.contains('.'))
			seriesTime = seriesTime.split('.').first();

		tag = gdcm::Tag(0x0008,0x0032);
		QString imageTime = QString::fromStdString(sf.ToString(tag));

		if(imageTime.isEmpty()){
			tag = gdcm::Tag(0x0008,0x0033);
			imageTime = QString::fromStdString(sf.ToString(tag));
		}
		if(imageTime.contains('.'))
			imageTime = imageTime.split('.').first();

		tag = gdcm::Tag(0x0008,0x0050);
		QString accessionNumber = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0008,0x0060);
		QString modality = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0008,0x0064);
		QString conversionType = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0008,0x0068);
		QString presentationIntent = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0008,0x0070); //needed for ultrasound
		QString manufacturer = QString::fromStdString(sf.ToString(tag));

		//tag = gdcm::Tag(0x0040,0x0254);
		tag = gdcm::Tag(0x0008,0x103e);
		QString seriesDesc = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);
		if (seriesDesc.isEmpty()){// Use Protocol Name if seriesDesc is empty
			tag = gdcm::Tag(0x0018,0x1030);
			seriesDesc = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag); //  This tag should be a series-specific protocol name, but is sometimes set per-study so use as a backup
		}
		tag = gdcm::Tag(0x0008,0x1090); //needed for ultrasound
		QString modelName = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0010,0x0010);
		QString name = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0010,0x0020);
		QString mrn = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0010,0x0030);
		QString birthdate = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0010,0x0040);
		QString sex = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0018,0x0022);
		QString scanOptions = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0018,0x0050);
		QString sliceThickness = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0018,0x0087);
		float magneticFieldStrength = QString::fromStdString(sf.ToString(tag)).toFloat();

		tag = gdcm::Tag(0x0018,0x0088);
		QString spaceBetweenSlices = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0018,0x1060);
		QString triggerTime = QString::fromStdString(sf.ToString(tag));

		//new tags needed for mammography
		tag = gdcm::Tag(0x0018,0x5101);
		QString viewPosition = QString::fromStdString(sf.ToString(tag));
#if useDebug && useDebugDWI
		qDebug()<<"Reading new dwi tags (gdcm)";
#endif
		//new tags needed for dwi images
		tag = gdcm::Tag(0x0018,0x9087);
		QString diffusionBValue = QString::fromStdString(sf.ToString(tag));
		if (diffusionBValue.isEmpty() && manufacturer.contains("SIEMENS", Qt::CaseInsensitive)){
			tag = gdcm::Tag(0x0019,0x100C); // private tag Siemens uses for b-values
			      diffusionBValue = QString::fromStdString(sf.ToString(tag));
		}

		tag = gdcm::Tag(0x0018,0x9089);
		QString diffusionGradientOrientation = QString::fromStdString(sf.ToString(tag));
#if useDebug && useDebugDWI
		qDebug()<<"DWI tag values";
		qDebug()<<"Bvalue"<<diffusionBValue;
		qDebug()<<"GradientOrientation"<<diffusionGradientOrientation;
#endif
		tag = gdcm::Tag(0x0020,0x000d);
		pubdict.GetDictEntry(tag);
		QString studyUID = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0020,0x000e);
		QString seriesUID = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0020,0x0011);
		QString seriesNumber = tagmap.value(tag).isEmpty() ? QString::fromStdString(sf.ToString(tag)) : tagmap.value(tag);

		tag = gdcm::Tag(0x0020,0x0013);
		QString instanceNum = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0020,0x0020);
		QString patientOrientation = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0020,0x0032);
		QString imagePosition = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0020,0x0037);
		QString imageOrientation = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0020,0x0062);
		QString imageLaterality = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0020,0x1041);
		QString sliceLocation = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0020,0x0100);
		QString temporalPosID = QString::fromStdString(sf.ToString(tag)).trimmed();

		tag = gdcm::Tag(0x0020,0x0105);
		QString numTemporalPositions = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0028,0x0004);
		QString photometricInterpretation = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0028,0x0008);
		QString numberOfFrames = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0028,0x0010);
		auto height = QString::fromStdString(sf.ToString(tag)).toUInt();

		tag = gdcm::Tag(0x0028,0x0011);
		auto width = QString::fromStdString(sf.ToString(tag)).toUInt();

		tag = gdcm::Tag(0x0028,0x0030);
		float pixelsize_y = QString::fromStdString(sf.ToString(tag)).split('\\').first().toFloat();
		float pixelsize_x = QString::fromStdString(sf.ToString(tag)).split('\\').last().toFloat();

		tag = gdcm::Tag(0x0028,0x1050);
		QString windowCenter = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0028,0x1051);
		QString windowWidth = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0028,0x0106);
		QString minPixval = QString::fromStdString(sf.ToString(tag));

		tag = gdcm::Tag(0x0028,0x0107);
		QString maxPixval = QString::fromStdString(sf.ToString(tag));

		//don't process if this is already CAD output
		QStringList startsWithFilter{"CAD~",
			                         "Reformatted",
			                         "DynaCAD",
			                         "ssub"};
		QStringList imageTypeFilter{"DERIVED\\SECONDARY\\PROJECTION IMAGE",
			                        "DERIVED\\PRIMARY\\PROJECTION IMAGE"};
		// containsFilter // is empty :(
		QStringList containsFilter{};
		QStringList endsWithFilter{"RECON",
			                       "SUB"};
		QStringList regExFilter{R"(^\(*\)-\()"};
		bool filtered = false;

		if(conversionType.trimmed()=="SD"|| conversionType.trimmed()=="DI" || conversionType.trimmed()=="WSD"){
		   modality="OT";
	   }

		for(const auto & s : std::as_const(startsWithFilter))
			filtered = filtered || seriesDesc.startsWith(s);
		for(const auto & s : std::as_const(imageTypeFilter))
			filtered = filtered || imageType.startsWith(s);
		for(const auto & s : std::as_const(containsFilter))
			filtered = filtered || seriesDesc.contains(s);
		for(const auto & s : std::as_const(endsWithFilter))
			filtered = filtered || seriesDesc.trimmed().endsWith(s);
		for(const auto & s : std::as_const(regExFilter))
			filtered = filtered || seriesDesc.contains(QRegExp(s));

		if(filtered)
			return -5;

		//This if for hologic internal processing, but can end up in DICOM folder
		if(presentationIntent.startsWith("FOR PROCESSING"))
			return -6;

		//tagList is list of potentially useful tags
		if(tagList.empty())
			createTagList();

		//Done collecting necessary data
		//Connect to Database
		QSqlDatabase db = createDatabase("QIsqlAddToDatabase");
		//bool skipUniqueCheck = sqlDatabaseType == QString("QPSQL");
		bool skipUniqueCheck = false;
		bool allowInsert = true;
		bool ok = db.open();
		if(!ok){
			if(qobject_cast<QApplication*>(QCoreApplication::instance()) != nullptr){
				int retries = 0;
				while( (!ok) && (retries++ < 5) ){
					QThread::sleep(1); // Wait one second, and retry
				}
				QMessageBox::critical(nullptr,"Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
			}
			else{
				fprintf(stderr, "Critical: Database could not be opened");
				return -1;
			}
		}

		QUANTX::Modality qModality = QUANTX::NOIMAGE;
		if(modality == "MR")
			qModality = QUANTX::MRI;
		else if(modality == "US")
			qModality = QUANTX::US;
		//else if(modality == "US3D") //Need to check for seriesDesc if we want US3D in seaparate table
		//    qModality = QUANTX::US3D;
		else if(modality == "MG")
			qModality = QUANTX::MAMMO;

		//Fix some values for MR images that have a rescale slope
		if(qModality == QUANTX::MRI){
			tag = gdcm::Tag(0x0028,0x1053);
			auto rescaleSlope = QString::fromStdString(sf.ToString(tag)).toDouble();
			if(rescaleSlope != 0.0){
				windowWidth = QString::number(windowWidth.toDouble() / rescaleSlope,'f');
				windowCenter = QString::number(windowCenter.toDouble() / rescaleSlope,'f');
			}
		}

		//Fix some values for GE TOMO IMAGES
		if(qModality == QUANTX::MAMMO && numberOfFrames.toInt() > 1){
			gdcm::DataElement element;
			gdcm::SmartPointer<gdcm::SequenceOfItems> sequence;
			gdcm::SequenceOfItems::ItemVector itemVector;
			gdcm::Item item;
			gdcm::DataSet dataSet;

			tag = gdcm::Tag(0x5200,0x9229);
			element = dicomHeader.GetDataElement(tag);
			sequence = element.GetValueAsSQ();

			itemVector = sequence->Items;
			if(!itemVector.empty()){
				item = itemVector[0];
				dataSet = item.GetNestedDataSet();

				if(windowWidth == windowCenter && windowWidth == ""){
					tag = gdcm::Tag(0x0028,0x9132);
					itemVector = dataSet.GetDataElement(tag).GetValueAsSQ()->Items;
					if(!itemVector.empty()){
						item = itemVector[0];

						tag = gdcm::Tag(0x0028,0x1050);
						element = item.GetNestedDataSet().GetDataElement(tag);
						windowCenter = QByteArray(element.GetByteValue()->GetPointer(), static_cast<int>(element.GetVL()));

						tag = gdcm::Tag(0x0028,0x1051);
						element = item.GetNestedDataSet().GetDataElement(tag);
						windowWidth = QByteArray(element.GetByteValue()->GetPointer(), static_cast<int>(element.GetVL()));
#if useDebug
						qDebug() << "Window Center/Width:" << windowCenter.toFloat() << "/" << windowWidth.toFloat();
#endif
					}
				}

				if(spaceBetweenSlices.toFloat() <= 0.0f){
					tag = gdcm::Tag(0x0028,0x9110);
					if(dataSet.FindDataElement(tag)){
						itemVector = dataSet.GetDataElement(tag).GetValueAsSQ()->Items;
						if(!itemVector.empty()){
							item = itemVector[0];

							tag = gdcm::Tag(0x0018,0x0050);
							element = item.GetNestedDataSet().GetDataElement(tag);
							sliceThickness = QByteArray(element.GetByteValue()->GetPointer(), static_cast<int>(element.GetVL()));

							tag = gdcm::Tag(0x0028,0x0030);
							element = item.GetNestedDataSet().GetDataElement(tag);
							QString pixelSizes = QByteArray(element.GetByteValue()->GetPointer(), static_cast<int>(element.GetVL()));
							pixelsize_y = pixelSizes.split('\\').first().toFloat();
							pixelsize_x = pixelSizes.split('\\').last().toFloat();
#if useDebug
							qDebug() << "SliceThickness:" << sliceThickness.toFloat() <<
							            "pixelsize_x:" << pixelsize_x <<
							            "pixelsize_y:" << pixelsize_y;
#endif
							//This needs to actually be checked with the frames
							spaceBetweenSlices = sliceThickness;
						}
					}
				}
			}

			if(spaceBetweenSlices.toFloat() <= 0.0f){
				tag = gdcm::Tag(0x5200,0x9230);
				element = dicomHeader.GetDataElement(tag);
				sequence = element.GetValueAsSQ();

				itemVector = sequence->Items;
				if(!itemVector.empty()){
					item = itemVector[0];
					dataSet = item.GetNestedDataSet();

					tag = gdcm::Tag(0x0028,0x9110);
					if(dataSet.FindDataElement(tag)){
						itemVector = dataSet.GetDataElement(tag).GetValueAsSQ()->Items;
						if(!itemVector.empty()){
							item = itemVector[0];

							tag = gdcm::Tag(0x0018,0x0050);
							element = item.GetNestedDataSet().GetDataElement(tag);
							sliceThickness = QByteArray(element.GetByteValue()->GetPointer(), static_cast<int>(element.GetVL()));

							tag = gdcm::Tag(0x0028,0x0030);
							element = item.GetNestedDataSet().GetDataElement(tag);
							QString pixelSizes = QByteArray(element.GetByteValue()->GetPointer(), static_cast<int>(element.GetVL()));
							pixelsize_y = pixelSizes.split('\\').first().toFloat();
							pixelsize_x = pixelSizes.split('\\').last().toFloat();
#if useDebug
							qDebug() << "SliceThickness:" << sliceThickness.toFloat() <<
							            "pixelsize_x:" << pixelsize_x <<
							            "pixelsize_y:" << pixelsize_y;
#endif
							//This needs to actually be checked with the frames
							spaceBetweenSlices = sliceThickness;
						}
					}
				}
			}

		}

		QByteArray imageData;
		// Detect Hologic Tomo File
		bool hologicTomoFile = (qModality == QUANTX::MAMMO && dicomHeader.FindDataElement( gdcm::Tag(0x7e01,0x0010) ));
		// Handle special cases
		if(hologicTomoFile){
			if(numberOfFrames.toInt() <= 1){
				tag = gdcm::Tag(0x0020,0x1002);
				numberOfFrames = QString::fromStdString(sf.ToString(tag));
			}
			hologicTomo::HologicTomoReturn hologicData = hologicTomo::getImageData(dicomHeader);
			imageData = hologicData.imageData;
			spaceBetweenSlices = QString::number(static_cast<double>(hologicData.spaceBetweenSlices)); // Just set a value now, but can be read by getImageData() - I think we do this because the value is stored as float in the DICOM file
			width = hologicData.width; // probably need pixelsize_x and pixelsize_y to correct dimensions too
			height = hologicData.height;
		}

		if(windowWidth == windowCenter && windowWidth == ""){
			if(seriesDesc.contains("SecurView")){
				windowWidth = "4096";
				windowCenter = "2047";
			}
			else{
				tag = gdcm::Tag(0x0028,0x0101);
				auto bitsStored = QString::fromStdString(sf.ToString(tag)).toUInt();
				windowWidth = QString::number(pow(2.0,bitsStored),'f',0);
				windowCenter = QString::number(pow(2.0,bitsStored-1),'f',0);
			}

		}

		QSqlRecord imageRecord   = db.record(imageTable(qModality));
		QSqlRecord imageUIDOnly  = db.record(imageTable(qModality));
		QSqlRecord seriesRecord  = db.record(sqlSeriesTable);
		QSqlRecord seriesUIDOnly = db.record(sqlSeriesTable);
		QSqlRecord seriesUIDandDbIdOnly = db.record(sqlSeriesTable);
		QSqlRecord seriesDescOnly= db.record(sqlSeriesTable);
		QSqlRecord studyRecord   = db.record(sqlStudiesTable);
		QSqlRecord studyUIDOnly  = db.record(imageTable(qModality));
		QSqlRecord studyUIDandDbIdOnly  = db.record(sqlStudiesTable);
		QSqlRecord patientRecord = db.record(sqlPatientsTable);
		QSqlRecord patientMRNOnly= db.record(sqlPatientsTable);
		QSqlRecord patientMRNandDbIdOnly= db.record(sqlPatientsTable);
		QSqlRecord dicomRecord   = db.record(sqlDicomTable);
		QSqlRecord dicomUIDOnly  = db.record(sqlDicomTable);

		//Set image record values
		imageRecord.setValue("mrn",mrn);
		imageRecord.setValue("image_uid",imageUID);
		imageRecord.setValue("series_uid",seriesUID);
		imageRecord.setValue("study_uid",studyUID);
		imageRecord.setValue("image_date",QDate::fromString(imageDate,"yyyyMMdd"));
		imageRecord.setValue("image_time",QTime::fromString(imageTime,"hhmmss"));
		imageRecord.setValue("width",width);
		imageRecord.setValue("height",height);
		imageRecord.setValue("pixelsize_x",pixelsize_x);
		imageRecord.setValue("pixelsize_y",pixelsize_y);
		imageRecord.setValue("image_type",imageType);
		imageRecord.setValue("window_width",windowWidth);
		imageRecord.setValue("window_center",windowCenter);
		imageRecord.setValue("min_pixval",minPixval);
		imageRecord.setValue("max_pixval",maxPixval);
		imageRecord.setValue("image_position",imagePosition);
		imageRecord.setValue("slice_location",sliceLocation);  //single value of image Position
		imageRecord.setValue("temporal_pos_id",temporalPosID);
		imageRecord.setValue("instance_num",instanceNum);
		imageRecord.setValue("scan_options",scanOptions);
		imageRecord.setValue("view_position",viewPosition);
		imageRecord.setValue("image_laterality",imageLaterality);
		imageRecord.setValue("patient_orientation",patientOrientation);
		imageRecord.setValue("photometric_interpretation",photometricInterpretation);
		imageRecord.setValue("manufacturer",manufacturer);
		imageRecord.setValue("model_name",modelName);
		if(imageRecord.contains("version"))
			imageRecord.setValue("version",0);
		if(qModality == QUANTX::NOIMAGE)
			imageRecord.setValue("modality",modality);
		if(qModality == QUANTX::US || qModality == QUANTX::MAMMO) //qModality == QUANTX::US3D cannot happen here
			if(numberOfFrames.toInt() != 0) // Bad conversion (or NULL value) is zero
				imageRecord.setValue("number_of_frames", numberOfFrames);
		if(qModality == QUANTX::MRI){
			imageRecord.setValue("diffusion_b_value",diffusionBValue);
			imageRecord.setValue("diffusion_gradient_orientation",diffusionGradientOrientation);
			if(!triggerTime.isEmpty())
				imageRecord.setValue("trigger_time",qRound(triggerTime.toDouble()));
			imageRecord.setValue("magnetic_field_strength",magneticFieldStrength);
		}
		int dataLoc = imageRecord.indexOf("image_data");
		QSqlField dataField = imageRecord.field(dataLoc);
		imageRecord.remove(dataLoc);

		//Set series record values
		seriesRecord.setValue("mrn",mrn);
		seriesRecord.setValue("series_uid",seriesUID);
		seriesRecord.setValue("study_uid",studyUID);
		seriesRecord.setValue("series_number",seriesNumber);
		seriesRecord.setValue("modality",modality);
		seriesRecord.setValue("series_desc",seriesDesc);
		seriesRecord.setValue("series_date",QDate::fromString(seriesDate,"yyyyMMdd"));
		seriesRecord.setValue("series_time",QTime::fromString(seriesTime,"hhmmss"));
		if(!numTemporalPositions.isEmpty())
			seriesRecord.setValue("num_temporal_positions",numTemporalPositions);
		seriesRecord.setValue("image_orientation",imageOrientation);
		seriesRecord.setValue("space_between_slices",spaceBetweenSlices);
		seriesRecord.setValue("version",0);

		//Set study record values
		studyRecord.setValue("mrn",mrn);
		studyRecord.setValue("study_uid",studyUID);
		studyRecord.setValue("modality",modality);
		studyRecord.setValue("study_date",QDate::fromString(studyDate,"yyyyMMdd"));
		studyRecord.setValue("study_time",QTime::fromString(studyTime,"hhmmss"));
		studyRecord.setValue("accession_number",accessionNumber);
		studyRecord.setValue("version",0);

		//Set patient record values
		patientRecord.setValue("mrn",mrn);
		patientRecord.setValue("patient_name",name);
		patientRecord.setValue("dob",QDate::fromString(birthdate,"yyyyMMdd"));
		patientRecord.setValue("sex",sex);
		patientRecord.setValue("version",0);

		imageUIDOnly.setValue("image_uid",imageUID);
		for(int i=imageUIDOnly.count()-1;i>=0;i--)
			if(imageUIDOnly.isNull(i))
				imageUIDOnly.remove(i);
		seriesUIDOnly.setValue("series_uid",seriesUID);
		for(int i=seriesUIDOnly.count()-1;i>=0;i--)
			if(seriesUIDOnly.isNull(i))
				seriesUIDOnly.remove(i);
		for (int i=0; i<seriesUIDandDbIdOnly.count(); i++){
			seriesUIDandDbIdOnly.setGenerated(i, false);
		}
		seriesUIDandDbIdOnly.setGenerated("id", true);
		seriesUIDandDbIdOnly.setGenerated("series_uid", true);
		auto seriesUIDandDBidOnly = seriesUIDOnly;
		seriesUIDandDBidOnly.append(seriesUIDOnly.field("id"));
		seriesDescOnly.setValue("series_desc",seriesDesc);
		for(int i=seriesDescOnly.count()-1;i>=0;i--)
			if(seriesDescOnly.isNull(i))
				seriesDescOnly.remove(i);
		studyUIDOnly.setValue("study_uid",studyUID);
		for(int i=studyUIDOnly.count()-1;i>=0;i--)
			if(studyUIDOnly.isNull(i))
				studyUIDOnly.remove(i);
		for (int i=0; i<studyUIDandDbIdOnly.count(); i++){
			studyUIDandDbIdOnly.setGenerated(i, false);
		}
		studyUIDandDbIdOnly.setGenerated("id", true);
		studyUIDandDbIdOnly.setGenerated("study_uid", true);
		patientMRNOnly.setValue("mrn",mrn);
		for(int i=patientMRNOnly.count()-1;i>=0;i--)
			if(patientMRNOnly.isNull(i))
				patientMRNOnly.remove(i);
		for (int i=0; i<patientMRNandDbIdOnly.count(); i++){
			patientMRNandDbIdOnly.setGenerated(i, false);
		}
		patientMRNandDbIdOnly.setGenerated("id", true);
		patientMRNandDbIdOnly.setGenerated("mrn", true);

		dicomUIDOnly.setValue("0008,0018",imageUID);
		for(int i=dicomUIDOnly.count()-1;i>=0;i--)
			if(dicomUIDOnly.isNull(i))
				dicomUIDOnly.remove(i);

		//Set dicom record values
		QString columnName;
		QChar zero = '0';
		for(auto & i : tagList){
			//temp = input->getTagValue(tagList.at(i));
			tag = gdcm::Tag(static_cast<uint16_t>(i.x()), static_cast<uint16_t>(i.y()));
			temp = QString::fromStdString(sf.ToString(tag));
			columnName = (QString::number(i.x(),16) + QString(',') + QString::number(i.y(),16));
			while(columnName.indexOf(',') < 4)
				columnName.insert(0,&zero,1); //Add zeros in front of first tag val
			while(columnName.size() < 9)
				columnName.insert(5,&zero,1);       //Add zeros in front of second tag val
			dicomRecord.setValue(columnName,temp);
		}
		dicomRecord.setValue("full_header", headerByteArray);
		for(int i=dicomRecord.count()-1;i>=0;i--)
			if(dicomRecord.isNull(i))
				dicomRecord.remove(i);

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		QScopedPointer<QSqlQuery> sqlQueryCheck(new QSqlQuery(db));
		int patient_db_id;
		int study_db_id;
		int series_db_id;
		if(!skipUniqueCheck){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,imageTable(qModality),imageUIDOnly,false) + QString(' ')
			        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,imageTable(qModality),imageUIDOnly,true);
			sqlQueryCheck->prepare(sqlStatement);
			for(int i=0;i<imageUIDOnly.count();i++)
				sqlQueryCheck->bindValue(i,imageUIDOnly.value(i));
			ok = sqlQueryCheck->exec();
#if useDebug
			if(!ok){
				qDebug() << "Unique Query Error:" << sqlQuery->lastError().text();
			}
#endif
			allowInsert = !sqlQueryCheck->next();
		}
		if(!allowInsert){
#if useDebug
			qDebug() << "Image UID found in Database:" << imageUID;
			qDebug() << sqlDatabaseName << sqlHostName << sqlDatabaseName << sqlUserName << sqlPassword;
#endif
			//Don't waste time adding shit to database for this image
			skipUniqueCheck = true;
			allowInsert = false;
			UIDskip = true;
		}
		if(!skipUniqueCheck){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlPatientsTable,patientMRNandDbIdOnly,false) + QString(' ')
			        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlPatientsTable,patientMRNOnly,true);
#if useDebug
			qDebug() << "Patient search statement: " << sqlStatement;
#endif
			sqlQueryCheck->prepare(sqlStatement);
			for(int i=0;i<patientMRNOnly.count();i++)
				sqlQueryCheck->bindValue(i,patientMRNOnly.field(i).value());
			sqlQueryCheck->exec();
			//qDebug() << "Patient Select Statement: " << sqlStatement;
			//qDebug() << "Patient Select Result   : " << sqlQuery->size();
			allowInsert = !sqlQueryCheck->next();
		}
		if(allowInsert){
			for(int i=patientRecord.count()-1;i>=0;i--)
				if(patientRecord.isNull(i))
					patientRecord.remove(i);
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement,sqlPatientsTable,patientRecord,true);
			sqlQuery->prepare(sqlStatement);
			for(int i=0;i<patientRecord.count();i++)
				sqlQuery->bindValue(i,patientRecord.field(i).value());
			ok = sqlQuery->exec();
#if useDebug
			if(!ok){
				qDebug() << "Patient Insert Not OK";
				qDebug() << sqlQuery->lastError().text();
			}
			else {
				qDebug() << "Patient Insert OK";
			}
#endif
			if (ok){
				patient_db_id = sqlQuery->lastInsertId().toInt();
#if useDebug
			qDebug() << "New patient db ID:" << patient_db_id;
#endif
			} else {
				sqlQueryCheck->exec();
				sqlQueryCheck->first();
			}
		}
		if(! (allowInsert && ok)){
			patient_db_id = sqlQueryCheck->record().value("id").toInt();
#if useDebug
			qDebug() << "Already in DB patient db ID:" << patient_db_id;
#endif
		}
		if(!skipUniqueCheck){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlStudiesTable,studyUIDandDbIdOnly,false) + QString(' ')
			        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlStudiesTable,studyUIDOnly,true);
#if useDebug
			qDebug() << "Study search statement: " << sqlStatement;
#endif
			sqlQueryCheck->prepare(sqlStatement);
			for(int i=0;i<studyUIDOnly.count();i++)
				sqlQueryCheck->bindValue(i,studyUIDOnly.field(i).value());
			sqlQueryCheck->exec();
			//qDebug() << "Study Select Statement: " << sqlStatement;
			//qDebug() << "Study Select Result   : " << sqlQuery->size();
			allowInsert = !sqlQueryCheck->next();
		}
		if(allowInsert){
			//studyRecord.append(QSqlField("qi_patients_id", QVariant::ULongLong));
			studyRecord.setValue("qi_patients_id", patient_db_id);
			for(int i=studyRecord.count()-1;i>=0;i--)
				if(studyRecord.isNull(i))
					studyRecord.remove(i);
			//qDebug() << "No Study Found: " << sqlQuery->executedQuery();
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement,sqlStudiesTable,studyRecord,true);
			sqlQuery->prepare(sqlStatement);
			for(int i=0;i<studyRecord.count();i++)
				sqlQuery->bindValue(i,studyRecord.field(i).value());
			ok = sqlQuery->exec();
#if useDebug
			if(!ok){
				qDebug() << "Study Insert Not OK";
				qDebug() << sqlQuery->lastError().text();
			}
			else
				qDebug() << "Study Insert OK";
#endif
			if (ok){
				study_db_id = sqlQuery->lastInsertId().toInt();
#if useDebug
			qDebug() << "New study db ID:" << study_db_id;
#endif
			} else {
				sqlQueryCheck->exec();
				sqlQueryCheck->first();
			}
	}
		if(! (allowInsert && ok)){
		study_db_id = sqlQueryCheck->record().value("id").toInt();
#if useDebug
		    qDebug() << "Already in DB study db ID:" << study_db_id;
#endif
	}
		if(!skipUniqueCheck){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesUIDandDbIdOnly,false) + QString(' ')
			        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesUIDOnly,true);
#if useDebug
			qDebug() << "Series search statement: " << sqlStatement;
#endif
			sqlQueryCheck->prepare(sqlStatement);
			for(int i=0;i<seriesUIDOnly.count();i++)
				sqlQueryCheck->bindValue(i,seriesUIDOnly.value(i));
			sqlQueryCheck->exec();
#if useDebug
			qDebug() << "Series Select Statement: " << sqlStatement;
			qDebug() << "Series Select Result   : " << sqlQueryCheck->size();
#endif
			allowInsert = !sqlQueryCheck->next();
		}
		if(allowInsert){
			//seriesRecord.append(QSqlField("qi_studies_id", QVariant::ULongLong));
			seriesRecord.setValue("qi_studies_id", study_db_id);
			for(int i=seriesRecord.count()-1;i>=0;i--)
				if(seriesRecord.isNull(i))
					seriesRecord.remove(i);
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement,sqlSeriesTable,seriesRecord,true);
			sqlQuery->prepare(sqlStatement);
			for(int i=0;i<seriesRecord.count();i++)
				sqlQuery->bindValue(i,seriesRecord.value(i));
			ok = sqlQuery->exec();
#if useDebug
			if(!ok){
				qDebug() << "Series Insert Not OK";
				qDebug() << sqlQuery->lastError().text();
			}
			else
				qDebug() << "Series Insert OK";
#endif
			if (ok){
				series_db_id = sqlQuery->lastInsertId().toInt();
#if useDebug
			qDebug() << "New series db ID:" << series_db_id;
#endif
			} else {
				sqlQueryCheck->exec();
				sqlQueryCheck->first();
			}
		}
		if(! (allowInsert && ok)){
			series_db_id = sqlQueryCheck->record().value("id").toInt();
#if useDebug
			qDebug() << "Already in DB series db ID:" << series_db_id;
#endif
		}
		if(!(skipUniqueCheck = false)){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,imageTable(qModality),imageUIDOnly,false) + QString(' ')
			        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,imageTable(qModality),imageUIDOnly,true);
			sqlQueryCheck->prepare(sqlStatement);
			for(int i=0;i<imageUIDOnly.count();i++)
				sqlQueryCheck->bindValue(i,imageUIDOnly.value(i));
			ok = sqlQueryCheck->exec();
#if useDebug
			if(!ok){
				qDebug() << "Unique Query Error:" << sqlQuery->lastError().text();
			}
#endif
			allowInsert = !sqlQueryCheck->next();
		}
		if(allowInsert){
			// If there are no special cases, load the data normally
			if( imageData.isEmpty() ) {
				//Read image data from the file
				const gdcm::Image &gimage = imagereader.GetImage();
				gdcm::TransferSyntax imageTS = gimage.GetTransferSyntax();
				if(imageTS == gdcm::TransferSyntax::JPEGLosslessProcess14 || imageTS == gdcm::TransferSyntax::JPEGLosslessProcess14_1){
					gdcm::ImageChangeTransferSyntax change;
					change.SetInput(gimage);
					change.Change();
				}
#if useDebug
				qDebug() << "Pixel Format:" << gimage.GetPixelFormat().GetScalarTypeAsString();
#endif
				imageData.fill(static_cast<char>(0), static_cast<int>(gimage.GetBufferLength()));
				gimage.GetBuffer(imageData.data());
			}

			//store image data in SQL Record
			imageRecord.insert(dataLoc,dataField);
			//if(sqlDatabaseType == QString("QPSQL"))
			//    imageRecord.setValue("image_data",imageData);
			//else
			imageRecord.setValue("image_data",qCompress(imageData)); //store compressed image data, compress 23 lines up to save memory?

			imageRecord.setValue("qi_series_id", series_db_id);


#if useDebug
			qDebug() << imageRecord.count();
#endif
			for(int i=imageRecord.count()-1;i>=0;i--)
				if(imageRecord.isNull(i))
					imageRecord.remove(i);
#if useDebug
			qDebug() << imageRecord.count();
#endif

			//qDebug() << "Sql Inserted size of image_data: " << imageRecord.field("image_data").value().toByteArray().size();
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement,imageTable(qModality),imageRecord,true);
			sqlQuery->prepare(sqlStatement);
			for(int i=0;i<imageRecord.count();i++)
				sqlQuery->bindValue(i,imageRecord.value(i));
			ok = sqlQuery->exec();
#if useDebug
			if(!ok){
				qDebug() << "Image Insert Not OK";
				qDebug() << sqlQuery->lastError().text();
			}
			else
				qDebug() << "Image Insert OK";
			//qDebug() << "Image Insert Statement: " << sqlStatement;
#endif
		}
		else{
			skipUniqueCheck = true;
			allowInsert = false;
		}
		if(!skipUniqueCheck){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlDicomTable,dicomUIDOnly,false) + QString(' ')
			        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlDicomTable,dicomUIDOnly,true);
			sqlQueryCheck->prepare(sqlStatement);
			for(int i=0;i<dicomUIDOnly.count();i++)
				sqlQueryCheck->bindValue(i,dicomUIDOnly.field(i).value());
			sqlQueryCheck->exec();
			//qDebug() << "Dicom Select Statement: " << sqlStatement;
			//qDebug() << "Dicom Select Result   : " << sqlQuery->size();
			allowInsert = !sqlQueryCheck->next();
		}
		if(allowInsert){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement,sqlDicomTable,dicomRecord,true);
			sqlQuery->prepare(sqlStatement);
			for(int i=0;i<dicomRecord.count();i++)
				sqlQuery->bindValue(i,dicomRecord.field(i).value());
			bool ok = sqlQuery->exec();
#if useDebug
			if(ok)
				qDebug() << "Dicom Insert OK";
#endif
			if(!ok && !sqlQuery->lastError().text().contains("duplicate")){ //Statement is longer than maximum number of characters
#if useDebug
				qDebug() << "Dicom Insert Not OK";
#endif
				sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement,sqlDicomTable,dicomUIDOnly,true);
				sqlQuery->prepare(sqlStatement);
				for(int i=0;i<dicomUIDOnly.count();i++)
					sqlQuery->bindValue(i,dicomUIDOnly.field(i).value());
				sqlQuery->exec();

				QSqlRecord dicomSmallRecord;
				int jump = (dicomRecord.count() + 1) / 2;
				for(int i=0;i<dicomRecord.count();i+=jump){
					if(i+jump > dicomRecord.count())
						jump = dicomRecord.count() - i;
					dicomSmallRecord.clear();
					for(int j=0;j<jump;j++){
						dicomSmallRecord.append(dicomRecord.field(i+j));
					}
					//dicomSmallRecord.append(dicomRecord.field(i));
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::UpdateStatement,sqlDicomTable,dicomSmallRecord,true) + QString(' ')
					        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlDicomTable,dicomUIDOnly,false);
					sqlQuery->prepare(sqlStatement);
					//qDebug() << "Dicom Select Statement: " << sqlStatement;
					for(int j=0;j<dicomSmallRecord.count();j++)
						sqlQuery->bindValue(j,dicomSmallRecord.field(j).value());
					ok = sqlQuery->exec();
#if useDebug
					if(ok)
						qDebug() << i;
#endif
					if(!ok){
						if(jump == 1){
							//qDebug() << "Not OK: " << dicomRecord.field(i).name();
							continue;
						}
						jump = (jump + 1)/2;
						i = i-jump;
					}
				}
			}
		}

		sqlQuery->finish();
		sqlQuery.reset();
		sqlQueryCheck->finish();
		sqlQueryCheck.reset();
		db.close();
		//end DB usage
	}
	QSqlDatabase::removeDatabase("QIsqlAddToDatabase");

	//return 1 if we added the file to the database, 0 otherwise
	return UIDskip?0:1;
}
