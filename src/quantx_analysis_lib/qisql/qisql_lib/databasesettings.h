#ifndef DATABASESETTINGS_H
#define DATABASESETTINGS_H

#include "qisqlDllCheck.h"
#include <QStringList>

class QSettings;

/*!
 * \brief Class for encapsulating database settings.
 * \ingroup SQLModule
 */
struct QISQL_EXPORT DatabaseSettings
{
public:
	DatabaseSettings();
	DatabaseSettings(const QStringList& inSettings);
	DatabaseSettings(const QString& inType, const QString& inHost, const QString& inDbName,
	                 const QString& inUserName, const QString& inPassword, const QString& inPort = QString());
	DatabaseSettings( const QSettings& settings );
	DatabaseSettings(const DatabaseSettings& other) = default;
	DatabaseSettings(DatabaseSettings&& other) = default;
	DatabaseSettings& operator=(const DatabaseSettings& rhs) = default;
	DatabaseSettings& operator=(DatabaseSettings&& rhs) = default;
	~DatabaseSettings() = default;

	enum class SettingName
	{
		type = 0,
		host,
		databaseName,
		userName,
		password,
		port,
		MAX_SETTINGS
	};

	inline const QString getSetting(const SettingName inSettingName) const { return dbSettings.contains(inSettingName) ? dbSettings.at(inSettingName) : QString(); } //!< Getter for the setting by name as passed to it for lookup. \param inSettingName The name by which to look up the setting.
	inline const QString databaseDriverType() const {return getSetting(SettingName::type);} //!< Get setting for db driver type.
	inline const QString hostName() const {return getSetting(SettingName::host);} //!< Get setting for db hostname.
	inline const QString databaseName() const {return getSetting(SettingName::databaseName);} //!< Get setting for db name.
	inline const QString userName() const {return getSetting(SettingName::userName);} //!< Get setting for username.
	inline const QString password() const{return getSetting(SettingName::password);} //!< Get setting for password.
	inline const QString port() const {return getSetting(SettingName::port);} //!< Get setting for port to use for e.g. Postgres connection type.
	QStringList getDBSettingsList() const; //!< Get the QStringList property used for db settings initialization.

	inline void setSetting(const SettingName inSettingName, const QString& inValue) {dbSettings[inSettingName] = inValue;} //!< Setter for db setting property by name. \param inSettingName the name to map the setting to. \param inValue The setting value to store using the name param.
	inline void setDatabaseDriverType(const QString& inTypeName) {setSetting(SettingName::type, inTypeName);} //!< Setter for the db driver type. \param inTypeName The db type to set.
	inline void setHostName(const QString& hostName);
	inline void setDatabaseName(const QString& inDbName){setSetting(SettingName::databaseName, inDbName);} //!< Setter for the db name. \param inDbName The db name to set.
	inline void setUserName(const QString& inUserName){setSetting(SettingName::userName, inUserName);} //!< Setter for the username. \param inUserName username to set.
	inline void setPassword(const QString& inPassword){setSetting(SettingName::password, inPassword);} //!< Setter for the password. \param inPassword the password to set.
	inline void setPort(const QString& inPort) {setSetting(SettingName::port, inPort);} //!< Setter for the port. \param inPort Port to set.
	void setDBSettingsList(const QStringList& inDBSettings); //!< Setter for the db settings list. \param inDBSettings The QStringList to set the settings from.

	void readFromSettings(const QSettings& settings);

private:
	std::unordered_map<SettingName, QString> dbSettings; //!< The private QStringList of the db settings.

private:
	void initializeDBSettingsList();
};

#endif // DATABASESETTINGS_H
