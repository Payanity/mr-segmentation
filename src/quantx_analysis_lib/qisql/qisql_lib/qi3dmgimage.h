#ifndef QI3DMGIMAGE_H
#define QI3DMGIMAGE_H

#include "qi3dimage.h"

/*!
 * \brief Image class for storing, retrieving, and interacting with 3D Mammogram immages.
 * \ingroup SQLModule
 * \class Qi3DMGImage
 */
class QISQL_EXPORT Qi3DMGImage : public Qi3DImage
{
public:
    Qi3DMGImage();
    int openSeries(QString uid, const QStringList &dbSettings);

protected:
    QString viewPosition; //!< Stores the view position.
    QString imageLaterality; //!< Stores the image laterality.
    QString patientOrientation; //!< Stores the patient orientation.
    QString photometricInterpretation; //!< Stores the image's photometric interpretation.

private:
    int getSeriesData(const QStringList &dbSettings);

};

#endif // QI3DMGIMAGE_H
