#include "qisql_mainwindow.h"
#include "sqlsearchwindow.h"
#include "gdcmImageReader.h"
#include <QFile>
#include <QDirIterator>
#include <QFileDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QVector>
#include <QFuture>
#include <QFutureWatcher>
#include <QtConcurrentRun>
#include <QEventLoop>
#include <QProgressBar>
#include <QCoreApplication>
#include <QMessageBox>

#define useDebug 0
#if useDebug
#include <QDebug>
#include <QPair>
#define DUMPALLVALUES 0
#endif

/*!
 * \brief Constructor that takes a parent for cleanup, view hierarchy, and signal/slot communication.
 * \param parent The QWidget parent of this window.
 */
QISqlMainWindow::QISqlMainWindow(QWidget *parent) :
    QDialog(parent)
{
	QPushButton* openButton = new QPushButton("Add Files to Database");
	//get rid of warning, if we want to ever use this window again, we need to fix the connection
	//connect(openButton,SIGNAL(pressed()),this,SLOT(openFile()));
	openButton->setObjectName("QISqlMainWindowOpenButton");
	QPushButton* searchButton = new QPushButton("Search Database Records");
	connect(searchButton, &QPushButton::pressed, this, &QISqlMainWindow::openSearchWindow);
	QPushButton* closeButton= new QPushButton("Exit");
	connect(closeButton, &QPushButton::pressed, this, &QISqlMainWindow::close);
	auto* vbox = new QVBoxLayout;
	vbox->addWidget(openButton);
	vbox->addWidget(searchButton);
	vbox->addWidget(closeButton);
	setLayout(vbox);
}

/*!
 * \brief Open DICOM files to add to the DB.
 * \param dbSettings Settings to use for connection to DB.
 */
void QISqlMainWindow::openFile(const QStringList &dbSettings)
{
	QStringList fileList = QFileDialog::getOpenFileNames(this,tr("Open Dicom Images"));
	if ( fileList.isEmpty() ){
		return;
	}
	QScopedPointer<QProgressDialog, QScopedPointerDeleteLater> progressDialog(new QProgressDialog("Adding Selected Files to Database","Please Wait...",0,fileList.size(),this));
	progressDialog->setValue(1);
	progressDialog->setModal(false);
	progressDialog->show();
	QObject::connect(this, &QISqlMainWindow::iteratorValue, progressDialog.data(), &QProgressDialog::setValue);

	//Run the SQL INSERT commands in a separate thread so that the UI still updates
	QEventLoop sqlWaitLoop;
	QFuture<void> sqlFuture;
	QFutureWatcher<void> sqlWatcher;
#if QT_VERSION >= 0x050700
	QObject::connect(&sqlWatcher, QOverload<>::of(&QFutureWatcher<void>::finished), &sqlWaitLoop, &QEventLoop::quit);
#else
	QObject::connect(&sqlWatcher,SIGNAL(finished()),&sqlWaitLoop,SLOT(quit()));
#endif
	sqlFuture = QtConcurrent::run(this,&QISqlMainWindow::fileLoop,&fileList,dbSettings);
	sqlWatcher.setFuture(sqlFuture);
	sqlWaitLoop.exec();

	//fileLoop(progressDialog,&fileList);
	progressDialog->setValue(progressDialog->maximum());
}

/*!
 * \brief Open a directory of DICOM files to add.
 * \param dbSettings DB settings to use to connect.
 * \param recurse Flag whether we should recurse through subdirectories or not.
 */
void QISqlMainWindow::openDir(const QStringList &dbSettings, bool recurse)
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory of DICOMs"));
	if ( dir.isEmpty() ){
		return;
	}
	QFileInfoList fileInfoList;
	QStringList fileList;
	QDir directory(dir);
	if (recurse) {
		QDirIterator it(dir, QDir::Files, QDirIterator::Subdirectories);
		while (it.hasNext()){
			fileInfoList << QFileInfo(it.next());
		}
#if useDebug
		qDebug() << "Total files:" << fileInfoList.size();
#endif
	} else {
		directory.setFilter(QDir::Files);
		fileInfoList = directory.entryInfoList();
	}
	for(const auto & file : std::as_const(fileInfoList))
		fileList << file.absoluteFilePath();
	if (!fileList.isEmpty()){
#if useDebug
		qDebug() << "Going to add " << fileList.size() << " files";
#endif
		QScopedPointer<QProgressDialog, QScopedPointerDeleteLater> progressDialog(new QProgressDialog("Adding Selected Files to Database", "Please Wait...", 0, fileList.size(), this));
		progressDialog->setValue(1);
		progressDialog->setModal(false);
		progressDialog->show();
		QObject::connect(this, &QISqlMainWindow::iteratorValue, progressDialog.data(), &QProgressDialog::setValue);

		//Run the SQL INSERT commands in a separate thread so that the UI still updates
		QEventLoop sqlWaitLoop;
		QFuture<void> sqlFuture;
		QFutureWatcher<void> sqlWatcher;
#if QT_VERSION >= 0x050700
		QObject::connect(&sqlWatcher, QOverload<>::of(&QFutureWatcher<void>::finished), &sqlWaitLoop, &QEventLoop::quit);
#else
		QObject::connect(&sqlWatcher, SIGNAL(finished()), &sqlWaitLoop, SLOT(quit()));
#endif
		sqlFuture = QtConcurrent::run(this, &QISqlMainWindow::fileLoop, &fileList, dbSettings);
		sqlWatcher.setFuture(sqlFuture);
		sqlWaitLoop.exec();

		progressDialog->setValue(progressDialog->maximum());
	}
    }

QByteArray getTagValue(const gdcm::Item &item, const gdcm::Tag &tag)
{
	if( !(item.FindDataElement(tag)) )
		return QByteArray();
	gdcm::DataElement element = item.GetDataElement(tag);
	if( element.GetVL() == 0)
		return QByteArray();
#if defined(DUMPALLVALUES) && DUMPALLVALUES
	qDebug() << QByteArray(element.GetByteValue()->GetPointer(), element.GetVL()).trimmed(); // Dump DICOMDIR contents
#endif
	return QByteArray(element.GetByteValue()->GetPointer(), static_cast<int>(element.GetVL()));
}

/*!
 * \brief Open a DICOMDIR file.
 * \param dbSettings DB Settings for connecting to DB.
 */
void QISqlMainWindow::openDICOMDIR(const QStringList &dbSettings)
{
	QString filename = QFileDialog::getOpenFileName(this,tr("Open DICOMDIR file"));
	gdcm::Reader reader;
#ifdef Q_OS_WIN
	std::ifstream infile(filename.toStdWString(), std::ios::binary);
#else
	std::ifstream infile(filename.toStdString(), std::ios::binary);
#endif
	reader.SetStream(infile);
	//reader.SetFileName(filename.toUtf8().data());

	if(!reader.Read()){
		QMessageBox::warning(this, "Bad DICOMDIR File", QString("Unable to read file\n%1").arg(filename));
		return;
	}

	gdcm::File file = reader.GetFile();
	gdcm::FileMetaInformation fileMetaInformation = file.GetHeader();

#if useDebug
	std::stringstream s;
	int iterationProgress = 0;
	s << fileMetaInformation;
	qDebug() << s.str().c_str();
#endif

	gdcm::DataSet dataSet = file.GetDataSet();
	gdcm::Item item;                    // temporary item
	QVector<gdcm::Tag> taglist;           // temporary tag list
	QMap<gdcm::Tag, QByteArray> tagmap; // tag values to set when inserting image

	gdcm::MediaStorage mediaStorage;
	mediaStorage.SetFromFile(file);
	if(gdcm::MediaStorage::MSType(mediaStorage) != gdcm::MediaStorage::MediaStorageDirectoryStorage){
		QMessageBox::warning(this, "Bad DICOMDIR File", QString("This file is not a DICOMDIR\n\nMedia storage type: %1").arg(mediaStorage.GetString()));
		return;
	}

	if(fileMetaInformation.FindDataElement(gdcm::Tag(0x0002, 0x0002))){
		gdcm::DataElement element = fileMetaInformation.GetDataElement(gdcm::Tag(0x0002, 0x0002));
		QByteArray sopClassUID(element.GetByteValue()->GetPointer(), static_cast<int>(element.GetVL()));
		if( sopClassUID != "1.2.840.10008.1.3.10" ){
			QMessageBox::warning(this, "Bad DICOMDIR File", QString("This file is not a DICOMDIR\n\nSOP class UID: %1").arg(QString(sopClassUID)));
			return;
		}
	}
	else{
		QMessageBox::warning(this, "Bad DICOMDIR File", QString("This file is not a DICOMDIR\n\nMissing SOP class UID"));
		return;
	}

	QScopedPointer<QISql> qisql(new QISql(dbSettings));

	gdcm::Tag directoryRecordTag(0x0004, 0x1430);   // Directory Record Type
	QByteArray directoryRecordValue;                // Directory Record Value
	auto dataSetIterator = dataSet.GetDES().begin();
	while(dataSetIterator != dataSet.GetDES().end()){
		gdcm::DataElement dataElement = *dataSetIterator++;
#if useDebug
		qDebug() << "Scanning Data Element" << ++iterationProgress << ":" << dataElement.GetTag().PrintAsPipeSeparatedString().c_str();
#endif

		if(dataElement.GetTag() == gdcm::Tag(0x0004, 0x1220)){ // Directory Record Sequence
			gdcm::SmartPointer<gdcm::SequenceOfItems> sequence = dataElement.GetValueAsSQ();
			QScopedPointer<QProgressDialog, QScopedPointerDeleteLater> progressDialog(new QProgressDialog("Adding Files to Database", "Please Wait...", 0, static_cast<int>(sequence->GetNumberOfItems()), this));
			progressDialog->setModal(false);
			QEventLoop sqlWaitLoop;
			QFuture<int> sqlFuture;
			QFutureWatcher<int> sqlWatcher;
#if QT_VERSION >= 0x050700
			QObject::connect(&sqlWatcher, QOverload<>::of(&QFutureWatcher<int>::finished), &sqlWaitLoop, &QEventLoop::quit);
#else
			QObject::connect(&sqlWatcher, SIGNAL(finished()), &sqlWaitLoop, SLOT(quit()));
#endif
			ulong itemNum = 0;
			while(itemNum < sequence->GetNumberOfItems()){
				directoryRecordValue = getTagValue(item = sequence->GetItem(++itemNum), directoryRecordTag);
				while(directoryRecordValue.trimmed() == "PATIENT"){
					taglist.clear();
					taglist << gdcm::Tag(0x0010, 0x0010); // Patient Name
					taglist << gdcm::Tag(0x0010, 0x0020); // Patient MRN
					taglist << gdcm::Tag(0x0010, 0x0040); // Patient Sex
					for(const auto & tag : std::as_const(taglist))
						tagmap.insert(tag, getTagValue(item, tag));

					directoryRecordValue = getTagValue(item = sequence->GetItem(++itemNum), directoryRecordTag);
					while(directoryRecordValue.trimmed() == "STUDY"){
						taglist.clear();
						taglist << gdcm::Tag(0x0008, 0x0020); // Study Date
						taglist << gdcm::Tag(0x0008, 0x1030); // Study Description
						taglist << gdcm::Tag(0x0020, 0x000d); // Study UID
						for(const auto & tag : std::as_const(taglist))
							tagmap.insert(tag, getTagValue(item, tag));

						directoryRecordValue = getTagValue(item = sequence->GetItem(++itemNum), directoryRecordTag);
						while(directoryRecordValue.trimmed() == "SERIES"){
							taglist.clear();
							taglist << gdcm::Tag(0x0008, 0x0060); // Series Modality
							taglist << gdcm::Tag(0x0008, 0x103e); // Series Description
							taglist << gdcm::Tag(0x0020, 0x000e); // Series UID
							taglist << gdcm::Tag(0x0020, 0x0011); // Series Number
							taglist << gdcm::Tag(0x0018, 0x1030); // Protocol Name
							for(const auto & tag : std::as_const(taglist))
								tagmap.insert(tag, getTagValue(item, tag));

							// Remove Null Values Before Adding Images To Database
							const auto tagkeys = tagmap.keys(QByteArray());
							for(const auto & tag : tagkeys)
								tagmap.remove(tag);
#if useDebug
							qDebug() << "tagmap:";
							for(const auto & key : tagmap.keys())
								qDebug() << key.PrintAsPipeSeparatedString().c_str() << tagmap.value(key);
#endif

							directoryRecordValue = getTagValue(item = sequence->GetItem(++itemNum), directoryRecordTag);
#if useDebug
							qDebug() << "Directory Record Value:" << directoryRecordValue;
#endif
							while(directoryRecordValue.trimmed() != "IMAGE" &&
							      directoryRecordValue.trimmed() != "SERIES" &&
							      directoryRecordValue.trimmed() != "STUDY" &&
							      directoryRecordValue.trimmed() != "PATIENT" &&
							      itemNum < sequence->GetNumberOfItems()) {
								directoryRecordValue = getTagValue(item = sequence->GetItem(++itemNum), directoryRecordTag);
							}
							while(directoryRecordValue.trimmed() == "IMAGE"){
								taglist.clear();
								taglist << gdcm::Tag(0x0004, 0x1511); // Image UID
								for(const auto & tag : std::as_const(taglist))
									tagmap.insert(tag, getTagValue(item, tag));

								QString imageFilename = filename.mid(0, QDir::toNativeSeparators(filename).lastIndexOf(QDir::separator()))
								                      + QDir::separator()
								                      + getTagValue(item, gdcm::Tag(0x0004, 0x1500));
								imageFilename = QDir::toNativeSeparators(imageFilename);

#if useDebug
								qDebug() << "taglist:" << getTagValue(item, gdcm::Tag(0x0004, 0x1511));
								if( !QFile(imageFilename).exists() )
									qDebug() << "File Not Found:" << imageFilename;
								else
									qDebug() << "File Found:" << imageFilename;
#endif

								// The gdcm::ImageReader needs to be created in the other thread so that it is not a const, use addFile
								sqlFuture = QtConcurrent::run(qisql.data(), &QISql::addFile, imageFilename, tagmap);
								sqlWatcher.setFuture(sqlFuture);
								sqlWaitLoop.exec();
								progressDialog->setValue(static_cast<int>(itemNum));

								// Next Image
								if(itemNum < sequence->GetNumberOfItems())
									directoryRecordValue = getTagValue(item = sequence->GetItem(++itemNum), directoryRecordTag);
								else
									break;
							}
							while(directoryRecordValue.trimmed() == "PRESENTATION"){
								// Skip these for now, we don't need them
								if(itemNum < sequence->GetNumberOfItems())
									directoryRecordValue = getTagValue(item = sequence->GetItem(++itemNum), directoryRecordTag);
								else
									break;
							}
						}
					}
				}
			}
		}
	}

	QMessageBox::warning(this,"Import DICOMDIR", "Finished importing images from DICOMDIR");
}


/*!
 * \brief Iterate through a file list in loop form.
 * \param fileList List of files to iterate on.
 * \param dbSettings DB settings for connecting to DB.
 */
void QISqlMainWindow::fileLoop(QStringList* fileList,const QStringList &dbSettings)
{

	QScopedPointer<QISql> qisql(new QISql(dbSettings));

	int numfiles = fileList->size();
#if useDebug
	if(numfiles > 0){
		qDebug() << fileList->at(0);
		auto chars = fileList->at(0).mid(31,7);
		qDebug() << chars;
		auto firstchar = chars.at(0);
		qDebug() << firstchar;
		qDebug() << QString::number(firstchar.unicode(), 16);

		std::ifstream teststream;
		teststream.open(fileList->at(0).toStdWString(), std::ios_base::binary);
		QByteArray filebytes;
		for(auto i = 0; i < 512; ++i){
			filebytes.append(static_cast<char>(teststream.get()));
			if(!teststream.good())
				break;
		}
		for(auto i = 0; i < filebytes.size(); i += 32)
			qDebug() << filebytes.mid(i, 32).toHex(' ');
		qDebug() << "teststream good:" << teststream.good();
		qDebug() << "teststream fail:" << teststream.fail();
		qDebug() << "teststream bad :" << teststream.bad();
		qDebug() << "teststream eof :" << teststream.eof();
		teststream.close();
	}
#endif
	for(auto i = 0; i < numfiles; ++i){
		emit iteratorValue(i);

		gdcm::ImageReader reader;
#ifdef Q_OS_WIN
		//std::experimental::filesystem::path infilepath(fileList->at(i).toStdWString());
		std::ifstream infile(fileList->at(i).toStdWString(), std::ios::binary);
#if useDebug
		qDebug() << "infile good:" << infile.good();
		qDebug() << "infile open:" << infile.is_open();
		auto filewstring = fileList->at(i).toStdWString();
		qDebug() << filewstring;
		qDebug() << QString::number(filewstring[31], 16);
#endif
#else
		std::ifstream infile(fileList->at(i).toStdString(), std::ios::binary);
#endif
		reader.SetStream(infile);
		//reader.SetFileName(fileList->at(i).toUtf8().data());
		qisql->addItem(reader);
		infile.close();
	}
}

/*!
 * \brief Open a SQL search window to search for cases.
 */
void QISqlMainWindow::openSearchWindow()
{
	QStringList dbSettings;
#ifdef Q_OS_WIN
	dbSettings << "QSQLITE" << "" << "C:\\QuantX\\quantx.db" << "" << "";
#else
#ifdef Q_OS_MAC
	dbSettings << "QSQLITE" << "" << QString(QCoreApplication::applicationDirPath() + "/../Database/quantx.db") << "" << "";
#else
	dbSettings << "QSQLITE" << "" << QDir::homePath() + "/quantx.db" << "" << "";
#endif
#endif
	auto* searchWindow = new SqlSearchWindow(dbSettings,this);
	searchWindow->show();
	searchWindow->setAttribute(Qt::WA_DeleteOnClose);
}
