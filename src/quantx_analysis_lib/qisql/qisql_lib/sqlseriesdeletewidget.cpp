#include "sqlseriesdeletewidget.h"
#include <QTreeWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QApplication>
#include <QMessageBox>
#include <QLabel>
#include "qisql.h"
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QFuture>
#include <QtConcurrent>
#include <QSqlError>

#define useDebug 0
#if useDebug
#include <QDebug>
#include <QThread>
#endif

/*!
 * \brief Constructor that takes a list of DB settings and a parent.
 * \param dbSettings DB settings list.
 * \param parent Parent used for view hierarchy, cleanup, signalling.
 */
SqlSeriesDeleteWidget::SqlSeriesDeleteWidget(const QStringList &dbSettings, QWidget *parent) :
    QDialog(parent)
{
	databaseSettings = dbSettings;

	auto *vbox = new QVBoxLayout;

	treeWidget = new QTreeWidget;
	treeWidget->setObjectName("deleteWindowTreeWidget");
	treeWidget->setHeaderHidden(true);
	connect(treeWidget, &QTreeWidget::itemChanged, this, &SqlSeriesDeleteWidget::itemChecked);
	initializeTreeWidget();
	vbox->addWidget(treeWidget,9);

	QPushButton* deleteButton = new QPushButton("Delete");
	deleteButton->setFixedWidth(100);
	vbox->addWidget(deleteButton,1,Qt::AlignRight);
	connect(deleteButton, &QPushButton::clicked, this, &SqlSeriesDeleteWidget::deleteButtonClicked);

	QPushButton* doneButton = new QPushButton("Done");
	doneButton->setFixedWidth(100);
	vbox->addWidget(doneButton,1,Qt::AlignRight);
	connect(doneButton, &QPushButton::clicked, this, &SqlSeriesDeleteWidget::close);
	//treeWidget->setColumnCount(2);

	setLayout(vbox);
	setWindowFlags(Qt::Window);
	setWindowTitle("Select Images To Remove From Database");
	resize(500,800);
}

/*!
 * Init the tree widget for deleting series.
 */
void SqlSeriesDeleteWidget::initializeTreeWidget()
{
	{
		treeWidget->clear();
		QScopedPointer<QISql> qisql(new QISql(databaseSettings));
		QSqlDatabase db = qisql->createDatabase("QIdeleteFromDatabaseInitializeTree");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(this,"Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		}

		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlPatientQuery(new QSqlQuery(db));
		QScopedPointer<QSqlQuery> sqlStudyQuery(new QSqlQuery(db));
		QScopedPointer<QSqlQuery> sqlSeriesQuery(new QSqlQuery(db));

		QString patientsTable = qisql->patientsTable();
		QSqlRecord patientRecord   = db.record(patientsTable);
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,patientsTable,patientRecord,false);

		QString studiesTable = qisql->studiesTable();
		QSqlRecord studyRecord   = db.record(studiesTable);
		QSqlRecord studyMRNOnly  = db.record(studiesTable);
		studyMRNOnly.setValue("mrn","temp");
		for(int i=studyMRNOnly.count()-1;i>=0;i--)
			if(studyMRNOnly.isNull(i))
				studyMRNOnly.remove(i);

		QString seriesTable = qisql->seriesTable();
		QSqlRecord seriesRecord   = db.record(seriesTable);
		QSqlRecord seriesStudyUIDOnly  = db.record(seriesTable);
		seriesStudyUIDOnly.setValue("study_uid","temp");
		for(int i=seriesStudyUIDOnly.count()-1;i>=0;i--)
			if(seriesStudyUIDOnly.isNull(i))
				seriesStudyUIDOnly.remove(i);


		int index_patient_name          = patientRecord.indexOf("patient_name");
		int index_patient_MRN           = patientRecord.indexOf("mrn");
		int index_study_modality        = studyRecord.indexOf("modality");
		int index_study_date            = studyRecord.indexOf("study_date");
		int index_study_studyUID        = studyRecord.indexOf("study_uid");
		int index_series_seriesDesc     = seriesRecord.indexOf("series_desc");
		int index_series_seriesUID      = seriesRecord.indexOf("series_uid");
		int index_series_seriesNum      = seriesRecord.indexOf("series_number");

		sqlPatientQuery->prepare(sqlStatement);
		sqlPatientQuery->exec();

		while(sqlPatientQuery->next()){
			QString name = sqlPatientQuery->value(index_patient_name).toString().replace("^"," ");
			QString mrn  = sqlPatientQuery->value(index_patient_MRN).toString();
			auto* patientItem = new QTreeWidgetItem;
			patientItem->setText(0,QString("%1 - %2").arg(name,mrn));
			patientItem->setText(1,mrn);

			//Now loop for Studies
			studyMRNOnly.setValue("mrn",mrn);
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,studiesTable,studyRecord,false) + QString(' ')
			        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,studiesTable,studyMRNOnly,true);

			sqlStudyQuery->prepare(sqlStatement);
			for(int i=0;i<studyMRNOnly.count();i++)
				sqlStudyQuery->bindValue(i,studyMRNOnly.value(i));
			sqlStudyQuery->exec();

			while(sqlStudyQuery->next()){
				QString modality = sqlStudyQuery->value(index_study_modality).toString();
				QString date = sqlStudyQuery->value(index_study_date).toString();
				QString studyUID = sqlStudyQuery->value(index_study_studyUID).toString();
				auto* studyItem = new QTreeWidgetItem(patientItem);
				studyItem->setText(0,QString("%1 Study: %2").arg(modality,date));
				studyItem->setText(1,studyUID);
				studyItem->setText(2,modality);

				//Now loop for Series
				seriesStudyUIDOnly.setValue("study_uid",studyUID);
				sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,seriesTable,seriesRecord,false) + QString(' ')
				             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,seriesTable,seriesStudyUIDOnly,true);

				sqlSeriesQuery->prepare(sqlStatement);
				for(int i=0; i<seriesStudyUIDOnly.count(); ++i)
					sqlSeriesQuery->bindValue(i,seriesStudyUIDOnly.value(i));
				sqlSeriesQuery->exec();

				while(sqlSeriesQuery->next()){
					auto* seriesItem = new QTreeWidgetItem(studyItem);
					seriesItem->setText(0,QString("Series %1: %2").arg(sqlSeriesQuery->value(index_series_seriesNum).toInt()).arg(sqlSeriesQuery->value(index_series_seriesDesc).toString()));
					seriesItem->setText(1,sqlSeriesQuery->value(index_series_seriesUID).toString());
				}
				sqlSeriesQuery->clear();
			}

			sqlStudyQuery->clear();

			//Now add it to the tree widget as a checkable item
			treeWidget->addTopLevelItem(patientItem);
			patientItem->setCheckState(0,Qt::Unchecked);
		}
		sqlPatientQuery->clear();
		sqlPatientQuery.reset();
		sqlStudyQuery.reset();
	}
	QSqlDatabase::removeDatabase("QIdeleteFromDatabaseInitializeTree");
}

/*!
 * \brief Search children of tree widget iterm.
 * \param item Item to search children of.
 * \param col Column to test state on.
 */
void SqlSeriesDeleteWidget::searchChildren(QTreeWidgetItem *item, int col)
{
	int childCount = item->childCount();
	int totalChecks = 0;

	for(int i=0; i<childCount; ++i)
		totalChecks += item->child(i)->checkState(col);

	if(totalChecks == 0)
		item->setCheckState(col,Qt::Unchecked);
	else if(childCount == totalChecks / 2)
		item->setCheckState(col,Qt::Checked);
	else
		item->setCheckState(col,Qt::PartiallyChecked);

	if(treeWidget->indexOfTopLevelItem(item) == -1)
		searchChildren(item->parent(),col);
}

/*!
 * \brief Item checked handler for the tree widget.
 * \param item Item checked.
 * \param col Column of item checked.
 */
void SqlSeriesDeleteWidget::itemChecked(QTreeWidgetItem *item, int col)
{
	Qt::CheckState isChecked = item->checkState(col);
	if(isChecked != Qt::PartiallyChecked){
		for(int i=0; i<item->childCount(); ++i)
			item->child(i)->setCheckState(col,isChecked);
		QTreeWidgetItem *cursorItem = treeWidget->itemAt(treeWidget->mapFromGlobal(treeWidget->cursor().pos()));
		if(cursorItem != nullptr)
			if(item == cursorItem && treeWidget->indexOfTopLevelItem(item) == -1)
				searchChildren(item->parent(),col);
	}
}

QUANTX::Modality parseModalityString(const QString& string)
{
	if(string == "MG")
		return QUANTX::MAMMO;
	if(string == "US")
		return QUANTX::US;
	if(string == "MR")
		return QUANTX::MRI;
	else
		return QUANTX::NOIMAGE;
}

/*!
 * \brief Delete button clicked handler.
 */
void SqlSeriesDeleteWidget::deleteButtonClicked()
{
	QScopedPointer<QDialog, QScopedPointerDeleteLater> dialog(new QDialog(this, Qt::FramelessWindowHint));
	dialog->setModal(true);
	dialog->setFixedSize(420,120);
	auto* vbox = new QVBoxLayout();
	vbox->setContentsMargins(0,0,0,0);
	QLabel *label = new QLabel(QString("Scanning Images to Delete\n\n\nPlease Wait..."));
	label->setFrameStyle(QFrame::Box | QFrame::Raised);
	label->setLineWidth(4);
	label->setMidLineWidth(3);
	label->setIndent(20);
	vbox->addWidget(label);
	dialog->setLayout(vbox);
	dialog->show();
	qApp->processEvents();
	//Now, run the loop and delete the images
	int numPatients = treeWidget->topLevelItemCount();
	QTreeWidgetItem *patientItem;
	QTreeWidgetItem *studyItem;
	QTreeWidgetItem *seriesItem;
	for(int i=0; i<numPatients; ++i){
		patientItem = treeWidget->topLevelItem(i);
		if(patientItem->checkState(0)){
			QString patientLabelString = (QString("Deleting From Patient %1\n") +
			                                      "\n" +
			                                      "\n" +
			                                      "Please Wait...").arg(patientItem->text(0));
			label->setText(patientLabelString);
			qApp->processEvents();
			int numStudies = patientItem->childCount();
			for(int j=0; j<numStudies; ++j){
				studyItem = patientItem->child(j);
				if(studyItem->checkState(0)){
					QString studyLabelString = (QString("Deleting From Patient %1\n") +
					                                    "Deleting From %2\n" +
					                                    "\n" +
					                                    "Please Wait...").arg(patientItem->text(0),studyItem->text(0));
					label->setText(studyLabelString);
					qApp->processEvents();
					QUANTX::Modality modality = parseModalityString(studyItem->text(2));
					int numSeries = studyItem->childCount();
					for(int k=0; k<numSeries; ++k){
						seriesItem = studyItem->child(k);
						if(seriesItem->checkState(0)){ // This Cannot Be Partially Checked
							label->setText((QString("Deleting From Patient: %1\n") +
							                        "Deleting From %2\n" +
							                        "Deleting From %3\n" +
							                        "Please Wait...").arg(patientItem->text(0),studyItem->text(0),seriesItem->text(0)));
							qApp->processEvents();
							deleteSeries(seriesItem->text(1), modality);
							label->setText(studyLabelString);
							qApp->processEvents();
						}
					}
					if(studyItem->checkState(0) == Qt::Checked) // Not Partially Checked
						deleteStudy(studyItem->text(1));
					label->setText(patientLabelString);
					qApp->processEvents();
				}
			}
			if(patientItem->checkState(0) == Qt::Checked) // Not Partially Checked
				deletePatient(patientItem->text(1));
			label->setText("Scanning Images to Delete\n\n\nPlease Wait...");
			qApp->processEvents();
		}
	}
	label->setText(QString("Finishing Processes\n\n\nPlease Wait..."));
	qApp->processEvents();
	initializeTreeWidget();
	dialog->close();
}

bool execSqlStatement(const QSqlDatabase &db, QSqlDriver::StatementType stype, const QString& table, const QMap<QString,QVariant>& values, QSqlQuery* sqlQuery = nullptr)
{
	QScopedPointer<QSqlQuery> sq;
	if(!sqlQuery){
		sq.reset(new QSqlQuery(db));
		sqlQuery = sq.data();
	}
	QSqlRecord record   = db.record(table);
	QSqlRecord columnRecord  = db.record(table);
	for(int i=0; i<values.count(); ++i)
		columnRecord.setValue(values.keys().at(i),values.values().at(i));
	for(int i=columnRecord.count()-1;i>=0;i--)
		if(columnRecord.isNull(i))
			columnRecord.remove(i);
	QString sqlStatement = db.driver()->sqlStatement(stype,table,record,false) + QString(' ')
	                     + db.driver()->sqlStatement(QSqlDriver::WhereStatement,table,columnRecord,true);

	sqlQuery->prepare(sqlStatement);
	for(int i=0; i<columnRecord.count(); ++i)
		sqlQuery->bindValue(i,columnRecord.value(i));

	auto sqlQueryExec = sqlQuery->exec();
    #if useDebug
	    qDebug()<<"Success?"<<sqlQueryExec;
    #endif
	return sqlQueryExec;
}

/*!
 * \brief Delete a series.
 * \param seriesUID UID of series to delete.
 * \param modality Modality of series to delete.
 */
void SqlSeriesDeleteWidget::deleteSeries(const QString& seriesUID, QUANTX::Modality modality)
{
#if useDebug
	qDebug() << "Delete Series" << seriesUID;
#endif
	{
		QScopedPointer<QISql> qisql(new QISql(databaseSettings));
		QSqlDatabase db = qisql->createDatabase("QIdeleteFromDatabaseSeries");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(this,"Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		}
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		QMap<QString,QVariant> values;
		values.insert("series_uid",seriesUID);
		QMap<QString,QVariant> dicomValues;
		dicomValues.insert("0020,000e",seriesUID);

		QFuture<bool> sqlFuture;

		//Delete Images
		sqlFuture = QtConcurrent::run(execSqlStatement, db, QSqlDriver::DeleteStatement, qisql->imageTable(modality),values,sqlQuery.data());
		sqlFuture.waitForFinished();
		if(!sqlFuture.result())
			QMessageBox::critical(this,"Image Delete Error","The image specified could not be deleted.");

		//Delete DICOM Data
		sqlFuture = QtConcurrent::run(execSqlStatement, db, QSqlDriver::DeleteStatement, qisql->dicomTable(),dicomValues,sqlQuery.data());
		sqlFuture.waitForFinished();
		if(!sqlFuture.result())
			QMessageBox::critical(this,"DICOM Delete Error","The DICOM data specified could not be deleted.");

		//Delete Series Thumbnails
		sqlFuture = QtConcurrent::run(execSqlStatement, db, QSqlDriver::DeleteStatement, qisql->thumbnailTable(),values,sqlQuery.data());
		sqlFuture.waitForFinished();
		if(!sqlFuture.result())
			QMessageBox::critical(this,"Series Thumbnail Delete Error","The series thumbnail specified could not be deleted.");

		//Delete Series Motion Correction
		QMap<QString,QVariant> mcValues;
		mcValues.insert("series_uid_moving", seriesUID);
		sqlFuture = QtConcurrent::run(execSqlStatement, db, QSqlDriver::DeleteStatement, QString("qi_motion_correction"), mcValues, sqlQuery.data());
		sqlFuture.waitForFinished();
		if(!sqlFuture.result())
			QMessageBox::critical(this,"Series Motion Correction Delete Error","The series motion correction specified could not be deleted.");

		//Delete Series
		sqlFuture = QtConcurrent::run(execSqlStatement, db, QSqlDriver::DeleteStatement, qisql->seriesTable(),values,sqlQuery.data());
		sqlFuture.waitForFinished();
		if(!sqlFuture.result())
			QMessageBox::critical(this,"Series Delete Error","The series specified could not be deleted.");

	}
	QSqlDatabase::removeDatabase("QIdeleteFromDatabaseSeries");
}

/*!
 * \brief Delete study.
 * \param studyUID UID of study to delete.
 */
void SqlSeriesDeleteWidget::deleteStudy(const QString& studyUID)
{
#if useDebug
	qDebug() << "Delete Study" << studyUID;
#endif
	{
		QScopedPointer<QISql> qisql(new QISql(databaseSettings));
		QSqlDatabase db = qisql->createDatabase("QIdeleteFromDatabaseStudy");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(this,"Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		}
		QMap<QString,QVariant> values;
		values.insert("study_uid",studyUID);

		QFuture<bool> sqlFuture;

		//Delete Images
		sqlFuture = QtConcurrent::run(execSqlStatement, db, QSqlDriver::DeleteStatement, qisql->studiesTable(), values, (QSqlQuery*)nullptr);
		sqlFuture.waitForFinished();
		if(!sqlFuture.result())
			QMessageBox::critical(this,"Study Delete Error","The study specified could not be deleted.");
	}
	QSqlDatabase::removeDatabase("QIdeleteFromDatabaseStudy");
}

/*!
 * \brief Delete patient.
 * \param mrn MRN of patient to delete.
 */
void SqlSeriesDeleteWidget::deletePatient(const QString& mrn)
{
#if useDebug
	qDebug() << "Delete Patient" << mrn;
#endif
	{
		QScopedPointer<QISql> qisql(new QISql(databaseSettings));
		QSqlDatabase db = qisql->createDatabase("QIdeleteFromDatabasePatient");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(this,"Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		}
		QMap<QString,QVariant> values;
		values.insert("mrn",mrn);

		QFuture<bool> sqlFuture;

		//Delete Images
		sqlFuture = QtConcurrent::run(execSqlStatement, db, QSqlDriver::DeleteStatement, qisql->patientsTable(), values, (QSqlQuery*)nullptr);
		sqlFuture.waitForFinished();
		if(!sqlFuture.result())
			QMessageBox::critical(this,"Patient Delete Error","The patient specified could not be deleted.");
	}
	QSqlDatabase::removeDatabase("QIdeleteFromDatabasePatient");
}
