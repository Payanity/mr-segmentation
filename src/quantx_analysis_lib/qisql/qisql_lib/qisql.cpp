#include "qisql.h"
#include "qisql_private.h"
#include <QSettings>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QThread>
#include "dicomquery.h"
#include <utility>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

/*!
 * \brief Create a QISql object and initialize it.
 * \param dbDriver Name of Qt database driver to use (e.g. QSQLITE, QPSQL)
 * \param dbHostName Host name to use for databse driver
 * \param dbDatabaseName Database name to use
 * \param dbUserName User name for database
 * \param dbPassword Password for database
 * \pre None.
 * \post A QISql object has been initialized.
 */
QISql::QISql(const QString & dbDriver,const QString & dbHostName,const QString & dbDatabaseName,const QString & dbUserName,const QString & dbPassword)
{
	setDatabaseType(dbDriver);
	setHostName(dbHostName);
	setDatabaseName(dbDatabaseName);
	setUserName(dbUserName);
	setPassword(dbPassword);


	sqlImageTable    = "qi_2d_images";
	sqlImageMGTable    = "qi_2d_images";
	sqlImageMRTable    = "qi_2d_images";
	sqlImageUSTable    = "qi_2d_images";
	sqlSeriesTable   = "qi_series";
	sqlStudiesTable  = "qi_studies";
	sqlPatientsTable = "qi_patients";
	sqlThumbnailTable = "qi_series_thumbnails";
	sqlSeriesView    = "qi_seriesview";
	sqlImageLocationsView = "qi_imagelocations";
	sqlDicomTable    = "dicom_data";

	tagList.clear();
}

//! \overload
QISql::QISql(const QStringList & dbSettings)
{
	if(!dbSettings.empty()) {
		setDatabaseType(dbSettings[0]);
	} else {
		setDatabaseType();
	}
	if(dbSettings.size() > 1) {
		setHostName(dbSettings[1]);
	} else {
		setHostName();
	}
	if(dbSettings.size() > 2) {
		setDatabaseName(dbSettings[2]);
	} else {
		setDatabaseName();
	}
	if(dbSettings.size() > 3) {
		setUserName(dbSettings[3]);
	} else {
		setUserName();
	}
	if(dbSettings.size() > 4) {
		setPassword(dbSettings[4]);
	} else {
		setPassword();
	}
	if(dbSettings.size() > 5) {
		setHostName(sqlHostName + ":" + dbSettings[5]);
	}

	sqlImageTable    = "qi_2d_images";
	sqlImageMGTable    = "qi_2d_images";
	sqlImageMRTable    = "qi_2d_images";
	sqlImageUSTable    = "qi_2d_images";
	sqlSeriesTable   = "qi_series";
	sqlStudiesTable  = "qi_studies";
	sqlPatientsTable = "qi_patients";
	sqlThumbnailTable = "qi_series_thumbnails";
	sqlSeriesView    = "qi_seriesview";
	sqlImageLocationsView = "qi_imagelocations";
	sqlDicomTable    = "dicom_data";

	tagList.clear();
}

/*!
 * \brief Add a DICOM file to the database.
 * \param filename Location of a valid DICOM file
 * \param tagmap List of tags that are database columns
 * \return 1 if item is added to database, 0 if it already exists in database, negative for error.
 */
int QISql::addFile(const QString &filename, const QMap<gdcm::Tag, QByteArray> &tagmap)
{
	gdcm::ImageReader reader;
#ifdef Q_OS_WIN
	std::ifstream infile(filename.toStdWString(), std::ios::binary);
#else
	std::ifstream infile(filename.toStdString(), std::ios::binary);
#endif
	reader.SetStream(infile);
	//reader.SetFileName(filename.toUtf8().data());
	return addItem(reader, tagmap);
}

/*!
 * \brief Create list of DICOM tags and add to tagList.
 * \details This is the list of columns in the DICOM database table.
 * \pre The QISql object is initialized.
 * \post tagList updated.
 */
void QISql::createTagList(){
	//List of tags for Dicom Table in SQL database
	tagList.clear();
#if useDebug
	qDebug()<<"Creating taglist";
#endif
	tagList << QPoint(0x0008,0x0008) << // Image Type
	           QPoint(0x0008,0x0016) << // SOP Class UID
	           QPoint(0x0008,0x0018) << // SOP Instance UID
	           QPoint(0x0008,0x0020) << // Study Date
	           QPoint(0x0008,0x0021) << // Series Date
	           QPoint(0x0008,0x0022) << // Acquisition Date
	           QPoint(0x0008,0x0023) << // Content Date
	           QPoint(0x0008,0x0030) << // Study Time
	           QPoint(0x0008,0x0031) << // Series Time
	           QPoint(0x0008,0x0032) << // Acquisition Time
	           QPoint(0x0008,0x0033) << // Content Time
	           QPoint(0x0008,0x0050) << // Accession Number
	           QPoint(0x0008,0x0060) << // Modality
	           QPoint(0x0008,0x0070) << // Manufacturer
	           QPoint(0x0008,0x0090) << // Referring Physician
	           QPoint(0x0008,0x1090) << // Manufacturer's Model Name
	           QPoint(0x0010,0x0010) << // Patient's Name
	           QPoint(0x0010,0x0020) << // Patient ID
	           QPoint(0x0010,0x0030) << // Patient's Birth Date
	           QPoint(0x0010,0x0040) << // Patient's Sex
	           QPoint(0x0010,0x1010) << // Patient's Age
	           QPoint(0x0010,0x21c0) << // Pregnancy Status  ----- UNSINGED SHORT
	           QPoint(0x0018,0x0020) << // Scanning Sequence
	           QPoint(0x0018,0x0021) << // Sequence Variant
	           QPoint(0x0018,0x0022) << // Scan Options
	           QPoint(0x0018,0x0023) << // MR Acquisition Type
	           QPoint(0x0018,0x0024) << // Sequence Name
	           QPoint(0x0018,0x0025) << // Angio Flag
	           QPoint(0x0018,0x0050) << // Slice Thickness
	           QPoint(0x0018,0x0080) << // Repetition Time
	           QPoint(0x0018,0x0081) << // Echo Time
	           QPoint(0x0018,0x0082) << // Inversion Time
	           QPoint(0x0018,0x0083) << // Number of Averages
	           QPoint(0x0018,0x0084) << // Imaging Frequency
	           QPoint(0x0018,0x0085) << // Imaged Nucleus
	           QPoint(0x0018,0x0086) << // Echo Number(s)
	           QPoint(0x0018,0x0087) << // Magnetic Field Strength
	           QPoint(0x0018,0x0088) << // Spacing Between Slices
	           QPoint(0x0018,0x0089) << // Number of Phase Encoding Steps
	           QPoint(0x0018,0x0090) << // Data Collection Diameter
	           QPoint(0x0018,0x0091) << // Echo Train Length
	           QPoint(0x0018,0x0093) << // Percent Sampling
	           QPoint(0x0018,0x0094) << // Percent Phase Field of View
	           QPoint(0x0018,0x0095) << // Pixel Bandwidth
	           QPoint(0x0018,0x1020) << // Software Version(s)
	           QPoint(0x0018,0x1040) << // Contrast/Bolus Route
	           QPoint(0x0018,0x1041) << // Contrast/Bolus Volume
	           QPoint(0x0018,0x1042) << // Contrast/Bolus Start Time
	           QPoint(0x0018,0x1043) << // Contrast/Bolus Stop Time
	           QPoint(0x0018,0x1044) << // Contrast/Bolus Total Dose
	           QPoint(0x0018,0x1045) << // Syringe Counts
	           QPoint(0x0018,0x1046) << // Contrast Flow Rate
	           QPoint(0x0018,0x1047) << // Contrast Flow Duration
	           QPoint(0x0018,0x1048) << // Contrast/Bolus Ingredient
	           QPoint(0x0018,0x1049) << // Contrast/Bolus Ingredient Concentration
	           QPoint(0x0018,0x1050) << // Spatial Resolution
	           QPoint(0x0018,0x1060) << // Trigger Time
	           QPoint(0x0018,0x1088) << // Heart Rate
	           QPoint(0x0018,0x1090) << // Cardiac Number of Images
	           QPoint(0x0018,0x1094) << // Trigger Window
	           QPoint(0x0018,0x1100) << // Reconstruction Diameter
	           QPoint(0x0018,0x1250) << // Receive Coil Name
	           QPoint(0x0018,0x1310) << // Acquisition Matrix  ----- UNSINGED SHORT
	           QPoint(0x0018,0x1312) << // In-plane Phase Encoding Direction
	           QPoint(0x0018,0x1314) << // Flip Angle
	           QPoint(0x0018,0x1315) << // Variable Flip Angle Flag
	           QPoint(0x0018,0x1316) << // SAR
	           QPoint(0x0018,0x5100) << // Patient Position
	           QPoint(0x0018,0x9075) << // Diffusion Directionality
	           QPoint(0x0018,0x9087) << // Diffusion BValue
	           QPoint(0x0018,0x9089) << // Diffusion GradientOrientation
	           QPoint(0x0020,0x000d) << // Study Instance UID
	           QPoint(0x0020,0x000e) << // Series Instance UID
	           QPoint(0x0020,0x0010) << // Study ID
	           QPoint(0x0020,0x0011) << // Series Number
	           QPoint(0x0020,0x0012) << // Acquisition Number
	           QPoint(0x0020,0x0013) << // Instance Number
	           QPoint(0x0020,0x0032) << // Image Position (Patient)
	           QPoint(0x0020,0x0037) << // Image Orientation (Patient)
	           QPoint(0x0020,0x0052) << // Frame of Reference UID
	           QPoint(0x0020,0x0060) << // Laterality
	           QPoint(0x0020,0x0100) << // Temporal Position Identifier
	           QPoint(0x0020,0x0105) << // Number of Temporal Positions
	           QPoint(0x0020,0x0110) << // Temporal Resolution
	           QPoint(0x0020,0x1040) << // Position Reference Indicator
	           QPoint(0x0020,0x1041) << // Slice Location
	           QPoint(0x0028,0x0002) << // Samples per Pixel  ----- UNSINGED SHORT
	           QPoint(0x0028,0x0003) << // Samples per Pixel Used  ----- UNSINGED SHORT
	           QPoint(0x0028,0x0004) << // Photometric Interpretation
	           QPoint(0x0028,0x0010) << // Rows    (Image Height)  ----- UNSINGED SHORT
	           QPoint(0x0028,0x0011) << // Columns (Image Width)  ----- UNSINGED SHORT
	           QPoint(0x0028,0x0030) << // Pixel Spacing
	           QPoint(0x0028,0x0100) << // Bits Allocated  ----- UNSINGED SHORT
	           QPoint(0x0028,0x0101) << // Bits Stored  ----- UNSINGED SHORT
	           QPoint(0x0028,0x0102) << // High Bit  ----- UNSINGED SHORT
	           QPoint(0x0028,0x0103) << // Pixel Representation  ----- UNSINGED SHORT
	           QPoint(0x0028,0x0106) << // Smallest Image Pixel Value  ----- SINGED SHORT
	           QPoint(0x0028,0x0107) << // Largest Image Pixel Value  ----- SINGED SHORT
	           QPoint(0x0028,0x0120) << // Pixel Padding Value  ----- SINGED SHORT
	           QPoint(0x0028,0x1050) << // Window Center
	           QPoint(0x0028,0x1051) << // Window Width
	           QPoint(0x0032,0x1032) << // Requesting Physician
	           QPoint(0x0040,0x0253) << // Performed Procedure Step ID
	           QPoint(0x0040,0x0254) << // Performed Procedure Step Description
	           QPoint(0x0040,0x9224) << // RealWorldValueIntercept
	           QPoint(0x0040,0x9225) << // RealWorlValueSlope
	           QPoint(0x2001,0x1003) << // Diffusion BValue (Philips)
	           QPoint(0x2001,0x1004) << // Diffusion Gradient Direction (Philips)
	           QPoint(0x2005,0x10b0) << // Diffusion Direction R/L (Philips)
	           QPoint(0x2005,0x10b1) << // Diffusion Direction A/P (Philips)
	           QPoint(0x2005,0x10b2); // Diffusion Direction F/H (Philips)

}

/*!
 * \brief Return the database type (e.g., ‘SQLITE’, “PSQL, etc.)
 * \return Database type
 * \pre The QISql object is initialized
 * \post A string that corresponds the database type is returned.
 */
QString QISql::databaseType(){
	return sqlDatabaseType;
}

/*!
 * \brief Return the hostname of the database.
 * \return Hostname of the database.
 * \pre The QISql object is initialized
 * \post A string that corresponds the hostname of the database is returned.
 */
QString QISql::hostName(){
	return sqlHostName;
}

/*!
 * \brief Return the name of the database.
 * \return Name of the database.
 * \pre The QISql object is initialized
 * \post A string that corresponds the name of the database is returned.
 */
QString QISql::databaseName(){
	return sqlDatabaseName;
}

/*!
 * \brief Return the username used to connect to the database.
 * \return User name used to connect to the database.
 * \pre The QISql object is initialized
 * \post The username used to connect to the database is returned.
 */
QString QISql::userName(){
	return sqlUserName;
}

/*!
 * \brief Return the password used to connect to the database.
 * \return Password used to connect to the database
 * \pre The QISql object is initialized
 * \post The password used to connect to the database is returned.
 */
QString QISql::password(){
	return sqlPassword;
}

/*!
 * \brief Return the name of the database table for images the specified modality.
 * \param modality Image modality to find table for
 * \return The name of the database table for images the specified modality
 * \pre The QISql object is initialized
 * \post The name of the database table for the specified modality is returned.
 */
QString QISql::imageTable(QUANTX::Modality modality){
	if (modality == QUANTX::MAMMO){
		return sqlImageMGTable;
	}if (modality == QUANTX::MG3D){
		return sqlImageMGTable;
	}if (modality == QUANTX::US) {
		return sqlImageUSTable;
	}else if (modality == QUANTX::US3D) {
		return sqlImageUSTable;
	}else if (modality == QUANTX::MRI) {
		return sqlImageMRTable;
	}else return sqlImageTable;
}

/*!
 * \brief Return the name of the database table for storing DICOM series information.
 * \return The name of the database table for storing DICOM series information.
 * \pre The QISql object is initialized
 * \post The name of the database table for storing DICOM series information is returned.
 */
QString QISql::seriesTable(){
	return sqlSeriesTable;
}

/*!
 * \brief Return the name of the database table for storing DICOM studies information.
 * \return The name of the database table for storing DICOM studies information.
 * \pre The QISql object is initialized
 * \post The name of the database table for storing DICOM studies information is returned.
 */
QString QISql::studiesTable(){
	return sqlStudiesTable;
}

/*!
 * \brief Return the name of the database table for storing patient information.
 * \return The name of the database table for storing patient information.
 * \pre The QISql object is initialized
 * \post The name of the database table for storing patient information is returned.
 */
QString QISql::patientsTable(){
	return sqlPatientsTable;
}

/*!
 * \brief Return the name of the database table for storing DICOM data elements of images.
 * \return The name of the database table for storing DICOM data elements of images.
 * \pre The QISql object is initialized
 * \post The name of the database table for storing DICOM data elements of images.
 */
QString QISql::dicomTable(){
	return sqlDicomTable;
}

/*!
 * \brief Return the name of the database table for storing thumbnail images of image series.
 * \return The name of the database table for storing thumbnail images of image series.
 * \pre The QISql object is initialized
 * \post The name of the database table for storing thumbnail images of image series.
 */
QString QISql::thumbnailTable(){
	return sqlThumbnailTable;
}

/*!
 * \brief Return the name of the database view for viewing series information, including number of images in the series.
 * \return The name of the database view for viewing series information
 * \pre The QISql object is initialized
 * \post The name of the database table for viewing series information, including number of images in the series.
 */
QString QISql::seriesView(){
	return sqlSeriesView;
}

/*!
 * \brief Return the name of the database view for viewing image locations (a reduced subset of the columns of the images table).
 * \return The name of the database view for viewing image locations.
 * \pre The QISql object is initialized.
 * \post The name of the database table for viewing image locations.
 */
QString QISql::imageLocationsView(){
	return sqlImageLocationsView;
}

/*!
 * \brief Set the database driver to use.
 * \param dbDriver Database driver to use
 * \return Always true
 * \pre The QISql object is initialized.
 * \post The database driver parameter is updated.
 */
bool QISql::setDatabaseType(QString dbDriver){
	sqlDatabaseType = std::move(dbDriver);
	return true;
}

/*!
 * \brief Set the host name to use.
 * \param dbHostName Host name to use
 * \return Always true
 * \pre The QISql object is initialized.
 * \post The host name parameter is updated.
 */
bool QISql::setHostName(QString dbHostName){
	sqlHostName = std::move(dbHostName);
	return true;
}

/*!
 * \brief Set the database name to use.
 * \param dbDatabaseName Database name to use.
 * \return Always true
 * \pre The QISql object is initialized.
 * \post The database name parameter is updated.
 */
bool QISql::setDatabaseName(QString dbDatabaseName){
	sqlDatabaseName = std::move(dbDatabaseName);
	return true;
}

/*!
 * \brief Set the user name to use.
 * \param dbUserName User name to use
 * \return Always true
 * \pre The QISql object is initialized.
 * \post The user parameter is updated.
 */
bool QISql::setUserName(QString dbUserName){
	sqlUserName = std::move(dbUserName);
	return true;
}

/*!
 * \brief Set the password to use.
 * \param dbPassword Password to use
 * \return Always true
 * \pre The QISql object is initialized.
 * \post The password parameter is updated.
 */
bool QISql::setPassword(QString dbPassword){
	sqlPassword = std::move(dbPassword);
	return true;
}

/*!
 * \brief Pull the complete metadata of an image from the database.
 * \details A multihash is used because some tags may show up in multiple tag groups.
 * \param imageUID Image instance UID to extract DICOM fields from.
 * \return A HashMap where the key is the DICOM tag and the value is the tag value
 * \pre The QISql object is initialized.
 * \post The complete metadata for the requested image is returned.
 */
QMultiHash<QString, QString> QISql::extractAllDicomFields(const QString& imageUID){
	QSqlDatabase db = createDatabase("QIsqlExtractDicomDatabase");
#if useDebug
	    qDebug()<<"Opening DB extractAllDicomFields";
#endif
		bool ok = db.open();  //if ok is false, we should error and quit
	if(!ok){
		int retries = 0;
		while( (!ok) && (retries++ < 5) ){
			QThread::sleep(1); // Wait one second, and retry
		}
		qWarning("Database Error!\nThe specified database could not be opened.\nCheck your database settings and try again.");
	}

	QSqlRecord dicomRecord = db.record(sqlDicomTable);
	QSqlRecord uidRecord = dicomRecord;
	uidRecord.setValue("0008,0018",imageUID);
	for (int i=uidRecord.count()-1;i>=0;i--)
		if(uidRecord.isNull(i)){
			uidRecord.remove(i);
			dicomRecord.setGenerated(i,false);
		}
	dicomRecord.setGenerated("full_header", true);

	QString sqlStatement;
	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
	QMultiHash<QString, QString> dicomOutput;

	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlDicomTable,dicomRecord,false) + QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlDicomTable,uidRecord,false);
	sqlQuery->exec(sqlStatement);
	if(!sqlQuery->next()){
#if useDebug
		qDebug()<<"No Records returned";
#endif
		return dicomOutput;
	}

	sqlQuery->first();
	dicomRecord = sqlQuery->record();
	QByteArray fullHeader = dicomRecord.value("full_header").toByteArray();
#if useDebug
	qDebug()<< "Header Size: "<<fullHeader.size();
#endif
	QString line;
	QString tag, dicomValue;
	const auto lineList = fullHeader.split('\n');
	for(const auto & lineByteArray : lineList){
		line = QString(lineByteArray.data());
		tag = getDicomTag(line);
		dicomValue = getDicomValue(line);
#if useDebug
		qDebug()<<line;
		qDebug()<<"Tag:"<<tag<<" Value: "<<dicomValue;
#endif
		if (tag.length() > 0)
			dicomOutput.insertMulti(tag,dicomValue);
	}
	sqlQuery.reset();
	db.close();

	return dicomOutput;
}

//!
//! \brief Queries the dicom_data table for the unique sets of dicom tag values of the specified series
//! \details Sorts the results by the first dicom tag in the list of dicom tags to return
//! \param seriesUID The dicom series UID used in the WHERE clause of the query
//! \param dicomTags The list of dicom tags to return (e.g., "0008,0031")
//! \prePost{\objInit{QISql}, Returns list of unique lists of dicom tag values sorted by the first dicom tag}
//!
QVector<QVariantList> QISql::getDicomValues(const QString &seriesUID, const QStringList &dicomTags)
{
	QVector<QVariantList> queryResult;
	{
		QSqlDatabase db = createDatabase("QIsqlGetDicomValues");
#if useDebug
		qDebug()<<"Opening DB getDicomValues";
#endif
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			qWarning("Database Error!\nThe specified database could not be opened.\nCheck your database settings and try again.");
		}
		QSqlRecord dicomRecord = db.record(sqlDicomTable);
		QSqlRecord seriesRecord = dicomRecord;
		seriesRecord.setValue("0020,000E", seriesUID);
		for (int i=seriesRecord.count()-1;i>=0;i--){
			if(seriesRecord.isNull(i)){
				seriesRecord.remove(i);
				dicomRecord.setGenerated(i,false);
			}
		}
		for (auto tag : dicomTags){
			dicomRecord.setGenerated(tag, true);
		}
		QString sqlStatement;
		QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement, sqlDicomTable, dicomRecord,false) + QString(' ')
		        + db.driver()->sqlStatement(QSqlDriver::WhereStatement, sqlDicomTable, seriesRecord,false);
		sqlStatement += QString(" ORDER by \"%1\"").arg(dicomTags.first());
		sqlStatement.replace("SELECT", "SELECT DISTINCT");
		sqlQuery->exec(sqlStatement);
		while(sqlQuery->next()){
			QVariantList recordResult;
			for (auto tag : dicomTags){
				recordResult << sqlQuery->value(tag);
			}
			queryResult << recordResult;
		}
	}
	QSqlDatabase::removeDatabase("QIsqlGetDicomValues");
	return queryResult;
}

/*!
 * \brief Read the DICOM tag from binary data.
 * \details Used in extractAllDicomFields()
 * \param line A single line from a DICOM dump. This should contain the tag and its value.
 * \return The DICOM tag found within the string
 * \pre None.
 * \post Return the DICOM tag.
 */
QString QISql::getDicomTag(const QString& line){
	int start, end;
	start = line.indexOf("(",0);
	if (start == -1)
		    return QString();

	    end = line.indexOf(")",start);
		return line.mid(start+1,end-start-1);

}

/*!
 * \brief Read value of a DICOM tag from binary data.
 * \details Used in extractAllDicomFields()
 * \param line A single line from a DICOM dump. This should contain the tag and its value.
 * \return The value found within the string
 * \pre None.
 * \post Return the value.
 */
QString QISql::getDicomValue(QString line) {
	//QString trimLine = line.simplified();
	QString dicomValue;
	//auto indent = 0;
	while(line.startsWith('\t')){
		line.remove(0,1);
		//++indent; // Do we care about the indent?
	}
	auto parsedStrings = line.split("\t");
	if(parsedStrings.size() > 2){
		if(parsedStrings.at(1).startsWith("SQ"))
			dicomValue += "SQ";
		dicomValue += parsedStrings.last().trimmed();
	}
	if(dicomValue.startsWith("Loaded:"))
		dicomValue = QString("<Binary Data>(%1 Bytes)").arg(dicomValue.split(':').last());

	/*int start, end;
	start = trimLine.indexOf("[",0);
	if (start == -1){
		QStringList parsedStrings = trimLine.split(" ");
		if (parsedStrings.count() >= 3 ){
			dicomTag = getDicomTag(line);
			if (dicomTag.length() > 0 && dicomTag.at(3) == '2') //File metadata (0002,*)
				dicomValue = parsedStrings.at(3);
			else
				dicomValue = parsedStrings.at(2);
			if (!dicomValue.startsWith("("))
				return dicomValue.trimmed();
			else {
				start = trimLine.indexOf(dicomValue);
				end = trimLine.indexOf(")",start);
				if (end == -1)
					return QString();
				else
					return trimLine.mid(start+1,end-start-1).trimmed();
			}
		}else
			return QString();
	} else {
		end = trimLine.indexOf("]",start);
		return trimLine.mid(start+1,end-start-1).trimmed();
	}
	*/
	return dicomValue;
}



//!
//! \brief Get the value of the 'id' field in the database for the specified UID and object type
//! \param dbSettings The connections settings for the database
//! \param uid The UID of a dicom object in database of type objectType
//! \param objectType The type of object to find the id for; study, series, or image.
//! \return Returns the value of the 'id' field for the UID and objectType, -1 is returned if the
//! object type is not valid or the object corresponding to uid is not in the database.
//! \note This is not intended to be called directly, use getStudyDbRowId(), getSeriesDbRowId(), or getImageDbRowId() instead.
//!
int getDbRowId(const QStringList & dbSettings, QString uid, QiDbObjectType objectType)
{
	QString tableName;
	QString uidColumn;
	int rowID = -1;
	if (uid.isEmpty())
		return rowID;
	switch (objectType){
	case QiDbObjectType::study:
		tableName = "qi_studies";
		uidColumn = "study_uid";
		break;
	case QiDbObjectType::series:
		tableName = "qi_series";
		uidColumn = "series_uid";
		break;
	case QiDbObjectType::image:
		tableName = "qi_2d_images";
		uidColumn = "image_uid";
		break;
	}

	QScopedPointer<QISql> qisql(new QISql(dbSettings)); // Just use this to get settings
	QString dbConnectionName("QIGetRowID");
	{
		QSqlDatabase db = qisql->createDatabase(dbConnectionName);
		if (db.open()){
			QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
			QSqlRecord uidAndDbId(db.record(tableName));
			QSqlRecord uidOnly(db.record(tableName));
			for (int i=0; i<uidAndDbId.count(); i++){
				uidAndDbId.setGenerated(i, false);
			}
			uidAndDbId.setGenerated( uidColumn, true);
			uidAndDbId.setGenerated("id", true);
			uidOnly.setValue( uidColumn, uid);
			for (int i = uidOnly.count() - 1; i >= 0; i--){
				if (uidOnly.isNull(i))
					uidOnly.remove(i);
			}
			QString sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement, tableName, uidAndDbId,false) + QString(' ')
			                       + db.driver()->sqlStatement(QSqlDriver::WhereStatement, tableName, uidOnly,true);
			sqlQuery->prepare(sqlStatement);
			for(int i=0;i<uidOnly.count();i++)
				sqlQuery->bindValue(i,uidOnly.value(i));
			auto ok = sqlQuery->exec();
			if (ok && sqlQuery->next()){
				rowID = sqlQuery->record().value("id").toInt();
#if useDebug
				qDebug() << QString("Database Row ID for %1: %2").arg(uid).arg(rowID);
#endif
			} else {
#if useDebug
				qDebug() << QString("Error getting database row id for %1").arg(uid);
#endif
			}
		}
	}
	QSqlDatabase::removeDatabase(dbConnectionName);
	return rowID;
}
//!
//! \brief Get the value of the 'id' field in the database for the series specified by seriesUid
//! \param dbSettings The connections settings for the database
//! \param seriesUid The series UID of a dicom series in database
//! \return Returns the value of the 'id' field for the specified by seriesUid, -1 is returned if the
//! series is not in the database.
//!
int QISql::getSeriesDbRowId(const QStringList & dbSettings, QString seriesUid)
{
	return getDbRowId(dbSettings, seriesUid, QiDbObjectType::series);
}

//!
//! \brief Get the value of the 'id' field in the database for the study specified by studyUid
//! \param dbSettings The connections settings for the database
//! \param studyUid The study UID of a dicom study in database
//! \return Returns the value of the 'id' field for the specified studyUid, -1 is returned if the
//! study is not in the database.
//!
int QISql::getStudyDbRowId(const QStringList & dbSettings, QString studyUid)
{
	return getDbRowId(dbSettings, studyUid, QiDbObjectType::study);
}

//!
//! \brief Get the value of the 'id' field in the database for the image specified by imageUid
//! \param dbSettings The connections settings for the database
//! \param imageUid The image UID of a dicom image in database
//! \return Returns the value of the 'id' field for the specified imageUid, -1 is returned if the
//! image is not in the database.
//!
int QISql::getImageDbRowId(const QStringList & dbSettings, QString imageUid)
{
	return getDbRowId(dbSettings, imageUid, QiDbObjectType::image);
}

//!
//! \brief getSeriesInfo Retrieve inforomation from the database for the dicom series in a study
//! \param dbSettings The database connection settings
//! \param studyUID The study UID of the dicom study for which the series information will be retrieved
//! \return A map of where the key is the Dicom series number and value a DicomSeriesInfo object.
//! \note The map may contain more entries than there are series in the study for instances when not all images in the
//! have the same values for the series parameter (e.g., a series may include one or more low resolution 'localizer' images,
//! in which case there would be one entry each for all images in the dicom series that share the exact same series parameters)
//!
QMultiMap<int, quantx::mranalysis::DicomSeriesInfo> QISql::getSeriesInfo(QStringList dbSettings, QString studyUID)
{
	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	// use multimap for cases where images in a series don't all have the same value for a field, e.g., a 3D series that contains a low-res localizer image
	QMultiMap<int, quantx::mranalysis::DicomSeriesInfo> seriesInfo;
	{
		auto db = qisql->createDatabase("getSeriesInfo");
		db.open();
		if (db.isOpen()){
			QSqlQuery query(db);
			QString sqlStatement = QString("SELECT qi_2d_images.series_uid, qi_series.series_number, qi_series.series_desc, ") +
			                       "qi_2d_images.scan_options, qi_2d_images.image_type, count(qi_2d_images.series_uid) as num_images, qi_2d_images.pixelsize_x, " +
			                       "qi_2d_images.pixelsize_y, qi_2d_images.width, qi_2d_images.height FROM qi_2d_images " +
			                       QString("INNER JOIN qi_series ON qi_2d_images.series_uid = qi_series.series_uid where qi_2d_images.study_uid='%1'").arg(studyUID) +
			                       "group by qi_2d_images.series_uid, qi_2d_images.pixelsize_x, qi_2d_images.pixelsize_y, qi_2d_images.width, " +
			                       "qi_2d_images.height, qi_series.series_desc, qi_series.series_number, qi_2d_images.scan_options, qi_2d_images.image_type " +
			                       "ORDER BY cast(qi_series.series_number as int)";
			if (!query.exec(sqlStatement)){
#if useDebug
				qDebug() << "Query Error: " << query.lastError();
#endif
			} else {
				while (query.next()){
					quantx::mranalysis::DicomSeriesInfo series;
					series.studyUID = studyUID;
					series.seriesUID= query.value("series_uid").toString();
					series.seriesDescription = query.value("series_desc").toString();
					series.imageType = query.value("image_type").toString().trimmed().split("\\");
					series.scanOptions = query.value("scan_options").toString().trimmed().split("\\");
					series.pixelSpacing = QPointF(query.value("pixelsize_x").toFloat(),
					                              query.value("pixelsize_y").toFloat());
					series.seriesNumber = query.value("series_number").toInt();
					series.numImages = query.value("num_images").toInt();
					seriesInfo.insert(query.value("series_number").toInt(), series);
				}
			}
		}
	}
	QSqlDatabase::removeDatabase("getSeriesInfo");

	return seriesInfo;

}


/*!
 * \brief Creates a database connection with the specified connection name, using the object's attributes
 * \details This helps create a QSqlDatabase object outside of scope so that the connection may be properly closed within a function scope
 * \param connectionName Name of the connection created
 * \return A QSqlDatabase object that is ready for use
 * \pre The QISql object is initialized
 * \post A Refernce to a new database connection is returned
 */
QSqlDatabase QISql::createDatabase(const QString & connectionName){
	QSqlDatabase db = QSqlDatabase::addDatabase(sqlDatabaseType, connectionName);
	if(db.isValid()){
		if(sqlDatabaseType == "QPSQL" && sqlHostName.contains(':')){
			db.setHostName(sqlHostName.split(':').first());
			db.setPort(sqlHostName.split(':').at(1).toInt());
		}
		else
			db.setHostName(sqlHostName);
		db.setDatabaseName(sqlDatabaseName);
		db.setUserName(sqlUserName);
		db.setPassword(sqlPassword);
	}
	return db;
}
