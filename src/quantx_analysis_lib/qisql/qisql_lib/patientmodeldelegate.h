#include <QStyledItemDelegate>
#include <QPainter>
#include <QImage>
class PatientModelDelegate : public QStyledItemDelegate
{
	Q_OBJECT
public:
	using QStyledItemDelegate::QStyledItemDelegate;
 
	void paint(QPainter *painter, const QStyleOptionViewItem &option,
	           const QModelIndex &index) const override{
		if (index.data().canConvert<QImage>()){
			QImage image = qvariant_cast<QImage>(index.data());
			painter->drawImage(option.rect, image);
		} else {
			QStyledItemDelegate::paint(painter, option, index);
		}
	}
     QSize sizeHint(const QStyleOptionViewItem &option,
	                const QModelIndex &index) const override{
		 if (index.data().canConvert<QImage>()){
			 QImage image = qvariant_cast<QImage>(index.data());
			 return QSize(image.width(), image.height());
		 } else {
			 return QStyledItemDelegate::sizeHint(option, index);
		 }
	 }
	
};
