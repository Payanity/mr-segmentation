#ifndef QISERIESINFO_H
#define QISERIESINFO_H

#include "qisqlDllCheck.h"
#include "../../quantxdata/quantxdata_lib/quantx.h"
#include <QVector>
#include <QTime>
#include <QDate>
#include <QString>
#include <QStringList>
#include <QList>

/*!
 * \class QiSeriesInfo
 * \ingroup SQLModule
 * \brief Class for carrying series info around in RAM, and working with and comparing series.
 */
class QISQL_EXPORT QiSeriesInfo
{
public:
    explicit QiSeriesInfo(QString uid = QString());

    void setModality(const QString &);
    QString modalityString() const;

    void setSeriesUID(const QString &uid) { seriesUID_ = uid; } //!< Setter for the series UID.
    void setStudyUID(const QString &s) { studyUID_ = s; } //!< Setter for the study UID.
    void setSeriesDesc(const QString &s) { seriesDesc_ = s; } //!< Setter for the series description.
    void setDate(const QDate &d) { date_ = d; } //!< Setter for the date.
    void setModality(const QUANTX::Modality &m) { modality_ = m; } //!< Setter for the modality. \overload
    void setNumImages(const int &i) { numImages_ = i; } //!< Setter for the number of images.
    void setNumTimePts(const int &i) { numTimePts_ = i; } //!< Setter for the number of time points.
    void setTime(const QTime &t) { time_ = t; } //!< Setter for the time of the series' acquisition.
    void setImageOrientation(const QVector<int> &orientation) { imageOrientation_ = orientation; } //!< Setter for the orientation.
    void setSeriesNumber(const QString &n) { seriesNumber_ = n; } //!< Setter for the series number.
    void setSpaceBetweenSlices(const QString &s) { spaceBetweenSlices_ = s; } //!< Setter for the space between slices.

    [[nodiscard]] const QString & seriesUID() const { return seriesUID_; } //!< Getter for the series UID.
    [[nodiscard]] const QString & studyUID() const { return studyUID_; } //!< Getter for the study UID.
    [[nodiscard]] const QString & seriesDesc() const { return seriesDesc_; } //!< Getter for the series description.
    [[nodiscard]] const QDate & date() const { return date_; } //!< Getter for the date.
    [[nodiscard]] const QUANTX::Modality & modality() const { return modality_; } //!< Getter for the modality.
    [[nodiscard]] const int & numImages() const { return numImages_; } //!< Getter for the number of images.
    [[nodiscard]] const int & numTimePts() const { return numTimePts_; } //!< Getter for the number of time points.
    [[nodiscard]] const QTime & time() const { return time_; } //!< Getter for the time.
    [[nodiscard]] const QVector<int> & imageOrientation() const { return imageOrientation_; } //!< Getter for the orientation.
    [[nodiscard]] const QString & seriesNumber() const { return seriesNumber_; } //!< Getter for the series number.
    [[nodiscard]] const QString & spaceBetweenSlices() const { return spaceBetweenSlices_; } //!< Getter for the space between slices.

	bool operator ==(const QiSeriesInfo &otherSeries) const {return seriesUID_ == otherSeries.seriesUID();} //!< Equality comparison operator. \return True if the series UIDs match, false otherwise.

private:
    QString mrn; //!< Stores the MRN.
    QString studyUID_; //!< StudyUID that this series belongs to
    QString seriesUID_; //!< Stores the series UID.
    QString seriesNumber_; //!< Stores the series number.
    QString seriesDesc_; //!< Stores the series description.
    QDate date_; //!< Stores the date.
    QTime time_; //!< Stores the time.
    QUANTX::Modality modality_; //!< Stores the modality.
    QVector<int> imageOrientation_; //!< Stores the orientations.

    int numImages_{0}; //!< Stores the number of images.
    int numTimePts_{0}; //!< Stores the number of time points.

    QString spaceBetweenSlices_; //!< Stores the space between slices.
};

#endif // QISERIESINFO_H
