#ifndef QUANTX_SERIESDESCRIPTIONUTILS_H
#define QUANTX_SERIESDESCRIPTIONUTILS_H

#include <QStringList>
#include "qisqlDllCheck.h"

namespace quantx::series {
/*!
 * \class SeriesDescriptionUtils
 * \ingroup SQLModule
 * \brief This is largely used as more or less a struct, but could end up having additional functionality at some point. It is a simple, clean way to transport these description string lists around.
 */
class QISQL_EXPORT SeriesDescriptionUtils {
public:
    SeriesDescriptionUtils() = default; //!< Defaulted constructor

    void setT2SeriesDesc(QStringList s){ t2SeriesDesc_=std::move(s);} //!< Setter for T2 series descriptions.
    void setDynSeriesDesc(QStringList s){ dynSeriesDesc_=std::move(s);} //!< Setter for dynamic series descriptions.
    void setDynMultiSeriesDesc(QStringList s){ dynMultiSeriesDesc_=std::move(s);} //!< Setter for multi series descriptions.
    void setAdcSeriesDesc(QStringList s){ adcSeriesDesc_=std::move(s);} //!< Setter for ADC series descriptions.
    void setDwiSeriesDesc(QStringList s){ dwiSeriesDesc_=std::move(s);} //!< Setter for Diffusion Weighted Imaging series descriptions.
    void setMcT2SeriesDesc(QStringList s){ mcT2SeriesDesc_=std::move(s);} //!< Setter for motion corrected T2 series descriptions.
    void setMcDynSeriesDesc(QStringList s){ mcDynSeriesDesc_=std::move(s);} //!< Setter for motion corrected dynamic series descriptions.
    void setMcAdcSeriesDesc(QStringList s){ mcAdcSeriesDesc_=std::move(s);} //!< Setter for motion corrected ADC Series descriptions.
    void setMcDwiSeriesDesc(QStringList s){ mcDwiSeriesDesc_=std::move(s);} //!< Setter for motion corrected diffusion weighted series descriptions.
    void setFastDynamicSeriesDesc(const QStringList &s){ fastDynamicSeriesDesc_=s;} //!< Setter for fast dynamic series descriptions.
    void setPhilipsResearchNumPre(int n){ philipsResearchNumPre_ = n; } //!< Setter for Philips research number of precontrast time points.
    void setAutoLoadSeries(bool autoLoad = true){ autoLoadSeries_ = autoLoad; } //!< Setter for autoload flag.

    [[nodiscard]] QStringList t2SeriesDesc() const { return t2SeriesDesc_; } //!< Getter for T2 series descriptions.
    [[nodiscard]] QStringList dynSeriesDesc() const { return dynSeriesDesc_; } //!< Getter for dynamic series descriptions.
    [[nodiscard]] QStringList dynMultiSeriesDesc() const {return dynMultiSeriesDesc_; } //!< Getter for multi series descriptions.
    [[nodiscard]] QStringList adcSeriesDesc() const {return adcSeriesDesc_; } //!< Getter for ADC series descriptions.
    [[nodiscard]] QStringList mcT2SeriesDesc() const {return mcT2SeriesDesc_; } //!< Getter for motion corrected T2 series descriptions.
    [[nodiscard]] QStringList mcDynSeriesDesc() const {return mcDynSeriesDesc_; } //!< Getter for motion corrected dynamic series descriptions.
    [[nodiscard]] QStringList mcAdcSeriesDesc() const {return mcAdcSeriesDesc_; } //!< Getter for ADC series descriptions.
    [[nodiscard]] QStringList mcDwiSeriesDesc() const {return mcDwiSeriesDesc_; } //!< Getter for motion corrected diffusion weighted series descriptions.
    [[nodiscard]] QStringList fastDynamicSeriesDesc() const {return fastDynamicSeriesDesc_; } //!< Getter for fast dynamic series descriptions.
    [[nodiscard]] int philipsResearchNumPre() const {return philipsResearchNumPre_; } //!< Getter for Philips research number of precontrast time points.
    [[nodiscard]] bool autoLoadSeries() const {return autoLoadSeries_; } //!< Getter for autoload flag.

private:
    QStringList t2SeriesDesc_; //!< Stores T2 series descriptions.
    QStringList dynSeriesDesc_; //!< Stores dynamic series descriptions.
    QStringList dynMultiSeriesDesc_; //!< Stores dynamic multi series descriptions.
    QStringList adcSeriesDesc_; //!< Stores ADC series descriptions.
    QStringList dwiSeriesDesc_; //!< Stores diffusion weighted series descriptions.
    //perhaps the motion-corrected images should just be bumped in priority?
    QStringList mcT2SeriesDesc_; //!< Stores motion corrected T2 series descriptions.
    QStringList mcDynSeriesDesc_; //!< Stores motion corrected dynamic series descriptions.
    QStringList mcAdcSeriesDesc_; //!< Stores motion corrected ADC series descriptions.
    QStringList mcDwiSeriesDesc_; //!< Stores motion corrected diffusion weighted descriptions.
    QStringList fastDynamicSeriesDesc_; //!< Stores fast dynamic series descriptions.
    int philipsResearchNumPre_{}; //!< Stores Philips research number of precontrast time points.
    bool autoLoadSeries_{}; //!< Stores autoload flag.
};
}
#endif //QUANTX_SERIESDESCRIPTIONUTILS_H
