#include "serieswatcherdialog.h"
#include "qisql.h"
#include <QSqlDriver>
#include <QEventLoop>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QPushButton>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

/*!
 * \brief Constructor for the dialog.
 * \param seriesUID Series UID to watch.
 * \param jobInfo String identifying job(s).
 * \param dbSettings DB settings to connect with.
 * \param parent Parent QWidget used for cleanup, dialog hierarchy management, and signalling.
 */
SeriesWatcherDialog::SeriesWatcherDialog(QString seriesUID, QString jobInfo, QStringList dbSettings, QWidget *parent) :
    QDialog(parent), seriesUID_(seriesUID), dbSettings_(dbSettings)
{
	auto layout = new QVBoxLayout(this);
//	jobType_ = jobInfo.split(",").first();
	jobType_ = "motioncorrect"; // we only add to the jobs table for DCE series, so all will get motion corrected
	jobStatus_ = jobInfo.split(",").last();
	jobStatus_.replace(0, 1, jobStatus_[0].toTitleCase());
	QLabel *textLabel = new QLabel(this);
	statusLabel = new QLabel(QString("<b>Motion Correction Status: %1</b>").arg(jobStatus_), this);
//	if (jobType_ == "motioncorrect")
		textLabel->setText(QString("The DCE series %1 is in the process of motion correcting on the QuantX server.\n\n"
		                           "Loading will continue once it has finished or click 'load uncorrected DCE' to load the original image data or 'Cancel' to stop loading the series.").arg(seriesUID_));

	auto buttonBox = new QDialogButtonBox(QDialogButtonBox::Cancel, this);

	auto loadOriginalButton = buttonBox->addButton("Load Uncorrected DCE", QDialogButtonBox::DestructiveRole);
	layout->addWidget(textLabel);
	layout->addWidget(statusLabel);
	layout->addWidget(buttonBox);
	//connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
	connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
	connect(loadOriginalButton, &QPushButton::clicked, [=]{this->done(static_cast<int>(WatcherStatus::LOADORIGINAL));});
	this->setLayout(layout);
	this->setWindowTitle("Please Wait");
	this->setAttribute(Qt::WA_DeleteOnClose);
	subscribeToDBNotification(jobType_);

}

/*!
 * \brief Subscribes the dialog to DB notifications for a job type.
 * \param jobType Type of job to subscribe to.
 */
void SeriesWatcherDialog::subscribeToDBNotification(QString jobType)
{
	    QScopedPointer<QISql> qisql(new QISql(dbSettings_));
		db = qisql->createDatabase("QIWaitForServer");

		db.open();
		if (db.driver()->hasFeature(QSqlDriver::EventNotifications)){
#if useDebug
			qDebug() << "(dialog)Good, Database Driver supports notifications";
#endif
			db.driver()->subscribeToNotification(jobType + "_status");
#if useDebug
			qDebug() << "(dialog)Going to wait for notifications from DB...";
#endif
			QObject::connect(db.driver(), QOverload<const QString &, QSqlDriver::NotificationSource, const QVariant &>::of(&QSqlDriver::notification),
			                 [=](const QString &name, QSqlDriver::NotificationSource source, const QVariant &payload){
				Q_UNUSED(source)
				auto payloadStrings = payload.toString().split(",");
#if useDebug
				qDebug() << "(dialog)Received: " << payload;
				qDebug() << "(dialog)seriesUID: " << seriesUID_;
#endif
				if (name == (jobType + "_status") && payloadStrings.first() == seriesUID_) {
					if (payloadStrings.last() == "finished"){
#if useDebug
					qDebug() << "(dialog)Job finished for " << seriesUID_;
#endif
					this->accept();
					} else {
						QString status = payloadStrings.last();
						status.replace(0, 1, status[0].toTitleCase());
						statusLabel->setText(QString("<b>Motion Correction Status: %1</b>").arg(status));
					}
				}});
		} else {
#if useDebug
			qDebug() << "Database Driver Does NOT Support notifications";
#endif
		}
}
