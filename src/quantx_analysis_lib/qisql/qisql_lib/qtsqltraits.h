#ifndef QTSQLTRAITS_H
#define QTSQLTRAITS_H

#include <Wt/Dbo/Dbo.h>

#include <Wt/WDate.h>
#include <Wt/WTime.h>

// These functions fix issues that arise when we use std::optional

namespace Wt {
    namespace Dbo {

	    template<>
	    struct sql_value_traits<Wt::WTime, void>
		{
			static const bool specialized = true;

			static std::string type( SqlConnection* conn, int )
			{
				return conn->dateTimeType( SqlDateTimeType::Time );
			}

			static void bind( const Wt::WTime& time, SqlStatement* statement, int column, int )
			{
				if( time.isNull() ) {
					statement->bindNull( column );
				} else {
					statement->bind( column, time.toTimeDuration() );
				}
			}

			static bool read( Wt::WTime& time, SqlStatement* statement, int column, int )
			{
				std::chrono::duration<int, std::milli> t;
				auto bSuccess = statement->getResult( column, &t );
				if( bSuccess ) {
					time = Wt::WTime::fromTimeDuration(t);
				} else {
					time = Wt::WTime{};
				}

				return bSuccess;
			}
		};

		template<>
		struct sql_value_traits<Wt::WDate, void>
		{
			static const bool specialized = true;

			static std::string type( SqlConnection* conn, int )
			{
				return conn->dateTimeType( SqlDateTimeType::Date );
			}

			static void bind( const Wt::WDate& date, SqlStatement* statement, int column, int )
			{
				if( date.isNull() ) {
					statement->bindNull( column );
				} else {
					statement->bind( column, date.toTimePoint(), SqlDateTimeType::Date );
				}
			}

			static bool read( Wt::WDate& date, SqlStatement* statement, int column, int )
			{
				std::chrono::system_clock::time_point s;
				auto bSuccess = statement->getResult( column, &s, SqlDateTimeType::Date );
				if( bSuccess ) {
					date = Wt::WDate(s);
				} else {
					date = Wt::WDate{};
				}

				return bSuccess;
			}
		};

	}// Dbo
}// Wt

#endif // QTSQLTRAITS_H
