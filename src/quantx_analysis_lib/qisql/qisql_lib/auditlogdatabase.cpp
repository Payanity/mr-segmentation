#include "auditlogdatabase.h"

#include "model/auditlog.h"
#include "databasehelper.h"
#include <QDateTime>
#include <Wt/Dbo/backend/Postgres.h>
#include <Wt/Dbo/backend/Sqlite3.h>

using namespace quantx::auditlog;

/*!
 * D-pointer internal implementation for the QuantXAuditLogDatabase.
 */
class QuantXAuditLogDatabasePrivate
{
public:
	Wt::Dbo::Session session;
	std::unique_ptr<Wt::Dbo::SqlConnection> connection;
	DatabaseHelper::ConnectionType connectionType;
	QStringList dbSettings;

	/*!
	 * \brief Initialize an audit log db session.
	 */
	void InitializeSession()
	{
		session.setConnection(connection->clone());
		session.mapClass<AuditLog>("qi_auditlog");
		try {
			session.createTables();
			std::cerr << "Created database" << std::endl;
		} catch (std::exception& e) {
			std::cerr << e.what() << std::endl;
			std::cerr << "Using existing database" << std::endl;
		}
	}

};

/*!
 * \brief Construct a default-constructed instance of QuantXAuditLogDatabase.
 */
QuantXAuditLogDatabase::QuantXAuditLogDatabase()
    :d(std::make_unique<QuantXAuditLogDatabasePrivate>())
{
	d->connection = std::make_unique<Wt::Dbo::backend::Postgres>("host=localhost port=5432 dbname=quantinsights user=postgres password=postgres");
	d->connectionType = DatabaseHelper::ConnectionType::Postgres;
	d->InitializeSession();
}

/*!
 * \brief Construct an instance from settings and a settable success flag.
 * \param databaseSettings QStringList of setttings for the db.
 * \param bSuccess The bool flag whose address is dereferenced and set to succes/fail value.
 */
QuantXAuditLogDatabase::QuantXAuditLogDatabase(const QStringList& databaseSettings, bool* bSuccess)
    :d(std::make_unique<QuantXAuditLogDatabasePrivate>())
{
	bool success = true;
	if(databaseSettings.first() == "QPSQL")
	{
		auto host = databaseSettings.at(1).toStdString();
		std::string port = "5432";
		if (databaseSettings.at(1).contains(":"))
		{
			host = databaseSettings.at(1).split(":").first().toStdString();
			port = databaseSettings.at(1).split(":").at(1).toStdString();
		}
		auto params = "host="     + host +
		             " port="     + port +
		             " dbname="   + databaseSettings.at(2).toStdString() +
		             " user="     + databaseSettings.at(3).toStdString() +
		             " password=" + databaseSettings.at(4).toStdString() ;
		try {
			d->connection = std::make_unique<Wt::Dbo::backend::Postgres>(params);
			d->connectionType = DatabaseHelper::ConnectionType::Postgres;
		} catch (std::exception e) {
			std::cerr << e.what() << std::endl;
			success = false;
		}
	}
	else // SQLite will be default
	{
		// create Default db for now
		QString databaseFilePath;
		if(!databaseSettings.at(2).isEmpty())
		{
			databaseFilePath = databaseSettings.at(2);
		}
		else
		{
			databaseFilePath = DatabaseHelper::getTempSQLiteDatabasePath("quantx.db");
		}

		try {
			d->connection = std::make_unique<Wt::Dbo::backend::Sqlite3>(databaseFilePath.toStdString());
			d->connectionType = DatabaseHelper::ConnectionType::SQLite;
		} catch (std::exception e) {
			std::cerr << e.what() << std::endl;
			success = false;
		}
	}
	if(success)
	{
		d->InitializeSession();
		d->dbSettings = databaseSettings;
	}

	if(bSuccess)
	{
		*bSuccess = success;
	}
}

/*!
 * \brief Construct a new instance of the audit log db using the connection from an existing sequence.
 * \param other The QuantXAuditLogDatabase instance used for its connection.
 */
QuantXAuditLogDatabase::QuantXAuditLogDatabase(const QuantXAuditLogDatabase& other)
    : d(std::make_unique<QuantXAuditLogDatabasePrivate>())
{
	d->connection = other.connection();
	d->InitializeSession();
}

/*!
 * \brief Destructor for the audit log db object.
 */
QuantXAuditLogDatabase::~QuantXAuditLogDatabase() = default; // Need to default here instead of .h to avoid inlining the destructor


/*!
 * \brief Getter for the db connection.
 * \return The db connection.
 */
std::unique_ptr<Wt::Dbo::SqlConnection> QuantXAuditLogDatabase::connection() const
{
	return d->connection->clone();
}

/*!
 * \brief Record an action in the audit log.
 * \param ActionDescription String describing the action to be written to the audit log.
 * \param MRN Medical Record Number associated with the action to be written.
 */
void QuantXAuditLogDatabase::RecordActionInLog(const QString& ActionDescription, const QString& MRN)
{
	auto AudLog = std::make_unique<AuditLog>();

	// Grab Date And Time
	AudLog->DateAndTime = QDateTime::currentDateTime().toString("yyyyMMddHHmmss").toStdString();

	// Grab Username
	AudLog->UserName = DatabaseHelper::getLoggedInUserName().toStdString();

	AudLog->ActionDesc = ActionDescription.toStdString();
	AudLog->MRN = MRN.toStdString();

	try{
		Wt::Dbo::Transaction transaction(d->session);
		d->session.add(std::move(AudLog));
		transaction.commit();
	} catch (std::exception e) {
		std::cerr << e.what() << std::endl;
	}
}

/*!
 * \brief Getter for the audit log db session.
 * \return Session object for the current session.
 */
Wt::Dbo::Session& QuantXAuditLogDatabase::session() const
{
	return d->session;
}

/*!
 * \brief Getter for all log entries.
 * \return Vector of the audit log entries.
 */
QVector<AuditLog> QuantXAuditLogDatabase::getAllItems() const
{
	QVector<AuditLog> logs;
	Wt::Dbo::Transaction userAccessTransaction(d->session);
	auto items = userAccessTransaction.session().find<AuditLog>().resultList();
	for(auto item : items)
	{
		AuditLog log;

		log.DateAndTime = item->DateAndTime;
		log.UserName = item->UserName;
		log.ActionDesc = item->ActionDesc;
		log.MRN = item->MRN;

		logs.append(log);
	}

	return logs;
}
