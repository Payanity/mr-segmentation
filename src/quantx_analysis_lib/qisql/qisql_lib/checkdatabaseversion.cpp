#include "sqlsearchwindow.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlDriver>
#include <QSqlField>
#include <QtConcurrent>
#include <QDialog>
#include <QLabel>
#include <QVBoxLayout>
#include <QPair>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

/*!
 * Check db verison.
 * \param db Db to check.
 */
void SqlSearchWindow::checkDatabaseVersion(QSqlDatabase &db)
{
//    QSqlRecord versionRecord = db.record("qi_database_version");
//    QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
//    QFuture<bool> sqlFuture;
//    QStringList sqlStatements;
//    if(versionRecord.isEmpty()){
//        sqlStatements.clear();
//        if(databaseSettings.first().startsWith("QPSQL")){
//            QString sequence = "qi_database_version_idqi_version_seq";
//            sqlStatements << QString("CREATE SEQUENCE %1 INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1").arg(sequence);
//            sqlStatements << QString("CREATE TABLE qi_database_version ( idqi_version INTEGER PRIMARY KEY DEFAULT nextval('%1'::regclass), version int NOT NULL )").arg(sequence);
//        }
//        else
//            sqlStatements << "CREATE TABLE qi_database_version ( idqi_version INTEGER PRIMARY KEY, version int NOT NULL )";

//        for(const auto & sqlStatement : std::as_const(sqlStatements)){
//            sqlFuture = QtConcurrent::run(sqlQuery.data(), &QSqlQuery::exec, sqlStatement);
//            sqlFuture.waitForFinished();
//        }
//        versionRecord = db.record("qi_database_version");
//    }
//    sqlQuery->exec("SELECT version FROM qi_database_version ORDER BY version DESC");

//    int version = 0;
//    sqlQuery->first();
//    version = sqlQuery->value("version").toInt();
//#if useDebug
//	qDebug() << db.driverName() << db.hostName() << db.port();
//    qDebug() << "Database Version Is:" << QString::number(static_cast<qreal>(version) / 100 , 'f', 2);
//#endif
//    if( version < 1 ){
//#if useDebug
//        qDebug() << "Updating From Version" << static_cast<float>(version) / 100 << "to 1.00";
//#endif
//		QScopedPointer<QDialog, QScopedPointerDeleteLater> dialog(new QDialog(window(), Qt::FramelessWindowHint));
//        dialog->setModal(false);
//        dialog->setFixedSize(420,60);
//        auto* vbox = new QVBoxLayout();
//        QLabel *label = new QLabel("Database is out of date, updating...", dialog.data());
//        label->setAlignment(Qt::AlignCenter);
//        vbox->addWidget(label);
//        dialog->setLayout(vbox);
//        dialog->show();
//        qApp->processEvents();

//        versionRecord.setValue("version", 100);
//        for(int i = versionRecord.count() - 1; i >= 0; i--)
//            if(versionRecord.isNull(i))
//                versionRecord.remove(i);

//        sqlStatements.clear();
//        sqlStatements << R"(ALTER TABLE "qi_images_mr" ADD COLUMN "triggerTime" int DEFAULT NULL)";
//        sqlStatements << R"(ALTER TABLE "dicom_data" ADD COLUMN "0018,1060" varchar DEFAULT NULL)";
//        sqlStatements << db.driver()->sqlStatement(QSqlDriver::InsertStatement, "qi_database_version", versionRecord, false);

//        for(const auto & sqlStatement : std::as_const(sqlStatements)){
//            sqlFuture = QtConcurrent::run(sqlQuery.data(), &QSqlQuery::exec, sqlStatement);
//            sqlFuture.waitForFinished();
//        }

//    }

//    if( version < 101 ){
//#if useDebug
//        qDebug() << "Updating From Version" << static_cast<float>(version) / 100 << "to 1.01";
//#endif
//		QScopedPointer<QDialog, QScopedPointerDeleteLater> dialog(new QDialog(window(), Qt::FramelessWindowHint));
//        dialog->setModal(false);
//        dialog->setFixedSize(420,60);
//        auto* vbox = new QVBoxLayout();
//        QLabel *label = new QLabel("Database is out of date, updating...", dialog.data());
//        label->setAlignment(Qt::AlignCenter);
//        vbox->addWidget(label);
//        dialog->setLayout(vbox);
//        dialog->show();
//        qApp->processEvents();

//        versionRecord.setValue("version", 101);
//        for(int i = versionRecord.count() - 1; i >= 0; i--)
//            if(versionRecord.isNull(i))
//                versionRecord.remove(i);

//        sqlStatements.clear();
//        sqlStatements << R"(ALTER TABLE "qi_images_mr" ADD COLUMN "magneticFieldStrength" real DEFAULT NULL)";
//        sqlStatements << db.driver()->sqlStatement(QSqlDriver::InsertStatement, "qi_database_version", versionRecord, false);

//        for(const auto & sqlStatement : std::as_const(sqlStatements)){
//            sqlFuture = QtConcurrent::run(sqlQuery.data(), &QSqlQuery::exec, sqlStatement);
//            sqlFuture.waitForFinished();
//        }

//        /* Updating this column takes too long, this is done in the QiMriImage class instead
//        QSqlRecord uidRecord = db.record("qi_images_mr");
//        uidRecord.setValue("image_uid","UID");
//        for(int i=uidRecord.count()-1;i>=0;i--)
//            if(uidRecord.isNull(i))
//                uidRecord.remove(i);
//        QSqlRecord mfsRecord = db.record("qi_images_mr");
//        mfsRecord.setValue("magneticFieldStrength",0.0);
//        for(int i=mfsRecord.count()-1;i>=0;i--)
//            if(mfsRecord.isNull(i))
//                mfsRecord.remove(i);
//        QSqlRecord dicomRecord = db.record("dicom_data");
//        dicomRecord.setValue("0008,0018"," ");
//        dicomRecord.setValue("0018,0087"," ");
//        for(int i=dicomRecord.count()-1;i>=0;i--)
//            if(dicomRecord.isNull(i))
//                dicomRecord.remove(i);
//        QString sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,"dicom_data",dicomRecord,false);
//        sqlQuery->exec(sqlStatement);

//        QScopedPointer<QSqlQuery> sqlUpdateQuery(new QSqlQuery(db));
//        while(sqlQuery->next()){
//            uidRecord.setValue("imageUID",sqlQuery->value("0008,0018").toString());
//            mfsRecord.setValue("magneticFieldStrength",sqlQuery->value("0018,0087").toString().toFloat());
//            sqlStatement = db.driver()->sqlStatement(QSqlDriver::UpdateStatement,"qi_images_mr",mfsRecord,false) + QString(' ')
//                         + db.driver()->sqlStatement(QSqlDriver::WhereStatement, "qi_images_mr",uidRecord,false);
//            sqlFuture = QtConcurrent::run(sqlUpdateQuery.data(),&QSqlQuery::exec,sqlStatement);
//            sqlFuture.waitForFinished();
//        }
//        */
//    }

//    if( version < 110 ){
//#if useDebug
//        qDebug() << "Updating From Version" << static_cast<float>(version) / 100 << "to 1.10";
//#endif
//		QScopedPointer<QDialog, QScopedPointerDeleteLater> dialog(new QDialog(window(), Qt::FramelessWindowHint));
//        dialog->setModal(false);
//        dialog->setFixedSize(420,60);
//        auto* vbox = new QVBoxLayout();
//        QLabel *label = new QLabel("Database is out of date, updating...", dialog.data());
//        label->setAlignment(Qt::AlignCenter);
//        vbox->addWidget(label);
//        dialog->setLayout(vbox);
//        dialog->show();
//        qApp->processEvents();

//        versionRecord.setValue("version", 110);
//        for(int i = versionRecord.count() - 1; i >= 0; i--)
//            if(versionRecord.isNull(i))
//                versionRecord.remove(i);

//        sqlStatements.clear();

//        QString columns =  R"( "seriesUID_moving" varchar DEFAULT NULL, "seriesUID_fixed" varchar DEFAULT NULL, "t_fixed" INT DEFAULT NULL, "Xform" %1, "imageHeader" %1, "default_value" REAL DEFAULT NULL )";

//        if(databaseSettings.first().startsWith("QPSQL")){
//            QString sequence = "qi_motion_correction_idqi_motion_correction";
//            sqlStatements << QString("CREATE SEQUENCE %1 INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1").arg(sequence);
//            sqlStatements << QString(R"(CREATE TABLE "qi_motion_correction" ( "idqi_motion_correction" INTEGER PRIMARY KEY DEFAULT nextval('%1'::regclass),%2 ))").arg(sequence, columns.arg("BYTEA"));
//        }
//        else
//            sqlStatements << QString(R"(CREATE TABLE "qi_motion_correction" ( "idqi_motion_correction" INTEGER PRIMARY KEY,%1 ))").arg(columns.arg("BLOB"));

//        sqlStatements << db.driver()->sqlStatement(QSqlDriver::InsertStatement, "qi_database_version", versionRecord, false);

//        for(const auto & sqlStatement : std::as_const(sqlStatements)){
//            sqlFuture = QtConcurrent::run(sqlQuery.data(), &QSqlQuery::exec, sqlStatement);
//            sqlFuture.waitForFinished();
//        }
//    }

//    if( version < 200 ){
//#if useDebug
//        qDebug() << "Updating From Version" << static_cast<float>(version) / 100 << "to 2.00";
//#endif
//		QScopedPointer<QDialog, QScopedPointerDeleteLater> dialog(new QDialog(window(), Qt::FramelessWindowHint));
//        dialog->setModal(false);
//        dialog->setFixedSize(420,60);
//        auto* vbox = new QVBoxLayout();
//        QLabel *label = new QLabel("Database is out of date, updating...", dialog.data());
//        label->setAlignment(Qt::AlignCenter);
//        vbox->addWidget(label);
//        dialog->setLayout(vbox);
//        dialog->show();
//        qApp->processEvents();

//        versionRecord.setValue("version", 200);
//        for(int i = versionRecord.count() - 1; i >= 0; i--)
//            if(versionRecord.isNull(i))
//                versionRecord.remove(i);

//        sqlStatements.clear();

//        QList<QPair<QString,QString> > qi_images_cols;
//        qi_images_cols << QPair<QString,QString>("\"MRN\"",                       "mrn");
//        qi_images_cols << QPair<QString,QString>("\"imageUID\"",                  "image_uid");
//        qi_images_cols << QPair<QString,QString>("\"seriesUID\"",                 "series_uid");
//        qi_images_cols << QPair<QString,QString>("\"studyUID\"",                  "study_uid");
//        qi_images_cols << QPair<QString,QString>("\"date\"",                      "image_date");
//        qi_images_cols << QPair<QString,QString>("\"time\"",                      "image_time");
//        qi_images_cols << QPair<QString,QString>("\"imageType\"",                 "image_type");
//        qi_images_cols << QPair<QString,QString>("\"windowWidth\"",               "window_width");
//        qi_images_cols << QPair<QString,QString>("\"windowCenter\"",              "window_center");
//        qi_images_cols << QPair<QString,QString>("\"minPixval\"",                 "min_pixval");
//        qi_images_cols << QPair<QString,QString>("\"maxPixval\"",                 "max_pixval");
//        qi_images_cols << QPair<QString,QString>("\"imagePosition\"",             "image_position");
//        qi_images_cols << QPair<QString,QString>("\"sliceLocation\"",             "slice_location");
//        qi_images_cols << QPair<QString,QString>("\"temporalPosID\"",             "temporal_pos_id");
//        qi_images_cols << QPair<QString,QString>("\"instanceNum\"",               "instance_num");
//        qi_images_cols << QPair<QString,QString>("\"scanOptions\"",               "scan_options");
//        qi_images_cols << QPair<QString,QString>("\"viewPosition\"",              "view_position");
//        qi_images_cols << QPair<QString,QString>("\"imageLaterality\"",           "image_laterality");
//        qi_images_cols << QPair<QString,QString>("\"patientOrientation\"",        "patient_orientation");
//        qi_images_cols << QPair<QString,QString>("\"photometricInterpretation\"", "photometric_interpretation");
//        qi_images_cols << QPair<QString,QString>("\"modelName\"",                 "model_name");

//        sqlStatements << QString("ALTER TABLE qi_images DROP COLUMN \"diffusionBValue\"");
//        sqlStatements << QString("ALTER TABLE qi_images DROP COLUMN \"diffusionGradientOrientation\"");

//        QList<QPair<QString,QString> > qi_images_mg_cols = qi_images_cols;
//        qi_images_mg_cols << QPair<QString,QString>("\"numberOfFrames\"", "number_of_frames");

//        sqlStatements << QString("ALTER TABLE qi_images_mg DROP COLUMN \"diffusionBValue\"");
//        sqlStatements << QString("ALTER TABLE qi_images_mg DROP COLUMN \"diffusionGradientOrientation\"");

//        QList<QPair<QString,QString> > qi_images_us_cols = qi_images_mg_cols;

//        sqlStatements << QString("ALTER TABLE qi_images_us DROP COLUMN \"diffusionBValue\"");
//        sqlStatements << QString("ALTER TABLE qi_images_us DROP COLUMN \"diffusionGradientOrientation\"");

//        QList<QPair<QString,QString> > qi_images_mr_cols = qi_images_cols;
//        qi_images_mr_cols << QPair<QString,QString>("\"triggerTime\"",                  "trigger_time");
//        qi_images_mr_cols << QPair<QString,QString>("\"magneticFieldStrength\"",        "magnetic_field_strength");
//        qi_images_mr_cols << QPair<QString,QString>("\"diffusionBValue\"",              "diffusion_b_value");
//        qi_images_mr_cols << QPair<QString,QString>("\"diffusionGradientOrientation\"", "diffusion_gradient_orientation");

//        QList<QPair<QString,QString> > qi_motion_correction_cols;
//        qi_motion_correction_cols << QPair<QString,QString>("\"seriesUID_moving\"", "series_uid_moving");
//        qi_motion_correction_cols << QPair<QString,QString>("\"seriesUID_fixed\"" , "series_uid_fixed");
//        qi_motion_correction_cols << QPair<QString,QString>("\"Xform\"",            "xform");
//        qi_motion_correction_cols << QPair<QString,QString>("\"imageHeader\"",      "image_header");

//        QList<QPair<QString,QString> > qi_patients_cols;
//        qi_patients_cols << QPair<QString,QString>("\"MRN\"",  "mrn");
//        qi_patients_cols << QPair<QString,QString>("\"DOB\"",  "dob");
//        qi_patients_cols << QPair<QString,QString>("\"name\"", "patient_name");

//        QList<QPair<QString,QString> > qi_series_cols;
//        qi_series_cols << QPair<QString,QString>("\"MRN\"",                  "mrn");
//        qi_series_cols << QPair<QString,QString>("\"seriesUID\"",            "series_uid");
//        qi_series_cols << QPair<QString,QString>("\"studyUID\"",             "study_uid");
//        qi_series_cols << QPair<QString,QString>("\"seriesNumber\"",         "series_number");
//        qi_series_cols << QPair<QString,QString>("\"seriesDesc\"",           "series_desc");
//        qi_series_cols << QPair<QString,QString>("\"date\"",                 "series_date");
//        qi_series_cols << QPair<QString,QString>("\"time\"",                 "series_time");
//        qi_series_cols << QPair<QString,QString>("\"numTemporalPositions\"", "num_temporal_positions");
//        qi_series_cols << QPair<QString,QString>("\"imageOrientation\"",     "image_orientation");
//        qi_series_cols << QPair<QString,QString>("\"spaceBetweenSlices\"",   "space_between_slices");

//        QList<QPair<QString,QString> > qi_series_thumbnails_cols;
//        qi_series_thumbnails_cols << QPair<QString,QString>("\"seriesUID\"", "series_uid");
//        qi_series_thumbnails_cols << QPair<QString,QString>("\"maxDim\"",    "max_dim");
//        qi_series_thumbnails_cols << QPair<QString,QString>("\"minDim\"",    "min_dim");

//        QList<QPair<QString,QString> > qi_studies_cols;
//        qi_studies_cols << QPair<QString,QString>("\"MRN\"",      "mrn");
//        qi_studies_cols << QPair<QString,QString>("\"studyUID\"", "study_uid");
//        qi_studies_cols << QPair<QString,QString>("\"date\"",     "study_date");
//        qi_studies_cols << QPair<QString,QString>("\"time\"",     "study_time");

//        QList<QPair<QString,QString> > dicom_data_cols;
//        dicom_data_cols << QPair<QString,QString>("\"fullHeader\"", "full_header");

//        QMap<QString, QList<QPair<QString,QString> > > tablemap;
//        tablemap.insert("qi_images", qi_images_cols);
//        tablemap.insert("qi_images_mg", qi_images_mg_cols);
//        tablemap.insert("qi_images_us", qi_images_us_cols);
//        tablemap.insert("qi_images_mr", qi_images_mr_cols);
//        tablemap.insert("qi_motion_correction", qi_motion_correction_cols);
//        tablemap.insert("qi_patients", qi_patients_cols);
//        tablemap.insert("qi_series", qi_series_cols);
//        tablemap.insert("qi_series_thumbnails", qi_series_thumbnails_cols);
//        tablemap.insert("qi_studies", qi_studies_cols);
//        tablemap.insert("dicom_data", dicom_data_cols);

//        for(auto i = tablemap.constBegin(); i != tablemap.constEnd(); ++i)
//            for(const auto & col : i.value())
//                sqlStatements << QString("ALTER TABLE %1 RENAME COLUMN %2 TO %3").arg(i.key(), col.first, col.second);

//        sqlStatements << db.driver()->sqlStatement(QSqlDriver::InsertStatement, "qi_database_version", versionRecord, false);

//#if useDebug
//        qDebug() << "-------------------------------------------------------------------------------------------------------";
//        qDebug() << "Sql Statments:";
//        qDebug() << sqlStatements;
//#endif

//        for(const auto & sqlStatement : std::as_const(sqlStatements)){
//            sqlFuture = QtConcurrent::run(sqlQuery.data(), &QSqlQuery::exec, sqlStatement);
//            sqlFuture.waitForFinished();
//        }

//    }
//    if( version < 201 ){
//#if useDebug
//        qDebug() << "Updating From Version" << static_cast<float>(version) / 100 << "to 2.01";
//#endif
//		QScopedPointer<QDialog, QScopedPointerDeleteLater> dialog(new QDialog(window(), Qt::FramelessWindowHint));
//        dialog->setModal(false);
//        dialog->setFixedSize(420,60);
//        auto* vbox = new QVBoxLayout();
//        QLabel *label = new QLabel("Database is out of date, updating...", dialog.data());
//        label->setAlignment(Qt::AlignCenter);
//        vbox->addWidget(label);
//        dialog->setLayout(vbox);
//        dialog->show();
//        qApp->processEvents();

//        versionRecord.setValue("version", 201);
//        for(int i = versionRecord.count() - 1; i >= 0; i--)
//            if(versionRecord.isNull(i))
//                versionRecord.remove(i);

//        sqlStatements.clear();
//        sqlStatements << R"(ALTER TABLE "qi_studies" ADD COLUMN "accession_number" text DEFAULT NULL)";
//        sqlStatements << R"(ALTER TABLE "dicom_data" ADD COLUMN "0008,0050" text DEFAULT NULL)";

//        QSqlRecord studyUIDRecord;
//        studyUIDRecord.append(QSqlField("study_uid", QVariant::String));
//        //auto sqlQuery = std::make_unique<QSqlQuery>(db);

//        auto sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,"qi_studies",studyUIDRecord,false);
//        auto sqlFuture = QtConcurrent::run(sqlQuery.get(), &QSqlQuery::exec, sqlStatement);
//        sqlFuture.waitForFinished();
//        while(sqlQuery->next()){
//            QSqlRecord dicomStudyUIDRecord;
//            dicomStudyUIDRecord.append(QSqlField("0020,000d", QVariant::String));
//            dicomStudyUIDRecord.setValue("0020,000d", sqlQuery->value("study_uid"));

//            QSqlRecord dicomImageUIDRecord;
//            dicomImageUIDRecord.append(QSqlField("0008,0018", QVariant::String));

//            auto findHeaderStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,"dicom_data",dicomImageUIDRecord,false) + QString(' ')
//                                     + db.driver()->sqlStatement(QSqlDriver::WhereStatement, "dicom_data",dicomStudyUIDRecord,false) + " LIMIT 1";

//            auto sqlAccessionQuery = std::make_unique<QSqlQuery>(db);
//            auto sqlFindAccessionFuture = QtConcurrent::run(sqlAccessionQuery.get(), &QSqlQuery::exec, findHeaderStatement);
//            sqlFindAccessionFuture.waitForFinished();
//            sqlAccessionQuery->first();

//            QScopedPointer<QISql> qisql(new QISql(databaseSettings));
//            auto dicomFields = qisql->extractAllDicomFields(sqlAccessionQuery->value("0008,0018").toString());
//            QSqlDatabase::removeDatabase("QIsqlExtractDicomDatabase");

//#if 0
//            qDebug() << "-------------------------------------------------------------------------------------------------------";
//            qDebug() << dicomFields;
//#endif

//            QSqlRecord studyAccessionRecord;
//            studyAccessionRecord.append(QSqlField("accession_number", QVariant::String));
//            studyAccessionRecord.setValue("accession_number", dicomFields.value("0008,0050"));
//            sqlStatements << db.driver()->sqlStatement(QSqlDriver::UpdateStatement,"qi_studies",studyAccessionRecord,false) + QString(' ')
//                           + db.driver()->sqlStatement(QSqlDriver::WhereStatement, "qi_studies",sqlQuery->record(),false);
//        }

//        sqlStatements << db.driver()->sqlStatement(QSqlDriver::InsertStatement, "qi_database_version", versionRecord, false);

//#if useDebug
//        qDebug() << "-------------------------------------------------------------------------------------------------------";
//        qDebug() << "Sql Statments:";
//        qDebug() << sqlStatements;
//#endif
//        for(const auto & sqlStatement : std::as_const(sqlStatements)){
//            sqlFuture = QtConcurrent::run(sqlQuery.get(), &QSqlQuery::exec, sqlStatement);
//            sqlFuture.waitForFinished();
//        }
//    }
//	if( version < 202 ){
//#if useDebug
//		qDebug() << "Updating From Version" << static_cast<float>(version) / 100 << "to 2.02";
//#endif
//		QScopedPointer<QDialog, QScopedPointerDeleteLater> dialog(new QDialog(window(), Qt::FramelessWindowHint));
//		dialog->setModal(false);
//		dialog->setFixedSize(420,60);
//		auto* vbox = new QVBoxLayout();
//		QLabel *label = new QLabel("Database is out of date, updating...", dialog.data());
//		label->setAlignment(Qt::AlignCenter);
//		vbox->addWidget(label);
//		dialog->setLayout(vbox);
//		dialog->show();
//		qApp->processEvents();

//		versionRecord.setValue("version", 202);
//		for(int i = versionRecord.count() - 1; i >= 0; i--)
//			if(versionRecord.isNull(i))
//				versionRecord.remove(i);

//		sqlStatements.clear();
//		sqlStatements << R"(ALTER TABLE "dicom_data" ADD COLUMN "0008,0090" text DEFAULT NULL)";
//		sqlStatements << R"(UPDATE "dicom_data" SET "0008,0090" = "0032,1032")"; // Set referring physician to requesting physician


//		sqlStatements << db.driver()->sqlStatement(QSqlDriver::InsertStatement, "qi_database_version", versionRecord, false);

//#if useDebug
//		qDebug() << "-------------------------------------------------------------------------------------------------------";
//		qDebug() << "Sql Statments:";
//		qDebug() << sqlStatements;
//#endif
//		for(const auto & sqlStatement : std::as_const(sqlStatements)){
//			sqlFuture = QtConcurrent::run(sqlQuery.get(), &QSqlQuery::exec, sqlStatement);
//			sqlFuture.waitForFinished();
//		}
//	}
}
