#ifndef AUDITLOGDATABASE_H
#define AUDITLOGDATABASE_H

#include "qisqlDllCheck.h"
#include <QStringList>
#include <Wt/Dbo/Dbo.h>
#include <memory>

class QuantXAuditLogDatabasePrivate;
namespace quantx {
namespace auditlog {
class AuditLog;
}
}

/*!
 * \class QuantXAuditLogDatabase
 * \ingroup SQLModule
 *
 * \brief The QuantX Audit Log Database
 *
 * This class contains the functions needed for the data and UI modules
 * to create entries in the audit log database.
 *
 */

class QISQL_EXPORT QuantXAuditLogDatabase
{
public:
	QuantXAuditLogDatabase();
	QuantXAuditLogDatabase(const QStringList& databaseSettings, bool* bSuccess);
	QuantXAuditLogDatabase(const QuantXAuditLogDatabase& other);
	~QuantXAuditLogDatabase();

	std::unique_ptr<Wt::Dbo::SqlConnection> connection() const;
	Wt::Dbo::Session& session() const;

	void RecordActionInLog(const QString& ActionDescription, const QString& MRN = "");

	QVector<quantx::auditlog::AuditLog> getAllItems() const;

private:
	std::unique_ptr<QuantXAuditLogDatabasePrivate> d; //!< The internally held private d-pointer instance that holds the private impl for the audit log db object.
};

#endif // AUDITLOGDATABASE_H
