#include "treeitem.h"

/*
    treeitem.cpp

    A container for items of data supplied by the simple tree model.
*/

/*!
 * \brief Constructor that takes a parent tree item.
 * \param parent Parent item.
 */
TreeItem::TreeItem( TreeItem* parent )
    :m_parentItem( parent )
{}

/*!
 * \brief Constructor that takes a vector of QVariant, and a parent item.
 * \param data Data parameter to store in the item.
 * \param parent Parent item (optional).
 */
TreeItem::TreeItem(const QVector<QVariant> &data, TreeItem *parent)
    : m_itemData(data), m_parentItem(parent)
{}

/*!
 * \brief Destructor.
 */
TreeItem::~TreeItem()
{
    qDeleteAll(m_childItems);
}

/*!
 * \brief Append a child item.
 * \param item Item to append.
 */
void TreeItem::appendChild(TreeItem *item)
{
    m_childItems.append(item);
}

/*!
 * \brief Delete all child items.
 */
void TreeItem::deleteChildren()
{
	qDeleteAll(m_childItems);
}

/*!
 * \brief Get child item by row.
 * \param row Row to get item for.
 * \return Pointer to found tree item.
 */
TreeItem *TreeItem::child(int row)
{
    if (row < 0 || row >= m_childItems.size())
        return nullptr;
    return m_childItems.at(row);
}

/*!
 * \brief Get the count of this item's children.
 * \return Number of child items.
 */
int TreeItem::childCount() const
{
    return m_childItems.count();
}

/*!
 * \brief Get this item's index in its parent's children (index among sibling items
 * \return Child number.
 */
int TreeItem::childNumber() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<TreeItem*>(this));
    return 0;
}

/*!
 * \brief Get number of columns on this item's data.
 * \return Column count.
 */
int TreeItem::columnCount() const
{
    return m_itemData.count();
}

/*!
 * \brief Get data for a given column.
 * \param column Column to get data for.
 * \return Data for the column requested.
 */
QVariant TreeItem::data(int column) const
{
    if (column < 0 || column >= m_itemData.size())
        return QVariant();
    return m_itemData.at(column);
}

/*!
 * \brief Insert children into the current item.
 * \param position Where to insert the children.
 * \param count How many child rows to insert.
 * \param columns Column count for each child item.
 * \return True if success, false otherwise.
 */
bool TreeItem::insertChildren(int position, int count, int columns)
{
    if (position < 0 || position > m_childItems.size())
        return false;

    for (int row = 0; row < count; ++row) {
        QVector<QVariant> data(columns);
        TreeItem *item = new TreeItem(data, this);
        m_childItems.insert(position, item);
    }

    return true;
}

/*!
 * \brief Set data for a given column.
 * \param column Column on which to set the data.
 * \param value Data to set.
 * \return True if successful, false otherwise.
 */
bool TreeItem::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= m_itemData.size())
        return false;

    m_itemData[column] = value;
    return true;
}

/*!
 * \brief Get pointer to parent item.
 * \return Pointer to parent item.
 */
TreeItem *TreeItem::parentItem()
{
    return m_parentItem;
}

/*!
 * \brief Get row of this tree item.
 * \return Index of this item amongst siblings. 0 if unparented.
 */
int TreeItem::row() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}
