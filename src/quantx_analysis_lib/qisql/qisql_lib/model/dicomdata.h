#ifndef DICOMDATA_H
#define DICOMDATA_H

#include <Wt/Dbo/Dbo.h>

namespace quantx {
namespace dicom {
class DicomData;
}
}

namespace Wt {
    namespace Dbo {
	// Additional custom traits for the DicomData class, used internally by Wt::dbo
	    template<>
	    struct dbo_traits<quantx::dicom::DicomData> : public dbo_default_traits
		{
			static const char* surrogateIdField()
			{
				return "iddicom_data";
			}
			static const char* versionField()
			{
				return 0;
			}
		};

	}
}

namespace quantx {
namespace dicom {

/*!
 * \brief The DicomData database table class
 * \ingroup SQLModule
 * This class is used to store DICOM image metadata
 * The database columns line up with common DICOM tags, and the original full header is also available
 */
class DicomData
{
public:
	std::string tag_0008_0008; //!< Image Type
	std::string tag_0008_0016; //!< SOP Class UID
	std::string tag_0008_0018; //!< SOP Instance UID
	std::string tag_0008_0020; //!< Study Date
	std::string tag_0008_0021; //!< Series Date
	std::string tag_0008_0023; //!< Content Date
	std::string tag_0008_0030; //!< Study Time
	std::string tag_0008_0031; //!< Series Time
	std::string tag_0008_0032; //!< Acquisition Time
	std::string tag_0008_0033; //!< Content Time
	std::string tag_0008_0060; //!< Modality
	std::string tag_0008_0070; //!< Manufacturer
	std::string tag_0008_1090; //!< Retrieve URL
	std::string tag_0010_0010; //!< Patient's Name
	std::string tag_0010_0020; //!< Patient ID
	std::string tag_0010_0030; //!< Patient's Birth Date
	std::string tag_0010_0040; //!< Patient's Sex
	std::string tag_0010_1010; //!< Patient's Age
	std::string tag_0010_21c0; //!< Pregnancy Status
	std::string tag_0018_0020; //!< Scanning Sequence
	std::string tag_0018_0021; //!< Sequence Variant
	std::string tag_0018_0022; //!< Scan Options
	std::string tag_0018_0023; //!< MR Acquisition Type
	std::string tag_0018_0024; //!< Sequence Name
	std::string tag_0018_0025; //!< Angio Flag
	std::string tag_0018_0050; //!< Slice Thickness
	std::string tag_0018_0080; //!< Repetition Time
	std::string tag_0018_0081; //!< Echo Time
	std::string tag_0018_0082; //!< Inversion Time
	std::string tag_0018_0083; //!< Number of Averages
	std::string tag_0018_0084; //!< Imaging Frequency
	std::string tag_0018_0085; //!< Imaged Nucleus
	std::string tag_0018_0086; //!< Echo Number(s)
	std::string tag_0018_0087; //!< Magnetic Field Strength
	std::string tag_0018_0088; //!< Spacing Between Slices
	std::string tag_0018_0089; //!< Number of Phase Encoding Steps
	std::string tag_0018_0090; //!< Data Collection Diameter
	std::string tag_0018_0091; //!< Echo Train Length
	std::string tag_0018_0093; //!< Percent Sampling
	std::string tag_0018_0094; //!< Percent Phase Field of View
	std::string tag_0018_0095; //!< Pixel Bandwidth
	std::string tag_0018_1020; //!< Software Version(s)
	std::string tag_0018_1040; //!< Contrast/Bolus Route
	std::string tag_0018_1041; //!< Contrast/Bolus Volume
	std::string tag_0018_1042; //!< Contrast/Bolus Start Time
	std::string tag_0018_1043; //!< Contrast/Bolus Stop Time
	std::string tag_0018_1044; //!< Contrast/Bolus Total Dose
	std::string tag_0018_1045; //!< Syringe Counts
	std::string tag_0018_1046; //!< Contrast Flow Rate
	std::string tag_0018_1047; //!< Contrast Flow Duration
	std::string tag_0018_1048; //!< Contrast/Bolus Ingredient
	std::string tag_0018_1049; //!< Contrast/Bolus Ingredient Concentration
	std::string tag_0018_1050; //!< Spatial Resolution
	std::string tag_0018_1060; //!< Trigger Time
	std::string tag_0018_1088; //!< Heart Rate
	std::string tag_0018_1090; //!< Cardiac Number of Images
	std::string tag_0018_1094; //!< Trigger Window
	std::string tag_0018_1100; //!< Reconstruction Diameter
	std::string tag_0018_1250; //!< Receive Coil Name
	std::string tag_0018_1310; //!< Acquisition Matrix
	std::string tag_0018_1312; //!< In-Plane Phase Encoding Direction
	std::string tag_0018_1314; //!< Flip Angle
	std::string tag_0018_1315; //!< Variable Flip Angle Flag
	std::string tag_0018_1316; //!< SAR
	std::string tag_0018_5100; //!< Patient Position
	std::string tag_0018_9075; //!< Diffusion Dirctionality
	std::string tag_0018_9087; //!< Diffusion B-Value
	std::string tag_0018_9089; //!< Diffusion Gradient Orientation
	std::string tag_0020_000d; //!< Study Instance UID
	std::string tag_0020_000e; //!< Series Instance UID
	std::string tag_0020_0010; //!< Study ID
	std::string tag_0020_0011; //!< Series Number
	std::string tag_0020_0012; //!< Acquisition Number
	std::string tag_0020_0013; //!< Instance Number
	std::string tag_0020_0032; //!< Image Position (Patient)
	std::string tag_0020_0037; //!< Image Orientation (Patient)
	std::string tag_0020_0052; //!< Frame of Reference UID
	std::string tag_0020_0060; //!< Laterality
	std::string tag_0020_0100; //!< Temporal Position Identifier
	std::string tag_0020_0105; //!< Number of Temporal Positions
	std::string tag_0020_0110; //!< Temporal Resolution
	std::string tag_0020_1040; //!< Position Reference Indicator
	std::string tag_0020_1041; //!< Slice Location
	std::string tag_0028_0002; //!< Samples per Pixel
	std::string tag_0028_0003; //!< Samples per Pixel Used
	std::string tag_0028_0004; //!< Potometric Interpretation
	std::string tag_0028_0010; //!< Rows
	std::string tag_0028_0011; //!< Columns
	std::string tag_0028_0030; //!< Pixel Spacing
	std::string tag_0028_0100; //!< Bits Allocated
	std::string tag_0028_0101; //!< Bits Stored
	std::string tag_0028_0102; //!< High Bit
	std::string tag_0028_0103; //!< Pixel Representation
	std::string tag_0028_0106; //!< Smallest Image Pixel Value
	std::string tag_0028_0107; //!< Largest Image Pixel Value
	std::string tag_0028_0120; //!< Pixel Padding Value
	std::string tag_0028_1050; //!< Window Center
	std::string tag_0028_1051; //!< Window Width
	std::string tag_0032_1032; //!< Requesting Physician
	std::string tag_0040_0253; //!< Performed Procedure Step ID
	std::string tag_0040_0254; //!< Performed Procedure Step Description
	std::string tag_0040_9224; //!< Real World Value Intercept
	std::string tag_0040_9225; //!< Real World Value Slope
	std::string tag_2001_1003; //!< Diffusion B-Factor
	std::string tag_2001_1004; //!< Diffusion Direction
	std::string tag_2001_10b0; //!< Diffusion Direction RL
	std::string tag_2001_10b1; //!< Diffusion Direction AP
	std::string tag_2001_10b2; //!< Diffusion Direction FH
	std::vector<unsigned char> fullHeader; //!< Full DICOM Header

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist( Action& a )
	{
		Wt::Dbo::field( a, tag_0008_0008, "0008,0008" );
		Wt::Dbo::field( a, tag_0008_0016, "0008,0016" );
		Wt::Dbo::field( a, tag_0008_0018, "0008,0018" );
		Wt::Dbo::field( a, tag_0008_0020, "0008,0020" );
		Wt::Dbo::field( a, tag_0008_0021, "0008,0021" );
		Wt::Dbo::field( a, tag_0008_0023, "0008,0023" );
		Wt::Dbo::field( a, tag_0008_0030, "0008,0030" );
		Wt::Dbo::field( a, tag_0008_0031, "0008,0031" );
		Wt::Dbo::field( a, tag_0008_0032, "0008,0032" );
		Wt::Dbo::field( a, tag_0008_0033, "0008,0033" );
		Wt::Dbo::field( a, tag_0008_0060, "0008,0060" );
		Wt::Dbo::field( a, tag_0008_0070, "0008,0070" );
		Wt::Dbo::field( a, tag_0008_1090, "0008,1090" );
		Wt::Dbo::field( a, tag_0010_0010, "0010,0010" );
		Wt::Dbo::field( a, tag_0010_0020, "0010,0020" );
		Wt::Dbo::field( a, tag_0010_0030, "0010,0030" );
		Wt::Dbo::field( a, tag_0010_0040, "0010,0040" );
		Wt::Dbo::field( a, tag_0010_1010, "0010,1010" );
		Wt::Dbo::field( a, tag_0010_21c0, "0010,21c0" );
		Wt::Dbo::field( a, tag_0018_0020, "0018,0020" );
		Wt::Dbo::field( a, tag_0018_0021, "0018,0021" );
		Wt::Dbo::field( a, tag_0018_0022, "0018,0022" );
		Wt::Dbo::field( a, tag_0018_0023, "0018,0023" );
		Wt::Dbo::field( a, tag_0018_0024, "0018,0024" );
		Wt::Dbo::field( a, tag_0018_0025, "0018,0025" );
		Wt::Dbo::field( a, tag_0018_0050, "0018,0050" );
		Wt::Dbo::field( a, tag_0018_0080, "0018,0080" );
		Wt::Dbo::field( a, tag_0018_0081, "0018,0081" );
		Wt::Dbo::field( a, tag_0018_0082, "0018,0082" );
		Wt::Dbo::field( a, tag_0018_0083, "0018,0083" );
		Wt::Dbo::field( a, tag_0018_0084, "0018,0084" );
		Wt::Dbo::field( a, tag_0018_0085, "0018,0085" );
		Wt::Dbo::field( a, tag_0018_0086, "0018,0086" );
		Wt::Dbo::field( a, tag_0018_0087, "0018,0087" );
		Wt::Dbo::field( a, tag_0018_0088, "0018,0088" );
		Wt::Dbo::field( a, tag_0018_0089, "0018,0089" );
		Wt::Dbo::field( a, tag_0018_0090, "0018,0090" );
		Wt::Dbo::field( a, tag_0018_0091, "0018,0091" );
		Wt::Dbo::field( a, tag_0018_0093, "0018,0093" );
		Wt::Dbo::field( a, tag_0018_0094, "0018,0094" );
		Wt::Dbo::field( a, tag_0018_0095, "0018,0095" );
		Wt::Dbo::field( a, tag_0018_1020, "0018,1020" );
		Wt::Dbo::field( a, tag_0018_1040, "0018,1040" );
		Wt::Dbo::field( a, tag_0018_1041, "0018,1041" );
		Wt::Dbo::field( a, tag_0018_1042, "0018,1042" );
		Wt::Dbo::field( a, tag_0018_1043, "0018,1043" );
		Wt::Dbo::field( a, tag_0018_1044, "0018,1044" );
		Wt::Dbo::field( a, tag_0018_1045, "0018,1045" );
		Wt::Dbo::field( a, tag_0018_1046, "0018,1046" );
		Wt::Dbo::field( a, tag_0018_1047, "0018,1047" );
		Wt::Dbo::field( a, tag_0018_1048, "0018,1048" );
		Wt::Dbo::field( a, tag_0018_1049, "0018,1049" );
		Wt::Dbo::field( a, tag_0018_1050, "0018,1050" );
		Wt::Dbo::field( a, tag_0018_1060, "0018,1060" );
		Wt::Dbo::field( a, tag_0018_1088, "0018,1088" );
		Wt::Dbo::field( a, tag_0018_1090, "0018,1090" );
		Wt::Dbo::field( a, tag_0018_1094, "0018,1094" );
		Wt::Dbo::field( a, tag_0018_1100, "0018,1100" );
		Wt::Dbo::field( a, tag_0018_1250, "0018,1250" );
		Wt::Dbo::field( a, tag_0018_1310, "0018,1310" );
		Wt::Dbo::field( a, tag_0018_1312, "0018,1312" );
		Wt::Dbo::field( a, tag_0018_1314, "0018,1314" );
		Wt::Dbo::field( a, tag_0018_1315, "0018,1315" );
		Wt::Dbo::field( a, tag_0018_1316, "0018,1316" );
		Wt::Dbo::field( a, tag_0018_5100, "0018,5100" );
		Wt::Dbo::field( a, tag_0018_9075, "0018,9075" );
		Wt::Dbo::field( a, tag_0018_9087, "0018,9087" );
		Wt::Dbo::field( a, tag_0018_9089, "0018,9089" );
		Wt::Dbo::field( a, tag_0020_000d, "0020,000d" );
		Wt::Dbo::field( a, tag_0020_000e, "0020,000e" );
		Wt::Dbo::field( a, tag_0020_0010, "0020,0010" );
		Wt::Dbo::field( a, tag_0020_0011, "0020,0011" );
		Wt::Dbo::field( a, tag_0020_0012, "0020,0012" );
		Wt::Dbo::field( a, tag_0020_0013, "0020,0013" );
		Wt::Dbo::field( a, tag_0020_0032, "0020,0032" );
		Wt::Dbo::field( a, tag_0020_0037, "0020,0037" );
		Wt::Dbo::field( a, tag_0020_0052, "0020,0052" );
		Wt::Dbo::field( a, tag_0020_0060, "0020,0060" );
		Wt::Dbo::field( a, tag_0020_0100, "0020,0100" );
		Wt::Dbo::field( a, tag_0020_0105, "0020,0105" );
		Wt::Dbo::field( a, tag_0020_0110, "0020,0110" );
		Wt::Dbo::field( a, tag_0020_1040, "0020,1040" );
		Wt::Dbo::field( a, tag_0020_1041, "0020,1041" );
		Wt::Dbo::field( a, tag_0028_0002, "0028,0002" );
		Wt::Dbo::field( a, tag_0028_0003, "0028,0003" );
		Wt::Dbo::field( a, tag_0028_0004, "0028,0004" );
		Wt::Dbo::field( a, tag_0028_0010, "0028,0010" );
		Wt::Dbo::field( a, tag_0028_0011, "0028,0011" );
		Wt::Dbo::field( a, tag_0028_0030, "0028,0030" );
		Wt::Dbo::field( a, tag_0028_0100, "0028,0100" );
		Wt::Dbo::field( a, tag_0028_0101, "0028,0101" );
		Wt::Dbo::field( a, tag_0028_0102, "0028,0102" );
		Wt::Dbo::field( a, tag_0028_0103, "0028,0103" );
		Wt::Dbo::field( a, tag_0028_0106, "0028,0106" );
		Wt::Dbo::field( a, tag_0028_0107, "0028,0107" );
		Wt::Dbo::field( a, tag_0028_0120, "0028,0120" );
		Wt::Dbo::field( a, tag_0028_1050, "0028,1050" );
		Wt::Dbo::field( a, tag_0028_1051, "0028,1051" );
		Wt::Dbo::field( a, tag_0032_1032, "0032,1032" );
		Wt::Dbo::field( a, tag_0040_0253, "0040,0253" );
		Wt::Dbo::field( a, tag_0040_0254, "0040,0254" );
		Wt::Dbo::field( a, tag_0040_9224, "0040,9224" );
		Wt::Dbo::field( a, tag_0040_9225, "0040,9225" );
		Wt::Dbo::field( a, tag_2001_1003, "2001,1003" );
		Wt::Dbo::field( a, tag_2001_1004, "2001,1004" );
		Wt::Dbo::field( a, tag_2001_10b0, "2001,10b0" );
		Wt::Dbo::field( a, tag_2001_10b1, "2001,10b1" );
		Wt::Dbo::field( a, tag_2001_10b2, "2001,10b2" );
		Wt::Dbo::field( a, fullHeader, "full_header" );
	}
};

}
}

#endif // DICOMDATA_H
