#ifndef OVERLAY_H
#define OVERLAY_H

#include <Wt/Dbo/Dbo.h>
#include <optional>

namespace quantx {

namespace dicom {
class DicomSeries;
}

namespace overlay {

/*!
 * \brief The DbOverlay database table class
 * \ingroup SQLModule
 * This class is used to store and load overlays (annotations) to and from the database
 */

class DbOverlay
{
public:
	std::string UUID; //!< Unique identifier for this overlay
	std::string sessionId; //!< Session ID that this overlay was created in
	std::string mrn; //!< Patient MRN
	std::string studyUID; //!< Study UID
	std::string seriesUID; //!< Series UID
	std::string accession_number; //!< Accession Number
	std::string overlayTypeName; //!< Overlay Type String
	std::optional<int> imageNumber; //!< Image Number (e.g. MR slice) that this overlay was on
	std::vector<unsigned char> overlayImage; //!< Image capture of overlay
	std::vector<unsigned char> overlayData; //!< Raw data used to generate overlay

	Wt::Dbo::ptr<quantx::dicom::DicomSeries> series; //!< Overlay belongs to One Series

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist(Action &a)
	{
		Wt::Dbo::field( a, UUID, "uuid" );
		Wt::Dbo::field( a, sessionId, "session" );
		Wt::Dbo::field( a, mrn, "mrn" );
		Wt::Dbo::field( a, studyUID, "studyuid" );
		Wt::Dbo::field( a, seriesUID, "seriesuid" );
		Wt::Dbo::field( a, accession_number, "accession_number" );
		Wt::Dbo::field( a, overlayTypeName, "overlay_type_name" );
		Wt::Dbo::field( a, imageNumber, "image_number" );
		Wt::Dbo::field( a, overlayImage, "overlay_image" );
		Wt::Dbo::field( a, overlayData, "overlay_data" );

		Wt::Dbo::belongsTo( a, series );
	}
};

} // overlay
} // quantx

#endif // OVERLAY_H

