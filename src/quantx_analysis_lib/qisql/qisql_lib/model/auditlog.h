#ifndef AUDITLOG_H
#define AUDITLOG_H

#include <Wt/Dbo/Dbo.h>

namespace quantx {
namespace auditlog {

/*!
 * \brief The AuditLog database table class
 * \ingroup SQLModule
 * This class is used to store and load any actions for recording into the audit logs.
 */

class AuditLog
{
public:
	std::string DateAndTime;  //! Date and time of the action
	std::string UserName; //! The user name of the account that performed the action
	std::string ActionDesc; //! A description of the action performed
	std::string MRN; //! The MRN of the patient on which the action was performed

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::field(a, DateAndTime, "date_and_time");
		Wt::Dbo::field(a, UserName, "username");
		Wt::Dbo::field(a, ActionDesc, "action_description");
		Wt::Dbo::field(a, MRN, "mrn");
	}

};

}
}

#endif // AUDITLOG_H
