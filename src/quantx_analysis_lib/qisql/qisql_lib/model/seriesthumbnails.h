#ifndef SERIESTHUMBNAILS_H
#define SERIESTHUMBNAILS_H

#include <Wt/Dbo/Dbo.h>

namespace quantx {
namespace dicom {
class DicomSeries;

/*!
 * \brief The SeriesThumbnails database table class
 * \ingroup SQLModule
 * This class is used to store and load image thumbnails for each DICOM series
 */

class SeriesThumbnails
{
public:
	std::string seriesUID; //!< Series UID that this thumbnail is a representation of
	int maxDim; //!< The longest dimension of (width, height)
	int minDim; //!< The shortest dimension of (width, height)
	int width; //!< The thumbnail width
	int height; //!< The thumbnail height
	std::vector<unsigned char> thumbnail; //!< Thumbnail image data

	Wt::Dbo::ptr<DicomSeries> series; //!< SeriesThumbnails belongs to one Series

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist( Action& a )
	{
		Wt::Dbo::field( a, seriesUID, "series_uid" );
		Wt::Dbo::field( a, maxDim, "max_dim" );
		Wt::Dbo::field( a, minDim, "min_dim" );
		Wt::Dbo::field( a, width, "width" );
		Wt::Dbo::field( a, height, "height" );
		Wt::Dbo::field( a, thumbnail, "thumbnail" );

		Wt::Dbo::belongsTo( a, series );
	}
};
}
}

#endif // SERIESTHUMBNAILS_H
