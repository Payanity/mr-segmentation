#ifndef LESION_H
#define LESION_H

#include <Wt/Dbo/Dbo.h>

#include "lesiondetails.h"

namespace quantx {
namespace dicom {
class DicomSeries;
}
}
//class LesionDetails;

namespace quantx {
namespace mranalysis {

/*!
 * \brief The Lesion datbase table class
 * \ingroup SQLModule
 * This class is used to store and load MR lesions which have been analyzed by QuantX
 */
class Lesion
{
public:
	std::string UUID; //!< Lesion UUID
	std::string sessionId; //!< Session ID when lesion was created
	std::string mrn; //!< Patient MRN
	std::string studyUID; //!< Study UID
	std::string accession_number; //!< Accession Number
	std::string seriesUID; //!< Series UID
	double qi_score; //!< QI Score
	double volume; //!< Volume
	double surface_area; //!< Surface Area
	int seed_x; //!< Seed point for segmentation in image coordinates
	int seed_y; //!< Seed point for segmentation in image coordinates
	int seed_z; //!< Seed point for segmentation in image coordinates
	float seed_realworld_x; //!< Seed point for segmentation in real world coordinates
	float seed_realworld_y; //!< Seed point for segmentation in real world coordinates
	float seed_realworld_z; //!< Seed point for segmentation in real world coordinates
	std::vector<unsigned char> timepoints; //!< Timestamps of DCE image
	std::vector<unsigned char> maxUptake; //!< Max update of lesion (using c-means)
	std::vector<unsigned char> avgUptake; //!< Average uptake of lesion

	Wt::Dbo::ptr<quantx::dicom::DicomSeries> series; //!< Lesion belongs to One Series
	Wt::Dbo::weak_ptr<LesionDetails> lesionDetails; //!< Lesion has One LesionDetails

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist(Action &a)
	{
		Wt::Dbo::field( a, UUID, "uuid" );
		Wt::Dbo::field( a, sessionId, "session" );
		Wt::Dbo::field( a, mrn, "mrn" );
		Wt::Dbo::field( a, studyUID, "studyuid" );
		Wt::Dbo::field( a, accession_number, "accession_number" );
		Wt::Dbo::field( a, seriesUID, "seriesuid" );
		Wt::Dbo::field( a, qi_score, "qi_score" );
		Wt::Dbo::field( a, volume, "volume" );
		Wt::Dbo::field( a, surface_area, "surface_area" );
		Wt::Dbo::field( a, seed_x, "seed_x" );
		Wt::Dbo::field( a, seed_y, "seed_y" );
		Wt::Dbo::field( a, seed_z, "seed_z" );
		Wt::Dbo::field( a, seed_realworld_x, "seed_realworld_x" );
		Wt::Dbo::field( a, seed_realworld_y, "seed_realworld_y" );
		Wt::Dbo::field( a, seed_realworld_z, "seed_realworld_z" );
		Wt::Dbo::field( a, timepoints, "timepoints" );
		Wt::Dbo::field( a, maxUptake, "maxuptake" );
		Wt::Dbo::field( a, avgUptake, "avguptake" );

		Wt::Dbo::belongsTo( a, series );
		Wt::Dbo::hasOne( a, lesionDetails );
	}
};
}
}

#include "dicomseries.h"

#endif // LESION_H
