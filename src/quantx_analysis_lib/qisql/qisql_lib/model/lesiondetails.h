#ifndef LESIONDETAILS_H
#define LESIONDETAILS_H

#include <Wt/Dbo/Dbo.h>

namespace quantx {
namespace mranalysis {
class Lesion;

/*!
 * \brief The LesionDetails database table class
 * \ingroup SQLModule
 * This class is used to store and load additional details about an MR lesion analyzed using QuantX
 */

class LesionDetails
{
public:
	std::string UUID; //!< UUID of lesion
	std::vector<unsigned char> features; //!< Features calculated by MRAnalysis module
	std::vector<unsigned char> segmentation_outline; //!< Outline of lesion segmentation
	std::vector<unsigned char> segmentation_filled; //!< List of all points within lesion boundary
	std::vector<unsigned char> rawdata; //!< Raw pixel data used for feature calculation
	std::vector<unsigned char> similarcases; //!< Similar cases that were displayed to the user at the time of analysis

	Wt::Dbo::ptr<Lesion> lesion; //!< LesionDetails belongs to One Lesion

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist(Action &a)
	{
		Wt::Dbo::field(a, UUID, "uuid");
		Wt::Dbo::field(a, features, "features");
		Wt::Dbo::field(a, segmentation_outline, "segmentation_outline");
		Wt::Dbo::field(a, segmentation_filled, "segmentation_filled");
		Wt::Dbo::field(a, rawdata, "rawdata");
		Wt::Dbo::field(a, similarcases, "similarcases");

		Wt::Dbo::belongsTo( a, lesion );
	}
};
}
}

#endif // LESIONDETAILS_H
