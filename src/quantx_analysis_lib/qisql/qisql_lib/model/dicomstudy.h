#ifndef STUDY_H
#define STUDY_H

#include <Wt/Dbo/Dbo.h>

namespace quantx {
namespace patient {
class Patient;
}
namespace dicom {
class DicomSeries;
}
}



namespace quantx {
namespace dicom {
/*!
 * \brief The DicomStudy database table class
 * \ingroup SQLModule
 * This class is used to store and load DICOM study metadata
 */

class DicomStudy
{
public:
	std::optional<std::string> mrn; //!< Patient MRN
	std::optional<std::string> studyuid; //!< Study UID
	std::optional<std::string> modality; //!< Modality
	std::optional<Wt::WDate> date; //!< Study Date
	std::optional<Wt::WTime> time; //!< Study Time
	std::optional<std::string> accessionNumber;  //!< Accession Number

	Wt::Dbo::ptr<quantx::patient::Patient> patient; //!< Study belongs to one Patient
	Wt::Dbo::collection<Wt::Dbo::ptr<DicomSeries>> series; //!< Study has many Series

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template <class Action>
	void persist( Action& a )
	{
		Wt::Dbo::belongsTo( a, patient );

		Wt::Dbo::field( a, mrn, "mrn" );
		Wt::Dbo::field( a, studyuid, "study_uid" );
		Wt::Dbo::field( a, modality, "modality" );
		Wt::Dbo::field( a, date, "study_date" );
		Wt::Dbo::field( a, time, "study_time" );
		Wt::Dbo::field( a, accessionNumber, "accession_number" );

		Wt::Dbo::hasMany( a, series, Wt::Dbo::RelationType::ManyToOne );
	}

};
}
}

#include "patient.h"
#include "dicomseries.h"

#endif // STUDY_H
