#ifndef PLUGINDATA_H
#define PLUGINDATA_H

#include <Wt/Dbo/Dbo.h>

/*!
 * \brief The PluginData database table class
 * \ingroup SQLModule
 * This class is used to store and load data that may be used by a QuantX Plugin
 */
class PluginData
{
public:
	PluginData() = default; //!< PluginData constructor
	~PluginData() = default; //!< PluginData destructor

	std::string pluginName{}; //!< The name of the plugin that created the object
	std::string sessionId{}; //!< ID of the session in which this object was created
	std::optional<std::string> mrn{}; //!< Patient MRN
	std::string key{}; //!< The key in a key-value pair lookup table for use by the plugin
	std::vector<unsigned char> data{}; //!< The raw data of this object for use by the plugin

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist( Action& a )
	{
		Wt::Dbo::field( a, pluginName, "name" );
		Wt::Dbo::field( a, sessionId, "session_id" );
		Wt::Dbo::field( a, mrn, "mrn" );
		Wt::Dbo::field( a, key, "key" );
		Wt::Dbo::field( a, data, "data" );
	}
};

#endif // PLUGINDATA_H
