#ifndef SERIES_H
#define SERIES_H

#include <Wt/Dbo/Dbo.h>

#include "images2d.h"
#include "lesion.h"
#include "overlay.h"
#include <limits>
#include <compare>


namespace quantx {
namespace dicom {
class DicomStudy;
class SeriesThumbnails;
class MotionCorrection;
class Images2D;
}
namespace mranalysis {
class Lesion;
}

}

namespace quantx {
namespace dicom {

/*!
 * \brief The DicomSeries database table class
 * \ingroup SQLModule
 * This class is used to store and load DICOM series metadata
 */

class DicomSeries
{
public:
	std::optional<std::string> mrn; //!< Patient MRN
	std::string seriesuid; //!< Series UID
	std::optional<std::string> studyuid; //!< Study UID
	std::optional<std::string> seriesNumber; //!< Series Number
	std::optional<std::string> modality; //!< Modality
	std::optional<std::string> seriesDesc; //!< Series Description
	std::optional<Wt::WDate> date; //!< Series Date
	std::optional<Wt::WTime> time; //!< Series Time
	std::optional<int> numTemporalPositions; //!< Number of Temporal Positions (if multiple exist)
	std::optional<std::string> imageOrientation; //!< Image Orientation (Patient)
	std::optional<std::string> spaceBetweenSlices; //!< Space Between Slices (if multiple slices)

	Wt::Dbo::ptr<DicomStudy> study; //!< Series has one Study
	Wt::Dbo::collection<Wt::Dbo::ptr<SeriesThumbnails>> thumbnails; //!< Series has Many SeriesThumbnails
	Wt::Dbo::weak_ptr<MotionCorrection> motionCorrection; //!< Series has One Motion Correction
	Wt::Dbo::collection<Wt::Dbo::ptr<quantx::mranalysis::Lesion>> lesions; //!< Series has Many Lesions
	Wt::Dbo::collection<Wt::Dbo::ptr<Images2D>> images2D; //!< Series has Many 2DImages
	Wt::Dbo::collection<Wt::Dbo::ptr<overlay::DbOverlay>> overlays; //!< Series has Many Overlays

	constexpr auto operator <=> (const DicomSeries &other) const {
		return convertSeriesNumStringToInt(seriesNumber.value()) <=> convertSeriesNumStringToInt(other.seriesNumber.value());
	}

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template <class Action>
	void persist( Action& a )
	{
		Wt::Dbo::belongsTo( a, study );

		Wt::Dbo::field( a, mrn, "mrn" );
		Wt::Dbo::field( a, seriesuid, "series_uid" );
		Wt::Dbo::field( a, studyuid, "study_uid" );
		Wt::Dbo::field( a, seriesNumber, "series_number" );
		Wt::Dbo::field( a, modality, "modality" );
		Wt::Dbo::field( a, seriesDesc, "series_desc" );
		Wt::Dbo::field( a, date, "series_date" );
		Wt::Dbo::field( a, time, "series_time" );
		Wt::Dbo::field( a, numTemporalPositions, "num_temporal_positions" );
		Wt::Dbo::field( a, imageOrientation, "image_orientation" );
		Wt::Dbo::field( a, spaceBetweenSlices, "space_between_slices" );

		Wt::Dbo::hasMany( a, thumbnails, Wt::Dbo::RelationType::ManyToOne );
		Wt::Dbo::hasOne( a, motionCorrection );
		Wt::Dbo::hasMany( a, lesions, Wt::Dbo::RelationType::ManyToOne );
		Wt::Dbo::hasMany( a, images2D, Wt::Dbo::RelationType::ManyToOne );
		Wt::Dbo::hasMany( a, overlays, Wt::Dbo::RelationType::ManyToOne );
	}

private:
	static constexpr auto INVALID_OR_MISSING_SERIES_NUM_COMPARISON_VALUE = std::numeric_limits<int>::max();
	/*!
	 * \brief DicomSeries::convertSeriesNumStringToInt
	 * \param seriesNum The string we want to convert to an int.
	 * \return An integer, set to INVALID_OR_MISSING_SERIES_NUM_COMPARISON_VALUE (currently 100k) upon failure to convert.
	 */
	inline int convertSeriesNumStringToInt(std::string seriesNum) const {
		int seriesNumConverted = 0;

		try {
			seriesNumConverted = std::stoi(seriesNum);
		} catch (...) {
			seriesNumConverted = INVALID_OR_MISSING_SERIES_NUM_COMPARISON_VALUE;
		}

		return seriesNumConverted;
	}
};
}
}

#include "lesion.h"
#include "images2d.h"
#include "dicomstudy.h"
#include "seriesthumbnails.h"
#include "motioncorrection.h"

#endif // SERIES_H
