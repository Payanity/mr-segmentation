#ifndef PATIENT_H
#define PATIENT_H

#include <Wt/Dbo/Dbo.h>
#include <Wt/WDate.h>
#include <Wt/Dbo/Field.h>



namespace quantx {
namespace dicom {
class DicomStudy;
}
namespace patient {

/*!
 * \brief The Patient database table class
 * \ingroup SQLModule
 * This class is used to store and load Patient metadata to and from the database
 */

class Patient
{
public:
	std::string mrn; //!< Patient MRN
	std::optional<std::string> name; //!< Patient Name
	std::optional<Wt::WDate> dateOfBirth; //!< Patient Date of Birth
	std::optional<std::string> sex; //!< Patient Sex

	Wt::Dbo::collection<Wt::Dbo::ptr<dicom::DicomStudy>> studies; //!< One Patient has Many Study

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template <class Action>
	void persist( Action& a )
	{
		Wt::Dbo::field( a, mrn, "mrn" );
		Wt::Dbo::field( a, name, "patient_name" );
		Wt::Dbo::field( a, dateOfBirth, "dob" );
		Wt::Dbo::field( a, sex, "sex" );

		Wt::Dbo::hasMany( a, studies, Wt::Dbo::RelationType::ManyToOne );
	}
};

}
}

#endif // PATIENT_H
