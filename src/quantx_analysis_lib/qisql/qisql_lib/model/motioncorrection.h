#ifndef MOTIONCORRECTION_H
#define MOTIONCORRECTION_H

#include <string>

#include <Wt/Dbo/Dbo.h>

namespace quantx {
namespace dicom {
class DicomSeries;
}
}

namespace quantx {
namespace dicom {

/*!
 * \brief The MotionCorrection database table class
 * \ingroup SQLModule
 * This class is used to store and load image transformations for DCE motion correction
 */

class MotionCorrection
{
public:
	std::string seriesuid_moving; //!< Series UID of warped image
	std::string seriesuid_fixed; //!< Series UID of fixed image
	std::optional<int> t_fixed; //!< Timepoint of fixed image
	std::vector<unsigned char> xform; //!< Plastimatch transform data
	std::vector<unsigned char> imageHeader; //!< Plastimatch image header information
	std::optional<float> default_value; //!< Plastimatch default_value

	Wt::Dbo::ptr<DicomSeries> series; //!< Motion Correction belongs to one Series

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist( Action& a )
	{
		Wt::Dbo::field( a, seriesuid_moving, "series_uid_moving" );
		Wt::Dbo::field( a, seriesuid_fixed, "series_uid_fixed" );
		Wt::Dbo::field( a, t_fixed, "t_fixed" );
		Wt::Dbo::field( a, xform, "xform" );
		Wt::Dbo::field( a, imageHeader, "image_header" );
		Wt::Dbo::field( a, default_value, "default_value" );

		Wt::Dbo::belongsTo( a, series);
	}
};
}
}

#endif // MOTIONCORRECTION_H
