#ifndef ACQUISITIONPROTOCOL_H
#define ACQUISITIONPROTOCOL_H

#include <Wt/Dbo/Dbo.h>

namespace quantx {
namespace acquisition {

/*!
 * \brief The Acquisition Protocol database table for site-wide preferences
 * \ingroup SQLModule
 * This class is used to store and load different acquisition protocol types in the database for site-wide preferences.
 * One example of its use is having different time points for the initial rise based on manufacturer or acquisition site.
 */

class AcquisitionProtocol
{
public:

	int protocolNumber; //!< Index of the protocol number
	std::string keyName; //!< The key of a key-value pair used for protocol lookup
	std::string keyValue; //!< The value of a key-value pair used for protocol lookup

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist( Action& a )
	{
		Wt::Dbo::field( a, protocolNumber, "protocol_number" );
		Wt::Dbo::field( a, keyName, "key_name" );
		Wt::Dbo::field( a, keyValue, "key_value" );
	}

};

}
}

#endif // ACQUISITIONPROTOCOL_H
