#ifndef USERACCESS_H
#define USERACCESS_H

#include <Wt/Dbo/Dbo.h>

/*!
 * \brief The UserAccess database table class
 * \ingroup SQLModule
 * This class is used to manage user access to the QuantX software
 */

class UserAccess
{
public:
	std::string userName; //!< User name for policy
	std::string groupName; //!< Group name for policy
	std::string enabled; //!< Holds data representing whether this policy is enabled
	std::string allow; //!< Holds data representing whether this policy allows/disallows the user/group

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::field(a, userName, "username");
		Wt::Dbo::field(a, groupName, "group_name");
		Wt::Dbo::field(a, enabled, "enabled");
		Wt::Dbo::field(a, allow, "allow");
	}
};

#endif // USERACCESS_H
