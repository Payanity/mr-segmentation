#ifndef IMAGES2D_H
#define IMAGES2D_H

#include <Wt/Dbo/Dbo.h>
#include <Wt/WDate.h>
#include "qtsqltraits.h"

namespace quantx {
namespace dicom {
class DicomSeries;
}
}

namespace quantx {
namespace dicom {

/*!
 * \brief The Images2D database table class
 * \ingroup SQLModule
 * This class is used to stoare and load DICOM Image data and metadata
 */

class Images2D
{
public:
	std::optional<std::string> mrn; //!< Patient MRN
	std::string imageuid; //!< Image UID
	std::optional<std::string> seriesuid; //!< Series UID
	std::optional<std::string> studyuid; //!< Study UID
	std::optional<std::string> modality; //!< Modality
	std::optional<std::vector<unsigned char>> image_data; //!< Raw DICOM Image Data
	std::optional<Wt::WDate> date; //!< Image Date
	std::optional<Wt::WTime> time; //!< Image Time
	std::optional<int> width; //!< Image Width
	std::optional<int> height; //!< Image Height
	std::optional<double> pixelsize_x; //!< Real World Width of Image Columns
	std::optional<double> pixelsize_y; //!< Real World Height of Image Rows
	std::optional<std::string> imageType; //!< Image Type
	std::optional<std::string> windowWidth; //!< Default Window Width (Contrast)
	std::optional<std::string> windowCenter; //!< Default Window Center (Brightness)
	std::optional<std::string> minPixval; //!< Minimum Pixel Value in Image
	std::optional<std::string> maxPixval; //!< Maximum Pixel Value in Image
	std::optional<std::string> imagePosition; //!< Image Position (Patient)
	std::optional<std::string> sliceLocation; //!< Slice Location (coordinates)
	std::optional<std::string> temporalPosID; //!< Temporal Position ID
	std::optional<std::string> instanceNum; //!< Instance Number
	std::optional<std::string> scanOptions; //!< Scan Options
	std::optional<std::string> viewPosition; //!< View Position (patient)
	std::optional<std::string> imageLaterality; //!< Image Laterality (patient)
	std::optional<std::string> patientOrientation; //!< Patient Orientation
	std::optional<std::string> photometricInterpretation; //!< Photometric Interpretation
	std::optional<std::string> manufacturer; //!< Manufacturer of Acquisitiom Device
	std::optional<std::string> modelName; //!< Model of Acquisition Device
	//MR
	std::optional<std::string> diffusionBValue; //!< Diffusion B-Value
	std::optional<std::string> diffusionGradientOrientation; //!< Diffusion Gradient Orientation
	std::optional<int> triggerTime; //!< Trigger Time
	std::optional<float> magneticFieldStrength; //!< Magnetic Field Strength
	// MG and US
	std::optional<int> numberOfFrames; //!< Number Of Frames

	Wt::Dbo::ptr<DicomSeries> series; //!< Images2D belongs to One Series

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist( Action& a )
	{
		Wt::Dbo::field( a, mrn, "mrn" );
		Wt::Dbo::field( a, imageuid, "image_uid" );
		Wt::Dbo::field( a, seriesuid, "series_uid" );
		Wt::Dbo::field( a, studyuid, "study_uid" );
		Wt::Dbo::field( a, modality, "modality" );
		Wt::Dbo::field( a, image_data, "image_data" );
		Wt::Dbo::field( a, date, "image_date" );
		Wt::Dbo::field( a, time, "image_time" );
		Wt::Dbo::field( a, width, "width" );
		Wt::Dbo::field( a, height, "height" );
		Wt::Dbo::field( a, pixelsize_x, "pixelsize_x" );
		Wt::Dbo::field( a, pixelsize_y, "pixelsize_y" );
		Wt::Dbo::field( a, imageType, "image_type" );
		Wt::Dbo::field( a, windowWidth, "window_width" );
		Wt::Dbo::field( a, windowCenter, "window_center" );
		Wt::Dbo::field( a, minPixval, "min_pixval" );
		Wt::Dbo::field( a, maxPixval, "max_pixval" );
		Wt::Dbo::field( a, imagePosition, "image_position" );
		Wt::Dbo::field( a, sliceLocation, "slice_location" );
		Wt::Dbo::field( a, temporalPosID, "temporal_pos_id" );
		Wt::Dbo::field( a, diffusionBValue, "diffusion_b_value" );
		Wt::Dbo::field( a, diffusionGradientOrientation, "diffusion_gradient_orientation" );
		Wt::Dbo::field( a, instanceNum, "instance_num" );
		Wt::Dbo::field( a, scanOptions, "scan_options" );
		Wt::Dbo::field( a, viewPosition, "view_position" );
		Wt::Dbo::field( a, imageLaterality, "image_laterality" );
		Wt::Dbo::field( a, patientOrientation, "patient_orientation" );
		Wt::Dbo::field( a, photometricInterpretation, "photometric_interpretation" );
		Wt::Dbo::field( a, manufacturer, "manufacturer" );
		Wt::Dbo::field( a, modelName, "model_name" );
		Wt::Dbo::field( a, triggerTime, "trigger_time" );
		Wt::Dbo::field( a, magneticFieldStrength, "magnetic_field_strength" );
		Wt::Dbo::field( a, numberOfFrames, "number_of_frames" );

		Wt::Dbo::belongsTo( a, series );
	}
};
}
}

#include "dicomseries.h"

#endif // IMAGES2D_H
