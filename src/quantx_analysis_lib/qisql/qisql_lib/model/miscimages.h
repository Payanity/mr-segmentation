#ifndef MISCIMAGES_H
#define MISCIMAGES_H

#include <Wt/Dbo/Dbo.h>

namespace quantx {
namespace dicom {
class MiscImages;
}
}

namespace Wt {
    namespace Dbo {
	// Additional custom traits for the MiscImages class, used internally by Wt::dbo
	    template<>
	    struct dbo_traits<quantx::dicom::MiscImages> : public dbo_default_traits
		{
			static const char* surrogateIdField()
			{
				return "idqi_images";
			}
			static const char* versionField()
			{
				return 0;
			}
		};

	}
}

namespace quantx {
namespace dicom {

/*!
 * \brief The MiscImages database table class
 * \ingroup SQLModule
 * \deprecated Use Images2D
 * This class is used to stoare and load DICOM Image data and metadata
 */

class MiscImages
{
public:
	std::string mrn; //!< Patient MRN
	std::string imageuid; //!< Image UID
	std::string seriesuid; //!< Series UID
	std::string studyuid; //!< Study UID
	std::string modality; //!< Modality
	std::vector<unsigned char> image_data; //!< Raw DICOM Image Data
	Wt::WDate date; //!< Image Date
	Wt::WTime time; //!< Image Time
	int width; //!< Image Width
	int height; //!< Image Height
	double pixelsize_x; //!< Real World Width of Image Columns
	double pixelsize_y; //!< Real World Height of Image Rows
	std::string imageType; //!< Image Type
	std::string windowWidth; //!< Default Window Width (Contrast)
	std::string windowCenter; //!< Default Window Center (Brightness)
	std::string minPixval; //!< Minimum Pixel Value in Image
	std::string maxPixval; //!< Maximum Pixel Value in Image
	std::string imagePosition; //!< Image Position (Patient)
	std::string sliceLocation; //!< Slice Location (coordinates)
	std::string temporalPosID; //!< Temporal Position ID
	std::string instanceNum; //!< Instance Number
	std::string scanOptions; //!< Scan Options
	std::string viewPosition; //!< View Position (patient)
	std::string imageLaterality; //!< Image Laterality (patient)
	std::string patientOrientation; //!< Patient Orientation
	std::string photometricInterpretation; //!< Photometric Interpretation
	std::string manufacturer; //!< Manufacturer of Acquisitiom Device
	std::string modelName; //!< Model of Acquisition Device

	//! This function is used internally by Wt::dbo to connect attributes to columns in a database table
	template<class Action>
	void persist( Action& a )
	{
		Wt::Dbo::field( a, mrn, "mrn" );
		Wt::Dbo::field( a, imageuid, "image_uid" );
		Wt::Dbo::field( a, seriesuid, "series_uid" );
		Wt::Dbo::field( a, studyuid, "study_uid" );
		Wt::Dbo::field( a, modality, "modality" );
		Wt::Dbo::field( a, image_data, "image_data" );
		Wt::Dbo::field( a, date, "image_date" );
		Wt::Dbo::field( a, time, "image_time" );
		Wt::Dbo::field( a, width, "width" );
		Wt::Dbo::field( a, height, "height" );
		Wt::Dbo::field( a, pixelsize_x, "pixelsize_x" );
		Wt::Dbo::field( a, pixelsize_y, "pixelsize_y" );
		Wt::Dbo::field( a, imageType, "image_type" );
		Wt::Dbo::field( a, windowWidth, "window_width" );
		Wt::Dbo::field( a, windowCenter, "window_center" );
		Wt::Dbo::field( a, minPixval, "min_pixval" );
		Wt::Dbo::field( a, maxPixval, "max_pixval" );
		Wt::Dbo::field( a, imagePosition, "image_position" );
		Wt::Dbo::field( a, sliceLocation, "slice_location" );
		Wt::Dbo::field( a, temporalPosID, "temporal_pos_id" );
		Wt::Dbo::field( a, instanceNum, "instance_num" );
		Wt::Dbo::field( a, scanOptions, "scan_options" );
		Wt::Dbo::field( a, viewPosition, "view_position" );
		Wt::Dbo::field( a, imageLaterality, "image_laterality" );
		Wt::Dbo::field( a, patientOrientation, "patient_orientation" );
		Wt::Dbo::field( a, photometricInterpretation, "photometric_interpretation" );
		Wt::Dbo::field( a, manufacturer, "manufacturer" );
		Wt::Dbo::field( a, modelName, "model_name" );
	}
};
}
}

#endif // MISCIMAGES_H
