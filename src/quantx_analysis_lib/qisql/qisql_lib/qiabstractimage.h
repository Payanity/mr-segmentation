#ifndef QIABSTRACTIMAGE_H
#define QIABSTRACTIMAGE_H

#include "qisqlDllCheck.h"
#include "../../quantxdata/quantxdata_lib/quantx.h"
#include <QVector>
#include <QTime>
#include <QVector3D>
#include <QStringList>
#include <QString>
#include <QList>

/*!
 * \brief Base class for all modality image types.
 * \ingroup SQLModule
 * \class QiAbstractImage
 * This class has many of the methods that can be overridden for modality-specific internal functionality.
 */
class QISQL_EXPORT QiAbstractImage
{
public:
	QiAbstractImage() = default;
	QiAbstractImage( const QiAbstractImage & ) = default;
	QiAbstractImage( QiAbstractImage && ) = default;
	QiAbstractImage& operator=( const QiAbstractImage & ) = default;
	QiAbstractImage& operator=( QiAbstractImage && ) = default;
	virtual ~QiAbstractImage() = default;

	virtual const double & windowWidth() const { return windowWidth_; } //!< Getter for the DICOM window width attribute.
	virtual const double & windowLevel() const { return windowLevel_; } //!< Getter for the DICOM window level attribute.
	virtual const quint16 & maxPixval() const { return maxPixval_; } //!< Getter for the max pixel value of the image.
	virtual const quint16 & minPixval() const { return minPixval_; } //!< Getter for the min pixel value of the image.
	virtual const float & dx() const { return dx_; } //!< Getter for the x dimension real-world size of a pixel/voxel in millimeters.
	virtual const float & dy() const { return dy_; } //!< Getter for the y dimension real-world size of a pixel/voxel in millimeters.
	virtual const int & nx() const { return nx_; } //!< Getter for the number of pixels or voxels in the x dimension.
	virtual const int & ny() const { return ny_; } //!< Getter for the number of pixels or voxels in the y dimension.
	virtual const QUANTX::Modality & modality() const { return modality_; } //!< Getter for the image's modality.
	virtual const QString & seriesUID() const { return seriesUID_; } //!< Getter for the series UID (Unique ID) for this image.
	virtual const QString & studyUID() const { return studyUID_; } //!< Getter for the study UID for this image.
	virtual const QString & accessionNumber() const { return accessionNumber_; } //!< Getter for the accession number for this image.
	virtual const QStringList & scanOptions() const { return scanOptions_; } //!< Getter for the list of scan options associated with this image.
	virtual const QString & mrn() const { return mrn_; } //!< Getter for the MRN (Medical Record Number) associated with this image.
	virtual const QString & seriesDesc() const { return seriesDesc_; } //!< Getter for the series description for this image.
	virtual const QTime & time() const { return time_; } //!< Getter for the image's acquisition time.
    virtual const QDate & date() const {return date_;} //!< Getter for the image's acquisition date.
    virtual const QString & manufacturer() const { return manufacturer_; } //!< Getter for the manufacturer of the scanner / device used to acquire this image.
	virtual const QString & modelName() const { return modelName_; } //!< Getter for the model name of the scanner / device used to acquire this image.

protected:
	double windowWidth_{}; //!< Stores the DICOM window width attribute value.
	double windowLevel_{}; //!< Stores the DICOM window level attribute value.
	quint16 maxPixval_{}; //!< Stores the max pixel value.
	quint16 minPixval_{}; //!< Stores the minimum pixel value.
	float dx_{}; //!< Stores the x dimension voxel size in millimeters.
	float dy_{}; //!< Stores the y dimension voxel size in millimeters.
	int nx_{}; //!< Stores the number of x dimension voxels.
	int ny_{}; //!< Stores the number of y dimension voxels.
	QUANTX::Modality modality_; //!< Stores the modality of the image.
	QString seriesUID_; //!< Stores the series UID of the image.
	QStringList scanOptions_; //!< Stores the list of scan options for the image.
	QString mrn_; //!< Stores the image's MRN.
	QString seriesDesc_; //!< Stores the series description.
	QString studyUID_; //!< Stores the study UID.
	QString accessionNumber_; //!< Stores the accession number.
	QTime time_; //!< Stores the time of the image.
    QDate date_; //!< Stores the date of the image.
	QString manufacturer_; //!< Stores the acquisition scanner manufacturer.
	QString modelName_; //!< Stores the acquisition scanner model name.

	/*!
	 * \brief Helper method to return a modality enum that maps to a string representation passed to the method.
	 * \param modalityString String representation of the modality.
	 * \return Modality enum value.
	 */
	[[nodiscard]] inline virtual const QUANTX::Modality modalityFromString(const QString& modalityString) const noexcept {
						if (modalityString == "US") {
							return QUANTX::US;
						} else if (modalityString == "US3D") {
							return QUANTX::US3D;
						} else if (modalityString == "MG") {
							return QUANTX::MAMMO;
						} else if (modalityString == "MG3D") {
							return QUANTX::MG3D;
						} else if (modalityString == "MR") {
							return QUANTX::MRI;
						} else {
							return QUANTX::OTHER;
						}
	}
};

#endif // QIABSTRACTIMAGE_H
