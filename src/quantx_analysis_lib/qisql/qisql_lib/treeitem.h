#ifndef TREEITEM_H
#define TREEITEM_H

#include <QVariant>
#include <QVector>
#include "qisqlDllCheck.h"


/*!
 * \brief The TreeItem class
 * \class TreeItem
 * \ingroup SQLModule
 * This is a data structure used by classes that use trees like the PatientModel etc.
 */
class QISQL_EXPORT TreeItem
{
public:
	explicit TreeItem( TreeItem* parentItem );
    explicit TreeItem(const QVector<QVariant> &data, TreeItem *parentItem = nullptr);
    ~TreeItem();

    void appendChild(TreeItem *child);

    TreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
	int childNumber() const;
    QVariant data(int column) const;
    bool insertChildren(int position, int count, int columns);
	bool setData(int column, const QVariant &value);
	int row() const;
    TreeItem *parentItem();
	void deleteChildren();

private:
    QVector<TreeItem*> m_childItems; //!< Stores pointers to child items.
    QVector<QVariant> m_itemData; //!< Stores item data.
    TreeItem *m_parentItem; //!< Stores pointer to parent item.
};

#endif // PATIENTITEMS_H
