#ifndef SERIESWATCHERDIALOG_H
#define SERIESWATCHERDIALOG_H
#include "qisqlDllCheck.h"
#include <QDialog>
#include <QSqlDatabase>

class QLabel;
class QSqlDatabase;

/*!
 * \class SeriesWatcherDialog
 * \ingroup SQLModule
 * \brief This dialog is used to watch progress on motion correction for series.
 * \note This will probably be refactored to use the new SQL service at some future point.
 */
class QISQL_EXPORT SeriesWatcherDialog : public QDialog
{
	Q_OBJECT
public:

	/*!
	 * \brief Status enumeration for the watcher.
	 */
	enum class WatcherStatus {
		DONTLOAD = 0,
		MCFINISHED = 1,
		LOADORIGINAL = 2,
		UNKNOWN = 3
	};
	Q_ENUM(WatcherStatus)
	explicit SeriesWatcherDialog(QString seriesUID, QString jobType, QStringList dbSettings, QWidget *parent = nullptr);

signals:

public slots:

private:
	void subscribeToDBNotification(QString jobType);
	QString seriesUID_; //!< Stores the series UID.
	QString jobType_; //!< Stores the job type.
	QString jobStatus_; //!< Stores the job status.
	QStringList dbSettings_; //!< Stores the DB settings.
	QLabel *statusLabel; //!< Stores the status label UI component pointer.
	QSqlDatabase db; //!< Stores the DB object.
};

#endif // SERIESWATCHERDIALOG_H
