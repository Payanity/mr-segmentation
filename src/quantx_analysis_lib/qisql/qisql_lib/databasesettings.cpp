#include "databasesettings.h"

#include <QSettings>

#include "databasehelper.h"
#include "simplecrypt.h"
#include <stdexcept>

#define useDebug 0
#if useDebug
#include <QDebug>
void printSettings( const DatabaseSettings& databaseSettings );
#endif

namespace {
    constexpr auto settingDriverString = "database/databaseType";
    constexpr auto settingHostNameString = "database/hostName";
    constexpr auto settingDatabaseNameString = "database/databaseName";
    constexpr auto settingUserNameString = "database/userName";
    constexpr auto settingPasswordString = "database/password";

    constexpr auto defaultDriverValue = "QSQLITE";
    constexpr auto defaultHostNameValue = "";
    constexpr auto defaultDatabaseNameValue = "quantx.db";
    constexpr auto defaultUserNameValue = "";
    constexpr auto defaultPasswordValue = "";
}

/*!
 * \brief Default constructor for database settings object.
 */
DatabaseSettings::DatabaseSettings()
{
	initializeDBSettingsList();
}

/*!
 * \brief Constructor that takes a QStringLlist reference param to initialize all the properties.
 * \param inSettings The QStringList of init params.
 */
DatabaseSettings::DatabaseSettings(const QStringList& inSettings)
{
	setDBSettingsList(inSettings);
}

void DatabaseSettings::setDBSettingsList(const QStringList& inDBSettings)
{
	initializeDBSettingsList();
	for(auto i = 0; i < inDBSettings.size(); ++i){
		dbSettings[SettingName(i)] = inDBSettings[i];
	}
}

/*!
 * \brief Constructor that takes a QSettings reference param to init the properties.
 * \param settings The QSettings object of init params.
 */
DatabaseSettings::DatabaseSettings( const QSettings& settings )
{
	initializeDBSettingsList();
	readFromSettings( settings );
}

/*!
 * \brief Constructor overload that takes the individual init params as separate string args.
 * \param inType The DB driver type.
 * \param inHost The DB hostname.
 * \param inDbName The DB name.
 * \param inUserName The DB username.
 * \param inPassword The DB password that maps to the username.
 * \param inPort The port to use for the Postgres or other future non-sqlite DB types.
 */
DatabaseSettings::DatabaseSettings(const QString& inType, const QString& inHost, const QString& inDbName, const QString& inUserName, const QString& inPassword, const QString& inPort)
{
	initializeDBSettingsList();

	setDatabaseDriverType(inType);
	setHostName(inHost);
	setDatabaseName(inDbName);
	setUserName(inUserName);
	setPassword(inPassword);
	setPort(inPort);
}

/*!
 * \brief Setter for the hostname property for the DB.
 * \param hostName Hostname in string form used to set it.
 */
void DatabaseSettings::setHostName( const QString& hostName )
{
	if( hostName.contains( ':' ) ) {
		auto hostNamePortPair = hostName.split( ':' , QString::SplitBehavior::SkipEmptyParts );
		if( hostNamePortPair.size() == 2 ) {
			setSetting( SettingName::host, hostNamePortPair[0] );
			setSetting( SettingName::port, hostNamePortPair[1] );
		}
	} else {
		setSetting( SettingName::host, hostName );
	}
}

/*!
 * \brief Populate the settings after construction, using a QSettings param.
 * \param settings The QSettings object that contains settings to be read for setting up the DB connection.
 */
void DatabaseSettings::readFromSettings( const QSettings& settings )
{
	setDatabaseDriverType( settings.value( settingDriverString, defaultDriverValue ).toString() );
	setHostName( settings.value( settingHostNameString, defaultHostNameValue ).toString() );
	setDatabaseName( settings.value( settingDatabaseNameString, defaultDatabaseNameValue ).toString() );
	setUserName( settings.value( settingUserNameString, defaultUserNameValue ).toString() );

	SimpleCrypt passwordCrypt( DatabaseHelper::DatabasePasswordKey );
	setPassword( passwordCrypt.decryptToString( settings.value( settingPasswordString, defaultPasswordValue ).toString() ) );

#if useDebug
	printSettings( *this );
#endif
}

/*!
 * \brief Empty out and reset (or populate with blanks) the DatabaseSettings::dbSettings property.
 */
void DatabaseSettings::initializeDBSettingsList()
{
	dbSettings.clear();
}

#if useDebug
void printSettings( const DatabaseSettings& databaseSettings )
{
	qDebug() << "Current Database Settings:";
	qDebug() << "Database Driver Type:" << databaseSettings.databaseDriverType();
	qDebug() << "HostName:" << databaseSettings.hostName();
	qDebug() << "Database Name:" << databaseSettings.databaseName();
	qDebug() << "User Name:" << databaseSettings.userName();
	qDebug() << "Password:" << databaseSettings.password();
	qDebug() << "Port:" << databaseSettings.port();
}
#endif

QStringList DatabaseSettings::getDBSettingsList() const {
	QStringList stringSettings;
	for(SettingName i = SettingName::type; i < SettingName::MAX_SETTINGS; i = SettingName(static_cast<int>(i) + 1) ){
		try {
			stringSettings << dbSettings.at(i);
		} catch( std::out_of_range ) { }
	}
	return stringSettings;
}

