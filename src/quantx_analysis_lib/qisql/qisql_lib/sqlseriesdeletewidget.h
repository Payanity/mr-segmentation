#ifndef SQLSERIESDELETEWIDGET_H
#define SQLSERIESDELETEWIDGET_H

#include "qisqlDllCheck.h"
#include <QDialog>
#include "../../quantxdata/quantxdata_lib/quantx.h"
class QTreeWidget;
class QTreeWidgetItem;

/*!
 * \brief The SqlSeriesDeleteWidget class
 * \class SqlSeriesDeleteWidget
 * \ingroup SQLModule
 * \note This will potentially be deprecated at some point, or at least refactored to use the new SQL service.
 * This widget is from the pre-MVC architecture changes, but is used in some places to delete widgets still.
 */
class QISQL_EXPORT SqlSeriesDeleteWidget : public QDialog
{
    Q_OBJECT
public:
	explicit SqlSeriesDeleteWidget(const QStringList &dbSettings, QWidget *parent = nullptr);
    void deleteSeries(const QString& seriesUID, QUANTX::Modality modality);

signals:

public slots:

private:
    QStringList databaseSettings; //!< Stores the DB settings.
    QTreeWidget* treeWidget; //!< Stores pointer to tree widget.
    void deleteStudy(const QString& studyUID);
    void deletePatient(const QString& mrn);

private slots:
    void initializeTreeWidget();
    void itemChecked(QTreeWidgetItem *item, int col);
    void searchChildren(QTreeWidgetItem *item, int col);
    void deleteButtonClicked();

};

#endif // SQLSERIESDELETEWIDGET_H
