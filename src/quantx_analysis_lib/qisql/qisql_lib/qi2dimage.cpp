#include "qi2dimage.h"
#include "qisql.h"
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QtAlgorithms>
#include <QByteArray>
//#include <QProgressDialog>
//#include <QPushButton>
#include <QCoreApplication>
#include <QFuture>
#include <QFutureWatcher>
#include <QtConcurrentRun>
#include <QEventLoop>
//#include <QMessageBox>
#include <QList>
#include <QRgb>
#include <utility>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

/*!
 * \brief Opens a series using params to find the image data in the db.
 * \param uid UID of the series to open.
 * \param qmodality Modality to load, in case there are more than one for the series collection.
 * \param dbSettings Database settings to connect to the db from which to load the image.
 * \param getImageNum Frame to retrieve.
 * \return Number of images in the series.
 * \note This and other such classes will probably be refactored at some point in the future to use the quantx::sql::SqlDatabaseService class.
 */
int Qi2DImage::openSeries(QString uid, QUANTX::Modality qmodality, const QStringList &dbSettings, int getImageNum){
	seriesUID_ = std::move(uid);
	data.clear();
	windowWidth_ = 0;
	windowLevel_ = 0;
	maxPixval_ = 0;
	minPixval_ = 0;
	dx_ = 0.0;
	dy_ = 0.0;
	nx_ = 0;
	ny_ = 0;
	int returnVal = getSeriesData(dbSettings, qmodality, getImageNum);
	QSqlDatabase::removeDatabase("QIopenDatabase");
	return returnVal;
}

/*!
 * \brief Get series data for a given modality and frame / image number.
 * \param dbSettings Database settings for connecting and loading the image.
 * \param qmodality Modality to load.
 * \param getImageNum Image number to retrieve.
 * \return Number of images retrieved.
 */
int Qi2DImage::getSeriesData(const QStringList &dbSettings, QUANTX::Modality qmodality, int getImageNum){
	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	QSqlDatabase db = qisql->createDatabase("QIopenDatabase");
#if useDebug
	    qDebug()<<"Opening DB (Qi2DImage)";
#endif
		db.open();
	//bool ok = db.open();  //if ok is false, we should error and quit
	//if(!ok){
	//int retries = 0;
	//while( (!ok) && (retries++ < 5) ){
	//    QThread::sleep(1); // Wait one second, and retry
	//}
	//    QMessageBox::critical(0,"Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
	//}
	QString sqlImageTable = qisql->imageTable(qmodality);
	QString sqlSeriesTable = qisql->seriesTable();

	QSqlRecord seriesRecord  = db.record(sqlSeriesTable);
	QSqlRecord imageRecord   = db.record(sqlImageTable);
	QSqlRecord uidRecord = seriesRecord;
	uidRecord.setValue("series_uid",seriesUID_);
	for(int i=uidRecord.count()-1;i>=0;i--)
		if(uidRecord.isNull(i))
			uidRecord.remove(i);

	QString sqlStatement;
	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));

	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,uidRecord,false);
#if useDebug
	qDebug() << "qi2dimage (series) sql statement: " << sqlStatement;
#endif
	sqlQuery->exec(sqlStatement);
	if(!sqlQuery->next())
		return -1; //no records match series UID
	//if(sqlQuery->size() == 0)
	//    return -1; //no records match series UID
	//if(sqlQuery->size() > 1)
	//    return -2; //more than one record with series UID requested

	sqlQuery->first();
	seriesRecord = sqlQuery->record(); //Set series record data to result instead of empty
	mrn_ = seriesRecord.value("mrn").toString();
	seriesDesc_ = seriesRecord.value("series_desc").toString();
	modality_ = modalityFromString(seriesRecord.value("modality").toString());
	// New query to get the image data for this series
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
#if useDebug
	qDebug() << "qi2dimage (image) sql statement: " << sqlStatement;
#endif

	//cannot use QProgressDialog because of Wt
	/*
	QProgressDialog *fileOpenProgress = new QProgressDialog(QString("Opening ") + modality + QString(" Case (time depends on speed of disk or network)"),"Please Wait...",0,100,parent);
	fileOpenProgress->setWindowTitle("Opening Image...");
	QPushButton* abortProgressButton = new QPushButton("Please Wait...");
	fileOpenProgress->setCancelButton(abortProgressButton);
	fileOpenProgress->setWindowModality(modalProgress);
	abortProgressButton->setEnabled(false);
	fileOpenProgress->setMinimumDuration(0);
	fileOpenProgress->setMaximum(100);
	fileOpenProgress->setValue(1);
	fileOpenProgress->show();
	qApp->processEvents();
	*/

	//Run the SQL Query in a separate thread so that the UI still updates
	QEventLoop sqlWaitLoop;
	QFuture<bool> sqlFuture;
	QFutureWatcher<bool> sqlWatcher;
#if QT_VERSION >= 0x050700
	QObject::connect(&sqlWatcher, QOverload<>::of(&QFutureWatcher<bool>::finished), &sqlWaitLoop, &QEventLoop::quit);
#else
	QObject::connect(&sqlWatcher,SIGNAL(finished()),&sqlWaitLoop,SLOT(quit()));
#endif
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlWatcher.setFuture(sqlFuture);
	sqlWaitLoop.exec();

	int index_time          = imageRecord.indexOf("image_time");
    int index_date          = imageRecord.indexOf("image_date");
    int index_width         = imageRecord.indexOf("width");
	int index_height        = imageRecord.indexOf("height");
	int index_pixelsize_x   = imageRecord.indexOf("pixelsize_x");
	int index_pixelsize_y   = imageRecord.indexOf("pixelsize_y");
	int index_windowWidth   = imageRecord.indexOf("window_width");
	int index_windowCenter  = imageRecord.indexOf("window_center");
	int index_minPixval     = imageRecord.indexOf("min_pixval");
	int index_maxPixval     = imageRecord.indexOf("max_pixval");

	int index_image_data    = imageRecord.indexOf("image_data");
	int index_scanOptions   = imageRecord.indexOf("scan_options");
	int index_viewPosition  = imageRecord.indexOf("view_position");
	int index_imageLaterality = imageRecord.indexOf("image_laterality");
	int index_patientOrientation = imageRecord.indexOf("patient_orientation");
	int index_photometricInterpretation = imageRecord.indexOf("photometric_interpretation");
	int index_manufacturer  = imageRecord.indexOf("manufacturer");
	int index_modelName     = imageRecord.indexOf("model_name");
	int index_numberOfFrames      = imageRecord.indexOf("number_of_frames");

	numberOfFrames_ = 0;
	int frameSelect = 0;

	QByteArray imageData;
	quint16 *imgptr,*dataptr;
	quint8 *imgptr8bit;

	sqlQuery->first();
	if(index_numberOfFrames > 0){
		numberOfFrames_ = sqlQuery->value(index_numberOfFrames).toInt();
		if(numberOfFrames_ > 1 && getImageNum < numberOfFrames_){
			frameSelect = getImageNum;
			getImageNum = 0;
#if useDebug
			qDebug() << "frameSelect:" << frameSelect << "spaceBetweenSlices:" << seriesRecord.value("space_between_slices").toFloat();
#endif
		}
	}
	for(int i=0;i<getImageNum;i++)
		if(!sqlQuery->next())
			return -1; //Requested an image number that does not exist
	//fileOpenProgress->setValue(50);
	time_ =  sqlQuery->value(index_time).toTime();
    date_ = sqlQuery->value(index_date).toDate();
    windowWidth_ = sqlQuery->value(index_windowWidth).toUInt();
	windowLevel_ = sqlQuery->value(index_windowCenter).toUInt();
	minPixval_ = static_cast<quint16>(sqlQuery->value(index_minPixval).toUInt());
	maxPixval_ = static_cast<quint16>(sqlQuery->value(index_maxPixval).toUInt());
	dx_ = sqlQuery->value(index_pixelsize_x).toFloat();
	dy_ = sqlQuery->value(index_pixelsize_y).toFloat();
	nx_ = sqlQuery->value(index_width).toInt();
	ny_ = sqlQuery->value(index_height).toInt();
	if(index_scanOptions >= 0)
		scanOptions_ = sqlQuery->value(index_scanOptions).toString().trimmed().split('\\');
	else
		scanOptions_.clear();

	viewPosition_ = sqlQuery->value(index_viewPosition).toString();
	imageLaterality_ = sqlQuery->value(index_imageLaterality).toString();
	patientOrientation_ = sqlQuery->value(index_patientOrientation).toString();
	photometricInterpretation_ = sqlQuery->value(index_photometricInterpretation).toString();
	manufacturer_ = sqlQuery->value(index_manufacturer).toString();
	modelName_ = sqlQuery->value(index_modelName).toString();

	//For new mammograms - a quick fix that needs to be handled properly in the future
	if(dx_ <= 0.0f)
		dx_ = 0.1f;
	if(dy_ <= 0.0f)
		dy_ = 0.1f;

	//Fix for images that have multiple window/level values
	if(windowWidth_ <= 0)
		windowWidth_ = sqlQuery->value(index_windowWidth).toString().split('\\').first().toUInt();
	if(windowLevel_ <= 0)
		windowLevel_ = sqlQuery->value(index_windowCenter).toString().split('\\').first().toUInt();

	//I overlooked planar configurations. We'll have to add this to the DB in the future
	int planarConfiguration = 0;
	if(manufacturer_.contains("Philips") && photometricInterpretation_.contains("RGB",Qt::CaseInsensitive)){
#if useDebug
		qDebug() << "Found RGB image from manufacturer Philips: assuming planar configuration is 0";
#endif
		planarConfiguration = 0;
	}
	else if(manufacturer_.contains("ATL") && photometricInterpretation_.contains("RGB",Qt::CaseInsensitive)){
#if useDebug
		qDebug() << "Found RGB image from manufacturer ATL: assuming planar configuration is 1";
#endif
		planarConfiguration = 1;
	}


	data.clear();
	data.resize(ny_);
	//if(dbSettings.at(0) == QString("QPSQL"))
	//    imageData = sqlQuery->value(index_image_data).toByteArray();
	//else
	    imageData = qUncompress(sqlQuery->value(index_image_data).toByteArray());

		if(photometricInterpretation_.contains("RGB",Qt::CaseInsensitive)){
#if useDebug
		qDebug() << "Found RGB image with planar configuration:" << planarConfiguration;
#endif

		for (int y=0;y<ny_;y++)
			data.at(y) = (std::vector<quint16>(nx_ * 2));


		if(numberOfFrames_ > 1) {
			imgptr8bit = reinterpret_cast<quint8 *>(imageData.data());
			int numPixels = nx_ * ny_;
			QRgb *pixel;
			data.resize(ny_ * numberOfFrames_);
			for (int y=1;y<ny_*numberOfFrames_;y++){
				data.at(y) = (std::vector<quint16>(nx_ * 2));
				pixel = reinterpret_cast<QRgb *>(data[y].data());
				for(int x=0;x<nx_;x++){
					if(planarConfiguration == 0)
						pixel[x] = qRgb(imgptr8bit[(3 * nx_ * y) + (3 * x)],imgptr8bit[(3 * nx_ * y) + (3 * x) + 1],imgptr8bit[(3 * nx_ * y) + (3 * x) + 2]);
					else if(planarConfiguration == 1)
						pixel[x] = qRgb(imgptr8bit[(nx_ * y) + x],imgptr8bit[(nx_ * y) + numPixels + x],imgptr8bit[(nx_ * y) + 2*numPixels + x]);
				}
			}
		}
		else if(imageData.size() == (nx_ * ny_ * 3)) {
			imgptr8bit = reinterpret_cast<quint8 *>(imageData.data());
			int numPixels = nx_ * ny_;
			QRgb *pixel;
			for(int y=0;y<ny_;y++){
				pixel = reinterpret_cast<QRgb *>(data[y].data());
				for(int x=0;x<nx_;x++){
					if(planarConfiguration == 0)
						pixel[x] = qRgb(imgptr8bit[(3 * nx_ * y) + (3 * x)],imgptr8bit[(3 * nx_ * y) + (3 * x) + 1],imgptr8bit[(3 * nx_ * y) + (3 * x) + 2]);
					else if(planarConfiguration == 1)
						pixel[x] = qRgb(imgptr8bit[(nx_ * y) + x],imgptr8bit[(nx_ * y) + numPixels + x],imgptr8bit[(nx_ * y) + 2*numPixels + x]);
					else{ //if(planarConfiguration == 0) and data is big endian - should not need this anymore
						int baseLocation = (3 * nx_ * y) + (3 * x);
						if(baseLocation % 2)
							pixel[x] = qRgb(imgptr8bit[baseLocation - 1],imgptr8bit[baseLocation + 2],imgptr8bit[baseLocation + 1]);
						else
							pixel[x] = qRgb(imgptr8bit[baseLocation + 1],imgptr8bit[baseLocation],imgptr8bit[baseLocation + 3]);
					}
				}
			}
		}
	}
	else if(numberOfFrames_ > 1) {
			data.resize(ny_ * numberOfFrames_);
		for (int y=0;y<ny_*numberOfFrames_;y++)
			data.at(y) = (std::vector<quint16>(nx_));
		if(imageData.size() == (nx_ * ny_ * 2) * numberOfFrames_) {
			imgptr = reinterpret_cast<quint16 *>(imageData.data());
			for(int y=0;y<ny_*numberOfFrames_;y++){
				dataptr=data[y].data();
				for(int x=0;x<nx_;x++){
					dataptr[x] = imgptr[(nx_ * y) + x];
				}
			}
			windowWidth_ = 36000;
			windowLevel_ = 35000;
		} else if (imageData.size() == (nx_ * ny_) * numberOfFrames_) { // 8-bit image
			imgptr8bit = reinterpret_cast<quint8 *>(imageData.data());
			for(int y=0;y<ny_;y++){
				for(int y=0;y<ny_*numberOfFrames_;y++){
					dataptr=data[y].data();
					for(int x=0;x<nx_;x++){
						dataptr[x] = imgptr8bit[(nx_ * y) + x];
					}
				}
			}
			windowWidth_ = 220;
			windowLevel_ = 127;
		}
	}
	else{
		data.resize(ny_);
		for (int y=0;y<ny_;y++)
			data.at(y) = (std::vector<quint16>(nx_));
		if(imageData.size() == (nx_ * ny_ * 2)) {
			imgptr = reinterpret_cast<quint16 *>(imageData.data());
			for(int y=0;y<ny_;y++){
				dataptr=data[y].data();
				for(int x=0;x<nx_;x++){
					dataptr[x] = imgptr[(nx_ * y) + x];
				}
			}
		} else if (imageData.size() == (nx_ * ny_)) { // 8-bit image
			imgptr8bit = reinterpret_cast<quint8 *>(imageData.data());
			for(int y=0;y<ny_;y++){
				dataptr=data[y].data();
				for(int x=0;x<nx_;x++){
					dataptr[x] = imgptr8bit[(nx_ * y) + x];
				}
			}
		}
	}

	if(modality_ == QUANTX::Modality::US){

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// THIS CODE SECTION PERFORMS A SEARCH FOR MARKERS (ruler burned into image) THAT INDICATE THE PIXEL SIZE  //
		// Right now, all of these locations are hard-coded per manufacturer but should probably be per device???  //
		// Is there a better way to do this?                                                                       //
		// Does pixel size matter if measurements can be taken on the acquisition device?                          //
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if(manufacturer_.contains("ATL")){
			QVector<QPoint> locs;
			QVector<int> yvals;
			for(int i=0;i<2;i++)
				for(int j=0;j<2;j++)
					locs << QPoint(i,j);
			for(int y=54;y<426;y++){
				bool skip=false;
				if(photometricInterpretation_.contains("RGB",Qt::CaseInsensitive)){
					for(const auto & point : std::as_const(locs))
						if(reinterpret_cast<QRgb *>(data[y+point.y()].data())[555+point.x()] != qRgb(176,176,176))
							skip = true;
				}
				else{
					for(const auto & point : std::as_const(locs))
						if(data[y+point.y()][555+point.x()] != 176)
							skip = true;
				}
				if(!skip)
					yvals<<y;
			}
			dx_ = dy_ = static_cast<float>(static_cast<int>((10000*yvals.size())/(2*(yvals.last()-52))))/1000;
#if useDebug
			qDebug() << "yvals:" << yvals;
			qDebug() << QString("Pixel Size: %1um").arg(1000 * static_cast<double>(dx_));
#endif
		}
		else if(manufacturer_.contains("Philips")){
			QVector<QPoint> locs;
			QVector<int> yvals;
			for(int i=0;i<3;i++)
				for(int j=0;j<3;j++)
					locs << QPoint(i,j);
			for(int y=127;y<730;y++){
				bool skip=false;
				if(photometricInterpretation_.contains("RGB",Qt::CaseInsensitive)){
					for(const auto & point : std::as_const(locs))
						if(reinterpret_cast<QRgb *>(data[y+point.y()].data())[916+point.x()] != qRgb(255,255,255))
							skip=true;
				}
				else{
					for(const auto & point : std::as_const(locs))
						if(data[y+point.y()][916+point.x()] != 254)
							skip=true;
				}
				if(!skip)
					yvals<<y;
			}
			dx_ = dy_ = static_cast<float>(static_cast<int>((100000*yvals.size())/(2*(yvals.last()-124))))/10000;
#if useDebug
			qDebug() << QString("Pixel Size: %1um").arg(1000 * static_cast<double>(dx_));
#endif
		}
	}

	  //fileOpenProgress->setValue(fileOpenProgress->maximum());

	  int numItems = 1;
	  while(sqlQuery->next())
		  numItems++;
#if useDebug
	    qDebug()<<"Closing DB Qi2DImage";
#endif
		sqlQuery.reset();
		db.close();
	  return numItems + getImageNum; //return the number of images in the series
}

/*!
 * \brief This method returns a single vector of data for the whole image, rather than vector of vectors or list of lists, etc.
 * \return
 */
std::vector<quint16> Qi2DImage::linearDataVector(){// instead of a list of vectors, this converts the image data to a single vector
	std::vector<quint16> linearvector;
	for(const auto & row : std::as_const(data))
		linearvector.insert(std::end(linearvector), std::begin(row), std::end(row));
	return linearvector;
}

