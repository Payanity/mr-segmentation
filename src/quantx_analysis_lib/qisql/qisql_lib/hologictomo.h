#ifndef HOLOGICTOMO_H
#define HOLOGICTOMO_H

#include <QByteArray>
#include "gdcmImageReader.h"

namespace hologicTomo {

/*!
 * \brief Small struct for containing information about Hologic Tomo scans.
 * \ingroup SQLModule
 */
struct HologicTomoReturn {
    QByteArray imageData; //!< Stores image data.
	float spaceBetweenSlices{}; //!< Stores space between slices.
    quint16 width{}; //!< Stores image width.
    quint16 height{}; //!< Stores image height.
};

HologicTomoReturn getImageData(const gdcm::DataSet &dicomHeader);

}

#endif // HOLOGICTOMO_H
