#include "hologictomo.h"
#include <QDataStream>
#include <QVector>

#ifndef CHARLS_IMEXPORT
#define CHARLS_DLL
#endif
#include "gdcmcharls/interface.h"

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

namespace hologicTomo {

/*!
* \brief Utility function for getting Hologic Tomo info from a DICOM header.
* \param dicomHeader DICOM Header to parse for the Hologic Tomo data.
* \return Hologic Tomo data parsed from the DICOM header.
*/
HologicTomoReturn getImageData(const gdcm::DataSet &dicomHeader)
{
	//gdcm::File &file = imagereader.GetFile();
	//const gdcm::DataSet &dicomHeader = file.GetDataSet();
	const gdcm::DataElement &imageSequence = dicomHeader.GetDataElement( gdcm::Tag(0x7e01,0x1010) );
	auto sqImageSequence = imageSequence.GetValueAsSQ();
#if useDebug
	qDebug() << "value length:" << imageSequence.GetValue().GetLength();
	qDebug() << "item sequence pointer" << imageSequence.GetValueAsSQ();
	qDebug() << "fragment sequence pointer" << imageSequence.GetSequenceOfFragments();
	//qDebug() << "List of tags in Sequence:";
	//for(auto item : sqImageSequence->Items)
	//    qDebug() << item.GetTag().PrintAsPipeSeparatedString().c_str();
#endif
	gdcm::SequenceOfItems::ItemVector itemVector = sqImageSequence->Items;
	QByteArray jpegLSdata;
	QVector<quint32> dataSizes;
	for(const auto & item : itemVector){
		const gdcm::DataSet& dataSet = item.GetNestedDataSet();
		// We should check if the DICOM tag exists first.
		// Also, a fallback method would be to check for the 0x1011 tag, which contains lower resolution images
		const gdcm::DataElement &dicomPixelData = dataSet.GetDataElement( gdcm::Tag(0x7e01,0x1012) );
		jpegLSdata.append( dicomPixelData.GetByteValue()->GetPointer(), dicomPixelData.GetVL() );
		dataSizes << dicomPixelData.GetVL();
	}
#if useDebug
	qDebug() << "Data Element Lengths:" << dataSizes;
	qDebug() << "ByteArray length:" << jpegLSdata.length();
#endif

	// First, let's read the header information
	QByteArray creatorInfo(16, 0), versionInfo(4, 0);
	quint16 numberOfFrames, bytesOfHeaderData; // bytesOfHeaderData is the first dataSize element - 16 (size of creatorInfo)
	quint16 cols, colsPadding, rows, rowsPadding, bitsStored, bitsStoredPadding, near, nearPadding;
	QVector<QVector<float> > locationArray;

	QDataStream ds(&jpegLSdata, QIODevice::ReadOnly);
	ds.setByteOrder(QDataStream::LittleEndian);
	ds.setFloatingPointPrecision(QDataStream::SinglePrecision);

	ds.readRawData(creatorInfo.data(), 16);
	ds.readRawData(versionInfo.data(), 4);
	ds >> numberOfFrames >> bytesOfHeaderData >> cols >> colsPadding >> rows >> rowsPadding >> bitsStored >> bitsStoredPadding >> near >> nearPadding;

	for(auto i = 0; i < numberOfFrames; ++i){
		QVector<float> lv(6);
		for(auto i = 0; i < 6; ++i)
			ds >> lv[i];
		locationArray << lv;
	}
#if useDebug
	QByteArray cInfo = creatorInfo;
	cInfo.replace(0, ' ');
	for(const auto &v : std::as_const(locationArray))
		qDebug() << "ID" << v[3] << "Location" << v[0] << "zero" << v[1] << "zero" << v[2] << "one" << v[4] << "zero" << v[5];
	qDebug() << "Creator Info:" << cInfo << "Version:" << versionInfo;
	qDebug() << rows << "rows," << cols << "cols," << numberOfFrames << "frames";
#endif

	for(auto i = 1; i < dataSizes.size() - 1; ++i)
		ds.skipRawData(dataSizes.at(i));
	ds.skipRawData(dataSizes.last() - 1024);
	QVector<quint32> frameStart(numberOfFrames + 1);
	for(auto i = 0; i < numberOfFrames + 1; ++i){
		ds >> frameStart[i];
	}

	QByteArray jpegLSHeader;
	QDataStream jh(&jpegLSHeader, QIODevice::WriteOnly);
	jh << quint8(0xFF) << quint8(0xD8);       // Start of image (SOI) marker
	jh << quint8(0xFF) << quint8(0xF7);       // Start of JPEG-LS frame (SOF55) marker
	jh << quint8(0x00) << quint8(0x0B);       // Length of SOF55 marker segment = 11 bytes
	jh << static_cast<quint8>(bitsStored);    // Precision (10 bit images = 0x0A)
	jh << quint16(rows) << quint16(cols);     // Image Resolution
	jh << quint8(0x01);                       // Number of components in frame (1)
	jh << quint8(0x01);                       // Component ID (first and only frame component)
	jh << quint8(0x11);                       // Sub-sampling: H1 = 1, V1 = 1
	jh << quint8(0x00);                       // Tq = 0 (this should always be zero)
	jh << quint8(0xFF) << quint8(0xDA);       // Start of scan (SOS) marker
	jh << quint8(0x00) << quint8(0x08);       // Length of SOS marker segment = 8 bytes
	jh << quint8(0x01);                       // Number of components in this scan (1)
	jh << quint8(0x01);                       // Component ID (first and only scan component)
	jh << quint8(0x00);                       // Mapping table index, 0 = no mapping table
	jh << static_cast<quint8>(near);          // NEAR value for near-lossless image compression, 0 = lossless
	jh << quint8(0x00);                       // Interleave mode, 0 = non-interleaved
	jh << quint8(0x00);                       // Point transform: Al = 0, Ah = 0 (no point transform)

	QByteArray jpegLSFooter;
	QDataStream jf(&jpegLSFooter, QIODevice::WriteOnly);
	jf << quint8(0xFF) << quint8(0xD9);                       // End of image (EOI) marker

	QByteArray rawImageData;
	for(auto i = 0; i < numberOfFrames; ++i){
		QByteArray jpegLSImage;
		jpegLSImage.append(jpegLSHeader).append(jpegLSdata.mid(frameStart.at(i), frameStart.at(i+1) - frameStart.at(i))).append(jpegLSFooter); // This creates a valid jpeg-LS image;
		//auto bpp = (bitsStored + 7) / 8; // Bytes per pixel
		QByteArray decodedImage;
		decodedImage.resize( rows * cols * ((bitsStored + 7) / 8) );
		//QByteArray errorText(256, 0);
#if useDebug
		auto result =
#endif
		JpegLsDecode(decodedImage.data(), decodedImage.size(), jpegLSImage.data(), jpegLSImage.size(), nullptr);
#if useDebug
		qDebug() << "JpegLsDecode Result:" << int(result);
		if(int(result))
			qDebug() << "Error Text:" << errorText;
#endif
		rawImageData.append(decodedImage);
	}
#if useDebug
	qDebug() << "Raw Image Data Size:" << rawImageData.size();
#endif

	auto spaceBetweenSlices = qAbs(locationArray.last().first() - locationArray.first().first()) / (numberOfFrames - 1);

	HologicTomoReturn rdata;
	rdata.imageData = rawImageData;
	rdata.spaceBetweenSlices = spaceBetweenSlices;
	rdata.width = cols;
	rdata.height = rows;

	return rdata;
}

}
