#include "qiseriesinfo.h"

#include <utility>

/*!
 * \brief Constructor that takes a UID.
 * \param uid UID of the series to create an info object for.
 */
QiSeriesInfo::QiSeriesInfo(QString uid) :
    seriesUID_(std::move(uid))
{ }

/*!
 * \brief Setter for modality.
 * \param seriesmodality Modality of the series.
 * \see QUANTX::Modality
 * This method takes a string representation and then maps the corresponding QUANTX::Modality and stores it.
 */
void  QiSeriesInfo::setModality(const QString &seriesmodality)
{
    if(seriesmodality == "MG")
        modality_ = QUANTX::MAMMO;
    else if(seriesmodality == "MG3D")
        modality_ = QUANTX::MG3D;
    else if(seriesmodality == "US")
        modality_ = QUANTX::US;
    else if(seriesmodality == "US3D")
        modality_ = QUANTX::US3D;
    else if(seriesmodality == "MR")
        modality_ = QUANTX::MRI;
    else if(seriesmodality == "OT")
        modality_ = QUANTX::OTHER;

    else
        modality_ = QUANTX::NOIMAGE;
}

/*!
 * \brief Getter for the modality as a string representation.
 * \return String representation of the modality as mapped against the privately stored QUANTX::Modality.
 */
QString QiSeriesInfo::modalityString() const
{
    switch (modality_)
    {

    case QUANTX::MRI :
        return QString("MR");

    case QUANTX::MAMMO :
        return QString("MG");

    case QUANTX::MG3D :
        return QString("MG3D");

    case QUANTX::US :
        return QString("US");

    case QUANTX::US3D :
        return QString("US3D");

    case QUANTX::OTHER :
    default :
        return QString("OT");

    }
}
