#ifndef DATABASEHELPER_H
#define DATABASEHELPER_H

#include "qisqlDllCheck.h" // we could make this a header-only library instead of exporting it
#include "simplecrypt.h"
#include <QDataStream>

struct KnownCase;

/*!
 * \brief Utility class with static methods for interacting with data I/O for the database
 * \ingroup SQLModule
 */
class DatabaseHelper
{
public:
	template <typename T> static T decodeBinary(QByteArray buf);
	template <typename T> static T decodeBinary(std::vector<unsigned char> in);
	template <typename T> static T cryptDecodeBinary(std::vector<unsigned char> in, quint64 key);
	template <typename T> static QByteArray encodeBinary(T in);
	template <typename T> static QByteArray cryptEncodeBinary(T in, quint64 key);
	QISQL_EXPORT static QByteArray encodeKnownCase(const QVector<const KnownCase *> &in, int index);
	QISQL_EXPORT static QByteArray cryptEncodeKnownCase(const QVector<const KnownCase *>& in, int index, quint64 key = SimCaseKey);
	QISQL_EXPORT static QVector<const KnownCase *> decodeKnownCase(std::vector<unsigned char> in);
	QISQL_EXPORT static QVector<const KnownCase *> cryptDecodeKnownCase(std::vector<unsigned char> in, quint64 key = SimCaseKey);
	QISQL_EXPORT static QVector<const KnownCase *> decodeKnownCaseHelper(QByteArray buf);
	QISQL_EXPORT static QString getLoggedInUserName();
	QISQL_EXPORT static QStringList getUserGroups(const QString& inUserName);
	QISQL_EXPORT static QString getTempSQLiteDatabasePath(const QString& databaseName);
	QISQL_EXPORT static QString createSessionId();
	QISQL_EXPORT static QDateTime dateTimeFromSessionId( const QString& sessionId );

	/*!
	 * \brief The possible enum values for various SimpleCrypt encryption keys.
	 */
	enum Keys : quint64 {
		FeatureKey          = Q_UINT64_C(0x609d4e44568a23f8),
		SegOutlineKey       = Q_UINT64_C(0xace98387dee4ac80),
		SegFilledKey        = Q_UINT64_C(0x02394acebb093aaf),
		RawDataKey          = Q_UINT64_C(0xf898ea0873848acd),
		SimCaseKey          = Q_UINT64_C(0xc89ae2281000ffe8),
		DatabasePasswordKey = Q_UINT64_C(0x32dac01a6fd0b632)
	};

	/*!
	 * \brief The possible enum values for types of DB connection in use.
	 */
	enum class ConnectionType
	{
		Postgres,
		SQLite
	};
};

/*!
 * \brief Decode binary data from a QByteArray.
 * \tparam T The output type for the decoder.
 * \param buf The QByteArray to decode from
 * \return The output of type T.
 */
template <typename T>
T DatabaseHelper::decodeBinary(QByteArray buf)
{
	T out;
	QDataStream s(&buf, QIODevice::ReadOnly);
	s.setVersion(QDataStream::Qt_5_6); //Version 17, used from 5.6 on
	s >> out;
	return out;
}

/*!
 * \brief Decode binary data from a QByteArray.
 * \tparam T The output type for the decoder.
 * \param in The std::vector<unsigned char> to decode from.
 * \return The output of type T.
 */
template <typename T>
T DatabaseHelper::decodeBinary(std::vector<unsigned char> in)
{
	QByteArray buf(reinterpret_cast<char*>(in.data()), static_cast<int>(in.size()));
	return decodeBinary<T>(buf);
}
/*!
 * \brief Decode encrypted binary.
 * \tparam T The return type to decode to.
 * \param in The std::vector<unsigned char> to decrypt and decode.
 * \param key The decryption key in quint64 form.
 * \return The decoded, decrypted data.
 */
template <typename T>
T DatabaseHelper::cryptDecodeBinary(std::vector<unsigned char> in, quint64 key)
{
	SimpleCrypt crypto(key);
	crypto.setCompressionMode(SimpleCrypt::CompressionAlways);
	crypto.setIntegrityProtectionMode(SimpleCrypt::ProtectionHash);
	auto buf = crypto.decryptToByteArray(QByteArray(reinterpret_cast<char*>(in.data()), static_cast<int>(in.size())));
	return decodeBinary<T>(buf);
}

/*!
 * \brief Encode binary data.
 * \tparam T The input type to encode binary from.
 * \param in The input.
 * \return A QByteArray of binary data, encoded from the input.
 */
template <typename T>
QByteArray DatabaseHelper::encodeBinary(T in)
{
	QByteArray a;
	QDataStream s(&a, QIODevice::WriteOnly);
	s.setVersion(QDataStream::Qt_5_6); //Version 17, used from 5.6 on
	s << in;
	return a;
}

/*!
 * \brief Encrypt and encode binary data from input.
 * \tparam T Type param of input data to encode and encrypt.
 * \param in Input data.
 * \param key Encryption key in quint64 form.
 * \return QByteArray of the encoded and encrypted data.
 */
template <typename T>
QByteArray DatabaseHelper::cryptEncodeBinary(T in, quint64 key)
{
	SimpleCrypt crypto(key);
	crypto.setCompressionMode(SimpleCrypt::CompressionAlways);
	crypto.setIntegrityProtectionMode(SimpleCrypt::ProtectionHash);
	return crypto.encryptToByteArray(encodeBinary(in));
}

#endif // DATABASEHELPER_H
