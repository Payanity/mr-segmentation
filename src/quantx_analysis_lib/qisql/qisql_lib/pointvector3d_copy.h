#ifndef POINTVECTOR3D_H
#define POINTVECTOR3D_H

#include "../../quantxdata/quantxdata_lib/quantx.h"

// The PointVector3D class may be useful anywhere we need three coordinates and don't want QVector3D

// Set the POINTVECTOR3D_ISNULL_SUPPORT parameter to true if you need isNull() support (perhaps we'll want this in the future?)
#define POINTVECTOR3D_ISNULL_SUPPORT false
/*!
 * \brief This class is meant to be useful as a replacement for QVector3D.
 * \ingroup SQLModule
 * \tparam T Type to use for generating the template class instance.
 */
template <class T>
class PointVector3D {
public:
#if POINTVECTOR3D_ISNULL_SUPPORT
	explicit PointVector3D( T x, T y, T z ) : x_{x}, y_{y}, z_{z}, isNull_{false} {}
	PointVector3D() : x_{}, y_{}, z_{}, isNull_{true} {}
	void setX( const T & x ) { x_ = x; isNull_ = false; }
	void setY( const T & y ) { y_ = y; isNull_ = false; }
	void setZ( const T & z ) { z_ = z; isNull_ = false; }
	constexpr bool isNull() { return isNull_; } // This will test if we used the constructor without any parameters, and have not set x, y, or z
#else
	constexpr PointVector3D( T x = T{}, T y = T{}, T z = T{} ) : x_{x}, y_{y}, z_{z} {} //!< Constructor that brace-initializes the 3 data members of type T.
	void setX( const T & x ) { x_ = x; } //!< Setter for the x property.
	void setY( const T & y ) { y_ = y; } //!< Setter for the y property.
	void setZ( const T & z ) { z_ = z; } //!< Setter for the z property.
	// bool isNull() { return *this == PointVector3D<T>{}; } // This name is improper, because it actually tests for zero instead of null
#endif
	constexpr const T & x() const { return x_; } //!< Getter for the x property.
	constexpr const T & y() const { return y_; } //!< Getter for the y property.
	constexpr const T & z() const { return z_; } //!< Getter for the z property.

	/*!
	 * \brief Equality comparison operator.
	 * \param other The other PointVector3D<T> to compare against.
	 * \return True if equal, false otherwise.
	 */
	constexpr bool operator==( const PointVector3D<T> & other ) const {
		return ( x_ == other.x() ) && ( y_ == other.y() ) && ( z_ == other.z() );
	}

	/*!
	 * \brief Inquality comparison operator.
	 * \param other The other PointVector3D<T> to compare against.
	 * \return False if equal, true otherwise.
	 */
	constexpr bool operator!=( const PointVector3D<T> & other ) const {
		return ( x_ != other.x() ) || ( y_ != other.y() ) || ( z_ != other.z() );
	}

	/*!
	 * \brief Helper method to reorient current instance to a new QUANTX::Orientation.
	 * \param to New orientation to transform to.
	 * \return Reoriented PointVector3D<T>
	 */
	constexpr PointVector3D<T> reoriented(const QUANTX::Orientation to) const { return reorientedprivate(to, false); }

	/*!
	 * \brief Helper method to reorient current instance from another QUANTX::Orientation, and subsequently to a new QUANTX::Orientation.
	 * \param from Orientation we're transforming from.
	 * \param to New orientation to transform to.
	 * \return Reoriented PointVector3D<T>
	 */
	constexpr PointVector3D<T> reoriented(const QUANTX::Orientation from, const QUANTX::Orientation to) const { return reorientedprivate(from, true).reorientedprivate(to, false); }
private:
	T x_; //!< Stores the x property.
	T y_; //!< Stores the y property.
	T z_; //!< Stores the z property.
	constexpr PointVector3D<T> reorientedprivate(const QUANTX::Orientation to, bool reverse) const;
#if POINTVECTOR3D_ISNULL_SUPPORT
	bool isNull_;
#endif
};

/*!
 * \brief Internal impl of the reorientation.
 * \tparam T Type param of the template class.
 * \param to New orientation to use.
 * \param reverse Whether this is a reversal for 'from' conversion cases.
 * \return Converted PointVector3D<T>
 */
template <class T>
constexpr PointVector3D<T> PointVector3D<T>::reorientedprivate(const QUANTX::Orientation to, bool reverse) const
{
#if POINTVECTOR3D_ISNULL_SUPPORT
	if( isNull_ )
		return PointVector3D<T>();
#endif
	using namespace QUANTX;
	Q_ASSERT( to != FULLMIP );
	if(to == XYPLANE)
		return *this;

	T x = x_;
	T y = y_;
	T z = z_;

	if( reverse ) {
		if(to == XZPLANE || to == YZPLANE || to == ZXPLANE || to == SAGITTAL_LEFT || to == SAGITTAL_RIGHT)          std::swap(y, z);
		if(to == ZXPLANE || to == ZYPLANE)                                                                          std::swap(x, z);
	}
	if(to == YZPLANE || to == YXPLANE || to == SAGITTAL_LEFT || to == SAGITTAL_RIGHT)                               std::swap(x, y);
	if( !reverse ) {
		if(to == ZXPLANE || to == ZYPLANE)                                                                          std::swap(x, z);
                if(to == XZPLANE || to == YZPLANE || to == ZXPLANE || to == SAGITTAL_LEFT || to == SAGITTAL_RIGHT)  std::swap(y, z);
	}

	return {x, y, z}; 
}

#if QUANTX_BUILD_WITH_TESTS
Q_DECLARE_METATYPE(PointVector3D<int>)
#endif

// The following enables support for QDebug

#include <QDebugStateSaver>

#ifndef QT_NO_DEBUG_STREAM
template <class T>
QDebug operator<<(QDebug debug, const PointVector3D<T> &pv)
{
	QDebugStateSaver saver(debug);
	debug.nospace() << "PointVector3D<" << typeid(T).name() << ">(" << pv.x() << ", " << pv.y() << ", " << pv.z() << ')';

	return debug;
}
#endif // QT_NO_DEBUG_STREAM

#endif // POINTVECTOR3D_H
