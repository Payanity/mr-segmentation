#ifndef USERACCESSDATABASE_H
#define USERACCESSDATABASE_H

#include "qisqlDllCheck.h"
#include <memory>
#include <QStringList>
#include <Wt/Dbo/Dbo.h>

#define QUANTX_DEFAULT_ACCESS_USER "quantx_default_user"

class QuantXUserAccessDatabasePrivate;
class UserAccess;

/*!
 * \brief The QuantXUserAccessDatabase class
 * \class QuantXUserAccessDatabase
 * \ingroup SQLModule
 * This class allows interaction wwith the User Access database, to do things like manage users and groups and how they relate to QuantX.
 */
class QISQL_EXPORT QuantXUserAccessDatabase
{
public:
	QuantXUserAccessDatabase();
	QuantXUserAccessDatabase(const QStringList& databaseSettings, bool* bSuccess);
	QuantXUserAccessDatabase(const QuantXUserAccessDatabase& other);
	~QuantXUserAccessDatabase();

	std::unique_ptr<Wt::Dbo::SqlConnection> connection() const;
	Wt::Dbo::Session& session() const;
	void createDefaultEntry() const;

	bool allowCurrentUserAccess(QString * reasonForNotAllow) const;

	void addNewUser(const QString& inUserName, const QString& isEnabled = "no", const QString& isAllowed = "no") const;
	void addNewGroup(const QString& inGroupName, const QString& isEnabled = "no", const QString& isAllowed = "no") const;

	void modifyUser(const QString& inUserName, const QString& isEnabled, const QString& isAllowed) const;
	void modifyGroup(const QString& inUserName, const QString& isEnabled, const QString& isAllowed) const;

	void removeUser(const QString& inUserName) const;
	void removeGroup(const QString& inUserName) const;

	QVector<UserAccess> getAllItems() const;

	void clearAllEntries();


private:
	std::unique_ptr<QuantXUserAccessDatabasePrivate> d; //!< Stores the private d-pointer implementation.
};

#endif // USERACCESSDATABASE_H
