#ifndef CHECKSERVERJOBS_H
#define CHECKSERVERJOBS_H
#include "qisqlDllCheck.h"
#include <QStringList>

/*!
 * \brief Utility function for checking server jobs.
 * \ingroup SQLModule
 * \param seriesUID UID of series to check.
 * \param dbSettings DB settings to use for connection.
 * \return Status result.
 */
QISQL_EXPORT QString checkServerJobs(const QString & seriesUID, const QStringList & dbSettings);

#endif // CHECKSERVERJOBS_H
