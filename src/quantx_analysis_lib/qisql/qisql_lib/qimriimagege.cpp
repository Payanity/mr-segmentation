#include "qimriimage.h"
#include "qisql.h"
#include "dicomquery.h"
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlField>
#include <QtAlgorithms>
#include <QByteArray>
//#include <QProgressDialog>
#include <QPushButton>
#include <QCoreApplication>
#include <QFuture>
#include <QFutureWatcher>
#include <QtConcurrentRun>
#include <QEventLoop>
//#include <QMessageBox>

//For internal testing of Query times. Remove prior to deployment.
#define useDebug 0
#if useDebug
#include <QElapsedTimer>
#include <QDebug>
#include <QSqlError>
#endif

#define testFullHeader 0

//#define UCGEIMAGECHECK (seriesUID_.startsWith("1.2.840.113619.2.") || seriesUID_.startsWith("1.2.528.1.1001."))
#define UCGEIMAGECHECK true

QVector<float> getSliceLocations(QVector<QVector3D> & imagePositions, QUANTX::Orientation orientation);

/*!
 * \brief Version of series data getter that is specific to GE scanners.
 * \param dbSettings DB settings to use to connect.
 * \param dynMultiSeriesDesc The list of dynamic multi series descriptions.
 * \return -1 if none found, 1 if successful.
 */
int QiMriImage::getGESeriesData(const QStringList &dbSettings, const QStringList &dynMultiSeriesDesc){
	Q_UNUSED(dynMultiSeriesDesc)
#if useDebug
	QElapsedTimer whole;
	whole.start();
	qDebug() << "Using GeSeriesData Method";
#endif

	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	QSqlDatabase db = qisql->createDatabase("QIopenDatabase");
	bool ok = db.open();  //if ok is false, we should error and quit
	if(!ok){
		int retries = 0;
		while( (!ok) && (retries++ < 5) ){
			QThread::sleep(1); // Wait one second, and retry
		}
		qWarning("Database Error!\nThe specified database could not be opened.\nCheck your database settings and try again.");
#if useDebug
		qDebug() << db.lastError();
		qDebug() << "Database Settings:" << dbSettings;
#endif
	}

	QString sqlImageTable = qisql->imageTable(QUANTX::MRI);
	QString sqlSeriesTable = qisql->seriesTable();

	QSqlRecord seriesRecord  = db.record(sqlSeriesTable);
	QSqlRecord imageRecord   = db.record(sqlImageTable);
	QSqlRecord seriesDescRecord = seriesRecord;
	QSqlRecord uidRecord = seriesRecord;
	uidRecord.setValue("series_uid",seriesUID_);
	for(int i=uidRecord.count()-1;i>=0;i--)
		if(uidRecord.isNull(i))
			uidRecord.remove(i);

	auto imageInfoRecord = imageRecord;
	imageInfoRecord.remove(imageInfoRecord.indexOf("image_data"));

	QString sqlStatement;
	QFuture<bool> sqlFuture;
	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));

	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,uidRecord,false);
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlFuture.waitForFinished();
	if(!sqlQuery->next())
		return -1; //no records match series UID
	//if(sqlQuery->size() == 0)
	//    return -1; //no records match series UID
	//if(sqlQuery->size() > 1)
	//    return -2; //more than one record with series UID requested

	sqlQuery->first();
	seriesRecord = sqlQuery->record(); //Set series record data to result instead of empty
	mrn_ = seriesRecord.value("mrn").toString();
	nt_ = seriesRecord.value("num_temporal_positions").toInt();
	seriesDesc_ = seriesRecord.value("series_desc").toString();

	// Pull Accession Number Here
	studyUID_ = seriesRecord.value("study_uid").toString();
	seriesNums_ << seriesRecord.value("series_number").toString().trimmed();
	QSqlRecord studyUIDRecord;
	studyUIDRecord.append(QSqlField("study_uid", QVariant::String));
	studyUIDRecord.setValue("study_uid", studyUID_);
	QSqlRecord studyAccessionRecord;
	studyAccessionRecord.append(QSqlField("accession_number", QVariant::String));
	auto sqlAccessionStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,qisql->studiesTable(),studyAccessionRecord,false) + QString(' ')
	                           + db.driver()->sqlStatement(QSqlDriver::WhereStatement,qisql->studiesTable(),studyUIDRecord,false);
	QScopedPointer<QSqlQuery> sqlAccessionQuery(new QSqlQuery(db));
	sqlFuture = QtConcurrent::run(sqlAccessionQuery.data(),&QSqlQuery::exec,sqlAccessionStatement);
	sqlFuture.waitForFinished();
	sqlAccessionQuery->first();
	accessionNumber_ = sqlAccessionQuery->value("accession_number").toString();

	// Let's start getting some debugging output
#if useDebug
	qDebug() << "Elapsed Time:" << whole.elapsed();
	qDebug() << "Series Number To Open:" << seriesRecord.value("series_number").toString() << "   Series UID Used:" << seriesUID_;
	qDebug() << "Series Description:" << seriesDesc_;
#endif

	// Find the first postcontrast image
	QSqlRecord imageTestRecord = imageRecord;
	imageTestRecord.setValue("study_uid", studyUID_);
	imageTestRecord.setValue("instance_num", "1 ");
	QSqlField scanOptionsField = imageTestRecord.field("scan_options");
	QSqlField pixelsize_xField = imageTestRecord.field("pixelsize_x");
	QSqlField pixelsize_yField = imageTestRecord.field("pixelsize_y");
	if(seriesUID_.startsWith("1.3.6.1.4.1.14519."))
		imageTestRecord.setValue("trigger_time", "0");
	if(UCGEIMAGECHECK)
		imageTestRecord.setValue("series_uid", seriesUID_);
	for(int i = imageTestRecord.count() - 1; i >= 0; i--)
		if(imageTestRecord.isNull(i))
			imageTestRecord.remove(i);
	if(UCGEIMAGECHECK){
		// Find scan options
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,imageTestRecord,false);
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		if(sqlQuery->first()){
			// I think we need to build a list without scan options, then search for matching scan options in case MP_GEMS gets added :/
#if useDebug
			qDebug() << "Elapsed Time:" << whole.elapsed();
			qDebug() << "Setting scan options:" << sqlQuery->value("scan_options") << ", pixelsize(x,y) = (" << QString::number(sqlQuery->value("pixelsize_x").toDouble(),'f',15) << "," << QString::number(sqlQuery->value("pixelsize_y").toDouble(),'f',15) << ")" <<
			                                                                                          "= (" << sqlQuery->value("pixelsize_x")            << "," << sqlQuery->value("pixelsize_y")            << ")" ;
#endif

			scanOptionsField.setValue(sqlQuery->value("scan_options"));
			pixelsize_xField.setValue(sqlQuery->value("pixelsize_x"));
			pixelsize_yField.setValue(sqlQuery->value("pixelsize_y"));
			imageTestRecord.append(scanOptionsField);
			imageTestRecord.append(pixelsize_xField);
			imageTestRecord.append(pixelsize_yField);
			if (seriesUID_.endsWith(".mc")){
#if useDebug
				qDebug() << "Adding .mc Series UID restriction to search!!!";
#endif
				imageTestRecord.setValue("series_uid", "%.mc");
			} else
				imageTestRecord.remove(imageTestRecord.indexOf("series_uid"));
		}
	}
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
	             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,imageTestRecord,false);
	if(sqlStatement.contains(R"("qi_2d_images"."pixelsize_x" = )")){
		sqlStatement.replace(R"("qi_2d_images"."pixelsize_x" = )", R"(ROUND(CAST("qi_2d_images"."pixelsize_x" AS DECIMAL),5) = ROUND()");
		sqlStatement.replace(R"( AND "qi_2d_images"."pixelsize_y" = )", R"(,5) AND ROUND(CAST("qi_2d_images"."pixelsize_y" AS DECIMAL),5) = ROUND()");
		sqlStatement.append(",5)");
	}
	if (sqlStatement.contains(R"("qi_2d_images"."series_uid" = )")){
		sqlStatement.replace(R"("qi_2d_images"."series_uid" = )",R"("qi_2d_images"."series_uid" LIKE )");
	}
//	if(sqlStatement.contains(R"("scan_options" = )")){
//		sqlStatement.replace(R"("scan_options" = )", R"("scan_options" LIKE )");
//	}
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlFuture.waitForFinished();
#if useDebug
	if (sqlFuture.result() != true)
		qDebug() << "Query error: " << db.lastError();
	else
		qDebug() << "Query result count: " << sqlQuery->size();
#endif
	QStringList firstPostUIDList;
	while(sqlQuery->next())
		firstPostUIDList << sqlQuery->value("series_uid").toString();
#if useDebug
	qDebug() << "Elapsed Time:" << whole.elapsed();
	qDebug().noquote() << "Sql Statement:" << sqlStatement;
	qDebug() << "Possible first post (maybe pre?) detected with series UIDs:" << firstPostUIDList;
#endif

	// Now that we have the first post, let's see if there are any precontrast images available
	QSqlRecord seriesTestRecord = seriesDescRecord;
	seriesTestRecord.setValue("study_uid",seriesRecord.value("study_uid"));
	if (seriesUID_.endsWith(".mc")){
		seriesTestRecord.setValue("series_uid","%.mc");
	}
	for(int i = seriesTestRecord.count() - 1; i >= 0; i--)
		if(seriesTestRecord.isNull(i))
			seriesTestRecord.remove(i);
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
	             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesTestRecord,false)
	             + QString(" ORDER BY CAST(series_number AS INT)");
	if (sqlStatement.contains(R"("qi_series"."series_uid" = )")){
		sqlStatement.replace(R"("qi_series"."series_uid" = )",R"("qi_series"."series_uid" LIKE )");
	}
#if useDebug
	qDebug() <<"Search for pre-contrast series SqlStatement: " << sqlStatement;
#endif
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlFuture.waitForFinished();
	QMap<int,QString> seriesMap;
	QMap<int,QString> seriesMapStrict;
	while(sqlQuery->next()){
		seriesMap.insert(sqlQuery->value("series_number").toInt(), sqlQuery->value("series_uid").toString());
		QString desc = sqlQuery->value("series_desc").toString();
		if( !( desc.contains("PJN") || desc.contains(")-(") ) )
			seriesMapStrict.insert(sqlQuery->value("series_number").toInt(), sqlQuery->value("series_uid").toString());
	}
	int firstPostSeriesNum = 999;
	for(const auto & s : std::as_const(firstPostUIDList))
		firstPostSeriesNum = qMin(firstPostSeriesNum, seriesMap.key(s, 999));
	QString preSeriesTestUID = seriesMap.value( firstPostSeriesNum < 100 ? firstPostSeriesNum - 1 : firstPostSeriesNum - 2 );
#if useDebug
	qDebug() << "Elapsed Time:" << whole.elapsed();
	qDebug() << "First postcontrast series number is" << firstPostSeriesNum;
#endif
	imageTestRecord = imageRecord;
	imageTestRecord.setValue("study_uid", seriesRecord.value("study_uid"));
	imageTestRecord.setValue("series_uid", seriesMap.value(firstPostSeriesNum));
	imageTestRecord.setValue("instance_num", "1 ");
	for(int i = imageTestRecord.count() - 1; i >= 0; i--)
		if(imageTestRecord.isNull(i))
			imageTestRecord.remove(i);
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
	             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,imageTestRecord,false);
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlFuture.waitForFinished();
	sqlQuery->first();
	QSqlRecord firstPostImageRecord = sqlQuery->record();

	bool separatePrecontrast = false;
	imageTestRecord = imageRecord;
	imageTestRecord.setValue("study_uid", seriesRecord.value("study_uid"));
	imageTestRecord.setValue("series_uid", "placeholder");
	imageTestRecord.setValue("pixelsize_x", firstPostImageRecord.value("pixelsize_x"));
	imageTestRecord.setValue("pixelsize_y", firstPostImageRecord.value("pixelsize_y"));
	imageTestRecord.setValue("instance_num", "1 ");
	imageTestRecord.setValue("scan_options", firstPostImageRecord.value("scan_options"));
	for(int i = imageTestRecord.count() - 1; i >= 0; i--)
		if(imageTestRecord.isNull(i))
			imageTestRecord.remove(i);

	if(!preSeriesTestUID.isEmpty())
		imageTestRecord.setValue("series_uid", preSeriesTestUID);
	else{
		if (seriesUID_.endsWith(".mc")){
			imageTestRecord.setValue("series_uid","%.mc");
		} else {
			imageTestRecord.remove(imageTestRecord.indexOf("series_uid"));
		}
	}
	    QString preScanOptions = firstPostImageRecord.value("scan_options").toString();
		preScanOptions.remove("\\MP_GEMS").remove("MP_GEMS\\");
	imageTestRecord.append(QSqlField("image_type", QVariant::String));
	    imageTestRecord.setValue("scan_options", preScanOptions);
		imageTestRecord.setValue("image_type", firstPostImageRecord.value("image_type").toString());
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,imageTestRecord,false);
#if useDebug
		qDebug() << "Search for precontrast series Sqlstatement (pre_replacement):";
		qDebug() << sqlStatement;
#endif
		if(sqlStatement.contains(R"("qi_2d_images"."pixelsize_x" = )")){
			sqlStatement.replace(R"("qi_2d_images"."pixelsize_x" = )", R"(ROUND(CAST("qi_2d_images"."pixelsize_x" AS DECIMAL),5) = ROUND()");
			sqlStatement.replace(R"( AND "qi_2d_images"."pixelsize_y" = )", R"(,5) AND ROUND(CAST("qi_2d_images"."pixelsize_y" AS DECIMAL),5) = ROUND()");
			sqlStatement.replace(R"( AND "qi_2d_images"."instance_num" = )", R"(,5) AND "qi_2d_images"."instance_num" = )");
		}
		if (sqlStatement.contains(R"("qi_2d_images"."series_uid" = )")){
			sqlStatement.replace(R"("qi_2d_images"."series_uid" = )",R"("qi_2d_images"."series_uid" LIKE )");
		}
#if useDebug
		qDebug() << "Search for precontrast series Sqlstatement:";
		qDebug() << sqlStatement;
#endif
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		separatePrecontrast = sqlQuery->next();
		imageTestRecord.setValue("scan_options", firstPostImageRecord.value("scan_options"));

#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
#endif
	if(separatePrecontrast){
		if (sqlQuery->value("series_uid").toString() == firstPostUIDList.first()){
			separatePrecontrast = false;
		} else {
		preSeriesTestUID = sqlQuery->value("series_uid").toString();
#if useDebug
		qDebug() << "Precontrast series found with series UID" << preSeriesTestUID;
		qDebug() << "Postcontrast seriesUID: " << firstPostUIDList;
#endif
		}
#if useDebug
	} else {
		qDebug() << "Postcontrast series is/contains precontrast image";
#endif
	}

	seriesTestRecord = seriesDescRecord;
	seriesTestRecord.setValue("study_uid", seriesRecord.value("study_uid"));
	seriesTestRecord.setValue("series_uid", firstPostImageRecord.value("series_uid"));
	for(int i = seriesTestRecord.count() - 1; i >= 0; i--)
		if(seriesTestRecord.isNull(i))
			seriesTestRecord.remove(i);
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
	             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesTestRecord,false);
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlFuture.waitForFinished();
	sqlQuery->first();
	int firstPostNumTemporalPos = sqlQuery->value("num_temporal_positions").toInt();

	// Let's build a list of all the series in the DCE image
	QMap<int,QString> dceMap;
	if(separatePrecontrast){
		int firstSeriesNum = seriesMap.key(preSeriesTestUID);
		int firstPostSeriesNum = seriesMap.key(firstPostUIDList.first());

		dceMap.insert(firstSeriesNum, preSeriesTestUID);
		if(firstPostSeriesNum > 100 && seriesMap.contains(firstSeriesNum + 1) && ((firstSeriesNum + 1) > firstPostSeriesNum))
			dceMap.insert(firstSeriesNum + 1, seriesMap.value(firstSeriesNum + 1));
	}
#if useDebug
	qDebug() << "Series Map: " << seriesMap;
	qDebug() << "dceMap: " << dceMap;
#endif
	if(!imageTestRecord.contains("series_uid")){
		imageTestRecord.append(QSqlField("series_uid", QVariant::String));
	}
	const auto keys = seriesMapStrict.keys();
	for(const auto & key : keys){
		if(key < firstPostSeriesNum)
			continue;
		if(firstPostSeriesNum < 100 && key > 100)
			break;
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Checking series number" << key;
#endif
		imageTestRecord.setValue("series_uid", seriesMapStrict.value(key));
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,imageTestRecord,false);
		if(sqlStatement.contains(R"("qi_2d_images"."pixelsize_x" = )")){
			sqlStatement.replace(R"("qi_2d_images"."pixelsize_x" = )", R"(ROUND(CAST("qi_2d_images"."pixelsize_x" AS DECIMAL),5) = ROUND()");
			sqlStatement.replace(R"( AND "qi_2d_images"."pixelsize_y" = )", R"(,5) AND ROUND(CAST("qi_2d_images"."pixelsize_y" AS DECIMAL),5) = ROUND()");
			sqlStatement.replace(R"( AND "qi_2d_images"."instance_num" = )", R"(,5) AND "qi_2d_images"."instance_num" = )");
		}
#if useDebug

		qDebug() << "SQL Statement:";
		qDebug().noquote() << sqlStatement;
		qDebug() << "-------------------------------------------------------------------------------------------------------------------";

#endif
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		if(sqlQuery->next()){
			dceMap.insert(key, seriesMapStrict.value(key));
		}
		else if(!UCGEIMAGECHECK)
			break;
	}
#if useDebug
	qDebug() << "Elapsed Time:" << whole.elapsed();
	qDebug() << "List of series in DCE MRI";
	qDebug() << "---------------------------------------------------------------------------------------------------------------------------------";
	for(const auto & key : dceMap.keys())
		qDebug() << "Series Number:" << key << "  Series UID:" << dceMap.value(key) << ((key == firstPostSeriesNum && firstPostNumTemporalPos > 1) ? QString("! Contains %1 Time Points !").arg(firstPostNumTemporalPos).toUtf8().data() : "");
	qDebug() << "---------------------------------------------------------------------------------------------------------------------------------";
#endif

	bool isDynMulti = false;
	bool splitBreasts = false;
	if(dceMap.values().contains(seriesUID_)){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "This series is part of a DCE MRI, preparing to load DCE...";
#endif
#if (QT_VERSION >= QT_VERSION_CHECK(5, 2, 0))
		//seriesUID_ = dceMap.first();
#else
		seriesUID_ = dceMap.values().first();
#endif
		for(const auto & key : dceMap.keys()){
			if (dceMap.value(key) != seriesUID_)
				associatedSeries_ << dceMap.value(key);
		}

		uidRecord.setValue("series_uid",seriesUID_);
		if(seriesUID_.startsWith("1.3.6.1.4.1.14519.")) // TCGA images don't have a good series description
			seriesDesc_ = "DCE MRI";
		nt_ = dceMap.size();
		if(firstPostSeriesNum > 100)
			nt_ /= 2;
		if(firstPostNumTemporalPos > 1)
			nt_ += firstPostNumTemporalPos - 1;
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Modified Series Description:" << seriesDesc_ << "   Number of Time Points Detected:" << nt_;
#endif
		isDynMulti = true;
	}
	if(nt_ < 1)
		nt_ = 1;
	// New query to get the image data for this series
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
	             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);

	auto progressMaximum = nt_ * 250 * 2;
	emit openProgressNewMaximum(progressMaximum);  // (n_TimePts*250*2) is an estimate of twice the number of slices we are going
	                                               // to read. Will be exact if each timept has 250 slices. We only
	                                               // have the n_TimePts after reading the series table. It isn't critical
	                                               // if it isn't exact since it's only for the progressbardialog
	                                               // Also, note that the SQLITE driver doesn't support sqlQuery->size();

	//Run the SQL Query in a separate thread so that the UI still updates
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlFuture.waitForFinished();
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit finishedMRImageQuery();
#endif

	//int index_imageUID      = imageRecord.indexOf("image_uid");
	int index_time          = imageRecord.indexOf("image_time");
	//int index_width         = imageRecord.indexOf("width");
	//int index_height        = imageRecord.indexOf("height");
	//int index_pixelsize_x   = imageRecord.indexOf("pixelsize_x");
	//int index_pixelsize_y   = imageRecord.indexOf("pixelsize_y");
	int index_windowWidth   = imageRecord.indexOf("window_width");
	int index_windowCenter  = imageRecord.indexOf("window_center");
	int index_minPixval     = imageRecord.indexOf("min_pixval");
	int index_maxPixval     = imageRecord.indexOf("max_pixval");
	int index_imagePosition = imageRecord.indexOf("image_position");
	//int index_sliceLocation = imageRecord.indexOf("slice_location");
	int index_temporalPosID = imageRecord.indexOf("temporal_pos_id");
	int index_diffusionBValue = imageRecord.indexOf("diffusion_b_value");
	//int index_diffusionGradientOrientation = imageRecord.indexOf("diffusion_gradient_orientation");
	int index_instanceNum   = imageRecord.indexOf("instance_num");
	//int index_idqi_images   = imageRecord.indexOf("idqi_images");
	int index_image_data    = imageRecord.indexOf("image_data");
	//int index_scanOptions   = imageRecord.indexOf("scan_options");
	//int index_manufacturer  = imageRecord.indexOf("manufacturer");
	//int index_modelName     = imageRecord.indexOf("model_name");
	//int index_mfs           = imageRecord.indexOf("magnetic_field_strength");
	int index_triggerTime     = imageRecord.indexOf("trigger_time");

	int index_info_imagePosition   = imageInfoRecord.indexOf("image_position");
	int index_info_sliceLocation   = imageInfoRecord.indexOf("slice_location");
	int index_info_diffusionBValue = imageInfoRecord.indexOf("diffusion_b_value");
	int index_info_triggerTime     = imageInfoRecord.indexOf("trigger_time");
	int index_info_scanOptions     = imageRecord.indexOf("scan_options");

	//int index_imageType     = imageRecord.indexOf("image_type");

	int querySize = 0;
#if useDebug
	qDebug() << "Elapsed Time:" << whole.elapsed();
	qDebug() << "Start determination of slice locations and trigger times";
#endif
	QVector<float> locations_temp;
	QVector<QVector3D> imagePositions_;
	sqlQuery->first();
	QVector<ulong> triggerTimes;
	do{
		querySize++;
		emit openProgressUpdated(sqlQuery->at());

		if(!locations_temp.contains(sqlQuery->value(index_info_sliceLocation).toFloat()))
			locations_temp << sqlQuery->value(index_info_sliceLocation).toFloat();
		auto currentImagePosition = sqlQuery->value(index_info_imagePosition).toString().split("\\");
		if(currentImagePosition.size() > 2){
			if(!imagePositions_.contains(QVector3D(currentImagePosition.at(0).toFloat(), currentImagePosition.at(1).toFloat(), currentImagePosition.at(2).toFloat())))
				imagePositions_ << QVector3D(currentImagePosition.at(0).toFloat(), currentImagePosition.at(1).toFloat(), currentImagePosition.at(2).toFloat());
		}
#if useDebug
		else {
			qDebug() << "Image Position does not contain 3 components. Image Position:" << currentImagePosition;
		}
#endif
		if(!bValues_.contains(sqlQuery->value(index_info_diffusionBValue).toDouble()))
			bValues_ << sqlQuery->value(index_info_diffusionBValue).toDouble();
		if(!triggerTimes.contains(sqlQuery->value(index_info_triggerTime).toUInt()))
			triggerTimes << sqlQuery->value(index_info_triggerTime).toUInt();
	}while(sqlQuery->next());
	std::sort(locations_temp.begin(), locations_temp.end());
	std::sort(bValues_.begin(), bValues_.end());
	std::sort(triggerTimes.begin(), triggerTimes.end());
#if useDebug
	qDebug() << "Elapsed Time:" << whole.elapsed();
	qDebug() << "Query Size:" << querySize;
	qDebug() << "Number of Unique Trigger times: " << triggerTimes.size();
#endif
	sqlQuery->first();
	emit openProgressNewMaximum( progressMaximum = 2 * querySize );
	emit openProgressUpdated(querySize);
#if testFullHeader
	QString imageUID = sqlQuery->value(imageRecord.indexOf("image_uid")).toString();
	QMultiHash<QString, QString> header = qisql->extractAllDicomFields(imageUID);
	QVector<QString> patientID = header.values("0010,0010");
	qDebug()<<"Header Size:"<<header.count();
	qDebug()<<"Patient ID:"<<patientID.first();
#endif

	QStringList orientationString = seriesRecord.value("image_orientation").toString().split('\\');
	for(int i = 0; i < orientationString.size(); i++)
		orientation_ << orientationString.at(i).toFloat();

	maxPixval_ = static_cast<quint16>(sqlQuery->value("max_pixval").toUInt());
	minPixval_ = static_cast<quint16>(sqlQuery->value("min_pixval").toUInt());
	float imagedx = sqlQuery->value("pixelsize_x").toFloat();
	float imagedy = sqlQuery->value("pixelsize_y").toFloat();
	float imagedz = seriesRecord.value("space_between_slices").toFloat();
	int imagenx = sqlQuery->value("width").toInt();
	int imageny = sqlQuery->value("height").toInt();
	int imagenz = locations_temp.size();
	// Fix for spaceBetweenSlices not existing in header
	if(imagedz <= 0.0f)
		imagedz = ( locations_temp.last() - locations_temp.first() ) / (imagenz - 1);

	int imgy,imgx,imgz;
	int *patx = &imgx, *paty = &imgy, *patz = &imgz;
	//Find first dimension
	if(qRound(orientation_[0]) != 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "First Dimension is X:" << orientation_[0];
#endif
		nx_ = imagenx;
		dx_ = imagedx * qRound(orientation_[0]);
		patx = &imgx;
	}
	if(qRound(orientation_[1]) != 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "First Dimension is Y" << orientation_[1];
#endif
		ny_ = imagenx;
		dy_ = imagedx * qRound(orientation_[1]);
		paty = &imgx;
	}
	if(qRound(orientation_[2]) != 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "First Dimension is Z" << orientation_[2];
#endif
		nz_ = imagenx;
		dz_ = imagedx * qRound(orientation_[2]);
		patz = &imgz;
	}
	//Find second dimension
	if(qRound(orientation_[3]) != 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Second Dimension is X:" << orientation_[3];
#endif
		nx_ = imageny;
		dx_ = imagedy * qRound(orientation_[3]);
		patx = &imgy;
	}
	if(qRound(orientation_[4]) != 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Second Dimension is Y:" << orientation_[4];
#endif
		ny_ = imageny;
		dy_ = imagedy * qRound(orientation_[4]);
		paty = &imgy;
	}
	if(qRound(orientation_[5]) != 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Second Dimension is Z:" << orientation_[5];
#endif
		nz_ = imageny;
		dz_ = imagedy * qRound(orientation_[5]);
		patz = &imgy;
	}
	//Find third dimension
	if(qRound(orientation_[0]) == 0 && qRound(orientation_[3]) == 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Third Dimension is X:" << orientation_[3];
#endif
		nx_ = imagenz;
		dx_ = imagedz;
		patx = &imgz;
//        if(orientationString[0].startsWith('-') && orientationString[3].startsWith('-'))
//            dx_ = -dx_;
	}
	if(qRound(orientation_[1]) == 0 && qRound(orientation_[4]) == 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Third Dimension is Y:" << orientation_[4];
#endif
		ny_ = imagenz;
		dy_ = imagedz;
		paty = &imgz;
//        if(orientationString[1].startsWith('-') && orientationString[4].startsWith('-'))
//            dy_ = -dy_;
	}
	if(qRound(orientation_[2]) == 0 && qRound(orientation_[5]) == 0){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Third Dimension is Z:" << orientation_[5];
#endif
		nz_ = imagenz;
		dz_ = imagedz;
		patz = &imgz;
//        if(orientationString[2].startsWith('-') && orientationString[5].startsWith('-'))
//            dz_ = -dz_;
	}
	// determine acquisition orientation (like the above, this assumes image is aligned with one of the patient planes)
	if ((qRound(orientation_[0]) * qRound(orientation_[4]) != 0) || (qRound(orientation_[1]) * qRound(orientation_[3]) != 0)){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Acquisition plane is axial, resolution:" << dx_ << dy_ << dz_;
#endif
		acquisitionOrientation_ = QUANTX::XYPLANE;
	} else if ((qRound(orientation_[0]) * qRound(orientation_[5]) != 0) || (qRound(orientation_[2]) * qRound(orientation_[3]) != 0)){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Acquisition plane is coronal, resolution:" << dx_ << dy_ << dz_;
#endif
		acquisitionOrientation_ = QUANTX::XZPLANE;
	} else if ((qRound(orientation_[1]) * qRound(orientation_[5]) != 0) || (qRound(orientation_[2]) * qRound(orientation_[4]) != 0)){
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Acquisition plane is sagittal, resolution:" << dx_ << dy_ << dz_;
#endif
		acquisitionOrientation_ = QUANTX::YZPLANE;
	}

	QVector<float> locations_ = getSliceLocations(imagePositions_, acquisitionOrientation_);

	// Check to see if the two breasts are split into different series
	QString secondBreastUID;
	if(acquisitionOrientation_ == QUANTX::YZPLANE){
#if useDebug
		qDebug() << "isDynMulti:" << isDynMulti;
#endif
		if( isDynMulti && !seriesDesc_.contains("Left", Qt::CaseInsensitive) && !seriesDesc_.contains("Right", Qt::CaseInsensitive) && !seriesDesc_.contains("Lt. ", Qt::CaseInsensitive) && !seriesDesc_.contains("Rt. ", Qt::CaseInsensitive) ){
			if( firstPostSeriesNum > 100 )
				splitBreasts = true;
		}
		else {
			imageTestRecord = imageRecord;
			imageTestRecord.setValue("study_uid", seriesRecord.value("study_uid"));
			imageTestRecord.setValue("series_uid", "placeholder");
			imageTestRecord.setValue("pixelsize_x", firstPostImageRecord.value("pixelsize_x"));
			imageTestRecord.setValue("pixelsize_y", firstPostImageRecord.value("pixelsize_y"));
			imageTestRecord.setValue("instance_num", "1 ");
			imageTestRecord.setValue("scan_options", sqlQuery->value("scan_options"));
			for(int i = imageTestRecord.count() - 1; i >= 0; i--)
				if(imageTestRecord.isNull(i))
					imageTestRecord.remove(i);

#if useDebug
			qDebug() << "seriesMap.key() - 1:" << seriesMap.key(seriesUID_) - 1;
			qDebug() << "seriesMap.key()    :" << seriesMap.key(seriesUID_);
			qDebug() << "seriesMap.key() + 1:" << seriesMap.key(seriesUID_) + 1;
			qDebug() << "seriesMap:" << seriesMap;
#endif
			if(seriesMap.contains(seriesMap.key(seriesUID_) - 1)){
				imageTestRecord.setValue("series_uid", seriesMap.value(seriesMap.key(seriesUID_) - 1));
				sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
				             + db.driver()->sqlStatement(QSqlDriver::WhereStatement, sqlImageTable,imageTestRecord,false);
				if(sqlStatement.contains(R"("qi_2d_images"."pixelsize_x" = )")){
					sqlStatement.replace(R"("qi_2d_images"."pixelsize_x" = )", R"(ROUND(CAST("qi_2d_images"."pixelsize_x" AS DECIMAL),5) = ROUND()");
					sqlStatement.replace(R"( AND "qi_2d_images"."pixelsize_y" = )", R"(,5) AND ROUND(CAST("qi_2d_images"."pixelsize_y" AS DECIMAL),5) = ROUND()");
					sqlStatement.replace(R"( AND "qi_2d_images"."instance_num" = )", R"(,5) AND "qi_2d_images"."instance_num" = )");
				}
        #if useDebug
				/*
				qDebug() << "SQL Statement:";
				qDebug() << sqlStatement;
				qDebug() << "-------------------------------------------------------------------------------------------------------------------";
				*/
        #endif
				sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
				sqlFuture.waitForFinished();
				splitBreasts = sqlQuery->next();
			}
			if( (!splitBreasts) && seriesMap.contains(seriesMap.key(seriesUID_) + 1)){
				imageTestRecord.setValue("series_uid", seriesMap.value(seriesMap.key(seriesUID_) + 1));
				sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
				             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,imageTestRecord,false);
				if(sqlStatement.contains(R"("qi_2d_images"."pixelsize_x" = )")){
					sqlStatement.replace(R"("qi_2d_images"."pixelsize_x" = )", R"(ROUND(CAST("qi_2d_images"."pixelsize_x" AS DECIMAL),5) = ROUND()");
					sqlStatement.replace(R"( AND "qi_2d_images"."pixelsize_y" = )", R"(,5) AND ROUND(CAST("qi_2d_images"."pixelsize_y" AS DECIMAL),5) = ROUND()");
					sqlStatement.replace(R"( AND "qi_2d_images"."instance_num" = )", R"(,5) AND "qi_2d_images"."instance_num" = )");
				}
        #if useDebug
				/*
				qDebug() << "SQL Statement:";
				qDebug() << sqlStatement;
				qDebug() << "-------------------------------------------------------------------------------------------------------------------";
				*/
        #endif
				sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
				sqlFuture.waitForFinished();
				splitBreasts = sqlQuery->next();
			}
			if(splitBreasts && isDynMulti && (nt_ > 1)){
				if(dceMap.size() == 3 && seriesDesc_.contains("T2"))
					dceMap.remove(dceMap.lastKey());
				nt_ /= 2;
			}
		}
		if(splitBreasts){
#if useDebug
			qDebug() << "Elapsed Time:" << whole.elapsed();
			qDebug() << "Left and Right Breasts are in Separate Series";
#endif
			if( isDynMulti ){
				if( dceMap.size() > 1 )
					uidRecord.setValue("series_uid", dceMap.values().at(1));
				else{
					splitBreasts = false;
					uidRecord.setValue("series_uid", seriesUID_);
#if useDebug
					qDebug() << "Could not find other breast!?!?!";
#endif
				}
			}
			else{
				secondBreastUID = sqlQuery->record().value("series_uid").toString();
				uidRecord.setValue("series_uid", secondBreastUID);
			}
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
			             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
			/*if(sqlStatement.contains("\"qi_2d_images\".\"pixelsize_x\" = ")){
				sqlStatement.replace("\"qi_2d_images\".\"pixelsize_x\" = ", "ROUND(CAST(\"qi_2d_images\".\"pixelsize_x\" AS DECIMAL),5) = ROUND(");
				sqlStatement.replace(" AND \"qi_2d_images\".\"pixelsize_y\" = ", ",5) AND ROUND(CAST(\"qi_2d_images\".\"pixelsize_y\" AS DECIMAL),5) = ROUND(");
				sqlStatement.replace(" AND \"qi_2d_images\".\"instance_num\" = ", ",5) AND \"qi_2d_images\".\"instance_num\" = ");
			}*/
    #if useDebug
			/*
			qDebug() << "SQL Statement:";
			qDebug() << sqlStatement;
			qDebug() << "-------------------------------------------------------------------------------------------------------------------";
			*/
    #endif
			sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
			sqlFuture.waitForFinished();
			sqlQuery->first();
			do{
				querySize++;
				const auto sliceIndex = 0; // This is always QUANTX::YZPLANE
				if(!locations_.contains(sqlQuery->value(index_info_imagePosition).toString().split("\\").at(sliceIndex).toFloat()))
					locations_ << sqlQuery->value(index_info_imagePosition).toString().split("\\").at(sliceIndex).toFloat();
			}while(sqlQuery->next());
			std::sort(locations_.begin(), locations_.end());
			imagenz = locations_.size();
			nx_ = imagenz;
			emit openProgressNewMaximum( progressMaximum = 2 * querySize );
			emit openProgressUpdated(querySize);
		}
#if useDebug
		else
			qDebug() << "Sagittal but not split breasts";
#endif
		uidRecord.setValue("series_uid",seriesUID_);
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageInfoRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
		/*if(sqlStatement.contains("\"qi_2d_images\".\"pixelsize_x\" = ")){
			sqlStatement.replace("\"qi_2d_images\".\"pixelsize_x\" = ", "ROUND(CAST(\"qi_2d_images\".\"pixelsize_x\" AS DECIMAL),5) = ROUND(");
			sqlStatement.replace(" AND \"qi_2d_images\".\"pixelsize_y\" = ", ",5) AND ROUND(CAST(\"qi_2d_images\".\"pixelsize_y\" AS DECIMAL),5) = ROUND(");
			sqlStatement.replace(" AND \"qi_2d_images\".\"instance_num\" = ", ",5) AND \"qi_2d_images\".\"instance_num\" = ");
		}*/
#if useDebug
		/*
		qDebug() << "SQL Statement:";
		qDebug() << sqlStatement;
		qDebug() << "-------------------------------------------------------------------------------------------------------------------";
		*/
#endif
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		sqlQuery->first();
	}

	nb_ = bValues_.size();
	if(nb_ < 1)
		nb_ = 1;
	if(index_info_scanOptions >= 0)
		scanOptions_ = sqlQuery->value(index_info_scanOptions).toString().trimmed().split('\\');
	else
		scanOptions_.clear();

	manufacturer_ = sqlQuery->value("manufacturer").toString();
	modelName_    = sqlQuery->value("model_name").toString();
	magneticFieldStrength_ = sqlQuery->value("magnetic_field_strength").toFloat();
	if(magneticFieldStrength_ <= 0.0f){ // Update table if magnetic field strength is missing
		QFuture<bool> sqlUpdateFuture;
		QScopedPointer<QSqlQuery> sqlUpdateQuery(new QSqlQuery(db));
		QSqlRecord dicomUIDRecord;
		dicomUIDRecord.append(QSqlField("0008,0018",QVariant::String));
		dicomUIDRecord.setValue("0008,0018",sqlQuery->value("image_uid"));
		QSqlRecord dicomMFSRecord;
		dicomMFSRecord.append(QSqlField("0018,0087",QVariant::String));
		QString sqlUpdateStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,"dicom_data",dicomMFSRecord,false) + QString(' ')
		                           + db.driver()->sqlStatement(QSqlDriver::WhereStatement, "dicom_data",dicomUIDRecord,false);
		sqlUpdateFuture = QtConcurrent::run(sqlUpdateQuery.data(),&QSqlQuery::exec,sqlUpdateStatement);
		sqlUpdateFuture.waitForFinished();
		sqlUpdateQuery->first();
		magneticFieldStrength_ = sqlUpdateQuery->value("0018,0087").toString().toFloat();
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Magnetic Field Strength Missing, Magnetic Field is" << magneticFieldStrength_ << "Tesla";
#endif
		QSqlRecord imageUIDRecord;
		imageUIDRecord.append(imageRecord.field("image_uid"));
		imageUIDRecord.setValue("image_uid",sqlQuery->value("image_uid"));
		QSqlRecord imageMFSRecord;
		imageMFSRecord.append(imageRecord.field("magnetic_field_strength"));
		imageMFSRecord.setValue("magnetic_field_strength",magneticFieldStrength_);
		sqlUpdateStatement = db.driver()->sqlStatement(QSqlDriver::UpdateStatement,"qi_2d_images",imageMFSRecord,false) + QString(' ')
		                   + db.driver()->sqlStatement(QSqlDriver::WhereStatement, "qi_2d_images",imageUIDRecord,false);
		sqlUpdateFuture = QtConcurrent::run(sqlUpdateQuery.data(),&QSqlQuery::exec,sqlUpdateStatement);
		sqlUpdateFuture.waitForFinished();
	}

	int dim4Size;// fourth dimension of the data set, nb if DWI, nt otherwise
	if (nb_ > 1)
		dim4Size = nb_;
	else
		dim4Size = nt_;
	if (dim4Size < triggerTimes.size()){
		dim4Size = triggerTimes.size();
		nt_ = dim4Size;
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "!!!! Adjust number of timepoints to match number of unique trigger times !!!!";
#endif
		querySize /= dim4Size;
	}

#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit determinedMRSize();
	qApp->processEvents();
#endif
	data_.clear();
	data_.resize(dim4Size);
	for(int t = 0; t < dim4Size; t++){
		data_.at(t) = (std::vector<std::vector<std::vector<quint16> > >(nz_));
		for(int z = 0; z < nz_; z++){
			data_[t].at(z) = (std::vector<std::vector<quint16> >(ny_));
			for(int y = 0; y < ny_; y++){
				data_[t][z].at(y) = (std::vector<quint16>(nx_));
			}
		}

		for (int t = 0 ; t < nt_; t++)
			instanceNums_ << QMap<float, QString>();
#if useDebug
		qDebug() << "Elapsed Time:" << whole.elapsed();
		qDebug() << "Empty data array created for t =" << t << "with size [" << nz_ << ny_ << nx_ << "]";
#endif
	}
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit builtMRBuffer();
	qApp->processEvents();
#endif

	// NOTE: windowCenter and windowWidth are stored as double in the QiAbstractImage class
	auto sumWidths = 0.0;
	auto sumCenters = 0.0;
	QStringList positionString;
	positions_.resize(nz_);
	int timePt;
	timestamps_.resize(nt_);
	QByteArray imageData;
	quint16 *imgptr,*dataptr;
	quint16 curmax;
	quint16 curmin;
	int queryProgress = 0;

	if(isDynMulti){ // Always true?
#if useDebug
		qDebug() << "isDynMulti" << isDynMulti;
#endif
		emit openProgressNewMaximum( progressMaximum = querySize * (nt_ + 1) );
		int previousExtraTimePoints = 0;
		//There are two series
		for(int nSeries = 0; nSeries < dceMap.size(); nSeries++){
			int currentExtraTimePoints = 0;
			// Prep second series for data load
			// This should have been done previously
			uidRecord.setValue("series_uid",dceMap.values().at(nSeries));
			sqlQuery->clear();
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
			             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
			sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
			sqlFuture.waitForFinished();
			sqlQuery->first();

			int sliceIndex = 2;
			if (acquisitionOrientation_ == QUANTX::XZPLANE )
				sliceIndex = 1;
			else if (acquisitionOrientation_ == QUANTX::YZPLANE )
				sliceIndex = 0;

			// Now, load the data as normal
			do{
				emit openProgressUpdated(querySize+queryProgress);

				sumWidths += sqlQuery->value(index_windowWidth).toDouble();
				sumCenters += sqlQuery->value(index_windowCenter).toDouble();
				curmax = static_cast<quint16>(sqlQuery->value(index_maxPixval).toUInt());
				curmin = static_cast<quint16>(sqlQuery->value(index_minPixval).toUInt());
				imgz = locations_.indexOf(sqlQuery->value(index_imagePosition).toString().split("\\").at(sliceIndex).toFloat());
#if useDebug
				qDebug() << "imgz" << imgz;
#endif

				//int timePt = sqlQuery->value(index_temporalPosID).toInt();
				timePt = splitBreasts ? nSeries / 2 : nSeries;
				//if( timePt < (splitBreasts ? nSeries / 2 : nSeries) ){
				    if( sqlQuery->record().value(index_instanceNum).toInt() > imagenz ) // Not sure if this is the best way to handle a split breast DCE image, if those exist
						timePt += ((sqlQuery->record().value(index_instanceNum).toInt() - 1) / imagenz);
					//}
					timePt += previousExtraTimePoints;


				while( timePt >= nt_ ){ // ((splitBreasts ? nSeries / 2 : nSeries) + previousExtraTimePoints + currentExtraTimePoints) ){
#if useDebug
					qDebug() << "Elapsed Time:" << whole.elapsed();
					qDebug() << "Found another timepoint in series" << dceMap.keys().at(nSeries);
					qDebug() << "Expanding from" << nt_ << "to" << nt_ + 1 << "timepoints";
#endif
					// Fix arrays for the extra size
					data_.push_back(std::vector<std::vector<std::vector<quint16> > >(nz_));
					for(int z = 0; z < nz_; z++){
						data_[nt_].at(z) = (std::vector<std::vector<quint16> >(ny_));
						for(int y = 0; y < ny_; y++){
							data_[nt_][z].at(y) = (std::vector<quint16>(nx_));
						}
					}
					instanceNums_.append(QMap<float, QString>{});
					timestamps_.append(QTime());
					++currentExtraTimePoints;
					++nt_;
				}

				auto instanceNum = sqlQuery->value(index_instanceNum).toString();
#if useDebug
				qDebug() << "timePt:" << timePt << "instanceNum:" << instanceNum;
#endif
				instanceNums_[timePt].insert(imgz, instanceNum); // imgz is acquisition orientation component of image position

				if(imgz == 0){
					//if(separatePrecontrast)
					    timestamps_[timePt] = sqlQuery->value(index_time).toTime();
					//else
					//	timestamps_[timePt] = QTime(0, 0).addMSecs(sqlQuery->value(index_triggerTime).toInt());
#if 0 // For now, let's test this out
						if(UCGEIMAGECHECK)
						timestamps_[timePt] = sqlQuery->value(index_time).toTime();
					if((!separatePrecontrast)/* && (!sqlQuery->value(index_triggerTime).isNull())*/){
						//if(timePt > 0 && timestamps_[timePt] == timestamps_[0])
						    timestamps_[timePt] = QTime(0, 0).addMSecs(sqlQuery->value(index_triggerTime).toInt());
							//timestamps_[timePt] = QTime(0, ( separatePrecontrast && ( timePt > 0 ) ) ? 1 : 0).addMSecs(sqlQuery->value(index_triggerTime).toInt());
							//if(timePt > 1 && timestamps_[timePt] == QTime(0,1))
							//    timestamps_[timePt] = QTime(0, timePt * 2 - 1);
					}
#endif
				}
#if useDebug
				if(imgz == 0){
					qDebug() << "Elapsed Time:" << whole.elapsed();
					qDebug() << "Timestamp" << timePt << "found, time is" << timestamps_[timePt];
					qDebug() << "Image Time:" << sqlQuery->value(index_time).toTime() << " Trigger Time:" << sqlQuery->value(index_triggerTime).toInt() << "ms";
				}
#endif
				if(nSeries == 0){
					positionString = sqlQuery->value(index_imagePosition).toString().split('\\');
#if useDebug
					qDebug() << "positionString:" << positionString;
#endif
					//Find third dimension
					if(qRound(orientation_[2]) == 0 && qRound(orientation_[5]) == 0){
						//Third dimension is Z
						if(positionString.size() == 3)
							positions_[imgz] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat());
						else
							positions_[imgz] = QVector3D(0.0,0.0,0.0);
					}
					else{
						auto px = positionString.at(0).toFloat();
						auto py = positionString.at(1).toFloat();
						auto pz = positionString.at(2).toFloat();
						//Third dimension is X or Y
						if(px < positions_.first().x() && qRound(orientation_[0]) == 0 && qRound(orientation_[3]) == 0){
							for(int z=0; z<nz_; ++z)
								    positions_[z] = QVector3D(px, py, pz + (z * dz_));
						}
						if(py < positions_.first().y() && qRound(orientation_[1]) == 0 && qRound(orientation_[4]) == 0){
							for(int z=0; z<nz_; ++z)
								    positions_[z] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat() + (z * dz_));
						}
					}
				}

				if(curmax > maxPixval_)
					maxPixval_ = curmax;
				if(curmin < minPixval_)
					minPixval_ = curmin;

				imageData = qUncompress(sqlQuery->value(index_image_data).toByteArray());
#if useDebug
				qDebug() << "Image Data Bytes:" << imageData.size();
#endif
				imgptr = reinterpret_cast<quint16 *>(imageData.data());
				if(imageData.size() == (imagenx * imageny * 2))
					for(imgy = 0; imgy < imageny; imgy++){
						for(imgx = 0; imgx < imagenx; imgx++){
							int x = (dx_ >= 0.0f) ? *patx : nx_ - *patx - 1;
							int y = (dy_ >= 0.0f) ? *paty : ny_ - *paty - 1;
							int z = (dz_ >= 0.0f) ? *patz : nz_ - *patz - 1;
/*
							int x = *patx;
							//int x = (dx_ >= 0.0) ? *patx : nx_ - *patx - 1;
							//int y = *paty;
							//int y = ny_ - *paty - 1;
							int y = (acquisitionOrientation_ == QUANTX::XYPLANE) ? *paty : ny_ - *paty - 1;
							int z = *patz;
							//int z = (dz_ >= 0.0) ? *patz : nz_ - *patz - 1;
							//int z = (dz_ >= 0.0) ? *patz : nz_ - *patz - 1;
*/
							dataptr = data_[timePt][z][y].data();
							dataptr[x] = imgptr[(imagenx * imgy) + imgx];
						}
					}
				queryProgress++;
			}while(sqlQuery->next());
			previousExtraTimePoints += currentExtraTimePoints;
		}
	}
	else{
		sqlQuery->clear();
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		sqlQuery->first();
		bool firstBreast = true;

		int sliceIndex = 2;
		if (acquisitionOrientation_ == QUANTX::XZPLANE )
			sliceIndex = 1;
		else if (acquisitionOrientation_ == QUANTX::YZPLANE )
			sliceIndex = 0;

		do{
			emit openProgressUpdated(querySize+queryProgress);

			sumWidths += sqlQuery->value(index_windowWidth).toDouble();
			sumCenters += sqlQuery->value(index_windowCenter).toDouble();
			curmax = static_cast<quint16>(sqlQuery->value(index_maxPixval).toUInt());
			curmin = static_cast<quint16>(sqlQuery->value(index_minPixval).toUInt());
			imgz = locations_.indexOf(sqlQuery->value(index_imagePosition).toString().split("\\").at(sliceIndex).toFloat());

			if (nb_ > 1)
				timePt = bValues_.indexOf(sqlQuery->value(index_diffusionBValue).toDouble());
			else
				timePt = sqlQuery->value(index_temporalPosID).toInt() - 1;
			if(timePt == -2)
				timePt = (sqlQuery->value(index_instanceNum).toInt() - 1) % nt_;
			if(timePt < 0)
				timePt = 0;

			if(imgz == 0)
				timestamps_[timePt] = sqlQuery->value(index_time).toTime();
			if(timePt == 0){
				positionString = sqlQuery->value(index_imagePosition).toString().split('\\');
				//Find third dimension
				if(qRound(orientation_[2]) == 0 && qRound(orientation_[5]) == 0){
					//Third dimension is Z
					if(positionString.size() == 3)
						positions_[imgz] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat());
					else
						positions_[imgz] = QVector3D(0.0,0.0,0.0);
				}
				else{
					float px = positionString.at(0).toFloat();
					float py = positionString.at(1).toFloat();
					float pz = positionString.at(2).toFloat();
					//Third dimension is X or Y
					if(px < positions_.first().x() && qRound(orientation_[0]) == 0 && qRound(orientation_[3]) == 0){
						    for(int z=0; z<nz_; ++z)
								positions_[z] = QVector3D(px, py, pz + (z * dz_));
					}
					if(py < positions_.first().y() && qRound(orientation_[1]) == 0 && qRound(orientation_[4]) == 0){
						    for(int z=0; z<nz_; ++z)
								positions_[z] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat() + (z * dz_));
					}
				}
			}

			if(curmax > maxPixval_)
				maxPixval_ = curmax;
			if(curmin < minPixval_)
				minPixval_ = curmin;

			//if(dbSettings.at(0) == QString("QPSQL"))
			//    imageData = sqlQuery->value(index_image_data).toByteArray();
			//else
			imageData = qUncompress(sqlQuery->value(index_image_data).toByteArray());
			imgptr = reinterpret_cast<quint16 *>(imageData.data());
			if(imageData.size() == (imagenx * imageny * 2))
				for(imgy=0;imgy<imageny;imgy++){
					for(imgx=0;imgx<imagenx;imgx++){
						int x = (dx_ >= 0.0f) ? *patx : nx_ - *patx - 1;
						int y = (dy_ >= 0.0f) ? *paty : ny_ - *paty - 1;
						int z = (dz_ >= 0.0f) ? *patz : nz_ - *patz - 1;
						dataptr = data_[timePt][z][y].data();
						dataptr[x] = imgptr[(imagenx * imgy) + imgx];
					}
				}
			queryProgress++;
			if(splitBreasts && firstBreast){
				if(sqlQuery->next())
					sqlQuery->previous();
				else{
					uidRecord.setValue("series_uid",secondBreastUID);
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					firstBreast = false;
				}
			}
		}while(sqlQuery->next());
	}

	// Fix for negative z-dimension
	if(acquisitionOrientation_ == QUANTX::XYPLANE)
		if(positions_[0].z() > positions_[1].z()){
			for(int i = 0; i < ((nz_) / 2); ++i){
				std::swap(positions_[i], positions_[nz_ - 1 - i]);
				for(int t = 0; t < nt_; ++t)
					data_[t][i].swap(data_[t][nz_ - 1 - i]);
			}
		}

#if useDebug
	qDebug() << "Elapsed Time:" << whole.elapsed();
	qDebug() << "Real-world dimesions:";
	QVector3D p0 = positions_[0];
	qDebug() << QString("x(0) = %1, x(%2) = %3").arg(static_cast<double>(p0.x())).arg(nx_ - 1).arg(static_cast<double>(p0.x() + (nx_ - 1) * dx_));
	qDebug() << QString("y(0) = %1, y(%2) = %3").arg(static_cast<double>(p0.y())).arg(ny_ - 1).arg(static_cast<double>(p0.y() + (ny_ - 1) * dy_));
	qDebug() << QString("z(0) = %1, z(%2) = %3").arg(static_cast<double>(p0.z())).arg(nz_ - 1).arg(static_cast<double>(positions_.at(nz_ - 1).z()));
#endif

	// Fix for positions not changing
	if(positions_.size() > 1 && positions_[0] == positions_[1]){
		if(acquisitionOrientation_ == QUANTX::XYPLANE)
			for(int i = 1; i < positions_.size(); i++)
				positions_[i] = positions_[0] + QVector3D(0, 0, i * dz_);
	}


	emit openProgressUpdated(progressMaximum);

	windowWidth_ = sumWidths / (imagenz * dim4Size);
	windowLevel_ = sumCenters / (imagenz * dim4Size);

	if ( nt_ > 1 ){
		auto basetimestamp = 0;
		for(auto i = 1; i < timestamps_.size(); ++i){
//#endif
			if(timestamps_[i] == timestamps_[basetimestamp]){
				timestamps_[i] = timestamps_[basetimestamp].addMSecs(triggerTimes[i - basetimestamp]); // Set all the timestamps relative to the initial timestamp being
			}
		else
				basetimestamp = i;
		}
#if ( __cplusplus >= 202001L )
		for(auto basetime = timestamps_[0]; auto & timestamp : timestamps_)
#else
		auto basetime = timestamps_[0];
		for(auto & timestamp : timestamps_)
#endif
			timestamp = QTime(0,0).addMSecs(basetime.msecsTo(timestamp)); // Set all the timestamps relative to the initial timestamp being
		injectionTime_ = (timestamps_[1] < QTime(0,1)) ? QTime(0,0) : timestamps_[1].addSecs( -60 ); // initially set injection time so it's 60 secs before the first post-contrast
	}
#if useDebug
	qDebug() << "timestamps[] = " << timestamps_;
#endif

	origin_ = findOrigin();
	dx_ = qAbs(dx_);
	dy_ = qAbs(dy_);
	dz_ = qAbs(dz_);
#if useDebug
	qDebug() << "New Origin:" << origin_;
#endif

	sqlQuery.reset();
	db.close();
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit finishedMRFillBuffer();
#endif
	// populate seriesNums_ if there are associated series
	if (!associatedSeries_.isEmpty()){
		QVector<int> seriesNumsInt{seriesNums_.first().toInt()};
		auto studySeriesInfo = QISql::getSeriesInfo(dbSettings, studyUID_);
		for (const auto &series : associatedSeries_){
			for (const auto &info : studySeriesInfo){
				if(info.seriesUID == series){
					seriesNumsInt << info.seriesNumber;
					break;
				}
			}
		}
		std::sort(seriesNumsInt.begin(), seriesNumsInt.end());
		seriesNums_.clear();
		for (const auto &seriesNum : seriesNumsInt){
			seriesNums_ << QString::number(seriesNum);
		}
	}
	// for the case when all the post timepoints are in a single series
	if (seriesNums_.count() == 2){
		for (int i = 0; i < nt_ - 2; i++){
			seriesNums_ << seriesNums_.last();
		}
	}

	return 1; //success!

}
