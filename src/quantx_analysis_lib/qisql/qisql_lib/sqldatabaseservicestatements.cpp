#include "sqldatabaseservicestatements.h"

#include <QMap>

#define DROP_TABLES 0

namespace SQLite{
    namespace Update110To200 {
	    constexpr auto tempTable = R"(CREATE TEMPORARY TABLE %1_backup(idqi_images, MRN, imageUID, seriesUID, studyUID, modality, image_data,
	                               date, time, width, height, pixelsize_x, pixelsize_y, imageType, windowWidth, windowCenter, minPixval,
	                               maxPixval, imagePosition, sliceLocation, temporalPosID, instanceNum, scanOptions, viewPosition, imageLaterality,
	                               patientOrientation, photometricInterpretation, manufacturer, modelName) )";
	    constexpr auto tableToTempData = R"(INSERT INTO %1_backup SELECT idqi_images, MRN, imageUID, seriesUID, studyUID, modality, image_data,
	                              date, time, width, height, pixelsize_x, pixelsize_y, imageType, windowWidth, windowCenter, minPixval,
	                              maxPixval, imagePosition, "sliceLocation", temporalPosID, instanceNum, scanOptions, viewPosition, imageLaterality,
	                              patientOrientation, photometricInterpretation, manufacturer, modelName FROM %1)";
	    constexpr auto createTable = R"(CREATE TABLE %1(idqi_images, MRN, imageUID, seriesUID, studyUID, modality, image_data,
	                                date, time, width, height, pixelsize_x, pixelsize_y, imageType, windowWidth, windowCenter, minPixval,
	                                maxPixval, imagePosition, sliceLocation, temporalPosID, instanceNum, scanOptions, viewPosition, imageLaterality,
	                                patientOrientation, photometricInterpretation, manufacturer, modelName) )";
	    constexpr auto tempToTableData = R"(INSERT INTO %1 SELECT idqi_images, MRN, imageUID, seriesUID, studyUID, modality, image_data,
	                          date, time, width, height, pixelsize_x, pixelsize_y, imageType, windowWidth, windowCenter, minPixval,
	                          maxPixval, imagePosition, sliceLocation, temporalPosID, instanceNum, scanOptions, viewPosition, imageLaterality,
	                          patientOrientation, photometricInterpretation, manufacturer, modelName FROM %1_backup)";

	    QStringList dropColumns( const QString& tableName )
		{
			QStringList sqlStatements;

			sqlStatements << QString( SQLite::Update110To200::tempTable ).arg( tableName );
			sqlStatements << QString( SQLite::Update110To200::tableToTempData ).arg( tableName );
			sqlStatements << QString( "DROP TABLE %1" ).arg( tableName );
			sqlStatements << QString( SQLite::Update110To200::createTable ).arg( tableName );
			sqlStatements << QString( SQLite::Update110To200::tempToTableData ).arg( tableName );
			sqlStatements << QString( "DROP TABLE %1_backup" ).arg( tableName );

			if( tableName.contains( "mg" ) || tableName.contains( "us" ) ) {
				for( auto& statement : sqlStatements ) {
					statement.replace( "modality", "numberOfFrames" );

				}
			}

			return sqlStatements;
		}
	}
}

/*!
 * \brief Finds the current DB version.
 * \return Query string.
 */
const QString SqlDatabaseServiceStatements::currentDatabaseVersion() const
{
	return R"(SELECT idqi_version, version
	          FROM qi_database_version
	          ORDER BY version ASC)";
}

/*!
 * \brief Query to find list of studies.
 * \return Query string.
 */
const QString SqlDatabaseServiceStatements::studyList() const
{
	return R"(SELECT "idqi_studies", "study_uid"
	          FROM "qi_studies")";
}

/*!
 * \brief Select the DICOM header.
 * \return Query string.
 */
const QString SqlDatabaseServiceStatements::dicomHeader() const
{
	return R"(SELECT "0008,0018" FROM "dicom_data")";
}

/*!
 * \brief Updates the DB version in the table.
 * \param versionId Version ID to update to.
 * \param versionNum Version number to update to.
 * \return Query string.
 */
const QString SqlDatabaseServiceStatements::updateToVersion( const int versionId, const int versionNum ) const
{
	return QString( R"_stmnt_(
	                INSERT INTO qi_database_version
	                VALUES (%1, %2)
	                )_stmnt_").arg(versionId).arg(versionNum);
}

/*!
 * \brief Updates from 0 to 1.
 * \return Query string
 */
const QStringList SqlDatabaseServiceStatements::updateFromVer0ToVer1() const
{
	return QStringList{ R"(ALTER TABLE "qi_images_mr" ADD COLUMN "triggerTime" int DEFAULT NULL)",
		R"(ALTER TABLE "dicom_data" ADD COLUMN "0018,1060" varchar DEFAULT NULL)" };
}

/*!
 * \brief Updates from 1 to 101.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatements::updateFromVer1ToVer101() const
{
	return QStringList{ R"(ALTER TABLE "qi_images_mr" ADD COLUMN "magneticFieldStrength" real DEFAULT NULL)" };
}

/*!
 * \brief Updates 101 to 110 for SQlite.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer101ToVer110() const
{
	QString columns =  R"( "seriesUID_moving" varchar DEFAULT NULL, "seriesUID_fixed" varchar DEFAULT NULL, "t_fixed" INT DEFAULT NULL, "Xform" %1, "imageHeader" %1, "default_value" REAL DEFAULT NULL )";

	return QStringList{ QString(R"(CREATE TABLE "qi_motion_correction" ( "idqi_motion_correction" INTEGER PRIMARY KEY,%1 ))").arg(columns.arg("BLOB")) };
}

/*!
 * \brief Updates from 101 to 110 for Postgres.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer101ToVer110() const
{
	QString columns =  R"( "seriesUID_moving" varchar DEFAULT NULL, "seriesUID_fixed" varchar DEFAULT NULL, "t_fixed" INT DEFAULT NULL, "Xform" %1, "imageHeader" %1, "default_value" REAL DEFAULT NULL )";
	QString sequence = "qi_motion_correction_idqi_motion_correction";

	return QStringList{ QString("CREATE SEQUENCE %1 INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1").arg(sequence),
	QString(R"(CREATE TABLE "qi_motion_correction" ( "idqi_motion_correction" INTEGER PRIMARY KEY DEFAULT nextval('%1'::regclass),%2 ))").arg(sequence, columns.arg("BYTEA")) };
}

/*!
 * \brief Updates from 110 to 200 for SQLite.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer110ToVer200() const
{
	QStringList statementList;

	QVector<QPair<QString,QString> > qi_images_cols;
	qi_images_cols << QPair<QString,QString>("\"MRN\"",                       "mrn");
	qi_images_cols << QPair<QString,QString>("\"imageUID\"",                  "image_uid");
	qi_images_cols << QPair<QString,QString>("\"seriesUID\"",                 "series_uid");
	qi_images_cols << QPair<QString,QString>(R"("studyUID")",                 "study_uid");
	qi_images_cols << QPair<QString,QString>("\"date\"",                      "image_date");
	qi_images_cols << QPair<QString,QString>("\"time\"",                      "image_time");
	qi_images_cols << QPair<QString,QString>("\"imageType\"",                 "image_type");
	qi_images_cols << QPair<QString,QString>("\"windowWidth\"",               "window_width");
	qi_images_cols << QPair<QString,QString>("\"windowCenter\"",              "window_center");
	qi_images_cols << QPair<QString,QString>("\"minPixval\"",                 "min_pixval");
	qi_images_cols << QPair<QString,QString>("\"maxPixval\"",                 "max_pixval");
	qi_images_cols << QPair<QString,QString>("\"imagePosition\"",             "image_position");
	qi_images_cols << QPair<QString,QString>("\"sliceLocation\"",             "slice_location");
	qi_images_cols << QPair<QString,QString>("\"temporalPosID\"",             "temporal_pos_id");
	qi_images_cols << QPair<QString,QString>("\"instanceNum\"",               "instance_num");
	qi_images_cols << QPair<QString,QString>("\"scanOptions\"",               "scan_options");
	qi_images_cols << QPair<QString,QString>("\"viewPosition\"",              "view_position");
	qi_images_cols << QPair<QString,QString>("\"imageLaterality\"",           "image_laterality");
	qi_images_cols << QPair<QString,QString>("\"patientOrientation\"",        "patient_orientation");
	qi_images_cols << QPair<QString,QString>("\"photometricInterpretation\"", "photometric_interpretation");
	qi_images_cols << QPair<QString,QString>("\"modelName\"",                 "model_name");

	//statementList << QString("ALTER TABLE qi_images DROP COLUMN \"diffusionBValue\"");
	//statementList << QString("ALTER TABLE qi_images DROP COLUMN \"diffusionGradientOrientation\"");

	statementList << SQLite::Update110To200::dropColumns( "qi_images" );

	QVector<QPair<QString,QString> > qi_images_mg_cols = qi_images_cols;
	qi_images_mg_cols << QPair<QString,QString>("\"numberOfFrames\"", "number_of_frames");

	//statementList << QString("ALTER TABLE qi_images_mg DROP COLUMN \"diffusionBValue\"");
	//statementList << QString("ALTER TABLE qi_images_mg DROP COLUMN \"diffusionGradientOrientation\"");
	statementList << SQLite::Update110To200::dropColumns( "qi_images_mg" );

	QVector<QPair<QString,QString> > qi_images_us_cols = qi_images_mg_cols;

	//statementList << QString("ALTER TABLE qi_images_us DROP COLUMN \"diffusionBValue\"");
	//statementList << QString("ALTER TABLE qi_images_us DROP COLUMN \"diffusionGradientOrientation\"");
	statementList << SQLite::Update110To200::dropColumns( "qi_images_us" );

	statementList << QString( "DROP VIEW qi_imagelocations" );

	QVector<QPair<QString,QString> > qi_images_mr_cols = qi_images_cols;
	qi_images_mr_cols << QPair<QString,QString>("\"triggerTime\"",                  "trigger_time");
	qi_images_mr_cols << QPair<QString,QString>("\"magneticFieldStrength\"",        "magnetic_field_strength");
	qi_images_mr_cols << QPair<QString,QString>("\"diffusionBValue\"",              "diffusion_b_value");
	qi_images_mr_cols << QPair<QString,QString>("\"diffusionGradientOrientation\"", "diffusion_gradient_orientation");

	QVector<QPair<QString,QString> > qi_motion_correction_cols;
	qi_motion_correction_cols << QPair<QString,QString>("\"seriesUID_moving\"", "series_uid_moving");
	qi_motion_correction_cols << QPair<QString,QString>("\"seriesUID_fixed\"" , "series_uid_fixed");
	qi_motion_correction_cols << QPair<QString,QString>("\"Xform\"",            "xform");
	qi_motion_correction_cols << QPair<QString,QString>("\"imageHeader\"",      "image_header");

	QVector<QPair<QString,QString> > qi_patients_cols;
	qi_patients_cols << QPair<QString,QString>("\"MRN\"",  "mrn");
	qi_patients_cols << QPair<QString,QString>("\"DOB\"",  "dob");
	qi_patients_cols << QPair<QString,QString>("\"name\"", "patient_name");

	QVector<QPair<QString,QString> > qi_series_cols;
	qi_series_cols << QPair<QString,QString>("\"MRN\"",                  "mrn");
	qi_series_cols << QPair<QString,QString>("\"seriesUID\"",            "series_uid");
	qi_series_cols << QPair<QString,QString>("\"studyUID\"",             "study_uid");
	qi_series_cols << QPair<QString,QString>("\"seriesNumber\"",         "series_number");
	qi_series_cols << QPair<QString,QString>("\"seriesDesc\"",           "series_desc");
	qi_series_cols << QPair<QString,QString>("\"date\"",                 "series_date");
	qi_series_cols << QPair<QString,QString>("\"time\"",                 "series_time");
	qi_series_cols << QPair<QString,QString>("\"numTemporalPositions\"", "num_temporal_positions");
	qi_series_cols << QPair<QString,QString>("\"imageOrientation\"",     "image_orientation");
	qi_series_cols << QPair<QString,QString>("\"spaceBetweenSlices\"",   "space_between_slices");

	QVector<QPair<QString,QString> > qi_series_thumbnails_cols;
	qi_series_thumbnails_cols << QPair<QString,QString>("\"seriesUID\"", "series_uid");
	qi_series_thumbnails_cols << QPair<QString,QString>("\"maxDim\"",    "max_dim");
	qi_series_thumbnails_cols << QPair<QString,QString>("\"minDim\"",    "min_dim");

	QVector<QPair<QString,QString> > qi_studies_cols;
	qi_studies_cols << QPair<QString,QString>("\"MRN\"",      "mrn");
	qi_studies_cols << QPair<QString,QString>("\"studyUID\"", "study_uid");
	qi_studies_cols << QPair<QString,QString>("\"date\"",     "study_date");
	qi_studies_cols << QPair<QString,QString>("\"time\"",     "study_time");

	QVector<QPair<QString,QString> > dicom_data_cols;
	dicom_data_cols << QPair<QString,QString>("\"fullHeader\"", "full_header");

	QMap<QString, QVector<QPair<QString,QString> > > tablemap;
	tablemap.insert("qi_images", qi_images_cols);
	tablemap.insert("qi_images_mg", qi_images_mg_cols);
	tablemap.insert("qi_images_us", qi_images_us_cols);
	tablemap.insert("qi_images_mr", qi_images_mr_cols);
	tablemap.insert("qi_motion_correction", qi_motion_correction_cols);
	tablemap.insert("qi_patients", qi_patients_cols);
	tablemap.insert("qi_series", qi_series_cols);
	tablemap.insert("qi_series_thumbnails", qi_series_thumbnails_cols);
	tablemap.insert("qi_studies", qi_studies_cols);
	tablemap.insert("dicom_data", dicom_data_cols);

	for(auto i = tablemap.constBegin(); i != tablemap.constEnd(); ++i) {
		for(const auto & col : i.value()) {
			statementList << QString( "ALTER TABLE %1 RENAME COLUMN %2 TO %3" ).arg(i.key(), col.first, col.second);
		}
	}

	statementList << QString( R"(CREATE VIEW qi_imagelocations AS select qi_images.series_uid AS series_uid,qi_images.image_uid AS image_uid,qi_images.temporal_pos_id AS temporal_pos_id,cast(qi_images.slice_location as decimal(40,30)) AS location from qi_images order by qi_images.series_uid,qi_images.temporal_pos_id,cast(qi_images.slice_location as decimal(40,30)) )" );

	return statementList;
}

/*!
 * \brief Update from 110 to 200.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer110ToVer200() const
{
	QStringList statementList;

	QVector<QPair<QString,QString> > qi_images_cols;
	qi_images_cols << QPair<QString,QString>("\"MRN\"",                       "mrn");
	qi_images_cols << QPair<QString,QString>("\"imageUID\"",                  "image_uid");
	qi_images_cols << QPair<QString,QString>("\"seriesUID\"",                 "series_uid");
	qi_images_cols << QPair<QString,QString>("\"studyUID\"",                  "study_uid");
	qi_images_cols << QPair<QString,QString>("\"date\"",                      "image_date");
	qi_images_cols << QPair<QString,QString>("\"time\"",                      "image_time");
	qi_images_cols << QPair<QString,QString>("\"imageType\"",                 "image_type");
	qi_images_cols << QPair<QString,QString>("\"windowWidth\"",               "window_width");
	qi_images_cols << QPair<QString,QString>("\"windowCenter\"",              "window_center");
	qi_images_cols << QPair<QString,QString>("\"minPixval\"",                 "min_pixval");
	qi_images_cols << QPair<QString,QString>("\"maxPixval\"",                 "max_pixval");
	qi_images_cols << QPair<QString,QString>("\"imagePosition\"",             "image_position");
	qi_images_cols << QPair<QString,QString>("\"sliceLocation\"",             "slice_location");
	qi_images_cols << QPair<QString,QString>("\"temporalPosID\"",             "temporal_pos_id");
	qi_images_cols << QPair<QString,QString>("\"instanceNum\"",               "instance_num");
	qi_images_cols << QPair<QString,QString>("\"scanOptions\"",               "scan_options");
	qi_images_cols << QPair<QString,QString>("\"viewPosition\"",              "view_position");
	qi_images_cols << QPair<QString,QString>("\"imageLaterality\"",           "image_laterality");
	qi_images_cols << QPair<QString,QString>("\"patientOrientation\"",        "patient_orientation");
	qi_images_cols << QPair<QString,QString>("\"photometricInterpretation\"", "photometric_interpretation");
	qi_images_cols << QPair<QString,QString>("\"modelName\"",                 "model_name");

	statementList << QString("ALTER TABLE qi_images DROP COLUMN \"diffusionBValue\"");
	statementList << QString("ALTER TABLE qi_images DROP COLUMN \"diffusionGradientOrientation\"");

	QVector<QPair<QString,QString> > qi_images_mg_cols = qi_images_cols;
	qi_images_mg_cols << QPair<QString,QString>("\"numberOfFrames\"", "number_of_frames");

	statementList << QString("ALTER TABLE qi_images_mg DROP COLUMN \"diffusionBValue\"");
	statementList << QString("ALTER TABLE qi_images_mg DROP COLUMN \"diffusionGradientOrientation\"");

	QVector<QPair<QString,QString> > qi_images_us_cols = qi_images_mg_cols;

	statementList << QString("ALTER TABLE qi_images_us DROP COLUMN \"diffusionBValue\"");
	statementList << QString("ALTER TABLE qi_images_us DROP COLUMN \"diffusionGradientOrientation\"");

	QVector<QPair<QString,QString> > qi_images_mr_cols = qi_images_cols;
	qi_images_mr_cols << QPair<QString,QString>("\"triggerTime\"",                  "trigger_time");
	qi_images_mr_cols << QPair<QString,QString>("\"magneticFieldStrength\"",        "magnetic_field_strength");
	qi_images_mr_cols << QPair<QString,QString>("\"diffusionBValue\"",              "diffusion_b_value");
	qi_images_mr_cols << QPair<QString,QString>("\"diffusionGradientOrientation\"", "diffusion_gradient_orientation");

	QVector<QPair<QString,QString> > qi_motion_correction_cols;
	qi_motion_correction_cols << QPair<QString,QString>("\"seriesUID_moving\"", "series_uid_moving");
	qi_motion_correction_cols << QPair<QString,QString>("\"seriesUID_fixed\"" , "series_uid_fixed");
	qi_motion_correction_cols << QPair<QString,QString>("\"Xform\"",            "xform");
	qi_motion_correction_cols << QPair<QString,QString>("\"imageHeader\"",      "image_header");

	QVector<QPair<QString,QString> > qi_patients_cols;
	qi_patients_cols << QPair<QString,QString>("\"MRN\"",  "mrn");
	qi_patients_cols << QPair<QString,QString>("\"DOB\"",  "dob");
	qi_patients_cols << QPair<QString,QString>("\"name\"", "patient_name");

	QVector<QPair<QString,QString> > qi_series_cols;
	qi_series_cols << QPair<QString,QString>("\"MRN\"",                  "mrn");
	qi_series_cols << QPair<QString,QString>("\"seriesUID\"",            "series_uid");
	qi_series_cols << QPair<QString,QString>("\"studyUID\"",             "study_uid");
	qi_series_cols << QPair<QString,QString>("\"seriesNumber\"",         "series_number");
	qi_series_cols << QPair<QString,QString>("\"seriesDesc\"",           "series_desc");
	qi_series_cols << QPair<QString,QString>("\"date\"",                 "series_date");
	qi_series_cols << QPair<QString,QString>("\"time\"",                 "series_time");
	qi_series_cols << QPair<QString,QString>("\"numTemporalPositions\"", "num_temporal_positions");
	qi_series_cols << QPair<QString,QString>("\"imageOrientation\"",     "image_orientation");
	qi_series_cols << QPair<QString,QString>("\"spaceBetweenSlices\"",   "space_between_slices");

	QVector<QPair<QString,QString> > qi_series_thumbnails_cols;
	qi_series_thumbnails_cols << QPair<QString,QString>("\"seriesUID\"", "series_uid");
	qi_series_thumbnails_cols << QPair<QString,QString>("\"maxDim\"",    "max_dim");
	qi_series_thumbnails_cols << QPair<QString,QString>("\"minDim\"",    "min_dim");

	QVector<QPair<QString,QString> > qi_studies_cols;
	qi_studies_cols << QPair<QString,QString>("\"MRN\"",      "mrn");
	qi_studies_cols << QPair<QString,QString>("\"studyUID\"", "study_uid");
	qi_studies_cols << QPair<QString,QString>("\"date\"",     "study_date");
	qi_studies_cols << QPair<QString,QString>("\"time\"",     "study_time");

	QVector<QPair<QString,QString> > dicom_data_cols;
	dicom_data_cols << QPair<QString,QString>("\"fullHeader\"", "full_header");

	QMap<QString, QVector<QPair<QString,QString> > > tablemap;
	tablemap.insert("qi_images", qi_images_cols);
	tablemap.insert("qi_images_mg", qi_images_mg_cols);
	tablemap.insert("qi_images_us", qi_images_us_cols);
	tablemap.insert("qi_images_mr", qi_images_mr_cols);
	tablemap.insert("qi_motion_correction", qi_motion_correction_cols);
	tablemap.insert("qi_patients", qi_patients_cols);
	tablemap.insert("qi_series", qi_series_cols);
	tablemap.insert("qi_series_thumbnails", qi_series_thumbnails_cols);
	tablemap.insert("qi_studies", qi_studies_cols);
	tablemap.insert("dicom_data", dicom_data_cols);

	for(auto i = tablemap.constBegin(); i != tablemap.constEnd(); ++i) {
		for(const auto & col : i.value()) {
			statementList << QString( "ALTER TABLE %1 RENAME COLUMN %2 TO %3" ).arg(i.key(), col.first, col.second);
		}
	}

	return statementList;
}

/*!
 * \brief Update from 200 to 201.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatements::updateFromVer200ToVer201() const
{
	return QStringList{ R"(ALTER TABLE "qi_studies" ADD COLUMN "accession_number" text DEFAULT NULL)",
		                R"(ALTER TABLE "dicom_data" ADD COLUMN "0008,0050" text DEFAULT NULL)" };
}

/*!
 * \brief Update from 201 to 202.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatements::updateFromVer201ToVer202() const
{
	return QStringList{ R"(ALTER TABLE "dicom_data" ADD COLUMN "0008,0090" text DEFAULT NULL)",
		                R"(UPDATE "dicom_data" SET "0008,0090" = "0032,1032")" }; // Set referring physician to requesting physician
}

/*!
 * \brief Update from 202 to 300 for Postgres.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer202ToVer300() const
{
	return QStringList{
		// Renaming tables
		R"(ALTER TABLE "qi_lesiondetails"
		   RENAME TO "pre_kondo_qi_lesiondetails")",
		R"(ALTER TABLE "qi_lesions"
		   RENAME TO "pre_kondo_qi_lesions")",
		R"(ALTER TABLE "qi_motion_correction"
		   RENAME TO "pre_kondo_qi_motion_correction")",
		R"(ALTER TABLE "qi_patients"
		   RENAME TO "pre_kondo_qi_patients")",
		R"(ALTER TABLE "qi_series"
		   RENAME TO "pre_kondo_qi_series")",
		R"(ALTER TABLE "qi_series_thumbnails"
		   RENAME TO "pre_kondo_qi_series_thumbnails")",
		R"(ALTER TABLE "qi_studies"
		   RENAME TO "pre_kondo_qi_studies")",
		// Creating new tables with foreign key constraints //
		// 2DImages
		R"_stmnt_(create table "qi_2d_images" (
		"id" bigserial primary key ,
		"version" integer not null,
		"mrn" text,
		"image_uid" text,
		"series_uid" text,
		"study_uid" text,
		"modality" text,
		"image_data" bytea not null,
		"image_date" date,
		"image_time" interval,
		"width" integer,
		"height" integer,
		"pixelsize_x" double precision,
		"pixelsize_y" double precision,
		"image_type" text,
		"window_width" text,
		"window_center" text,
		"min_pixval" text,
		"max_pixval" text,
		"image_position" text,
		"slice_location" text,
		"temporal_pos_id" text,
		"diffusion_b_value" text,
		"diffusion_gradient_orientation" text,
		"instance_num" text,
		"scan_options" text,
		"view_position" text,
		"image_laterality" text,
		"patient_orientation" text,
		"photometric_interpretation" text,
		"manufacturer" text,
		"model_name" text,
		"trigger_time" integer,
		"magnetic_field_strength" real,
		"number_of_frames" int,
		"qi_series_id" bigint
	  )
	  )_stmnt_",
		// Lesions
		R"_stmnt_(create table "qi_lesions" (
		"id" bigserial primary key ,
		"version" integer not null,
		"uuid" text not null,
		"session" text not null,
		"mrn" text not null,
		"studyuid" text not null,
		"accession_number" text,
		"seriesuid" text not null,
		"qi_score" double precision not null,
		"volume" double precision not null,
		"surface_area" double precision not null,
		"seed_x" integer not null,
		"seed_y" integer not null,
		"seed_z" integer not null,
		"seed_realworld_x" real not null,
		"seed_realworld_y" real not null,
		"seed_realworld_z" real not null,
		"timepoints" bytea not null,
		"maxuptake" bytea not null,
		"avguptake" bytea not null,
		"qi_series_id" bigint
	  ))_stmnt_",
		// Lesion Details
		R"_stmnt_(create table "qi_lesiondetails" (
		"id" bigserial primary key ,
		"version" integer not null,
		"uuid" text not null,
		"features" bytea not null,
		"segmentation_outline" bytea not null,
		"segmentation_filled" bytea not null,
		"rawdata" bytea not null,
		"similarcases" bytea not null,
		"qi_lesions_id" bigint
	  ))_stmnt_",
		 // Motion Correction
		R"_stmnt_(create table "qi_motion_correction" (
		"id" bigserial primary key ,
		"version" integer not null,
		"series_uid_moving" text not null,
		"series_uid_fixed" text not null,
		"t_fixed" integer,
		"xform" bytea not null,
		"image_header" bytea not null,
		"default_value" real,
		"qi_series_id" bigint
	  ))_stmnt_",
		// Patient
		R"_stmnt_(create table "qi_patients" (
		"id" bigserial primary key ,
		"version" integer not null,
		"mrn" text not null,
		"patient_name" text not null,
		"dob" date,
		"sex" text not null
	  ))_stmnt_",
		// Dicom Series
		R"_stmnt_(create table "qi_series" (
		"id" bigserial primary key ,
		"version" integer not null,
		"mrn" text not null,
		"series_uid" text not null,
		"study_uid" text not null,
		"series_number" text not null,
		"modality" text not null,
		"series_desc" text not null,
		"series_date" date,
		"series_time" interval,
		"num_temporal_positions" integer,
		"image_orientation" text not null,
		"space_between_slices" text not null,
		"qi_studies_id" bigint
	  ))_stmnt_",
		// Series Thumbnails
		R"_stmnt_(create table "qi_series_thumbnails" (
		"id" bigserial primary key ,
		"version" integer not null,
		"series_uid" text not null,
		"max_dim" integer not null,
		"min_dim" integer not null,
		"width" integer not null,
		"height" integer not null,
		"thumbnail" bytea not null,
		"qi_series_id" bigint
	  ))_stmnt_",
		// Dicom Studies
		R"_stmnt_(create table "qi_studies" (
		"id" bigserial primary key ,
		"version" integer not null,
		"mrn" text not null,
		"study_uid" text not null,
		"modality" text not null,
		"study_date" date,
		"study_time" interval,
		"accession_number" text,
		"qi_patients_id" bigint
	  ))_stmnt_",
		R"_stmnt_(alter table "qi_series" add constraint "fk_qi_series_qi_studies" foreign key ("qi_studies_id") references "qi_studies" ("id") deferrable initially deferred)_stmnt_",
		R"_stmnt_(alter table "qi_studies" add constraint "fk_qi_studies_qi_patients" foreign key ("qi_patients_id") references "qi_patients" ("id") deferrable initially deferred)_stmnt_",
		R"_stmnt_(alter table "qi_2d_images" add constraint "fk_qi_2d_images_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred)_stmnt_",
		R"_stmnt_(alter table "qi_lesions" add constraint "fk_qi_lesions_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred)_stmnt_",
		R"_stmnt_(alter table "qi_lesiondetails" add constraint "fk_qi_lesiondetails_qi_lesions" foreign key ("qi_lesions_id") references "qi_lesions" ("id") deferrable initially deferred)_stmnt_",
		R"_stmnt_(alter table "qi_motion_correction" add constraint "fk_qi_motion_correction_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred)_stmnt_",
		R"_stmnt_(alter table "qi_series_thumbnails" add constraint "fk_qi_series_thumbnails_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred)_stmnt_",
		// Transfer Data Statements //
		// Patients
		R"_stmnt_(
		    INSERT INTO qi_patients
		    SELECT pre_kondo_qi_patients.idqi_patients, 0, pre_kondo_qi_patients.mrn, pre_kondo_qi_patients.patient_name, pre_kondo_qi_patients.dob, pre_kondo_qi_patients.sex
		    FROM pre_kondo_qi_patients
		)_stmnt_",
		// Studies
		R"_stmnt_(
		    INSERT INTO qi_studies
		    SELECT pre_kondo_qi_studies.idqi_studies, 0, pre_kondo_qi_studies.mrn, pre_kondo_qi_studies.study_uid, pre_kondo_qi_studies.modality, pre_kondo_qi_studies.study_date, pre_kondo_qi_studies.study_time, pre_kondo_qi_studies.accession_number, pre_kondo_qi_patients.idqi_patients
		    FROM pre_kondo_qi_studies
		    INNER JOIN pre_kondo_qi_patients
		    ON pre_kondo_qi_studies.mrn = pre_kondo_qi_patients.mrn
		)_stmnt_",
		// Series
		R"_stmnt_(
		    INSERT INTO qi_series
		    SELECT pre_kondo_qi_series.idqi_series, 0, pre_kondo_qi_series.mrn, pre_kondo_qi_series.series_uid, pre_kondo_qi_series.study_uid, pre_kondo_qi_series.series_number, pre_kondo_qi_series.modality, pre_kondo_qi_series.series_desc, pre_kondo_qi_series.series_date, pre_kondo_qi_series.series_time, pre_kondo_qi_series.num_temporal_positions, pre_kondo_qi_series.image_orientation, pre_kondo_qi_series.space_between_slices, pre_kondo_qi_studies.idqi_studies
		    FROM pre_kondo_qi_series
		    INNER JOIN pre_kondo_qi_studies
		    ON pre_kondo_qi_series.study_uid = pre_kondo_qi_studies.study_uid
		)_stmnt_",
		// SeriesThumbnails
		R"_stmnt_(
		    INSERT INTO qi_series_thumbnails
		    SELECT pre_kondo_qi_series_thumbnails.idqi_series_thumbnails, 0, pre_kondo_qi_series_thumbnails.series_uid, pre_kondo_qi_series_thumbnails.max_dim, pre_kondo_qi_series_thumbnails.min_dim, pre_kondo_qi_series_thumbnails.width, pre_kondo_qi_series_thumbnails.height, pre_kondo_qi_series_thumbnails.thumbnail, pre_kondo_qi_series.idqi_series
		    FROM pre_kondo_qi_series_thumbnails
		    INNER JOIN pre_kondo_qi_series
		    ON pre_kondo_qi_series_thumbnails.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// Motion Correction
		R"_stmnt_(
		    INSERT INTO qi_motion_correction
		    SELECT pre_kondo_qi_motion_correction.idqi_motion_correction, 0, pre_kondo_qi_motion_correction.series_uid_moving, pre_kondo_qi_motion_correction.series_uid_fixed, pre_kondo_qi_motion_correction.t_fixed, pre_kondo_qi_motion_correction.xform, pre_kondo_qi_motion_correction.image_header, pre_kondo_qi_motion_correction.default_value, pre_kondo_qi_series.idqi_series
		    FROM pre_kondo_qi_motion_correction
		    INNER JOIN pre_kondo_qi_series
		    ON pre_kondo_qi_motion_correction.series_uid_fixed = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// Image
		R"_stmnt_(
		    INSERT INTO  qi_2d_images (version, mrn, image_uid, series_uid, study_uid, modality, image_data, image_date, image_time, width, height, pixelsize_x, pixelsize_y, image_type, window_width, window_center, min_pixval, max_pixval, image_position, slice_location, temporal_pos_id, instance_num, scan_options, view_position, image_laterality, patient_orientation, photometric_interpretation, manufacturer, model_name, qi_series_id)
		    SELECT  0, qi_images.mrn, qi_images.image_uid, qi_images.series_uid, qi_images.study_uid, 'OT', qi_images.image_data, qi_images.image_date, qi_images.image_time, qi_images.width, qi_images.height, qi_images.pixelsize_x, qi_images.pixelsize_y, qi_images.image_type, qi_images.window_width, qi_images.window_center, qi_images.min_pixval, qi_images.max_pixval, qi_images.image_position, qi_images.slice_location, qi_images.temporal_pos_id, qi_images.instance_num, qi_images.scan_options, qi_images.view_position, qi_images.image_laterality, qi_images.patient_orientation, qi_images.photometric_interpretation, qi_images.manufacturer, qi_images.model_name, pre_kondo_qi_series.idqi_series
		    FROM qi_images
		    INNER JOIN pre_kondo_qi_series
		    ON qi_images.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// Mammo
		R"_stmnt_(
		    INSERT INTO  qi_2d_images (version, mrn, image_uid, series_uid, study_uid, modality, image_data, image_date, image_time, width, height, pixelsize_x, pixelsize_y, image_type, window_width, window_center, min_pixval, max_pixval, image_position, slice_location, temporal_pos_id, instance_num, scan_options, view_position, image_laterality, patient_orientation, photometric_interpretation, manufacturer, model_name, number_of_frames, qi_series_id)
		    SELECT  0, qi_images_mg.mrn, qi_images_mg.image_uid, qi_images_mg.series_uid, qi_images_mg.study_uid, 'MG', qi_images_mg.image_data, qi_images_mg.image_date, qi_images_mg.image_time, qi_images_mg.width, qi_images_mg.height, qi_images_mg.pixelsize_x, qi_images_mg.pixelsize_y, qi_images_mg.image_type, qi_images_mg.window_width, qi_images_mg.window_center, qi_images_mg.min_pixval, qi_images_mg.max_pixval, qi_images_mg.image_position, qi_images_mg.slice_location, qi_images_mg.temporal_pos_id, qi_images_mg.instance_num, qi_images_mg.scan_options, qi_images_mg.view_position, qi_images_mg.image_laterality, qi_images_mg.patient_orientation, qi_images_mg.photometric_interpretation, qi_images_mg.manufacturer, qi_images_mg.model_name, qi_images_mg.number_of_frames, pre_kondo_qi_series.idqi_series
		    FROM qi_images_mg
		    INNER JOIN pre_kondo_qi_series
		    ON qi_images_mg.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// US
		R"_stmnt_(
		    INSERT INTO  qi_2d_images (version, mrn, image_uid, series_uid, study_uid, modality, image_data, image_date, image_time, width, height, pixelsize_x, pixelsize_y, image_type, window_width, window_center, min_pixval, max_pixval, image_position, slice_location, temporal_pos_id, instance_num, scan_options, view_position, image_laterality, patient_orientation, photometric_interpretation, manufacturer, model_name, number_of_frames, qi_series_id)
		    SELECT  0, qi_images_us.mrn, qi_images_us.image_uid, qi_images_us.series_uid, qi_images_us.study_uid, 'US', qi_images_us.image_data, qi_images_us.image_date, qi_images_us.image_time, qi_images_us.width, qi_images_us.height, qi_images_us.pixelsize_x, qi_images_us.pixelsize_y, qi_images_us.image_type, qi_images_us.window_width, qi_images_us.window_center, qi_images_us.min_pixval, qi_images_us.max_pixval, qi_images_us.image_position, qi_images_us.slice_location, qi_images_us.temporal_pos_id, qi_images_us.instance_num, qi_images_us.scan_options, qi_images_us.view_position, qi_images_us.image_laterality, qi_images_us.patient_orientation, qi_images_us.photometric_interpretation, qi_images_us.manufacturer, qi_images_us.model_name, qi_images_us.number_of_frames, pre_kondo_qi_series.idqi_series
		    FROM qi_images_us
		    INNER JOIN pre_kondo_qi_series
		    ON qi_images_us.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// MR
		R"_stmnt_(
		    INSERT INTO  qi_2d_images (version, mrn, image_uid, series_uid, study_uid, modality, image_data, image_date, image_time, width, height, pixelsize_x, pixelsize_y, image_type, window_width, window_center, min_pixval, max_pixval, image_position, slice_location, temporal_pos_id, diffusion_b_value, diffusion_gradient_orientation, instance_num, scan_options, view_position, image_laterality, patient_orientation, photometric_interpretation, manufacturer, model_name, qi_series_id, trigger_time, magnetic_field_strength)
		    SELECT  0, qi_images_mr.mrn, qi_images_mr.image_uid, qi_images_mr.series_uid, qi_images_mr.study_uid, 'MR', qi_images_mr.image_data, qi_images_mr.image_date, qi_images_mr.image_time, qi_images_mr.width, qi_images_mr.height, qi_images_mr.pixelsize_x, qi_images_mr.pixelsize_y, qi_images_mr.image_type, qi_images_mr.window_width, qi_images_mr.window_center, qi_images_mr.min_pixval, qi_images_mr.max_pixval, qi_images_mr.image_position, qi_images_mr.slice_location, qi_images_mr.temporal_pos_id, qi_images_mr.diffusion_b_value, qi_images_mr.diffusion_gradient_orientation, qi_images_mr.instance_num, qi_images_mr.scan_options, qi_images_mr.view_position, qi_images_mr.image_laterality, qi_images_mr.patient_orientation, qi_images_mr.photometric_interpretation, qi_images_mr.manufacturer, qi_images_mr.model_name, pre_kondo_qi_series.idqi_series, qi_images_mr.trigger_time, qi_images_mr.magnetic_field_strength
		    FROM qi_images_mr
		    INNER JOIN pre_kondo_qi_series
		    ON qi_images_mr.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// Lesions
		R"_stmnt_(
		    INSERT INTO qi_lesions
		    SELECT pre_kondo_qi_lesions.id, pre_kondo_qi_lesions.version, pre_kondo_qi_lesions.uuid, pre_kondo_qi_lesions.session, pre_kondo_qi_lesions.mrn, pre_kondo_qi_lesions.studyuid, pre_kondo_qi_lesions.accession_number, pre_kondo_qi_lesions.seriesuid, pre_kondo_qi_lesions.qi_score, pre_kondo_qi_lesions.volume, pre_kondo_qi_lesions.surface_area, pre_kondo_qi_lesions.seed_x, pre_kondo_qi_lesions.seed_y, pre_kondo_qi_lesions.seed_z, pre_kondo_qi_lesions.seed_realworld_x, pre_kondo_qi_lesions.seed_realworld_y, pre_kondo_qi_lesions.seed_realworld_z, pre_kondo_qi_lesions.timepoints, pre_kondo_qi_lesions.maxuptake, pre_kondo_qi_lesions.avguptake
		    FROM pre_kondo_qi_lesions
		)_stmnt_",
		// LesionDetails
		R"_stmnt_(
		    INSERT INTO qi_lesiondetails
		    SELECT pre_kondo_qi_lesiondetails.id, pre_kondo_qi_lesiondetails.version, pre_kondo_qi_lesiondetails.uuid, pre_kondo_qi_lesiondetails.features, pre_kondo_qi_lesiondetails.segmentation_outline, pre_kondo_qi_lesiondetails.segmentation_filled, pre_kondo_qi_lesiondetails.rawdata, pre_kondo_qi_lesiondetails.similarcases
		    FROM pre_kondo_qi_lesiondetails
		    INNER JOIN pre_kondo_qi_lesions
		    ON pre_kondo_qi_lesiondetails.uuid = pre_kondo_qi_lesions.uuid
		)_stmnt_",

#if DROP_TABLES
		,
		R"_stmnt_(DROP TABLE pre_kondo_qi_lesiondetails)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_lesions)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_motion_correction)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_patients)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_series)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_series_thumbnails)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_studies)_stmnt_"
#endif
	};
}

/*!
 * \brief Update from 202 to 300 for SQLite.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer202ToVer300() const
{
	return QStringList{
		// Create Lesion Tables if not exist
		R"_stmnt_( CREATE TABLE IF NOT EXISTS "qi_lesiondetails"
		(
		  "id" integer primary key autoincrement,
		  "version" integer not null,
		  "uuid" text not null,
		  "features" blob not null,
		  "segmentation_outline" blob not null,
		  "segmentation_filled" blob not null,
		  "rawdata" blob not null,
		  "similarcases" blob not null
		)
		)_stmnt_",
		R"_stmnt_(
		CREATE TABLE IF NOT EXISTS "qi_lesions" (
		  "id" integer primary key autoincrement,
		  "version" integer not null,
		  "uuid" text not null,
		  "session" text not null,
		  "mrn" text not null,
		  "studyuid" text not null,
		  "accession_number" text not null,
		  "seriesuid" text not null,
		  "qi_score" double precision not null,
		  "volume" double precision not null,
		  "surface_area" double precision not null,
		  "seed_x" integer not null,
		  "seed_y" integer not null,
		  "seed_z" integer not null,
		  "seed_realworld_x" real not null,
		  "seed_realworld_y" real not null,
		  "seed_realworld_z" real not null,
		  "timepoints" blob not null,
		  "maxuptake" blob not null,
		  "avguptake" blob not null
		)
		)_stmnt_",
		// Renaming tables
		R"(ALTER TABLE "qi_lesiondetails"
		   RENAME TO "pre_kondo_qi_lesiondetails")",
		R"(ALTER TABLE "qi_lesions"
		   RENAME TO "pre_kondo_qi_lesions")",
		R"(ALTER TABLE "qi_motion_correction"
		   RENAME TO "pre_kondo_qi_motion_correction")",
		R"(ALTER TABLE "qi_patients"
		   RENAME TO "pre_kondo_qi_patients")",
		R"(ALTER TABLE "qi_series"
		   RENAME TO "pre_kondo_qi_series")",
		R"(ALTER TABLE "qi_series_thumbnails"
		   RENAME TO "pre_kondo_qi_series_thumbnails")",
		R"(ALTER TABLE "qi_studies"
		   RENAME TO "pre_kondo_qi_studies")",
		// Creating new tables with foreign key constraints //
		// 2DImages
		R"_stmnt_(create table "qi_2d_images" (
		"id" integer primary key autoincrement,
		"version" integer not null,
		"mrn" text,
		"image_uid" text,
		"series_uid" text,
		"study_uid" text,
		"modality" text,
		"image_data" blob not null,
		"image_date" text,
		"image_time" integer,
		"width" integer,
		"height" integer,
		"pixelsize_x" double precision,
		"pixelsize_y" double precision,
		"image_type" text,
		"window_width" text,
		"window_center" text,
		"min_pixval" text,
		"max_pixval" text,
		"image_position" text,
		"slice_location" text,
		"temporal_pos_id" text,
		"diffusion_b_value" text,
		"diffusion_gradient_orientation" text,
		"instance_num" text,
		"scan_options" text,
		"view_position" text,
		"image_laterality" text,
		"patient_orientation" text,
		"photometric_interpretation" text,
		"manufacturer" text,
		"model_name" text,
		"trigger_time" integer,
		"magnetic_field_strength" real,
		"number_of_frames" int,
		"qi_series_id" bigint,
		constraint "fk_qi_2d_images_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred)
	  )_stmnt_",
		// Lesions
		R"_stmnt_(create table "qi_lesions" (
		"id" integer primary key autoincrement,
		"version" integer not null,
		"uuid" text,
		"session" text,
		"mrn" text,
		"studyuid" text,
		"accession_number" text,
		"seriesuid" text,
		"qi_score" double precision not null,
		"volume" double precision not null,
		"surface_area" double precision not null,
		"seed_x" integer not null,
		"seed_y" integer not null,
		"seed_z" integer not null,
		"seed_realworld_x" real not null,
		"seed_realworld_y" real not null,
		"seed_realworld_z" real not null,
		"timepoints" blob not null,
		"maxuptake" blob not null,
		"avguptake" blob not null,
		"qi_series_id" bigint,
		constraint "fk_qi_lesions_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred
	  ))_stmnt_",
		// Lesion Details
		R"_stmnt_(create table "qi_lesiondetails" (
		"id" integer primary key autoincrement,
		"version" integer not null,
		"uuid" text not null,
		"features" blob not null,
		"segmentation_outline" blob not null,
		"segmentation_filled" blob not null,
		"rawdata" blob not null,
		"similarcases" blob not null,
		"qi_lesions_id" bigint,
		constraint "fk_qi_lesiondetails_qi_lesions" foreign key ("qi_lesions_id") references "qi_lesions" ("id") deferrable initially deferred
	  ))_stmnt_",
		 // Motion Correction
		R"_stmnt_(create table "qi_motion_correction" (
		"id" integer primary key autoincrement,
		"version" integer not null,
		"series_uid_moving" text,
		"series_uid_fixed" text,
		"t_fixed" integer,
		"xform" blob not null,
		"image_header" blob not null,
		"default_value" real,
		"qi_series_id" bigint,
		constraint "fk_qi_motion_correction_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred
	  ))_stmnt_",
		// Patient
		R"_stmnt_(create table "qi_patients" (
		"id" integer primary key autoincrement,
		"version" integer not null,
		"mrn" text,
		"patient_name" text,
		"dob" text,
		"sex" text
	  ))_stmnt_",
		// Dicom Series
		R"_stmnt_(create table "qi_series" (
		"id" integer primary key autoincrement,
		"version" integer not null,
		"mrn" text,
		"series_uid" text,
		"study_uid" text,
		"series_number" text,
		"modality" text,
		"series_desc" text,
		"series_date" text,
		"series_time" integer,
		"num_temporal_positions" integer,
		"image_orientation" text,
		"space_between_slices" text,
		"qi_studies_id" bigint,
		constraint "fk_qi_series_qi_studies" foreign key ("qi_studies_id") references "qi_studies" ("id") deferrable initially deferred
	  ))_stmnt_",
		// Series Thumbnails
		R"_stmnt_(create table "qi_series_thumbnails" (
		"id" integer primary key autoincrement,
		"version" integer not null,
		"series_uid" text not null,
		"max_dim" integer not null,
		"min_dim" integer not null,
		"width" integer not null,
		"height" integer not null,
		"thumbnail" blob not null,
		"qi_series_id" bigint,
		constraint "fk_qi_series_thumbnails_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred
	  ))_stmnt_",
		// Dicom Studies
		R"_stmnt_(create table "qi_studies" (
		"id" integer primary key autoincrement,
		"version" integer not null,
		"mrn" text,
		"study_uid" text,
		"modality" text,
		"study_date" text,
		"study_time" integer,
		"accession_number" text,
		"qi_patients_id" bigint,
		constraint "fk_qi_studies_qi_patients" foreign key ("qi_patients_id") references "qi_patients" ("id") deferrable initially deferred
	  ))_stmnt_",
		// Transfer Data Statements //
		// LesionDetails
		R"_stmnt_(
		    INSERT INTO qi_lesiondetails
		    SELECT pre_kondo_qi_lesiondetails.id, pre_kondo_qi_lesiondetails.version, pre_kondo_qi_lesiondetails.uuid, pre_kondo_qi_lesiondetails.features, pre_kondo_qi_lesiondetails.segmentation_outline, pre_kondo_qi_lesiondetails.segmentation_filled, pre_kondo_qi_lesiondetails.rawdata, pre_kondo_qi_lesiondetails.similarcases
		    FROM pre_kondo_qi_lesiondetails
		    INNER JOIN pre_kondo_qi_lesions
		    ON pre_kondo_qi_lesiondetails.uuid = pre_kondo_qi_lesions.uuid
		)_stmnt_",
		// Lesions
		R"_stmnt_(
		    INSERT INTO qi_lesions
		    SELECT pre_kondo_qi_lesions.id, pre_kondo_qi_lesions.version, pre_kondo_qi_lesions.uuid, pre_kondo_qi_lesions.session, pre_kondo_qi_lesions.mrn, pre_kondo_qi_lesions.studyuid, pre_kondo_qi_lesions.accession_number, pre_kondo_qi_lesions.seriesuid, pre_kondo_qi_lesions.qi_score, pre_kondo_qi_lesions.volume, pre_kondo_qi_lesions.surface_area, pre_kondo_qi_lesions.seed_x, pre_kondo_qi_lesions.seed_y, pre_kondo_qi_lesions.seed_z, pre_kondo_qi_lesions.seed_realworld_x, pre_kondo_qi_lesions.seed_realworld_y, pre_kondo_qi_lesions.seed_realworld_z, pre_kondo_qi_lesions.timepoints, pre_kondo_qi_lesions.maxuptake, pre_kondo_qi_lesions.avguptake
		    FROM pre_kondo_qi_lesions
		)_stmnt_",
		// Patients
		R"_stmnt_(
		    INSERT INTO qi_patients
		    SELECT pre_kondo_qi_patients.idqi_patients, 0, pre_kondo_qi_patients.mrn, pre_kondo_qi_patients.patient_name, pre_kondo_qi_patients.dob, pre_kondo_qi_patients.sex
		    FROM pre_kondo_qi_patients
		)_stmnt_",
		// Series
		R"_stmnt_(
		    INSERT INTO qi_series
		    SELECT pre_kondo_qi_series.idqi_series, 0, pre_kondo_qi_series.mrn, pre_kondo_qi_series.series_uid, pre_kondo_qi_series.study_uid, pre_kondo_qi_series.series_number, pre_kondo_qi_series.modality, pre_kondo_qi_series.series_desc, pre_kondo_qi_series.series_date, pre_kondo_qi_series.series_time, pre_kondo_qi_series.num_temporal_positions, pre_kondo_qi_series.image_orientation, pre_kondo_qi_series.space_between_slices, pre_kondo_qi_studies.idqi_studies
		    FROM pre_kondo_qi_series
		    INNER JOIN pre_kondo_qi_studies
		    ON pre_kondo_qi_series.study_uid = pre_kondo_qi_studies.study_uid
		)_stmnt_",
		// SeriesThumbnails
		R"_stmnt_(
		    INSERT INTO qi_series_thumbnails
		    SELECT pre_kondo_qi_series_thumbnails.idqi_series_thumbnails, 0, pre_kondo_qi_series_thumbnails.series_uid, pre_kondo_qi_series_thumbnails.max_dim, pre_kondo_qi_series_thumbnails.min_dim, pre_kondo_qi_series_thumbnails.width, pre_kondo_qi_series_thumbnails.height, pre_kondo_qi_series_thumbnails.thumbnail, pre_kondo_qi_series.idqi_series
		    FROM pre_kondo_qi_series_thumbnails
		    INNER JOIN pre_kondo_qi_series
		    ON pre_kondo_qi_series_thumbnails.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// Studies
		R"_stmnt_(
		    INSERT INTO qi_studies
		    SELECT pre_kondo_qi_studies.idqi_studies, 0, pre_kondo_qi_studies.mrn, pre_kondo_qi_studies.study_uid, pre_kondo_qi_studies.modality, pre_kondo_qi_studies.study_date, pre_kondo_qi_studies.study_time, pre_kondo_qi_studies.accession_number, pre_kondo_qi_patients.idqi_patients
		    FROM pre_kondo_qi_studies
		    INNER JOIN pre_kondo_qi_patients
		    ON pre_kondo_qi_studies.mrn = pre_kondo_qi_patients.mrn
		)_stmnt_",
		// Motion Correction
		R"_stmnt_(
		    INSERT INTO qi_motion_correction
		    SELECT pre_kondo_qi_motion_correction.idqi_motion_correction, 0, pre_kondo_qi_motion_correction.series_uid_moving, pre_kondo_qi_motion_correction.series_uid_fixed, pre_kondo_qi_motion_correction.t_fixed, pre_kondo_qi_motion_correction.xform, pre_kondo_qi_motion_correction.image_header, pre_kondo_qi_motion_correction.default_value, pre_kondo_qi_series.idqi_series
		    FROM pre_kondo_qi_motion_correction
		    INNER JOIN pre_kondo_qi_series
		    ON pre_kondo_qi_motion_correction.series_uid_fixed = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// Image
		R"_stmnt_(
		    INSERT INTO  qi_2d_images (version, mrn, image_uid, series_uid, study_uid, modality, image_data, image_date, image_time, width, height, pixelsize_x, pixelsize_y, image_type, window_width, window_center, min_pixval, max_pixval, image_position, slice_location, temporal_pos_id, diffusion_b_value, diffusion_gradient_orientation, instance_num, scan_options, view_position, image_laterality, patient_orientation, photometric_interpretation, manufacturer, model_name, qi_series_id)
		    SELECT  0, qi_images.mrn, qi_images.image_uid, qi_images.series_uid, qi_images.study_uid, qi_images.modality, qi_images.image_data, qi_images.image_date, qi_images.image_time, qi_images.width, qi_images.height, qi_images.pixelsize_x, qi_images.pixelsize_y, qi_images.image_type, qi_images.window_width, qi_images.window_center, qi_images.min_pixval, qi_images.max_pixval, qi_images.image_position, qi_images.slice_location, qi_images.temporal_pos_id, '', '', qi_images.instance_num, qi_images.scan_options, qi_images.view_position, qi_images.image_laterality, qi_images.patient_orientation, qi_images.photometric_interpretation, qi_images.manufacturer, qi_images.model_name, pre_kondo_qi_series.idqi_series
		    FROM qi_images
		    INNER JOIN pre_kondo_qi_series
		    ON qi_images.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// Mammo
		R"_stmnt_(
		    INSERT INTO  qi_2d_images (version, mrn, image_uid, series_uid, study_uid, modality, image_data, image_date, image_time, width, height, pixelsize_x, pixelsize_y, image_type, window_width, window_center, min_pixval, max_pixval, image_position, slice_location, temporal_pos_id, diffusion_b_value, diffusion_gradient_orientation, instance_num, scan_options, view_position, image_laterality, patient_orientation, photometric_interpretation, manufacturer, model_name, qi_series_id)
		    SELECT  0, qi_images_mg.mrn, qi_images_mg.image_uid, qi_images_mg.series_uid, qi_images_mg.study_uid, "MG", qi_images_mg.image_data, qi_images_mg.image_date, qi_images_mg.image_time, qi_images_mg.width, qi_images_mg.height, qi_images_mg.pixelsize_x, qi_images_mg.pixelsize_y, qi_images_mg.image_type, qi_images_mg.window_width, qi_images_mg.window_center, qi_images_mg.min_pixval, qi_images_mg.max_pixval, qi_images_mg.image_position, qi_images_mg.slice_location, qi_images_mg.temporal_pos_id, '', '', qi_images_mg.instance_num, qi_images_mg.scan_options, qi_images_mg.view_position, qi_images_mg.image_laterality, qi_images_mg.patient_orientation, qi_images_mg.photometric_interpretation, qi_images_mg.manufacturer, qi_images_mg.model_name, pre_kondo_qi_series.idqi_series
		    FROM qi_images_mg
		    INNER JOIN pre_kondo_qi_series
		    ON qi_images_mg.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// US
		R"_stmnt_(
		    INSERT INTO  qi_2d_images (version, mrn, image_uid, series_uid, study_uid, modality, image_data, image_date, image_time, width, height, pixelsize_x, pixelsize_y, image_type, window_width, window_center, min_pixval, max_pixval, image_position, slice_location, temporal_pos_id, diffusion_b_value, diffusion_gradient_orientation, instance_num, scan_options, view_position, image_laterality, patient_orientation, photometric_interpretation, manufacturer, model_name, qi_series_id)
		    SELECT  0, qi_images_us.mrn, qi_images_us.image_uid, qi_images_us.series_uid, qi_images_us.study_uid, "US", qi_images_us.image_data, qi_images_us.image_date, qi_images_us.image_time, qi_images_us.width, qi_images_us.height, qi_images_us.pixelsize_x, qi_images_us.pixelsize_y, qi_images_us.image_type, qi_images_us.window_width, qi_images_us.window_center, qi_images_us.min_pixval, qi_images_us.max_pixval, qi_images_us.image_position, qi_images_us.slice_location, qi_images_us.temporal_pos_id, '', '', qi_images_us.instance_num, qi_images_us.scan_options, qi_images_us.view_position, qi_images_us.image_laterality, qi_images_us.patient_orientation, qi_images_us.photometric_interpretation, qi_images_us.manufacturer, qi_images_us.model_name, pre_kondo_qi_series.idqi_series
		    FROM qi_images_us
		    INNER JOIN pre_kondo_qi_series
		    ON qi_images_us.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",
		// MR
		R"_stmnt_(
		    INSERT INTO  qi_2d_images (version, mrn, image_uid, series_uid, study_uid, modality, image_data, image_date, image_time, width, height, pixelsize_x, pixelsize_y, image_type, window_width, window_center, min_pixval, max_pixval, image_position, slice_location, temporal_pos_id, diffusion_b_value, diffusion_gradient_orientation, instance_num, scan_options, view_position, image_laterality, patient_orientation, photometric_interpretation, manufacturer, model_name, qi_series_id, trigger_time, magnetic_field_strength)
		    SELECT  0, qi_images_mr.mrn, qi_images_mr.image_uid, qi_images_mr.series_uid, qi_images_mr.study_uid, "MR", qi_images_mr.image_data, qi_images_mr.image_date, qi_images_mr.image_time, qi_images_mr.width, qi_images_mr.height, qi_images_mr.pixelsize_x, qi_images_mr.pixelsize_y, qi_images_mr.image_type, qi_images_mr.window_width, qi_images_mr.window_center, qi_images_mr.min_pixval, qi_images_mr.max_pixval, qi_images_mr.image_position, qi_images_mr.slice_location, qi_images_mr.temporal_pos_id, qi_images_mr.diffusion_b_value, qi_images_mr.diffusion_gradient_orientation, qi_images_mr.instance_num, qi_images_mr.scan_options, qi_images_mr.view_position, qi_images_mr.image_laterality, qi_images_mr.patient_orientation, qi_images_mr.photometric_interpretation, qi_images_mr.manufacturer, qi_images_mr.model_name, pre_kondo_qi_series.idqi_series, qi_images_mr.trigger_time, qi_images_mr.magnetic_field_strength
		    FROM qi_images_mr
		    INNER JOIN pre_kondo_qi_series
		    ON qi_images_mr.series_uid = pre_kondo_qi_series.series_uid
		)_stmnt_",

#if DROP_TABLES
		,
		R"_stmnt_(DROP TABLE pre_kondo_qi_lesiondetails)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_lesions)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_motion_correction)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_patients)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_series)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_series_thumbnails)_stmnt_",
		R"_stmnt_(DROP TABLE pre_kondo_qi_studies)_stmnt_"
#endif
	};
}

/*!
 * \brief Update from 300 to 301 for SQLite.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer300ToVer301() const
{
	return QStringList{
		// Convert time values to milliseconds since start of day and create indexes for the qi_2d_images table
		R"(UPDATE "qi_series" SET series_time = strftime("%H", series_time) * 60 * 60 * 1000 + strftime("%M", series_time) * 60 * 1000 + cast(cast(strftime("%f", series_time) as REAL) * 1000 as integer))",
		R"(UPDATE "qi_studies" SET study_time = strftime("%H", study_time) * 60 * 60 * 1000 + strftime("%M", study_time) * 60 * 1000 + cast(cast(strftime("%f", study_time) as REAL) * 1000 as integer))",
		R"(UPDATE "qi_2d_images" SET image_time = strftime("%H", image_time) * 60 * 60 * 1000 + strftime("%M", image_time) * 60 * 1000 + cast(cast(strftime("%f", image_time) as REAL) * 1000 as integer))",
		R"(CREATE INDEX "imageUID_idx_2D" ON "qi_2d_images" ("image_uid"))",
		R"(CREATE INDEX "seriesUID_idx_2D" ON "qi_2d_images" ("series_uid"))",
		R"(CREATE INDEX "foreignkey_idx_2D" ON "qi_2d_images" ("qi_series_id"))",
		R"(CREATE INDEX "seriesUIDtempPos_idx_2D" ON "qi_2d_images" ("series_uid","temporal_pos_id"))"
	};
}

/*!
 * \brief Update from 300 to 301 for Postgres.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer300ToVer301() const
{
	return QStringList{
		// Convert time values to milliseconds since start of day and create indexes for the qi_2d_images table
		//R"(UPDATE "qi_series" SET series_time = extract(HOUR from series_time) * 60 * 60 * 1000 + extract(MINUTE from series_time) * 60 * 1000 + extract(MILLISECONDS from series_time) )",
		//R"(UPDATE "qi_studies" SET study_time = extract(HOUR from study_time) * 60 * 60 * 1000 + extract(MINUTE from study_time) * 60 * 1000 + extract(MILLISECONDS from study_time) )",
		//R"(UPDATE "qi_2d_images" SET image_time = extract(HOUR from image_time) * 60 * 60 * 1000 + extract(MINUTE from image_time) * 60 * 1000 + extract(MILLISECONDS from image_time))",
		R"(CREATE INDEX "imageUID_idx_2D" ON "qi_2d_images" ("image_uid"))",
		R"(CREATE INDEX "seriesUID_idx_2D" ON "qi_2d_images" ("series_uid"))",
		R"(CREATE INDEX "foreignkey_idx_2D" ON "qi_2d_images" ("qi_series_id"))",
		R"(CREATE INDEX "seriesUIDtempPos_idx_2D" ON "qi_2d_images" ("series_uid","temporal_pos_id"))"
	};
}

/*!
 * \brief Update from 301 to 302 for SQLite.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer301ToVer302() const
{
	return QStringList{
		R"_stmnt_(
		create table "qi_overlay" (
		  "id" integer primary key autoincrement,
		  "version" integer not null,
		  "uuid" text not null,
		  "session" text not null,
		  "mrn" text not null,
		  "studyuid" text not null,
		  "seriesuid" text not null,
		  "accession_number" text not null,
		  "overlay_type_name" text not null,
		  "image_number" integer,
		  "overlay_image" blob not null,
		  "overlay_data" blob not null,
		  "qi_series_id" bigint,
		  constraint "fk_qi_overlay_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred
		)
		)_stmnt_"
	};
}

/*!
 * \brief Update 301 to 302 for Postgres.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer301ToVer302() const
{
	return QStringList{
		R"_stmnt_(
		create table "qi_overlay" (
		  "id" bigserial primary key,
		  "version" integer not null,
		  "uuid" text not null,
		  "session" text not null,
		  "mrn" text not null,
		  "studyuid" text not null,
		  "seriesuid" text not null,
		  "accession_number" text not null,
		  "overlay_type_name" text not null,
		  "image_number" integer,
		  "overlay_image" bytea not null,
		  "overlay_data" bytea not null,
		  "qi_series_id" bigint,
		  constraint "fk_qi_overlay_qi_series" foreign key ("qi_series_id") references "qi_series" ("id") deferrable initially deferred
		)
		)_stmnt_"
	};
}

/*!
 * \brief Update 302 to 303 for SQLite.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer302ToVer303() const
{
	return QStringList{
		R"_stmnt_(
		CREATE TABLE "qi_plugin_data" (
		    "id"	integer primary key autoincrement,
		    "version" integer not null,
		    "name"	text not null,
		    "session_id"  text not null,
		    "mrn"   text,
		    "key"   text not null,
		    "data"	blob not null
		)
		)_stmnt_"
	};
}

/*!
 * \brief Update 302 to 303 for Postgres.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer302ToVer303() const
{
	return QStringList{
		R"_stmnt_(
		CREATE TABLE "qi_plugin_data" (
		    "id"	bigserial primary key,
		    "version"  integer not null,
		    "name"	text not null,
		    "session_id"  text not null,
		    "mrn"   text,
		    "key"   text not null,
		    "data"	bytea not null
		)
		)_stmnt_"
	};
}

/*!
 * \brief Update 303 to 304 for SQLite.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer303ToVer304() const
{
	return QStringList{
		R"_stmnt_(
		create table "qi_acquisition_protocols" (
		  "id" integer primary key autoincrement,
		  "version" integer not null,
		  "protocol_number" integer not null,
		  "key_name" text not null,
		  "key_value" text not null
		)
		)_stmnt_"
	};
}

/*!
 * \brief Update 303 to 304 for Postgres.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer303ToVer304() const
{
	return QStringList{
		R"_stmnt_(
		create table "qi_acquisition_protocols" (
		  "id" bigserial primary key,
		  "version" integer not null,
		  "protocol_number" integer not null,
		  "key_name" text not null,
		  "key_value" text not null
		)
		)_stmnt_"
	};
}

/*!
 * \brief Update 304 to 310 for SQLite.
 * \return Empty query string.
 * \note This function exists to keep the versioning with postgres databases. However, this update only provides notification triggers which must be implemented via the C interface for SQLite.
 */
const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer304ToVer310() const
{
	return QStringList{};
}

/*!
 * \brief Update 304 to 310 for Postgres.
 * \return Query string.
 */
const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer304ToVer310() const
{
	return QStringList{
		R"_stmnt_(
		CREATE TABLE IF NOT EXISTS qi_jobs_status (
		    series_uid character varying NOT NULL,
		    status character varying,
		    jobtype character varying,
		    job_time timestamp(3) with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
		);
		)_stmnt_",
		R"_stmnt_(
		DROP TRIGGER IF EXISTS series_notify ON qi_series CASCADE;
		)_stmnt_",
		R"_stmnt_(
		DROP FUNCTION IF EXISTS series_insert_notify() CASCADE;
		)_stmnt_",
		R"_stmnt_(
		DROP TRIGGER IF EXISTS status_notify ON qi_jobs_status CASCADE;
		)_stmnt_",
		R"_stmnt_(
		DROP FUNCTION IF EXISTS status_update() CASCADE;
		)_stmnt_",
		R"_stmnt_(
		CREATE FUNCTION series_insert_notify() RETURNS trigger
		    LANGUAGE plpgsql
		    AS $$declare
		  channel text := TG_ARGV[0];
		begin
		 PERFORM (
		     select pg_notify(channel, CONCAT(NEW.study_uid, ',', NEW.series_uid, ',', NEW.modality, ',', NEW.num_temporal_positions, ',', NEW.series_desc)::text)
		  );
		  RETURN NULL;
		end;
		$$;
		)_stmnt_",
		R"_stmnt_(
		CREATE FUNCTION status_update() RETURNS trigger
		    LANGUAGE plpgsql
		    AS $$declare
		  channel text;
		  record RECORD;
		  status text;
		begin
		  IF (TG_OP = 'DELETE') THEN
		     record = OLD;
		     status = 'finished';
		  ELSE
		     record = NEW;
		     status = record.status;
		  END IF;
		  channel = CONCAT(record.jobtype,'_status');
		  PERFORM (
		     select pg_notify(channel, CONCAT(record.series_uid, ',', status)::text)
		  );
		  RETURN NULL;
		end;
		$$;
		)_stmnt_",
		R"_stmnt_(
		CREATE TRIGGER series_notify AFTER INSERT ON qi_series FOR EACH ROW EXECUTE PROCEDURE series_insert_notify('series_insert');
		)_stmnt_",
		R"_stmnt_(
		CREATE TRIGGER status_notify AFTER INSERT OR DELETE OR UPDATE ON qi_jobs_status FOR EACH ROW EXECUTE PROCEDURE status_update();
		)_stmnt_"
	};
}

const QStringList SqlDatabaseServiceStatementsSqlite::updateFromVer310ToVer320() const
{
	return QStringList{
		R"_stmnt_(
		CREATE UNIQUE INDEX "qi_patients_mrn" ON "qi_patients" ("mrn");
		)_stmnt_",
		R"_stmnt_(
		CREATE UNIQUE INDEX "qi_studies_study_uid" ON "qi_studies" ("study_uid");
		)_stmnt_",
		R"_stmnt_(
		CREATE UNIQUE INDEX "qi_series_series_uid" ON "qi_series" ("series_uid");
		)_stmnt_",
		R"_stmnt_(
		CREATE UNIQUE INDEX "qi_2d_images_image_uid" ON "qi_2d_images" ("image_uid");
		)_stmnt_"
	};
}

const QStringList SqlDatabaseServiceStatementsPostgres::updateFromVer310ToVer320() const
{
	return QStringList{
		R"_stmnt_(
		ALTER TABLE "qi_patients" ADD UNIQUE ("mrn");
		)_stmnt_",
		R"_stmnt_(
		ALTER TABLE "qi_studies" ADD UNIQUE ("study_uid");
		)_stmnt_",
		R"_stmnt_(
		ALTER TABLE "qi_series" ADD UNIQUE ("series_uid");
		)_stmnt_",
		R"_stmnt_(
		ALTER TABLE "qi_2d_images" ADD UNIQUE ("image_uid");
		)_stmnt_"
	};
}

