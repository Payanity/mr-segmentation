#include "databasehelper.h"
#include "../../quantxdata/quantxdata_lib/knowncase.h"
#include <QProcess>
#include <QApplication>
#include <QDir>
#include <QStandardPaths>
#include <QDateTime>

#ifdef Q_OS_WIN
#include <Windows.h>
#include "LM.h"
#endif

#define useDebug 0

#if useDebug
#include <QDebug>
#endif

namespace {
    constexpr auto overlaySeparatorChar = '-';
    constexpr auto overlayDateTimeFormat{ "yyyyMMddHHmmss" };

    // @TO-DO: Add static assert to check the overlaySeparatorChar is not in the
    // overlayDateTimeFormat when std::string becomes constexpr in C++20
}

/*!
 * \brief Encode binary data from input known case.
 * \param in Input data in KnownCase form.
 * \param index Index of feature in KnownCase object to encode.
 * \return QByteArray of the encoded KnownCase data.
 */
QByteArray DatabaseHelper::encodeKnownCase(const QVector<const KnownCase *> & in, int index)
{
	QByteArray a;
	QDataStream s(&a, QIODevice::WriteOnly);
	s.setVersion(QDataStream::Qt_5_6); //Version 17, used from 5.6 on
	for(const auto & knownCase : in) {
		s << knownCase->filename;
		s << knownCase->modality;
		s << knownCase->truth;
		s << knownCase->features.at(index);
	}
	return a;
}

/*!
 * \brief Encode and encrypt binary data from input known case.
 * \param in Input data in KnownCase form.
 * \param index Index of feature in KnownCase object to encode.
 * \param key Encryption key to encrypted binary data with.
 * \return QByteArray of the encrypted, encoded KnownCase data.
 */
QByteArray DatabaseHelper::cryptEncodeKnownCase(const QVector<const KnownCase *>& in, int index, quint64 key)
{
	SimpleCrypt crypto(key);
	crypto.setCompressionMode(SimpleCrypt::CompressionAlways);
	crypto.setIntegrityProtectionMode(SimpleCrypt::ProtectionHash);
	return crypto.encryptToByteArray(encodeKnownCase(in, index));
}

QVector<const KnownCase *> DatabaseHelper::decodeKnownCaseHelper(QByteArray buf)
{
	QVector<const KnownCase *> out;
	QDataStream s(&buf, QIODevice::ReadOnly);
	s.setVersion(QDataStream::Qt_5_6); //Version 17, used from 5.6 on
	while(!s.atEnd()){
		auto kc = new KnownCase;
		kc->features.resize(1);
		s >> kc->filename;
		s >> kc->modality;
		s >> kc->truth;
		s >> kc->features[0];
		out.append(std::move(kc));
	}
	return out;
}

/*!
 * \brief Decode KnownCase object vector from std::vector<unsigned char>.
 * \param in The std::vector<unsigned char> that we are using to decode from.
 * \return A QVector<const KnownCase *> of decoded data.
 */
QVector<const KnownCase *> DatabaseHelper::decodeKnownCase(std::vector<unsigned char> in)
{
	QByteArray buf(reinterpret_cast<char*>(in.data()), static_cast<int>(in.size()));
	return decodeKnownCaseHelper(buf);
}

/*!
 * \brief Decrypt and decode KnownCase objects from input data.
 * \param in The std::vector<unsigned char> to decode and decrypt.
 * \param key The decryption key.
 * \return The QVector<const KnownCase *> of decrypted, decoded KnownCase data.
 */
QVector<const KnownCase *> DatabaseHelper::cryptDecodeKnownCase(std::vector<unsigned char> in, quint64 key)
{
	SimpleCrypt crypto(key);
	crypto.setCompressionMode(SimpleCrypt::CompressionAlways);
	crypto.setIntegrityProtectionMode(SimpleCrypt::ProtectionHash);
	auto buf = crypto.decryptToByteArray(QByteArray(reinterpret_cast<char*>(in.data()), static_cast<int>(in.size())));
	return decodeKnownCaseHelper(buf);
}

/*!
 * \brief Get name of logged-in user.
 * \return Name of user.
 */
QString DatabaseHelper::getLoggedInUserName()
{
	QString UserName;

	// First try with getenv
	std::string EnvVarName;
#ifdef Q_OS_WIN
	EnvVarName = "USERNAME";
#else
	EnvVarName = "USER";
#endif
	UserName = QString::fromLocal8Bit(qgetenv(EnvVarName.c_str()));

	// If Username is not set, then default to OS Specific functions of getting Username
	if(UserName.isEmpty())
	{
#ifdef Q_OS_WIN
		// Begin Windows
		TCHAR username[UNLEN + 1];
		DWORD username_size = UNLEN + 1;
		GetUserName(static_cast<TCHAR*>(username), &username_size);
#ifndef UNICODE
	UserName = username;
#else
		std::wstring username_wStr = username;
		UserName = (std::string(username_wStr.begin(), username_wStr.end())).c_str();
		// End Windows
#endif
#else
		// Begin linux and mac
		QString processName = "whoami";

		QProcess process;
		process.setProgram(processName);
		process.start();
		while(process.state() != QProcess::NotRunning)
		{
			qApp->processEvents();
		}

		UserName = process.readAllStandardOutput();


		// End Linux and Mac
#endif
	}

	return UserName.toLower();
}

/*!
 * \brief Get user group names in QStringList form.
 * \param inUserName The OS-level username to get groups for.
 * \return The list of names.
 */
QStringList DatabaseHelper::getUserGroups(const QString& inUserName)
{
	QStringList Groups;

#ifdef Q_OS_WIN
	LPCWSTR userName = reinterpret_cast<LPCWSTR>(inUserName.utf16());
#if useDebug
	qDebug() << "UserName: " << inUserName;
#endif
	LPGROUP_USERS_INFO_0 buffer = nullptr;
	DWORD entriesRead;
	DWORD totalEntriesPossible;
#define MULTI_USER_ACCESS_GROUPS 0

#if MULTI_USER_ACCESS_GROUPS
	NET_API_STATUS errorCode = NetUserGetLocalGroups(nullptr,
	                                            userName,
	                                            0,
	                                            LG_INCLUDE_INDIRECT,
	                                            reinterpret_cast<LPBYTE*>(&buffer),
	                                            MAX_PREFERRED_LENGTH,
	                                            &entriesRead,
	                                            &totalEntriesPossible);
#else
	NET_API_STATUS errorCode = NetUserGetGroups(nullptr,
	                                            userName,
	                                            0,
	                                            reinterpret_cast<LPBYTE*>(&buffer),
	                                            MAX_PREFERRED_LENGTH,
	                                            &entriesRead,
	                                            &totalEntriesPossible);
#endif
	if(NERR_Success == errorCode)
	{
#if useDebug
		qDebug() << "Number of Entries read: " << entriesRead << " Number of Total Entries: " << totalEntriesPossible;
#endif
		if(buffer != nullptr)
		{
			for(DWORD entryIt = 0; entryIt < entriesRead; ++entryIt)
			{
				auto groupName = buffer[entryIt].grui0_name;
#if useDebug
				qDebug() << "Group Name: " << QString::fromWCharArray(groupName).toLower();
#endif
				Groups.append(QString::fromWCharArray(groupName).toLower());
			}
		}
	}
#if useDebug
	else
	{
		qDebug() << "NetUserGetGroups Error Occurred: " << errorCode;
	}
#endif

	// Free the Buffer
	if(buffer != nullptr)
	{
		NetApiBufferFree(buffer);
	}
#endif

	return Groups;
}

/*!
 * \brief Gets the temp file path for a given SQLite DB name.
 * \param databaseName Name of the db in question.
 * \return The string file path.
 */
QString DatabaseHelper::getTempSQLiteDatabasePath(const QString& databaseName)
{
	QDir tempPath(QStandardPaths::writableLocation(QStandardPaths::TempLocation));

	if(!tempPath.exists())
	{
		tempPath.mkpath(tempPath.absolutePath());
	}

#if useDebug
	qDebug() << "Temp SQLite File Path Location:" << tempPath.absoluteFilePath(databaseName);
#endif

	return tempPath.absoluteFilePath(databaseName);
}

/*!
 * \brief Creates a session ID for a given logged-in user.
 * \return The session ID.
 */
QString DatabaseHelper::createSessionId()
{
	auto sessionId = getLoggedInUserName();
	sessionId += overlaySeparatorChar;
	sessionId += QDateTime::currentDateTime().toString( overlayDateTimeFormat );

	return sessionId;
}

/*!
 * \brief Get a date-time value for a given session ID.
 * \param sessionId The session ID to decode date-time from.
 * \return QDateTime converted from the session ID.
 */
QDateTime DatabaseHelper::dateTimeFromSessionId( const QString& sessionId )
{
	if( !sessionId.isEmpty() ) {
		auto dateTimePos = sessionId.lastIndexOf( overlaySeparatorChar );
		auto dateTimeString = sessionId.mid( dateTimePos + 1 );
		return QDateTime::fromString( dateTimeString, overlayDateTimeFormat );
	}

	return QDateTime{};
}
