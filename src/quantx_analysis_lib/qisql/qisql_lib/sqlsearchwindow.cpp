#include "sqlsearchwindow.h"
#include "qisql.h"
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QSqlTableModel>
#include <QMessageBox>
#include <QFileInfo>
#include <QDir>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlDriver>
#include <QSqlField>
#include <QProgressDialog>
#include <QCoreApplication>
#include <QThread>
#include "qimriimage.h"
#include "qi3dusimage.h"
#include "qi3dmgimage.h"
#include "sqlseriesdeletewidget.h"

#ifdef Q_OS_MAC
#include "qiosirixdatabasewidget.h"
#endif

#define useDebug 0

#if useDebug
#include <QDebug>
#endif

#if NEWSTEADNAMECHECK
QString newsteadNameCheck(QString name);
#endif //NEWSTEADNAMECHECK

/*!
 * \brief Constructor that takes a parent and a DB settings list.
 * \param dbSettings List of DB settings.
 * \param parent Parent widget for view hierarchy, cleanup, and signalling.
 */
SqlSearchWindow::SqlSearchWindow(const QStringList &dbSettings, QWidget *parent) :
    QWidget(parent)
{
	databaseSettings = dbSettings;

	searchBarLineEdit = new QLineEdit(parent);
	searchBarLineEdit->setObjectName("sqlSearchBar");
	searchBarLineEdit->setPlaceholderText(tr("Search by Name, MRN, Date, ..."));

	QPushButton* searchButton=new QPushButton("Clear Search");
	searchButton->setObjectName("sqlClearSearchButton");
	QPushButton* rescanButton = new QPushButton("ReScan");
	rescanButton->setObjectName("sqlRescanButton");


	connect(rescanButton, &QPushButton::clicked, this, &SqlSearchWindow::initializeTableWidget);

	connect(searchBarLineEdit, &QLineEdit::textChanged, this, &SqlSearchWindow::searchFilterUpdate);
	connect(searchButton, &QPushButton::clicked, searchBarLineEdit, &QLineEdit::clear);
	connect(rescanButton, &QPushButton::clicked, searchBarLineEdit, &QLineEdit::clear);

	auto* searchGrid = new QGridLayout;

	searchGrid->addWidget(searchBarLineEdit, 0, 1);
	searchGrid->addWidget(searchButton, 0, 2);
	searchGrid->addWidget(rescanButton, 0, 3);

#ifdef Q_OS_MAC
	//Search for Osirix
	QFileInfo osirixDBfileInfo(QDir::homePath() + "/Documents/Osirix Data/Database.sql");
#if useDebug
	qDebug() << "Osirix File Location:" << osirixDBfileInfo.absoluteFilePath();
#endif
	if(osirixDBfileInfo.exists()){
		QPushButton* osirixButton = new QPushButton("Import From OsiriX");
		connect(osirixButton, &QPushButton::clicked, this, &SqlSearchWindow::connectToOsirix);
		searchGrid->addWidget(osirixButton,1,4);
	}
#endif

	auto* vbox = new QVBoxLayout;
	tableWidget = new QTableWidget;
	tableWidget->setObjectName("searchWindowTableWidget");
	initializeTableWidget();
	vbox->addLayout(searchGrid);
	vbox->addWidget(tableWidget);

	setLayout(vbox);
	setWindowFlags(Qt::Window);
	setWindowTitle("Quant X Patient Database          -          Qlarity Imaging, LLC");
	resize(900,500);

    tableWidget->setColumnWidth(0,420);
    setAutoLoadSeries();

#if useDebug
	qDebug() << tableWidget->rowCount() << "total rows";
#endif
}

/*!
 * \brief Inits the table widget.
 */
void SqlSearchWindow::initializeTableWidget()
{
	{
		tableWidget->setSortingEnabled(false);
		if(databaseSettings.first() == "QSQLITE" && !databaseSettings.at(2).isEmpty()){
			QFileInfo sqlDBfileInfo(databaseSettings.at(2));
			if(!sqlDBfileInfo.exists()){
				if(QMessageBox::warning(this,"Database File Missing","The specified database does not exist.\n\nDo you want to create an empty database at the location: " + databaseSettings.at(2),QMessageBox::Yes | QMessageBox::No,QMessageBox::No) == QMessageBox::Yes){
					if(sqlDBfileInfo.absoluteDir().mkpath(sqlDBfileInfo.absolutePath()))
					{
						QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE","CreateEmptyDB");
						db.setDatabaseName(databaseSettings.at(2));
						db.setUserName(databaseSettings.at(3));
						db.setPassword(databaseSettings.at(4));
						db.open();
						QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
						QFile commandFile(":/sql/sqliteInitialize");
						QStringList commands;
						commandFile.open(QIODevice::ReadOnly);
						while(!commandFile.atEnd())
							commands << commandFile.readLine();
						commandFile.close();
						for(const auto & command : std::as_const(commands))
							sqlQuery->exec(command);
						sqlQuery.reset();
						db.close();
					}
					QSqlDatabase::removeDatabase("CreateEmptyDB");
				}
			}
		}

		QScopedPointer<QISql> qisql(new QISql(databaseSettings));
		QSqlDatabase db = qisql->createDatabase("QIsearchDatabaseInitializeTree");
		bool ok = db.open();  //if ok is false, we should error and quit
		if(!ok){
			int retries = 0;
			while( (!ok) && (retries++ < 5) ){
				QThread::sleep(1); // Wait one second, and retry
			}
			QMessageBox::critical(this,"Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
		}

		checkDatabaseVersion(db);

		tableWidget->clear();
		tableWidget->setColumnCount(5);
		tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

		QScopedPointer<QSqlTableModel> sqlModel(new QSqlTableModel(this,db));
		sqlModel->setTable(qisql->patientsTable());
		sqlModel->select();

		int index_name          = sqlModel->fieldIndex("patient_name");
		int index_MRN           = sqlModel->fieldIndex("mrn");
		int index_sex           = sqlModel->fieldIndex("sex");

		//QVector<QStandardItem *> rowItems;
		//QVector<QStandardItem *> placeHolder;
		while(sqlModel->canFetchMore())
			sqlModel->fetchMore();
		int numRows = sqlModel->rowCount();
		tableWidget->setRowCount(numRows);
#if useDebug
		qDebug() << "Begin tableWidget Update";
#endif
	    for(int i=0;i<numRows;i++)
		{
			//placeHolder.clear();
			//rowItems.clear();

			QString name = sqlModel->index(i,index_name).data().toString().replace("^"," ");
#if NEWSTEADNAMECHECK
			name = newsteadNameCheck(name);
#endif
			auto mrDatePair = findMRDate(qisql->studiesTable(), sqlModel->index(i,index_MRN).data().toString(), db);
			tableWidget->setItem(i,0, new QTableWidgetItem(name));
			tableWidget->setItem(i,1, new QTableWidgetItem(sqlModel->index(i,index_MRN).data().toString()));
			tableWidget->setItem(i,2, new QTableWidgetItem(mrDatePair.first));                  //Image Date
			tableWidget->setItem(i,3, new QTableWidgetItem(mrDatePair.second));                  //Accession Number
			tableWidget->setItem(i,4, new QTableWidgetItem(sqlModel->index(i,index_sex).data().toString()));
		}

		tableWidget->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));
		tableWidget->setHorizontalHeaderItem(1,new QTableWidgetItem("MRN"));
		tableWidget->setHorizontalHeaderItem(2,new QTableWidgetItem("MR Date"));
		tableWidget->setHorizontalHeaderItem(3,new QTableWidgetItem("Accession Number"));
		tableWidget->setHorizontalHeaderItem(4,new QTableWidgetItem("Sex"));

		//tableWidget->setModel(&itemModel);
		tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
		//tableWidget->setColumnHidden(8,true);
		//tableWidget->setColumnHidden(9,true);
		tableWidget->setAutoScroll(true);
		tableWidget->setAutoScrollMargin(64);

		connect(tableWidget, &QTableWidget::cellDoubleClicked, this, &SqlSearchWindow::doubleClickAnyColumn, Qt::UniqueConnection);

		sqlModel->clear();
		sqlModel.reset();
		db.close();
	}

	QSqlDatabase::removeDatabase("QIsearchDatabaseInitializeTree");
	tableWidget->setSortingEnabled(true);
	searchFilterUpdate("");
}

/*!
 * \brief Find MR date and accession number.
 * \param studiesTable Table to do the querying on.
 * \param mrnFilter Filter string for querying on MRN.
 * \param db Database to use.
 * \return Pair of MR date and accession number.
 */
QPair<QString, QString> SqlSearchWindow::findMRDate(const QString& studiesTable, const QString& mrnFilter, QSqlDatabase &db)
{

	QString mrdate("");
	QString accessionNumber;

	QSqlRecord studyRecord   = db.record(studiesTable);
	QSqlRecord studyMRNOnly  = db.record(studiesTable);
	studyMRNOnly.setValue("mrn",mrnFilter);
	studyMRNOnly.setValue("modality","MR");
	for(int i=studyMRNOnly.count()-1;i>=0;i--)
		if(studyMRNOnly.isNull(i))
			studyMRNOnly.remove(i);

	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
	QString sqlStatement;
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,studiesTable,studyRecord,false) + QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,studiesTable,studyMRNOnly,true);

	sqlQuery->prepare(sqlStatement);
	for(int i=0;i<studyMRNOnly.count();i++)
		sqlQuery->bindValue(i,studyMRNOnly.value(i));
	sqlQuery->exec();

	int index_date          = studyRecord.indexOf("study_date");
	int index_accession     = studyRecord.indexOf("accession_number");

	while(sqlQuery->next()){
		QString mrdatenew = sqlQuery->value(index_date).toString();
		if(mrdatenew > mrdate){
			mrdate = mrdatenew;
			accessionNumber = sqlQuery->value(index_accession).toString();
		}
	}

#if useDebug
	qDebug() << "findMRDate() MRN:" << mrnFilter << "Date:" << mrdate;
#endif

	sqlQuery->clear();
	sqlQuery.reset();

	return qMakePair(mrdate, accessionNumber);
}

namespace {
// Helper functions for doubleClickLoadSeries()

bool findModality(const QString & mrn, const QSharedPointer<QSqlTableModel>& sqlModel, const QString& modalityString, const QString& seriesDesc = QString(), const QString& customFilter = QString())
{
	QString filter;
	filter = QString("mrn = '" + mrn + "'" + " and modality = '" + modalityString + "'");
	if(!seriesDesc.isEmpty())
		filter += " and  series_desc LIKE '%" + seriesDesc + "%'";
	if(!customFilter.isEmpty())
		filter += " and " + customFilter;
	sqlModel->setFilter(filter);
	sqlModel->select();

	int rowCount = sqlModel->rowCount();
#if useDebug
	qDebug() << QString("Number of %1 Series:").arg(modalityString + ( seriesDesc.isEmpty() ? QString() : (" (" + seriesDesc + ") ") ) ) << rowCount;
#endif
	return (rowCount > 0);
}

bool findModalityFromList(const QString & mrn, const QSharedPointer<QSqlTableModel>& sqlModel, const QString& modalityString, const QStringList & seriesDesc, QString customFilter = QString())
{
	Q_UNUSED(customFilter)
	for(const auto & desc : seriesDesc)
		if(findModality(mrn, sqlModel, modalityString, desc))
			return true;
	return false;
}

bool is3DSeries(const QSharedPointer<QSqlTableModel>& sqlModel)
{
	int index_spaceBetweenSlices    = sqlModel->fieldIndex("space_between_slices");
	auto spaceBetweenSlices = sqlModel->index(0,index_spaceBetweenSlices).data().toFloat();
	return (spaceBetweenSlices > 0.0f);
}

} // namespace

/*!
 * \brief Handles row / column double clicking in GUI table to load series.
 * \param row Row clicked.
 * \param col Column clicked.
 */
void SqlSearchWindow::doubleClickAnyColumn(int row, int col)
{
	Q_UNUSED(col)

	emit selectedMRN(tableWidget->item(row,1)->text());
	if(autoLoadSeries){
		doubleClickLoadSeries(row);
		QSqlDatabase::removeDatabase("QIsearchDatabaseExpandStudy");
	}
}

/*!
 * \brief Load series row.
 * \param row Index of row to load.
 */
void SqlSearchWindow::doubleClickLoadSeries(int row)
{
	QScopedPointer<QISql> qisql(new QISql(databaseSettings));
	QSqlDatabase db = qisql->createDatabase("QIsearchDatabaseExpandStudy");
	bool ok = db.open();
	if(!ok){
		int retries = 0;
		while( (!ok) && (retries++ < 5) ){
			QThread::sleep(1); // Wait one second, and retry
		}
		QMessageBox::critical(this,"Database Error","The specified database could not be opened.\nCheck your database settings and try again.");
	}
	//QSharedPointer<QSqlTableModel> sqlModel(new QSqlTableModel(this,db), &QSqlTableModel::deleteLater);
	QSharedPointer<QSqlTableModel> sqlModel(new QSqlTableModel(this,db));
	sqlModel->setTable(qisql->seriesTable());
	int index_seriesUID = sqlModel->fieldIndex("series_uid");
	QString mrn = tableWidget->item(row,1)->text();
	QString mrDate = QString("'%1'").arg(tableWidget->item(row,2)->text());
#if useDebug
	qDebug() << "row is" << row << "out of" << tableWidget->rowCount() << "total" << "MRN is" << mrn;
#endif
//    if( findModality(mrn, sqlModel, "MG") ) //Load Mammo Image
//		loadSeries(sqlModel->index(0,index_seriesUID).data().toString(), is3DSeries(sqlModel) ? QUANTX::MG3D : QUANTX::MAMMO);
//    else if( findModality(mrn, sqlModel, "US") ) //Load US Image
//		loadSeries(sqlModel->index(0,index_seriesUID).data().toString(), is3DSeries(sqlModel) ? QUANTX::US3D : QUANTX::US);
//    else if( static_cast<void>(sqlModel->setSort(sqlModel->fieldIndex("series_number"),Qt::AscendingOrder)),
//             findModalityFromList(mrn, sqlModel, "MR", t2SeriesDesc, "series_date = " + mrDate ) ) //Load T2 MR Image
//		loadSeries(sqlModel->index(0,index_seriesUID).data().toString(), QUANTX::MRI);
//    else if( static_cast<void>(sqlModel->setSort(sqlModel->fieldIndex("series_number"),Qt::AscendingOrder)),
//             findModalityFromList(mrn, sqlModel, "MR", dynSeriesDesc + dynMultiSeriesDesc, "series_date = " + mrDate) ) //Load DCE MR Image
//		loadSeries(sqlModel->index(0,index_seriesUID).data().toString(), QUANTX::MRI);
//    else if( findModality(mrn, sqlModel, "MR", QString(), "series_date = " + mrDate) ) //Load First MR Image on last MR date (fallback, may cause a crash)
//		loadSeries(sqlModel->index(0,index_seriesUID).data().toString(), QUANTX::MRI);
//    else if( findModality(mrn, sqlModel, "MR") ) //Load First MR Image found (fallback, may cause a crash)
//		loadSeries(sqlModel->index(0,index_seriesUID).data().toString(), QUANTX::MRI);
//	db.close();
}

/*!
 * \brief Load series.
 * \param seriesID ID of series to load.
 * \param modality_ Modality to load.
 */
void SqlSearchWindow::loadSeries(QString seriesID, QUANTX::Modality modality_)
{
//    if (modality == QUANTX::MRI) {
//		auto *mriImage = new QiMriImage;
//#ifdef QI_INTERNAL_TIMING_OUTPUT
//		emit startLoadingMRSeries(mriImage);
//#endif
//#if useDebug
//        qDebug() << "Loading MRI from SqlSearchWindow object";
//#endif
//		QScopedPointer<QProgressDialog> progressDialog(new QProgressDialog("Opening MRI Case\n(time depends on speed of disk or network)\nPlease Wait...", "", 0, 100, this, Qt::FramelessWindowHint));
//        progressDialog->setCancelButton(nullptr);
//        progressDialog->setFixedSize(420,100);
//        progressDialog->setWindowModality(Qt::WindowModal);
//        progressDialog->setMinimumDuration(0);
//        progressDialog->setValue(1);
//        progressDialog->show();
//        qApp->processEvents();
//
//        connect(mriImage, &QiMriImage::openProgressUpdated, progressDialog.data(), &QProgressDialog::setValue);
//        connect(mriImage, &QiMriImage::openProgressNewMaximum, progressDialog.data(), &QProgressDialog::setMaximum);
//		mriImage->openSeries(seriesID, databaseSettings, dynMultiSeriesDesc, fastDynamicSeriesDesc);
//
//        if(mriImage->nz() == 0){
//            if( QMessageBox::warning(this,"Cannot Open MR Series","MR series failed to open.\nData is likely to be corrupt. Do you wish to delete this series?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes){
//                SqlSeriesDeleteWidget deleteWidget(databaseSettings,this);
//                deleteWidget.deleteSeries(mriImage->seriesUID(), QUANTX::MRI);
//            }
//            delete mriImage;
//            return;
//        }
//
//        //mriImage should be deleted when the other program is done with it
//        emit imageLoaded(mriImage);
//    } else if(modality == QUANTX::US3D){
        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::US3D,this));
//        qApp->processEvents();
//        auto *us3DImage = new Qi3DUSImage;
//		us3DImage->openSeries(seriesID, databaseSettings);
//        emit imageLoaded(us3DImage);
//        openDialog->close();
//    } else if(modality == QUANTX::MG3D){
//        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::MG3D,this));
//        qApp->processEvents();
//        auto *mg3DImage = new Qi3DMGImage;
//		mg3DImage->openSeries(seriesID, databaseSettings);
//        emit imageLoaded(mg3DImage);
//        openDialog->close();
//    } else if (modality == QUANTX::MAMMO) {
//        auto *image = new Qi2DImage;
//        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::MAMMO,this));
//		int numImages = image->openSeries(seriesID, modality, databaseSettings);
//        if(numImages == 1)
//            emit imageLoaded(image);
//        else{
//            QList<Qi2DImage *> twoDImageList;
//            twoDImageList << image;
//            for(int i=1;i<numImages;i++){
//                twoDImageList << new Qi2DImage;
//				numImages = twoDImageList[i]->openSeries(seriesID, modality, databaseSettings,i);
//            }
//            if(numImages == -1){ //error opening last image in list
//                delete twoDImageList.last();
//                twoDImageList.removeLast();
//            }
//            emit imageLoaded(twoDImageList);
//        }
//        openDialog->close();
//    } else if(modality == QUANTX::US){
//        QScopedPointer<QDialog> openDialog(createLoadingImageDialog(QUANTX::US,this));
//        QList<Qi2DImage *> twoDImageList;
//        int numImages = 1;
//        for(int i=0;i<numImages;i++){
//            twoDImageList << new Qi2DImage;
//			numImages = twoDImageList[i]->openSeries(seriesID, modality, databaseSettings,i);
//        }
//        if(numImages == -1){ //error opening last image in list
//            delete twoDImageList.last();
//            twoDImageList.removeLast();
//        }
//        //mriImage should be deleted when the other program is done with it
//        emit imageLoaded(twoDImageList);
//        openDialog->close();
//    }
}

/*!
 * \brief Setter for DB settings.
 * \param dbSettings DB settings to use.
 */
void SqlSearchWindow::setDatabaseSettings(const QStringList &dbSettings)
{
	databaseSettings = dbSettings;
	initializeTableWidget();
}

/*!
 * \brief Getter for DB settings.
 * \return DB settings.
 */
QStringList SqlSearchWindow::getDatabaseSettings()
{
	return databaseSettings;
}

/*!
 * \brief Creates a loading dialog for images.
 * \param modality Modality of image to load.
 * \param parent Parent for the loading dialog.
 * \return Pointer to the QDialog created.
 */
QDialog* SqlSearchWindow::createLoadingImageDialog(QUANTX::Modality modality,QWidget *parent)
{
	auto* dialog = new QDialog(parent, Qt::FramelessWindowHint);
	dialog->setModal(false);
	dialog->setFixedSize(420,60);
	auto* vbox = new QVBoxLayout();
	QLabel *label;
	if (modality == QUANTX::MAMMO)
		label = new QLabel(QString("Loading selected mammogram, please wait..."));
	else if (modality == QUANTX::MG3D)
		label = new QLabel(QString("Loading selected tomography image, please wait..."));
	else
		label = new QLabel(QString("Loading selected sonogram, please wait..."));
	label->setAlignment(Qt::AlignCenter);
	vbox->addWidget(label);
	dialog->setLayout(vbox);
	dialog->show();
	return dialog;
}

/*!
 * \brief Update the search filter.
 * \param SearchText Text to search on.
 */
void SqlSearchWindow::searchFilterUpdate(const QString& SearchText)
{
	auto ItemList = tableWidget->findItems(SearchText, Qt::MatchFlag::MatchContains);
	QSet<int> VisibleRows;
	for(auto Item : ItemList)
	{
		VisibleRows.insert(Item->row());
	}

	for(int Row = 0; Row < tableWidget->rowCount(); ++Row)
	{
		tableWidget->setRowHidden(Row, !VisibleRows.contains(Row));
	}
}

/*!
 * \brief Tells the table widget to sort by column.
 * \param c Column to sort on.
 * \param s Sort order to use.
 */
void SqlSearchWindow::sortByColumn(int c, Qt::SortOrder s)
{
	tableWidget->sortByColumn(c,s);

}

/*!
 * \brief Open a case.
 * \param mrn MRN of case to open.
 * \return True if successful, false if not.
 */
bool SqlSearchWindow::openCase(const QString& mrn)
{
	QModelIndexList selectedItem = tableWidget->model()->match(tableWidget->model()->index(0,1),0,mrn,1);
	if (!selectedItem.isEmpty()) {
		int tableRow = selectedItem.first().row();
		doubleClickAnyColumn(tableRow,0);
		return true;
	}
	    return false;
}

#if NEWSTEADNAMECHECK
QString newsteadNameCheck(QString name)
{
	if(name == "16343486")
		name = "repeat 00063";
	else if(name == "16680886")
		name = "00066";
	else if(name == "23319558")
		name = "repeat 00053";
	else if(name == "39542580")
		name = "00068";
	else if(name == "50879207")
		name = "00069";
	else if(name == "53413226")
		name = "00070";
	else if(name == "63798781")
		name = "00071";
	else if(name == "65572658")
		name = "00072";
	else if(name == "74436615")
		name = "00073";
	else if(name == "74527571")
		name = "00074";
	else if(name == "86198399")
		name = "00075";
	else if(name == "90011350")
		name = "00076";

	return name;
}
#endif

#ifdef Q_OS_MAC
void SqlSearchWindow::connectToOsirix()
{
	QiOsirixDatabaseWidget* osirix = new QiOsirixDatabaseWidget(this);
	osirix->setWindowModality(Qt::ApplicationModal);
	osirix->setAttribute(Qt::WA_DeleteOnClose);
	connect(osirix, &QiOsirixDatabaseWidget::casesImported, this, &SqlSearchWindow::initializeTableWidget);
	osirix->show();
}
#endif

