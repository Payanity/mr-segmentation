#-------------------------------------------------
#
# Project created by QtCreator 2011-03-29T12:31:48
#
#-------------------------------------------------

MRIV_ROOT = ../../..
QT       += gui sql core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets concurrent

TARGET = qisql
TEMPLATE = lib

MOC_DIR           = moc
OBJECTS_DIR       = obj
DESTDIR           = $${MRIV_ROOT}/lib

include( $${MRIV_ROOT}/quantxconfig.pri )
include( $${MRIV_ROOT}/src/directoryconfig.pri )

unix : !macx {
    QMAKE_CXXFLAGS += -std=c++14
}

INCLUDEPATH += . \
               $${GDCM_PATH}/include/gdcm-$${GDCM_VERSION} \
               $${MRIV_ROOT}/src/motioncorrection/motioncorrection_mr \
               $${MRIV_ROOT}/include/simplecrypt \
               $${WT_PATH}/include \
               $${PLASTIMATCH_PATH}/include \
               $${ITK_PATH}/include/ITK-$${ITK_VERSION} \
               $${PLASTIMATCH_SRC_PATH}/../../libs/dlib-$${PLASTIMATCH_DLIB_VERSION} \
               $${PLASTIMATCH_SRC_PATH}/base \
               $${PLASTIMATCH_SRC_PATH}/sys \
               $${PLASTIMATCH_SRC_PATH}/register \
               $${PLASTIMATCH_SRC_PATH}



unix|win32 {
    contains(CONFIG, QiSqlDll ) {
        CONFIG       += dll
        DEFINES      += QISQL_DLL
        DLLDESTDIR   = $${MRIV_ROOT}/bin
    }
    else {
        CONFIG += staticlib
    }
}
else {
    CONFIG += staticlib
}

win32 {
    LIBS += -lAdvapi32
    LIBS += -lNetapi32
}

CONFIG(debug, debug|release) {
    TARGET = $$join(TARGET,,,_debug)
    win32 {
        LIBS += -L$${GDCM_PATH}/lib -lgdcmCommon_debug \
            -L$${GDCM_PATH}/lib -lgdcmDICT_debug \
            -L$${GDCM_PATH}/lib -lgdcmDSED_debug \
            -L$${GDCM_PATH}/lib -lgdcmMSFF_debug \
            -L$${GDCM_PATH}/lib -lgdcmMEXD_debug \
            -L$${GDCM_PATH}/lib -lgdcmcharls_debug \
            -L$${MRIV_ROOT}/lib -lmotioncorrection_mr_debug \
            -L$${WT_PATH}/lib -lwtd -lwtdbod -lwtdbosqlite3d -lwtdbopostgresd \
            -L$${PLASTIMATCH_PATH}/lib/ -lplmbase_d
    }
    else {
        LIBS += -L$${GDCM_PATH}/lib -lgdcmCommon \
            -L$${GDCM_PATH}/lib -lgdcmDICT \
            -L$${GDCM_PATH}/lib -lgdcmDSED \
            -L$${GDCM_PATH}/lib -lgdcmMSFF \
            -L$${GDCM_PATH}/lib -lgdcmcharls \
            -L$${GDCM_PATH}/lib -lgdcmMEXD \
            -L$${MRIV_ROOT}/lib -lmotioncorrection_mr_debug \
            -L$${WT_PATH}/lib -lwtd -lwtdbod -lwtdbosqlite3d -lwtdbopostgresd \
            -L$${PLASTIMATCH_PATH}/lib/ -lplmbase
    }
    #FOLLOWING SECTION COPIES DEBUGGING FILES TO .DLL DIRECTORY
    win32-msvc*:dll {
        PLB_TARGET      = $${DESTDIR}/$${TARGET}.pdb
        PLBDESTDIR      = $${DLLDESTDIR}
        QMAKE_POST_LINK = $$quote(copy /y $$replace(PLB_TARGET,"/","\\") $$replace(PLBDESTDIR,"/","\\"))
    }

    # QMAKE_LFLAGS_WINDOWS += /DELAYLOAD:motioncorrection_mr_debug.dll \
    #                         /DELAYLOAD:plmbase_d.dll
}

CONFIG(release, debug|release) {
    LIBS += -L$${GDCM_PATH}/lib -lgdcmCommon \
            -L$${GDCM_PATH}/lib -lgdcmDICT \
            -L$${GDCM_PATH}/lib -lgdcmDSED \
            -L$${GDCM_PATH}/lib -lgdcmMSFF \
            -L$${GDCM_PATH}/lib -lgdcmcharls \
            -L$${GDCM_PATH}/lib -lgdcmMEXD \
            -L$${MRIV_ROOT}/lib -lmotioncorrection_mr \
            -L$${WT_PATH}/lib -lwt -lwtdbo -lwtdbosqlite3 -lwtdbopostgres \
            -L$${PLASTIMATCH_PATH}/lib/ -lplmbase

    # QMAKE_LFLAGS_WINDOWS += /DELAYLOAD:motioncorrection_mr.dll
    #                         /DELAYLOAD:plmbase.dll
}

unix : !macx {
    LIBS += -L$${PLASTIMATCH_PATH}/lib/ -lplmregister
}

macx {
    LIBS += -L$${ITK_PATH}/lib/ -lITKCommon-$${ITK_VERSION} \
            -L$${PLASTIMATCH_PATH}/lib/ -lplmregister
    SOURCES += $${ITK_SRC_PATH}/Modules/Core/Common/src/itkIndent.cxx \
               $${ITK_SRC_PATH}/Modules/Core/Common/src/itkRegion.cxx \
               $${MRIV_ROOT}/src/motioncorrection/motioncorrection_mr/motioncorrection_mr.cpp
}

LIBS += -L$${WT_PATH}/lib

SOURCES += \
    checkserverjobs.cpp \
    auditlogdatabase.cpp \
    dicomquery.cpp \
    databasesettings.cpp \
    patientmodel.cpp \
    qisql.cpp \
    serieswatcherdialog.cpp \
    sqldatabaseservice.cpp \
    sqldatabaseservicestatements.cpp \
    sqlsearchwindow.cpp \
    qimriimage.cpp \
    qipatientinventory.cpp \
    qiseriesinfo.cpp \
    qi2dimage.cpp \
    qisql_mainwindow.cpp \
    qi3dusimage.cpp \
    qi3dmgimage.cpp \
    qiosirixdatabasewidget.cpp \
    sqlseriesdeletewidget.cpp \
    qi3dimage.cpp \
    checkdatabaseversion.cpp \
    qimriimagege.cpp \
    additem.cpp \
    hologictomo.cpp \
    $${MRIV_ROOT}/include/simplecrypt/simplecrypt.cpp \
    databasehelper.cpp \
    treeitem.cpp \
    useraccessdatabase.cpp

HEADERS  += \
    checkserverjobs.h \
    auditlogdatabase.h \
    dicomquery.h \
    databasesettings.h \
    model/auditlog.h \
    model/dicomdata.h \
    model/dicomseries.h \
    model/dicomstudy.h \
    model/images2d.h \
    model/lesion.h \
    model/lesiondetails.h \
    model/mammoimages.h \
    model/miscimages.h \
    model/motioncorrection.h \
    model/mrimages.h \
    model/patient.h \
    model/seriesthumbnails.h \
    model/useraccess.h \
    model/usimages.h \
    patientmodel.h \ 
    qisql.h \
    qtsqltraits.h \
    serieswatcherdialog.h \
    sqldatabaseservice.h \
    sqldatabaseservicestatements.h \
    sqlsearchwindow.h \
    qimriimage.h \
    qisqlDllCheck.h \
    qipatientinventory.h \
    qiseriesinfo.h \
    qi2dimage.h \
    qisql_mainwindow.h \
    qi3dimage.h \
    qi3dusimage.h \
    qi3dmgimage.h \
    qiosirixdatabasewidget.h \
    sqlseriesdeletewidget.h \
    qiabstractimage.h \
    hologictomo.h \
    qisqlmodels.h \
    pointvector3d.h \
    $${MRIV_ROOT}/include/simplecrypt/simplecrypt.h \
    databasehelper.h \
    treeitem.h \
    useraccessdatabase.h

RESOURCES += \
    dicomFiles.qrc \
    $${MRIV_ROOT}/include/tango-icon-theme-0.8.90/tango-icons-qisql.qrc \
    sqlFiles.qrc



