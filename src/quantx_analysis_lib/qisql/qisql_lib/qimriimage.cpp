#include "qimriimage.h"
#include "qisql.h"
#include "dicomquery.h"
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlField>
#include <QtAlgorithms>
#include <QByteArray>
//#include <QProgressDialog>
#include <QPushButton>
#include <QCoreApplication>
#include <QFuture>
#include <QFutureWatcher>
#include <QtConcurrentRun>
#include <QEventLoop>
//#include <QtMath>
#include <QTextEdit>
#include <QDataStream>
#include <utility>
#include <algorithm>
//#include <QMessageBox>

//For internal testing of Query times. Remove prior to deployment.
#define useDebug 0
#if useDebug
#include <QElapsedTimer>
#include <QDebug>
#include <QSqlError>
#endif

#define testFullHeader 0
// Siemens scanners can use the following value instead of 0
#define MAX_ZERO_VALUE 2.051034e-010

#define OPENPROGRESSUPDATE(p) if((( p )%32)==0){emit openProgressUpdated( p );}

QStringList getSeriesDescriptions(QStringList dbSettings, QStringList seriesUIDs)
{
	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	QMap<QString, QString> seriesDescriptions;
	{
		auto db = qisql->createDatabase("seriesDescriptions");
		db.open();
		if (db.isOpen()){
			QSqlQuery query(db);
			QString inClause = "('" + seriesUIDs.join("','") + "')";
			QString sqlStatement = "SELECT series_uid, series_desc from qi_series WHERE series_uid in " + inClause +
			                       " ORDER BY series_uid";
			if (!query.exec(sqlStatement)){
#if useDebug
				qDebug() << "Query Error: " << query.lastError();
#endif
			} else {
				while (query.next()){
					seriesDescriptions.insert(query.value("series_uid").toString(), query.value("series_desc").toString());
				}
			}
		}
	}
	QSqlDatabase::removeDatabase("seriesDescriptions");
	QStringList descriptions;
	// this will ensure returned stringlist will have same size and order as input series list
	// any series not in database with have blank string in returned descriptions
	for (auto seriesUID : seriesUIDs){
		    descriptions << seriesDescriptions.value(seriesUID);
	}
	return descriptions;

}

QStringList sortSeriesBySeriesNum(QStringList dbSettings, QStringList seriesUIDs)
{
	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	QMap<int, QString> sortedSeries;
	{
		auto db = qisql->createDatabase("seriesSort");
		db.open();
		if (db.isOpen()){
			QSqlQuery query(db);
			QString inClause = "('" + seriesUIDs.join("','") + "')";
			QString sqlStatement = "SELECT series_uid, series_number from qi_series WHERE series_uid in " + inClause +
			                       " ORDER BY series_number";
			if (!query.exec(sqlStatement)){
#if useDebug
				qDebug() << "Query Error: " << query.lastError();
#endif
			} else {
				while (query.next()){
					sortedSeries.insert(query.value("series_number").toInt(), query.value("series_uid").toString());
				}
			}
		}
	}
	QSqlDatabase::removeDatabase("seriesSort");

	return sortedSeries.values();

}

//!
//! \brief getSliceLocations Get the sorted slice locations for the given orientation. Also sorts the imagePositions by
//! the component determined by the orientation
//! \param imagePositions A vector of imagePositions for each slice
//! \param orientation The orientation for which the locations will be returned
//! \post The input imagePositions vector is sorted by the component corresponding to the orientation
//! \return Sorted vector of slice locations for the given orientation
//!
QVector<float> getSliceLocations(QVector<QVector3D> & imagePositions, QUANTX::Orientation orientation)
{
	QVector<float> locations;
	QMap<float, QVector3D> positionMap;
	if (orientation == QUANTX::XYPLANE){
		for(const auto & position : imagePositions){
			positionMap.insert(position.z(), position);
			locations << position.z();
		}
	} else if (orientation == QUANTX::YZPLANE){
		for(const auto & position : imagePositions){
			positionMap.insert(position.x(), position);
			locations << position.x();
		}
	} else {
		for(const auto & position : imagePositions){
			positionMap.insert(position.y(), position);
			locations << position.y();
		}
	}
	imagePositions = positionMap.values().toVector();
	std::sort(locations.begin(), locations.end());
	return locations;
}

/*!
 * \brief Open an MRI series.
 * \param uid UID of the series to attempt to open.
 * \param dbSettings DB settings to use to connect to the database.
 * \param dynMultiSeriesDesc The list of dynamic multi series descriptions.
 * \param fastDynamicSeriesDesc The list of fast dynamic multi series descriptions.
 * \return 1 or -1 for success or fail, -999 for an exception code.
 */
int QiMriImage::openSeries(QString uid, const QStringList &dbSettings, const QStringList &dynMultiSeriesDesc, const QStringList &fastDynamicSeriesDesc)
{
	seriesUID_ = std::move(uid);
	data_.clear();
	windowWidth_ = 0;
	windowLevel_ = 0;
	maxPixval_ = 0;
	minPixval_ = 0;
	dx_ = 0.0;
	dy_ = 0.0;
	dz_ = 0.0;
	timestamps_.clear();
	nx_ = 0;
	ny_ = 0;
	nz_ = 0;
	nt_ = 0;
	orientation_.clear();
	acquisitionOrientation_ = QUANTX::XYPLANE;
	precontrastIdx_ = -1;
	firstPostIdxFixed = -1;
	seriesNums_.clear();
	origin_ = QVector3D(); // this is the real-world location of the center of the first voxel (index 0,0,0) in the image. When loading image data, we make this the most Right, Anterior, and Inferior voxel regardless of acquisition orientation
	auto returnVal = -1;
	try {
		returnVal = getSeriesData(dbSettings, dynMultiSeriesDesc, fastDynamicSeriesDesc);
	} catch(...) {
		seriesDesc_.clear();
		nx_ = 0;
		ny_ = 0;
		nz_ = 0;
		nt_ = 0;
		returnVal = -999;
	}
	QSqlDatabase::removeDatabase("QIopenDatabase");
	return returnVal;
}

/*!
 * \brief Fetches the series data.
 * \param dbSettings DB settings to use to connect to the database.
 * \param dynMultiSeriesDesc The list of dynamic multi series descriptions.
 * \param fastDynamicSeriesDesc The list of fast dynamic multi series descriptions.
 * \return 1 or -1 for success or fail.
 */
int QiMriImage::getSeriesData(const QStringList &dbSettings, const QStringList &dynMultiSeriesDesc, const QStringList &fastDynamicSeriesDesc)
{
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit startGetSeriesData();
#endif
	// Before we do anything, lets make sure the skipped indices are reset
	instanceNumsIndicesToRemove.clear();

	if(     seriesUID_.startsWith("1.3.6.1.4.1.14519.") ||
	        //seriesUID_.startsWith("1.3.6.1.4.1.18047.") || // These postcontrast series type adds MP_GEMS to DCE options
	        seriesUID_.startsWith("1.2.840.113619.") ||
	        seriesUID_.startsWith("1.2.528.1.1001.")  // GE Medical Systems Nederland BV - see http://oidref.com/1.2.528.1.1001
	        )
		return getGESeriesData(dbSettings, dynMultiSeriesDesc); // Note: This is for *very* old GE images
#if useDebug
	QElapsedTimer whole, loop;
	whole.start();
#endif
	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	QSqlDatabase db = qisql->createDatabase("QIopenDatabase");
#if useDebug
	    qDebug() << "Opening DB QiMRImage for series" << seriesUID_;
#endif
		bool ok = db.open();  //if ok is false, we should error and quit
	if(!ok){
		int retries = 0;
		while( (!ok) && (retries++ < 5) ){
			QThread::sleep(1); // Wait one second, and retry
		}
		qWarning("Database Error!\nThe specified database could not be opened.\nCheck your database settings and try again.");
		qWarning("%s", dbSettings.join(" : ").toStdString().c_str());
	}

	QString sqlImageTable = qisql->imageTable(QUANTX::MRI);
	QString sqlSeriesTable = qisql->seriesTable();

	QSqlRecord seriesRecord  = db.record(sqlSeriesTable);
	QSqlRecord imageRecord   = db.record(sqlImageTable);
	QSqlRecord seriesDescRecord = seriesRecord;
	QSqlRecord uidRecord = seriesRecord;
	uidRecord.setValue("series_uid",seriesUID_);
	for(int i=uidRecord.count()-1;i>=0;i--)
		if(uidRecord.isNull(i))
			uidRecord.remove(i);

	QString sqlStatement;
	QFuture<bool> sqlFuture;
	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));

	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
	        + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,uidRecord,false);
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	sqlFuture.waitForFinished();
	if(!sqlQuery->next())
		return -1; //no records match series UID
	//if(sqlQuery->size() == 0)
	//    return -1; //no records match series UID
	//if(sqlQuery->size() > 1)
	//    return -2; //more than one record with series UID requested

	sqlQuery->first();
	seriesRecord = sqlQuery->record(); //Set series record data to result instead of empty
	mrn_ = seriesRecord.value("mrn").toString();
	studyUID_ = seriesRecord.value("study_uid").toString();
	seriesNums_ << seriesRecord.value("series_number").toString().trimmed();
	QSqlRecord studyUIDRecord;
	studyUIDRecord.append(QSqlField("study_uid", QVariant::String));
	studyUIDRecord.setValue("study_uid", studyUID_);
	QSqlRecord studyAccessionRecord;
	studyAccessionRecord.append(QSqlField("accession_number", QVariant::String));
	auto sqlAccessionStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,qisql->studiesTable(),studyAccessionRecord,false) + QString(' ')
	                           + db.driver()->sqlStatement(QSqlDriver::WhereStatement,qisql->studiesTable(),studyUIDRecord,false);
	QScopedPointer<QSqlQuery> sqlAccessionQuery(new QSqlQuery(db));
	sqlFuture = QtConcurrent::run(sqlAccessionQuery.data(),&QSqlQuery::exec,sqlAccessionStatement);
	sqlFuture.waitForFinished();
	sqlAccessionQuery->first();
	accessionNumber_ = sqlAccessionQuery->value("accession_number").toString();

	nt_ = seriesRecord.value("num_temporal_positions").toInt();
	seriesDesc_ = seriesRecord.value("series_desc").toString();
	seriesDescRecord.setValue("series_desc",seriesDesc_);
	seriesDescRecord.setValue("study_uid",seriesRecord.value("study_uid"));
	for(int i = seriesDescRecord.count() - 1; i >= 0; i--)
		if(seriesDescRecord.isNull(i))
			seriesDescRecord.remove(i);

	bool isDynMulti = false;
	//bool isFastDynamic = false;
	int nSeriesGE = 2;
	bool splitBreasts = false;
	bool capitalP = false;
	if( (seriesDesc_.trimmed().endsWith("pre") || seriesDesc_.trimmed().endsWith("pre fat sat") || seriesDesc_.trimmed().endsWith("pre FS") ||
	  ( seriesUID_.startsWith("1.3.6.1.4.1.18047") && ( seriesDesc_.contains(" PRE") || seriesDesc_.contains(" Pre") || seriesDesc_.contains(" pre") ) ) ||
	  ( seriesDesc_.trimmed().endsWith("VIBRANT+C") )) && !seriesUID_.startsWith("1.3.12.2.1107.5.2."))
	{
#if useDebug
		qDebug() << "Precontrast Series Description Detected";
#endif
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
		             + QString(" ORDER BY CAST(series_number AS INT)");
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		sqlQuery->first();
		seriesRecord = sqlQuery->record();
		seriesUID_ = seriesRecord.value("series_uid").toString();
		uidRecord.setValue("series_uid",seriesUID_);
#if useDebug
		qDebug() << "Series Number Used:" << seriesRecord.value("series_number").toString() << "   Series UID Used:" << seriesUID_;
#endif
		QString seriesDescPost = seriesDesc_;
		if(seriesDesc_.contains(" pre")){
			seriesDescPost.replace(" pre"," post");
			if(seriesDescPost.endsWith(' '))
				seriesDescPost.resize(seriesDescPost.size() - 1);
			else
				seriesDescPost.append(' ');
			/*
			seriesDescPost.replace("pre fat sat ","post fat sat");
			seriesDescPost.replace("pre fat sat","post fat sat ");
			seriesDescPost.replace("pre FS ","post FS");
			seriesDescPost.replace("pre FS","post FS ");
			seriesDescPost.replace("pre ","post");
			seriesDescPost.replace("pre","post ");
			*/
		}
		else{
			capitalP = true;
			if( seriesDesc_.contains("AXIAL T1 PRE") )
				seriesDescPost = "AXIAL T1 POST%";
			else if( seriesDesc_.contains("Axial T1 Pre") )
				seriesDescPost = "Axial T1 Post%";
			//else if( seriesDesc_.contains("Axial T1 FS pre") ) // This should be caught above since it has 'pre' in it
			//    seriesDescPost = "Axial T1 FS post%";
			else if( seriesDesc_.contains("Axial Vibrant Pre") )
				seriesDescPost = "Axial Vibrant Post%";
			else if( seriesDesc_.contains("VIBRANT+C") )
				seriesDescPost = "VIBRANT+C%";
		}
		seriesDescPost.prepend('%');
		seriesDescRecord.setValue("series_desc",seriesDescPost);
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
		             + QString(" ORDER BY CAST(series_number AS INT)");

		sqlStatement.replace("series_desc\" = ", "series_desc\" LIKE ");
#if useDebug
		qDebug() << "SQL Statement:" << sqlStatement;
#endif

		sqlQuery->clear();
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		if(sqlQuery->next()){
			isDynMulti = true;
#if useDebug
			qDebug() << "Postcontrast num_temporal_positions:" << sqlQuery->record().value("num_temporal_positions").toInt();
#endif
			if(nt_ < 1){ // We may have to read the number of timepoints from the post series
				nt_ = 1 + sqlQuery->record().value("num_temporal_positions").toInt();
				if(capitalP)
					nSeriesGE = nt_;
			}
			if(nt_ == 1){ // We are missing the number of temporal positions
				int nextSeriesNum;
				do{
					nextSeriesNum = sqlQuery->record().value("series_number").toInt() + 1;
					++nt_;
					if(nextSeriesNum > 100){
						++nextSeriesNum;
						//sqlQuery->next();
					}
				}while( sqlQuery->next() && ( ( nextSeriesNum == sqlQuery->record().value("series_number").toInt() ) || ( nextSeriesNum > 100 ) ) );
				if(nt_ == 2 && nextSeriesNum < 100){
					sqlQuery->first();
#if useDebug
					qDebug() << "GE with one post series found, check to see if it has multiple time points";
					qDebug() << "Series Number" << sqlQuery->record().value("series_number").toInt();
#endif
					QSqlRecord postUidRecord = uidRecord;
					postUidRecord.setValue("series_uid", sqlQuery->record().value("series_uid"));
					QSqlRecord postTriggerTimeRecord = uidRecord;
					postTriggerTimeRecord.append( QSqlField("instance_num", QVariant::String) );
					postTriggerTimeRecord.append( QSqlField("trigger_time", QVariant::Int) );

					// We'll get these images later, so pulling them into cache now shouldn't be a big deal
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,postTriggerTimeRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,postUidRecord,false);
					sqlQuery->clear();
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();

					QVector<int> triggerTimes;
					while(sqlQuery->next()){
#if useDebug
						qDebug() << sqlQuery->record().value("instance_num").toInt() << ":" << sqlQuery->record().value("trigger_time").toInt();
#endif
						if(!triggerTimes.contains(sqlQuery->record().value("trigger_time").toInt()))
							triggerTimes.append(sqlQuery->record().value("trigger_time").toInt());
					}
					nt_ = 1 + triggerTimes.size();
				}
				nSeriesGE = nt_;
			}
		}
#if useDebug
		qDebug() << "Series Description Pre:" << seriesDesc_ << "   Series Description Post:" << seriesDescPost << "   Number of Time Points Detected:" << nt_ << "   Number of Series Used:" << nSeriesGE;
#endif
	}

	for(const auto & s : fastDynamicSeriesDesc)
		if(seriesDesc_.contains(s))
			isFastDynamic_ = true;

#if useDebug
	qDebug() << "fast dynamic series desc:" << fastDynamicSeriesDesc;
	qDebug() << "series desc:" << seriesDesc_;
	if(isFastDynamic_)
		qDebug() << "Fast Dynamic Scan Found!";
#endif

	// New query to get the image data for this series
	sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
	             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);

	int progressMaximum = nt_ * 250 * 2;
	emit openProgressNewMaximum(progressMaximum);  // (n_TimePts*250*2) is an estimate of twice the number of slices we are going
	                                               // to read. Will be exact if each timept has 250 slices. We only
	                                               // have the n_TimePts after reading the series table. It isn't critical
	                                               // if it isn't exact since it's only for the progressbardialog
	                                               // Also, note that the SQLITE driver doesn't support sqlQuery->size();
	/*QProgressDialog *fileOpenProgress = new QProgressDialog("Opening MRI Case (time depends on speed of disk or network)","Please Wait...",0,100,parent);
	fileOpenProgress->setWindowTitle("Opening Image...");
	QPushButton* abortProgressButton = new QPushButton("Please Wait...");
	fileOpenProgress->setCancelButton(abortProgressButton);
	fileOpenProgress->setWindowModality(modalProgress);
	abortProgressButton->setEnabled(false);
	fileOpenProgress->setMinimumDuration(0);
	fileOpenProgress->setValue(1);
	fileOpenProgress->show();
	qApp->processEvents();*/

	//Run the SQL Query in a separate thread so that the UI still updates
	//QEventLoop sqlWaitLoop;

	//QFutureWatcher<bool> sqlWatcher;
	//QObject::connect(&sqlWatcher,SIGNAL(finished()),&sqlWaitLoop,SLOT(quit()));
	sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
	//sqlWatcher.setFuture(sqlFuture);
	//sqlWaitLoop.exec();
	sqlFuture.waitForFinished();
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit finishedMRImageQuery();
#endif
#if useDebug
	int queryTime = whole.elapsed();
#endif

	//int index_imageUID      = imageRecord.indexOf("image_uid");
	int index_time          = imageRecord.indexOf("image_time");
	int index_date          = imageRecord.indexOf("image_date");
	int index_width         = imageRecord.indexOf("width");
	int index_height        = imageRecord.indexOf("height");
	int index_pixelsize_x   = imageRecord.indexOf("pixelsize_x");
	int index_pixelsize_y   = imageRecord.indexOf("pixelsize_y");
	int index_windowWidth   = imageRecord.indexOf("window_width");
	int index_windowCenter  = imageRecord.indexOf("window_center");
	int index_minPixval     = imageRecord.indexOf("min_pixval");
	int index_maxPixval     = imageRecord.indexOf("max_pixval");
	int index_imagePosition = imageRecord.indexOf("image_position");
	int index_sliceLocation = imageRecord.indexOf("slice_location"); // This is only used temporarily
	int index_temporalPosID = imageRecord.indexOf("temporal_pos_id");
	int index_diffusionBValue = imageRecord.indexOf("diffusion_b_value");
	//int index_diffusionGradientOrientation = imageRecord.indexOf("diffusion_gradient_orientation");
	int index_instanceNum   = imageRecord.indexOf("instance_num");
	//int index_idqi_images   = imageRecord.indexOf("idqi_images");
	int index_image_data    = imageRecord.indexOf("image_data");
	int index_scanOptions   = imageRecord.indexOf("scan_options");
	int index_manufacturer  = imageRecord.indexOf("manufacturer");
	int index_modelName     = imageRecord.indexOf("model_name");
	int index_mfs           = imageRecord.indexOf("magnetic_field_strength");

	int index_imageType     = imageRecord.indexOf("image_type");
	QStringList imageTypesToSkip;
	if(seriesDesc_.contains("VISTA")){
		imageTypesToSkip << "PHASE MAP"; // This is a phase map embedded into T2 series used at UChicago
		imageTypesToSkip << "I_SE" << "R_SE"; // Looks like UC sometimes splits the real and imaginary components too
	}
	if(seriesUID_.startsWith("1.3.12.2.1107.5.2.30.25609.") && !seriesDesc_.contains("localizer"))
		imageTypesToSkip << "LOCALIZER"; // Ignore localizer images bundled with a 3D series

	int querySize = 0;
	QVector<float> locations_temp;
	QVector<QVector3D> imagePositions_;
	if(!sqlQuery->first())
		throw(std::runtime_error("no sql query results"));
	do{
		querySize++;
		OPENPROGRESSUPDATE(sqlQuery->at());
		//fileOpenProgress->setValue(sqlQuery->at());

		bool skipImageType = false;
		for(const auto & imgTypeSkip : std::as_const(imageTypesToSkip))
			if(sqlQuery->value(index_imageType).toString().contains(imgTypeSkip))
				skipImageType = true;
		if(skipImageType)
			continue;

		QString manufacturerTemp = sqlQuery->value(index_manufacturer).toString();

		bool floatConvertSuccessful = false;
		auto sliceLocationValue = sqlQuery->value( index_sliceLocation ).toFloat( &floatConvertSuccessful );
		if( floatConvertSuccessful ) {
			if( !locations_temp.contains( sliceLocationValue ) ) {
				locations_temp << sliceLocationValue;
			}
		}

		auto currentImagePosition = sqlQuery->value(index_imagePosition).toString().split("\\");
		if( (currentImagePosition.size() >= 3) &&
		    (!imagePositions_.contains(QVector3D(currentImagePosition.at(0).toFloat(), currentImagePosition.at(1).toFloat(), currentImagePosition.at(2).toFloat()))) )
			imagePositions_ << QVector3D(currentImagePosition.at(0).toFloat(), currentImagePosition.at(1).toFloat(), currentImagePosition.at(2).toFloat());

		if(!bValues_.contains(sqlQuery->value(index_diffusionBValue).toDouble()))
			bValues_ << sqlQuery->value(index_diffusionBValue).toDouble();
	}while(sqlQuery->next());
	std::sort(locations_temp.begin(), locations_temp.end());
#if useDebug
	qDebug() << "(a) locations_temp" << locations_temp;
#endif
	std::sort(bValues_.begin(), bValues_.end());
	sqlQuery->first();
	progressMaximum = 2 * querySize;
	emit openProgressNewMaximum(progressMaximum);
	emit openProgressUpdated(querySize);
	//fileOpenProgress->setMaximum(2*querySize);
	//fileOpenProgress->setValue(querySize);
#if testFullHeader
	QString imageUID = sqlQuery->value(imageRecord.indexOf("image_uid")).toString();
	QMultiHash<QString, QString> header = qisql->extractAllDicomFields(imageUID);
	QVector<QString> patientID = header.values("0010,0010");
	qDebug()<<"Header Size:"<<header.count();
	qDebug()<<"Patient ID:"<<patientID.first();
#endif

	QStringList orientationString = seriesRecord.value("image_orientation").toString().split('\\');
	for(int i = 0; i < orientationString.size(); i++) {
		bool ok;
		float o = orientationString.at(i).toFloat(&ok);
		if(ok)
			orientation_ << o;
	}
	if(orientationString.isEmpty() || orientationString.first().isEmpty())
		throw(std::runtime_error("No Image Orientation")); // Maybe we should handle a NOPLANE orientation?
#if useDebug
	qDebug() << "Orientation String: " << orientationString;
#endif
	date_ = sqlQuery->value(index_date).toDate();
	maxPixval_ = static_cast<quint16>(sqlQuery->value(index_maxPixval).toUInt());
	minPixval_ = static_cast<quint16>(sqlQuery->value(index_minPixval).toUInt());
	float imagedx = sqlQuery->value(index_pixelsize_x).toFloat();
	float imagedy = sqlQuery->value(index_pixelsize_y).toFloat();
	float imagedz = seriesRecord.value("space_between_slices").toFloat();
	int imagenx = sqlQuery->value(index_width).toInt();
	int imageny = sqlQuery->value(index_height).toInt();
	int imagenz = locations_temp.size();
	if(imagenz <= 1) // No images in this series!
		return -100;
	// Fix for spaceBetweenSlices not existing in header
	if(imagedz <= 0.0f){
#if useDebug
		qDebug() << "Space between slices is not in the database for this case!!!!!";
		qDebug() << "First Location:" << locations_temp.first();
		qDebug() << "Last Location:" << locations_temp.last();
		qDebug() << "Number of slices:" << imagenz;
		qDebug() << "old imagedz:" << imagedz << " new imagedz:" << ( locations_temp.last() - locations_temp.first() ) / (imagenz - 1);
#endif
		imagedz = ( locations_temp.last() - locations_temp.first() ) / (imagenz - 1);
	}

	int imgy,imgx,imgz;
	int *patx = &imgx, *paty = &imgy, *patz = &imgz;
	//Find first dimension
	if(qRound(orientation_[0]) != 0){
#if useDebug
		qDebug() << "First Dimension is X:" << orientation_[0];
#endif
		nx_ = imagenx;
		dx_ = imagedx * qRound(orientation_[0]);
		patx = &imgx;
	}
	if(qRound(orientation_[1]) != 0){
#if useDebug
		qDebug() << "First Dimension is Y" << orientation_[1];
#endif
		ny_ = imagenx;
		dy_ = imagedx * qRound(orientation_[1]);
		paty = &imgx;
	}
	if(qRound(orientation_[2]) != 0){
#if useDebug
		qDebug() << "First Dimension is Z" << orientation_[2];
#endif
		nz_ = imagenx;
		dz_ = imagedx * qRound(orientation_[2]);
		patz = &imgz;
	}
	//Find second dimension
	if(qRound(orientation_[3]) != 0){
#if useDebug
		qDebug() << "Second Dimension is X:" << orientation_[3];
#endif
		nx_ = imageny;
		dx_ = imagedy * qRound(orientation_[3]);
		patx = &imgy;
	}
	if(qRound(orientation_[4]) != 0){
#if useDebug
		qDebug() << "Second Dimension is Y:" << orientation_[4];
#endif
		ny_ = imageny;
		dy_ = imagedy * qRound(orientation_[4]);
		paty = &imgy;
	}
	if(qRound(orientation_[5]) != 0){
#if useDebug
		qDebug() << "Second Dimension is Z:" << orientation_[5];
#endif
		nz_ = imageny;
		dz_ = imagedy * qRound(orientation_[5]);
		patz = &imgy;
	}
	//Find third dimension
	if(qRound(orientation_[0]) == 0 && qRound(orientation_[3]) == 0){
#if useDebug
		qDebug() << "Third Dimension is X:" << orientation_[3];
#endif
		nx_ = imagenz;
		dx_ = imagedz;
		patx = &imgz;
//        if(orientationString[0].startsWith('-') && orientationString[3].startsWith('-'))
//            dx_ = -dx_;
	}
	if(qRound(orientation_[1]) == 0 && qRound(orientation_[4]) == 0){
#if useDebug
		qDebug() << "Third Dimension is Y:" << orientation_[4];
#endif
		ny_ = imagenz;
		dy_ = imagedz;
		paty = &imgz;
//        if(orientationString[1].startsWith('-') && orientationString[4].startsWith('-'))
//            dy_ = -dy_;
	}
	if(qRound(orientation_[2]) == 0 && qRound(orientation_[5]) == 0){
#if useDebug
		qDebug() << "Third Dimension is Z:" << orientation_[5];
#endif
		nz_ = imagenz;
		dz_ = imagedz;
		patz = &imgz;
//        if(orientationString[2].startsWith('-') && orientationString[5].startsWith('-'))
//            dz_ = -dz_;
	}
	// determine acquisition orientation (like the above, this assumes image is aligned with one of the patient planes)
	if ((qRound(orientation_[0]) * qRound(orientation_[4]) != 0) || (qRound(orientation_[1]) * qRound(orientation_[3]) != 0)){
#if useDebug
		qDebug() << "Acquisition plane is axial, resolution:" << dx_ << dy_ << dz_;
#endif
		acquisitionOrientation_ = QUANTX::XYPLANE;
	} else if ((qRound(orientation_[0]) * qRound(orientation_[5]) != 0) || (qRound(orientation_[2]) * qRound(orientation_[3]) != 0)){
#if useDebug
		qDebug() << "Acquisition plane is coronal, resolution:" << dx_ << dy_ << dz_;
#endif
		acquisitionOrientation_ = QUANTX::XZPLANE;
	} else if ((qRound(orientation_[1]) * qRound(orientation_[5]) != 0) || (qRound(orientation_[2]) * qRound(orientation_[4]) != 0)){
#if useDebug
		qDebug() << "Acquisition plane is sagittal, resolution:" << dx_ << dy_ << dz_;
#endif
		acquisitionOrientation_ = QUANTX::YZPLANE;
	}

	QVector<float> locations_ = getSliceLocations(imagePositions_, acquisitionOrientation_);
#if useDebug
	qDebug() << "(b) locations_ from ImagePosition: " << locations_;
#endif



	// Check to see if the two breasts are split into different series
	if(acquisitionOrientation_ == QUANTX::YZPLANE){
		QString seriesDescPost = seriesDescRecord.value("series_desc").toString();
		seriesDescRecord.setValue("series_desc",seriesDesc_);
		seriesDescRecord.setValue("study_uid",seriesRecord.value("study_uid"));
		for(int i = seriesDescRecord.count() - 1; i >= 0; i--)
			if(seriesDescRecord.isNull(i))
				seriesDescRecord.remove(i);
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
		             + QString(" ORDER BY CAST(series_number AS INT)");
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		sqlQuery->first();
		int seriesNum = sqlQuery->record().value("series_number").toInt();
		if( (sqlQuery->record().value("series_uid").toString() == seriesUID_) && (sqlQuery->next()) ){
			if(sqlQuery->record().value("series_number").toInt() == seriesNum + 1){
				splitBreasts = true;
				if( nt_ == nSeriesGE )
					nt_ -= std::floor(nt_ / 2);
			}
		}
		else if(seriesDesc_.contains("left")){
			QString rightSeriesDesc = seriesDesc_.trimmed().replace("left","right");
			if(rightSeriesDesc.size() % 2)
				rightSeriesDesc += ' ';
			seriesDescRecord.setValue("series_desc",rightSeriesDesc);
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
			             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
			             + QString(" ORDER BY CAST(series_number AS INT)");
			sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
			sqlFuture.waitForFinished();
			if(sqlQuery->first())
				splitBreasts = true;
		}
		if(splitBreasts){
#if useDebug
			qDebug() << "Left and Right Breasts are in Separate Series";
			qDebug() << "using 'imagePosition' for 'locations_";
#endif
			uidRecord.setValue("series_uid", sqlQuery->record().value("series_uid"));
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
			             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
			sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
			sqlFuture.waitForFinished();
			sqlQuery->first();
			float currentLocation;
			do{
				querySize++;
				currentLocation = sqlQuery->value(index_imagePosition).toString().split("\\").first().toFloat();
				if(!locations_.contains(currentLocation))
					locations_ << currentLocation;
			}while(sqlQuery->next());
			std::sort(locations_.begin(), locations_.end());
#if useDebug
			qDebug() << "Locations: " << locations_;
#endif

			int imagenz = locations_.size();
			nx_ = imagenz;
			progressMaximum= 2 * querySize;
			emit openProgressNewMaximum(progressMaximum);
			emit openProgressUpdated(querySize);
		}
		seriesDescRecord.setValue("series_desc",seriesDescPost);
		uidRecord.setValue("series_uid",seriesUID_);
		sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
		             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
		sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
		sqlFuture.waitForFinished();
		sqlQuery->first();
	}

	nb_ = bValues_.size();
	if(nt_ < 1)
		nt_ = 1;
	if(nb_ < 1)
		nb_ = 1;
#if useDebug
	if (nt_ > 1)
		qDebug() << "Dynamic Series!";
	else if (nb_ > 1){
		qDebug() << "Diffusion Series!";
		qDebug() << "B-Values: " << bValues_;
	} else
		qDebug() << "Standard 3D Series";
#endif
	if(index_scanOptions >= 0)
		scanOptions_ = sqlQuery->value(index_scanOptions).toString().trimmed().split('\\');
	else
		scanOptions_.clear();

	manufacturer_ = sqlQuery->value(index_manufacturer).toString();
	modelName_    = sqlQuery->value(index_modelName).toString();
	magneticFieldStrength_ = sqlQuery->value(index_mfs).toFloat();
	if(magneticFieldStrength_ <= 0.0f){ // Update table if magnetic field strength is missing
		QFuture<bool> sqlUpdateFuture;
		QScopedPointer<QSqlQuery> sqlUpdateQuery(new QSqlQuery(db));
		QSqlRecord dicomUIDRecord;
		dicomUIDRecord.append(QSqlField("0008,0018",QVariant::String));
		dicomUIDRecord.setValue("0008,0018",sqlQuery->value("image_uid"));
		QSqlRecord dicomMFSRecord;
		dicomMFSRecord.append(QSqlField("0018,0087",QVariant::String));
		QString sqlUpdateStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,"dicom_data",dicomMFSRecord,false) + QString(' ')
		                           + db.driver()->sqlStatement(QSqlDriver::WhereStatement, "dicom_data",dicomUIDRecord,false);
		sqlUpdateFuture = QtConcurrent::run(sqlUpdateQuery.data(),&QSqlQuery::exec,sqlUpdateStatement);
		sqlUpdateFuture.waitForFinished();
		sqlUpdateQuery->first();
		magneticFieldStrength_ = sqlUpdateQuery->value("0018,0087").toString().toFloat();
#if useDebug
		qDebug() << "Magnetic Field Strength Missing, Magnetic Field is" << magneticFieldStrength_ << "Tesla";
#endif
		QSqlRecord imageUIDRecord;
		imageUIDRecord.append(imageRecord.field("image_uid"));
		imageUIDRecord.setValue("image_uid",sqlQuery->value("image_uid"));
		QSqlRecord imageMFSRecord;
		imageMFSRecord.append(imageRecord.field("magnetic_field_strength"));
		imageMFSRecord.setValue("magnetic_field_strength",magneticFieldStrength_);
		sqlUpdateStatement = db.driver()->sqlStatement(QSqlDriver::UpdateStatement,"qi_2d_images",imageMFSRecord,false) + QString(' ')
		                   + db.driver()->sqlStatement(QSqlDriver::WhereStatement, "qi_2d_images",imageUIDRecord,false);
		sqlUpdateFuture = QtConcurrent::run(sqlUpdateQuery.data(),&QSqlQuery::exec,sqlUpdateStatement);
		sqlUpdateFuture.waitForFinished();
	}

	int dynMultiType = 0;
	for(const auto & dynMultiDesc : dynMultiSeriesDesc)
		isDynMulti = isDynMulti || seriesDesc_.startsWith(dynMultiDesc, Qt::CaseInsensitive) ||
		             seriesDesc_.startsWith(QString("Motion Corr. ") + dynMultiDesc, Qt::CaseInsensitive);
	if (isDynMulti){
		//don't consider series that contain any of these strings to be part of a multiseries DCE, even if they match
		// with a string in dynMultiDesc
		QStringList excludeDynMultiDescs{"interview",
			                             "sub"};
		for(const auto &excludeDesc : excludeDynMultiDescs){
			if (seriesDesc_.contains(excludeDesc, Qt::CaseInsensitive)){
				isDynMulti = false;
				break;
			}
		}
	}

	bool dynMultiBigGap = false;
	QMultiMap<int, quantx::mranalysis::DicomSeriesInfo> studySeriesInfo;
	int dim4Size;// fourth dimension of the data set, nb if DWI, nt otherwise
	if (nb_ > 1)
		dim4Size = nb_;
	else{
		if(isDynMulti){
#if useDebug
			qDebug() << "Multiple 3D Series Dynamic Scan!";
#endif
			// We need to get all the series info for these, so we can know if we're done, or just skipping non-motion-corrected
			// series UIDs when one doesn't come back in the query in the do / while block below.
			// While it's not that efficient to do this here, there's not really any way around it without a more
			// substantial refactoring, since we don't get the study UID until this point anyway.
			studySeriesInfo = QISql::getSeriesInfo(dbSettings, studyUID_);

			//detect number of timepoints, we clear the sqlQuery so be careful not to collect anything important after this
			//Check if series UID for dynamic has a timepoint suffix
			int endOffsetIdx = 1;
			if (seriesUID_.endsWith(".mc"))
				endOffsetIdx = 4;
			int uidStart = seriesUID_.midRef(seriesUID_.size() - endOffsetIdx,1).toInt();
			if( seriesUID_.at(seriesUID_.size() - (endOffsetIdx + 1)) == '.' && !seriesDesc_.contains("TRA FL3D")){ // exclude series w/ this series description, want to use series description matching section instead
				dynMultiType = 1;
#if useDebug
				qDebug() << "Standard Series UIDs";
#endif

				nt_ = 0;
				int currentSeriesNum = -3;
				bool querySuccess;
				do{
					if(nt_ > 0){
						currentSeriesNum = sqlQuery->record().value("series_number").toInt();
					}
					nt_++;
					sqlQuery->clear();
					if (endOffsetIdx == 1)
						uidRecord.setValue("series_uid",seriesUID_.mid(0,seriesUID_.size() - 1) + QString::number(nt_ + uidStart));
					else
						uidRecord.setValue("series_uid",seriesUID_.mid(0,seriesUID_.size() - endOffsetIdx) + QString::number(nt_ + uidStart) + ".mc");
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,uidRecord,false);

					auto valuesForSeries = studySeriesInfo.values(nt_ + uidStart);
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					querySuccess = sqlQuery->first();
					if (!querySuccess && valuesForSeries.size() == 0) {
						// Store the current value of nt_ so we know how far in to index to remove invalid collections of instanceNums
						instanceNumsIndicesToRemove.emplace(nt_ - 2); // Subtract one to get an index, one more because we incremented it earlier

						// This is a bit hacky but trying to avoid altering the previous flow as much as possible, due to short testing time,
						// but we want to make sure that in this case we don't have one too many nt_
//						nt_--;
						break;
					}
					//the second clause below, studySeriesInfo.values ..., is meant for continuing the loop when there are gaps in the UID increment for motion correction cases but only when the last UID portion and the series number are the same for the series in the case, so lets add that additional condition
				}while(querySuccess || (studySeriesInfo.values(nt_ + uidStart).size() == 1 && (seriesNums_.first().toInt() == uidStart))); // There should only be one here, if we aren't getting a motion corr. -- see QUANTX-1212
				/*
				if(nt_ == 0 && seriesDesc_.contains("TTC=")){
					int ttcLoc = seriesDesc_.indexOf("=");
					seriesDesc_ = seriesDesc_.mid(0,ttcLoc);
					seriesDescRecord.setValue("series_desc",seriesDesc_ + "%");
				}*/
				// Check for break in series with interim scan
				if(seriesDesc_.contains("POST")){
					//seriesDescRecord.setValue("series_desc","fl3d_tra_interVIEWS ");
					sqlQuery->clear();
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false);
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					while(sqlQuery->next())
						if(sqlQuery->record().value("series_number").toInt() == (currentSeriesNum + 2) ){
							++nt_;
						}
					//seriesDescRecord.setValue("series_desc",seriesDesc_);
				}
				if( nt_ < 2 && seriesUID_.startsWith("1.3.12.2.1107.5.2.")){
					if(seriesDesc_.contains("_pre", Qt::CaseInsensitive)){
						seriesDescRecord.setValue("series_desc", seriesDesc_.mid(0, seriesDesc_.indexOf("_pre", 0, Qt::CaseInsensitive)) + "%");
					} else if(seriesDesc_.contains("pre", Qt::CaseInsensitive)){
						seriesDescRecord.setValue("series_desc", seriesDesc_.mid(0, seriesDesc_.indexOf("pre", 0, Qt::CaseInsensitive)) + "%");
					} else {
						seriesDescRecord.setValue("series_desc", seriesDesc_ + "%");
					}
					dynMultiBigGap = true;
					sqlQuery->clear();
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false);

					if(seriesDescRecord.value("series_desc").toString().contains("%"))
						sqlStatement.replace("series_desc\" = ", "series_desc\" LIKE ");
#if useDebug
					qDebug() << "sqlStatement:" << sqlStatement;
#endif

					sqlFuture = QtConcurrent::run(sqlQuery.data(), &QSqlQuery::exec, sqlStatement);
					sqlFuture.waitForFinished();
					// Just grab everything with the same series description (though adding scan parameters would help)
					nt_ = 0;
					instanceNumsIndicesToRemove.clear();
					while(sqlQuery->next()){
						++nt_;
					}
				}
			}
			else if(seriesDesc_.contains(" pre") || capitalP){
				dynMultiType = 3;
#if useDebug
				qDebug() << "GE-style dynamic image using two series (MSKCC)";
#endif
			}
			//Check for series description Pre->Post
			else{
				dynMultiType = 2;
#if useDebug
				qDebug() << "Custom Series UIDs!";
#endif
				int uidStart = seriesUID_.split('.').last().toInt(); // This won't always work anymore
				QString uidPrefix = seriesUID_.mid(0,seriesUID_.size() - seriesUID_.split('.').last().size());

				QString seriesDescPost = seriesDesc_;
				int index_seriesDesc = seriesRecord.indexOf("series_desc");
				nt_ = 0;
				bool seriesStartsWith = false;
				QStringList startsWithFilter{"t1_fl3d_tra_dyn",
					                         "T1 FL3D SAG",
					                         "T1_FL3D_SAG",
					                         "T1 FL3D AX",
					                         "T1_FL3D_TRA_",
					                         "TRA FL3D",
				                             "fl3d_"};
				for (const auto &s : startsWithFilter){
					if (seriesDesc_.startsWith(s) || seriesDesc_.startsWith("Motion Corr. " + s)){
						seriesStartsWith = true;
						break;
					}
				}
				if( seriesDesc_.contains("POST") || seriesDesc_.startsWith("fl3d_") ||
				    (seriesStartsWith /*&& !seriesDesc_.trimmed().endsWith("Pre") && !seriesDesc_.trimmed().endsWith("Post") */)/* || seriesDesc_.startsWith("AXIAL DYN 1 PHASE") */ ){
					// Siemens with [syngo MR B19] software version should be picked up here?
					// We might also just be able to make the search for "post" case insensitive to pick up these cases instead of using startwith()
					seriesDescRecord.setValue("series_desc",seriesDesc_);
					seriesDescRecord.setValue("study_uid", seriesRecord.value("study_uid"));
					for(int i = seriesDescRecord.count() - 1; i >= 0; i--)
						if(seriesDescRecord.isNull(i))
							seriesDescRecord.remove(i);
					sqlQuery->clear();
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
					               + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false);
					for (const auto & likeSeriesDesc : startsWithFilter){
						if(seriesDesc_.contains(likeSeriesDesc)){
							QString baseSeriesDesc(likeSeriesDesc);
							sqlStatement.replace("series_desc\" = ", QString("series_desc\" LIKE "));
							sqlStatement.replace(seriesDesc_, seriesDesc_.mid(0,seriesDesc_.indexOf(baseSeriesDesc) + baseSeriesDesc.length()) + "%");
						}
					}
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					while(sqlQuery->next())
						if (!sqlQuery->value("series_desc").toString().contains("SUB", Qt::CaseInsensitive) &&
						    !sqlQuery->value("series_desc").toString().contains("MIP", Qt::CaseInsensitive)){
							++nt_;
						}
				}
				else{
					seriesDescPost.replace("PRE ","POST"); //DICOM Strings should be even length
					seriesDescPost.replace("PRE","POST ");
					do{
						++nt_;
						sqlQuery->clear();
						uidRecord.setValue("series_uid", uidPrefix + QString::number(nt_ + uidStart));
						sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
						             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,uidRecord,false);
						sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
						sqlFuture.waitForFinished();
					}while(sqlQuery->first() && (sqlQuery->value(index_seriesDesc).toString() == seriesDescPost));

					auto uidPrefix2 = uidPrefix;
					auto uidvalstr = uidPrefix2.split('.', QString::SkipEmptyParts).last();
					auto uidval = uidvalstr.toULongLong();
					uidPrefix2.replace(uidvalstr, QString::number(++uidval));
#if useDebug
					qDebug() << "uidval:" << uidval << "| uidPrefix2:" << uidPrefix2;
#endif

					nt_ -= 1;
					do{
						++nt_;
						sqlQuery->clear();
						uidRecord.setValue("series_uid", uidPrefix2 + QString::number(nt_ + uidStart));
						sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
						             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,uidRecord,false);
						sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
						sqlFuture.waitForFinished();
					}while(sqlQuery->first() && (sqlQuery->value(index_seriesDesc).toString() == seriesDescPost));

				}
			}
			if (dynMultiType < 3){
				for (int t= instanceNums_.size(); t<nt_; t++)
					instanceNums_ << QMap<float, QString>();
#if useDebug
				qDebug() << "InstanceNums 2:" << instanceNums_;
#endif
			}
		}
		dim4Size = nt_;
	}
#if useDebug
	qDebug() << "4th Dimension Size: " << dim4Size;
	qDebug() << "Dicom Series Nums: " << seriesNums_;
#endif

#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit determinedMRSize();
	qApp->processEvents();
#endif
	data_.clear();
	data_.resize(dim4Size);
	for(int t = 0; t < dim4Size; t++){
		data_.at(t) = (std::vector<std::vector<std::vector<quint16> > >(nz_));
		for(int z = 0; z < nz_; z++){
			data_[t].at(z) = (std::vector<std::vector<quint16> >(ny_));
			for(int y = 0; y < ny_; y++){
				data_[t][z].at(y) = (std::vector<quint16>(nx_));
			}
		}
	}
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit builtMRBuffer();
	qApp->processEvents();
#endif
	// There are cases where we do not yet know what the final nt_ will be at this point. Safest to populate this for all MR Series
	//if (nt_ > 1 ){
	    for (int t = instanceNums_.size() ; t < dim4Size; t++)
			instanceNums_ << QMap<float, QString>();
	//}

	// NOTE: windowCenter and windowWidth are stored as double in the QiAbstractImage class
	auto sumWidths = 0.0;
	auto sumCenters = 0.0;
	QStringList positionString;
	positions_.resize(nz_);
	//int timePt;
	timestamps_.resize(nt_);
	QByteArray imageData;
	quint16 *imgptr,*dataptr;
	quint16 curmax;
	quint16 curmin;
	int queryProgress = 0;
#if useDebug
	loop.start();
#endif
	if(isDynMulti){
#if useDebug
		qDebug() << "dynMultiType:" << dynMultiType;
#endif
		if(dynMultiType < 3){
			// This assumes an axial image
			auto seriesUidNoMC = seriesUID_;
			seriesUidNoMC.remove(".mc");
			int uidStart = seriesUidNoMC.split('.').last().toInt();
			QString uidPrefix = seriesUidNoMC.mid(0, seriesUidNoMC.size() - seriesUidNoMC.split('.').last().size());
			int skipImageOffset = 0;
			for(int timePt = 0; timePt < nt_; timePt++){
				bool seriesStartsWith = false;
				QStringList startsWithFilter{"t1_fl3d_tra_dyn",
					                         "T1 FL3D SAG",
					                         "T1_FL3D_SAG",
					                         "T1 FL3D AX",
					                         "T1_FL3D_TRA_",
					                         "TRA FL3D",
				                            "fl3d_"};
				for (const auto &s : startsWithFilter){
					if (seriesDesc_.startsWith(s) || seriesDesc_.startsWith("Motion Corr. " + s)){
						seriesStartsWith = true;
						break;
					}
				}
				if( seriesDesc_.contains("POST") || ((dynMultiType == 2) && (seriesDesc_.startsWith("fl3d_") ||
				                                                             (seriesStartsWith /*&& !seriesDesc_.trimmed().endsWith("Pre") && !seriesDesc_.trimmed().endsWith("Post") */))) ||
				    dynMultiBigGap ){
					// Siemens with [syngo MR B19] software version should be picked up here? dynMultiType = 1 also seems to load fine using this section of code
					// We might also just be able to make the search for "post" case insensitive to pick up these cases instead of using startwith()
					sqlQuery->clear();
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
					             + QString(" ORDER BY series_date ASC, series_time ASC, cast(series_number as INTEGER) ASC");
					for (const auto & likeSeriesDesc : startsWithFilter){
						if(seriesDesc_.contains(likeSeriesDesc)){
							QString baseSeriesDesc(likeSeriesDesc);
							sqlStatement.replace("series_desc\" = ", QString("series_desc\" LIKE "));
							sqlStatement.replace(seriesDesc_, seriesDesc_.mid(0,seriesDesc_.indexOf(baseSeriesDesc) + baseSeriesDesc.length()) + "%");
						}
					}

					if(seriesDescRecord.value("series_desc").toString().contains("%"))
						sqlStatement.replace("series_desc\" = ", "series_desc\" LIKE ");

					sqlFuture = QtConcurrent::run(sqlQuery.data(), &QSqlQuery::exec, sqlStatement);
					sqlFuture.waitForFinished();
					sqlQuery->first();
					for(int i = 0; i < timePt; ++i){
						sqlQuery->next();
						// Skip the sagittal reformatting from Siemens Scanners
						while( ((sqlQuery->value("series_number").toInt() >= 100) && (sqlQuery->value("series_uid").toString().startsWith("1.3.12.2.1107.5.2."))) ||
						       sqlQuery->value("series_desc").toString().contains("SUB", Qt::CaseInsensitive) ||
						       sqlQuery->value("series_desc").toString().contains("MIP", Qt::CaseInsensitive))
							sqlQuery->next();
					}
					uidRecord.setValue("series_uid",sqlQuery->value("series_uid"));
					associatedSeries_ << sqlQuery->value("series_uid").toString();
#if useDebug
					qDebug() << "Series UID for Timepoint" << timePt << ":" << sqlQuery->value("series_uid");
#endif
				}
				else{
					auto currentSeriesUID = uidPrefix + QString::number(timePt + uidStart) + (seriesUID_.endsWith(".mc") ? ".mc" : "");
					uidRecord.setValue("series_uid", currentSeriesUID);
					if (timePt > 0)
						associatedSeries_ << currentSeriesUID;
				}

				int sliceIndex = 2;
				if (acquisitionOrientation_ == QUANTX::XZPLANE )
					sliceIndex = 1;
				else if (acquisitionOrientation_ == QUANTX::YZPLANE )
					sliceIndex = 0;

				sqlQuery->clear();
				sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
				             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
				sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
				sqlFuture.waitForFinished();
				bool ok = sqlQuery->first();

				if(!ok && dynMultiType == 2){
					auto uidvalstr = uidPrefix.split('.', QString::SkipEmptyParts).last();
					auto uidval = uidvalstr.toULongLong();
					uidPrefix.replace(uidvalstr, QString::number(++uidval));
					uidRecord.setValue("series_uid",uidPrefix + QString::number(timePt + uidStart));
					sqlQuery->clear();
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					sqlQuery->first();
				}
				auto queryValue = sqlQuery->value(index_imagePosition).toString();
				auto splitValues = queryValue.split("\\");
				if (splitValues.size() < 3 || locations_.indexOf(splitValues.at(sliceIndex).toFloat()) == -1){// Checks to make sure we have enough dimensions' worth of split string values here, if so then also checks to see if the image is actually contained in the locations_ QVector.
					skipImageOffset++;//If the image isn't in locations_ then we need to add 1 to the offset and skip over it since it shouldn't be included
					if (!associatedSeries_.isEmpty())
						associatedSeries_.removeLast();
					continue;//We need to use a "continue" here since there could still be valid images to include
				}

				do{
					OPENPROGRESSUPDATE(querySize + queryProgress);
					//fileOpenProgress->setValue(fileOpenProgress->value() + 1);

					bool skipImageType = false;
					for(const auto & imgTypeSkip : std::as_const(imageTypesToSkip))
						if(sqlQuery->value(index_imageType).toString().contains(imgTypeSkip))
							skipImageType = true;
					if(skipImageType)
						continue;

					sumWidths += sqlQuery->value(index_windowWidth).toDouble();
					sumCenters += sqlQuery->value(index_windowCenter).toDouble();
					curmax = static_cast<quint16>(sqlQuery->value(index_maxPixval).toUInt());
					curmin = static_cast<quint16>(sqlQuery->value(index_minPixval).toUInt());
					imgz = locations_.indexOf(sqlQuery->value(index_imagePosition).toString().split("\\").at(sliceIndex).toFloat());

					auto instanceNum = sqlQuery->value(index_instanceNum).toString();
					instanceNums_[timePt].insert(imgz, instanceNum); // imgz is acquisition orientation component of image position
					if(imgz == 0)
						timestamps_[timePt-skipImageOffset] = sqlQuery->value(index_time).toTime();//Need to subtract the offset here as well incase we skipped any invalid images
					//timestamps_[timePt] = QTime(0,timePt);
#if useDebug
					if(imgz == 0)
						qDebug() << "Timestamp" << timePt - skipImageOffset << "found, time is" << timestamps_[timePt - skipImageOffset];
#endif
					if(timePt-skipImageOffset == 0){//Subtracting the offset from here as well incase the very first image is invalid, in which case this code block would never run
						positionString = sqlQuery->value(index_imagePosition).toString().split('\\');
						if(positionString.size() == 3)
							positions_[imgz] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat());
						else
							positions_[imgz] = QVector3D(0.0,0.0,0.0);
					}

					if(curmax > maxPixval_)
						maxPixval_ = curmax;
					if(curmin < minPixval_)
						minPixval_ = curmin;

					//if(dbSettings.at(0) == QString("QPSQL"))
					//    imageData = sqlQuery->value(index_image_data).toByteArray();
					//else
					imageData = qUncompress(sqlQuery->value(index_image_data).toByteArray());
					imgptr = reinterpret_cast<quint16 *>(imageData.data());
					if(imageData.size() == (imagenx * imageny * 2))
						for(imgy=0; imgy<imageny; imgy++){
							for(imgx=0; imgx<imagenx; imgx++){
								int x = (dx_ >= 0.0f) ? *patx : nx_ - *patx - 1;
								int y = (dy_ >= 0.0f) ? *paty : ny_ - *paty - 1;
								int z = (dz_ >= 0.0f) ? *patz : nz_ - *patz - 1;

								// The use of the offset here is to prevent the indeces from skipping values due to the skipping of invalid images
								dataptr = data_[timePt-skipImageOffset][z][y].data(); //flip y data
								dataptr[x] = imgptr[(imagenx * imgy) + imgx];
							}
						}
					queryProgress++;
				}while(sqlQuery->next());
			}

			// We need to remove invalid instanceNums_ maps as well, since there could be series in the middle that we skipped and had no way to know
			// ahead of time were invalid, but we want to leave the last one if we ONLY bailed due to nonContiguousDynMulti_
			if (dynMultiType == 1 && instanceNumsIndicesToRemove.size() > 0) {
				for (const auto& indexToRemove : instanceNumsIndicesToRemove) {
					if (instanceNums_.at(indexToRemove).isEmpty()) {
						instanceNums_.remove(indexToRemove);
						timestamps_.removeLast();
					}
				}
			}
			nt_ = nt_ - static_cast<size_t>(skipImageOffset); // We need to subtract the number of images we skipped from nt_ since it originally contained all the images we iterated through, including the ones we had to skip
		}
		else if(dynMultiType == 3){
			progressMaximum = querySize * (nt_ + 1);
			emit openProgressNewMaximum(progressMaximum);
			//There are two series
			bool firstBreast = true;

			for(int nSeries = 0; nSeries < nSeriesGE; nSeries++){
				if(nSeries > 0){
					// Prep second series for data load
					// This should have been done previously
					//seriesDescRecord.setValue("series_desc",seriesDescPost);
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
					             + QString(" ORDER BY CAST(series_number AS INT)");

					// add condition to where clause so we only retrieve motion-corrected or non-motion-corrected series based on what the initial series is
					if (seriesUID_.endsWith(".mc")){
						sqlStatement.replace("\"series_desc\" = ", "\"series_uid\" LIKE '%.mc' AND \"qi_series\".\"series_desc\" = ");
					} else {
						sqlStatement.replace("\"series_desc\" = ", "\"series_uid\" NOT LIKE '%.mc' AND \"qi_series\".\"series_desc\" = ");
					}
					sqlStatement.replace("series_desc\" = ", "series_desc\" LIKE ");

					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					sqlQuery->first();
					for(int i = 1; i < nSeries; ++i){
						sqlQuery->next();
						if(splitBreasts)
							sqlQuery->next();
					}
					if(!firstBreast)
						sqlQuery->next();
					uidRecord.setValue("series_uid",sqlQuery->value("series_uid").toString());
					if (nSeries > 0)
						associatedSeries_ << sqlQuery->value("series_uid").toString();
					sqlQuery->clear();
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					sqlQuery->first();
				}
				else if(!firstBreast){
					QString seriesDescPost = seriesDescRecord.value("series_desc").toString();
					seriesDescRecord.setValue("series_desc", seriesDesc_);
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
					             + QString(" ORDER BY CAST(series_number AS INT)");
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					sqlQuery->first();
					sqlQuery->next(); // Now we can get the second series UID
					seriesDescRecord.setValue("series_desc", seriesDescPost); // Reset the seriesDescRecord
					uidRecord.setValue("series_uid",sqlQuery->value("series_uid").toString());
					sqlQuery->clear();
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlImageTable,imageRecord,false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlImageTable,uidRecord,false);
					sqlFuture = QtConcurrent::run(sqlQuery.data(),&QSqlQuery::exec,sqlStatement);
					sqlFuture.waitForFinished();
					sqlQuery->first();
				}
				// Now, load the data as normal
				int sliceIndex = 2;
				if (acquisitionOrientation_ == QUANTX::XZPLANE )
					sliceIndex = 1;
				else if (acquisitionOrientation_ == QUANTX::YZPLANE )
					sliceIndex = 0;
				do{
					positionString = sqlQuery->value(index_imagePosition).toString().split('\\');
					if(positionString.size() < 3){
#if useDebug
						qDebug() << "Position string is empty!!! Skipping image";
#endif
						continue;
					}
					OPENPROGRESSUPDATE(querySize+queryProgress);

					sumWidths += sqlQuery->value(index_windowWidth).toDouble();
					sumCenters += sqlQuery->value(index_windowCenter).toDouble();
					curmax = static_cast<quint16>(sqlQuery->value(index_maxPixval).toUInt());
					curmin = static_cast<quint16>(sqlQuery->value(index_minPixval).toUInt());
					//imgz = locations_.indexOf(sqlQuery->value(index_sliceLocation).toFloat());
					imgz = locations_.indexOf(sqlQuery->value(index_imagePosition).toString().split("\\").at(sliceIndex).toFloat());
					if(imgz == -1){
						auto raw_imgz = sqlQuery->value(index_imagePosition).toString().split("\\").at(sliceIndex).toFloat();
						for(int z = 0; z < locations_.size(); ++z)
							if(qFuzzyCompare(locations_.at(z), raw_imgz)){
								imgz = z;
								break;
							}

						if(imgz == -1) // Still haven't found it yet, let's increase fuzziness to 1 micrometer
							for(int z = 0; z < locations_.size(); ++z)
								if(qAbs(locations_.at(z) - raw_imgz) < 0.001f){
									imgz = z;
									break;
								}
#if useDebug
						qDebug() << "Raw Image Z Location" << raw_imgz;
						if(imgz >= 0)
							qDebug() << "Found Z location" << locations_.at(imgz) << "at index" << imgz;
						else
							qDebug() << "Z location not found";
#endif
					}

					int timePt = sqlQuery->value(index_temporalPosID).toInt();
					if(timePt < nSeries){
						if(nSeries == 1 && sqlQuery->record().value("instance_num").toInt() > imagenz)
							timePt = 1 + ((sqlQuery->record().value("instance_num").toInt() - 1) / imagenz);
						else
							timePt = nSeries;
					}
					auto instanceNum = sqlQuery->value(index_instanceNum).toString();
					//float location = sqlQuery->value(index_sliceLocation).toFloat();
					instanceNums_[timePt].insert(imgz, instanceNum);
					if(imgz == 0){
						// Not sure if isFastDynamic_ is even required here, as this change only affect multi-series dynamic scans
						timestamps_[timePt] = QTime(0, ( (timePt > 0) && (nSeries > 0) && (!isFastDynamic_) ) ? 1 : 0 ).addMSecs(sqlQuery->value("trigger_time").toInt());
						if(timePt > 1 && timestamps_[timePt] == QTime(0,1))
							timestamps_[timePt] = QTime(0, timePt * 2 - 1);
					}
#if useDebug
					if(imgz == 0)
						qDebug() << "Timestamp" << timePt << "found, time is" << timestamps_[timePt];
#endif
#if useDebug
					/*
					qDebug() << "Position String: " << positionString;
					qDebug() << "imgz: " << imgz;
					qDebug() << "Slice Location: " << sqlQuery->value(index_sliceLocation).toFloat();
					*/
#endif
					if(nSeries == 0 && qRound(orientation_[2]) == 0 && qRound(orientation_[5]) == 0){
						//Third dimension is Z
						if(positionString.size() == 3)
							positions_[imgz] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat());
						else
							positions_[imgz] = QVector3D(0.0,0.0,0.0);
					}
					else{
						auto px = positionString.at(0).toFloat();
						auto py = positionString.at(1).toFloat();
						auto pz = positionString.at(2).toFloat();
						//Third dimension is X or Y
						if( queryProgress == 0  || (px < positions_.first().x() && qRound(orientation_[0]) == 0 && qRound(orientation_[3]) == 0) ){
							//if(dx_ < 0.0)
							//    px += nx_ * dx_;
							//if(dy_ < 0.0)
							//    py += ny_ * dy_;
							//if(dz_ > 0.0)
							    for(int z=0; z<nz_; ++z)
									positions_[z] = QVector3D(px, py, pz + (z * dz_));
								//else
								//    for(int z=0; z<nz_; ++z)
								//        positions_[z] = QVector3D(px, py, pz + ((nz_ - z - 1) * dz_));
						}
						if( queryProgress == 0  || (py < positions_.first().y() && qRound(orientation_[1]) == 0 && qRound(orientation_[4]) == 0) ){
							//if(dx_ < 0.0)
							//    px += nx_ * dx_;
							//if(dy_ < 0.0)
							//    py += ny_ * dy_;
							//if(dz_ > 0.0)
							    for(int z=0; z<nz_; ++z)
									positions_[z] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat() + (z * dz_));
								//else
								//    for(int z=0; z<nz_; ++z)
								//        positions_[z] = QVector3D(positionString.at(0).toDouble(),positionString.at(1).toDouble(),positionString.at(2).toDouble() + ((nz_ - z - 1) * dz_));
						}
					}

					if(curmax > maxPixval_)
						maxPixval_ = curmax;
					if(curmin < minPixval_)
						minPixval_ = curmin;

					imageData = qUncompress(sqlQuery->value(index_image_data).toByteArray());
					imgptr = reinterpret_cast<quint16 *>(imageData.data());
					if(imageData.size() == (imagenx * imageny * 2))
						for(imgy = 0; imgy < imageny; imgy++){
							for(imgx = 0; imgx < imagenx; imgx++){
								int x = (dx_ >= 0.0f) ? *patx : nx_ - *patx - 1;
								int y = (dy_ >= 0.0f) ? *paty : ny_ - *paty - 1;
								int z = (dz_ >= 0.0f) ? *patz : nz_ - *patz - 1;
								dataptr = data_[timePt][z][y].data();
								dataptr[x] = imgptr[(imagenx * imgy) + imgx];
							}
						}
					queryProgress++;
				}while(sqlQuery->next());
				if(nSeries == 1 && nt_ > 2 && (timestamps_[2] < timestamps_[1]))
					nSeriesGE = nt_;
				if(splitBreasts && firstBreast)
					nSeries -= 1;
				if(splitBreasts)
					firstBreast = !firstBreast;
			}
		}
	}
	else{
		QScopedPointer<QSqlQuery> sqlQueryR;
		bool firstBreast = true;
		if(splitBreasts){
			sqlQueryR.reset(new QSqlQuery(db));
			seriesDescRecord.setValue("series_desc",seriesDesc_);
			seriesDescRecord.setValue("study_uid",seriesRecord.value("study_uid"));
			for(int i = seriesDescRecord.count() - 1; i >= 0; i--)
				if(seriesDescRecord.isNull(i))
					seriesDescRecord.remove(i);
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
			             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
			             + QString(" ORDER BY CAST(series_number AS INT)");
			sqlFuture = QtConcurrent::run(sqlQueryR.data(),&QSqlQuery::exec,sqlStatement);
			sqlFuture.waitForFinished();
			sqlQueryR->first();
			int seriesNum = sqlQueryR->record().value("series_number").toInt();
			bool found = false;
			if( (sqlQueryR->record().value("series_uid").toString() == seriesUID_) && (sqlQueryR->next()) ){
				if(sqlQueryR->record().value("series_number").toInt() == seriesNum + 1)
					found = true;
			}
#if useDebug
			qDebug() << "Right breast has same series description?" << found;
#endif
			if(!found){
				QString rightSeriesDesc = seriesDesc_.trimmed().replace("left", "right");
				if(rightSeriesDesc.size() % 2)
					rightSeriesDesc += ' ';
				seriesDescRecord.setValue("series_desc", rightSeriesDesc);
				sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesRecord,false) + QString(' ')
				             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesDescRecord,false)
				             + QString(" ORDER BY CAST(series_number AS INT)");
#if useDebug
				    qDebug() << "sqlStatement:" << sqlStatement;
#endif
					sqlFuture = QtConcurrent::run(sqlQueryR.data(), &QSqlQuery::exec,sqlStatement);
				sqlFuture.waitForFinished();
				sqlQueryR->first();
			}
		}

#if useDebug
		    qDebug() << "Standard MR Load Sequence, querySize:" << querySize;
#endif
			int sliceIndex = 2;
			if (acquisitionOrientation_ == QUANTX::XZPLANE )
				sliceIndex = 1;
			else if (acquisitionOrientation_ == QUANTX::YZPLANE )
				sliceIndex = 0;

			do{
				OPENPROGRESSUPDATE(querySize+queryProgress);
			//fileOpenProgress->setValue(fileOpenProgress->value() + 1);

			bool skipImageType = false;
			for(const auto & imgTypeSkip : std::as_const(imageTypesToSkip))
				if(sqlQuery->value(index_imageType).toString().contains(imgTypeSkip))
					skipImageType = true;
			if(skipImageType)
				continue;

			sumWidths += sqlQuery->value(index_windowWidth).toDouble();
			sumCenters += sqlQuery->value(index_windowCenter).toDouble();
			curmax = static_cast<quint16>(sqlQuery->value(index_maxPixval).toUInt());
			curmin = static_cast<quint16>(sqlQuery->value(index_minPixval).toUInt());
			//imgz = locations_.indexOf(sqlQuery->value(index_sliceLocation).toFloat());
			imgz = locations_.indexOf(sqlQuery->value(index_imagePosition).toString().split("\\").at(sliceIndex).toFloat());
#if useDebug
			// This was useful, but is WAY too noisy
			//qDebug() << "current location =" << sqlQuery->value(index_imagePosition).toString().split("\\").at(sliceIndex).toFloat();
			if(imgz == 0) {
				qDebug() << "Temporal Position ID from SQL Query:" << sqlQuery->value(index_temporalPosID).toInt();
			}
#endif

			int timePt;
			if (nb_ > 1) {
				timePt = bValues_.indexOf(sqlQuery->value(index_diffusionBValue).toDouble());
			}
			else {
				timePt = sqlQuery->value(index_temporalPosID).toInt() - 1;
			}

			if(timePt < 0) {
				timePt = (sqlQuery->value(index_instanceNum).toInt() - 1) / locations_.size();
			}
			if(timePt < 0) {
				timePt = 0;
			}
#if useDebug
			if(imgz == 0)
				qDebug() << "timePt:" << timePt + 1 << ", timestamps:" << timestamps_.size() << ", b-values:" << nb_ << ", instance number:" << sqlQuery->value(index_instanceNum).toInt() ;
#endif
			while( timePt >= nt_  && nb_ <= 1){ // ((splitBreasts ? nSeries / 2 : nSeries) + previousExtraTimePoints + currentExtraTimePoints) ){
#if useDebug
				qDebug() << "Elapsed Time:" << whole.elapsed();
				qDebug() << "Found another timepoint in series";
				qDebug() << "Expanding from" << nt_ << "to" << nt_ + 1 << "timepoints";
#endif
				// Fix arrays for the extra size
				data_.push_back(std::vector<std::vector<std::vector<quint16> > >(nz_));
				for(int z = 0; z < nz_; z++){
					data_[nt_].at(z) = (std::vector<std::vector<quint16> >(ny_));
					for(int y = 0; y < ny_; y++){
						data_[nt_][z].at(y) = (std::vector<quint16>(nx_));
					}
				}
				instanceNums_.append(QMap<float, QString>{});
				timestamps_.append(QTime());
				//++currentExtraTimePoints;
				++nt_;
			}
			if(timePt >= (timestamps_.size() * nb_))
				throw(std::runtime_error("Timepoint found greater than number of timestamps"));

			if((imgz == 0) && (timePt < timestamps_.size())){
				timestamps_[timePt] = sqlQuery->value(index_time).toTime();
#if useDebug
				qDebug() << "Timestamp:" << timestamps_[timePt];
#endif
			}

			instanceNums_[timePt].insert(imgz, sqlQuery->value(index_instanceNum).toString());

			if(timePt == 0){
				positionString = sqlQuery->value(index_imagePosition).toString().split('\\');
				//Find third dimension
				if(qRound(orientation_[2]) == 0 && qRound(orientation_[5]) == 0){
					//Third dimension is Z
					if(positionString.size() == 3)
						positions_[imgz] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat());
					else
						positions_[imgz] = QVector3D(0.0,0.0,0.0);
				}
				else{
					float px = positionString.at(0).toFloat();
					float py = positionString.at(1).toFloat();
					float pz = positionString.at(2).toFloat();
					//Third dimension is X or Y
					if((px < positions_.first().x() || positions_.first() == QVector3D(0, 0, 0)) && qRound(orientation_[0]) == 0 && qRound(orientation_[3]) == 0){
						//if(dx_ < 0.0)
						//    px += nx_ * dx_;
						//if(dy_ < 0.0)
						//    py += ny_ * dy_;
						//if(dz_ > 0.0)
						    for(int z=0; z<nz_; ++z)
								positions_[z] = QVector3D(px, py, pz + (z * dz_));
							//else
							//    for(int z=0; z<nz_; ++z)
							//        positions_[z] = QVector3D(px, py, pz + ((nz_ - z - 1) * dz_));
					}
					if(py < positions_.first().y() && qRound(orientation_[1]) == 0 && qRound(orientation_[4]) == 0){
						//if(dx_ < 0.0)
						//    px += nx_ * dx_;
						//if(dy_ < 0.0)
						//    py += ny_ * dy_;
						//if(dz_ > 0.0)
						    for(int z=0; z<nz_; ++z)
								positions_[z] = QVector3D(positionString.at(0).toFloat(),positionString.at(1).toFloat(),positionString.at(2).toFloat() + (z * dz_));
							//else
							//    for(int z=0; z<nz_; ++z)
							//        positions_[z] = QVector3D(positionString.at(0).toDouble(),positionString.at(1).toDouble(),positionString.at(2).toDouble() + ((nz_ - z - 1) * dz_));
					}
				}
			}

			if(curmax > maxPixval_)
				maxPixval_ = curmax;
			if(curmin < minPixval_)
				minPixval_ = curmin;

			//if(dbSettings.at(0) == QString("QPSQL"))
			//    imageData = sqlQuery->value(index_image_data).toByteArray();
			//else
			imageData = qUncompress(sqlQuery->value(index_image_data).toByteArray());
			imgptr = reinterpret_cast<quint16 *>(imageData.data());
			if(imageData.size() == (imagenx * imageny * 2))
				for(imgy=0;imgy<imageny;imgy++){
					for(imgx=0;imgx<imagenx;imgx++){
						int x = (dx_ >= 0.0f) ? *patx : nx_ - *patx - 1;
						int y = (dy_ >= 0.0f) ? *paty : ny_ - *paty - 1;
						int z = (dz_ >= 0.0f) ? *patz : nz_ - *patz - 1;
						dataptr = data_[timePt][z][y].data();
						dataptr[x] = imgptr[(imagenx * imgy) + imgx];
					}
				}
			queryProgress++;
			if(splitBreasts && firstBreast){
				if(sqlQuery->next())
					sqlQuery->previous();
				else{
#if useDebug
					qDebug() << "Finished left breast, moving to right breast with Series UID" << sqlQueryR->record().value("series_uid");
#endif
					uidRecord.setValue("series_uid", sqlQueryR->record().value("series_uid"));
					sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement, sqlImageTable, imageRecord, false) + QString(' ')
					             + db.driver()->sqlStatement(QSqlDriver::WhereStatement,  sqlImageTable, uidRecord,   false);
					sqlFuture = QtConcurrent::run(sqlQuery.data(), &QSqlQuery::exec, sqlStatement);
					sqlFuture.waitForFinished();
					firstBreast = false;
				}
			}
			}while(sqlQuery->next());
	}

	// Fix for negative z-dimension
	if(acquisitionOrientation_ == QUANTX::XYPLANE)
		if(positions_[0].z() > positions_[1].z()){
			for(int i = 0; i < ((nz_) / 2); ++i){
				std::swap(positions_[i], positions_[nz_ - 1 - i]);
				for(int t = 0; t < nt_; ++t)
					data_[t][i].swap(data_[t][nz_ - 1 - i]);
#if useDebug
				qDebug() << "Swapping" << i << "," << nz_ - 1 - i;
#endif
			}
		}

	// Fix for positions not changing
	if(positions_.size() > 1 && positions_[0] == positions_[1]){
		positions_ = imagePositions_; // use imagePositions and retry
		if(positions_.size() > 1 && positions_[0] == positions_[1]){
			if(acquisitionOrientation_ == QUANTX::XYPLANE)
				for(int i = 1; i < positions_.size(); i++)
					positions_[i] = positions_[0] + QVector3D(0, 0, i * dz_);
		}
	}

	//fileOpenProgress->setValue(fileOpenProgress->maximum());
	emit openProgressUpdated(progressMaximum);

	windowWidth_ = sumWidths / (imagenz * dim4Size);
	windowLevel_ = sumCenters / (imagenz * dim4Size);
	if (nt_ > 1 ){
		if (hasDuplicateTimestamps()){
			correctDuplicateTimestamps(dbSettings);
		}
		if(isFastDynamic_){
			injectionTime_ = timestamps_[0];
#if useDebug
			qDebug() << "timestamps:" << timestamps_;
			qDebug() << "injectionTime:" << injectionTime_;
			qDebug() << "firstPostIDX()" << firstPostIdx();
			qDebug() << "getTimestamps()" << getTimestamps();
#endif
		}
		else
			injectionTime_ = timestamps_[1].addSecs( -60 ); // initially set injection time so it's 60 secs before the first post-contrast
	}
	origin_ = findOrigin();
	dx_ = qAbs(dx_);
	dy_ = qAbs(dy_);
	dz_ = qAbs(dz_);
#if useDebug
	qDebug()<<"Closing DB QiMRImage";
#endif

	    sqlQuery.reset();
		db.close();
	// check if there are any missing slices and add any missing slices to instanceNums_. This should rarely if ever happen (occurred for one test case)
	if (nt_ > 1){
		int numSlices = nz_;
		if (acquisitionOrientation_ == QUANTX::XZPLANE)
			numSlices = ny_;
		if (acquisitionOrientation_ == QUANTX::YZPLANE)
			numSlices = nx_;
		for (auto t=0;t < nt_;t++){
			if (instanceNums_.at(t).count() != numSlices){
#if useDebug
				qDebug().noquote() << QString("Missing slices at time %1: %2").arg(t).arg(numSlices - instanceNums_.at(t).count());
#endif
				QList<float> completeLocations;
				for (int t1 = 0; t1 < nt_; t1++){
					if (instanceNums_.at(t1).count() == numSlices){
						completeLocations = instanceNums_.at(t1).keys();
						break;
					}
				}
				if (completeLocations.count() == numSlices){// make sure at least one timepoint had all the slices
					for (auto key : std::as_const(completeLocations)){
						if (instanceNums_.at(t).value(key).isEmpty()){
							instanceNums_[t].insert(key, QString());
						}
					}
				}
			}
		}
	}

	// populate seriesNums_ if there are associated series
	if (!associatedSeries_.isEmpty()){
		associatedSeries_.removeDuplicates();
		associatedSeries_.removeOne(seriesUID_);
		seriesNums_.clear();
		auto seriesInDCE = associatedSeries_;
		seriesInDCE.prepend(seriesUID_);
		for (const auto &info : studySeriesInfo){
			for (const auto &series : seriesInDCE){
				if(info.seriesUID == series){
					seriesNums_ << QString::number(info.seriesNumber);
					break;
				}
			}
		}
	}
	// for the case when all the post timepoints are in a single series
	if (seriesNums_.count() == 2){
		for (int i = 0; i < nt_ - 2; i++){
			seriesNums_ << seriesNums_.last();
		}
	}

#if useDebug
	qDebug() << "ImagePosition 0:" << positions_[0] << ", Location:" << locations_[0];
	qDebug() << "ImagePosition 1:" << positions_[1] << ", Location:" << locations_[1];
	qDebug() << QString("ImagePosition %1:").arg(positions_.size() - 1).toStdString().c_str() << positions_.last() << ", Location:" << locations_.last();
	qDebug() << "Orientatin: " << orientation_;
	qDebug() << "Origin: " << origin_;
	qDebug() << "Query Time: " << queryTime;
	qDebug() << "Loop Time : " << loop.elapsed();
	qDebug() << "Whole Time: " << whole.elapsed();
#endif
#ifdef QI_INTERNAL_TIMING_OUTPUT
	emit finishedMRFillBuffer();
#endif
	return 1; //success!

}

/*!
 * \brief Finds the origin of the image.
 * \return The image origin.
 */
QVector3D QiMriImage::findOrigin()
{
	auto o = positions().first();
	if (dx_ < 0 && (acquisitionOrientation_ != QUANTX::YZPLANE) )
		o.setX( o.x() + (nx_ - 1) * dx_ );
	if (dy_ < 0 && (acquisitionOrientation_ != QUANTX::XZPLANE) )
		o.setY( o.y() + (ny_ - 1) * dy_ );
	if (dz_ < 0 && (acquisitionOrientation_ != QUANTX::XYPLANE) )
		o.setZ( o.z() + (nz_ - 1) * dz_ );
	return o;
}

/*!
 * \brief Update the stored image data in the DB with new warped data.
 * \param dbSettings DB settings to connect with.
 * \param textEdit TextEdit to update with string info on the new warped data being generated.
 * \param replace Whether to overwrite the old record, or just add to it.
 */
void QiMriImage::updateDBWithWarpedData(const QStringList & dbSettings, QTextEdit * textEdit, bool replace)
{
	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	QSqlDatabase db = qisql->createDatabase("QIopenDatabase");
#if useDebug
	qDebug()<<"Opening DB QiMRImage";
#endif
	bool ok = db.open();  //if ok is false, we should error and quit
	if(!ok){
		int retries = 0;
		while( (!ok) && (retries++ < 5) ){
			QThread::sleep(1); // Wait one second, and retry
		}
		qWarning("Database Error!\nThe specified database could not be opened.\nCheck your database settings and try again.");
	}
#if useDebug
	QElapsedTimer databaseTime, loopTime;
	databaseTime.start();
#endif

	QString sqlStatement;
	QScopedPointer<QSqlQuery> sqlQuery(new QSqlQuery(db));
	QString sqlImageTable = qisql->imageTable(QUANTX::MRI);
	QString sqlSeriesTable = qisql->seriesTable();
	QSqlRecord imageUpdateRecord = db.record(sqlImageTable);
	QSqlRecord imageWhereRecord = db.record(sqlImageTable);
	QSqlField imageUpdateImageDataField = imageUpdateRecord.field("image_data");
	QSqlField imageUpdateVersionField = imageUpdateRecord.field("version");
	QMap<uint, QSqlRecord> imageInstanceToRecordMap;

	QStringList series = associatedSeries_;
	series.prepend(seriesUID());
	series = sortSeriesBySeriesNum(dbSettings, series);
	auto seriesDescriptions = getSeriesDescriptions(dbSettings, series);
	int totalRecordCount = 0;
	for (int seriesIdx = 0; seriesIdx < series.count(); seriesIdx++){

		//update series description with 'Motion Corr.' prepended
		QSqlRecord seriesRecord  = db.record(sqlSeriesTable);
		QSqlRecord seriesDescRecord = seriesRecord; // data being updated
		QSqlRecord seriesUIDRecord = seriesRecord; // 'where' clause

		seriesUIDRecord.setValue("series_uid", series.at(seriesIdx));
		for(int i = seriesUIDRecord.count() - 1; i >= 0; i--)
			if(seriesUIDRecord.isNull(i))
				seriesUIDRecord.remove(i);

		if(!replace){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement,sqlSeriesTable,seriesDescRecord,false) + QString(' ')
			               +  db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesUIDRecord,false);
			sqlQuery->prepare(sqlStatement);
			ok = sqlQuery->exec();
			sqlQuery->first();
			seriesDescRecord = sqlQuery->record();
			seriesDescRecord.remove(seriesDescRecord.indexOf("id"));
		}
		QString newSeriesDesc = QString("Motion Corr. ") + seriesDescriptions.at(seriesIdx);
		seriesDescRecord.setValue("series_desc", newSeriesDesc);
		seriesDescRecord.setValue("version", 0);
		if(replace){
			for(int i = seriesDescRecord.count() - 1; i >= 0; i--)
				if(seriesDescRecord.isNull(i))
					seriesDescRecord.remove(i);
		}

		if(replace){
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::UpdateStatement,sqlSeriesTable,seriesDescRecord,false) + QString(' ')
			               + db.driver()->sqlStatement(QSqlDriver::WhereStatement,sqlSeriesTable,seriesUIDRecord,false);
		} else {
			seriesDescRecord.setValue("series_uid", series.at(seriesIdx).trimmed() + ".mc");
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement, sqlSeriesTable, seriesDescRecord, false) + QString(' ');
		}
#if useDebug
		qDebug() << "Updating series Description for Motion corrected image";
		qDebug() << "SQL statment: " << sqlStatement;
		for(auto i = 0; i < instanceNums_.size(); ++i){
			qDebug() << "Instance Numbers for timepoint" << i << ":";
			qDebug() << instanceNums_.at(i);
		}
#endif
		sqlQuery->exec(sqlStatement);
		int newMCSeriesRowID;
		if (!replace) // get id for qi_series_id for inserting the images
			newMCSeriesRowID = sqlQuery->lastInsertId().toInt();

		imageInstanceToRecordMap.clear();
		imageWhereRecord.setValue("series_uid", series.at(seriesIdx));
		imageWhereRecord.setValue("instance_num", instanceNums_.at(seriesIdx).values().first());
		for(auto i = imageWhereRecord.count() - 1; i >= 0; i--)
			if(imageWhereRecord.isNull(i))
				imageWhereRecord.remove(i);

		if(replace){
			for(auto i = imageUpdateRecord.count() - 1; i >= 0; i--)
				if(imageUpdateRecord.fieldName(i) != "image_data")
					imageUpdateRecord.remove(i);
		}

		//auto imageWhereInstanceCol = imageWhereRecord.indexOf("instance_num");
		int recordCount=0;
		QString subSeriesDesc;
		if(!replace){
			QScopedPointer<QSqlQuery> imageUpdateQuery(new QSqlQuery(db));
			//QSqlRecord imageUIDRecord = imageWhereRecord;
			QSqlRecord imageUpdateSearchRecord = imageUpdateRecord;
			imageWhereRecord.remove(imageWhereRecord.indexOf("instance_num"));
			imageUpdateSearchRecord.remove(imageUpdateSearchRecord.indexOf("image_data"));
			imageUpdateSearchRecord.remove(imageUpdateRecord.indexOf("id"));
			sqlStatement = db.driver()->sqlStatement(QSqlDriver::SelectStatement, sqlImageTable, imageUpdateSearchRecord, false) + QString(' ')
			               + db.driver()->sqlStatement(QSqlDriver::WhereStatement, sqlImageTable, imageWhereRecord, false);
			imageUpdateQuery->prepare(sqlStatement);
			ok = imageUpdateQuery->exec();
			recordCount = imageUpdateQuery->size();
#if useDebug
			qDebug() << "Series: " <<seriesIdx << " Record count for current series " << recordCount;
#endif
			//auto imageUpdateInstanceCol = imageUpdateSearchRecord.indexOf("instance_num");
			imageUpdateQuery->first();

			do{
				imageInstanceToRecordMap[imageUpdateQuery->value("instance_num").toString().toUInt()] = imageUpdateQuery->record();
			}while(imageUpdateQuery->next());
#if useDebug
			qDebug() << "imageInstanceToRecordMap size:" << imageInstanceToRecordMap.count();
#endif
		}
		int sliceWidth = nx_;
		int sliceHeight = ny_;
		int numSlices = nz_;
		bool flipH = false;
		bool flipV = false;
		if (acquisitionOrientation_ == QUANTX::XYPLANE){
			flipH = (qRound(orientation_.at(0)) == -1);
			flipV = (qRound(orientation_.at(4)) == -1);
		} else if (acquisitionOrientation_ == QUANTX::YZPLANE){
			numSlices = nx_;
			sliceWidth = ny_;
			sliceHeight = nz_;
			flipH = (qRound(orientation_.at(1)) == -1);
			flipV = (qRound(orientation_.at(5)) == -1);
		} else if (acquisitionOrientation_ == QUANTX::XZPLANE){
			numSlices = ny_;
			sliceWidth = nx_;
			sliceHeight = nz_;
			flipH = (qRound(orientation_.at(0)) == -1);
			flipV = (qRound(orientation_.at(5)) == -1);
		}
		int tStart = totalRecordCount / numSlices;
		int tEnd = tStart + qRound(static_cast<float>(recordCount) / numSlices); // rounding takes care of rare instance if there are a few missing slices
#if useDebug
		qDebug() << "Initialization Query Time: " << databaseTime.restart();
#endif

		for (auto t = tStart; t < tEnd; t++){
			if ( (!replace) || (t != 1) ){ // first post-contrast timepoint doesn't get motion corrected so we don't have to update it in the DB
				for (auto slice = 0; slice < numSlices; slice++){
#if useDebug
					loopTime.start();
#endif
					auto bytesPerPixel = 2;
					QByteArray imageData(sliceWidth * sliceHeight * bytesPerPixel, static_cast<char>(0));
					QDataStream stream(&imageData, QIODevice::WriteOnly);
					if (acquisitionOrientation_ == QUANTX::XYPLANE){
						for( auto y = 0; y < ny_; y++ ){
							for (auto x = 0; x < nx_; x++){
								auto w = flipH ? nx_ - x -1 : x;
								auto h = flipV ? ny_ - y -1 : y;
								stream.writeRawData(reinterpret_cast<const char *>(data_.at(t).at(slice).at(h).data()) + w * 2, 2);
							}
						}
					} else if (acquisitionOrientation_ == QUANTX::YZPLANE) {
						for (auto z = 0; z < nz_; z++){
							for (auto y = 0; y < ny_; y++){
								auto w = flipH ? ny_ - y -1 : y;
								auto h = flipV ? nz_ - z -1 : z;
								stream.writeRawData(reinterpret_cast<const char *>(data_.at(t).at(h).at(w).data()) + slice * 2, 2);
							}
						}
					} else if (acquisitionOrientation_ == QUANTX::XZPLANE) {
						for (auto z = 0; z < nz_; z++){
							for (auto x = 0; x < nx_; x++){
								auto w = flipH ? nx_ - x -1 : x;
								auto h = flipV ? nz_ - z -1 : z;
								stream.writeRawData(reinterpret_cast<const char *>(data_.at(t).at(h).at(slice).data()) + w * 2, 2);
							}
						}
					}
					//#if useDebug
					//					qDebug() << "Create Data Array Time: " << databaseTime.restart();
					//#endif

					auto currentInstanceNum = instanceNums_.at(t).values().at(slice);
					if (currentInstanceNum.isEmpty()){
#if useDebug
						qDebug() << "There was a missing slice, Skipping slice:" << slice;
#endif
						continue;
					}
					if(!replace){
#if useDebug
						qDebug() << "Taking imageInstanceRecord:" << currentInstanceNum.toUInt();
						qDebug() << imageInstanceToRecordMap.value(currentInstanceNum.toUInt());
#endif
						imageUpdateRecord = imageInstanceToRecordMap.take(currentInstanceNum.toUInt());
						imageUpdateRecord.append(imageUpdateImageDataField);
					}
					imageUpdateRecord.setValue("image_data", qCompress(imageData));

#if useDebug
					qDebug() << "Create Database Record Time: " << databaseTime.restart();
#endif

					if(replace){
						imageWhereRecord.setValue("instance_num", currentInstanceNum);
						sqlStatement = db.driver()->sqlStatement(QSqlDriver::UpdateStatement, sqlImageTable, imageUpdateRecord, true) + QString(' ')
						               + db.driver()->sqlStatement(QSqlDriver::WhereStatement, sqlImageTable, imageWhereRecord, false);
					} else {
						imageUpdateRecord.setValue("series_uid", series.at(seriesIdx).trimmed() + ".mc");
						imageUpdateRecord.setValue("image_uid", imageUpdateRecord.value("image_uid").toString().trimmed() + ".mc");
						imageUpdateRecord.setValue("qi_series_id", newMCSeriesRowID);
						sqlStatement = db.driver()->sqlStatement(QSqlDriver::InsertStatement, sqlImageTable, imageUpdateRecord, true);
					}
#if useDebug
					qDebug() << "Updating image data for Motion corrected image" << imageUpdateRecord.value("image_uid").toString();
					qDebug() << "Replace:" << replace;
					qDebug() << "Instance Number" << currentInstanceNum << ", timepoint" << t << ", slice" << slice;
					qDebug() << "\tSQL statment: " << sqlStatement;
					qDebug() << "\tImage Location from QMap key: " << instanceNums_.at(t).keys().at(slice);
#endif
					sqlQuery->prepare(sqlStatement);
					for(int i=0;i<imageUpdateRecord.count();i++){
						sqlQuery->bindValue(i,imageUpdateRecord.value(i));
					}
					ok = sqlQuery->exec();
					if(textEdit != nullptr) {
						QMetaObject::invokeMethod(textEdit, [textEdit, t, slice](){textEdit->append(QString("Updating DB for Timepoint: %1 Slice: %2").arg( t+1 ).arg( slice+1 ));}, Qt::QueuedConnection);
					}
					qApp->processEvents();

#if useDebug
					if(!ok){
						qDebug() << "Image Update Not OK";
						qDebug() << sqlQuery->lastError().text();
					}
					else
						qDebug() << "Image Update OK";

					qDebug() << "Insert Database Record Time: " << databaseTime.restart();
					qDebug() << "Total Time Required For Slice: " << loopTime.elapsed();
#endif
				}
			}
		}

		totalRecordCount += recordCount;
	}
}

/*!
 * \brief Getter for nt() style collection of timestamps for the image.
 * \return One precontrast timestamp, followed by all post-contrast timestamps.
 */
QVector<QTime> QiMriImage::getTimestamps() const
{
	QVector<QTime> times;
	times << timestamps_.at(precontrastIdx());
	for (int t = firstPostIdx(); t < timestamps_.size(); t++)
		times << timestamps_.at(t);
	return times;
}

/*!
 * \brief Return vector timestamps of a DCEMR in decimal minutes since the first timepoint (i.e., first value is always 0.0)
 * \pre A DCE MR image has been loaded
 * \post The vector of timestamps in minutes since first time point is returned
 */
QVector<double> QiMriImage::timePointsMinutes() const
{
	auto times = getTimestamps();
	QVector<double> minutes(times.count());
	minutes[0] = 0.0;
	for (int t = 1; t < times.count(); t++)
		minutes[t] = injectionTime().secsTo(times.at(t))/ 60.0;
	return minutes;
}

/*!
 * \brief Return QMap with the timestamps of a DCEMR in decimal minutes since the first timepoint as the keys and the corresponding dicom series number as the values
 * \pre A DCE MR image has been loaded
 * \post The map of timestamps (minutes) and dicom series numbers is returned
 */
QMap<double, QString> QiMriImage::getTimeAndSeriesNums() const
{
	QMap<double, QString> timeandseries;
	auto timepoints = timePointsMinutes();
	for (int t = 0; t < timepoints.count(); t++){
		if (seriesNums_.count() == 1){
			timeandseries.insert(timepoints.at(t), seriesNums_.first());
		} else {
			if ( seriesNums_.count() > t)
				timeandseries.insert(timepoints.at(t), seriesNums_.at(t) );
			else
				timeandseries.insert(timepoints.at(t), seriesNums_.at(seriesNums_.count() - 1) + "+" ); // shouldn't get here but possible if we don't extract the series numbers correctly
		}
	}
	return timeandseries;
}

/*!
 * \brief Getter for all timestamps without regard for a standard single pre-contrast timepoint.
 * \return Collection of all timestamps.
 */
QVector<QTime> QiMriImage::getAllTimestamps() const
{
	return timestamps_;
}

/*!
 * \brief Getter for the number of timepoints post contrast, plus one precontrast timepoint, as most protocols will end up following this pattern.
 * \return If the pre-contrast index is < 0, then return nt_ otherwise, return nt_ - firstPostIdx() + 1;
 */
int QiMriImage::nt() const
{
	return (precontrastIdx_ < 0) ? nt_ : nt_ - firstPostIdx() + 1;
}

/*!
 * \brief Getter for the precontrast timepoint index.
 * \return Integer representing the index of the precontrast timepoint.
 */
int QiMriImage::precontrastIdx() const
{
	return qMax(precontrastIdx_, 0);
}

/*!
 * \brief Getter for first post contrast index.
 * \return Integer representing the index of the first post-contrast timepoint.
 */
int QiMriImage::firstPostIdx() const
{
	if(firstPostIdxFixed > 0)
		return firstPostIdxFixed;
	if(precontrastIdx_ < 0)
		return 1;
	//we'll set the first post-contrast image to be the first image after the injection time
	int firstPost = precontrastIdx_ + 1;
	for (int t = precontrastIdx_ + 1; t < timestamps_.size(); t++){
		if (timestamps_[t] > injectionTime_){
			firstPost = t;
			break;
		}
	}
	return firstPost;
}

/*!
 * \brief Getter for timepoint index, based on some heuristics and the passed argument.
 * \param index The input index argument.
 * \return Either the precontrastIdx() if the input value is less than or equal to zero, or the min of the firstPostIdx() + index - 1, or nt() - 1 (Total num timepoints).
 */
int QiMriImage::getIdx( int index ) const
{
	return index <= 0 ? precontrastIdx() : std::min( firstPostIdx() + index - 1, nt_ - 1 );
}

/*!
 * \brief Getter for the number of b-values in this image.
 * \return Number of b-values.
 */
int QiMriImage::getNb() const
{
	return nb_;
}

/*!
 * \brief Returns the collection of b-values in this image.
 * \return QVector<double> of all b-values.
 */
QVector<double> QiMriImage::getBValues() const
{
	return bValues_;
}

//!
//! \brief Sets the injection time from the inject to the first post-contrast time.
//! \param injectionToFirstPostTime The inject to the first post-contrast time in seconds.
//!
//! \preInitPost{The injection time is set.}
//!
void QiMriImage::setInjectionTimeFromInjectionToFirstPost( const float injectionToFirstPostTime )
{
	if( timestamps_.size() > 1 ) {
		if( isFastDynamic_ ) {
			this->injectionTime_ = timestamps_[0];
		} else {
			this->injectionTime_ = timestamps_[1].addSecs( -injectionToFirstPostTime );
		}
	}
}

/*!
 * \brief Sets the nt_ (number of timepoints) in this image.
 * \param t Number of timepoints to set. If this value passed is less than 0, we set to 1, so there is never less than 1 timepoint. If it's greater than the total size of the collection of timepoints in the data, then we set it to this value, so that the nt_ is clamped to the passed argument.
 */
void QiMriImage::setNt(int t){
	auto maxT = static_cast<int>(data_.size());
	if(t < 0)
		nt_ = 1;
	else {
		if (t > maxT)
			nt_ = maxT;
		else
			nt_ = t;
	}
}
//!
//! \brief Checks if the image has duplicate timestamps
//! \prePost{\objInit{QiMriImage}, Returns true if there are duplicate timestamps, otherwise false}
//!
bool QiMriImage::hasDuplicateTimestamps()
{
	for (const auto& time : std::as_const(timestamps_)){
		if (timestamps_.count(time) > 1){
			return true;
		}
	}
	return false;
}

//!
//! \brief Attempts to correct duplicate timestamps by using the 'Content Time' dicom tag
//! \details Queries the dicom_data table to get the 'Content Times' of the series and updates the timestamps_ of the images.
//! \attention Content Time may refer to the time the image was saved and not the time it was acquired, so this should be used cautiously and only
//! when the Acquisition Time or Trigger time fields do not have appropriate values.
//! \param dbSettings connection settings to the QuantX database
//! \prePost{QiMriImage has been initialized and nt_ has been determined, Timestamps updated and returns true, returns false if timestamps are not updated}
//!
bool QiMriImage::correctDuplicateTimestamps(const QStringList &dbSettings)
{
	bool result = false;
	if (nt_ < 2) // it doesn't make sense to do this with only one timepoint
		return result;
	QScopedPointer<QISql> qisql(new QISql(dbSettings));
	{
		auto db = qisql->createDatabase("correctTimestamps");
		db.open();
		if (db.isOpen()){
			auto timepoints = qisql->getDicomValues(seriesUID_.endsWith(".mc") ? QString{seriesUID()}.remove(seriesUID_.length() - 3, 3) : seriesUID_,
			                                        QStringList{"0020,0100", "0008,0033"}); //get temporal pos id and 'content time' values
			QVector<QTime> times;
			for (const auto &time: std::as_const(timepoints)){// make sure the times are all valid/convertible before we use them
				times <<  QTime::fromString(time.at(1).toString().trimmed(), "hhmmss.z");
				if (!times.last().isValid())// couldn't convert to a Time
					return result;
			}
			if (timepoints.size() == nt_){// if these don't match we should use a different method/dicom value to get the correct timestamps
				for (int i = 0; i<nt_; i++){
					timestamps_[i] = times.at(i);
				}
				// the following isn't necessary if the timestamps are corrected prior to when injectionTime_ is normally set
				injectionTime_ = timestamps_[1].addSecs( -60 );
				result = true;
			}
		}
	}
	QSqlDatabase::removeDatabase("correctTimestamps");
	return result;
}
