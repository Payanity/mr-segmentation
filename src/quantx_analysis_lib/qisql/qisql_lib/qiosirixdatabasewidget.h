#ifndef QIOSIRIXDATABASEWIDGET_H
#define QIOSIRIXDATABASEWIDGET_H

#include <QtGlobal>
#ifdef Q_OS_MAC

#include "qisqlDllCheck.h"
#include <QWidget>
#include <QTableWidget>

class QISQL_EXPORT QiOsirixDatabaseWidget : public QWidget
{
    Q_OBJECT
public:
	explicit QiOsirixDatabaseWidget(QWidget *parent = nullptr);
    void setDatabaseSettings(const QStringList &dbSettings);
    QStringList getDatabaseSettings();

signals:
    void casesImported();

public slots:
    void importStudiesFromColumnSelection();
    void initializeTableWidget();
    void searchFilterUpdate();
    void sortByColumn(int c, Qt::SortOrder s);

protected:
    QTableWidget* tableWidget;
    QStringList databaseSettingsQI;
    QStringList databaseSettingsOSIRIX;
    QLineEdit* nameLineEdit;
    QLineEdit* mrnLineEdit;

private:

};

#endif // Q_OS_MAC
#endif // QIOSIRIXDATABASEWIDGET_H
