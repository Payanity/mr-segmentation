#ifndef SQLSEARCHWINDOW_H
#define SQLSEARCHWINDOW_H

#include "qisqlDllCheck.h"
#include <QWidget>
#include <QTableWidget>
//#include <QStandardItemModel>
#include "qi3dimage.h"
#include "qi2dimage.h"
#include "../../quantxdata/quantxdata_lib/quantx.h"
#include "qisql.h"
#include <QSqlDatabase>
#include <utility>
class QSqlTableModel;
class QiMriImage;

/*!
 * \brief The SqlSearchWindow class
 * \class SqlSearchWindow
 * \ingroup SQLModule
 * \deprecated
 * \note This class may still be used here and there, but those places should be refactored away at some point.
 * This class was previously the main way of accessing e.g. new patient loads, but we are now doing things differently, using the quantx::sql::SqlDatabaseService in an MVC architecture.
 */
class QISQL_EXPORT SqlSearchWindow : public QWidget
{
	Q_OBJECT

public:
    explicit SqlSearchWindow(const QStringList &dbSettings = QStringList(), QWidget *parent = nullptr);
    void setDatabaseSettings(const QStringList &dbSettings);
    QStringList getDatabaseSettings();
    QDialog *createLoadingImageDialog(QUANTX::Modality, QWidget *parent = nullptr);
    void setAutoLoadSeries(bool autoLoad = true){ autoLoadSeries = autoLoad; } //!< Setter for autoload.

signals:
	/*!
	 * \brief Emitted when an MRN is selected in the GUI.
	 */
	void selectedMRN(QString);

	/*!
	 * \brief Emitted when an MR series starts loading.
	 */
	void startLoadingMRSeries(QiMriImage *);

//	void imageLoaded(Qi3DImage *);
//	void imageLoaded(Qi2DImage *);
//	void imageLoaded(QVector<Qi2DImage *>);

public slots:
	QPair<QString,QString> findMRDate(const QString& studiesTable, const QString& mrnFilter, QSqlDatabase &db);
	virtual void doubleClickAnyColumn(int row, int col);
	void initializeTableWidget();
	void searchFilterUpdate(const QString& SearchText);
	void sortByColumn(int c, Qt::SortOrder s);
	bool openCase(const QString& mrn);
#ifdef Q_OS_MAC
	virtual void connectToOsirix();
#endif

protected:
	QTableWidget* tableWidget; //!< Stores pointer to the table widget.
	//QStandardItemModel itemModel;
	QStringList databaseSettings; //!< Stores the DB settings.
	QLineEdit* searchBarLineEdit; //!< Stores pointer to the line edit.

private:
	void doubleClickLoadSeries(int row);
	virtual void loadSeries(QString seriesID, QUANTX::Modality modality);
    virtual void checkDatabaseVersion(QSqlDatabase &db);

    bool autoLoadSeries{}; //!< Stores autoload value

};

#endif // SQLSEARCHWINDOW_H
