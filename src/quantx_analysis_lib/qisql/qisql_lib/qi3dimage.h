#ifndef QI3DIMAGE_H
#define QI3DIMAGE_H

#include "qiabstractimage.h"
#include <QObject>
#include <QMap>
#include "pointvector3d.h"
class Plm_image;
class QTextEdit;

/*!
 * \brief Base class for all 3D modalities.
 * \ingroup SQLModule
 * \class Qi3DImage
 */
class QISQL_EXPORT Qi3DImage : public QObject, public QiAbstractImage
{
	Q_OBJECT
public:
	Qi3DImage() = default;
	const std::vector<std::vector<std::vector<std::vector<quint16> > > > & data() const { return data_; } //!< Getter for the image data.

	constexpr inline const float & dz() const { return dz_; } //!< Getter for the size in real world millimeters of the z voxel dimension.
	constexpr inline const int & nz() const { return nz_; } //!< Getter for the number of voxels in the z dimension.
	virtual int nt() const { return nt_; } //!< Getter for the number of timepoints in the image, assuming only one pre-contrast timepoint. This is normal for most non-research e.g. MR protocols etc.
	virtual const QVector<float> & orientation() const { return orientation_; } //!< Getter for the orientation.
	virtual const QVector<QVector3D> & positions() const { return positions_; } //!< Getter for the right-most (from patient perspective), anterior-most voxel per slice, in a QVector.
	virtual const QVector3D & origin() const { return origin_; } //!< Getter for the origin of the image slices. This should equal the first of Qi3DImage::positions()
	constexpr inline const QUANTX::Orientation & acquisitionOrientation() const { return acquisitionOrientation_; } //!< Getter for the acquisition orientation of the image.
	virtual QVector3D toRealworld(PointVector3D<int> index);
	virtual QVector3D toRealworld(int x, int y, int z);
	virtual PointVector3D<int> toIndex(QVector3D realworldPos);
	virtual PointVector3D<int> toIndex(float x, float y, float z);
	virtual int toIndexX(float x);
	virtual int toIndexY(float y);
	virtual int toIndexZ(float z);

	inline PointVector3D<int> fromCoordinateSystem( const PointVector3D<int> coordinates, Qi3DImage* from ) { return fromCoordinateSystem( coordinates.x(), coordinates.y(), coordinates.z(), from ); }
	inline PointVector3D<int> toCoordinateSystem( const PointVector3D<int> coordinates, Qi3DImage* to ) { return toCoordinateSystem( coordinates.x(), coordinates.y(), coordinates.z(), to ); }
	inline PointVector3D<int> fromCoordinateSystem( const int x, const int y, const int z, Qi3DImage* from ) { return this == from ? PointVector3D<int>{ x, y, z } : this->toIndex( from->toRealworld( x, y, z ) ); }
	inline PointVector3D<int> toCoordinateSystem( const int x, const int y, const int z, Qi3DImage* to ) { return this == to ? PointVector3D<int>{ x, y, z } : to->toIndex( this->toRealworld( x, y, z ) ); }

	constexpr PointVector3D<int> imageSize() const { return {nx_, ny_, nz_}; }
	constexpr PointVector3D<float> voxelSize() const { return {dx_, dy_, dz_}; }

	virtual int numSlices(QUANTX::Orientation orientation) const;
	constexpr int numSlices() const {
		if (acquisitionOrientation_ == QUANTX::XYPLANE)
			return nz_;
		if (acquisitionOrientation_ == QUANTX::YZPLANE)
			return nx_;
		if (acquisitionOrientation_ == QUANTX::XZPLANE)
			return ny_;

		return 1; // shouldn't get here
	}

	void motionCorrectImage(const QStringList& databaseSettings, int fixedIdx = 1, QTextEdit * textEdit = nullptr);
	void setOrigin(const QVector3D & o){origin_ = o;}

Q_SIGNALS:
	void openProgressUpdated(int); //!< Emitted when the progress of opening an image has updated.
	void openProgressNewMaximum(int); //!< Emitted when a new max value used for computing load progress has been determined.
	void startMotionCorrection(); //!< Emitted only when we are using (building with) internal timing output, and the motion correction starts.
	void finishedMCCreatePlastimatchImages(); //!< Emitted when the motion-correction create plastimatch images is complete.
	void finishedMCXForms(); //!< Emitted when the motion correction transforms are finished. Mainly useful for internal timing output/benchmarking.
	void finishedMCWarping(); //!< Emitted when the motion correction warping has finished. Mainly useful for internal timing output/benchmarking.
	void finishedMCQuantXImages(); //!< Emitted when the motion correction process is complete. Mainly useful for internal timing output/benchmarking.

protected:
	float dz_{}; //!< Stores the size in real world millimeters of a voxel in the z dimension.
	int nz_{}; //!< Stores the number of voxels in the z dimension.
	int nt_{}; //!< Stores the number of timepoints plus one pre-contrast timepoint.
	QVector<float> orientation_; //!< Stores the orientation of the image.
	QVector<QVector3D> positions_; //!< Stores the right-most, anterior-most voxel per slice from the patient perspective.
	QVector3D origin_; //!< Stores the origin, which should be the first of the positions_
	QVector<QMap< float, QString > > instanceNums_; //!< Stores instance nums.
	QUANTX::Orientation acquisitionOrientation_; //!< Stores the acquisition orientation.
	std::vector<std::vector<std::vector<std::vector<quint16> > > > data_; //!< Stores the image data.
	QVector<Plm_image *> createPlastimatchImages(QTextEdit * textEdit = nullptr);
	void setWarpedData(Plm_image *image, int t);

private:

};

#endif // QI3DIMAGE_H
