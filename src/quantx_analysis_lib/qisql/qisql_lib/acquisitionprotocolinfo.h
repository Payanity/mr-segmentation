#ifndef ACQUISITIONPROTOCOLINFO_H
#define ACQUISITIONPROTOCOLINFO_H

#include <QString>

namespace quantx {
namespace acquisition {

/*!
 * \brief Simple struct for containing data for the AcquisitionProtocolInfo.
 * \ingroup SQLModule
 */
struct AcquisitionProtocolInfo
{
	int protocolNumber{ -1 }; //!< Stores protocol number.
	QString keyName{}; //!< Stores key name.
	QString keyValue{}; //!< Stores key value.

};

}
}

#endif // ACQUISITIONPROTOCOLINFO_H
