#ifndef QISQLMODELS_H
#define QISQLMODELS_H

// I don't think this file is used anywhere...
#if 0

#include <string>
#include <vector>

#include <Wt/WDate.h>

namespace QiSqlModel {

//Tables

// The first few should be directly related to other QiSql library classes. We use helper functions
// to convert this database query to the image objects exported by this library.
class Images;
class ImagesMG;
class ImagesMR;
class ImagesUS;
class Series;
class Studies;
class Patients;
class SeriesThumbnails;
class DicomData;
class DatabaseVersion;
class MotionCorrection;

// Views

class Seriesview;
class Imagelocations;
class Seriesnumimages;

class Images {
public:
	Images() {}
	std::string mrn;
	std::string imageuid;
	std::string seriesuid;
	std::string studyuid;
	std::vector<unsigned char> image_data;
	Wt::WDate date;
	Wt::WTime time;
	int width;
	int height;
	double pixelsize_x;
	double pixelsize_y;
	std::string imageType;
	std::string windowWidth;
	std::string windowCenter;
	std::string minPixval;
	std::string maxPixval;
	std::string imagePosition;
	std::string sliceLocation;
	std::string temporalPosID;
	//std::string diffusionBValue;
	//std::string diffustionGradientOrientation;
	std::string instanceNum;
	std::string scanOptions;
	std::string viewPosition;
	std::string imageLaterality;
	std::string patientOrientation;
	std::string photometricInterpretation;
	std::string manufacturer;
	std::string modelName;
};

class ImagesMG {
public:
	ImagesMG() {}
	std::string mrn;
	std::string imageuid;
	std::string seriesuid;
	std::string studyuid;
	std::vector<unsigned char> image_data;
	Wt::WDate date;
	Wt::WTime time;
	int width;
	int height;
	double pixelsize_x;
	double pixelsize_y;
	std::string imageType;
	std::string windowWidth;
	std::string windowCenter;
	std::string minPixval;
	std::string maxPixval;
	std::string imagePosition;
	std::string sliceLocation;
	std::string temporalPosID;
	//std::string diffusionBValue;
	//std::string diffustionGradientOrientation;
	std::string instanceNum;
	std::string scanOptions;
	std::string viewPosition;
	std::string imageLaterality;
	std::string patientOrientation;
	std::string photometricInterpretation;
	std::string manufacturer;
	std::string modelName;
	int numberOfFrames;
};

class ImagesMR {
public:
	ImagesMR() {}
	std::string mrn;
	std::string imageuid;
	std::string seriesuid;
	std::string studyuid;
	std::vector<unsigned char> image_data;
	Wt::WDate date;
	Wt::WTime time;
	int width;
	int height;
	double pixelsize_x;
	double pixelsize_y;
	std::string imageType;
	std::string windowWidth;
	std::string windowCenter;
	std::string minPixval;
	std::string maxPixval;
	std::string imagePosition;
	std::string sliceLocation;
	std::string temporalPosID;
	std::string diffusionBValue;
	std::string diffustionGradientOrientation;
	std::string instanceNum;
	std::string scanOptions;
	std::string viewPosition;
	std::string imageLaterality;
	std::string patientOrientation;
	std::string photometricInterpretation;
	std::string manufacturer;
	std::string modelName;
	int triggerTime;
	double magneticFieldStrength;
};

class ImagesUS {
public:
	ImagesUS() {}
	std::string mrn;
	std::string imageuid;
	std::string seriesuid;
	std::string studyuid;
	std::vector<unsigned char> image_data;
	Wt::WDate date;
	Wt::WTime time;
	int width;
	int height;
	double pixelsize_x;
	double pixelsize_y;
	std::string imageType;
	std::string windowWidth;
	std::string windowCenter;
	std::string minPixval;
	std::string maxPixval;
	std::string imagePosition;
	std::string sliceLocation;
	std::string temporalPosID;
	//std::string diffusionBValue;
	//std::string diffustionGradientOrientation;
	std::string instanceNum;
	std::string scanOptions;
	std::string viewPosition;
	std::string imageLaterality;
	std::string patientOrientation;
	std::string photometricInterpretation;
	std::string manufacturer;
	std::string modelName;
	int numberOfFrames;
};

class Series {
public:
	Series();
	std::string mrn;
	std::string seriesuid;
	std::string studyuid;
	std::string seriesNumber;
	std::string modality;
	std::string seriesDesc;
	Wt:WDate date;
	Wt::WTime time;
	int numTemporalPositions;
	std::string imageOrientation;
	std::string spaceBetweenSlices;
};

class Studies {
public:
	Studies();
	std::string mrn;
	std::string studyuid;
	std::string modality;
	Wt::WDate date;
	Wt::WTime time;
};

class Patients {
public:
	Patients();
	std::string mrn;
	std::string patientName;
	Wt::WDate dob;
	std::string sex;
};

class SeriesThumbnails {
public:
	SeriesThumbnails();
	std::string seriesUID;
	int maxDim;
	int minDim;
	int width;
	int height;
	std::vector<unsigned char> thumbnail;
};

class DicomData {
public:
	DicomData();
	std::string tag_0008_0008;
	std::string tag_0008_0016;
	std::string tag_0008_0018;
	std::string tag_0008_0020;
	std::string tag_0008_0021;
	std::string tag_0008_0023;
	std::string tag_0008_0030;
	std::string tag_0008_0031;
	std::string tag_0008_0032;
	std::string tag_0008_0033;
	std::string tag_0008_0060;
	std::string tag_0008_0070;
	std::string tag_0008_1090;
	std::string tag_0010_0010;
	std::string tag_0010_0020;
	std::string tag_0010_0030;
	std::string tag_0010_0040;
	std::string tag_0010_1010;
	std::string tag_0010_21c0;
	std::string tag_0018_0020;
	std::string tag_0018_0021;
	std::string tag_0018_0022;
	std::string tag_0018_0023;
	std::string tag_0018_0024;
	std::string tag_0018_0025;
	std::string tag_0018_0050;
	std::string tag_0018_0080;
	std::string tag_0018_0081;
	std::string tag_0018_0082;
	std::string tag_0018_0083;
	std::string tag_0018_0084;
	std::string tag_0018_0085;
	std::string tag_0018_0086;
	std::string tag_0018_0087;
	std::string tag_0018_0088;
	std::string tag_0018_0089;
	std::string tag_0018_0090;
	std::string tag_0018_0091;
	std::string tag_0018_0093;
	std::string tag_0018_0094;
	std::string tag_0018_0095;
	std::string tag_0018_1020;
	std::string tag_0018_1040;
	std::string tag_0018_1041;
	std::string tag_0018_1042;
	std::string tag_0018_1043;
	std::string tag_0018_1044;
	std::string tag_0018_1045;
	std::string tag_0018_1046;
	std::string tag_0018_1047;
	std::string tag_0018_1048;
	std::string tag_0018_1049;
	std::string tag_0018_1050;
	std::string tag_0018_1060;
	std::string tag_0018_1088;
	std::string tag_0018_1090;
	std::string tag_0018_1094;
	std::string tag_0018_1100;
	std::string tag_0018_1250;
	std::string tag_0018_1310;
	std::string tag_0018_1312;
	std::string tag_0018_1314;
	std::string tag_0018_1315;
	std::string tag_0018_1316;
	std::string tag_0018_5100;
	std::string tag_0018_9075;
	std::string tag_0018_9087;
	std::string tag_0018_9089;
	std::string tag_0020_000d;
	std::string tag_0020_000e;
	std::string tag_0020_0010;
	std::string tag_0020_0011;
	std::string tag_0020_0012;
	std::string tag_0020_0013;
	std::string tag_0020_0032;
	std::string tag_0020_0037;
	std::string tag_0020_0052;
	std::string tag_0020_0060;
	std::string tag_0020_0100;
	std::string tag_0020_0105;
	std::string tag_0020_0110;
	std::string tag_0020_1040;
	std::string tag_0020_1041;
	std::string tag_0028_0002;
	std::string tag_0028_0003;
	std::string tag_0028_0004;
	std::string tag_0028_0010;
	std::string tag_0028_0011;
	std::string tag_0028_0030;
	std::string tag_0028_0100;
	std::string tag_0028_0101;
	std::string tag_0028_0102;
	std::string tag_0028_0103;
	std::string tag_0028_0106;
	std::string tag_0028_0107;
	std::string tag_0028_0120;
	std::string tag_0028_1050;
	std::string tag_0028_1051;
	std::string tag_0032_1032;
	std::string tag_0040_0253;
	std::string tag_0040_0254;
	std::string tag_0040_9224;
	std::string tag_0040_9225;
	std::string tag_2001_1003;
	std::string tag_2001_1004;
	std::string tag_2001_10b0;
	std::string tag_2001_10b1;
	std::string tag_2001_10b2;
	std::vector<unsigned char> fullHeader;
};

class DatabaseVersion {
public:
	DatabaseVersion();
	int version;
};

class MotionCorrection {
public:
	MotionCorrection();
	std::string seriesuid_moving;
	std::string seriesuid_fixed;
	int t_fixed;
	std::vector<unsigned char> xform;
	std::vector<unsigned char> imageHeader;
	float default_value;
};

} // namespace QiSqlModel

#endif

// Use the following example to change a primary key with Wt::Dbo
/*
namespace Wt {
	namespace Dbo {

		template<>
		struct dbo_traits<Images> : public dbo_default_traits {
			constexpr static char *surrogateIdField() {
				return "idqi_images";
			}
		};

	}
}
*/

#endif // QISQLMODELS_H
