#include "seganalysistest.h"
#include "lesiondatabase.h"
#include "model/lesion.h"
#include "model/lesiondetails.h"
#include "databasehelper.h"
#include "knowncase.h"
#include "Wt/WDateTime.h"
#define useDebug 0
#if useDebug
#include <QDebug>
#endif

using namespace quantx::mranalysis;

void SegAnalysisTest::lesionDBTest()
{
    QIWARN("TMRA 7");

    QString seriesUUID = "1.2.826.0.1.3680043.2.1125.1.53824442241018484344524396690377038";

	QStringList dbSettings{"QSQLITE", "", dbPath, "", ""};
#if useDebug
    qDebug() << "DB Settings: " << dbSettings;
#endif
    auto lesionDB = std::make_unique<QuantXLesionDatabase>(dbSettings);
    auto lesionDetailsDB = std::make_unique<QuantXLesionDatabase>(dbSettings);
    Wt::Dbo::Transaction lesionDBtransaction(lesionDB->session());

    QFile file("data_test/mrknowncases.dat");
    if (!file.open(QIODevice::ReadOnly)){
#if useDebug
        qDebug() << "Could not open know cases data file";
#endif
    }
    QVector<const KnownCase *> expectedKnownCases = DatabaseHelper::decodeKnownCaseHelper(file.readAll());
#if useDebug
        qDebug() << "Number of knowncases: " << expectedKnownCases.count();
#endif
    auto lesions = lesionDB->findLesionInDatabaseBySeries(seriesUUID.toStdString());
    auto lesions_found = 0;
    QICOMPARE2(lesions.size(), size_t(1), "Compare number of lesions found to expected number"); // The warning here is actually from inside the Wt Library

    for(const auto& lesion : lesions){
        lesions_found++;
        auto origFeatures = mrAnalysis->model()->getFeatures();
        QICOMPARE2(static_cast<float>(lesion->qi_score), static_cast<float>(origFeatures.at(0)), "Test lesion QI score value from DB");
        QICOMPARE2(static_cast<float>(lesion->volume * 1000.), static_cast<float>(origFeatures.at(8)), "Test lesion volume from DB");
        QICOMPARE2(static_cast<float>(lesion->surface_area), static_cast<float>(origFeatures.at(9)), "Test lesion surface area from DB");
        auto seedPoint = mrAnalysis->model()->getSeedPoint();
        QICOMPARE2(lesion->seed_x, seedPoint.x(), "Test lesion seed x from DB");
        QICOMPARE2(lesion->seed_y, seedPoint.y(), "Test lesion seed y area from DB");
        QICOMPARE2(lesion->seed_z, seedPoint.z(), "Test lesion seed z area from DB");
        auto realWordSeed = image->toRealworld(seedPoint.x(), seedPoint.y(), seedPoint.z());
        QICOMPARE2(lesion->seed_realworld_x, realWordSeed.x(), "Test lesion seed realworld x from DB");
        QICOMPARE2(lesion->seed_realworld_y, realWordSeed.y(), "Test lesion seed realworld y area from DB");
        QICOMPARE2(lesion->seed_realworld_z, realWordSeed.z(), "Test lesion seed realworld z area from DB");
        Wt::Dbo::Transaction lesionDetailsDBtransaction(lesionDetailsDB->session());
        auto lesionDetails = lesionDetailsDB->findLesionDetailsInDatabase(lesion->UUID);
        auto segfilled = DatabaseHelper::cryptDecodeBinary<QVector<QVector<QPoint> > >(lesionDetails->segmentation_filled, DatabaseHelper::SegFilledKey);
        auto knownCases = DatabaseHelper::cryptDecodeKnownCase(lesionDetails->similarcases);
#if useDebug
        qDebug() << "Number of known cases from DB for current lesion: " << knownCases.count();
#endif
        for (int i = 0; i < knownCases.count(); i++){
            QICOMPARE2(knownCases.at(i)->filename, expectedKnownCases.at(i)->filename, QString("Compare filename for known case %1 from database with expected").arg(i).toUtf8());
        }
        QVector<QVector<QPolygon> > segmentation;
        for(const auto & slicePoints : std::as_const(segfilled)){
            segmentation << MrAnalysis::traceBorders2D(slicePoints);
        }
        QICOMPARE2(QString(lesion->UUID.c_str()), mrAnalysis->model()->uuid(), "Compare UUID from database with expected");
        auto maxUptake = mrAnalysis->model()->getMostUptaking();
        auto avgUptake = mrAnalysis->model()->getAvgUptake();
        auto timepoints = image->getTimestamps();
        auto dbTimepoints = DatabaseHelper::decodeBinary<QList<QTime>>(lesion->timepoints);
        auto dbMaxUptake = DatabaseHelper::decodeBinary<QVector<double>>(lesion->maxUptake);
        auto dbAvgUptake = DatabaseHelper::decodeBinary<QVector<double>>(lesion->avgUptake);        for (int i = 0; i < timepoints.count(); i++){
            QICOMPARE2(dbTimepoints.at(i), timepoints.at(i), "Compare timepoints from database with expected");
        }
#if useDebug
        qDebug() << "maxUptake: " << maxUptake;
        qDebug() << "DB maxUptake: " << dbMaxUptake;
        qDebug() << "avgUptake: " << avgUptake;
        qDebug() << "DB avgUptake: " << dbAvgUptake;
#endif
        QICOMPARE2(dbMaxUptake, maxUptake, "Compare max. uptake from database with expected");
        QICOMPARE2(dbAvgUptake, avgUptake, "Compare avg. uptake from database with expected");
        auto origRegions = mrAnalysis->getRegions();
        auto dbRegions = DatabaseHelper::cryptDecodeBinary<QVector<QRegion>>(lesionDetails->segmentation_outline, DatabaseHelper::SegOutlineKey);
        for (int i=0; i< origRegions.count(); i++){
            QICOMPARE2(dbRegions.at(i), origRegions.at(i), "Compare db segmentation region with expected");
        }

        auto dbFeatures = DatabaseHelper::cryptDecodeBinary<QVector<double>>(lesionDetails->features, DatabaseHelper::FeatureKey);
        for (int i=0; i< origFeatures.count(); i++){
            QICOMPARE2(dbFeatures.at(i), origFeatures.at(i), "Compare db features with expected");
        }
        lesionDetailsDBtransaction.commit();
    }
    lesionDBtransaction.commit();
}

void SegAnalysisTest::clearLesionDBTables()
{
	QStringList dbSettings{"QSQLITE", "", dbPath, "", ""};
    auto lesionDB = std::make_unique<QuantXLesionDatabase>(dbSettings);
    Wt::Dbo::Transaction lesionDBtransaction(lesionDB->session());
    Wt::Dbo::collection<Wt::Dbo::ptr<Lesion> > lesions = lesionDBtransaction.session().find<Lesion>();
    for(auto lesion : lesions)
        lesion.remove();
    Wt::Dbo::collection<Wt::Dbo::ptr<LesionDetails> > details = lesionDBtransaction.session().find<LesionDetails>();
    for(auto detail : details)
        detail.remove();
    lesionDBtransaction.commit();
}

void SegAnalysisTest::verifyLesionsDeleted()
{
	QStringList dbSettings{"QSQLITE", "", dbPath, "", ""};
	auto lesionDB = std::make_unique<QuantXLesionDatabase>(dbSettings);
	Wt::Dbo::Transaction lesionDBtransaction(lesionDB->session());
	QString seriesUUID = "1.2.826.0.1.3680043.2.1125.1.53824442241018484344524396690377038";
	auto lesions_found = lesionDB->findLesionInDatabaseBySeries(seriesUUID.toStdString());
	QICOMPARE2(lesions_found.size(), size_t(0), "Compare number of lesions found to expected number"); // The warning here is actually from inside the Wt Library

}
