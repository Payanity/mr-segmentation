#include "seganalysistest.h"

#define writeAnalysis 0

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

void SegAnalysisTest::analysisTest()
{
    QIWARN("TMRA 4");
    QString filename = "sql_test/00050-Motion Corr. dyn_eTHRIVE_448-488_125-167_130-169.dat";
    image->readMRTestData(filename);

    int segNumber = 1;

    QFile readFile;
    readFile.setFileName(QString(filename + "-seg%1").arg(segNumber));
    readFile.open(QIODevice::ReadOnly);
    QDataStream in(&readFile);
    QVector<QVector<QPolygon> > segmentation;
    in >> segmentation;
    readFile.close();

    mrAnalysis->setSegmentation(segmentation);
    mrAnalysis->setSegmentationDone(true);
    mrAnalysis->calculateFeatures(QVector<uint>(1, 5)); //Use one c-means run with 5 bins

#if writeAnalysis
    QFile writeFile;
    writeFile.setFileName(QString(filename + "-seg%1features").arg(segNumber));
    writeFile.open(QIODevice::WriteOnly);
    QDataStream out(&writeFile);
    out << mrAnalysis->getFeatures();
    writeFile.close();
#endif

    readFile.setFileName(QString(filename + "-seg%1features").arg(segNumber));
    readFile.open(QIODevice::ReadOnly);
    QVector<double> goodFeatures;
    in >> goodFeatures;
    readFile.close();
    QIVERIFY2(!goodFeatures.isEmpty(), "Test that expected lesion feature values have been read");
#if useDebug
    qDebug() << "Good Features" << goodFeatures;
    qDebug() << "Calc Features" << mrAnalysis->getFeatures();
#endif
    QList<int> seVersionFeatures;
    seVersionFeatures <<  8  // volume
               <<  9  // surface area
               << 12; // effective radius
    for(int i=0;i<goodFeatures.count();i++){
#ifdef SE_VERSION_TEST
        if ( seVersionFeatures.indexOf(i) != -1 )
#endif
            QICOMPARE2(static_cast<float>(mrAnalysis->model()->getFeatures()[i]),
                       static_cast<float>(goodFeatures[i]),
                       QString("Test lesion feature value %1").arg(i).toUtf8());
    }
}
