#include <QVector>
#include <QList>
#include "binarycube.h"
#include "seganalysistest.h"
#include "mrfeatures.h"
#include <cmath>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

void SegAnalysisTest::binarycubeTest(){
   QIWARN("TMRA 6");
   const double PI = 4.0*atan(1.0);
   //std::vector<std::vector<int> > vertices1, vertices2;
   //std::vector<int> p1, p2, p3, p4,
   //         q1, q2, q3, q4;

   std::array p1 {false, false, true};
   std::array p2 {false, true, true};

   std::array q1 {true, false, false};
   std::array q2 {false, false, false};

   std::vector<std::array<bool, 3> > vertices1 {p1, p2};
   std::vector<std::array<bool, 3> > vertices2 {q1, q2};

   QIVERIFY2(binarycube(vertices1) == binarycube(vertices2), "Test comparison of same 3D shape in two different orientations");

   vertices1 = {p1};
   vertices2 = {q1};
   QIVERIFY2(binarycube(vertices1) == binarycube(vertices2), "Test comparison of same 3D shape in two different orientations");

   std::array p3 {true, false, false};

   q1 = {true, false, false};
   q2 = {true, false, true};
   std::array q3 {false, true, false};

   vertices1 = {p1, p2, p3};
   vertices2 = {q1, q2, q3};
   QIVERIFY2(binarycube(vertices1) == binarycube(vertices2), "Test comparison of same 3D shape in two different orientations");

   vertices2[2][1] = false;
   QIVERIFY2(binarycube(vertices1) != binarycube(vertices2), "Test comparison of two 3D shapes");

   p1[0] = p1[1] = p1[2] = false;
   p2[0] = true;
   p2[1] = p2[2] = false;
   p3[0] = p3[2] = true;
   p3[1] = false;
   std::array p4 {false, true, false};

   q1[0] = q1[1] = true;
   q1[2] = false;
   q2[0] = q2[2] = true;
   q2[1] = false;
   q3[0] = q3[2] = false;
   q3[1] = true;
   std::array q4 {true, true, true};


   vertices1 = {p1, p2, p3, p4};
   vertices2 = {q1, q2, q3, q4};
   QIVERIFY2(binarycube(vertices1) == binarycube(vertices2), "Test comparison of same 3D shape in two different orientations");

   p1[0] = p1[1] = p1[2] = false;
   p2[0] = p2[2] = false;
   p2[1] = true;

   q1[0] = q1[1] = q1[2] = true;
   q2[0] = q2[1] = q2[2] = false;

   vertices1 = {p1, p2};
   vertices2 = {q1, q2};
   QIVERIFY2(binarycube(vertices1) != binarycube(vertices2), "Test comparison of two 3D shapes");

   p1[0] = true;
   p1[1] = p1[2] = false;
   p2[0] = false;
   p2[1] = p2[2] = true;

   q1[0] = q1[1] = q1[2] = true;
   q2[0] = q2[1] = q2[2] = false;

   vertices1 = {p1, p2};
   vertices2 = {q1, q2};
   QIVERIFY2(binarycube(vertices1) == binarycube(vertices2), "Test comparison of two 3D shapes");

   //test surface area
   //make a box and a sphere
   QVector<QVector<QVector<double> > > box, sphere;
   int width, height, length;
   double radius, dist;
   int max_radius = 100;
   width = 20;
   height = 60;
   length = 40;
   radius = 85.0;

   for (int i = 0; i < max_radius * 2; i++){
       box.append(QVector<QVector<double> >());
       sphere.append(QVector<QVector<double> >());

       for(int j = 0; j < max_radius * 2; j++){
           box[i].append(QVector<double>());
           sphere[i].append(QVector<double>());
           for(int k = 0; k < max_radius * 2; k++){
               if ( i >= 10 && i < 10 + width &&
                    j >= 10 && j < 10 + height &&
                    k >= 10 && k < 10 + length )
                   box[i][j].append(1);
               else
                   box[i][j].append(0);
               dist = sqrt( pow(i-max_radius,2) + pow(j-max_radius,2) + pow(k-max_radius,2) );
               if ( dist <= radius )
                   sphere[i][j].append(1);
               else
                   sphere[i][j].append(std::min(0.0, dist - radius));
           }
       }
   }
   double calculatedBox, calculatedSphere, expectedBox, expectedSphere;
   calculatedBox = findSurfaceArea(box);
   calculatedSphere = findSurfaceArea(sphere);
   expectedBox = width*height*2+width*length*2+length*height*2;
   expectedSphere = 4.0*PI*radius*radius;

   double sphereError = qAbs((expectedSphere - calculatedSphere)/expectedSphere);
   double boxError = qAbs((expectedBox - calculatedBox)/expectedBox);

#if useDebug
   qDebug() << "Expected Box area: " << expectedBox;
   qDebug() << "Expected Sphere area: " << expectedSphere;
   qDebug() << "Calculated Box area: " << calculatedBox;
   qDebug() << "Calculated Sphere area: " << calculatedSphere;
   qDebug() << "Sphere Error: " << sphereError * 100 << "%";
   qDebug() << "Box Error: " << boxError * 100 << "%";
#endif

   QIVERIFY2(sphereError < 0.01, "Test surface area calculation of a sphere");
   QIVERIFY2(boxError < 0.1, "Test surface area calcualtion of a cube");

// test all cases
   const binarycube b1(std::vector<std::array<bool, 3> >{ {false, false, false} });
   const binarycube b2(std::vector<std::array<bool, 3> >{ {true, false, false},
                                                        {true, true, false} });
   const binarycube b3(std::vector<std::array<bool, 3> >{ {true, false, false},
                                                        {true, true, true} });
   const binarycube b4(std::vector<std::array<bool, 3> >{ {false, false, false},
                                                        {true, true, true} });
   const binarycube b5(std::vector<std::array<bool, 3> >{ {true, false, false},
                                                        {true, true, false},
                                                        {false, true, false} });
   const binarycube b6(std::vector<std::array<bool, 3> >{ {true, false, false},
                                                        {true, true, false},
                                                        {false, true, true} });
   const binarycube b7(std::vector<std::array<bool, 3> >{ {false, true, false},
                                                        {true, false, false},
                                                        {true, true, true} });
   const binarycube b8(std::vector<std::array<bool, 3> >{ {false, false, false},
                                                        {true, false, false},
                                                        {false, true, false},
                                                        {true, true, false} });
   const binarycube b9(std::vector<std::array<bool, 3> >{ {true, false, false},
                                                        {false, true, false},
                                                        {true, true, false},
                                                        {true, true, true} });
   const binarycube b10(std::vector<std::array<bool, 3> >{ {true, false, false},
                                                         {true, true, false},
                                                         {false, false, true},
                                                         {false, true, true} });
   const binarycube b11(std::vector<std::array<bool, 3> >{ {true, false, false},
                                                         {false, true, false},
                                                         {false, true, true},
                                                         {true, true, false} });
   const binarycube b12(std::vector<std::array<bool, 3> >{ {false, false, true},
                                                         {true, false, false},
                                                         {true, true, false},
                                                         {false, true, false} });
   const binarycube b13(std::vector<std::array<bool, 3> >{ {false, false, false},
                                                         {true, true, false},
                                                         {true, false, true},
                                                         {false, true, true} });
   const binarycube b14(std::vector<std::array<bool, 3> >{ {false, false, false},
                                                         {true, true, false},
                                                         {false, true, false},
                                                         {true, true, true} });

   const QList<binarycube> cubes{b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14};


   //rotate each cube in every way and ensure everything is equal
   for (int i = 0; i < 14; i++){
       for (int yaw = 0; yaw < 4; yaw++){
           for(int pitch = 0; pitch < 4; pitch++){
               for(int roll = 0; roll < 4; roll++){
                   auto copy = cubes.at(i);
                   copy.rotate(yaw, pitch, roll);
                   QIVERIFY2(copy == cubes.at(i), QString("Test Lindblad cube configuration %1 in orientation %2").arg(i).arg(( yaw * 16 ) + ( pitch * 4 ) + roll).toUtf8().data());
                   //ensure there are no false positives
                   QVector<bool> expectedCubeCompare = QVector<bool>(13, false);
                   QVector<bool> actualCubeCompare;
                   for(int j = 0; j < 14; j++){
                       if (j != i){
                           actualCubeCompare << (copy == cubes.at(j));
                       }
                   }
                   QIVERIFY2(actualCubeCompare == expectedCubeCompare, QString("Test orientation %1 of Lindblad cube configuration %2 to all other Lindblad cube configurations").arg(( yaw * 16) + (pitch*4) + roll).arg(i).toUtf8().data());
               }
           }
       }
   }




}
