#include "seganalysistest.h"
//#define _USE_MATH_DEFINES // for C++
#include <cmath>

#ifndef M_PI
const double M_PI = std::acos(-1);
#endif

#define useDebug 0
#if useDebug
#include <QDebug>
#endif
#define saveLesionImages 0

void SegAnalysisTest::segmentSimulatedLesionTest()
{
	QIWARN("TMRA 5");
	QVector3D centerPoint(100,100,75);
	int xrange,yrange,zrange;
	//zrange=1;
	//xrange=2;
	//yrange=2;
	zrange=0;
	xrange=0;
	yrange=0;
	int lesSizeStart, lesSizeEnd;
	lesSizeStart=5;//lesion radius in mm
	lesSizeEnd=6;
	QIVERIFY2(true, "Create simulated lesions with different sizes");
	for (int radius=lesSizeStart;radius<=lesSizeEnd;radius++){
		QScopedPointer<TestMriImage> simImage(new TestMriImage());
		simImage->createSimulatedSphereMRData(200,200,150,5,static_cast<float>(0.2),static_cast<float>(0.2),centerPoint,radius,QString("Sim%1mm").arg(2*radius,2,10,QChar('0')), QUANTX::XYPLANE);
#if useDebug
		qDebug()<<"simimage variables"<<simImage->nx()<<simImage->ny()<<simImage->nz();
#endif
		QScopedPointer<MrAnalysisTest> mrAnalysis(new MrAnalysisTest(std::make_shared<MRLesionModel>(simImage.data())));
#if saveLesionImages
		auto lesionImages = simImage->getSlices(50,100,6);
#if useDebug
		qDebug()<<"size of lesion image list:"<<lesionImages.size();
#endif
		for(auto image : lesionImages)
			image.save("mranalysis_test/"+image.text("description")+"_S"+image.text("slice")+"_"+image.text("time")+".png","PNG");
#endif
		QIVERIFY2(true,"Run segmentation of simulated lesion with a range of seed point locations");
		for (int zseed=centerPoint.z()-zrange;zseed<=centerPoint.z()+zrange;zseed++)
			for(int yseed=centerPoint.y()-yrange;yseed<=centerPoint.y()+yrange;yseed++)
				for(int xseed=centerPoint.x()-xrange;xseed<=centerPoint.x()+xrange;xseed++){
#if useDebug
					qDebug()<<"Relative seed point (x,y,z):"<<xseed-centerPoint.x()<<yseed-centerPoint.y()<<zseed-centerPoint.z();
#endif
#if saveLesionImages
					QVector<QVector<QPolygon> > mrSegmentation =
#endif
					mrAnalysis->segmentSinglePoint({xseed, yseed, zseed});
					QVector<QVector<QPoint> > mrSegmentationPoints = mrAnalysis->getFilledRegion(QUANTX::XYPLANE);
					bool segmentationCorrect = true;
					ulong countFilledRegion = 0;
					ulong countMRSegmentationPoints = 0;
					int expectedImageValue = 0;
					// check that the segmentation exactly matches the simulated lesion
					for (int z=0;z<mrSegmentationPoints.size();z++){
						countMRSegmentationPoints += mrSegmentationPoints[z].count();
						for (int y=0;y<simImage->ny();y++)
							for (int x=0;x<simImage->nx();x++){
								if (mrSegmentationPoints[z].contains(QPoint(x,y))){
									expectedImageValue = 200;
									countFilledRegion++;
								}else
									expectedImageValue = 40;
								if ( qAbs(simImage->data()[4][z][y][x] - expectedImageValue) > 5 ) // +/- 5 was the added random variation added to lesion/non-lesion voxels values
									segmentationCorrect = false;
							}
					}

#if useDebug
					qDebug()<<"# of voxels in segmentation filled region:"<<countFilledRegion;
#endif
					//QIVERIFY2(segmentationCorrect,"segmentation filled region does not match simulated lesion");
					QIVERIFY2(segmentationCorrect, "Test the segmentation result");
					QICOMPARE2(countMRSegmentationPoints,countFilledRegion, "Test the number of voxels in segmentation");

#if saveLesionImages
					for (int z=0;z<mrSegmentation.size();z++){
						if (mrSegmentation[z].first().size() > 0){
							QImage sliceImage = simImage->getSlice(z,3);
							for(const auto & region : std::as_const(mrSegmentation[z]))
								for(const auto & vertex : std::as_const(region))
									sliceImage.setPixel(vertex,QColor("red").rgb());
							if (z == centerPoint.z())
								sliceImage.setPixel(centerPoint.toPoint(),QColor("green").rgb());
							sliceImage.save("mranalysis_test/seg"+sliceImage.text("description")+"_S"+sliceImage.text("slice")+"_"+sliceImage.text("time")+".png","PNG");
						}
					}
#endif
					QIVERIFY2(true, "Calculate the lesion features");
					mrAnalysis->calculateFeatures(QVector<uint>(2, 5)); //Use two c-means run with 5 bins
					QVector<double> features = mrAnalysis->model()->getFeatures();
#if useDebug
					qDebug() << "features size:" << features.size();
#endif
					double volume = features[8];
					double surfaceArea = features[9];
					double expectedVolume = 4.0/3.0 * M_PI *radius*radius*radius; // volume of sphere with r=5mm
					double expectedSurfaceArea = 4.0* M_PI *radius*radius    ;// surface area of sphere with r=5mm
					double errorTolerance = 0.01;
#if useDebug
					qDebug()<<"Calculated: Volume: "<<volume<<" (mm^3), Surface Area: "<<surfaceArea<<" (mm^2)";
					qDebug()<<"Expected: Volume: "<<expectedVolume<<" (mm^3), Surface Area: "<<expectedSurfaceArea<<" (mm^2)";
#endif
					QIVERIFY2(qAbs(volume-expectedVolume)/expectedVolume < errorTolerance, QString("Test calculated volume of simulated lesion is within %1% tolerance").arg(errorTolerance*100).toUtf8().data());
					QIVERIFY2(qAbs(surfaceArea-expectedSurfaceArea)/expectedSurfaceArea < errorTolerance, QString("Test calculated surface area of simulated lesion is within %1% tolerance").arg(errorTolerance*100).toUtf8().data());
				}
	}
}
