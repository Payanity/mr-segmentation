#ifndef SEGANALYSISTEST_H
#define SEGANALYSISTEST_H

#include "testmriimage.h"
#include "mranalysistest.h"
#include "qitestcase.h"
#include <QtTest/QtTest>
#include <QObject>


class SegAnalysisTest : public QObject
{
	Q_OBJECT

public:
	QSharedPointer<TestMriImage> image;
	QSharedPointer<MrAnalysisTest> mrAnalysis;
	QString dbPath;
    void clearLesionDBTables();
	void verifyLesionsDeleted();

private slots:
	void initTestCase() { dbPath = "C:\\QuantX\\quantx-15cases_postwtdbo_kondo_converted.db"; image.reset(new TestMriImage, &TestMriImage::deleteLater); mrAnalysis.reset(new MrAnalysisTest(std::make_shared<MRLesionModel>(image.data())), &MrAnalysisTest::deleteLater); clearLesionDBTables(); QIWARN("TMRA 1"); QIVERIFY2(true, "Test Initialization"); }
    void segComponentTest();
    //test the functions in morphological.h
    void segmentationTest();
    void analysisTest();
    void segmentSimulatedLesionTest();
    void binarycubeTest();
    void lesionDBTest();
	void mrLesionModelTest();
	void cleanupTestCase() { mrAnalysis.clear();image.clear(); clearLesionDBTables(); QIWARN("TMRA 9"); QIVERIFY2(true, "Test Cleanup"); verifyLesionsDeleted(); }
    
};

#endif // SEGANALYSISTEST_H
