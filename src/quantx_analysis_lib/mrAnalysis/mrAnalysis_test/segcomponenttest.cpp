#include "seganalysistest.h"
#include "cmeans.h"

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

#define writeCMeans 0

void SegAnalysisTest::segComponentTest()
{
    QIWARN("TMRA 2");
    QVector<QPoint> lesion2D;
    QVector<bool> borderTouch;
    QVector<int> growcount(4,1);
    lesion2D << QPoint(5,5) << QPoint(6,6);

    borderTouch = MrAnalysis::checkBorders(QRect(5,5,1,1),lesion2D);
    for(int i=0; i < borderTouch.size(); i++)
        QIVERIFY2(borderTouch[i], QString("Test checkBorders() output for 2D lesion touching all sides of bounding box, side %1").arg(i).toUtf8().data());
    QIVERIFY2(MrAnalysis::updateGrowCount(&growcount,borderTouch), "Test check of whether border needs expanding");
    for(int i = 0; i < growcount.size(); i++)
        QICOMPARE2(growcount[i],0, QString("Test grow counts reset for all sides, side %1").arg(i).toUtf8().data());
    QIVERIFY2(true, "Sufficiently expand bounding box");
    borderTouch = MrAnalysis::checkBorders(QRect(4,4,4,4),lesion2D);
    for(int i=0; i < borderTouch.size(); i++)
        QIVERIFY2(!borderTouch[i], QString("Test checkBorders() output for 2D lesion not touching any sides of bounding box, side %1").arg(i).toUtf8().data());
    growcount.fill(1);
    QIVERIFY2(!MrAnalysis::updateGrowCount(&growcount,borderTouch), "Test check of whether border needs expanding");
    QIVERIFY2(true, "Set flag for one side of bounding box that the lesion touches");
    borderTouch[3] = true;
    for(int n=1;n<10;n++){
        QIVERIFY2(MrAnalysis::updateGrowCount(&growcount,borderTouch), QString("Test check of whether border needs expanding, iteration %1").arg(n).toUtf8().data());
        for(int i=0; i < 3; i++)
            QICOMPARE2(growcount[i],0, QString("Test grow count of bounding box side not touching lesion, iteration %1, side %2").arg(n).arg(i).toUtf8().data());
        QICOMPARE2(growcount[3],n, QString("Test grow count of bounding box side touching lesion, iteration %1, side 3").arg(n).toUtf8().data());
    }

    lesion2D << QPoint(8,3) << QPoint(3,8);

#if useDebug
    qDebug() << MrAnalysis::regionCentroid(lesion2D);
    qDebug() << MrAnalysis::regionEllipse(lesion2D, 1.0, 1.0);
#endif
    QICOMPARE2(MrAnalysis::regionCentroid(lesion2D),QPointF(5.5,5.5), "Test calculation of region centroid");
    QVector<double> ellipseParameters1;
    //ellipseParameters1 << 10.0664 << 2.3094 << 0.973329 << -0.785398;
    ellipseParameters1 << 7.07106781187
                       << 1.41421356237
                       << 0.979795897113
                       << -0.785398163397;

    QVector<double> ellipseParameters2 = MrAnalysis::regionEllipse(lesion2D, 1.0, 1.0);
    for(int i=0;i<4;i++)
        QICOMPARE2(QTest::toString(ellipseParameters2[i]),
                   QTest::toString(ellipseParameters1[i]), "Test lesion ellipse-fit parameter");

    QICOMPARE2(MrAnalysis::regionSize(QVector<QVector<QPoint> >(10,lesion2D)), 40L, "Test size of lesion in voxels");
    QIVERIFY2(true, "Generate 10x10 data array");
    QVector<QVector<double> >data(10,QVector<double>(10));
    for(int i=1;i<11;i++)
        for(int j=0;j<10;j++)
            data[i-1][j] = i * (1 + (j * 0.5));
    QIVERIFY2(MrAnalysis::normalizeByFirstColumn(&data), "Test data normalization completed");
    for(int i=0;i<10;i++){
        QVector<double> expectedVector;
        for(int j=1;j<10;j++){
            //QICOMPARE(data[i][j-1], 1 + (j * 0.5));
            expectedVector << 1 + (j * 0.5);
        }
        QICOMPARE2(data[i], expectedVector, QString("Test normalized data value, row %1").arg(i).toUtf8().data());
        //values verified, not modify for cMeans
        for(int j=1;j<10;j++)
            data[i][j-1] *= 3.75*i*i + 2.1*i*j - 2.35*j*j - 1.1*i + 2.8*j;
    }

    QVector<QVector<QVector<double> > > cmeansResults;
    QVector<double> weights(9,1.0);
    QVector<uint> nBins;
    nBins << 5 << 10 << 10;
    QVERIFY2(true, "Run c-means on data with different c-means parameters");
    QVERIFY2(true, "Different # of clusters");
    QVERIFY2(true, "Different random seeds");
    QVERIFY2(true, "Different exponents");
    QVERIFY2(true, "Different stopping criterion");
    cmeansResults << cMeans(data, 3, weights, 2.0, 100, 0.00001,    false,5,      nullptr, nBins); //try different cluster sizes
    cmeansResults << cMeans(data, 2, weights, 2.0, 100, 0.00001,    false,5,      nullptr, nBins);
    cmeansResults << cMeans(data, 3, weights, 2.0, 100, 0.00001,    true, 5,      nullptr, nBins); //output objective functions
    cmeansResults << cMeans(data, 2, weights, 2.0, 100, 0.00001,    true, 5,      nullptr, nBins);
    cmeansResults << cMeans(data, 3, weights, 2.0, 100, 0.00001,    true, 0,      nullptr, nBins); //try different rand seeds
    cmeansResults << cMeans(data, 2, weights, 2.0, 100, 0.00001,    true, 0,      nullptr, nBins);
    cmeansResults << cMeans(data, 3, weights, 2.0, 100, 0.00001,    true, 123456, nullptr, nBins);
    cmeansResults << cMeans(data, 2, weights, 2.0, 100, 0.00001,    true, 123456, nullptr, nBins);
    cmeansResults << cMeans(data, 3, weights, 2.0, 100, 0.00001,    true, 9999,   nullptr, nBins);
    cmeansResults << cMeans(data, 2, weights, 2.0, 100, 0.00001,    true, 9999,   nullptr, nBins);
    cmeansResults << cMeans(data, 3, weights, 1.5, 100, 0.00001,    true, 5,      nullptr, nBins); //try different exponents
    cmeansResults << cMeans(data, 2, weights, 1.5, 100, 0.00001,    true, 5,      nullptr, nBins);
    cmeansResults << cMeans(data, 3, weights, 3.0, 100, 0.00001,    true, 5,      nullptr, nBins);
    cmeansResults << cMeans(data, 2, weights, 3.0, 100, 0.00001,    true, 5,      nullptr, nBins);
    cmeansResults << cMeans(data, 3, weights, 0.5, 100, 0.00001,    true, 5,      nullptr, nBins); //bad exponent
    cmeansResults << cMeans(data, 3, weights, 2.0, 100, 0.00000001, true, 5,      nullptr, nBins); //try different min improves
    cmeansResults << cMeans(data, 2, weights, 2.0, 100, 0.00000001, true, 5,      nullptr, nBins);
    cmeansResults << cMeans(data, 3, weights, 2.0, 100, 0.1    ,    true, 5,      nullptr, nBins);
    cmeansResults << cMeans(data, 2, weights, 2.0, 100, 0.1    ,    true, 5,      nullptr, nBins);

#if writeCMeans
    QFile writeFile;
    writeFile.setFileName("mranalysis_test/cmeansExpected.dat");
    writeFile.open(QIODevice::WriteOnly);
    QDataStream out(&writeFile);
    out << cmeansResults;
    writeFile.close();
#endif

    QFile readFile;
    readFile.setFileName("mranalysis_test/cmeansExpected.dat");
    readFile.open(QIODevice::ReadOnly);
    QDataStream in(&readFile);
	in.setVersion(16);
    QVector<QVector<QVector<double> > > cmeansExpected;
    in >> cmeansExpected;
    readFile.close();
    QIVERIFY2(!cmeansExpected.isEmpty(), "Test that expected c-means results have been read");

    QICOMPARE2(cmeansResults.size(), cmeansExpected.size(), "Verify Number of CMeans Results");
	for(int i = 0; i < cmeansResults.size(); i++){
#if useDebug
		qDebug() << "i =" << i;
#endif
        QICOMPARE2(cmeansResults[i].size(),cmeansExpected[i].size(), QString("Verify Number of Vectors in CMeans Test %1").arg(i + 1).toUtf8().data() );
		for(int j = 0; j < cmeansResults[i].size(); j++){
#if useDebug
			qDebug() << "j =" << j;
			qDebug() << "ACTUAL:" << cmeansResults[i][j];
			qDebug() << "EXPECTED:" << cmeansExpected[i][j];
#endif
            QICOMPARE2(cmeansResults[i][j].size(), cmeansExpected[i][j].size(),  QString("Verify Vector size of CMeans result, Test %1, Vector %2").arg(i + 1).arg(j).toUtf8().data() );
			for(int k = 0; k < cmeansResults[i][j].size(); k++){
#if useDebug
				//qDebug() << QString("(%1,%2,%3) result:%4 expected:%5").arg(i).arg(j).arg(k).arg(cmeansResults[i][j][k]).arg(cmeansExpected[i][j][k]);
#endif
                QICOMPARE2(static_cast<float>(cmeansResults[i][j][k]), static_cast<float>(cmeansExpected[i][j][k]), QString("Compare CMeans output value, Test %1, Vector %2, Element %3").arg(i + 1).arg(j).arg(k).toUtf8().data() );
            }
        }
    }

// This fails for some reason even though testing each value individually above passes
//    for(int i=0; i<cmeansResults.size();i++){
//        for (int j=0; j < cmeansResults[i].size();j++){
//#if useDebug
//            qDebug() << QString("Results (%1,%2): ").arg(i).arg(j) << cmeansResults[i][j];
//            qDebug() << QString(" Expected (%1,%2): ").arg(i).arg(j) << cmeansExpected[i][j];
//#endif
//            QCOMPARE(cmeansResults[i][j], cmeansExpected[i][j]);
//        }
//    }
}
