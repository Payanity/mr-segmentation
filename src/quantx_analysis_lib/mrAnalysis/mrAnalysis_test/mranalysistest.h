#ifndef MRANALYSISTEST_H
#define MRANALYSISTEST_H

#include "mranalysis.h"

class MrAnalysisTest : public MrAnalysis
{
    Q_OBJECT

public:
	MrAnalysisTest(std::shared_ptr<MRLesionModel> model, QObject *parent = nullptr) : MrAnalysis(model,parent) {}
    void setSegmentationDone(bool b) { model_->segmentationDone_ = b; }
    void setAnalysisDone(bool b) { model_->analysisDone_ = b; }
    QPoint getCenter() { return QPoint(model_->xSeed, model_->ySeed); }
    int getZStart() { return model_->zSeed; }
    //virtual void setSegmentation(const QVector<QVector<QPolygon> > &s) { segmentation = s; }
    virtual void setRegionData(const QVector<QVector<double> > &r) { model_->regionData = r; }
	std::shared_ptr<MRLesionModel> model(){ return model_; }
};

#endif // MRANALYSISTEST_H
