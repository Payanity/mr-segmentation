#include "seganalysistest.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setAttribute(Qt::AA_Use96Dpi, true);
    QTEST_DISABLE_KEYPAD_NAVIGATION
    SegAnalysisTest tc;
#ifdef SE_VERSION_TEST
    return QITest::qiExec(&tc, argc, argv, "mranalysis_test_510K/qi_mranalysis_testoutput.xml");
#else
    return QITest::qiExec(&tc, argc, argv, "mranalysis_test/qi_mranalysis_testoutput.xml");
#endif
}

//#include "moc_seganalysistest.cpp"
