#include "seganalysistest.h"
#include "mrlesionmodel.h"


void SegAnalysisTest::mrLesionModelTest()
{
	QIWARN("TMRA 8");
	auto model = mrAnalysis->model();
	
	//Read expected data
	int segNumber = 1;
	QString filename = "sql_test/00050-Motion Corr. dyn_eTHRIVE_448-488_125-167_130-169.dat";
	QFile readFile;
	readFile.setFileName(QString(filename + "-seg%1").arg(segNumber));
	readFile.open(QIODevice::ReadOnly);
	QDataStream in(&readFile);
	QVector<QVector<QPolygon> > segmentation;
	in >> segmentation;
	readFile.close();
	
	readFile.setFileName(QString(filename + "-seg%1features").arg(segNumber));
	readFile.open(QIODevice::ReadOnly);
	QVector<double> goodFeatures;
	in >> goodFeatures;
	readFile.close();
	
	QICOMPARE2(model->mrn(), image->mrn(), "Compare MRN of lesion model with expected value");
	QICOMPARE2(model->studyUID(), image->studyUID(), "Compare MRN of lesion model with expected value");
	QICOMPARE2(model->accessionNumber(), image->accessionNumber(), "Compare accession number of lesion model with expected value");
	QICOMPARE2(model->getTimestamps(), image->getTimestamps(), "Compare seriesUID of lesion model with expected value");
	QICOMPARE2(model->orientation(), image->acquisitionOrientation(), "Compare orientation of lesion model with expected value");
	QICOMPARE2(model->precontrastIdx(), image->precontrastIdx(), "Compare precontrast index of lesion model with expected value");
	QICOMPARE2(model->getTimestamps(), image->getTimestamps(), "Compare timestamps of lesion model with expected value");
	QICOMPARE2(model->getInjectionTime(), image->injectionTime(), "Compare contrast injection time of lesion model with expected value");
	QIVERIFY2(model->segmentationDone(), "Verify lesion model reports that segmentation has been completed");
	QIVERIFY2(model->analysisDone(), "Verify lesion model reports that MR lesion analysis has been completed");
	auto modelFeatures = model->getFeatures();
	for( auto i= 0; i < goodFeatures.size(); i++){
		QICOMPARE2(static_cast<float>(modelFeatures.at(i)), static_cast<float>(goodFeatures.at(i)), QString("Compare model features value %1 with expected").arg(i).toUtf8().data());
	}
//	QICOMPARE2(model->getFeatures(), goodFeatures, "Compare lesion features of lesion model with expected values");
	QICOMPARE2(model->getSegmentation(), segmentation, "Compare segmentation of lesion model with expected segmentation");

}
