#include "seganalysistest.h"
#include "lesiondatabase.h"
#include "model/lesion.h"
#include "model/lesiondetails.h"
#include "databasehelper.h"
#include "Wt/WDateTime.h"

#define writeSeg 0
#define writePreviewSeg 0

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

void SegAnalysisTest::segmentationTest()
{
	QIWARN("TMRA 3");
	QIVERIFY2(true, "Load MR image data and run segmentation");
	QString filename = "sql_test/00050-Motion Corr. dyn_eTHRIVE_448-488_125-167_130-169.dat";
	image->readMRTestData(filename);
	auto previewAnalysis = QScopedPointer<MrAnalysisTest>(new MrAnalysisTest(std::make_shared<MRLesionModel>(image.data())));
	auto previewSegmentation = previewAnalysis->segmentSinglePoint(PointVector3D<int>(20, 21, 18), true);
	int previewSegmentedNumSlices = 0;
	for (auto slice : previewSegmentation){
		for (auto polygon : slice){
			if (!polygon.isEmpty()){
				previewSegmentedNumSlices++;
				break;
			}
		}
	}
#if writePreviewSeg
	QFile writeFile;
	writeFile.setFileName(QString(filename + "-segPreviewactual"));
	if(writeFile.open(QIODevice::WriteOnly)){
		QDataStream out(&writeFile);
		out << previewSegmentation;
		writeFile.close();
	}

	writeFile.setFileName(QString(filename + "-segFilledPreviewactual"));
	if(writeFile.open(QIODevice::WriteOnly)){
		QDataStream out(&writeFile);
		out << previewAnalysis->getFilledRegion(QUANTX::XYPLANE);
		writeFile.close();
	}
#endif
	//Test of the preview segmentation
	QICOMPARE2(previewSegmentedNumSlices, 1, "Verify only one slice is segmented for the preview segmentation");
	QIVERIFY2(true, "Read expected preview segmentation boundary data");
	QFile readFile;
	readFile.setFileName(QString(filename + "-segPreviewactual"));
	readFile.open(QIODevice::ReadOnly);
	QDataStream in(&readFile);
	in.setVersion(16);
	QVector<QVector<QPolygon> > goodPreviewSegmentation;
	in >> goodPreviewSegmentation;
	readFile.close();
	QIVERIFY2(!goodPreviewSegmentation.isEmpty(), "Test that expected preview segmentation data has been read");
#if useDebug
	qDebug() << "EXPECTED:" << goodPreviewSegmentation;
	qDebug() << "ACTUAL:" << previewSegmentation;
#endif
	QICOMPARE2(previewSegmentation, goodPreviewSegmentation, "Test preview segmentation boundary");

	QIVERIFY2(true, "Read expected preview segmentation filled region data");
	readFile.setFileName(QString(filename + "-segFilledPreviewactual"));
	readFile.open(QIODevice::ReadOnly);
	QVector<QVector<QPoint> > goodPreviewFill;
	in >> goodPreviewFill;
	readFile.close();
	QIVERIFY2(!goodPreviewFill.isEmpty(), "Test that expected preview segmentation filled region data has been read");
	QICOMPARE2(previewAnalysis->getFilledRegion(QUANTX::XYPLANE), goodPreviewFill, "Test segmentation filled region");
// end test of preview segmentation

	mrAnalysis.reset(new MrAnalysisTest(std::make_shared<MRLesionModel>(image.data())), &MrAnalysisTest::deleteLater);
	QVector<QVector<QPolygon> > mrSegmentation = mrAnalysis->segmentSinglePoint({20, 21, 18});
	mrAnalysis->calculateFeatures(QVector<uint>(1, 5)); //Use one c-means run with 5 bins
	QICOMPARE2(mrSegmentation.size(), 40, "Test number of slices of segmentation");

	int segNumber = 1;

#if writeSeg
	QFile writeFile;
	writeFile.setFileName(QString(filename + "-seg%1actual").arg(segNumber));
	if(writeFile.open(QIODevice::WriteOnly)){
		QDataStream out(&writeFile);
		out << mrSegmentation;
		writeFile.close();
	}

	writeFile.setFileName(QString(filename + "-seg%1filledactual").arg(segNumber));
	if(writeFile.open(QIODevice::WriteOnly)){
		QDataStream out(&writeFile);
		out << mrAnalysis->getFilledRegion(QUANTX::XYPLANE);
		writeFile.close();
	}
#endif
	QIVERIFY2(true, "Read expected segmentation boundary data");
	readFile.setFileName(QString(filename + "-seg%1").arg(segNumber));
	readFile.open(QIODevice::ReadOnly);
	//QDataStream in(&readFile);
	in.setVersion(16);
	QVector<QVector<QPolygon> > goodSegmentation;
	in >> goodSegmentation;
	readFile.close();
	QIVERIFY2(!goodSegmentation.isEmpty(), "Test that expected segmentation data has been read");
#if useDebug
	qDebug() << "EXPECTED:" << goodSegmentation;
	qDebug() << "ACTUAL:" << mrSegmentation;
#endif
	QICOMPARE2(mrSegmentation, goodSegmentation, "Test segmentation boundary");

	QIVERIFY2(true, "Read expected segmentation filled region data");
	readFile.setFileName(QString(filename + "-seg%1filled").arg(segNumber));
	readFile.open(QIODevice::ReadOnly);
	QVector<QVector<QPoint> > goodFill;
	in >> goodFill;
	readFile.close();
	QIVERIFY2(!goodFill.isEmpty(), "Test that expected segmentation filled region data has been read");
	QICOMPARE2(mrAnalysis->getFilledRegion(QUANTX::XYPLANE), goodFill, "Test segmentation filled region");
	QStringList dbSettings{"QSQLITE", "", dbPath, "", ""};

	QFile file("data_test/mrknowncases.dat");
	if (!file.open(QIODevice::ReadOnly)){
		qDebug() << "Could not open know cases data file";
	}
	qDebug() << "QI value: " << mrAnalysis->model()->getFeatures().at(0);
	QVector<const KnownCase *> knownCases = DatabaseHelper::decodeKnownCaseHelper(file.readAll());
	auto lesionDB = std::make_unique<QuantXLesionDatabase>(dbSettings);
	lesionDB->setConnectionProperty("show-queries", "true");
	// Generating the known cases should be outside the scope of this test. Instead, we should use staic KnownCase objects for the lesion database test
	auto mraUUID = lesionDB->addLesionToDatabase(static_cast<QiMriImage *>(image.data()), mrAnalysis->model(), knownCases);
	mrAnalysis->model()->setUuid(mraUUID); // compare with value in database in lesiondbtest()

}
