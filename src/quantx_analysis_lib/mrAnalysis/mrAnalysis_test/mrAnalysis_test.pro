#-------------------------------------------------
#
# Project created by QtCreator 2012-07-20T18:07:51
#
#-------------------------------------------------

QT       += core testlib sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets concurrent
TARGET = mranalysis_test
DEPENDPATH += .
MRIV_ROOT = ../../..
QI_TESTLIB_PATH = $${MRIV_ROOT}/src/global/testlib

include( $${MRIV_ROOT}/quantxconfig.pri )
include( $${MRIV_ROOT}/src/directoryconfig.pri )

INCLUDEPATH += . \
               $${MRIV_ROOT}/include \
               $${MRIV_ROOT}/src/qisql/qisql_lib \
               $${MRIV_ROOT}/src/quantxdata/quantxdata_lib \
               $${MRIV_ROOT}/include/simplecrypt \
               $${WT_PATH}/include \
               $${MRIV_ROOT}/include/jama \
               $${MRIV_ROOT}/src/mrAnalysis/mrAnalysis_lib \
               $${MRIV_ROOT}/src/qisql/qisql_test \
               $${MRIV_ROOT}/src/quantxdata/quantxdata_test \
               $${QI_TESTLIB_PATH}

MOC_DIR           = moc
OBJECTS_DIR       = obj
DESTDIR           = $${MRIV_ROOT}/bin

macx {
    LIBS += -framework IOKit \
            -framework CoreFoundation
}

LIBS += -L$${WT_PATH}/lib
CONFIG(debug, debug|release) {
    #TARGET = $$join(TARGET,,,_debug)
    LIBS += -L$${MRIV_ROOT}/lib -lmranalysis_debug \
            -L$${MRIV_ROOT}/lib -lqisql_debug \
            -L$${WT_PATH}/lib -lwtdbod -lwtdbosqlite3d -lwtdbopostgresd

}

CONFIG(release, debug|release) {
    LIBS += -L$${MRIV_ROOT}/lib -lmranalysis \
            -L$${MRIV_ROOT}/lib -lqisql \
            -L$${WT_PATH}/lib -lwtdbo -lwtdbosqlite3 -lwtdbopostgres
}

HEADERS += \
    ../../qisql/qisql_test/testmriimage.h \
    seganalysistest.h \
    mranalysistest.h

SOURCES += \
    ../../qisql/qisql_test/testmriimage.cpp \
    segmentationtest.cpp \
    analysistest.cpp \
    seganalysistest.cpp \
    segcomponenttest.cpp \
    segmentsimulatedlesiontest.cpp \
    binarycubetest.cpp \
    lesiondbtest.cpp \
    $${MRIV_ROOT}/include/simplecrypt/simplecrypt.cpp

#Global testlib sources

SOURCES += \
    $${QI_TESTLIB_PATH}/qitestresult.cpp \
    $${QI_TESTLIB_PATH}/qitestcase.cpp \
    $${QI_TESTLIB_PATH}/qixmltestlogger.cpp \
    $${QI_TESTLIB_PATH}/qitestlog.cpp \
    $${QI_TESTLIB_PATH}/private/qabstracttestlogger.cpp \
    $${QI_TESTLIB_PATH}/private/qbenchmarkmetric.cpp
