#include "qifunctions.h"

// these are from robfuncs.cpp
//
//these functions have no class and are frequently used so we will define them here
//so that we don't re-define them over and over again

#if USE_DEPRECATED_QIFUNCTIONS

inline double qiRound(double number)
{
  return number < 0.0 ? ceil(number - 0.5) : floor(number + 0.5);
}

// Use the OpenMP versions if it is available
#if defined(_OPENMP)
#pragma message("Using OpenMP version of min/max functions")

inline QPointF max(QVector<double> vector){
    //This function returns a QPointF where the x dimension is the
    //maximum value and the y dimension is the first index at which
    //the maximum value of the input vector is located
    float m = vector[0];
    float loc = 0;

#pragma omp parallel
    {
        float priv_max = m;
        float priv_loc = loc;
        #pragma omp for
        for(int i = 1; i < vector.size(); ++i)
            if(vector[i] > priv_max){
                priv_max = vector[i];
                priv_loc = i;
            }
        #pragma omp flush (m)
        #pragma omp flush (loc)
        if(priv_max > m){
            #pragma omp critical
                {
                    if(priv_max > m){
                        m = priv_max;
                        loc = priv_loc;
                    }
                }
        }
    }
    return QPointF(m,loc);
}

inline QPointF min(QVector<double> vector){
    //This function returns a QPointF where the x dimension is the
    //minimum value and the y dimension is the first index at which
    //the minimum value of the input vector is located
    float m = vector[0];
    float loc = 0;

#pragma omp parallel
    {
        float priv_min = m;
        float priv_loc = loc;
        #pragma omp for
        for(int i = 1; i < vector.size(); ++i)
            if(vector[i] < priv_min){
                priv_min = vector[i];
                priv_loc = i;
            }
        #pragma omp flush (m)
        #pragma omp flush (loc)
        if(priv_min < m){
            #pragma omp critical
                {
                    if(priv_min < m){
                        m = priv_min;
                        loc = priv_loc;
                    }
                }
        }
    }
    return QPointF(m,loc);
}

inline QPoint max(QVector<int> vector){
    //This function returns a QPoint where the x dimension is the
    //maximum value and the y dimension is the first index at which
    //the maximum value of the input vector is located
    int m = vector[0];
    int loc = 0;

#pragma omp parallel
    {
        int priv_max = m;
        int priv_loc = loc;
        #pragma omp for
        for(int i = 1; i < vector.size(); ++i)
            if(vector[i] > priv_max){
                priv_max = vector[i];
                priv_loc = i;
            }
        #pragma omp flush (m)
        #pragma omp flush (loc)
        if(priv_max > m){
            #pragma omp critical
                {
                    if(priv_max > m){
                        m = priv_max;
                        loc = priv_loc;
                    }
                }
        }
    }
    return QPoint(m,loc);
}

inline QPoint min(QVector<int> vector){
    //This function returns a QPoint where the x dimension is the
    //minimum value and the y dimension is the first index at which
    //the minimum value of the input vector is located
    int m = vector[0];
    int loc = 0;

#pragma omp parallel
    {
        int priv_min = m;
        int priv_loc = loc;
        #pragma omp for
        for(int i = 1; i < vector.size(); ++i)
            if(vector[i] < priv_min){
                priv_min = vector[i];
                priv_loc = i;
            }
        #pragma omp flush (m)
        #pragma omp flush (loc)
        if(priv_min < m){
            #pragma omp critical
                {
                    if(priv_min < m){
                        m = priv_min;
                        loc = priv_loc;
                    }
                }
        }
    }
    return QPoint(m,loc);
}

#else
#pragma message("Using single-threaded version of min/max functions")

inline QPointF max(QVector<double> vector){
    //This function returns a QPointF where the x dimension is the
    //maximum value and the y dimension is the first index at which
    //the maximum value of the input vector is located
    float m = vector[0];
    float loc = 0;
    for(int i=1;i<vector.size();i++)
        if(vector[i] > m){
            m = vector[i];
            loc = i;
        }
    return QPointF(m,loc);
}

inline QPointF min(QVector<double> vector){
    //This function returns a QPointF where the x dimension is the
    //minimum value and the y dimension is the first index at which
    //the minimum value of the input vector is located
    float m = vector[0];
    float loc = 0;
    for(int i=1;i<vector.size();i++)
        if(vector[i] < m){
            m = vector[i];
            loc = i;
        }
    return QPointF(m,loc);
}

inline QPoint max(QVector<int> vector){
    //This function returns a QPoint where the x dimension is the
    //maximum value and the y dimension is the first index at which
    //the maximum value of the input vector is located
    int m = vector[0];
    int loc = 0;
    for(int i=1;i<vector.size();i++)
        if(vector[i] > m){
            m = vector[i];
            loc = i;
        }
    return QPoint(m,loc);
}

inline QPoint min(QVector<int> vector){
    //This function returns a QPoint where the x dimension is the
    //minimum value and the y dimension is the first index at which
    //the minimum value of the input vector is located
    int m = vector[0];
    int loc = 0;
    for(int i=1;i<vector.size();i++)
        if(vector[i] < m){
            m = vector[i];
            loc = i;
        }
    return QPoint(m,loc);
}

#endif

inline int min(int a, int b){
    return (a<b) ? a : b;
}

inline int max(int a, int b){
    return (a>b) ? a : b;
}

#endif

int roundUpToMultiple(int x, int multiple)
{
    auto a = x + multiple - 1;
    return a - a % multiple;
}

// alternative to QTest::qWait
#ifdef Q_OS_WIN
#include <Windows.h> // for Sleep
#endif
void qiSleep(int ms)
{
#ifdef Q_OS_WIN
    Sleep(uint(ms));
#else
    struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
    nanosleep(&ts, NULL);
#endif
}
