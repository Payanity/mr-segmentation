#include "mrfeatures.h"
#include <QBitArray>
#include <functional>
#include "pointvector3d.h"

#define USE_SCHARR_OPERATOR 1

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

QVector<double> morphologyFeats(QVector<int>* xData, QVector<int>* yData, QVector<int>* zData, const QVector<QVector<QVector<double> > >* data, double dmin)
{
    int numPoints = xData->size();

    int minx = 0;
    int miny = 0;
    int minz = 0;

    long xSize = data->size();
    long ySize = data->last().size();
    long zSize = data->last().last().size();

    QVector<QVector<QBitArray> > lesionMask(xSize,QVector<QBitArray>(ySize,QBitArray(zSize,false)));
    QVector<QVector<QBitArray> > marginMask(xSize,QVector<QBitArray>(ySize,QBitArray(zSize,false)));
    QVector<QVector<QBitArray> > borderMask(xSize,QVector<QBitArray>(ySize,QBitArray(zSize,false)));

    long sumx = 0;
    long sumy = 0;
    long sumz = 0;
    QVector<double> gradx(numPoints), grady(numPoints), gradz(numPoints);
    QVector<double> grad3D(numPoints), grad3Dsquared(numPoints);
//#pragma omp parallel for reduction(+: sumx, sumy, sumz)
    for(int i = numPoints - 1; i >= 0; i--){
        auto loc = PointVector3D<int>(xData->at(i) - minx, yData->at(i) - miny, zData->at(i) - minz);
        if( (loc.x() + 1 < xSize) && (loc.y() + 1 < ySize) && (loc.z() + 1 < zSize) ){
            sumx += loc.x();
            sumy += loc.y();
            sumz += loc.z();
#if USE_SCHARR_OPERATOR
            gradx[i] = 100 * ( (*data)[loc.x()-1][loc.y()]  [loc.z()]   - (*data)[loc.x()+1][loc.y()]  [loc.z()]   ) +
                        30 * ( (*data)[loc.x()-1][loc.y()+1][loc.z()]   - (*data)[loc.x()+1][loc.y()+1][loc.z()]   ) +
                        30 * ( (*data)[loc.x()-1][loc.y()-1][loc.z()]   - (*data)[loc.x()+1][loc.y()-1][loc.z()]   ) +
                        30 * ( (*data)[loc.x()-1][loc.y()]  [loc.z()+1] - (*data)[loc.x()+1][loc.y()]  [loc.z()+1] ) +
                         9 * ( (*data)[loc.x()-1][loc.y()+1][loc.z()+1] - (*data)[loc.x()+1][loc.y()+1][loc.z()+1] ) +
                         9 * ( (*data)[loc.x()-1][loc.y()-1][loc.z()+1] - (*data)[loc.x()+1][loc.y()-1][loc.z()+1] ) +
                        30 * ( (*data)[loc.x()-1][loc.y()]  [loc.z()-1] - (*data)[loc.x()+1][loc.y()]  [loc.z()-1] ) +
                         9 * ( (*data)[loc.x()-1][loc.y()+1][loc.z()-1] - (*data)[loc.x()+1][loc.y()+1][loc.z()-1] ) +
                         9 * ( (*data)[loc.x()-1][loc.y()-1][loc.z()-1] - (*data)[loc.x()+1][loc.y()-1][loc.z()-1] ) ;

            grady[i] = 100 * ( (*data)[loc.x()]  [loc.y()-1][loc.z()]   - (*data)[loc.x()]  [loc.y()+1][loc.z()]   ) +
                        30 * ( (*data)[loc.x()+1][loc.y()-1][loc.z()]   - (*data)[loc.x()+1][loc.y()+1][loc.z()]   ) +
                        30 * ( (*data)[loc.x()-1][loc.y()-1][loc.z()]   - (*data)[loc.x()-1][loc.y()+1][loc.z()]   ) +
                        30 * ( (*data)[loc.x()]  [loc.y()-1][loc.z()+1] - (*data)[loc.x()]  [loc.y()+1][loc.z()+1] ) +
                         9 * ( (*data)[loc.x()+1][loc.y()-1][loc.z()+1] - (*data)[loc.x()+1][loc.y()+1][loc.z()+1] ) +
                         9 * ( (*data)[loc.x()-1][loc.y()-1][loc.z()+1] - (*data)[loc.x()-1][loc.y()+1][loc.z()+1] ) +
                        30 * ( (*data)[loc.x()]  [loc.y()-1][loc.z()-1] - (*data)[loc.x()]  [loc.y()+1][loc.z()-1] ) +
                         9 * ( (*data)[loc.x()+1][loc.y()-1][loc.z()-1] - (*data)[loc.x()+1][loc.y()+1][loc.z()-1] ) +
                         9 * ( (*data)[loc.x()-1][loc.y()-1][loc.z()-1] - (*data)[loc.x()-1][loc.y()+1][loc.z()-1] ) ;

            gradz[i] = 100 * ( (*data)[loc.x()]  [loc.y()]  [loc.z()-1] - (*data)[loc.x()]  [loc.y()]  [loc.z()+1] ) +
                        30 * ( (*data)[loc.x()+1][loc.y()]  [loc.z()-1] - (*data)[loc.x()+1][loc.y()]  [loc.z()+1] ) +
                        30 * ( (*data)[loc.x()-1][loc.y()]  [loc.z()-1] - (*data)[loc.x()-1][loc.y()]  [loc.z()+1] ) +
                        30 * ( (*data)[loc.x()]  [loc.y()+1][loc.z()-1] - (*data)[loc.x()]  [loc.y()+1][loc.z()+1] ) +
                         9 * ( (*data)[loc.x()+1][loc.y()+1][loc.z()-1] - (*data)[loc.x()+1][loc.y()+1][loc.z()+1] ) +
                         9 * ( (*data)[loc.x()-1][loc.y()+1][loc.z()-1] - (*data)[loc.x()-1][loc.y()+1][loc.z()+1] ) +
                        30 * ( (*data)[loc.x()]  [loc.y()-1][loc.z()-1] - (*data)[loc.x()]  [loc.y()-1][loc.z()+1] ) +
                         9 * ( (*data)[loc.x()+1][loc.y()-1][loc.z()-1] - (*data)[loc.x()+1][loc.y()-1][loc.z()+1] ) +
                         9 * ( (*data)[loc.x()-1][loc.y()-1][loc.z()-1] - (*data)[loc.x()-1][loc.y()-1][loc.z()+1] ) ;
#else
            gradx[i] = (*data)[loc.x()][loc.y()][loc.z()] - (*data)[loc.x()+1][loc.y()][loc.z()];
            grady[i] = (*data)[loc.x()][loc.y()][loc.z()] - (*data)[loc.x()][loc.y()+1][loc.z()];
            gradz[i] = (*data)[loc.x()][loc.y()][loc.z()] - (*data)[loc.x()][loc.y()][loc.z()+1];
#endif
            grad3Dsquared[i] = pow(gradx.at(i),2) + pow(grady.at(i),2) + pow(gradz.at(i),2);
            grad3D[i] = sqrt(grad3Dsquared.at(i));
#if useDebug
            if(grad3D[i] > 100000){
                qDebug() << "At location" << i << "grad3D =" << grad3D[i] << "   :   gradx =" << gradx[i] << "grady =" << grady[i] << "gradz =" << gradz[i];
                qDebug() << "loc.x():" << loc.x() << "loc.y():" << loc.y() << "loc.z():" << loc.z();
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()-1][loc.z()-1],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()-1][loc.z()-1],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()-1][loc.z()-1],'f', 5);
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()][loc.z()-1],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()][loc.z()-1],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()][loc.z()-1],'f', 5);
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()+1][loc.z()-1],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()+1][loc.z()-1],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()+1][loc.z()-1],'f', 5);
                qDebug() << "-------------------------------------------------------------------------------------------------------------------------------------------";
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()-1][loc.z()],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()-1][loc.z()],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()-1][loc.z()],'f', 5);
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()][loc.z()],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()][loc.z()],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()][loc.z()],'f', 5);
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()+1][loc.z()],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()+1][loc.z()],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()+1][loc.z()],'f', 5);
                qDebug() << "-------------------------------------------------------------------------------------------------------------------------------------------";
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()-1][loc.z()+1],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()-1][loc.z()+1],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()-1][loc.z()+1],'f', 5);
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()][loc.z()+1],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()][loc.z()+1],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()][loc.z()+1],'f', 5);
                qDebug() << QString::number( (*data)[loc.x()-1][loc.y()+1][loc.z()+1],'f', 5) << "|" << QString::number( (*data)[loc.x()][loc.y()+1][loc.z()+1],'f', 5) << "|" << QString::number( (*data)[loc.x()+1][loc.y()+1][loc.z()+1],'f', 5);
            }
#endif
            //if(grad3D.at(i) == 0.0)
            //    grad3D[i] = 1.0e-18;
            //set first mask as region only
            lesionMask[loc.x()][loc.y()].setBit(loc.z(),true);
        }
        else{
            // This shouldn't happen, but just in case it does...
            xData->removeAt(i);
            yData->removeAt(i);
            zData->removeAt(i);
            numPoints -= 1;
        }
    }

    //now find boundary pixels
//#pragma omp parallel for
    for(int i=0; i<numPoints; i++){
        //only use region points as possible edge points
        auto loc = PointVector3D<int>(xData->at(i) - minx, yData->at(i) - miny, zData->at(i) - minz);

        //Search for two neighboring pixels in the 8-connected
        //neighborhood which are false to determine border pixel
        //This skips the inside of certain spiculated patterns, and seems intentional
        if(!lesionMask[loc.x()+1][loc.y()].testBit(loc.z())){
            if(!lesionMask[loc.x()+1][loc.y()+1].testBit(loc.z()) || !lesionMask[loc.x()+1][loc.y()-1].testBit(loc.z()) || !lesionMask[loc.x()+1][loc.y()].testBit(loc.z()+1) || !lesionMask[loc.x()+1][loc.y()].testBit(loc.z()-1)){
                borderMask[loc.x()][loc.y()].setBit(loc.z(),true);
                continue;
            }
        }
        if(!lesionMask[loc.x()-1][loc.y()].testBit(loc.z())){
            if(!lesionMask[loc.x()-1][loc.y()+1].testBit(loc.z()) || !lesionMask[loc.x()-1][loc.y()-1].testBit(loc.z()) || !lesionMask[loc.x()-1][loc.y()].testBit(loc.z()+1) || !lesionMask[loc.x()-1][loc.y()].testBit(loc.z()-1)){
                borderMask[loc.x()][loc.y()].setBit(loc.z(),true);
                continue;
            }
        }
        if(!lesionMask[loc.x()][loc.y()+1].testBit(loc.z())){
            if(!lesionMask[loc.x()+1][loc.y()+1].testBit(loc.z()) || !lesionMask[loc.x()-1][loc.y()+1].testBit(loc.z()) || !lesionMask[loc.x()][loc.y()+1].testBit(loc.z()+1) || !lesionMask[loc.x()][loc.y()+1].testBit(loc.z()-1)){
                borderMask[loc.x()][loc.y()].setBit(loc.z(),true);
                continue;
            }
        }
        if(!lesionMask[loc.x()][loc.y()-1].testBit(loc.z())){
            if(!lesionMask[loc.x()+1][loc.y()-1].testBit(loc.z()) || !lesionMask[loc.x()-1][loc.y()-1].testBit(loc.z()) || !lesionMask[loc.x()][loc.y()-1].testBit(loc.z()+1) || !lesionMask[loc.x()][loc.y()-1].testBit(loc.z()-1)){
                borderMask[loc.x()][loc.y()].setBit(loc.z(),true);
                continue;
            }
        }
        if(!lesionMask[loc.x()][loc.y()].testBit(loc.z()+1)){
            if(!lesionMask[loc.x()+1][loc.y()].testBit(loc.z()+1) || !lesionMask[loc.x()-1][loc.y()].testBit(loc.z()+1) || !lesionMask[loc.x()][loc.y()+1].testBit(loc.z()+1) || !lesionMask[loc.x()][loc.y()-1].testBit(loc.z()+1)){
                borderMask[loc.x()][loc.y()].setBit(loc.z(),true);
                continue;
            }
        }
        if(!lesionMask[loc.x()][loc.y()].testBit(loc.z()-1)){
            if(!lesionMask[loc.x()+1][loc.y()].testBit(loc.z()-1) || !lesionMask[loc.x()-1][loc.y()].testBit(loc.z()-1) || !lesionMask[loc.x()][loc.y()+1].testBit(loc.z()-1) || !lesionMask[loc.x()][loc.y()-1].testBit(loc.z()-1)){
                borderMask[loc.x()][loc.y()].setBit(loc.z(),true);
                continue;
            }
        }
    }

    //set marginMask as dognut (sic) lesion mask
    //once again, use only lesion points as possible edge points
    //and save new region as x,y,z vectors
    //Use 4-connectivity to keep information about the lesion, not surrounding tissue
    //QVector<PointVector3D<int> > points; // Do we even care about this???
    for(int i=0; i<numPoints; i++){
        auto loc = PointVector3D<int>(xData->at(i) - minx, yData->at(i) - miny, zData->at(i) - minz);
        if(borderMask[loc.x()][loc.y()].testBit(loc.z())){
            if(!marginMask[loc.x()][loc.y()].testBit(loc.z())){
                marginMask[loc.x()][loc.y()].setBit(loc.z(),true);
                //points << loc;
            }
            if(!marginMask[loc.x()+1][loc.y()].testBit(loc.z())){
                marginMask[loc.x()+1][loc.y()].setBit(loc.z(),true);
                //points << PointVector3D<int>(loc.x() + 1, loc.y(), loc.z());
            }
            if(!marginMask[loc.x()-1][loc.y()].testBit(loc.z())){
                marginMask[loc.x()-1][loc.y()].setBit(loc.z(),true);
                //points << PointVector3D<int>(loc.x() - 1, loc.y(), loc.z());
            }
            if(!marginMask[loc.x()][loc.y()+1].testBit(loc.z())){
                marginMask[loc.x()][loc.y()+1].setBit(loc.z(),true);
                //points << PointVector3D<int>(loc.x(), loc.y() + 1, loc.z());
            }
            if(!marginMask[loc.x()][loc.y()-1].testBit(loc.z())){
                marginMask[loc.x()][loc.y()-1].setBit(loc.z(),true);
                //points << PointVector3D<int>(loc.x(), loc.y() - 1, loc.z());
            }
            if(!marginMask[loc.x()][loc.y()].testBit(loc.z()+1)){
                marginMask[loc.x()][loc.y()].setBit(loc.z()+1,true);
                //points << PointVector3D<int>(loc.x(), loc.y(), loc.z() + 1);
            }
            if(!marginMask[loc.x()][loc.y()].testBit(loc.z()-1)){
                marginMask[loc.x()][loc.y()].setBit(loc.z()-1,true);
                //points << PointVector3D<int>(loc.x(), loc.y(), loc.z() - 1);
            }
        }
    }

    double sumD = 0;
    double sumDgrad = 0.0;
    double sum2Dgrad = 0.0;
    int numPts = 0;

//#pragma omp parallel for reduction(+: numPts, sumD, sumDgrad, sum2Dgrad)
    for(int i = 0; i < numPoints; ++i)
        if(marginMask[(*xData)[i]][(*yData)[i]].testBit((*zData)[i])){
            ++numPts;
            sumD += (*data)[(*xData)[i] - minx][(*yData)[i] - miny][(*zData)[i] - minz];
            sumDgrad += grad3D[i];
            sum2Dgrad += grad3Dsquared[i];
#if useDebug
            //if(grad3D[i] > 100000)
            //    qDebug() << "At location" << i << "grad3D =" << grad3D[i];
#endif
        }
    /*
    for(int i = 0; i < numPts; ++i)
        for(int j = 0; j < numPoints; ++j)
            if( xPts[i] == (*xData)[j] && yPts[i] == (*yData)[j] && zPts[i] == (*zData)[j] ){
                sumD += (*data)[loc.x()][loc.y()][loc.z()];
                sumDgrad += grad3D[j];
                sum2Dgrad += grad3Dsquared[j];
                break;
            }
    */

    double Margin1 = (sumDgrad/numPts) / (sumD/numPts)/dmin; //normally need to apply separate dx,dy,dz but our data has been isotropized
    double Margin2 = sqrt((sum2Dgrad/numPts) - pow((sumDgrad/numPts),2)) / (sumD/numPts)/dmin;

#if useDebug
    qDebug() << "numPts:" << numPts << "sumDgrad:" << sumDgrad << "sum2Dgrad:" << sum2Dgrad << "dmin:" << dmin;
    qDebug() << "Margin1:" << Margin1 << "Margin2:" << Margin2;
#endif

    //find center of mass
    double meanx = static_cast<double>(sumx) / numPoints;
    double meany = static_cast<double>(sumy) / numPoints;
    double meanz = static_cast<double>(sumz) / numPoints;

    std::vector<double> rgh;
//#pragma omp parallel for
    for(int i=0; i<numPoints; i++){
        if(grad3D[i] != 0.0){
            //xdist = loc.x() - meanx
            double xdist = xData->at(i) - minx - meanx;
            double ydist = yData->at(i) - miny - meany;
            double zdist = zData->at(i) - minz - meanz;
            double totdist = sqrt(pow(xdist,2) + pow(ydist,2) + pow(zdist,2));

            rgh.push_back(((xdist*gradx.at(i)) + (ydist*grady.at(i)) + (zdist*gradz.at(i))) / (totdist*grad3D.at(i)));
        }
    }

    double minrgh, maxrgh;
    std::tie(minrgh, maxrgh) = std::invoke([](auto d){auto p = std::minmax_element(d.begin(), d.end()); return std::make_pair(*p.first, *p.second);}, rgh);
    int histsize = 50;
    double binwidth = (maxrgh - minrgh) / histsize;
    QVector<long> hist(histsize,0);
    for(const auto & curval : rgh){
        auto cutoff = minrgh;
        auto histloc = 1;
        while( histloc < histsize ){
            cutoff += binwidth;
            if(curval <= cutoff)
                break;
            ++histloc;
        }
        hist[histloc - 1] += 1;
    }
    //sumhist = 1.0
    double sum2hist = 0.0;
//#pragma omp parallel for reduction(+: sum2hist)
    for(int i=0; i<histsize; i++){
        double histval = static_cast<double>(hist.at(i)) / numPoints;
        sum2hist += pow(histval,2);
    }
    double vRGH = sqrt((sum2hist/histsize) - pow((1.0/static_cast<double>(histsize)),2));

    QVector<double> output;
    output << Margin1 << Margin2 << vRGH /*<< numPoints*/ ; //numPoints is 'size' feature from UC
    return output;
}

