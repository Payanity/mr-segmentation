#include "mranalysis.h"
#include <QRegion>
#include <utility>
#include <QBitArray>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif


/*!
 * \brief Create an MrAnalysis object and initialize it.
 * \param model MR Lesion Model that this object will use to perform calculations
 * \param parent As a QObject, one may optionally have a parent to manage its lifetime
 * \param cmeansSegmentBins Custom c-means parameters to use for segmentation.
 * \pre None.
 * \post An MrAnalysis object has been initialized.
 */
MrAnalysis::MrAnalysis(std::shared_ptr<MRLesionModel> model, QObject *parent, QVector<uint> cmeansSegmentBins) :
    QObject(parent),
    model_{model}
{
	mrCMeansSegmentationParameters = std::move(cmeansSegmentBins); // save locally to ensure consistent precontrast image for segmentation, analysis, and output
}

/*!
 * \brief Returns true if the segmentation is complete; false if incomplete.
 * \returns A boolean that denotes whether or not the segmentation has finished.
 */
bool MrAnalysis::segmentationDone() const { return model_->segmentationDone_; }

/*!
 * \brief Returns true if the MR Analysis is complete; false if incomplete.
 * \returns A boolean that denotes whether or not the MR Analysis has finished.
 */
bool MrAnalysis::analysisDone() const { return model_->analysisDone_; }

//QVector<double> MrAnalysis::getFeatures() const { return model_->features; }

//QVector<double> MrAnalysis::getBnnFeats(){ return bnnFeats; }

//QVector<double> MrAnalysis::getHistFeats(){ return histFeats; }

//QVector<double> MrAnalysis::getAvgUptake() const { return model_->avgUptake; }

//QVector<double> MrAnalysis::getMostUptaking() const { return model_->mostUptaking; }

//double MrAnalysis::getPlotMaxValue() const { return model_->plotMaxValue; }
/*!
 * \brief Returns the regions of the screen area.
 * \param orientation Orientation of QuantX.
 * \returns A vector of QRegions.
 */
QVector<QRegion> MrAnalysis::getRegions (QUANTX::Orientation orientation) const
{
	if(orientation == QUANTX::NOPLANE || true) // for now, only use acquisition orientation
		orientation = model_->image->acquisitionOrientation();
	QVector<QRegion> regions(model_->segmentation.size());
	for(auto i = 0; i < model_->segmentation.size(); ++i)
		for(const auto & p : model_->segmentation.at(i))
			regions[i] ^= p;
	return regions;
}

/*!
 * \brief Fill the outline of the ROI.
 * \param outputOrientation Orientation to get the area for
 * \param includeBorders Whether or not to include the borders
 * \return A list of points for each slice of the image in the output orientation
 * \pre Segmentation is finished.
 * \post The list of all points within the ROI is returned.
 */
QVector<QVector<QPoint> > MrAnalysis::getFilledRegion(QUANTX::Orientation outputOrientation, const bool includeBorders) const
{
	int imgx = 0,
	    imgy = 0,
	    imgz = 0;
	int *segx = &imgx;
	int *segy = &imgy;
	int *segz = &imgz;
	int *outx = &imgx;
	int *outy = &imgy;
	int *outz = &imgz;

	const auto image = model_->image;
	const auto & segmentation = model_->segmentation;

	auto outputSize = image->imageSize().reoriented(outputOrientation);
	auto regionFilled = QVector<QVector<QPoint>>(outputSize.z());
	auto outputArray = QVector<QVector<QBitArray>>(outputSize.z(), QVector<QBitArray>(outputSize.y(), QBitArray(outputSize.x(),false)));
	if(outputOrientation == QUANTX::XZPLANE){
		outy = &imgz;
		outz = &imgy;
	}
	else if(outputOrientation == QUANTX::YZPLANE){
		outx = &imgy;
		outy = &imgz;
		outz = &imgx;
	}

	if(model_->orientation() == QUANTX::XZPLANE){
		segy = &imgz;
		segz = &imgy;
	}
	else if(model_->orientation() == QUANTX::YZPLANE){
		segx = &imgy;
		segy = &imgz;
		segz = &imgx;
	}

	for( (*segz) = 0; (*segz) < segmentation.size(); (*segz)++ ){
		const auto & polyVector = segmentation.at(*segz);
		for(const auto & sliceOutline : polyVector){
			if(!sliceOutline.empty()){
				QRegion filledRegion(sliceOutline,Qt::OddEvenFill);
				QRect rect = sliceOutline.boundingRect();
				for( (*segy) = rect.top() - 1; (*segy) < rect.bottom() + 2; (*segy)++){
					for( (*segx) = rect.left() - 1; (*segx) < rect.right() + 2; (*segx)++){
						if(includeBorders || !sliceOutline.contains(QPoint(*segx, *segy))) {
							if(filledRegion.contains(QPoint(*segx, *segy)) || (includeBorders && sliceOutline.contains(QPoint(*segx, *segy)))){
								outputArray[*outz][*outy].toggleBit(*outx);
							}
						}
					}
				}
			}
		}
	}
	for( auto z = 0; z < outputSize.z(); ++z )
		for( auto y = 0; y < outputSize.y(); ++y )
			for( auto x = 0; x < outputSize.x(); ++x ){
				if(outputArray.at(z).at(y).at(x))
					regionFilled[z] << QPoint(x, y);
			}
	return regionFilled; // perhaps this return should be a struct with the orientation and MRA UUID?
}

/*!
 * \brief Sets segmentation for analysis.
 * \param s Holds the 2D vector of QPolygons for the segmentation
 * \param x_seed Holds the x component of the voxel index of the segmentation seed point.
 * \param y_seed Holds the y component of the voxel index of the segmentation seed point.
 * \param z_seed Holds the z component of the voxel index of the segmentation seed point.
 * \return A boolean on whether or not the segmentation was set.
 */
bool MrAnalysis::setSegmentation(const QVector<QVector<QPolygon> > &s, int x_seed, int y_seed, int z_seed)
{
	// Only allow us to set a new segmentation if it's a manual outline
	if((!model_->segmentation.isEmpty()) && model_->getSegmentationType() != QUANTX::MANUAL)
		return false;
	model_->segmentation = s;
	model_->xSeed = x_seed;
	model_->ySeed = y_seed;
	model_->zSeed = z_seed;
	model_->segmentationDone_ = true;
	model_->analysisDone_ = false;
	return true;
}

/*!
 * \brief Returns a list of strings of each feature name.
 * \return A QStringList of feature names in the MR Analysis.
 */
QStringList MrAnalysis::getFeatureNames()
{
	return {
		            "QuantX Score"              , //  0
		            "Max Uptake"            , //  1
		            "Time to Peak"          , //  2 // Peak Timepoint
		            "Uptake Rate"           , //  3
		            "Washout Rate"          , //  4
		            "Curve Shape Index"     , //  5 // Shape Index
		            "E2"                    , //  6
		            "SER"                   , //  7
		            "Volume"                , //  8
		            "Surface Area"          , //  9
		            "Sphericity"            , // 10
		            "Irregularity"          , // 11
		            "Effective Radius"      , // 12
		            "Contrast"              , // 13
		            "Correlation"           , // 14
		            "Diff Entropy"          , // 15
		            "Diff Variance"         , // 16
		            "Uniformity"            , // 17 // Energy
		            "Entropy"               , // 18
		            "Homogeneity"           , // 19 // Inverse Difference Moment
		            "IMC1"                  , // 20
		            "IMC2"                  , // 21
		            "Max CC"                , // 22
		            "Sum Average"           , // 23
		            "Sum Entropy"           , // 24
		            "Sum Variance"          , // 25
		            "Variance"              , // 26
		            "Margin Feature 1"      , // 27 // Mean gradient
		            "Margin Feature 2"      , // 28 // Stdev gradient
		            "vRGH"                  , // 29 // Variance of radial gradient histogram
		         // "Num Points"            , // 30 // Used as 'size' feature at UC
		            "Max Uptake Var"        , // 31
		            "Peak Timepoint Var"    , // 32
		            "Uptake Rate Var"       , // 33
		            "Washout Rate Var"        // 34
	};
}
