#include "mranalysis.h"
#include "morphological.h"
#include "qimriimage.h"
#include "cmeans.h"
#include <QPoint>
#include <QVector>
#include <QPolygon>
#include <QRegion>
#include <QImage>
#include <QtMath>
#include <cmath>
#include <array>

//change to 1 to normalize fat sat images, to 0 to skip normalization step
#define FATSATNORMALIZE 0
#define FATSATSUBTRACT 0
//change to 1 to normalize non-fat sat images, to 0 to skip normalization step
#define NOFATSATNORMALIZE 0
#define NOFATSATSUBTRACT 1
//number of clusters to use for segmentation
#define FATSATCLUSTERS 3
#define NOFATSATCLUSTERS 3
//cutoff values to use for segmentation
#define FATSATCUTOFF 0.3
#define NOFATSATCUTOFF 0.2

#define INITIALROISIZE 10

#define USE_HOLEFILL_3D 0
#define EXPAND_XY_3D 1

#define printSeedPoint 0
#define useDebug 0
#if useDebug || printSeedPoint
#include <QDebug>
#endif // useDebug || printSeedPoint
#if useDebug
#include <QElapsedTimer>
static QElapsedTimer segTimer;
#endif // useDebug

/*!
 * \brief getWeights Gets the default weighting for each timepoint as input to the c-means algorithm.
 * \param size The number of timepoints in the image (number of items in the returned vector).
 * \param fatSatToggle If true the MR image was acquired not using fat sat, if false image was acquired using fat sat.
 * \return A vector of doubles representing the weighting for each timepoint.
 */
inline QVector<double> getWeights(int size, bool fatSatToggle)
{
	QVector<double> weights(size,1.0);

	//No fat sat.
	if(fatSatToggle){
#if NOFATSATNORMALIZE
		weights[0] = 0.5;  //this is usually less important (initial gets pulled out)
#elif NOFATSATSUBTRACT
		weights[0] = 0.5;
#else
		weights[0] = 1.0;
		weights[1] = 0.5;  //this is usually less important
#endif
		if(size < 5){
			//Put more importance on postcontrast timepoints
			weights[size-2] = 2.0;
			weights[size-1] = 2.0;
		}
	}
	else{
		//has fat sat
#if FATSATNORMALIZE
		weights[0] = 0.5;  //this is usually less important (initial gets pulled out)
#elif FATSATSUBTRACT
		weights[0] = 0.5;
#else
		weights[0] = 0.5;
		//this is usually less important
		weights[1] = 0.5;
#endif
		if(size < 5){
			//Put more importance on postcontrast timepoints
			weights[size-2] = 2.5;
			weights[size-1] = 1.5;
		}
	}

	return weights;
}

/*!
 * \brief Returns a bouding rectangle for the given polygon.
 * \param points A vector of points representing the given polygon.
 * \return A QRect representing the bouding rectangle.
 */
inline QRect getBoundingRect(const QVector<QPoint> &points)
{
	QPolygon polygon;
	for(const auto &p : std::as_const(points))
		polygon << p;
	return polygon.boundingRect();
}

/*!
 * \brief Create an ROI based on a seed point.
 * \param start Location of seed point in this object's image coordinate system
 * \param SingleSliceOnly Create a segmentation using only a 2D plane based on the acquisition orientation
 * \return A set of polygons for each slice of the MR image in the acquisition plance
 * \pre Image is a valid pointer, seed point is valid.
 * \post An outline of an ROI is generated.
 */
QVector<QVector<QPolygon> > MrAnalysis::segmentSinglePoint(PointVector3D<int> start, bool SingleSliceOnly)
{
	bool fatSat = model_->image->scanOptions().contains("FS") || model_->image->scanOptions().contains("PFP");
#if useDebug
	qDebug() << "FatSat:" << fatSat << "   Scan Options: " << model_->image->scanOptions();
#endif
	if(fatSat)
		return segmentSinglePoint(start,FATSATCLUSTERS,FATSATCUTOFF,INITIALROISIZE, SingleSliceOnly);

	    return segmentSinglePoint(start,NOFATSATCLUSTERS,NOFATSATCUTOFF,INITIALROISIZE, SingleSliceOnly);
}

/*!
 * \brief Create an ROI based on a seed point.
 * \details \overload
 * \param start Location of seed point in this object's image coordinate system
 * \param nClusters Number of c-means clusters to use
 * \param cutoff Threshold for including additional points in segmentation
 * \param initialSize Initial size of bounding box to use for segmentation
 * \param SingleSliceOnly Create a segmentation using only a 2D plane based on the acquisition orientation
 * \return A set of polygons for each slice of the MR image in the acquisition plance
 * \pre Image is a valid pointer, seed point is valid.
 * \post An outline of an ROI is generated.
 */
QVector<QVector<QPolygon> > MrAnalysis::segmentSinglePoint(PointVector3D<int> start, int nClusters, double cutoff, int initialSize, bool SingleSliceOnly)
{
	auto & xSeed = model_->xSeed;
	auto & ySeed = model_->ySeed;
	auto & zSeed = model_->zSeed;
	auto & image = model_->image;
	auto & segmentation = model_->segmentation;

	xSeed = start.x();
	ySeed = start.y();
	zSeed = start.z();
#if useDebug || printSeedPoint
	qDebug() << "Segmentation Seed is at point (x,y,z):" << xSeed << "," << ySeed << "," << zSeed;
#endif
	emit startedSegmentation();
	double cmeansCutoffValue = cutoff;
	int cmeansCluster = nClusters;

#if useDebug
	qDebug() << "Parameters used: cutoff =" << cmeansCutoffValue << "nClusters =" << cmeansCluster << "nBins =" << mrCMeansSegmentationParameters;
#endif

	const auto pointList2D = segment2Dstage(initialSize, cmeansCluster, cmeansCutoffValue);
	QVector<QVector<QPoint> > pointList;
	if (!SingleSliceOnly)
		pointList = segment3Dstage(pointList2D, cmeansCluster, cmeansCutoffValue);
	else {
		pointList.resize(image->numSlices());
		int slice = zSeed;
		if (image->acquisitionOrientation() == QUANTX::YZPLANE)
			slice = xSeed;
		else if (image->acquisitionOrientation() == QUANTX::XZPLANE)
			slice = ySeed;
		pointList[slice] = pointList2D;
	}
	segmentation.clear();
	//trace border
	for(const auto & slicePoints : std::as_const(pointList)){
		segmentation << traceBorders2D(slicePoints);
	}

	model_->segmentationDone_ = true;
	model_->analysisDone_ = false;
	emit finishedSegmentation();
	return segmentation;
}

/*!
 * \brief 3D stage of the MR segmentation algorithm, running after the 2D stage.
 * \param pointList2D Polygon to be segmented.
 * \param cmeansCluster Number of c-means clusters to use.
 * \param cmeansCutoffValue Maximum membership ratio to a cluster center that is used to determine whether a point is part of that cluster.
 * \return A vector of polygons resulting from 3D segmentation.
 */
inline QVector<QVector<QPoint> > MrAnalysis::segment3Dstage(const QVector<QPoint> & pointList2D, int cmeansCluster, double cmeansCutoffValue)
{
	auto & image = model_->image;
	auto dx = static_cast<double>(image->dx());
	auto dy = static_cast<double>(image->dy());
	auto dz = static_cast<double>(image->dz());
	auto zStart = model_->zSeed;
	auto numX = image->nx();
	auto numY = image->ny();
	auto numZ = image->nz();
	if(model_->orientation() == QUANTX::XZPLANE){
		std::swap(dy, dz);
		zStart = model_->ySeed;
		numY = image->nz();
		numZ = image->ny();
	}
	if(model_->orientation() == QUANTX::YZPLANE){
		std::swap(dx, dy);
		std::swap(dy, dz);
		zStart = model_->xSeed;
		numX = image->ny();
		numY = image->nz();
		numZ = image->nx();
	}
	//parameters of the segmentation
	double areaCompareThreshold = 0.4;
	//double areaCompareThreshold = 5.0;
	//this is to force it to use the fcmpoly (for testing)
	double eccentricityThreshold = 0.7;
	if(image->manufacturer().contains("Siemens"))
		eccentricityThreshold = 0.6;
	int dilateKernelRadius = 3;
	double endSliceLesionFractionThreshold = 0.05; //this is the fraction of voxels in the end
	double endSliceLesionFractionThresholdXY = 0.10; //this is the fraction of voxels in the end
	// slices that can be lesion voxels, otherwise
	// we'll add the next slice to the fcm input and repeat

	// Now use properties to 2D lesion on center slice to decide how many slices to
	// use for 3D segmentation of the lesion

	//this is temporary
	int numSlices = 7;
	QVector<double> regionEllipseParams = regionEllipse(pointList2D, dx, dy);
	auto regionAreaInSqMM = pointList2D.size() * dx * dy;
	auto regionLengthInMM = regionEllipseParams.at(0);
#if useDebug
	qDebug() << "dx,dy,dz" << dx << dy << dz;
	qDebug() << "region Ellipse params" << regionEllipseParams;
	qDebug() << "region Area:" << regionAreaInSqMM;
	qDebug() << "region Length:" << regionLengthInMM;
#endif
	if (regionEllipseParams.at(2) > eccentricityThreshold)
		numSlices = qCeil(regionLengthInMM / dz);
	else
		numSlices = 2 * qCeil(sqrt(regionAreaInSqMM / 3.0) / dz); //Use 3.0 as under-estimation of PI, so, if anything, we include more slices

#if useDebug
	qDebug() << "numslice0" << numSlices;
#endif

	numSlices = ((numSlices % 2) != 0) ? numSlices : (numSlices + 1); //Make sure numSlices is odd
#if useDebug
	qDebug() << "numslice odd" << numSlices;
#endif

	//  End 2D segmentation for sizing
	/////////////////////////////////////////////////////
	int zBegin = qMax(zStart - (numSlices - 1) / 2, 0);
	int zEnd = qMin(zStart + (numSlices - 1) / 2, numZ - 1);
	int zInitialSize = zEnd - zBegin + 1;
	QRect initialRect = getBoundingRect(pointList2D);
	QRect boundingRect = initialRect;
	double rectArea = (boundingRect.height() * boundingRect.width());
	QVector<QVector<QPoint> > pointList;
	QVector<QVector<QPoint> > fcmoutput;
	QVector<QVector<double> > clusterCenters;
	QVector<double> oldClusterCenters;
#if useDebug
	    qDebug() << segTimer.elapsed() << "begin 3D";
#endif
		bool badseg = true;
	while(badseg) {
#if useDebug
		qDebug() << segTimer.elapsed() << "Get FCM Output";
#endif
		fcmoutput = fcmRect3D(boundingRect,zBegin, zEnd, cmeansCutoffValue, cmeansCluster, &clusterCenters);
		//if(oldClusterCenters.isEmpty())
		//    oldClusterCenters = clusterCenters.first();
#pragma omp parallel for // Paralellizing this loop causes a crash if you use [i] and first() instead of at(i) and constFirst()
		for(int i = 0; i < oldClusterCenters.size(); ++i){
			if( qAbs((clusterCenters.constFirst().at(i) - oldClusterCenters.at(i)) / oldClusterCenters.at(i)) > ( (*std::max_element(oldClusterCenters.begin(), oldClusterCenters.end()) / oldClusterCenters.constFirst()) * 0.025 ) ){ // cutoff for the change in each value for the centerpoint
				badseg = false;
#if useDebug
				qDebug() << "CLUSTER CENTER MOVED TOO MUCH!!! STOPPING SEGMENTATION ROUTINE NOW!!!";
				qDebug() << "Old Pixel Value:" << oldClusterCenters.at(i) << "New Pixel Value:" << clusterCenters.constFirst().at(i) << "Ratio of Change:" << qAbs((clusterCenters.constFirst().at(i) - oldClusterCenters.at(i)) / oldClusterCenters.at(i));
#endif
			}
		}
		if(!badseg)
			break;
		oldClusterCenters = clusterCenters.constFirst();

#if useDebug
		qDebug() << segTimer.elapsed() << "Label Region";
#endif
		QVector<QVector<QVector<QPoint> > > labeledRegions = labelRegions3D(fcmoutput);
#if useDebug
		qDebug() << segTimer.elapsed() << "Find Largest Region";
#endif
		QVector<double> labelSizes(labeledRegions.size(), 0);
#pragma omp parallel for
		for (int i = 0; i < labeledRegions.size(); i++){
			labelSizes[i] = regionSize(labeledRegions.at(i));
		}
		auto largestLabel = static_cast<int>( std::distance(labelSizes.begin(), std::max_element(labelSizes.begin(), labelSizes.end())) );

#if USE_HOLEFILL_3D
#if useDebug
		qDebug() << segTimer.elapsed() << "Fill in holes";
#endif
		pointList = holeFill3D(labeledRegions[largestLabel]);
#endif
		pointList = labeledRegions[largestLabel];

		double centersliceAreaInSqMM;


		centersliceAreaInSqMM = holeFill2D(pointList[zStart]).size() * dx * dy;


		if (centersliceAreaInSqMM < areaCompareThreshold * regionAreaInSqMM){
			// expect area of lesion on center slice of 3D seg.
			// to be close to the area of the 2D seg. if not, redo with fcmpoly
#if useDebug
			qDebug() << segTimer.elapsed() << "Center slice is too small";
#endif


			int minDim = std::min(boundingRect.width(), boundingRect.height());
			dilateKernelRadius = std::min(1, qRound((minDim * 0.3) / 2));
			QVector<QPoint> dilatedPointList = dilatePolygon2D(pointList2D, dilateKernelRadius);

			//QPolygon boundingRectPoly(boundingRect,true);
			//QList<QPoint>  newPointList = dilatedPointList.intersected(boundingRectPoly);//make sure dilated pointlist in Rect;

//#pragma omp parallel for // Note: Order matters here. If we want to parallelize it we need to do this in two steps
			for (int i = 0; i < dilatedPointList.size(); i++){
				//if (!(boundingRectPoly.containsPoint(dilatedPointList.at(i),Qt::OddEvenFill)))
				if(!boundingRect.contains(dilatedPointList[i],true))
					dilatedPointList.remove(i);
			}
#if useDebug
			qDebug() << segTimer.elapsed() << "start fcmPoly1slice";
#endif
			fcmoutput = fcmPoly1slice(dilatedPointList, zBegin, zEnd, cmeansCutoffValue, cmeansCluster);

#if useDebug
			qDebug() << segTimer.elapsed() << "label regions";
#endif
			labeledRegions = labelRegions3D(fcmoutput);

			labelSizes.resize(labeledRegions.size());
			labelSizes.fill(0);
#pragma omp parallel for
			for (int i = 0; i < labeledRegions.size(); i++){
				labelSizes[i] += regionSize(labeledRegions[i]);
			}

			auto largestLabel = static_cast<int>( std::distance(labelSizes.begin(), std::max_element(labelSizes.begin(), labelSizes.end())) );

#if USE_HOLEFILL_3D
			pointList = holeFill3D(labeledRegions[largestLabel]);
#endif
			pointList = labeledRegions[largestLabel];

		}
		badseg = false; //< we'll keep this segmentation as long as the tests below on the end slices don't fail
#if useDebug
		qDebug() << segTimer.elapsed() << "Check if lesion needs to grow";
#endif
		double sliceBeginLesionVoxelCount = pointList[zBegin].size();
		if (((sliceBeginLesionVoxelCount / rectArea) > endSliceLesionFractionThreshold) && (zBegin > 0)) {
			if ((zEnd - zBegin) <= qRound(zInitialSize * 1.7)){ // prevent from growing too much in Z-direction
				zBegin = qMax(zBegin - 1 - (zEnd - zBegin) / 20, 0);
				badseg = true;
			}
			else{
				badseg = false;
#if useDebug
				qDebug() << "z-dimension grew too much";
#endif
			}
		}
		double sliceEndLesionVoxelCount = pointList[zEnd].size();
		if (((sliceEndLesionVoxelCount / rectArea) > endSliceLesionFractionThreshold) && (zEnd < (numZ - 1))) {
			if ((zEnd - zBegin) <= qRound(zInitialSize * 1.7) ){ // prevent from growing too much in Z-direction
				zEnd = qMin(zEnd + 1 + (zEnd - zBegin) / 20, numZ - 1);
				badseg = true;
			}
			else{
				badseg = false;
#if useDebug
				qDebug() << "z-dimension grew too much";
#endif
			}
		}

#if EXPAND_XY_3D
		// If z dimensions didn't change, check the x and y dimensions
		if(!badseg){
#if useDebug
			qDebug() << "z-dimension did not change, now checking x and y dimensions";
			QRect oldRect = boundingRect;
#endif
			//QVector<int> borderCount(4, 0);
			int yzSize = boundingRect.height() * (zEnd - zBegin + 1);
			int xzSize = boundingRect.width()  * (zEnd - zBegin + 1);

			int bc0 = 0, bc1 = 0, bc2 = 0, bc3 = 0;
#pragma omp parallel for reduction(+: bc0, bc1, bc2, bc3) // The borderCount is a shared variable so we can't do this directly
			for(auto ploc = 0; ploc < pointList.size(); ++ploc){
				const auto & slicePoints = pointList.at(ploc);
				//int minBorderCount = qRound(sqrt(static_cast<float>(slicePoints.size())) / 4);
				//int minBorderCount = (boundingRect.width() + boundingRect.height()) / 4;
				//int minBorderCount = qMin(boundingRect.width(), boundingRect.height()) / 2;

				QVector<int> borders = checkBorderCount(boundingRect, slicePoints);
				/*
#pragma omp parallel for
				for(int i = 0; i < 4; ++i){
					borderCount[i] += borders[i];
				}
				*/
				bc0 += borders[0];
				bc1 += borders[1];
				bc2 += borders[2];
				bc3 += borders[3];
			}

			if ( (bc0 > (xzSize * endSliceLesionFractionThresholdXY * 1.0)) && (boundingRect.height() < 2 * initialRect.height()) ) {
				boundingRect.setTop(qMax(boundingRect.top() - 1 - boundingRect.height() / 20, 0));
				badseg = true;
			}
			if ( (bc1 > (yzSize * endSliceLesionFractionThresholdXY * 1.0)) && (boundingRect.width() < 2 * initialRect.width()) ) {
				boundingRect.setRight(qMin(boundingRect.right() + 1 + boundingRect.width() / 20, numX - 1));
				badseg = true;
			}
			if ( (bc2 > (xzSize * endSliceLesionFractionThresholdXY * 1.0)) && (boundingRect.height() < 2 * initialRect.height()) ) {
				boundingRect.setBottom(qMin(boundingRect.bottom() + 1 + boundingRect.height() / 20, numY - 1));
				badseg = true;
			}
			if ( (bc3 > (yzSize * endSliceLesionFractionThresholdXY * 1.0)) && (boundingRect.width() < 2 * initialRect.width()) ) {
				boundingRect.setLeft(qMax(boundingRect.left() - 1 - boundingRect.width() / 20, 0));
				badseg = true;
			}

			rectArea = (boundingRect.height() * boundingRect.width());
#if useDebug
			qDebug() << "Old Rect:" << oldRect << " New Rect:" << boundingRect << " Border Count:" << QVector<int>{bc0, bc1, bc2, bc3} << "Top/Bottom Threshold:" << xzSize * endSliceLesionFractionThresholdXY << "Left/Right Threshold:" << yzSize * endSliceLesionFractionThresholdXY;
#endif
		}
#endif //EXPAND_XY_3D

#if useDebug
		qDebug() << segTimer.elapsed() << "zBegin:" << zBegin << " zEnd:" << zEnd;
#endif
	}
	return holeFill3D(pointList);
}

/*!
 * \brief The 2D stage of the MR segmentation algorithm.
 * \param initialSize Initial size of lesion in mm.
 * \param cmeansCluster Number of c-means clusters to use.
 * \param cmeansCutoffValue The maximum membership ratio to a cluster center that is used to determine whether a point is part of that cluster.
 * \return The segmented 2D polygon (vector of points).
 */
inline QVector<QPoint> MrAnalysis::segment2Dstage(int initialSize, int cmeansCluster, double cmeansCutoffValue)
{
	auto & image = model_->image;
	float dx = image->dx();
	float dy = image->dy();
	int xStart = model_->xSeed;
	int yStart = model_->ySeed;
	int zStart = model_->zSeed;
	if(model_->orientation() == QUANTX::XZPLANE){
		dy = image->dz();
		yStart = model_->zSeed;
		zStart = model_->ySeed;
	}
	if(model_->orientation() == QUANTX::YZPLANE){
		dx = image->dy();
		dy = image->dz();
		xStart = model_->ySeed;
		yStart = model_->zSeed;
		zStart = model_->xSeed;
	}

	// Convert length from mm to pixels for initial lesion bounding rect
	int ROIsizeX = qMax(3, qRound(static_cast<float>(initialSize) / dx));
	int ROIsizeY = qMax(3, qRound(static_cast<float>(initialSize) / dy));

	// This is good for Axial images
	QRect initialRect( xStart - (ROIsizeX - 1) / 2, yStart - (ROIsizeY - 1) / 2, ROIsizeX, ROIsizeY);

	QVector<QPoint> fcmoutput;
	QVector<QPoint> pointList2D;
	//////////////////////////////////////////////////////////////////
	//  Do 2D FCM to estimate size of lesion and need size of cube for
	//  3D segmentation of the whole lesion
	//////////////////////////////////////////////////////////////////
	bool badseg = true;
	QVector<int> growCount(4);
	QVector<bool> borders(4);
#if useDebug
	segTimer.start();
#endif
	while (badseg){
#if useDebug
		//qDebug() << "ROI Rect:" << initialRect;
#endif
		fcmoutput = fcmRect2D(initialRect, zStart, cmeansCutoffValue, cmeansCluster);

		QVector<QVector<QPoint> > labeledRegions2D = labelRegions2D(fcmoutput);
		QVector<double> labelSizes2D(labeledRegions2D.size());
#pragma omp parallel for
		for (int i = 0; i < labeledRegions2D.size(); i++){
			labelSizes2D[i] = labeledRegions2D.at(i).size();
		}
		if(labelSizes2D.isEmpty()){
			badseg = true;
			borders.fill(true);
		}
		else{
			auto largestLabel2D = static_cast<int>( std::distance(labelSizes2D.begin(), std::max_element(labelSizes2D.begin(), labelSizes2D.end())) );
			pointList2D = holeFill2D(labeledRegions2D[largestLabel2D]);

			borders = checkBorders(initialRect, pointList2D);
			badseg = (borders.count(false) != 4); // Check to see if any borders are touching the lesion
		}
		if(badseg){ // enlarge ROI on any side that touched lesion
			updateGrowCount(&growCount,borders);
			if (growCount.contains(10)){ // Growing too much on one side, assume overgrowing
				badseg = false;
			}
			else{
				if (borders[0]) //touching top
					initialRect.setTop(initialRect.top() - 1);
				//initialRect.setTop(initialRect.top()-1-static_cast<int>(oldrect.height()/20));
				if (borders[1]) //touching right
					initialRect.setRight(initialRect.right() + 1);
				//initialRect.setRight(initialRect.right()+1+static_cast<int>(oldrect.width()/20));
				if (borders[2]) //touching bottom
					initialRect.setBottom(initialRect.bottom() + 1);
				//initialRect.setBottom(initialRect.bottom()+1+static_cast<int>(oldrect.height()/20));
				if (borders[3]) //touching left
					initialRect.setLeft(initialRect.left() - 1);
				//initialRect.setLeft(initialRect.left()-1-static_cast<int>(oldrect.width()/20));
			}

		}
#if useDebug
		qDebug() << segTimer.elapsed() << "initialRect:" << initialRect;
#endif
	}
	return pointList2D;
}

/*!
 * \brief Outlines the borders of a polygon.
 * \param pointList A vector of points representing a polygon.
 * \return A vector of polygons that represent outlines of the polygon on different slices.
 */
QVector<QPolygon> MrAnalysis::traceBorders2D(QVector<QPoint> pointList)
{
	static constexpr std::array<QPoint, 8> direction{
		//we need to define one QPoint or the compiler pukes
		         QPoint(-1,-1) , { 0,-1} , { 1,-1} , { 1, 0} ,
				       { 1, 1} , { 0, 1} , {-1, 1} , {-1, 0} };

	QPoint minXy;
	QPoint maxXy;

	if(!pointList.isEmpty())
		minXy = maxXy = pointList.first();
	for(auto i : pointList){
		if(i.x() < minXy.x())
			minXy.setX(i.x());
		if(i.x() > maxXy.x())
			maxXy.setX(i.x());
		if(i.y() < minXy.y())
			minXy.setY(i.y());
		if(i.y() > maxXy.y())
			maxXy.setY(i.y());
	}

	QVector<QPolygon> sliceOutlines(0);
	do{
		QPolygon slicePolygon(0);
		if(!pointList.empty()){
			QPoint startpoint,currentpoint;
			bool havestart = false;
			for(int y = minXy.y(); y <= maxXy.y(); y++){
				for(int x = minXy.x(); x <= maxXy.x(); x++){
					if(pointList.contains({x, y})){
						//set startpoint to left-most point in line with lowest y value
						startpoint = {x, y};
						havestart = true;
						break;
					}
				}
				if(havestart)
					break;
			}
			currentpoint = startpoint;
			slicePolygon << currentpoint;
			int d = 0;
			do{
				for(int i = 0; i < 8; i++){
					if(pointList.contains(currentpoint + direction[(d + i) % 8])){
						currentpoint = currentpoint + direction[(d + i) % 8];
						d = (d + i + 5) % 8;
						break;
					}
				}
				slicePolygon << currentpoint;
			}while(startpoint != currentpoint);

			const QRegion filled(slicePolygon);
			const auto rect = filled.boundingRect();
			QVector<QPoint> insidePointList;

			for(int y = rect.y(); y < rect.y() + rect.height(); y++){
				for(int x = rect.x(); x < rect.x() + rect.width(); x++){
					if((!pointList.contains({x, y})) && filled.contains({x, y})){
						insidePointList.append({x, y});
					}
				}
			}
			/*
			// First pass at this using recursion. We can also do this by adding insidePointList to the eroded pointList
			if(!insidePointList.isEmpty()){
				auto insideBorders = traceBorders2D(insidePointList);
				sliceOutlines += insideBorders;
			} */
//#pragma omp parallel for // Note: Order matters here. If we want to parallelize it we need to do this in two steps
			for(int i = pointList.size() - 1; i >= 0; i--){
				if(slicePolygon.contains(pointList[i]) || filled.contains(pointList[i]))
					pointList.remove(i);
			}
			pointList += insidePointList;
		}
		sliceOutlines << slicePolygon;
	}while(!pointList.empty());

#if useDebug
	for(auto i = 0; i < sliceOutlines.size(); ++i ){
		if(!sliceOutlines.at(i).isEmpty()){
			qDebug() << "traceborder slice has" << sliceOutlines.size() << "polygons";
		}
	}
#endif

	return sliceOutlines;
}

/*!
 * \brief Paints segmenation onto a given set of images.
 * \param segmentationImage Vector of images to paint the segmenation onto.
 * \param color The color of the segmentation represented as an unsigned int.
 * \param drawSeed If true, then paint segmentation onto images.
 */
void MrAnalysis::getSegmentation(QVector<QImage> *segmentationImage, uint color, bool drawSeed) const
{
	auto & image = model_->image;
	auto & orientation = model_->orientation();
	int maxx = image->nx();
	int maxy = image->ny();
	int maxz = image->nz();
#pragma omp parallel for
	for(int z = 0; z < model_->segmentation.size(); z++){
		for(const auto & polygon : std::as_const(model_->segmentation.at(z)))
			for(const auto & point : std::as_const(polygon)){
				int x = point.x();
				int y = point.y();
#if 0
				if(orientation == QUANTX::XYPLANE){
					if(flipMRIdimension.x())
						x = maxx - 1 - x;
					if(!flipMRIdimension.y())
						y = maxy - 1 - y;
				}
				else if(orientation == QUANTX::XZPLANE){
					if(flipMRIdimension.x())
						x = maxx - 1 - x;
					if(!flipMRIdimension.z())
						y = maxz - 1 - y;
				}
				else if(orientation == QUANTX::YZPLANE){
					if(flipMRIdimension.y())
						x = maxy - 1 - x;
					if(!flipMRIdimension.z())
						y = maxz - 1 - y;
				}
#endif
				(*segmentationImage)[z].setPixel(x, y, color);
			}
	}
	if(drawSeed){
		auto & xSeed = model_->xSeed;
		auto & ySeed = model_->ySeed;
		auto & zSeed = model_->zSeed;

		auto xCenter = 1;
		auto yCenter = 1;
		auto z = 0;
		if(orientation == QUANTX::XYPLANE){
			xCenter = std::min(maxx - 1, std::max(1, xSeed));
			yCenter = std::min(maxy - 1, std::max(1, ySeed));
			z = zSeed;
		}
		if(orientation == QUANTX::XZPLANE){
			xCenter = std::min(maxx - 1, std::max(1, xSeed));
			yCenter = std::min(maxz - 1, std::max(1, zSeed));
			z = ySeed;
		}
		if(orientation == QUANTX::YZPLANE){
			xCenter = std::min(maxy - 1, std::max(1, ySeed));
			yCenter = std::min(maxz - 1, std::max(1, zSeed));
			z = xSeed;
		}
		for(auto x = xCenter - 1; x <= xCenter + 1; ++x)
			for(auto y = yCenter - 1; y <= yCenter + 1; ++y)
				(*segmentationImage)[z].setPixel(x, y, QColor::fromRgba(color).lighter().rgba());
	}

}

/*!
 * \brief Checks if the lesion touches any of the borders of the rect used as input to the FCM
 * and returns "true" for edges that do.
 * \param rect The rectangle to check the borders of.
 * \param lesion2D The lesion to check.
 * \return A vector of booleans in which the edges of the lesion that touch rect are "true."
 */
inline QVector<bool> MrAnalysis::checkBorders(QRect rect, const QVector<QPoint>& lesion2D)
{
	//on return this will contain true for edges of lesion that touch the input rect border
	QVector<bool> borders(4, false);
	if(lesion2D.isEmpty())
		return borders;

	QRect lesionBoundingBox = getBoundingRect(lesion2D);

	//if boundingbox of lesion is thesame as the input rect, then the lesion is touching that border
	if (lesionBoundingBox.top() <= rect.top())
		borders[0] = true;
	if (lesionBoundingBox.right() >= rect.right())
		borders[1] = true;
	if (lesionBoundingBox.bottom() >= rect.bottom())
		borders[2] = true;
	if (lesionBoundingBox.left() <= rect.left())
		borders[3] = true;

	return borders;
}

/*!
 * \brief Checks if the lesion touches any of the borders of the rect used as input to the
 * FCM, and returns the number of lesion pixels on each border.
 * \param rect The rectangle to check the borders of.
 * \param lesion2D The lesion to check.
 * \return A vector of integers representing the number of lesion pixels on each border.
 */
inline QVector<int> MrAnalysis::checkBorderCount(const QRect & rect, const QVector<QPoint> & lesion2D)
{
	QVector<int> borderCount(4, 0);
	if(lesion2D.isEmpty())
		return borderCount;

	int left = rect.left();
	int right = rect.right();
	int top = rect.top();
	int bottom = rect.bottom();
	int bc0 = 0, bc1 = 0, bc2 = 0, bc3 = 0;
#pragma omp parallel for reduction(+: bc0, bc1, bc2, bc3)
	for(auto i = 0; i < lesion2D.size(); ++i){
		const auto & p = lesion2D.at(i);
		bc0 += (p.y() == top   ) ? 1 : 0;
		bc1 += (p.x() == right ) ? 1 : 0;
		bc2 += (p.y() == bottom) ? 1 : 0;
		bc3 += (p.x() == left  ) ? 1 : 0;
	}

	return QVector<int>{bc0, bc1, bc2, bc3};
}

/*!
 * \brief Updates the count for which how many borders have grown/expanded.
 * \param growcount A vector of integers keeping count of how many borders have grown.
 * \param borders A vector of booleans with each index representing a border having the value of "true" if that border has been expanded.
 * \return True if borders expanded.
 */
inline bool MrAnalysis::updateGrowCount(QVector<int> *growcount, QVector<bool> borders) {

	//which borders needed expanding
	QVector<int> expandedborders(0);
	//which borders were expanded last iteration
	QVector<int> previousborders(0);
	for (int i=0;i<4;i++){
		if (borders[i])
			expandedborders<<i;
		if (growcount->at(i)>0)
			previousborders<<i;
	}
	//this function should not have been called if borders didn't expand
	if(expandedborders.isEmpty())
		return false;
	 //if borders are expanded on more than one border, reset growcounts
	if(expandedborders.size()>1)
		growcount->fill(0);
	else{
		if ((!previousborders.isEmpty()) && (previousborders.at(0) == expandedborders.at(0)))
			(*growcount)[previousborders.at(0)]++;
		else {
			growcount->fill(0);
			(*growcount)[expandedborders.at(0)]=1;
		}
	}
	return true;
}

/*!
 * \brief Finds the centroid of a given 2D polygon.
 * \param region2D The polygon to find the centroid of.
 * \return Returns a centroid point.
 */
inline QPointF MrAnalysis::regionCentroid(const QVector<QPoint>& region2D)
{

	auto accumX = 0.0, accumY = 0.0;
	QPointF centroid;
#pragma omp parallel for reduction(+: accumX, accumY)
	for (auto i = 0; i < region2D.size(); ++i){
		accumX += region2D.at(i).x();
		accumY += region2D.at(i).y();
	}
	centroid.setX(accumX / region2D.size());
	centroid.setY(accumY / region2D.size());

	return centroid;
}

/*!
 * \brief Calculates an equivalent ellipse from given polygon (region).
 * \param region2D The region of the given polygon.
 * \param dx The distance between two pixels in the x dimension of the coordinate system defined by region2D
 * \param dy The distance between two pixels in the y dimension of the coordinate system defined by region2D
 * \return A vector containing its equivalent ellipse , (0)major and (1)minor axis,(2)eccentricity, (3) orientation (in radians) of region.
 */
inline QVector<double> MrAnalysis::regionEllipse(const QVector<QPoint>& region2D, double dx, double dy)
{
	QPointF centroid;
	double  x2moment     = 0,
	        y2moment     = 0,
	        x1y1moment    = 0,
	        //num         = 0,
	        //den         = 0,
	        orientation = 0;
	QVector<double> params;
	double common;

	centroid = regionCentroid(region2D);
#if useDebug
	qDebug() << "calculate moments of region";
#endif
	//calculate moments of region
	qreal cx = centroid.x();
	qreal cy = centroid.y();
#pragma omp parallel for reduction(+: x2moment, y2moment, x1y1moment)
	for (auto i = 0; i < region2D.size(); ++i){
		double xdist = (region2D.at(i).x() - cx) * dx;
		double ydist = (region2D.at(i).y() - cy) * dy;
		x2moment   += pow(xdist, 2);
		y2moment   += pow(ydist, 2);
		x1y1moment += xdist * ydist;
	};
	//Add 1/12 for the voxel at 0,0? However, the centroid is a float, not an int
	x2moment   = x2moment   / region2D.size();//+ dx / 12.
	y2moment   = y2moment   / region2D.size();//+ dy / 12.;
	x1y1moment = x1y1moment / region2D.size();
	common     = sqrt( pow( (x2moment - y2moment), 2) + 4 * pow(x1y1moment, 2) ); // This is used to calculate the difference in size between major and minor axes
#if useDebug
	qDebug() << "Ellipse x2moment:" << x2moment << "  y2moment:" << y2moment << "  x1y1moment:" << x1y1moment << "  common:" << common;
#endif
	//major axis
	params << 2 * sqrt( x2moment + y2moment + common );
	//minor axis
	params << 2 * sqrt( x2moment + y2moment - common );
	//eccentricity
	params << sqrt( 1 - ( ( x2moment + y2moment - common ) / ( x2moment + y2moment + common ) ) );
	//params << 2. * sqrt( (params.at(0) / 2) * (params.at(0) / 2) - (params.at(1) / 2) * (params.at(1) / 2) ) / params.at(0); // eccentricity - not sure what is going on here

	// calculate orientation
	/*
	if (y2moment > x2moment) {
		num = y2moment - x2moment + common;
		den = 2 * x1y1moment;
	} else {
		num = 2 * x1y1moment;
		den = x2moment - y2moment + common;
	}
	if ((num == 0) && (den == 0))
		orientation = 0;
	else
		orientation = atan(num / den);
	*/
	//This should be the same as above
	orientation = atan( (2 * x1y1moment) / (x2moment - y2moment)) / 2;
	params << orientation;


	return params;
}

/*!
 * \brief Find the most uptaking c-means cluster (most likely to be the lesion we are looking for).
 * \param cMeansResults The output from our c-means algorithm.
 * \param weights The weighting for each timepoint s input to the c-means algorithm.
 * \param nClusters Number of c-means clusters used for the c-means algorithm.
 * \param fatSatToggle A flag that should be set to true if an MR image was acquired using fat sat, false if an MR image was acquired not using fat sat (opposite of getWeights - double check).
 * \return The most uptaking cluster (integer).
 */
inline int getMostUptakingCluster(const QVector<QVector<double> > &cMeansResults, const QVector<double> &weights, const int &nClusters, bool fatSatToggle)
{
	int mostUpC = 0;
	double mostUptake = 0.0;
	int dimensions = weights.size();
	for(int i=0;i<nClusters;i++){
		double sumUptake = 0.0;
		//sumUptake -= cMeansResults[i][0];              //subtract first timepoint
#pragma omp parallel for reduction(+: sumUptake)
		for(int j=0; j<dimensions; j++){               //add rest of timepoints
			if(fatSatToggle)
#if FATSATNORMALIZE || FATSATSUBTRACT
				sumUptake += cMeansResults[i][j] * weights[j];
#else
				sumUptake += (cMeansResults.at(i).at(j) - cMeansResults.at(i).at(0)) * weights.at(j);
#endif
			else
#if NOFATSATNORMALIZE || NOFATSATSUBTRACT
				sumUptake += cMeansResults.at(i).at(j) * weights.at(j);
#else
				sumUptake += (cMeansResults[i][j] - cMeansResults[i][0]) * weights[j];
#endif
		}
		if(sumUptake > mostUptake){
			mostUptake = sumUptake;
			mostUpC = i;
		}
	}
	return mostUpC;
}

inline QVector<QPoint> MrAnalysis::fcmRect2D(QRect boundRect, int z, double threshold, int nClusters)
{
	return fcmRect3D(boundRect, z, z, threshold, nClusters).at(z);
}


/*!
 * \brief Generate FCM output from bounding rectangle, start slice, end slice.
 * \param boundRect The bounding rectangle of the image to perform the FCM segmentation on.
 * \param startZ The starting slice.
 * \param endZ The ending slice.
 * \param cmeansCutoffValue The maxiumum membership ratio to a cluster center that is used to determine whether a point is part of that cluster.
 * \param cmeansCluster Number of c-means clusters to use.
 * \param clusterCenters Centers of each cluster.
 * \return A 3D rectangle (vector of polygons) generated by the fuzzy c-means algorithm (FCM).
 */
inline QVector<QVector<QPoint> > MrAnalysis::fcmRect3D(QRect boundRect, int startZ, int endZ, double cmeansCutoffValue, int cmeansCluster, QVector<QVector<double> > *clusterCenters)
{
#if useDebug
	qDebug() << "start fcmRect3D";
#endif
	const auto & image = model_->image;
	const auto & orientation = model_->orientation();
	//collect region data
	QVector<QVector<double> > voxelData(0);
	auto timePts = image->nt();
	QVector<PointVector3D<int> > pointLocs;
	int imgx, imgy, imgz;
	int *segx = &imgx;
	int *segy = &imgy;
	int *segz = &imgz;
	int imgxStart = boundRect.left();
	int imgxEnd = boundRect.right();
	int imgyStart = boundRect.top();
	int imgyEnd = boundRect.bottom();
	int imgzStart = startZ;
	int imgzEnd = endZ;
	QVector<QVector<QPoint> > output;
	if(orientation == QUANTX::XYPLANE)
		output.resize(image->nz());
	else if(orientation == QUANTX::XZPLANE){
		segy = &imgz;
		segz = &imgy;
		imgyStart = startZ;
		imgyEnd = endZ;
		imgzStart = boundRect.top();
		imgzEnd = boundRect.bottom();
		output.resize(image->ny());
	}
	else if(orientation == QUANTX::YZPLANE){
		segx = &imgy;
		segy = &imgz;
		segz = &imgx;
		imgxStart = startZ;
		imgxEnd = endZ;
		imgyStart = boundRect.left();
		imgyEnd = boundRect.right();
		imgzStart = boundRect.top();
		imgzEnd = boundRect.bottom();
		output.resize(image->nx());
	}

	int firstPostIdx = image->firstPostIdx();
	for(imgz = imgzStart; imgz <= imgzEnd; imgz++){
		for(imgy = imgyStart; imgy <= imgyEnd; imgy++){
			for(imgx = imgxStart; imgx <= imgxEnd; imgx++){
				QVector<double> singleRow(timePts);
				singleRow[0] = image->data()[model_->precontrastIdx_][imgz][imgy][imgx];
				for(int t = 1; t < timePts; t++){
					singleRow[t] = image->data()[t + firstPostIdx - 1][imgz][imgy][imgx];
				}
				voxelData << singleRow;
				pointLocs << PointVector3D<int>(*segx,*segy,*segz);
			}
		}
	}

	bool fatSat = image->scanOptions().contains("FS") || image->scanOptions().contains("PFP");
	QVector<double> weights;
	if(fatSat){
		weights = getWeights(voxelData[0].size(),false);
#if FATSATNORMALIZE
		normalizeByFirstColumn(&voxelData);
#elif FATSATSUBTRACT
		subtractByFirstColumn(&voxelData);
#endif
	}
	else{
		weights = getWeights(voxelData[0].size(),true);
#if NOFATSATNORMALIZE
		normalizeByFirstColumn(&voxelData);
#elif NOFATSATSUBTRACT
		subtractByFirstColumn(&voxelData);
#endif
	}
	if(cmeansCluster < 2)
		cmeansCluster = 2;
#if useDebug
	//qDebug() << "FatSat:" << fatSat << "   ScanOptions:" << image->scanOptions() << "  Feature timepoint weights:" << weights;
	if(clusterCenters)
		qDebug() << "clusterCenters before cmeans:" << *clusterCenters;
#endif
	int max_iter = 100;
	QVector<QVector<double> > cMeansResults = cMeans(voxelData, cmeansCluster, weights, 2.0, max_iter,1.0e-6,false,5, nullptr, mrCMeansSegmentationParameters, nullptr, clusterCenters ? *clusterCenters : QVector<QVector<double> >());
	int mostUpC = getMostUptakingCluster(cMeansResults,weights,cmeansCluster, fatSat);
	if(clusterCenters){
		*clusterCenters = cMeansResults.mid(0, cmeansCluster);
		if(mostUpC != 0)
			clusterCenters->prepend(clusterCenters->takeAt(mostUpC));
	}
#if useDebug
	if(clusterCenters)
		qDebug() << "clusterCenters after cmeans:" << *clusterCenters;
	else
		qDebug() << "clusterCenters is NULL";
#endif
	int endI = cmeansCluster + pointLocs.size();
//#pragma omp parallel for NOTE: Since this is appending to a vector, order might matter (but it shouldn't)
	for(int i = cmeansCluster; i < endI; ++i)
		if(cMeansResults[i][mostUpC] > cmeansCutoffValue)
			output[pointLocs.at(i-cmeansCluster).z()] << QPoint(pointLocs.at(i-cmeansCluster).x(),pointLocs.at(i-cmeansCluster).y());
#if useDebug
	qDebug() << "end fcmRect3D";
#endif
	return output;
}


/*!
 * \brief Generate FCM output from a polygon (point list), start slice, end slice.
 * \param pointList The polygon to perform the FCM segmenation on.
 * \param startZ The start slice.
 * \param endZ The end slice.
 * \param cmeansCutoffValue The maxiumum membership ratio to a cluster center that is used to determine whether a point is part of that cluster.
 * \param cmeansCluster Number of c-means clusters to use.
 * \return FCM polygon, a 3D rectangle (vector of polygons) generated by the fuzzy c-means algorithm (FCM).
 */
inline QVector<QVector<QPoint> > MrAnalysis::fcmPoly1slice(const QVector<QPoint>&  pointList, int startZ, int endZ, double cmeansCutoffValue, int cmeansCluster){
	const auto & image = model_->image;
	const auto & orientation = model_->orientation();
	QVector<QVector<QPoint> > vectorList;
	if(orientation == QUANTX::XYPLANE)
		vectorList.resize(image->nz());
	if(orientation == QUANTX::XZPLANE)
		vectorList.resize(image->ny());
	if(orientation == QUANTX::YZPLANE)
		vectorList.resize(image->nx());
#pragma omp parallel for
	for(int z = startZ; z <= endZ; z++){
		if((z >= 0) && (z < vectorList.size()))
			vectorList[z] = pointList;
	}
	return fcmPoly3D(vectorList,cmeansCutoffValue,cmeansCluster);
}

//
/*!
 * \brief Generate FCM output from a 3D polygon.
 * \param pointList The 3D polygon.
 * \param cmeansCutoffValue The maxiumum membership ratio to a cluster center that is used to determine whether a point is part of that cluster.
 * \param cmeansCluster Number of c-means clusters to use.
 * \return FCM 3D output as a vector of polygons.
 */
inline QVector<QVector<QPoint> > MrAnalysis::fcmPoly3D(QVector<QVector<QPoint> > pointList, double cmeansCutoffValue, int cmeansCluster)
{
	const auto & image = model_->image;
	const auto & orientation = model_->orientation();
	QVector<QVector<QPoint> > output;
	auto timePts = image->nt();
	int segx, segy, segz;
	int *imgx = &segx;
	int *imgy = &segy;
	int *imgz = &segz;
	if(orientation == QUANTX::XYPLANE)
		output.resize(image->nz());
	else if(orientation == QUANTX::XZPLANE){
		imgy = &segz;
		imgz = &segy;
		output.resize(image->ny());
	}
	else if(orientation == QUANTX::YZPLANE){
		imgx = &segy;
		imgy = &segz;
		imgz = &segx;
		output.resize(image->nx());
	}

	//collect region data

	QVector<QVector<double> > voxelData(0);

	QVector<PointVector3D<int> > pointLocs;
	int firstPostIdx = image->firstPostIdx();
	for(segz = 0; segz < pointList.size(); segz++){
		for(int i = 0; i < pointList[segz].size(); i++){
			QVector<double> singleRow(timePts);
			segx = pointList[segz].at(i).x();
			segy = pointList[segz].at(i).y();
			singleRow[0] = image->data().at(model_->precontrastIdx_).at(*imgz).at(*imgy).at(*imgx);
			for(int t = 1; t < timePts; t++){
				singleRow[t] = image->data().at(t + firstPostIdx - 1).at(*imgz).at(*imgy).at(*imgx);
			}
			voxelData << singleRow;
			pointLocs << PointVector3D<int>(segx,segy,segz);
		}
	}

	//Check if image uses Fat Saturation, if not, normalize data to first timepoint
	bool fatSat = image->scanOptions().contains("FS") || image->scanOptions().contains("PFP");
	QVector<double> weights;
	if(fatSat){
		weights = getWeights(voxelData[0].size(),false);
#if FATSATNORMALIZE
		normalizeByFirstColumn(&voxelData);
#elif FATSATSUBTRACT
		subtractByFirstColumn(&voxelData);
#endif
	}
	else{
		weights = getWeights(voxelData[0].size(),true);
#if NOFATSATNORMALIZE
		normalizeByFirstColumn(&voxelData);
#elif NOFATSATSUBTRACT
		subtractByFirstColumn(&voxelData);
#endif
	}

	if(cmeansCluster < 2)
		cmeansCluster = 2;
	int max_iter = 100;
#if useDebug
	qDebug()<<"Feature (timepoint) weights:"<<weights;
#endif
	QVector<QVector<double> > cMeansResults = cMeans(voxelData, cmeansCluster, weights, 2.0, max_iter,1.0e-06,false,5, nullptr ,mrCMeansSegmentationParameters);
	int mostUpC = getMostUptakingCluster(cMeansResults,weights,cmeansCluster,fatSat);
	int endI = pointLocs.size();
//#pragma omp parallel for NOTE: This is appending to a vector, so order matters.
	for(int i = 0; i < endI; i++)
		if(cMeansResults[i + cmeansCluster][mostUpC] > cmeansCutoffValue)
			output[pointLocs.at(i).z()] << QPoint(pointLocs.at(i).x(),pointLocs.at(i).y());
	return output;
}

/*!
 * \brief Normalizes by the first column of a given data vector.
 * \param data The vector to normalize.
 * \return True.
 */
inline bool MrAnalysis::normalizeByFirstColumn(QVector<QVector<double> > *data) {

	int n_rows=data->size();
	int n_cols=data->last().size();

#pragma omp parallel for
	for (int i=0;i<n_rows;i++) {
		//avoid divide by zero
		if (qFuzzyIsNull((*data).at(i).at(0)))
		//weijie always sets zero values to one
			(*data)[i][0] = 1;
		for (int j=1;j<n_cols;j++){
			(*data)[i][j] = static_cast<double>((*data).at(i).at(j)) / (*data).at(i).at(0);
		}
		(*data)[i].remove(0);
	}

	return true;
}

/*!
 * \brief Subtracts data by the first column of a given data vector.
 * \param data Data vector.
 * \return True.
 */
inline bool MrAnalysis::subtractByFirstColumn(QVector<QVector<double> > *data) {

	int n_rows=data->size();
	int n_cols=data->last().size();

#pragma omp parallel for
	for (int i=0;i<n_rows;i++) {
		//avoid divide by zero (not sure if we need qFuzzy or ==, but either shouldn't happen under normal circumstances)
		if (qFuzzyIsNull((*data).at(i).at(0)))
		//weijie always sets zero values to one
			(*data)[i][0] = 1;
		for (int j=1;j<n_cols;j++){
			(*data)[i][j] = static_cast<double>((*data).at(i).at(j)) - (*data).at(i).at(0);
		}
		(*data)[i].remove(0);
	}

	return true;
}

/*!
 * \brief Calculates the size of any region.
 * \param region 3D polygon
 * \return The region size (long).
 */
inline long MrAnalysis::regionSize(const QVector<QVector<QPoint> > & region)
{
	long totalSize = 0;
#pragma omp parallel for reduction(+: totalSize)
	for(auto i = 0; i < region.size(); ++i)
		totalSize += region.at(i).size();
	return totalSize;
}

/**
 * @brief Sets segmentation type of lesion model accoring to segmentation type passed in.
 * @param segmentationType The segmentation type the model is to be set to.
 */
void MrAnalysis::setSegmentationType(QUANTX::SegmentationType segmentationType){
	model_->segmentationType_ = segmentationType;
}

//QUANTX::SegmentationType MrAnalysis::getSegmentationType() const { return model_->segmentationType_; }
