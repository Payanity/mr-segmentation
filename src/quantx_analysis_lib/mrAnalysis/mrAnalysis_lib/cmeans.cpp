//This is no longer Rob's re-implementation of Weijie's fuzzy c-means, which
//is just pretty much your standard fuzzy c-means classifier.
//This now uses a combination of binning and efficient c-means algorithms

#include "cmeans.h"
#include <QHash>
#include <QSet>
//#define _USE_MATH_DEFINES // for C++
#include <cmath>
//#include <QtMath>
#include <limits>
#include "mranalysis.h"
#include <random>

#define useDebug 0
#if useDebug
#include <QElapsedTimer>
#include <QDebug>
#define verboseDebug 0
static QElapsedTimer cmeansTimer;
#endif

struct reducedDataSet {
  //  QHash<unsigned long, QVector<double> > meanData;
  //  QHash<unsigned long, unsigned long> counts;
	QSet<quint32> usedBins;
	QVector<QVector<double> > reducedData;
	QVector<quint32> counts;
};

//inline bool checkNaNinf(const double &d) { return ( (d != d) || (d > std::numeric_limits<double>::max()) || (d < std::numeric_limits<double>::lowest()) ); } // Use std::isfinite()

/*
inline double updateCenters(QVector<QVector<double> > & data, QVector<QVector<double> > & centerpoints, const QVector<double> &weights,double expo)
{
	QVector<QVector<double> > oldCenterpoints(centerpoints->size());
	int nDataPoints = data->size();
	int nCluster = centerpoints->size();
	double denom;
	QVector<double> dsqr(nCluster);
	QVector<double> numer(nCluster);
	double uExp = 1.0/(expo - 1.0);
	double u;
	double cost = 0.0;
	QVector<double> rowSum(nCluster, 0.0);

	//save centerpoints and zero old one
	for(int i = 0; i < nCluster; i++){
		oldCenterpoints[i] = QVector<double>((*centerpoints)[i].size());
		for(int j = 0; j < (*centerpoints)[i].size(); j++){
			oldCenterpoints[i][j] = (*centerpoints)[i][j];
			(*centerpoints)[i][j] = 0.0;
		}
	}

	for(int i = 0; i < nDataPoints; i++){
		denom = 0.0;

		for(int j = 0; j < nCluster; j++){
			dsqr[j] = distSquared((*data)[i], oldCenterpoints[j], weights);
			numer[j] = pow(dsqr[j], uExp);
			denom += pow(numer[j],-1);
		}
		for(int j = 0; j < nCluster; j++){
			u = pow(numer[j] * denom, -expo);
			cost += dsqr[j] * u;
			(*centerpoints)[j] = addVectors((*centerpoints)[j], scaleVector(u, (*data)[i]));
			rowSum[j] += u;
		}
	}
	for(int j = 0; j < nCluster; j++){
		(*centerpoints)[j] =divideVector((*centerpoints)[j], rowSum[j]);

	}

	return cost;
}
*/

/*!
 * \brief Data reduction algorithm for c-means speed optimizations.
 * \param data Raw data for c-means classifier.
 * \param nBins Number of bins to use in each dimension of feature space.
 * \return The reduced data set.
 */
inline reducedDataSet const reduceData(const QVector<QVector<double> > & data, unsigned int nBins)
{
	const int nFeatures = data.at(0).size();
	const int nDataPoints = data.size();
	QVector<double> max;
	QVector<double> min;
	QVector<double> slope;
	QVector<double> intercept;
	reducedDataSet rData;
	QHash<quint32, QVector<double> > sumData;
	QHash<quint32, quint32> counts;
   // QVector<int> featurePowers(nFeatures);

	//! calculate min and max vectors
	//! we would need to create a custom iterator class to use std::minmax_element
	min.resize(nFeatures);
	max.resize(nFeatures);
	slope.resize(nFeatures);
	intercept.resize(nFeatures);

	//auto featureCompare = [=](auto d1, auto d2){return d1.at(j) < d2.at(j);};
	auto featureMinMax = [](const auto & d, const auto & j){auto p = std::minmax_element(d.begin(), d.end(), [=](auto d1, auto d2){return d1.at(j) < d2.at(j);}); return std::make_pair( (*p.first).at(j), (*p.second).at(j));};

	for(int j = 0; j < nFeatures; ++j)
		std::tie(min[j], max[j]) = featureMinMax(data, j);

	/*
#pragma omp parallel for
	for(int j = 0; j < nFeatures; j++){
		max[j] = min[j] = data->at(0).at(j);
	}
	for(int i = 1; i < nDataPoints; i++){
#pragma omp parallel for
		for(int j = 0; j < nFeatures; j++){
			if (min[j] > data->at(i).at(j)) {
				min[j] = data->at(i).at(j);
			} else if (max[j] < data->at(i).at(j)){
				max[j] = data->at(i).at(j);
			}
		}
	}
	*/

	//!calculate slopes and intercepts for binning
#pragma omp parallel for
	for(int i = 0; i < nFeatures; i++){
		if(qFuzzyCompare(min.at(i), max.at(i))){
			min[i] = min.at(i) * 0.9;
			max[i] = max.at(i) * 1.1;
		}
		slope[i] = (nBins - 0.00000001)/(max.at(i) - min.at(i));
		intercept[i] = min[i]*(nBins - 0.00000001)/(min.at(i) - max.at(i));
	}

	//bin data points
//#pragma omp parallel for
	QVector<quint32> mult(nFeatures);
	mult[0] = 1;
	for(int j = 1; j < nFeatures; ++j)
		mult[j] = mult[j - 1] * nBins;

	for(int i = 0; i < nDataPoints; i++){
		quint32 currentBin  = 0; // This should be able to store the value : nbins ^ nfeatures
#pragma omp parallel for reduction (+: currentBin)
		for(int j = 0; j < nFeatures; j++){
			auto tempBin = static_cast<uint>(data.at(i).at(j) * slope.at(j) + intercept.at(j));
			if (Q_UNLIKELY(tempBin >= nBins)){
				//qDebug() << "tempBin:" << tempBin;
				tempBin--;
			}
			currentBin += mult[j] * tempBin;
		}
		QVector<double> & sd = sumData[currentBin];
		if (!rData.usedBins.contains(currentBin)){
			rData.usedBins.insert(currentBin);
			sd.resize(nFeatures);
			counts[currentBin] = 0;
		}
		counts[currentBin]++;
#pragma omp parallel for
		for(int j = 0; j < nFeatures; ++j)
			sd[j] += data.at(i).at(j);
	}

	for(const auto & bin : std::as_const(rData.usedBins) ){
		rData.counts.append(counts[bin]);
		//method 1
		auto convertSumsToMeans = [](auto d, auto c){for(auto & v : d) v /= c; return d;};
		rData.reducedData.append(convertSumsToMeans(sumData[bin], counts[bin]));
		//method 2
		//rData->reducedData.append(divideVector(sumData[bin], counts[bin]));
		//method 3
		/*
		QVector meanData(nFeatures);
		auto cb = counts.at(bin);
#pragma omp parallel for
		for(int j = 0; j < nFeatures; ++j)
			meanData[j] = sumData.at(bin).at(j) / cb;
		rData->reducedData.append(meanData);
		*/
	}

	return rData;
}

/*!
 * \brief Creates random center points for each nCluster using a random seed.
 * \param nCluster Number of clusters to calculate random centerpoints for.
 * \param nFeatures Number of features each cluster contains
 * \param randSeed Used to generate the random number.
 * \return A vector of clusters of centerpoints.
 */
inline QVector<QVector<double> > createRandCenterpoints(const int &nCluster, const int &nFeatures, const int &randSeed = 0)
{
#if useDebug
	qDebug() << "createRandCenterpoints";
#endif
	QVector<QVector<double> > centerpoints;
	centerpoints.resize(nCluster);
	srand(static_cast<uint>(randSeed));
	//std::mt19937 g(randSeed);
	//std::uniform_real_distribution<double> dist(-500, 500); // We can use this to generate reasonable starting values

//#pragma omp parallel for // If this isn't ordered, the random numbers show up in different places every time.
	// Changing the order is not necessarily a bad thing, but needs to be tested and could make for more difficult auditing.
	// Using the ordered keywords in the correct spots seemingly helps with consistency of the first nFeatures values.
	// Perhaps srand() only affects a single thread? Maybe the compiler optimizations affect the generated values?
	for(int i = 0; i < nCluster; i++){
		centerpoints[i].resize(nFeatures);
		for(int j = 0; j < nFeatures; j++){
			centerpoints[i][j] = 500.0 - static_cast<double>(rand() * 1000.0) / RAND_MAX;
			//centerpoints[i][j] = dist(g);
		}
	}
#if useDebug
	qDebug() << "Random Centerpoints:" << centerpoints;
#endif
	return centerpoints;
}

/*!
 * \brief Scales each value in a vector based on the scalar given.
 * \param scalar Value to scale vector by.
 * \param vector The original vector.
 * \return The scaled vector.
 */
inline QVector<double> scaleVector(const double & scalar, const QVector<double> & vector)
{
	//returns a scaled vector
	const int size = vector.size();
	QVector<double> ans;
	ans.resize(size);

#pragma omp parallel for
	for(int i = 0; i < size; i++){
		ans[i] = vector.at(i) * scalar;
	}

	return ans;
}

/*!
 * \brief Adds each value in a vector with the value in the equivalent index of the other vector.
 * \param vec1
 * \param vec2
 * \return A vector with the added values.
 */
inline QVector<double> addVectors(const QVector<double> & vec1, const QVector<double> & vec2)
{
   const int size = vec1.size();
   QVector<double> sum;
   sum.resize(size);

#pragma omp parallel for
   for(int i = 0; i < size; i++){
	   sum[i] = vec1.at(i) + vec2.at(i);
   }

   return sum;
}

/*!
 * \brief Divides each value in a vector based on the scalar given.
 * \param vec The original vector.
 * \param scalar Value to divide vector by.
 * \return A vector with the divided values.
 */
inline QVector<double> divideVector(const QVector<double> & vec, const double & scalar)
{
	const int size = vec.size();
	QVector<double> ans;
	ans.resize(size);

#pragma omp parallel for
	for(int i = 0; i < size; i++){
		ans[i] = vec.at(i) / scalar;
	}

	return ans;
}

/*!
 * \brief Squares the distance between two vectors of equal length.
 * \param vec1
 * \param vec2
 * \param weights
 * \return The square of the distance between two vectors of equal length
 */
inline double distSquared(const QVector<double> & vec1, const QVector<double> & vec2, const QVector<double> &weights)
{
	const int size = vec1.size();
	double sum = 0.0;

#pragma omp parallel for reduction(+: sum)
	for(int i = 0; i < size; i++){
		sum += weights.at(i) * pow(vec1.at(i) - vec2.at(i), 2);
	}

	return sum;
}

/*!
 * \brief Generates the membership matrix for c-means algorithm
 * \details Generates a vector of the membership ratios for each c-means cluster for every point input to the c-means algorithm.
 * \param data Input data for the c-means algorithm.
 * \param centerpoints The centerpoint for each c-means cluster.
 * \param weights Weights for each timepoint used in the c-means algorithm.
 * \param expo The exponent used for the c-means algorithm.
 * \return A vector of vector of doubles.
 */
inline QVector<QVector<double> > generateMembershipMatrix(const QVector<QVector<double> > & data, QVector<QVector<double> > & centerpoints, const QVector<double> &weights, double expo)
{
#if useDebug
	qDebug() << "start generateMembershipMatrix";
#endif
	int nDataPoints = data.size();
	int nCluster = centerpoints.size();
	QVector<QVector<double > > membershipVals;
	membershipVals.resize(nDataPoints);
#pragma omp parallel for
	for (int i =0; i < nDataPoints; i++){
		membershipVals[i].resize(nCluster);
	}

#pragma omp parallel for
	for(int i = 0; i < nDataPoints; i++){
#if useDebug && verboseDebug
		//qDebug() << QString("running on %1 of %2 data points").arg(i+1).arg(nDataPoints);
#endif
		double denomSum = 0.0;
		QVector<double> distances(nCluster);
//#pragma omp parallel for reduction(+: denomSum)
		for(int j = 0; j < nCluster; j++){
			distances[j] = pow(distSquared(data.at(i), centerpoints.at(j), weights), 1.0/(expo-1.0));
			denomSum += 1.0/distances.at(j);
		}
		if (!std::isfinite(denomSum)){
			for(int j = 0; j < nCluster; j++){
				if (distances.at(j) == 0.0){
					membershipVals[i][j] = 1.0;
				}else{
					membershipVals[i][j] = 0.0;
				}
			}
		}else {
			for(int j = 0; j < nCluster; j++){
				membershipVals[i][j] = 1.0/(distances.at(j) * denomSum);
			}
		}
	}
#if useDebug
	qDebug() << "finish generateMembershipMatrix";
#endif
	return membershipVals;
}

/*!
 * \brief Updates the c-means centerpoints using input from the data reduction optimization.
 * \param rData Data for input to the c-means algorithm that has been output from reduceData().
 * \param centerpoints Current c-means centerpoints.
 * \param weights Weights for each timepoint used in the c-means algorithm.
 * \param expo The exponent for the c-means algorithm.
 * \return Cost.
 */
inline double updateCentersDataReduced(const reducedDataSet & rData, QVector<QVector<double> > & centerpoints, const QVector<double> & weights, const double & expo)
{
#if useDebug
	qDebug() << "updateCentersDataReduced";
#endif
	int nDataPoints = rData.usedBins.size();
	int nCluster = centerpoints.size();
	int nFeatures = centerpoints.first().size();
	double uExp = 1.0/(expo - 1.0);
	double cost = 0.0;
	QVector<double> rowSum;
	rowSum.resize(nCluster);

	//save centerpoints and zero old one
	//QVector<QVector<double> > oldCenterpoints = *centerpoints;
	//QVector<QVector<double> > newCenterpoints = QVector<QVector<double> >(nCluster, QVector<double>((*centerpoints).first().size()));
	QVector<QVector<double> > newCenterpoints;
	newCenterpoints.resize(nCluster);
#pragma omp parallel for
	for(int j = 0; j < nCluster; j++){
		newCenterpoints[j].resize(nFeatures);
//#pragma omp ordered
		//newCenterpoints.append(std::vector<double>((*centerpoints).first().size(), 0.0));
	}

//#pragma omp parallel
//    {
#pragma omp parallel for reduction(+: cost)
	for(int i = 0; i < nDataPoints; i++){
		double denom = 0.0;
		QVector<double> dsqr(nCluster);
		QVector<double> numer(nCluster);
		double u;

		// Run through once to get the denominator
//#pragma omp for reduction(+: denom)
		for(int j = 0; j < nCluster; j++){
			dsqr[j] = distSquared(rData.reducedData.at(i), centerpoints.at(j), weights);
			numer[j] = pow(dsqr.at(j), uExp);
			denom += pow(numer.at(j),-1);
		}
		for(int j = 0; j < nCluster; j++){
			if(numer[j] == 0.0)
				u = 1.0 * rData.counts.at(i);
			else if(std::isinf(denom))
				u = 0.0;
			else
				u = pow(numer.at(j) * denom, -expo) * rData.counts.at(i);

			auto scaledVector = scaleVector(u, rData.reducedData.at(i));
			QVector<double> & centerpointVector = newCenterpoints[j];
			for(int k = 0; k < nFeatures; ++k){
				const double & scaledVal = scaledVector.at(k);
#pragma omp atomic
				centerpointVector[k] += scaledVal;
				//const_cast<double &>(newCenterpoints.at(j).at(k)) += scaledVal;
				//newCenterpoints[j] = addVectors(newCenterpoints.at(j), scaledVector);
			}
#pragma omp atomic
			rowSum[j] += u;
			cost += dsqr.at(j) * u;
		}
#if useDebug
		if(std::isinf(denom))
			qDebug() << "Caught Infinity in denominator!";
#endif
	}
//    }

#if useDebug
	qDebug() << "Done. Scale centerpoints and return";
#endif

//#pragma omp parallel for // This causes a crash, even when using at(j) instead of [j]. Maybe something funny happens to centerpoints.
	for(int j = 0; j < nCluster; j++){
		centerpoints[j] =divideVector(newCenterpoints.at(j), rowSum.at(j));
		//for(int k = 0; k < centerpoints->at(j).size(); ++k)
		//    (*centerpoints)[j][k] = newCenterpoints.at(j).at(k) / rowSum.at(j);
	}

	return cost;
}

/*!
 * \brief Runs the c-means algorithm using data reduction optimizatoins for a fixed number of bins.
 * \details This should be used as a single stage of the overall c-means algorithm. Will output only the final centerpoints of each cluster unless either outputObjFn or outputMatrix are set to true.
 * \param data Raw data used as input for the c-means algorithm.
 * \param nCluster The number of clusters to use for the c-means algorithm.
 * \param weights The weighting that should be used for each dimension of the input data.
 * \param expo The exponent to use for the c-means algorithm.
 * \param max_iter The maximum number of iterations to run before stopping the algorithm.
 * \param min_improve The minimum improvement in the objective function before stopping the algorithm.
 * \param outputObjFn Set this flag to true to include all incremental objective functions as part of the output data.
 * \param outputMatrix Set this flag to true to include the final membership matrix as part of the output data.
 * \param randSeed Seed value to use for random number generator (setting to a fixed value is useful for auditing and debugging purposes).
 * \param progress Current value of progress to use for emitting signals (if either progress or mrAnalysis are set to nullptr, no signals will be emitted).
 * \param initialCenterpoints This may be used to set the starting centerpoints instead of randomly generated centerpoints (optional).
 * \param nBins The number of bins to use in each dimension of feature space during the data reduction step.
 * \param mrAnalysis MrAnalysis object used to send signals for progress updates (if either progress or mrAnalysis are set to nullptr, no signals will be emitted)
 * \return Vector of data reduced by the c-means algorithm.
 */
inline QVector<QVector<double> > cMeansDataReduced(const QVector<QVector<double> > & data, int nCluster, const QVector<double> &weights, double expo, int max_iter, double min_improve, bool outputObjFn, bool outputMatrix, int randSeed, int * progress, const QVector<QVector<double> > & initialCenterpoints, unsigned int nBins, MrAnalysis * mrAnalysis)
{
	if(expo <= 1.0)
		return QVector<QVector<double> >(0);

	const auto rData = reduceData(data, nBins);
	QVector<QVector<double> > prevIterCenterpoints;
	QVector<double> objectiveFunctionVals;
	int progressStart = 0;
#if useDebug
	//qDebug()<< "Number of Feature (rdata):"<<rData->reducedData[0].size();
	//qDebug()<< "Number of Actual features:"<<(*data)[0].size();
#endif
	if(progress)
		progressStart = *progress;

	auto centerpoints = initialCenterpoints;
	bool createRandom = centerpoints.isEmpty(); // First update, or we hit NAN
	//if (firstUpdate){
	    auto firstObjFunc = 0.;
		do{
			if(createRandom){
				centerpoints = createRandCenterpoints(nCluster, data.first().size(), randSeed);
				randSeed++; //increment randSeed if we get horrible points for clustering
			}
			firstObjFunc = updateCentersDataReduced(rData, centerpoints, weights, expo);
#if useDebug && verboseDebug
			qDebug() << "First Objective Function:" << firstObjFunc;
			if(!std::isfinite(firstObjFunc)){
				if(firstObjFunc > std::numeric_limits<double>::max())
					qDebug() << "Detected Positive Infinity Value";
				else if(firstObjFunc < std::numeric_limits<double>::lowest())
					qDebug() << "Detected Negative Infinity Value";
				else if(std::isnan(firstObjFunc)) //running this test because the first two are shit
					qDebug() << "Detected NaN";
			}
#endif
			createRandom = true; // If objective function is NaN, start with some new random centerpoints
		}while(!std::isfinite(firstObjFunc));
		objectiveFunctionVals.append(firstObjFunc);
	//}
	//else {
	//	objectiveFunctionVals.append(updateCentersDataReduced(rData, centerpoints, weights, expo));
	//}
	int iter = 1;

	while(iter <  max_iter){
		prevIterCenterpoints = centerpoints;
		objectiveFunctionVals.append(updateCentersDataReduced(rData, centerpoints, weights, expo));
#if useDebug && verboseDebug
		qDebug() << QString("Iteration %1 Objective Function: %2   -   Change: %3   -   Normalized: %4").arg(iter+1).arg(objectiveFunctionVals[iter]).arg(qAbs(objectiveFunctionVals[iter] - objectiveFunctionVals[iter-1])).arg(qAbs(objectiveFunctionVals[iter] - objectiveFunctionVals[iter-1]) / objectiveFunctionVals[iter - 1]).toUtf8().data();
		if(objectiveFunctionVals[iter] > std::numeric_limits<double>::max())
			qDebug() << QString("Iteration %1 Objective Function: Detected Positive Infinity Value").arg(iter+1).toUtf8().data();
		else if(objectiveFunctionVals[iter] < std::numeric_limits<double>::lowest())
			qDebug() << QString("Iteration %1 Objective Function: Detected Negative Infinity Value").arg(iter+1).toUtf8().data();
		else if(std::isnan(objectiveFunctionVals[iter]))
			qDebug() << QString("Iteration %1 Objective Function: Detected NaN").arg(iter+1).toUtf8().data();
#endif
		if (!std::isfinite(objectiveFunctionVals[iter])){ //objective function is NaN
			centerpoints = prevIterCenterpoints;
			objectiveFunctionVals.remove(iter);
			break;
		}
		if(qAbs(objectiveFunctionVals[iter] - objectiveFunctionVals[iter-1]) / objectiveFunctionVals[iter] < min_improve) //little or no improvement in centroid locations
			break;
		if(progress && mrAnalysis){
			*progress = progressStart + iter;
			mrAnalysis->updateProgress();
		}
		iter++;
	}

#if useDebug
	qDebug() << "reduced cmeans iterations" << iter << "with" << rData.usedBins.size() << "out of" << data.size() << "datapoints";
	qDebug() << "Centers: " << centerpoints;
#endif

	QVector<QVector<double> > output;
	if(outputObjFn)
		output.append(objectiveFunctionVals);
	output << centerpoints;
	if(outputMatrix)
		output << generateMembershipMatrix(data, centerpoints, weights, expo);
	if(progress && mrAnalysis){
		*progress = progressStart + max_iter;
		mrAnalysis->updateProgress();
	}

#if useDebug
	qDebug() << "delete rData and return";
#endif

	//delete rData;
	return output;
}

//!
//! \brief Perform fuzzy c-means clustering on the specified input data, number of clusters, and clustering parameters.
//! \param data
//! \param nCluster
//! \param weights
//! \param expo
//! \param max_iter
//! \param min_improve
//! \param outputObjFn
//! \param randSeed
//! \param progress
//! \param nBins
//! \param mrAnalysis
//! \param centerpoints
//! \return
//!
//! \prePost{Cluster center points and membership data the input data returned.}
//!
QVector<QVector<double> > cMeans(const QVector<QVector<double> > & data, int nCluster, const QVector<double> &weights, double expo, int max_iter, double min_improve, bool outputObjFn, int randSeed, int * progress, QVector<unsigned int> nBins, MrAnalysis *mrAnalysis, QVector<QVector<double> > centerpoints)
{
#if useDebug
	qDebug() << "C-Means iteration started" << cmeansTimer.elapsed() << "ms after last iteration finished";
	qDebug() << "Number of bins to use:" << nBins;
	cmeansTimer.restart();
#endif

	//int nFeatures = (*data)[0].size();
	//QVector<QVector<double> > centerpoints;
	//QVector<QVector<double> > * cpoints = nullptr;
	//if(!centerpoints.isEmpty())
	//    cpoints = &centerpoints;
	QVector<QVector<double> > stagecenterpoints;
	uint stagebins = nBins[0];

	if(expo <= 1.0){
		QVector<QVector<double> > badExpo(0);
		return badExpo;
	}
	if (nBins.size() > 1){
		do {
			if(progress)
				*progress = 0;
			stagecenterpoints = cMeansDataReduced(data, nCluster, weights, expo, max_iter, min_improve, false, false, randSeed, progress, centerpoints, stagebins++, mrAnalysis);
		}while ( !std::isfinite(stagecenterpoints[0][0]) ); // RNG seed gets incremented if we end up with an invalid result
		centerpoints = stagecenterpoints;
#if useDebug
		if(stagebins != nBins[0] + 1)
			qDebug() << "Number of bins changed from" << nBins[0] << "to" << stagebins - 1;
#endif
		//cpoints = &centerpoints;
		if (nBins.size() > 2){
			for(int i = 1; i < nBins.size() - 1; i++){
				stagebins = qMax(stagebins, nBins[i]);
				do {
					if(progress)
						*progress = max_iter;
					stagecenterpoints = cMeansDataReduced(data, nCluster, weights, expo, max_iter, min_improve, false, false, randSeed, progress, centerpoints, stagebins++, mrAnalysis);
				}while ( !std::isfinite(stagecenterpoints[0][0]) ); // RNG seed gets incremented if we end up with an invalid result
				centerpoints = stagecenterpoints;
#if useDebug
				if(stagebins != nBins[i] + 1)
					qDebug() << "Number of bins changed from" << nBins[i] << "to" << stagebins - 1;
#endif
			}
		}
	}
	stagebins = qMax(stagebins, nBins.last());
	do {
		if(progress)
			*progress = max_iter * (nBins.size() - 1);
		stagecenterpoints = cMeansDataReduced(data, nCluster, weights, expo, max_iter, min_improve, outputObjFn, true , randSeed, progress, centerpoints, stagebins++, mrAnalysis);
	}while ( !std::isfinite(stagecenterpoints[0][0]) ); // RNG seed gets incremented if we end up with an invalid result
#if useDebug
	if(stagebins != nBins.last() + 1)
		qDebug() << "Number of bins changed from" << nBins.last() << "to" << stagebins - 1;
	qDebug() << "C-Means iteration time to finish:" << cmeansTimer.elapsed() << "ms";
	cmeansTimer.restart();
#endif
	return stagecenterpoints;
}

/*
QVector<QVector<double> > cMeans(const QVector<QVector<double> > & data, int nCluster, double expo, int max_iter, double min_improve, bool outputObjFn, int randSeed, int * progress)
{
#if useDebug
   QElapsedTimer cmeansTimer;
   cmeansTimer.start();
#endif
	int nFeatures = (*data)[0].size();
	QVector<QVector<double> > centerpoints(nCluster);
	QVector<double> objectiveFunctionVals;
	QVector<QVector<double> > output;
	int iter;
	int progressStart = 0;

	if(progress)
		progressStart = *progress;
	if(expo <= 1.0){
		QVector<QVector<double> > badExpo(0);
		return badExpo;
	}
	srand(randSeed);
	for(int i = 0; i < nCluster; i++){
		centerpoints[i] = QVector<double>(nFeatures);
		for(int j = 0; j < nFeatures; j++){
			centerpoints[i][j] = 500.0 - static_cast<double>(rand() * 1000.0) / RAND_MAX;
		}
	}

	objectiveFunctionVals.append(updateCenters(data, &centerpoints, expo));
	iter = 1;
	while(iter <  max_iter){
		objectiveFunctionVals.append(updateCenters(data, &centerpoints, expo));
		if(qAbs(objectiveFunctionVals[iter] - objectiveFunctionVals[iter-1]) < min_improve){

			break;

		}
		if(progress)
			*progress = progressStart + iter;
		iter++;
	}
#if useDebug
	qDebug() << "not reduced cmeans iterations" << iter;
#endif

	if(outputObjFn)
		output.append(objectiveFunctionVals);
	output << centerpoints;
	output << generateMembershipMatrix(data, &centerpoints, expo);
	if(progress)
		*progress = progressStart + max_iter;
#if useDebug
	qDebug() << "took " << cmeansTimer.elapsed() << " ms without data reduction";
	cmeansTimer.restart();
	QVector<QVector<double> > output2 = cMeansDataReduced(data, nCluster, expo, max_iter, min_improve, outputObjFn, true, randSeed, progress, NULL, 60);
	qDebug() << "took " << cmeansTimer.elapsed() << " ms with data reduction";
	QVector<uint> nBins;
	nBins << 10 << 20 << 40 << 60 << 80 << 100;
	cmeansTimer.restart();
	QVector<QVector<double> > output3 = efficientcMeans(data, nCluster, expo, max_iter, min_improve, outputObjFn, randSeed, progress, nBins);
	qDebug() << "took " << cmeansTimer.elapsed() << " ms with efficient";

	QVector<double> error1;
	QVector<double> error2;
	for(int i=0; i<output[0].size(); i++){
		error1 << (output[0][i] - output2[0][i]) / output[0][i];
		error2 << (output[0][i] - output3[0][i]) / output[0][i];
	}
	qDebug() << "output error in data reduction:" << error1;
	qDebug() << "output error in efficient:" << error2;

	return output2;
#endif
	return output;
}
*/
