#ifndef DEBUGTABLEMAKER_H
#define DEBUGTABLEMAKER_H

// This is just a helper class for creating a table that is used during debugging

#include "../../mammoFeats/mammofeats_main/copytablewidget.h"
#include <QWindow>
#include <QMainWindow>
#include <QApplication>
#include <QStringList>
#include <QVariant>

//!
//! \brief This is just a helper class for creating a table that is used during debugging
//! \ingroup MRAnalysisModule
//!
class DebugTableMaker : public QObject {
	Q_OBJECT
public:
	DebugTableMaker(QVector<QStringList> data) : stringData(data) {}

	DebugTableMaker(const QVector<QVariant> & data) {
		for(const auto & row : data)
			stringData << row.toStringList();
	}

	template <class T> DebugTableMaker(QVector<QVector<T> > data) {
		int cols, rows = data.size();
		if(rows > 0)
			cols = data.first().size();
		for(int i = 0; i < rows; ++i){
			stringData.append( QStringList() );
			for(int j = 0; j < cols; ++j)
				stringData[i].append( QVariant(data.at(i).at(j) ).toString() );
		}
	}

	Q_INVOKABLE void makeTable() {
		int cols = 0, rows = stringData.size();
		if(rows > 0)
			cols = stringData.first().size();
		if(windowTitle.isEmpty())
			windowTitle = "Debug Table";

		QWidget * quantxGui = Q_NULLPTR;
		const auto topLevelWidgets = qApp->topLevelWidgets();
		for(const auto & widget : topLevelWidgets)
			if(widget->objectName() == "QuantXMainWindow"){
				quantxGui = widget;
				break;
			}

		CopyTableWidget * tableWidget = new CopyTableWidget( rows, cols, quantxGui );
		tableWidget->setWindowFlags(Qt::Window);
		tableWidget->setWindowTitle(windowTitle);
		tableWidget->setMinimumWidth(980);
		tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
		tableWidget->setAlternatingRowColors(true);
		tableWidget->setAttribute(Qt::WA_DeleteOnClose);
		tableWidget->show();
		tableWidget->activateWindow();
		for(int i = 0; i < rows; ++i)
			for(int j = 0; j < cols; ++j){
				QTableWidgetItem *resultItem = new QTableWidgetItem( stringData.at(i).at(j) );
				resultItem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
				tableWidget->setItem(i, j, resultItem);
			}
		//glcmTableWidget->update();
		qApp->processEvents();
		emit finished();
	}

	void setWindowTitle(QString title){ windowTitle = title; }
signals:
	void finished();
private:
	QVector<QStringList> stringData;
	QString windowTitle;
};

#endif // DEBUGTABLEMAKER_H
