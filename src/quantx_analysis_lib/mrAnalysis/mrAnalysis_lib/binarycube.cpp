#include "binarycube.h"
#include <QVector>
#include <QList>
#include <QtAlgorithms>
#include <utility>
#include <array>
#define useDebug 0
#if useDebug
#include <QDebug>
#endif

//!
//! \ctorBrief{binarycube}
//! \param verts Sets which vertices of the cube are ‘on’.
//!
//! \ctorPrePost
//!
binarycube::binarycube(const QVector<std::array<bool, 3> > &verts)
{
	for(const auto & vert : verts)
		vertices.push_back(vert);
	std::sort(vertices.begin(), vertices.end(), vectorCompareBoolean);
}

//!
//! \overload
//!
binarycube::binarycube(std::vector<std::array<bool, 3> > verts) :
    vertices{std::move(verts)}
{
	std::sort(vertices.begin(), vertices.end(), vectorCompareBoolean);
}


bool binarycube::operator ==(const binarycube & other) const
{

	if (vertices.size() == other.vertices.size()){

		//check to see if any of the 24 rotations match

		for(int yaw = 0; yaw < 4; yaw++){
			for(int roll = 0; roll < 4; roll++){
				binarycube copy(vertices);
				copy.rotate(yaw,0,roll);
				if (copy.vertices == other.vertices)
					return true;
			}
		}
		for(int roll = 0; roll < 4; roll++){
			binarycube copy(vertices);
			copy.rotate(0,1,roll);
			if (copy.vertices == other.vertices)
				return true;
		}
		for(int roll = 0; roll < 4; roll++){
			binarycube copy(vertices);
			copy.rotate(0,3,roll);
			if (copy.vertices == other.vertices)
				return true;
		}
	}
	return false;
}

//!
//! \brief Rotate the cube by the specified yaw, pitch, and roll values.
//! \param yaw
//! \param pitch
//! \param roll
//!
//! \preInitPost{The cube is rotated by the specified amounts.}
//!
inline void binarycube::rotate(int yaw, int pitch, int roll){

	auto nVertices = vertices.size();

	//yaw
	auto rotatelambda = [=](size_t i1, size_t i2, int rotations){
		auto points = std::vector<std::array<bool, 3> >();
		for (auto v : vertices){
			points.push_back({v[i1], v[i2]});
		}
		rotate2DPoints(points, rotations);
		for (size_t i = 0; i < nVertices; i++){
			vertices[i][i1] = points[i][0];
			vertices[i][i2] = points[i][1];
		}
	};
	rotatelambda(0, 1, yaw);
	rotatelambda(0, 2, pitch);
	rotatelambda(1, 2, roll);

	std::sort(vertices.begin(), vertices.end(), vectorCompareBoolean);
}

//!
//! \brief Rotate 2D points of a binary square about the specified number of 90 degree counter-clockwise rotations.
//! \param vec
//! \param counterclockwiseRotations
//!
//! \preInitPost{The 2D points of the binary square are rotated by the specified number of 90 degree counter-clockwise rotations.}
//!
inline void binarycube::rotate2DPoints(std::vector<std::array<bool, 3> > & vec, int counterclockwiseRotations){
	counterclockwiseRotations = counterclockwiseRotations % 4;

	for (auto & v : vec){
		for (int j = 0; j < counterclockwiseRotations; j++){
			if (!v[0] && !v[1]){
				v[0] = true;
			} else if(v[0] && !v[1]){
				v[1] = true;
			} else if(v[0] &&  v[1]){
				v[0] = false;
			} else if (!v[0] && v[1]){
				v[1] = false;
			}

		}
	}
}

/*
inline bool vectorCompare(const QVector<int> &v1, const QVector<int> &v2){
	//compares two three dimensional vectors representing 3D points
	//and returns true if the first is 'less than or equal to' the second

	const auto & x1 = v1[0];
	const auto & y1 = v1[1];
	const auto & z1 = v1[2];
	const auto & x2 = v2[0];
	const auto & y2 = v2[1];
	const auto & z2 = v2[2];

	if (x1 < x2)
		return true;
	if(x1 == x2){
		if (y1 < y2)
			return true;
		if (y1 == y2){
			if (z1 <= z2)
				return true;
		}

	}

	return false;
} */

//!
//! \brief Compare two 3D vectors and return true if the first is less than or equal to the second.
//! \param v1
//! \param v2
//! \return
//!
//! \prePost{True is returned if the first vector is less than or equal to the second\, otherwise false returned.}
//!
constexpr bool binarycube::vectorCompareBoolean(const std::array<bool, 3> &v1, const std::array<bool, 3> &v2){
	const auto & x1 = v1[0];
	const auto & y1 = v1[1];
	const auto & z1 = v1[2];
	const auto & x2 = v2[0];
	const auto & y2 = v2[1];
	const auto & z2 = v2[2];

	if (x2 && !x1)
		return true;
	if(x1 == x2){
		if (y2 && !y1)
			return true;
		if (y1 == y2){
			if (z2 || !z1)
				return true;
		}
	}
	return false;
}

//!
//! \brief Invert which vertices of the binary cube are ‘on’.
//!
//! \preInitPost{The binary cube is inverted.}
//!
void binarycube::invert()
{
	std::vector<std::array<bool, 3> > allVertices{{0, 0, 0},
		                                          {0, 0, 1},
		                                          {0, 1, 0},
		                                          {0, 1, 1},
		                                          {1, 0, 0},
		                                          {1, 0, 1},
		                                          {1, 1, 0},
		                                          {1, 1, 1}};

	for(const auto & v : vertices )
		allVertices.erase( std::remove(allVertices.begin(), allVertices.end(), v), allVertices.end() );
	vertices = allVertices;
	std::sort(vertices.begin(), vertices.end(), vectorCompareBoolean);
}
