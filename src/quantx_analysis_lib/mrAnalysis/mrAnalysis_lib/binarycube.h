#ifndef BINARYCUBE_H
#define BINARYCUBE_H

#include "mranalysisdllcheck.h"
#include <QList>
#include <array>

//!
//! \brief This is a helper class used in the calculation of the surface area of a lesion.
//! \ingroup MRAnalysisModule
//!
//! \funcReqBegin
//! \funcReq40
//! \funcReqEnd
//!
class QIMRANALYSIS_EXPORT binarycube
{
public:
	binarycube(const QVector<std::array<bool, 3> > & verts);
	binarycube(std::vector<std::array<bool, 3> >  verts);
	bool operator == (const binarycube & other) const;
	bool operator != (const binarycube & other) const { return !((*this) == other) ;}
	void rotate(int yaw, int  pitch, int  roll);
	void invert();

	constexpr static bool vectorCompareBoolean(const std::array<bool, 3> &v1, const std::array<bool, 3> &v2);

protected:
	std::vector<std::array<bool, 3> > vertices; //!<Which vertices of the cube are ‘on’.

private:
	void rotate2DPoints(std::vector<std::array<bool, 3> > &vec, int counterclockwiseRotations);
	void sortVertices();
};

//bool vectorCompare(const QVector<int> &v1, const QVector<int> &v2);


#endif // BINARYCUBE_H
