#include "mrlesionmodel.h"

//!
//! \ctorBrief{MRLesionModel}
//! \param i MRI Image
//!
//! \ctorPrePost
//!
MRLesionModel::MRLesionModel(QiMriImage * i)
    : image{i}
{
	precontrastIdx_ = i->precontrastIdx();
}
