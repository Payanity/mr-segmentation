#ifndef MRANALYSIS_H
#define MRANALYSIS_H

#include "mranalysisdllcheck.h"
#include "mrlesionmodel.h"
#include <QObject>
#include <QPoint>
#include <QRect>
#include <QVector3D>

//class QTimer;

/*!
 * \defgroup MRAnalysisModule MR Analysis Module
 * This class contains the functions needed for advanced image processing
 * and analytics performed on MR image data.
 *
 * \class MrAnalysis
 * \brief The QuantX MR Analysis Module
 * \ingroup MRAnalysisModule
 *
 * This class contains the functions needed for advanced image processing
 * and analytics performed on MR image data.
 *
 * ## Functional Requirements
 *
 * These functional requirements are based on the SRS and architecture,
 * detailing all primary goals of the module design.
 *
 * Requirement Name | SRS | Description
 * ---------------- | --- | -----------
 * Segmentation of ROI Based on Seed Point | 3.2.36 | Run a segmentation algorithm on an DCE-MRI image using the seed point specified.
 * Display of Segmentation Outline on Displayed Image | 3.2.37 | The Analysis module generates images of the outline that the UI modules displays.
 * Segmentation of ROI Based on Manual Outline | 3.2.38 | Generate an ROI based on manual outline that may be used for statistical analysis.
 * Perform Statistical Analysis on ROI | 3.2.40 | Run statistical analysis on an ROI, and in advanced mode generate a QI score.
 * Display Results of Statistical Analysis | 3.2.41 | The Analysis module is responsible for storing the feature attributes.
 * Select Similar Images Based on Statistical Analysis (Advanced Only) | 3.2.42 | The Analysis module is responsible for storing the feature attributes.
 * Change Active ROI Selection | 3.2.43 | The Analysis module is responsible for storing the feature attributes and generating ROI images.
 * Select Different Feature for Statistical Analysis Display (Advanced Only) | 3.2.44 | The Analysis module is responsible for storing the feature attributes.
 * Save Report | 3.2.50 | Information about the lesions are saved to the report.
 * Add Lesion Information to Database | 3.2.53 | Store all attributes in a database for later retrieval.
 * Retrieve Lesion Information from Database | 3.2.54 | Load all attributes from a database from a previous object.
 *
 */

class QIMRANALYSIS_EXPORT MrAnalysis : public QObject
{
	Q_OBJECT

public:
	MrAnalysis(std::shared_ptr<MRLesionModel> model, QObject *parent = nullptr, QVector<uint> cmeansSegmentBins = QVector<uint>(1, 2));
	QVector<QVector<QPolygon> > segmentSinglePoint(PointVector3D<int> start, bool SingleSliceOnly = false);
	QVector<QVector<QPolygon> > segmentSinglePoint(PointVector3D<int> start, int nClusters, double cutoff, int initialSize, bool SingleSliceOnly = false);
	QVector<QVector<QPoint> > getFilledRegion(QUANTX::Orientation outputOrientation, const bool includeBorders = true) const;
	void getSegmentation(QVector<QImage> *segmentationImage, uint color, bool drawSeed = false) const;
	bool calculateFeatures(QVector<uint> cmeansBins = QVector<uint>(1, 5));
	bool segmentationDone() const;

	//QUANTX::SegmentationType getSegmentationType() const;
	void setSegmentationType(QUANTX::SegmentationType);
	bool setSegmentation(const QVector<QVector<QPolygon> > &s, int x_seed = -1, int y_seed = -1, int z_seed = -1);

	bool analysisDone() const;
	//QVector<double> getFeatures() const;
	//QVector<double> getBnnFeats();
	//QVector<double> getHistFeats();
	//QVector<double> getAvgUptake() const;
	//QVector<double> getMostUptaking() const;
	//double getPlotMaxValue() const;
	QPair<double, double> normalizationFactor(const int &index) { return normalizationFactors.at(index); }
	QVector<QRegion> getRegions(QUANTX::Orientation = QUANTX::NOPLANE) const;
	//QByteArray json();

	//Static Functions (do not use global variables)
	static QVector<bool> checkBorders(QRect rect, const QVector<QPoint>& lesion2D);
	static QVector<int> checkBorderCount(const QRect &rect, const QVector<QPoint> &lesion2D);
	static bool updateGrowCount(QVector<int> *growcount, QVector<bool> borders);
	static QPointF regionCentroid(const QVector<QPoint>& region2D);
	static QVector<double> regionEllipse(const QVector<QPoint>& region2D, double dx, double dy);
	static bool normalizeByFirstColumn(QVector<QVector<double> > *data);
	static bool subtractByFirstColumn(QVector<QVector<double> > *data);
	static long regionSize(const QVector<QVector<QPoint> > &region);
	static QVector<QPolygon> traceBorders2D(QVector<QPoint> pointList);
	static QStringList getFeatureNames();

	//void setSeedPoint(const int & x, const int & y, const int & z){ xSeed = x; ySeed = y; zSeed = z; } // This makes more sense being part of setSegmentation()

Q_SIGNALS:
	void startedSegmentation();
	void finishedSegmentation();
	void startedMRAnalysis();
	void finishedMRAnalysis(QVector<double>,QVector<double>,double);
	void progressValue(int);

public Q_SLOTS:
	void updateProgress();

private:
	//Functions
	QVector<QPoint> segment2Dstage(int initialSize, int cmeansCluster, double cmeansCutoffValue);
	QVector<QVector<QPoint> > segment3Dstage(const QVector<QPoint> & pointList2D, int cmeansCluster, double cmeansCutoffValue);
	QVector<QPoint> fcmRect2D(QRect boundRect,int z, double threshold = 0.3, int nClusters = 2);
	QVector<QVector<QPoint> > fcmRect3D(QRect boundRect,int startZ,int endZ, double threshold = 0.3, int nClusters = 2, QVector<QVector<double> > * clusterCenters = nullptr);
	QVector<QVector<QPoint> > fcmPoly1slice(const QVector<QPoint>&  pointList, int startZ, int endZ, double threshold = 0.3, int nClusters = 2);
	QVector<QVector<QPoint> > fcmPoly3D(QVector<QVector<QPoint> > pointList, double threshold = 0.3, int nClusters = 2);
	QVector<QPair<double, double> > normalizationFactors;
	int cmeansProgress{};
	QVector<uint> mrCMeansSegmentationParameters; //!< This holds the data containing the c-means parameters used during single-click segmentation.

protected:
	//Variables
	std::shared_ptr<MRLesionModel> model_;
};

#endif // MRANALYSIS_H
