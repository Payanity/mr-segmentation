#include "lesiondatabase.h"
// #include "quantxdata.h"
#include "databasehelper.h"
#include "mranalysis.h"
//#include "Wt/WDateTime.h"
#include <QUuid>
#include <utility>
#include <Wt/Dbo/backend/Postgres.h>
#include <Wt/Dbo/backend/Sqlite3.h>
#include <QRegion>

#ifdef Q_OS_WIN
#include <Windows.h>
#include <lmcons.h>
#endif

class QuantXLesionDatabasePrivate {
public:
	//QuantXLesionDatabasePrivate();
	Wt::Dbo::Session session;
	std::unique_ptr<Wt::Dbo::SqlConnection> connection;
	QuantXLesionDatabase::ConnectionType connectionType;
	QStringList dbSettings;
	void initializeSession() {
		//connection->setProperty("show-queries", "true");
		session.setConnection(connection->clone());
		session.mapClass<quantx::mranalysis::Lesion>("qi_lesions"); //TODO Figure out the best way to centralize these so that this, AND the sql service can both see them and we don't have to hardcode magic strings here.
		session.mapClass<quantx::mranalysis::LesionDetails>("qi_lesiondetails");
		session.mapClass<quantx::dicom::DicomSeries>("qi_series");
		session.mapClass<quantx::dicom::DicomStudy>("qi_studies");
		session.mapClass<quantx::overlay::DbOverlay>( "qi_overlay" );
		session.mapClass<quantx::patient::Patient>("qi_patients");
		session.mapClass<quantx::dicom::SeriesThumbnails>("qi_series_thumbnails");
		session.mapClass<quantx::dicom::MotionCorrection>("qi_motion_correction");
		session.mapClass<quantx::dicom::Images2D>("qi_2d_images");
		try {
			session.createTables();
			std::cerr << "Created database." << std::endl;
		} catch (std::exception& e) {
			std::cerr << e.what() << std::endl;
			std::cerr << "Using existing database";
		}
	}
};

//!
//! \brief Create a QuantXLesionDatabase object and initialize it with the specified database connection parameters.
//!
//! \ctorPrePost
//!
QuantXLesionDatabase::QuantXLesionDatabase()
    : d(std::make_unique<QuantXLesionDatabasePrivate>())
{
	d->connection = std::make_unique<Wt::Dbo::backend::Postgres>("host=localhost port=5432 dbname=quantinsights user=postgres password=postgres");
	d->connectionType = ConnectionType::Postgres;
	d->initializeSession();
}

//!
//! \overload
//! \param conntype The connection type
//! \param parameters String of parameters
//!
QuantXLesionDatabase::QuantXLesionDatabase(const ConnectionType & conntype, const std::string & parameters)
    : d(std::make_unique<QuantXLesionDatabasePrivate>())
{
	d->connectionType = conntype;
	switch (conntype) {
		case ConnectionType::Postgres : d->connection = std::make_unique<Wt::Dbo::backend::Postgres>(parameters); break;
		case ConnectionType::SQLite   : d->connection = std::make_unique<Wt::Dbo::backend::Sqlite3> (parameters); break;
	}
	d->initializeSession();
}

//!
//! \overload
//! \param databaseSettings
//!
QuantXLesionDatabase::QuantXLesionDatabase(const QStringList & databaseSettings)
    : d(std::make_unique<QuantXLesionDatabasePrivate>())
{
	if(databaseSettings.first() == "QPSQL"){
		auto host = databaseSettings.at(1).toStdString();
		std::string port = "5432";
		if (databaseSettings.at(1).contains(":")){
			host = databaseSettings.at(1).split(":").first().toStdString();
			port = databaseSettings.at(1).split(":").at(1).toStdString();
		}
		auto params = "host="     + host +
		             " port="     + port +
		             " dbname="   + databaseSettings.at(2).toStdString() +
		             " user="     + databaseSettings.at(3).toStdString() +
		             " password=" + databaseSettings.at(4).toStdString() ;
		d->connection = std::make_unique<Wt::Dbo::backend::Postgres>(params);
		d->connectionType = ConnectionType::Postgres;
	}
	else if(databaseSettings.first() == "QSQLITE"){
		d->connection = std::make_unique<Wt::Dbo::backend::Sqlite3>(databaseSettings.at(2).toStdString());
		d->connectionType = ConnectionType::SQLite;
	}
	else {
		d->connection = std::make_unique<Wt::Dbo::backend::Postgres>("host=localhost port=5432 dbname=quantinsights user=postgres password=postgres");
		d->connectionType = ConnectionType::Postgres;
	}
	d->initializeSession();
	d->dbSettings = databaseSettings;
}

//!
//! \overload
//! \param other
//!
QuantXLesionDatabase::QuantXLesionDatabase(const QuantXLesionDatabase & other)
    : d(std::make_unique<QuantXLesionDatabasePrivate>())
{
	d->connection = other.connection();
	d->initializeSession();
}

//!
//! \brief Destructor for QuantXLesionDatabase().
//!
QuantXLesionDatabase::~QuantXLesionDatabase() = default;

//!
//! \brief Set the specified database connection property to the specified value.
//! \param name
//! \param value
//!
//! \preInitPost{The value of the specified database connection property is updated.}
//!
void QuantXLesionDatabase::setConnectionProperty(const std::string &name, const std::string &value)
{
	d->connection->setProperty(name, value);
}

//!
//! \brief Return the database connection.
//! \return A pointer to the database connection.
//!
//! \preInitPost{The database connection is returned.}
//!
std::unique_ptr<Wt::Dbo::SqlConnection> QuantXLesionDatabase::connection() const
{
	return d->connection->clone();
}

//!
//! \brief Return the list database settings.
//! \return A QStringList of database settings.
//!
//! \preInitPost{The list of database settings is returned.}
//!
QStringList QuantXLesionDatabase::dbSettings() const
{
	return d->dbSettings;
}

//!
//! \brief Add a lesion and lesiondetails to the database.
//! \param mrimage
//! \param mra
//! \param knownCases
//! \param session_id
//! \return Returns a string of lesion details...*
//!
//! \preInitPost{The lesion and lesiondetails added to the database the UUID of the lesion is returned.}
//!
QString QuantXLesionDatabase::addLesionToDatabase(QiMriImage * mrimage, const std::shared_ptr<MRLesionModel> & mra, QVector<const KnownCase *> knownCases, std::string session_id)
{
	auto mraUUID = QUuid::createUuid().toString(); // Leave brackets to make it more 'official'

	//auto mra = quantxData->mranalyses.last();
	auto features = mra->getFeatures();
	auto seed = mra->getSeedPoint();
	auto seed_realworld = mrimage->toRealworld(seed);

	auto timepoints = DatabaseHelper::encodeBinary(mrimage->getTimestamps());
	auto timeBuf = reinterpret_cast<unsigned char*>(timepoints.data());

	auto maxUptake = DatabaseHelper::encodeBinary(mra->getMostUptaking());
	auto maxBuf = reinterpret_cast<unsigned char*>(maxUptake.data());

	auto avgUptake = DatabaseHelper::encodeBinary(mra->getAvgUptake());
	auto avgBuf = reinterpret_cast<unsigned char*>(avgUptake.data());

	if(session_id.empty()){
#ifdef Q_OS_WIN
		TCHAR username[UNLEN + 1];
		DWORD username_size = UNLEN + 1;
		GetUserName(static_cast<TCHAR *>(username), &username_size);
#ifndef UNICODE
		session_id = username;
#else
		std::wstring username_wStr = username;
		session_id = std::string(username_wStr.begin(), username_wStr.end());
#endif
		session_id += QDateTime::currentDateTime().toString("yyyyMMddHHmmss").toStdString();
#endif
	}

	quantx::mranalysis::Lesion lesion;
	lesion.UUID = mraUUID.toStdString();
	lesion.sessionId = session_id;
	lesion.mrn = mrimage->mrn().toStdString();
	lesion.studyUID = mrimage->studyUID().toStdString();
	lesion.accession_number = mrimage->accessionNumber().toStdString();
	lesion.seriesUID = mrimage->seriesUID().toStdString();
	lesion.qi_score = features.first();
	lesion.volume = features.at(8) / 1000;
	lesion.surface_area = features.at(9);
	lesion.seed_x = seed.x();
	lesion.seed_y = seed.y();
	lesion.seed_z = seed.z();
	lesion.seed_realworld_x = seed_realworld.x();
	lesion.seed_realworld_y = seed_realworld.y();
	lesion.seed_realworld_z = seed_realworld.z();
	lesion.timepoints = std::vector<unsigned char>(timeBuf, timeBuf + timepoints.size());
	lesion.maxUptake = std::vector<unsigned char>(maxBuf, maxBuf + maxUptake.size());
	lesion.avgUptake = std::vector<unsigned char>(avgBuf, avgBuf + avgUptake.size());

	auto featureArray = DatabaseHelper::cryptEncodeBinary(features, DatabaseHelper::FeatureKey);
	auto* featureBuf = reinterpret_cast<unsigned char*>(featureArray.data());

	auto segmentation_outline = DatabaseHelper::cryptEncodeBinary(MrAnalysis(mra).getRegions(), DatabaseHelper::SegOutlineKey);
	auto* segOutBuf = reinterpret_cast<unsigned char*>(segmentation_outline.data());

	auto segmentation_filled = DatabaseHelper::cryptEncodeBinary(MrAnalysis(mra).getFilledRegion(mrimage->acquisitionOrientation()), DatabaseHelper::SegFilledKey);
	auto* segFilledBuf = reinterpret_cast<unsigned char*>(segmentation_filled.data());

	auto rawdata = DatabaseHelper::cryptEncodeBinary(mra->getRegionData(), DatabaseHelper::RawDataKey);
	auto* rawDataBuf = reinterpret_cast<unsigned char*>(rawdata.data());

	int feature_index = 0;  // feature index 0 is the QI Score
	//auto similarcases = DatabaseHelper::cryptEncodeKnownCase(quantxData->mrfindNNearestCases(features.first(), feature_index, 45), feature_index, DatabaseHelper::SimCaseKey);
	auto similarcases = DatabaseHelper::cryptEncodeKnownCase(knownCases, feature_index, DatabaseHelper::SimCaseKey);
	auto* simCaseBuf = reinterpret_cast<unsigned char*>(similarcases.data());

	quantx::mranalysis::LesionDetails lesionDetails;
	lesionDetails.UUID = lesion.UUID;
	lesionDetails.features = std::vector<unsigned char>(featureBuf, featureBuf + featureArray.size());
	lesionDetails.segmentation_outline = std::vector<unsigned char>(segOutBuf, segOutBuf + segmentation_outline.size());
	lesionDetails.segmentation_filled = std::vector<unsigned char>(segFilledBuf, segFilledBuf + segmentation_filled.size());
	lesionDetails.rawdata = std::vector<unsigned char>(rawDataBuf, rawDataBuf + rawdata.size());
	lesionDetails.similarcases = std::vector<unsigned char>(simCaseBuf, simCaseBuf + similarcases.size());


	Wt::Dbo::Transaction transaction(d->session);
	//session.add(std::make_unique<Lesion>(lesion));
	d->session.add(std::make_unique<quantx::mranalysis::Lesion>(lesion));
	d->session.add(std::make_unique<quantx::mranalysis::LesionDetails>(lesionDetails));
	transaction.commit();

	return mraUUID;
}


Wt::Dbo::Session & QuantXLesionDatabase::session()
//!
//! \brief Return the database session.
//! \return A pointer to the session.
//!
//! \preInitPost{The database session is returned.}
//!
{
	return d->session;
}

Wt::Dbo::ptr<quantx::mranalysis::Lesion> QuantXLesionDatabase::findLesionInDatabase(const std::string& uuid)
//!
//! \brief Find a lesion in the database by specifying the lesion’s uuid.
//! \param uuid
//! \return A pointer to the session calling a function that finds the lesion.
//!
//! \prePost{The QuantXLesionDatabase object has been initialized and session created.,A pointer to a Lesion database object is returned.}
//!
{
	return d->session.find<quantx::mranalysis::Lesion>().where("\"uuid\" = ?").bind(uuid);
}

Wt::Dbo::ptr<quantx::mranalysis::LesionDetails> QuantXLesionDatabase::findLesionDetailsInDatabase(const std::string& uuid)
//!
//! \brief Find a lesion details in the database by specifying the lesion’s uuid.
//! \param uuid
//! \return A pointer to the session calling a function that finds the lesion details.
//!
//! \prePost{The QuantXLesionDatabase object has been initialized and session created.,A pointer to a LesionDetails database object is returned.}
//!
{
	return d->session.find<quantx::mranalysis::LesionDetails>().where("\"uuid\" = ?").bind(uuid);
}

Wt::Dbo::collection<Wt::Dbo::ptr<quantx::mranalysis::Lesion> > QuantXLesionDatabase::findLesionInDatabaseBySeries(const std::string& seriesuid)
//!
//! \brief Find a lesion in the database by specifying the image series uid.
//! \param seriesuid
//! \return  A pointer to the session calling a function that finds the lesion details.
//!
//! \prePost{The QuantXLesionDatabase object has been initialized and session created.,Return a collection of pointers to Lesion database objects.}
//!
{
	return d->session.find<quantx::mranalysis::Lesion>().where("\"seriesuid\" = ?").bind(seriesuid);
}

//!
//! \brief Remove a lesion from the database by uuid.
//! \param uuid
//!
//! \prePost{The QuantXLesionDatabase object has been initialized and session created., Lesion is removed from the database.}
//!
void QuantXLesionDatabase::removeLesionFromDatabase(const std::string & uuid)
{
	Wt::Dbo::Transaction transaction(d->session);
	d->session.execute("DELETE FROM \"qi_lesions\" WHERE \"uuid\" = ?").bind(uuid);
	transaction.commit();
}

