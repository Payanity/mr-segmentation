#ifndef GLCMFEATS_H
#define GLCMFEATS_H

#include <QVector>

//Use glcmCalc to calculate a glcm, then glcmFeats to calulate features from said glcm

//!
//! \addtogroup MRAnalysisModule
//! @{
QVector<QVector<int> > glcmCalc(const QVector<int> & xData, const QVector<int> & yData, const QVector<int> & zData, const QVector<QVector<QVector<double> > > & data, bool localglcm=true, bool twoDmethod=false, int numLevels=32, int displacement = 1);
QVector<double> glcmFeats(const QVector<QVector<int> > &);
//! @}

#endif // GLCMFEATS_H
