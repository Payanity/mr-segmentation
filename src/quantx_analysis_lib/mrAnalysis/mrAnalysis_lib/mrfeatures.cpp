#include "mrfeatures.h"
#include "mranalysis.h"
#include "cmeans.h"
//#include "bnn.h"
#include <QVector>
#include "binarycube.h"
#include <QFile>
#include "changeprevalence.h"
#include <QApplication>
//#include <QTimer>
#include <QDataStream>
#include <functional>
#include <QtMath>

#include <cmath>
#include <utility>

#define useDebug 0
#if useDebug
#define GLCM_DEBUG_TABLE 0
#if GLCM_DEBUG_TABLE
#include "debugtablemaker.h"
#endif
#include <QDebug>
#endif

double testsvm(const QVector<double> & inputData, const QVector<QPair<double, double> > & localNormalizationFactors = (QVector<QPair<double, double> >()), QVector<int> featsToUse = QVector<int>() );

#define CMEANS_MAX_ITER 200

// linear = 0, cubic = 1
#define INTERPMETHOD 0

/*!
 * \brief Calculates features according to cmeansBins.
 * \details For the cmeansBin value, this should be a list of integers of any length where the numbers are incrementally larger, e.g. {2, 5, 10}. If there is a value of 0 in the list, that stage is skipped. This manages a tradeoff between speed (lower values) and accuracy
 * (higher values) by taking the output from each stage and forwarding it to the next stage.
 * \param cmeansBins Number of bins to use in each dimension of c-means reduction.
 * \return
 */
bool MrAnalysis::calculateFeatures(QVector<uint> cmeansBins)
{

	if(!model_->analysisDone_){
#if useDebug
		qDebug() << "Calculating Features";
#endif
		const auto image = model_->image;
		auto & regionData = model_->regionData;
		auto & avgUptake = model_->avgUptake;
		auto & mostUptaking = model_->mostUptaking;
		auto & features = model_->features;
		const auto & precontrastIdx_ = model_->precontrastIdx_;

		QVector<QVector<QPoint> > regionFilled = getFilledRegion(QUANTX::XYPLANE);
		QVector<int> size;
		int nPix = 0;
		for(int i=0; i<regionFilled.size();i++){
			size << regionFilled.at(i).size();
			nPix += size.at(i);
		}
		int max_iter = CMEANS_MAX_ITER;

		emit startedMRAnalysis();

		//collect region data
		auto timePts = image->nt();
		QVector<QTime> timestamps = image->getTimestamps();
		regionData.resize(0);
		QVector<double> singleRow(timePts);
		//QVector<double> avgUptake(timePts);
		avgUptake.resize(timePts);
		mostUptaking.resize(timePts);
		int firstPostIdx = image->firstPostIdx();
		for(int z = 0; z < size.size(); z++){
			for(const auto & point : std::as_const(regionFilled.at(z)) ){
				singleRow[0] = image->data().at(precontrastIdx_).at(z).at(point.y()).at(point.x());
				avgUptake[0] += singleRow.at(0);
				for(int j = 1; j < timePts; j++){
					singleRow[j] = image->data().at(j + firstPostIdx - 1 ).at(z).at(point.y()).at(point.x());
					avgUptake[j] += singleRow.at(j);
				}
				regionData << singleRow;
			}
		}

		emit progressValue(1);

		auto nCluster = std::lround(static_cast<double>(nPix) / 80);
		if(nCluster < 2)
			nCluster = 2;
		if(nCluster > 25)
			nCluster = 25;
		QVector<double> weights(timePts, 1.0);
		cmeansProgress = 0;
		auto cMeansResults = cMeans(regionData, nCluster, weights, 2.0, max_iter, 1.0e-06, false, 5, &cmeansProgress, std::move(cmeansBins), this);
		emit progressValue(50);
		qApp->processEvents();
#if useDebug
		qDebug() << "Have C-Means Results";
#endif
		//find the largest cMeansResults uptake at second post-contrast and use that cluster
		int mostUpC = 0;

		QVector<int> elapsedTime{0};
		//auto midTimePoint = image->firstPostIdx();
		auto midTimePoint = 1;
		for(auto i = 1; i < timePts; i++){
			//elapsedTime << image->injectionTime().secsTo(timestamps[i]);
			elapsedTime << 60 + timestamps[1].secsTo(timestamps[i]);
			if( abs(elapsedTime[i] - 135) < abs(elapsedTime[midTimePoint] - 135) )
				midTimePoint = i;
		}

		for(int i = 1; i < nCluster; i++)
			if((cMeansResults[i][midTimePoint] - cMeansResults[i][0]) > (cMeansResults[mostUpC][midTimePoint] - cMeansResults[mostUpC][0]))
				mostUpC = i;
#if useDebug
		qDebug() << "Elapsed times: " << elapsedTime;
		qDebug() << "midTimePoint:" << midTimePoint << "mostUpC:" << mostUpC << "timestamps:" << timestamps;
#endif
		QVector<double> dynFeats = dynamicFeature(cMeansResults[mostUpC], true, elapsedTime, midTimePoint);
		emit progressValue(71);
		qApp->processEvents();
#if useDebug
		qDebug() << "dynFeats:" << dynFeats;
#endif
		QVector<double> shapeFeats;

		shapeFeats = shapeFeature3D(regionFilled, static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()));
		emit progressValue(75);
		qApp->processEvents();
#if useDebug
		qDebug() << "shapeFeats:" << shapeFeats;
#endif
		QVector<QVector<double> > isotropFeats;
		QVector<QVector<double> > additionalFeats;
		//set up for different calculation styles
		bool runMorphology          = true;
		bool useLocalGlcm           = true;
		bool subtractionImgMorph    = true;
		bool use2DGlcm              = false;
		int  numGlcmGrayLevels      = 64;
		int  displacement           = 1;
		//end variable declaration
		//set up different methods based on training info
		/*
		if(windowMode == 2){
			useLocalGlcm        = false;
			subtractionImgMorph = false;
			use2DGlcm           = true;
			numGlcmGrayLevels   = 64;
			zScaleFactor        = 2.0;
		}
		*/
#if useDebug
		QTime time;
		time.start();
#endif
		midTimePoint = midTimePoint + firstPostIdx - 1; // texture calculation uses full image data with all timepoints
		// First, we use the mid Timepoint, and test three displacements
		displacement = 1;
		isotropFeats = runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, midTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
#if useDebug
		qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
		emit progressValue(77);
		qApp->processEvents();
		runMorphology = false;
		displacement = 2;
		additionalFeats << runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, midTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
#if useDebug
		qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
		emit progressValue(79);
		qApp->processEvents();
		displacement = 3;
		additionalFeats << runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, midTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
#if useDebug
		qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
		emit progressValue(82);
		qApp->processEvents();

		if(additionalFeats[0].isEmpty() || additionalFeats[1].isEmpty()) // Features could not be calculated for all displacements
			return false;

		// Now, use the max Timepoint (whatever it may be) and test three displacements
		int glcmLastPost = (elapsedTime.size() - 1);
		while( elapsedTime[glcmLastPost] > 300 ) --glcmLastPost;
		auto maxTimePoint = static_cast<int>(std::distance(cMeansResults[mostUpC].begin(), std::max_element(cMeansResults[mostUpC].begin(), cMeansResults[mostUpC].begin() + glcmLastPost)));
		if(maxTimePoint != midTimePoint){
			displacement = 1;
			additionalFeats << runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, maxTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
#if useDebug
			qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
			emit progressValue(84);
			qApp->processEvents();
			displacement = 2;
			additionalFeats << runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, maxTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
#if useDebug
			qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
			emit progressValue(86);
			qApp->processEvents();
			displacement = 3;
			additionalFeats << runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, maxTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
#if useDebug
			qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
			emit progressValue(89);
			qApp->processEvents();
		}
		else{
			additionalFeats << isotropFeats.first();
			additionalFeats << additionalFeats[0];
			additionalFeats << additionalFeats[1];
		}

		int firstPostTimePoint = image->firstPostIdx();
		if(firstPostTimePoint != midTimePoint){
			displacement = 1;
			additionalFeats << runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, firstPostTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
#if useDebug
			qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
			emit progressValue(91);
			qApp->processEvents();
			displacement = 2;
			additionalFeats << runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, firstPostTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
#if useDebug
			qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
			emit progressValue(93);
			qApp->processEvents();
			displacement = 3;
			additionalFeats << runGlcm3D(&regionFilled, image->data(), static_cast<double>(image->dx()), static_cast<double>(image->dy()), static_cast<double>(image->dz()), precontrastIdx_, firstPostTimePoint, runMorphology, useLocalGlcm, subtractionImgMorph, use2DGlcm, numGlcmGrayLevels, displacement);
		}
		else{
			additionalFeats << isotropFeats.first();
			additionalFeats << additionalFeats[0];
			additionalFeats << additionalFeats[1];
		}
#if useDebug
		qDebug() << QString("GLCM3D  %1 seconds to finish").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
		emit progressValue(95);
		qApp->processEvents();

		QVector<double> texFeats = isotropFeats.at(0);
		QVector<double> morphFeats;
		if(isotropFeats.size() > 1)
			morphFeats = isotropFeats.at(1);
		midTimePoint = midTimePoint - (firstPostIdx - 1); //regionData used by varDyanFeature() doesn't have extra precontrast time points
		QVector<double> varFeats = varDynFeature(regionData, NORMALIZATION_PRECONTRAST_DIVISION, elapsedTime, midTimePoint);

		emit progressValue(97);
		qApp->processEvents();

		// RUN T2 FEATURES HERE
		/*QVector<QVector<double> > t2Feats;
		QVector<double> t2texFeats;
		QVector<double> t2morphFeats;
		if(windowMode == 2){
			t2Feats = runGlcmT2(&region,imagedatat2,fslimaget2->dx,(zScaleFactor * fslimaget2->dz),mriHeight,runMorphology,useLocalGlcm,use2DGlcm,numGlcmGrayLevels);
			t2texFeats   = t2Feats.at(0);
			if(t2Feats.size() > 1)
				t2morphFeats = t2Feats.at(1);
		}*/

		features.clear();
		//        7 dynfeats, 5 shapefeats,14 texfeats, 3 morphfeats, 4 varfeats
		features << dynFeats << shapeFeats << texFeats << morphFeats << varFeats;
		for(const auto & f : std::as_const(additionalFeats))
			features << f;
		// APPEND T2 FEATURES HERE
		//    features << t2texFeats << t2morphFeats;

		emit progressValue(98);
		qApp->processEvents();

#if useDebug
		//Feature selection results using: IPS4 bootstrap, MeanStDevNormalization, JMI algorithm
		QVector<int> fullFeatureList;
		fullFeatureList <<  5 <<  1 <<  2 <<  3 <<  6 <<  //Features 1-5
		                    7 << 11 << 12 << 13 << 10 <<  //Features 6-10
		                   14 << 15 << 16 << 17 << 18 <<  //Features 11-15
		                   19 << 20 << 21 << 22 << 23 <<  //Features 16-20
		                   24 << 25 << 26 <<  9 << 27 <<  //Features 21-25
		                   28 <<  8 << 29 << 31 << 33 <<  //Features 26-30
		                   34 <<  4 << 32 << 30 ;         //Features 31-34

		double firstRunVal = testsvm(features);
		int count = 0;
		int numRuns = 1000;
		for(int i = 0; i < numRuns; ++i)
			if(firstRunVal == testsvm(features, QVector<QPair<double, double> >() , fullFeatureList.mid(0,11)))
				count++;
		qDebug() << "SVM output:" << firstRunVal << "was reproduced" << count << "/" << numRuns << "times using 11 features";
		count = 0;
		for(int i = 0; i < numRuns; ++i)
			if(firstRunVal == testsvm(features, QVector<QPair<double, double> >(), fullFeatureList.mid(0,12))) //This is wrong, but for some reason we were using it in the past
				count++;
		qDebug() << "SVM output:" << firstRunVal << "was reproduced" << count << "/" << numRuns << "times using 12 features";
#endif

		/* This normalization doesn't work very well
		QString normalizationFileName = ":/svm/saved_function";
		if(image->manufacturer().contains("Philips") && qFuzzyCompare( image->magneticFieldStrength(), static_cast<float>(3.0) ))
			normalizationFileName = ":/normalization/philips3T";
		if(image->manufacturer().contains("GE MEDICAL SYSTEMS") && ( qMax( image->dx(), qMax( image->dy(), image->dz() ) ) > 2.5 ) )
			normalizationFileName = ":/normalization/ge1.5T";
		if(image->manufacturer().contains("GE MEDICAL SYSTEMS") && ( qMax( image->dx(), qMax( image->dy(), image->dz() ) ) > 2.5 ) ){
			// Fix Max Uptake
			features[0]  = (features[0]  * 0.70) ;
			// Fix Uptake Rate
			features[2]  = (features[2]  * 0.70) ;
			// Fix E2
			features[5]  = (features[5]  * 0.70) ;
			// Fix Sphericity
			features[9]  = (features[9]  / 1.10) ;
			// Fix Irregularity
			features[10] = (features[10] * 0.70) + 0.22 ; // pow( 2.0 , (1.0 / 3.0) );
			// Fix Eff Radius
			//features[11] = (features[11] / 1.50) ;
			// Fix Contrast
			//features[12] = (features[12] * 1.50) ;
			// Fix Correlation
			features[13] = (features[10] * 0.80) + 0.02 ; // pow( features[13] , 3 );
		}
		QFile normalizationFile(normalizationFileName);
		if(normalizationFile.open(QIODevice::ReadOnly)){
			QDataStream ds(&normalizationFile);
			ds >> normalizationFactors;
			normalizationFile.close();
		}
		*/

#if useDebug
		double tempSVM = testsvm(features, normalizationFactors);
		qDebug() << "Feature Vector:" << features;
		qDebug() << "*** New prevalence/scaling functions ***";
		qDebug() << "SVM value: " << tempSVM;
#endif
		features.prepend( convertSVMToQiScore(testsvm(features, normalizationFactors)) );

#if useDebug
		//qDebug() << "Original QI value: " << features.first();
		qDebug() << "Calculated QI score" << convertSVMToQiScore( tempSVM );
		qDebug() << "SVM from QI: " << convertQiScoretoSVM( convertSVMToQiScore( tempSVM ) );
#endif


		//if(image->manufacturer().contains("GE MEDICAL SYSTEMS") && ( qMax( image->dx(), qMax( image->dy(), image->dz() ) ) > 2.5 ) )
		//    features[0] = 100 * changePrevalence(features[0] / 100, 0.5, 0.35);
		model_->analysisDone_ = true;

		/////////////////////////////////////////////////////////////////////////////////////////
		// At this point, the analysis is done. This section is prepping data for uptake plots
		for(int i = 0; i < timePts; ++i)
			avgUptake[i] = avgUptake[i] / regionData.size();
		double maxCmeans = (*std::max_element(cMeansResults[mostUpC].begin(), cMeansResults[mostUpC].end()) - cMeansResults[mostUpC][0]) / cMeansResults[mostUpC][0];
		double maxAvg    = (*std::max_element(avgUptake.begin(), avgUptake.end()) - avgUptake[0]) / avgUptake[0];

		mostUptaking = cMeansResults[mostUpC];
		model_->plotMaxValue = 50 * (static_cast<int>(2 * qMax(maxCmeans, maxAvg)) + 1);
		/////////////////////////////////////////////////////////////////////////////////////////

		emit progressValue(99);
		qApp->processEvents();
		emit finishedMRAnalysis(avgUptake, mostUptaking, model_->plotMaxValue);
	}
	return true;
}

/*!
 * \brief Update C-Means progress.
 */
void MrAnalysis::updateProgress()
{	//Minimum Progress Bar %
	int min = 1;
	// Maximum Progress Bar %
	int max = 70;
	int length = max - min;
	int totalLoops = CMEANS_MAX_ITER * (1 + 2 + 4); // Assume the maximum number of 3 bins, weighting 1, 2, 4
	int value = min + qRound(qreal(qMin(cmeansProgress, CMEANS_MAX_ITER) * length) / totalLoops);                             // 1x multiplier for first stage
	value += qRound(qreal(2 * qMax(0,qMin(cmeansProgress - CMEANS_MAX_ITER, CMEANS_MAX_ITER) ) * length) / totalLoops);       // 2x multiplier for second stage
	value += qRound(qreal(4 * qMax(0,qMin(cmeansProgress - (2 * CMEANS_MAX_ITER), CMEANS_MAX_ITER) ) * length) / totalLoops); // 4x multiplier for second stage

#if useDebug
	//qDebug() << "cmeansProgress:" << cmeansProgress << "value:" << value;
#endif

	emit progressValue(value);
	qApp->processEvents();
}

/*!
 * \brief Converts svm output to QIScore.
 * \param svm Svm number.
 * \return QIScore.
 */
double convertSVMToQiScore(double svm)
{

	//double scalingLinearMin = 0.14;
	//double scalingLinearMax = 0.77;
	//double prevalence = 0.697;

	//double prevalenceModified = changePrevalence( svm, prevalence);
#if useDebug
	//qDebug() << "SVM modified by prevalence:" << prevalenceModified;
#endif
	//return qMax( 1.0 , qMin( 99.0, 100. * ( prevalenceModified  - scalingLinearMin) / ( scalingLinearMax - scalingLinearMin ) ) );
	return qMax( 1.0 , qMin( 99.0, 100. * (0.5 + copysign( pow( 2 * qAbs(svm - 0.5), 0.5) / 2 , svm - 0.5 ) ) ) );
}

/*!
 * \brief Converts QIScore to raw svm output.
 * \param qiScore QIScore number.
 * \return Svm.
 */
double convertQiScoretoSVM(double qiScore)
{

	//double scalingLinearMin = 0.14;
	//double scalingLinearMax = 0.77;
	//double prevalence = 0.697;

	//double unScaled = qiScore  * ( scalingLinearMax - scalingLinearMin ) / 100. + scalingLinearMin;
#if useDebug
	//qDebug() << "Input QI Score: " << qiScore;
	//qDebug() << "unScaled QI Score:" << unScaled;
#endif
	//return changePrevalence(unScaled, 0.5, prevalence);

	return 0.5 + copysign( (pow( 0.02 * qAbs(qiScore - 50), 2) / 2) , qiScore - 50 );

}

/*!
 * \brief Calculates the dynamic features.
 * \param data The pixel values used for the dynamic feature calculations.
 * \param normalize Set this flag to true to run the calculations using a ratio of the pixel value at each timepoint to the initial value (enhancement ratio) instead of raw values.
 * \param elapsedTime The amount of time elapsed from the data at index 0 to the data at each index. This vector should be the same size as data.
 * \param midTimePoint The timepoint used for the 'initial rise' portion of the calculations.
 * \return An output vector containing  Weijie's 5 dynamic features.
 */
QVector<double> dynamicFeature(QVector<double> data, bool normalize, QVector<int> elapsedTime, int midTimePoint)
{
	//if(midTime >= (data.size() - 1) ) //make sure the mid time used for colormap calculation is not the final timepoint
	//    midTime = data.size() - 2;
	/*
	if(data.size() < 6){      //fix to match Weijie's linear interpolation of data
		if(data.size() == 3){
			data.insert(2,((3 * data.at(1)) + data.at(2))/4);
			elapsedTime.insert(2,((3 * elapsedTime.at(1)) + elapsedTime.at(2))/4);
		}
		if(data.size() == 4){
			data.insert(3,((2 * data.at(2)) + data.at(3))/3);
			elapsedTime.insert(3,((2 * elapsedTime.at(2)) + elapsedTime.at(3))/3);
		}
		if(data.size() == 5){
			data.insert(4,(data.at(3) + data.at(4))/2);
			elapsedTime.insert(4,(elapsedTime.at(3) + elapsedTime.at(4))/2);
		}
	}*/

	auto timePts = data.size();
	QVector<double> normalized;
	normalized << data;
	QVector<double> output;

	if(normalize){
		for(int i=0;i<timePts;i++){
			normalized[i] = static_cast<double>(data[i] - data[0]) / qMax(data[0], 1.0); // We don't want any voxel values of 0
		}
	}

	double maxup;
	int maxupLoc;
	std::tie(maxup, maxupLoc) = std::invoke([](auto d){auto p = std::max_element(d.begin(), d.end()); return std::make_pair(*p, std::distance(d.begin(), p));}, normalized);

	//HERE IS THE LIST OF VARIABLES IN OUTPUT VECTOR
	//The 5 features calculated are Weijie's 5 dynamic features

	output << maxup;                                                    // Max Uptake
	output << elapsedTime[maxupLoc];                                       // Peak TimePoint (0...timePts-1)
	if(maxupLoc > 0) // Variance can be maximum at initial timepoint
		output << maxup / elapsedTime[maxupLoc];                       // Uptake Rate (not fixed for different timepoints yet)
	else
		output << 0.0;
	if((timePts - 1 - maxupLoc) > 0)
		output << (maxup - normalized.last()) /
		          (elapsedTime[timePts-1] - elapsedTime[maxupLoc]);        // Washout Rate (this is similar to SER, but based on max timepoint rather than fixed timepoint)
	else
		output << 0.0;

	if(normalize){
		output << static_cast<double>(data.last() - data[midTimePoint]) / (data[midTimePoint] - data[0]);  // Shape Index
		output << normalized[midTimePoint];   // E2
		output << normalized[midTimePoint] / normalized.last(); //SER
	}

	return output;
}

/*void analysisProgressCanceled(QProgressDialog *progress, QVector<QVector<double> > *vector){
		progress->close();
		delete progress;
		delete vector;
		//QMessageBox::information(0, "MRI Window Region Analysis", "Analysis Halted by User");
	}*/

/*!
 * \brief Shapes a given 3D region according to the distances given.
 * \param region The given 3D region.
 * \param dx The distance between pixels in the x dimension of the coordinate system.
 * \param dy The distance between pixels in the y dimension of the coordinate system.
 * \param dz The distance between pixels in the z dimension of the coordinate system.
 * \return A vector of the volume, surfaceArea, sphericity, irregularity, and r2Eff of a shape.
 */
QVector<double> shapeFeature3D(const QVector<QVector<QPoint> > & region, double dx, double dy, double dz)
{

	QVector<int> xData;
	QVector<int> yData;
	QVector<int> zData;
	for( auto i = 0; i < region.size(); ++i ){
		zData.insert(zData.end(), region.at(i).size(), i);
		for( const auto & p : std::as_const(region.at(i)) ){
			xData.push_back(p.x());
			yData.push_back(p.y());
		}
	}

	auto numPoints = xData.size();

	auto boundingBox = makeBoundingBox(xData, yData, zData, nullptr);

	auto minx = *std::min_element(xData.begin(), xData.end());
	auto miny = *std::min_element(yData.begin(), yData.end());
	auto minz = *std::min_element(zData.begin(), zData.end());
	auto meanx = static_cast<double>(std::accumulate(xData.begin(), xData.end(), 0)) / xData.size();
	auto meany = static_cast<double>(std::accumulate(yData.begin(), yData.end(), 0)) / yData.size();
	auto meanz = static_cast<double>(std::accumulate(zData.begin(), zData.end(), 0)) / zData.size();

	for(auto i = 0; i < numPoints; ++i){
		boundingBox[ (xData[i] - minx + 1) ][ (yData[i] - miny + 1) ][ (zData[i] - minz + 1) ] = 1;
	}

	auto volume = numPoints * dx * dy * dz;
	auto r2Eff = pow((3 * volume /(4 * M_PI)), (static_cast<double>(1) / 3));

	quint32 pixInside = 0;

	for(auto i = 0; i < numPoints; ++i){
# if defined(__cpp_lib_hypot) && __cpp_lib_hypot >= 201603L
		if( std::hypot((xData[i] - meanx) * static_cast<double>(dx), (yData[i] - meany) * static_cast<double>(dy), (zData[i] - meanz) * static_cast<double>(dz)) <= r2Eff ) // 3D version of hypot is part of C++17 standard
#else
		if( (pow((xData[i] - meanx) * dx, 2) + pow((yData[i] - meany) * dy, 2) + pow((zData[i] - meanz) * dz, 2)) <= pow(r2Eff, 2) )
#endif
			++pixInside;
	}
	auto sphericity = static_cast<double>(pixInside) / numPoints;
	// We need to isotropisize X & Y as well
	auto surfaceArea = findSurfaceArea(isotropisize3D(&boundingBox, dx, dy, dz)) * pow(static_cast<double>(qMin(dx, qMin(dy, dz))), 2);
	auto irregularity = 1.0 - (4 * M_PI * pow(r2Eff, 2)) / surfaceArea;

	QVector<double> output;
	output << volume << surfaceArea << sphericity << irregularity << r2Eff;
#if useDebug
	qDebug() << "original size:" <<  boundingBox.size() << boundingBox[0].size() << boundingBox[0][0].size();
	QVector<QVector<QVector<double> > > ib = isotropisize3D(&boundingBox, dx, dy, dz);
	qDebug() << "after iso: " << ib.size() << ib[0].size() << ib[0][0].size();

	qDebug() << "dx: " << dx << "dy: " << dy << " dz: " << dz;

	qDebug() << "surface area: "<< surfaceArea;
#endif
	return output;
}

/*!
 * \brief Finds the boxes within a given bounding box.
 * \param boundingBox
 * \return Found boxes.
 */
std::list<std::vector<std::array<bool,3>> > findBoxes(const QVector<QVector<QVector<double> > > &boundingBox)
{
	std::list<std::vector<std::array<bool,3>> > boxes;
	int xSize = boundingBox.size();
	int ySize = boundingBox.first().size();
	int zSize = boundingBox.first().first().size();

	for (int i = 0; i < xSize; i++){
		for(int j = 0; j < ySize; j++){
			for(int k = 0; k < zSize; k++){
				std::vector<std::array<bool,3>> nextBox;

				if (qRound(boundingBox.at(i).at(j).at(k)))
#if ( __cplusplus >= 202001L )
					nextBox.emplace_back(false, false, false); // This should be legal in C++20
#else
					nextBox.push_back({false, false, false});
#endif

				if (Q_LIKELY(i < xSize - 1)){
					if (qRound(boundingBox.at(i+1).at(j).at(k)))
#if ( __cplusplus >= 202001L )
						nextBox.emplace_back(true, false, false); // This should be legal in C++20
#else
						nextBox.push_back({true, false, false});
#endif
					if (Q_LIKELY(k < zSize -1))
						if (qRound(boundingBox.at(i+1).at(j).at(k+1)))
#if ( __cplusplus >= 202001L )
							nextBox.emplace_back(true, false, true); // This should be legal in C++20
#else
							nextBox.push_back({true, false, true});
#endif
					if (Q_LIKELY(j < ySize - 1)){
						if(qRound(boundingBox.at(i+1).at(j+1).at(k)))
#if ( __cplusplus >= 202001L )
							nextBox.emplace_back(true, true, false); // This should be legal in C++20
#else
							nextBox.push_back({true, true, false});
#endif
						if (Q_LIKELY(k < zSize - 1)){
							if(qRound(boundingBox.at(i+1).at(j+1).at(k+1)))
#if ( __cplusplus >= 202001L )
								nextBox.emplace_back(true, true, true); // This should be legal in C++20
#else
								nextBox.push_back({true, true, true});
#endif
						}
					}
				}
				if (Q_LIKELY(j < ySize - 1)){
					if (qRound(boundingBox.at(i).at(j+1).at(k)))
#if ( __cplusplus >= 202001L )
						nextBox.emplace_back(false, true, false); // This should be legal in C++20
#else
						nextBox.push_back({false, true, false});
#endif

					if (Q_LIKELY(k < zSize - 1)){
						if(qRound(boundingBox.at(i).at(j+1).at(k+1)))
#if ( __cplusplus >= 202001L )
							nextBox.emplace_back(false, true, true); // This should be legal in C++20
#else
							nextBox.push_back({false, true, true});
#endif
					}

				}
				if (Q_LIKELY(k < zSize - 1))
					if (qRound(boundingBox.at(i).at(j).at(k+1)))
#if ( __cplusplus >= 202001L )
						nextBox.emplace_back(false, false, true); // This should be legal in C++20
#else
						nextBox.push_back({false, false, true});
#endif
				boxes.push_back(nextBox);

			}
		}
	}
	return boxes;
}

/*!
 * \brief Finds the surface area of an isotropisized image.
 * \param boundingBox The bounding box of an isotropisized image.
 * \return Surface area.
 */
double findSurfaceArea(const QVector<QVector<QVector<double> > > &boundingBox)
{

	//first we create a list of list of qvectors. Each qvector represents an existing pixel
	//and the list it's in represents a 2x2x2 section of the image. The outermost list
	//represents the whole image. Here we use qRound() in order to approximate a binary
	//isotropisized image


	const std::list<std::vector<std::array<bool,3>> > boxes = findBoxes(boundingBox);


	//now we calculate surface area using the Lindblad paper's approximations
	double surfaceArea = 0.0;
	//estimated values from Lindblad
	const std::vector<double> estimatedCaseValues{ 0.638 , 0.6690, 1.276    , 1.276  ,
		                                           0.5517, 1.307 , 1.914    , 0.9270 ,
		                                           0.4182, 1.338 , 1.573132 , 1.1897 ,
		                                           2.552 , 1.573132};
	//create cases described in LibdBlad

	//It's obvious that if the size is 1, that it matches this box
	//listCase1 << QVector<int>();
	//listCase1[0] << 0 << 0 << 0;

	/*
	const  listCase2;

	listCase3 << QVector<int>() << QVector<int>();
	listCase3[0] << 1 << 0 << 0;
	listCase3[1] << 1 << 1 << 1;
	*/

	/*
	listCase4 << QVector<int>() << QVector<int>();
	listCase4[0] << 0 << 0 << 0;
	listCase4[1] << 1 << 1 << 1;
	*/

	/*
	listCase5 << QVector<int>() << QVector<int>() << QVector<int>();
	listCase5[0] << 1 << 0 << 0;
	listCase5[1] << 1 << 1 << 0;
	listCase5[2] << 0 << 1 << 0;

	listCase6 << QVector<int>() << QVector<int>() << QVector<int>();
	listCase6[0] << 1 << 0 << 0;
	listCase6[1] << 1 << 1 << 0;
	listCase6[2] << 0 << 1 << 1;
	*/

	/*
	listCase7 << QVector<int>() << QVector<int>() << QVector<int>();
	listCase7[0] << 1 << 0 << 0;
	listCase7[1] << 0 << 1 << 0;
	listCase7[2] << 1 << 1 << 1;
	*/

	/*
	listCase8 << QVector<int>() << QVector<int>() << QVector<int>() << QVector<int>();
	listCase8[0] << 0 << 0 << 0;
	listCase8[1] << 1 << 0 << 0;
	listCase8[2] << 0 << 1 << 0;
	listCase8[3] << 1 << 1 << 0;

	listCase9 << QVector<int>() << QVector<int>() << QVector<int>() << QVector<int>();
	listCase9[0] << 1 << 0 << 0;
	listCase9[1] << 0 << 1 << 0;
	listCase9[2] << 1 << 1 << 0;
	listCase9[3] << 1 << 1 << 1;

	listCase10  << QVector<int>() << QVector<int>() << QVector<int>() << QVector<int>();
	listCase10[0] << 1 << 0 << 0;
	listCase10[1] << 1 << 1 << 0;
	listCase10[2] << 0 << 0 << 1;
	listCase10[3] << 0 << 1 << 1;

	listCase11  << QVector<int>() << QVector<int>() << QVector<int>() << QVector<int>();
	listCase11[0] << 1 << 0 << 0;
	listCase11[1] << 0 << 1 << 0;
	listCase11[2] << 0 << 1 << 1;
	listCase11[3] << 1 << 1 << 0;

	listCase12  << QVector<int>() << QVector<int>() << QVector<int>() << QVector<int>();
	listCase12[0] << 0 << 0 << 1;
	listCase12[1] << 1 << 0 << 0;
	listCase12[2] << 1 << 1 << 0;
	listCase12[3] << 0 << 1 << 0;

	listCase13  << QVector<int>() << QVector<int>() << QVector<int>() << QVector<int>();
	listCase13[0] << 0 << 0 << 0;
	listCase13[1] << 1 << 1 << 0;
	listCase13[2] << 1 << 0 << 1;
	listCase13[3] << 0 << 1 << 1;
	*/

	/*
	listCase14  << QVector<int>() << QVector<int>() << QVector<int>() << QVector<int>();
	listCase14[0] << 0 << 0 << 0;
	listCase14[1] << 0 << 1 << 0;
	listCase14[2] << 1 << 1 << 0;
	listCase14[3] << 1 << 1 << 1;
	*/

	//binarycube cube1(listCase1);
	const binarycube cube2(std::vector<std::array<bool,3>>{ {true, false, false},
	                                                        {true, true, false} });
	const binarycube cube3(std::vector<std::array<bool,3>>{ {true, false, false},
	                                                        {true, true, true} });
	//binarycube cube4(listCase4);
	const binarycube cube5(std::vector<std::array<bool,3>>{ {true, false, false},
	                                                        {true, true, false},
	                                                        {false, true, false} });
	const binarycube cube6(std::vector<std::array<bool,3>>{ {true, false, false},
	                                                        {true, true, false},
	                                                        {false, true, true} });
	//binarycube cube7(listCase7);
	const binarycube cube8(std::vector<std::array<bool,3>>{ {false, false, false},
	                                                        {true, false, false},
	                                                        {false, true, false},
	                                                        {true, true, false} });
	const binarycube cube9(std::vector<std::array<bool,3>>{ {true, false, false},
	                                                        {false, true, false},
	                                                        {true, true, false},
	                                                        {true, true, true} });
	const binarycube cube10(std::vector<std::array<bool,3>>{ {true, false, false},
	                                                         {true, true, false},
	                                                         {false, false, true},
	                                                         {false, true, true} });
	const binarycube cube11(std::vector<std::array<bool,3>>{ {true, false, false},
	                                                         {false, true, false},
	                                                         {false, true, true},
	                                                         {true, true, false} });
	const binarycube cube12(std::vector<std::array<bool,3>>{ {false, false, true},
	                                                         {true, false, false},
	                                                         {true, true, false},
	                                                         {false, true, false} });
	const binarycube cube13(std::vector<std::array<bool,3>>{ {false, false, false},
	                                                         {true, true, false},
	                                                         {true, false, true},
	                                                         {false, true, true} });
	//binarycube cube14(listCase14);

	//we determine how many of each of the 14 cases presented in Lindblad
	//are present
	std::vector<int> cases(14,0);
	for( const auto & box : boxes ){

		if(!box.empty() && box.size() < 8){
			binarycube testCube(box);

			switch(box.size()){
				case 1:
				case 7:
					cases[0]++;
					break;

				case 6:
					testCube.invert();
				Q_FALLTHROUGH();
				case 2:
					if (testCube == cube2)
					cases[1]++;
				else if (testCube == cube3)
					cases[2]++;
				else //if (testCube == cube4)
					cases[3]++;
					break;

				case 5:
					testCube.invert();
				Q_FALLTHROUGH();
				case 3:
					if (testCube == cube5)
					cases[4]++;
				else if(testCube == cube6)
					cases[5]++;
				else //if(testCube == cube7)
					cases[6]++;
					break;

				case 4:
					if (testCube == cube8)
					cases[7]++;
				else if(testCube == cube9)
					cases[8]++;
				else if(testCube == cube10)
					cases[9]++;
				else if(testCube == cube11)
					cases[10]++;
				else if(testCube == cube12)
					cases[11]++;
				else if(testCube == cube13)
					cases[12]++;
				else //if(testCube == cube14)
					cases[13]++;
					break;
			}
		}
	}

	for(size_t i = 0; i < 14; i++){
#if useDebug
		qDebug() << "Number of times we find cube" << i+1 << ":" << cases[i];
#endif
		surfaceArea += cases[i] * estimatedCaseValues[i];
	}

	return surfaceArea;
}

/////////////////////////////////////////////////////////////////////////////////////
/* These functions arent used
int maxDimInCommon(QVector<QVector3D> & box){
	//returns the maximum number of dimensions in common between two QVector3D<int>
	QVector<int> dimInCommon(box.length()-1, 0);

	for(int i=0; i < box.size()-1; i++){
		for(int j = i+1; j < box.length(); j++){
			if(box[i].x() == box[j].x())
				dimInCommon[i]++;
			if(box[i].y() == box[j].y())
				dimInCommon[i]++;
			if(box[i].z() == box[j].z())
				dimInCommon[i]++;
		}
	}

	int max = dimInCommon[0];
	for (int i = 1; i < dimInCommon.size(); i++){
		if (dimInCommon[i] > max)
			max = dimInCommon[i];
	}

	return max;
}

int numPointsWithCommonDim(QVector<QVector3D> & box, int commonDimensions){
	//finds the max number of QVector3D<int> in the list that share exactly commonDimensions
	//in common
	QVector<int> dimShared();
	for(int i =0; i< box.length(); i++){

	}

}
*/
/////////////////////////////////////////////////////////////////////////////////////

/*!
 * \brief Makes a bounding box of the data given.
 * \param xData
 * \param yData
 * \param zData
 * \param data
 * \return Bounding box.
 */
inline QVector<QVector<QVector<quint16> > > makeBoundingBox(const QVector<int> & xData, const QVector<int> & yData, const QVector<int> & zData, const std::vector<std::vector<std::vector<quint16> > > *data)
{
	int minx, maxx, miny, maxy, minz, maxz;
	auto qiMinMax = [](auto d){auto p = std::minmax_element(d.begin(), d.end()); return std::make_pair(*p.first, *p.second);};
	std::tie(minx, maxx) = qiMinMax(xData);
	std::tie(miny, maxy) = qiMinMax(yData);
	std::tie(minz, maxz) = qiMinMax(zData);

	auto sizex = maxx - minx + 3;
	auto sizey = maxy - miny + 3;
	auto sizez = maxz - minz + 3;

	auto boundingBox = QVector<QVector<QVector<quint16> > >(sizex); //pad with zeros on each side
//#pragma omp parallel for
	for(auto i = 0; i < (sizex); i++){
		boundingBox[i] = QVector<QVector<quint16> >(sizey);
//#pragma omp parallel for
		for(auto j = 0; j < (sizey); j++){
			boundingBox[i][j] = QVector<quint16>(sizez);
//#pragma omp parallel for
			for(auto k = 0; k < (sizez); k++){
				if(data == nullptr)
					boundingBox[i][j][k] = 0;
				else
					boundingBox[i][j][k] = data->at(minz - 1 + k).at(miny - 1 + j).at(minx - 1 + i); //data originally [z][y][x] but now [x][y][z], y flipped
			}
		}
	}
	return boundingBox;
}

/*!
 * \brief Run glcmFeats on a 3D polygon.
 * \param region
 * \param data
 * \param dx
 * \param dy
 * \param dz
 * \param firstTime
 * \param midTime
 * \param morphology
 * \param localglcm
 * \param subtractionMorph
 * \param twoDmethod
 * \param numLevels
 * \param displacement
 * \return
 */
QVector<QVector<double> > runGlcm3D(QVector<QVector<QPoint> > *region, const std::vector<std::vector<std::vector<std::vector<quint16> > > > &data, double dx, double dy, double dz, int firstTime, int midTime, bool morphology, bool localglcm, bool subtractionMorph, bool twoDmethod, int numLevels, int displacement){
	Q_UNUSED(subtractionMorph)

#if useDebug
	QTime time;
	time.start();
	qDebug() << QString("GLCM3D (%3): Start runGlcm3D at time %1 and displacement %2").arg(midTime).arg(displacement).arg(time.elapsed() / 1000.0, 7, 'f', 3);
#endif

	QVector<int> xData;
	QVector<int> yData;
	QVector<int> zData;
	for(int i=0; i<region->size(); i++)
		for(int j=0;j<region->at(i).size();j++){
			xData << region->at(i).at(j).x();
			yData << region->at(i).at(j).y();
			zData << i;
		}

#if useDebug
	qDebug() << QString("GLCM3D (%1): Create bounding boxes").arg(time.elapsed() / 1000.0, 7, 'f', 3);
#endif
	QVector<QVector<QVector<quint16> > > boundingBox = makeBoundingBox(xData,yData,zData,&(data.at(midTime)));
	QVector<QVector<QVector<quint16> > > boundingBox0 = makeBoundingBox(xData,yData,zData,&(data.at(firstTime)));
#if useDebug
	qDebug() << QString("GLCM3D (%1): Create scaled boxes").arg(time.elapsed() / 1000.0, 7, 'f', 3);
#endif
	QVector<QVector<QVector<double> > > scaledBox =  isotropisize3D(&boundingBox, dx, dy, dz, INTERPMETHOD);
	QVector<QVector<QVector<double> > > scaledBox0 =  isotropisize3D(&boundingBox0, dx, dy, dz, INTERPMETHOD);

#if useDebug
	qDebug() << QString("GLCM3D (%1): Create xyzData").arg(time.elapsed() / 1000.0, 7, 'f', 3);
#endif
	QVector<QVector<int> > xyzData;
	if (qFuzzyCompare(dx, dy) && qFuzzyCompare(dy, dz)){
		int minx = (*std::min_element(xData.begin(), xData.end())) - 1; //bounding box has 1 voxel padding
		int miny = (*std::min_element(yData.begin(), yData.end())) - 1;
		int minz = (*std::min_element(zData.begin(), zData.end())) - 1;
#pragma omp parallel for
		for (int i = 0; i < xData.size(); i++){
#pragma omp atomic
			xData[i] -= minx;
#pragma omp atomic
			yData[i] -= miny;
#pragma omp atomic
			zData[i] -= minz;
		}
		xyzData << xData;
		xyzData << yData;
		xyzData << zData;
	} else {
		xyzData = isotrpisizeData(xData, yData, zData, dx, dy, dz);
	}

#if useDebug
	qDebug() << QString("GLCM3D (%1): Create normalizedBox").arg(time.elapsed() / 1000.0, 7, 'f', 3);
#endif

	constexpr NORMALIZATION_TYPE morphologyNormType = NORMALIZATION_PRECONTRAST_DIVISION;
	QVector<QVector<QVector<double> > > normalizedBox(scaledBox0.size());
#pragma omp parallel for
	for(int i = 0; i < scaledBox0.size(); ++i){
		normalizedBox[i] = QVector<QVector<double> >(scaledBox0.last().size());
		for(int j = 0; j < scaledBox0.last().size(); ++j){
			normalizedBox[i][j] = QVector<double>(scaledBox0.last().last().size());
			for(int k = 0; k < scaledBox0.last().last().size(); ++k){
				if(morphologyNormType | NORMALIZATION_PRECONTRAST_SUBTRACTION)
					normalizedBox[i][j][k] = scaledBox[i][j].at(k) - scaledBox0[i][j].at(k);
				if(morphologyNormType | NORMALIZATION_PRECONTRAST_DIVISION){
					if(Q_UNLIKELY(scaledBox0[i][j].at(k) <= 1.0))
						normalizedBox[i][j][k] = 0.0; // A T1 pixel value of zero is an artifact anyway, so any uptake is noise in the signal
					else
						normalizedBox[i][j][k] = (scaledBox[i][j].at(k) - scaledBox0[i][j].at(k)) / scaledBox0[i][j].at(k);
				}
			}
		}
	}

#if useDebug
	qDebug() << QString("GLCM3D (%1): Start GLCM Calculation").arg(time.elapsed() / 1000.0, 7, 'f', 3);
#endif
	QVector<QVector<int> > glcm = glcmCalc(xyzData.at(0), xyzData.at(1), xyzData.at(2), normalizedBox, localglcm, twoDmethod, numLevels, displacement);
#if useDebug
	qDebug() << QString("GLCM3D (%1): Finished GLCM Calculation").arg(time.elapsed() / 1000.0, 7, 'f', 3);
#endif

	QVector<QVector<double> > output;
	output << glcmFeats(glcm);
#if useDebug
	qDebug() << QString("GLCM3D (%1): Finished GLCM Feature Calculation").arg(time.elapsed() / 1000.0, 7, 'f', 3);
#if GLCM_DEBUG_TABLE
	DebugTableMaker * tableMaker = new DebugTableMaker( glcm );
	tableMaker->setWindowTitle(QString("GLCM: t = %1, d = %2, MaxCC = %3").arg(midTime).arg(displacement).arg(output.first().at(9)));
	tableMaker->moveToThread(QApplication::instance()->thread());
	QObject::connect(tableMaker, &DebugTableMaker::finished, tableMaker, &DebugTableMaker::deleteLater);
	QMetaObject::invokeMethod(tableMaker, [tableMaker](){tableMaker->makeTable();});
#endif
#endif
	if(morphology)
		output << morphologyFeats(&(xyzData[0]), &(xyzData[1]), &(xyzData[2]), &normalizedBox, qMin(dx, qMin(dy, dz)));

#if useDebug
	if(morphology)
		qDebug() << QString("GLCM3D (%1): Finished Morphology Feature Calculation").arg(time.elapsed() / 1000.0, 7, 'f', 3);
	else
		qDebug() << "Not Running Morphology Feature Calculation";
#endif
	return output;
}

/*!
 * \brief Creates a copy of the data array with isotropic distances between pixels.
 * \param data Raw data used for feature calculation.
 * \param dx The distance between pixels in the x dimension of the coordinate system.
 * \param dy The distance between pixels in the y dimension of the coordinate system.
 * \param dz The distance between pixels in the z dimension of the coordinate system.
 * \param interpMethod
 * \return The isotropisized 3D data array.
 */
inline QVector<QVector<QVector<double> > > isotropisize3D(QVector<QVector<QVector<quint16> > > *data, double dx, double dy, double dz, int interpMethod)
{
	// Output Data is at the Minimum Resolution
	double dOutput = qMin(dx, qMin(dy, dz));
	// data is expected to be in [x][y][z] format from bounding box
	int nx = (*data).size();
	int ny = (*data).last().size();
	int nz = (*data).last().last().size();

	int numberOfDimensionsToChange = 0;
	numberOfDimensionsToChange += qFuzzyCompare(dx, dOutput) ? 0 : 1;
	numberOfDimensionsToChange += qFuzzyCompare(dy, dOutput) ? 0 : 1;
	numberOfDimensionsToChange += qFuzzyCompare(dz, dOutput) ? 0 : 1;

	QVector<QVector<QVector<double> > > output;

	QScopedPointer<QVector<QVector<QVector<quint16> > > > newData(nullptr); // We'll need to delete that newData pointer outside of the if statement below
	if(numberOfDimensionsToChange == 2){
		QVector<QVector<QVector<double> > > partialResult;
		if(qFuzzyCompare(dx, dOutput)){
			partialResult = isotropisize3D(data, dx, dy, dx, interpMethod);
			dy = dOutput;
			ny = partialResult.last().size();
		}
		else if(qFuzzyCompare(dy, dOutput)){
			partialResult = isotropisize3D(data, dx, dy, dy, interpMethod);
			dx = dOutput;
			nx = partialResult.size();
		}
		else{ //(dz == dOutput)
			partialResult = isotropisize3D(data, dz, dy, dz, interpMethod);
			dy = dOutput;
			ny = partialResult.last().size();
		}
		// Now, create new data vector from the first result
		newData.reset(new QVector<QVector<QVector<quint16> > >(nx, QVector<QVector<quint16> >(ny, QVector<quint16>(nz))));
		data = newData.data();
//#pragma omp parallel for
		for(int i = 0; i < nx; i++)
			for(int j = 0; j < ny; j++)
				for(int k = 0; k < nz; k++)
					(*data)[i][j][k] = static_cast<quint16>(qRound(partialResult[i][j].at(k)));
	}

	if(!qFuzzyCompare(dx, dOutput)){
		double ratio = dx / dOutput;
		auto outputSize = qCeil(nx * ratio);
		output = QVector<QVector<QVector<double> > >(outputSize, QVector<QVector<double> >(ny, QVector<double>(nz)));
		int i;
//#pragma omp parallel for
		for(int j = 0; j < ny; j++)
			for(int k = 0; k < nz; k++){
				QVector<quint16> lineVector(nx);
				for(i = 0; i < nx; i++)
					lineVector[i] = (*data)[i][j][k];
				QVector<double> dataVector = interpolateVector(&lineVector, ratio, interpMethod);
				for(i = 0; i < outputSize; i++)
					output[i][j][k] = dataVector[i];
			}
	}
	else if(!qFuzzyCompare(dy, dOutput)){
		double ratio = dy / dOutput;
		auto outputSize = qCeil(ny * ratio);
		output = QVector<QVector<QVector<double> > >(nx, QVector<QVector<double> >(outputSize, QVector<double>(nz)));
		int j;
//#pragma omp parallel for
		for(int i = 0; i < nx; i++)
			for(int k = 0; k < nz; k++){
				QVector<quint16> lineVector(ny);
				for(j = 0; j < ny; j++)
					lineVector[j] = (*data)[i][j][k];
				QVector<double> dataVector = interpolateVector(&lineVector, ratio, interpMethod);
				for(j = 0; j < outputSize; j++)
					output[i][j][k] = dataVector[j];
			}
	}
	else{ //(dz != dOutput)
		double ratio = dz / dOutput;
		output = QVector<QVector<QVector<double> > >(nx, QVector<QVector<double> >(ny));
//#pragma omp parallel for
		for(int i = 0; i < nx; i++)
			for(int j = 0; j < ny; j++)
				output[i][j] = interpolateVector(&((*data)[i][j]), ratio, interpMethod);
	}
	return output;
}

/*!
 * \brief Determine which method should be used for interpolation according to the value of method.
 * \param data The vector inputed for interpolation.
 * \param ratio The ratio to resize by.
 * \param method Integer value determining method of interpolation.
 * \return Interpolated vector.
 */
inline QVector<double> interpolateVector(QVector<quint16> *data, const double & ratio, int method){

	if (method == 1)
		return monoCubicInterpolateVector(data, ratio);
	// default linear
	    return linearInterpolateVector(data, ratio);
}

/*!
 * \brief Takes a vector and resize it using monotone cubic interpolation.
 * \param data Raw vector of data to have resized.
 * \param ratio The ratio of the expansion, e.g. if the input data has a size of 2 and ratio is 1.5, the output data will have a size of 3.
 * \return Resized vector according to  monotone cubic interpolation.
 */
inline QVector<double> monoCubicInterpolateVector(QVector<quint16> *data, const double & ratio){
	// from the example implementation on the following webpage and using the special case of uniform x-spacing.
	//https://en.wikipedia.org/wiki/Monotone_cubic_interpolation#Example_implementation
	//double outDx = 1.0;
	double inDx = ratio;
	double invInDx = 1.0/ratio;
	double sqInvInDx = invInDx * invInDx;
	int nz = data->size();
	auto outputSize = qCeil(nz * ratio);
	QVector<double> output(outputSize);
	if(data->size() == 1){
		return output.fill((*data)[0]);
	}
	QVector<double> slopes(nz -1);
//#pragma omp parallel for
	for (int i = 0; i < nz - 1; i++){
		slopes[i] =  ( (*data)[i+1] - (*data)[i] ) / inDx;
	}
	QVector<double> c1s(nz);
	c1s[0] = slopes[0];
//#pragma omp parallel for
	for (int i = 0; i < nz - 2; i++ ){
		if (slopes[i] * slopes[i+1] <= 0){
		 c1s[i+1] = 0.0;
	 } else {
		 //c1s[i+1] = (6 * inDx)/ ((3 * inDx)/slopes[i] + (3 * inDx)/slopes[i+1]);
		 c1s[i+1] = (2 * slopes[i] * slopes[i+1]) / (slopes[i] + slopes[i+1]); // simplified version of the above eq.
	 }
	}
	c1s[nz-1] = slopes[nz-2];
	QVector<double> c2s(nz - 1);
	QVector<double> c3s(nz - 1);
//#pragma omp parallel for
	for (int i = 0; i < nz -1; i++){
		double common = c1s[i] + c1s[i+1] - 2*slopes[i];
	  c2s[i] = (slopes[i] - c1s[i] - common) * invInDx;
	  c3s[i] = common * sqInvInDx;
	}

	// now to the interpolation
	double halfPixel = ratio/2.;
//#pragma omp parallel for
	for (int i = 0; i< outputSize; i++){
		//double frac = nz - 1 - (static_cast<double>(outputSize - 1 - i) / ratio);
		double x = 0.5 + static_cast<double>(i);
		int spot = static_cast<int>( (x - halfPixel)/ratio);
		double frac = x - (spot*ratio + halfPixel);
		if((spot >= 0) && (spot < nz - 1))
			output[i] = (*data)[spot] + c1s[spot] * frac + c2s[spot] * frac*frac + c3s[spot]*frac*frac*frac;
		else {
			output[i] = (*data)[spot] + c1s[spot] * frac; // linear extrapolation
		}
	}
#if useDebug
	qDebug() << "Done with interpolation";
	qDebug() << (*data);
	qDebug() << output;
#endif
	return output;
}

/*!
 * \brief Resize the data vector according to the ratio and interpolate empty indices.
 * \param data
 * \param ratio
 * \return The resized vector.
 */
inline QVector<double> linearInterpolateVector(QVector<quint16> *data, const double &ratio)
{
	int nz = data->size();
	auto outputSize = qCeil(nz * ratio);
	QVector<double> output(outputSize);
//#pragma omp parallel for
	for(int i = 0; i < outputSize; i++){
		double frac = nz - 1 - (static_cast<double>(outputSize - 1 - i) / ratio);
		int spot = static_cast<int>(frac);
		frac = frac - spot;
		if(spot < nz - 1)
			output[i] = ((1.0 - frac) * (*data)[spot]) + (frac * (*data)[spot + 1]);
		else
			output[i] = (*data)[spot];
	}
	return output;
}

/*!
 * \brief This converts (x,y,z) locations in raw data space to the locations in isotropic space.
 * \param xData The list of x locations for each point.
 * \param yData The list of y locations for each point.
 * \param zData The list of z locations for each point.
 * \param dx The distance between pixels in the x dimension of the coordinate system.
 * \param dy The distance between pixels in the y dimension of the coordinate system.
 * \param dz The distance between pixels in the z dimension of the coordinate system.
 * \return A vector of vectors of (x, y, z) locations in isotropic space.
 */
inline QVector<QVector<int> > isotrpisizeData(const QVector<int> & xData, const QVector<int> & yData, const QVector<int> & zData, double dx, double dy, double dz)
{
	// Output Data is at the Minimum Resolution
	auto dOutput = qMin(dx, qMin(dy, dz));
	QVector<QVector<int> > xyzData(3);

	auto qiMinMaxPadded = [](auto d){auto p = std::minmax_element(d.begin(), d.end()); return std::make_pair( (*p.first) - 1, (*p.second) + 1);};
	int minx, maxx, miny, maxy, minz, maxz;
	std::tie(minx, maxx) = qiMinMaxPadded(xData);
	std::tie(miny, maxy) = qiMinMaxPadded(yData);
	std::tie(minz, maxz) = qiMinMaxPadded(zData);

	if(!qFuzzyCompare(dx, dOutput)){
		auto size = maxx - minx + 1;
		auto ratio = dx / dOutput;
		auto outputSize = qCeil(size * ratio);
#pragma omp parallel for
		for(int x = outputSize - 1; x >= 0; x--){
			QVector<int> xOut;
			QVector<int> yOut;
			QVector<int> zOut;
			auto oldx = minx + size - 1 - ((outputSize - 1 - x) / ratio);  // Weijie isotropisizes based on z = maxz same vals
			auto testa = qFloor(oldx);
			auto testb = qCeil(oldx);
			auto points = QVector<QPoint> (0);
//#pragma omp parallel for
			for(int i = 0; i < xData.size(); i++){
				if(xData.at(i) == testa){
					xOut.append(x);
					yOut.append(yData.at(i) - miny);
					zOut.append(zData.at(i) - minz);
					points.append(QPoint(yData.at(i) - miny, zData.at(i) - minz));
				}
			}
			if(testa != testb)
//#pragma omp parallel for
				for(int i = 0; i < xData.size(); i++){
					if(xData.at(i) == testb){
						if(isNewPoint(QPoint(yData.at(i) - miny, zData.at(i) - minz), points)){
							xOut.append(x);
							yOut.append(yData.at(i) - miny);
							zOut.append(zData.at(i) - minz);
						}
					}
				}
#pragma omp flush(xyzData)
#pragma omp critical
			{
				xyzData[0] << xOut;
			xyzData[1] << yOut;
			xyzData[2] << zOut;
			}
		}
	}
	else if(!qFuzzyCompare(dy, dOutput)){
		auto size = maxy - miny + 1;
		auto ratio = dy / dOutput;
		auto outputSize = qCeil(size * ratio);
#pragma omp parallel for
		for(int y = outputSize - 1; y >= 0; y--){
			QVector<int> xOut;
			QVector<int> yOut;
			QVector<int> zOut;
			// Weijie isotropisizes based on z = maxz same vals
			auto oldy = miny + size - 1 - ((outputSize - 1 - y) / ratio);
			auto testa = qFloor(oldy);
			auto testb = qCeil(oldy);
			auto points = QVector<QPoint> (0);
//#pragma omp parallel for
			for(int i = 0; i < yData.size(); i++){
				if(yData.at(i) == testa){
					xOut.append(xData.at(i) - minx);
					yOut.append(y);
					zOut.append(zData.at(i) - minz);
					points.append(QPoint(xData.at(i) - minx, zData.at(i) - minz));
				}
			}
			if(testa != testb)
//#pragma omp parallel for
				for(int i = 0; i < yData.size(); i++){
					if(yData.at(i) == testb){
						if(isNewPoint(QPoint(xData.at(i) - minx, zData.at(i) - minz), points)){
							xOut.append(xData.at(i) - minx);
							yOut.append(y);
							zOut.append(zData.at(i) - minz);
						}
					}
				}
#pragma omp flush(xyzData)
#pragma omp critical
			{
				xyzData[1] << xOut;
			xyzData[2] << yOut;
			xyzData[3] << zOut;
			}
		}
	}
	else if(!qFuzzyCompare(dz, dOutput)){
		auto size = maxz - minz + 1;
		auto ratio = dz / dOutput;
		auto outputSize = qCeil(size * ratio);
#pragma omp parallel for
		for(int z = outputSize - 1; z >= 0; z--){
			QVector<int> xOut;
			QVector<int> yOut;
			QVector<int> zOut;
			auto oldz = minz + size - 1 - ((outputSize - 1 - z) / ratio);  // Weijie isotropisizes based on z = maxz same vals
			auto testa = qFloor(oldz);
			auto testb = qCeil(oldz);
			auto points = QVector<QPoint> (0);
//#pragma omp parallel for
			for(int i = 0; i < zData.size(); i++){
				if(zData.at(i) == testa){
					{
						xOut.append(xData.at(i) - minx);
					yOut.append(yData.at(i) - miny);
					zOut.append(z);
					}
					points.append(QPoint(xData.at(i) - minx, yData.at(i) - miny));
				}
			}
			if(testa != testb)
//#pragma omp parallel for
				for(int i = 0; i < zData.size(); i++){
					if(zData.at(i) == testb){
						if(isNewPoint(QPoint(xData.at(i) - minx, yData.at(i) - miny), points)){
							{
								xOut.append(xData.at(i) - minx);
							yOut.append(yData.at(i) - miny);
							zOut.append(z);
							}
						}
					}
				}
#pragma omp flush(xyzData)
#pragma omp critical
			{
				xyzData[0] << xOut;
			xyzData[1] << yOut;
			xyzData[2] << zOut;
			}
		}
	}
	//xyzData << xOut;
	//xyzData << yOut;
	//xyzData << zOut;
	return xyzData;
}

/*!
 * \brief Checks if point is in polygon or if it is a new point.
 * \param point
 * \param polygon
 * \return A boolean determining whether it's true or not if the point is in the polygon.
 */
inline bool isNewPoint(QPoint point,const QVector<QPoint>& polygon)
{
	/*for(int i=0;i<polygon.size();i++)
			if(point == polygon.at(i))
				return false;
		return true;
		*/
	return !polygon.contains(point);
}

/*!
 * \brief Calculates the dynamic features using variance data.
 * \param data The pixel values used for the dynamic feature calculations.
 * \param normType Normalization Type.
 * \param elapsedTime The amount of time elapsed from the data at index 0 to the data at each index. This vector should be the same size as data.
 * \param midTime The timepoint used for the 'initial rise' portion of the calculations.
 * \return An output vector containing  Weijie's 5 dynamic features with variance data.
 */
QVector<double> varDynFeature(const QVector<QVector<double> > & data, const NORMALIZATION_TYPE & normType, const QVector<int> & elapsedTime, const int &midTime)
{
	int totalPts = data.size();
	int timePts =  data.at(0).size();
#define USE_NORMALIZED_VAR_DYN_DATA 0
	auto normalized = data;
	QVector<double> means(timePts, 0.0);
	QVector<double> variances(timePts, 0.0);

	for(int z = 0; z < totalPts; z++){

		if(normType | NORMALIZATION_PRECONTRAST_DIVISION)
			for(int i=0;i<timePts;i++)
				normalized[z][i] = (data[z][i] - data[z][0]) / qMax(data[z][0], 1.0);

		else if(normType | NORMALIZATION_PRECONTRAST_SUBTRACTION)
			for(int i=0;i<timePts;i++)
				normalized[z][i] = data[z][i] - data[z][0];

		for(int i=0;i<timePts;i++)
			means[i] += normalized[z][i] / totalPts;

	}

	for(int z = 0; z < totalPts; z++)
		for(int i=0;i<timePts;i++)
			variances[i] += pow((normalized[z][i] - means[i]), 2) / totalPts;

	return dynamicFeature(variances, false, elapsedTime, midTime);
}

