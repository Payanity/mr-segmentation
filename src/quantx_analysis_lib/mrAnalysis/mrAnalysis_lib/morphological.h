#ifndef MORPHOLOGICAL_H
#define MORPHOLOGICAL_H

#include <QVector>
#include <QPoint>
#include "mranalysisdllcheck.h"

//!
//! \funcReqBegin
//! \funcReq36
//! \funcReqEnd
//!
//! \addtogroup MRAnalysisModule
//! @{
QIMRANALYSIS_EXPORT QVector<QPoint>  dilatePolygon2D(const QVector<QPoint>&  input, int radius, bool diagonals = true, QVector<QPoint>  target = QVector<QPoint> (0));
QVector<QVector<QPoint> > dilatePolygon3D(const QVector<QVector<QPoint> >& input, int radiusXY, int radiusZ, bool diagonals, QVector<QVector<QPoint> > target);
void floodFillSeg(QPoint first, int z, const QVector<int> & mins, const QVector<QVector<QPoint> >  & inputRegion, QVector<QVector<QVector<bool> > > *filledRegion, const bool inside);
void floodFillInside(QPoint first, int z, const QVector<int> & mins, const QVector<QVector<QPoint> > & inputRegion, QVector<QVector<QVector<bool> > > *filledRegion);
QIMRANALYSIS_EXPORT QVector<QVector<QPoint> > labelRegions2D(const QVector<QPoint> & inputRegion);
QIMRANALYSIS_EXPORT QVector<QVector<QVector<QPoint> > > labelRegions3D(QVector<QVector<QPoint> > inputRegion);
void floodFillOutside(QPoint first, int z, const QVector<int> & mins, const QVector<QVector<QPoint> > & inputRegion, QVector<QVector<QVector<bool> > > *filledRegion);
QIMRANALYSIS_EXPORT QVector<QVector<QPoint> > holeFill3D(QVector<QVector<QPoint> > inputRegion);
QIMRANALYSIS_EXPORT QVector<QPoint> holeFill2D(QVector<QPoint> inputRegion);
//! @}
//!


#endif // MORPHOLOGICAL_H

