#include "bnn.h"
#include <QStringList>
#include <QFile>
//#define _USE_MATH_DEFINES // for C++
#include <cmath>
#include <QPointer>

/*!
 * \brief bnnTest Runs a neural network test using pre-calculated parameters.
 * \param data The vector of features used as input.
 * \param windowMode 7 for mammo weights, 8 for ultrasound.
 * \return
 */
double bnnTest(QVector<double> data,int windowMode){
    auto nin = data.size();
	QScopedPointer<QFile, QScopedPointerDeleteLater> musigmafile;
	QScopedPointer<QFile, QScopedPointerDeleteLater> weightfile;
    if(windowMode == 2){
        musigmafile.reset(new QFile(":/bnn/t2musigma"));
        weightfile.reset(new QFile(":/bnn/t2weights"));
    }
    else if(windowMode == 3){
        musigmafile.reset(new QFile(":/bnn/invmusigma"));
        weightfile.reset(new QFile(":/bnn/invweights"));
    }
    else if(windowMode == 4){
        musigmafile.reset(new QFile(":/bnn/lnmusigma"));
        weightfile.reset(new QFile(":/bnn/lnweights"));
    }
    else if(windowMode == 5){
        musigmafile.reset(new QFile(":/bnn/gradmusigma"));
        weightfile.reset(new QFile(":/bnn/gradweights"));
    }
    else if(windowMode == 6){
        musigmafile.reset(new QFile(":/bnn/resmusigma"));
        weightfile.reset(new QFile(":/bnn/resweights"));
    }
    else if(windowMode == 7){//mammo
        weightfile.reset(new QFile(":/bnn/mammoweights"));
    }
    else if(windowMode == 8){//us
        weightfile.reset(new QFile(":/bnn/usweights"));
    }
    else return 0.50; //no weight file for this mode yet

    QStringList mus;
    QStringList sigmas;
    if (windowMode < 7) {// everything except mammo and US
        musigmafile->open(QIODevice::ReadOnly);
        mus = QString(musigmafile->readLine()).split(',');    //mus    = minn if using [0,1] normalization
        sigmas = QString(musigmafile->readLine()).split(','); //sigmas = dif  if using [0,1] normalization
        musigmafile->close();
    } else {
        for (int i=0;i<nin;i++){
            mus<<QString("0");
            sigmas<<QString("1");
        }
    }


    //read weights file
    QStringList weightString;
    weightfile->open(QIODevice::ReadOnly);
    if(nin != QString(weightfile->readLine()).toInt())
        return -1.0; //WRONG SIZE OF DATA VECTOR
    auto nhidden = QString(weightfile->readLine()).toInt();
    //nout = data.size();
    //nout will be based on size of input data vector, once it is 2D instead of 1D
    auto nout = 1;
    weightfile->readLine(); //this is also nout

    QVector<QVector<double> > w1(nhidden);
    QVector<double>           b1(nhidden);
    QVector<QVector<double> > w2(nout);
    QVector<double>           b2(nout);
    QVector<double>           a(nout);
    QVector<double>           z(nhidden);
    QVector<double>           y(nout);

    //normalize data for BNN
    for(int i=0;i<nin;i++){
        data[i] = (data[i]-mus.at(i).toDouble())/sigmas.at(i).toDouble();
        if(windowMode == 4){
            if(data[i]<0)
                data[i] = 0.0;
            if(data[i]>1)
                data[i] = 1.0;
        }
    }

    //read in weights
    //weightString = QString(weightfile->readLine()).split('\t');

    //To support more nouts, loop from 0->data.size() when it is 2D
    //FOLLOWING IS mlpunpak
    for(int i=0;i<nhidden;i++){
        w1[i].clear();
        weightString = QString(weightfile->readLine()).split('\t');
        for(int j=0;j<nin;j++)
            w1[i] << weightString.takeFirst().toDouble();
    }
    weightString = QString(weightfile->readLine()).split('\t');
    for(int i=0;i<nhidden;i++)
        b1[i] = weightString.takeFirst().toDouble();
    for(int i=0;i<nout;i++){
        w2[i].clear();
        weightString = QString(weightfile->readLine()).split('\t');
        for(int j=0;j<nhidden;j++)
            w2[i] << weightString.takeFirst().toDouble();
    }
    weightString = QString(weightfile->readLine()).split('\t');
    for(int i=0;i<nout;i++)
        b2[i] = weightString.takeFirst().toDouble();
    //NOW WE RUN mlpfwd
    for(int i=0;i<nhidden;i++){
        z[i] = b1[i];
        for(int j=0;j<nin;j++)
            z[i] += data[j] * w1[i][j];
        z[i] = tanh(z[i]);
    }
    for(int i=0;i<nout;i++){
        a[i] = b2[i];
        for(int j=0;j<nhidden;j++)
            a[i] +=  z[j]   * w2[i][j];
        //mincut & maxcut???
        y[i] = 1 / (1 + exp(-a[i]));
    }
    //return only first output unless support for more than one input vector is added
    return y.first();
}


/*!
 * \brief For running many, many bnns instead of just one.
 * \param data The vector of features used as input.
 * \return
 */
QVector<double> mcmcBnnTest(QVector<double> data){
    auto nin = data.size();
    QScopedPointer<QFile> musigmafile(new QFile(":/bnn/cadxmusigma"));
    QScopedPointer<QFile> weightfile (new QFile(":/bnn/cadxweights"));

    musigmafile->open(QIODevice::ReadOnly);
    QStringList mus = QString(musigmafile->readLine()).split(',');
    QStringList sigmas = QString(musigmafile->readLine()).split(',');
    musigmafile->close();

    //normalize data for BNN
    for(int i=0;i<nin;i++)
        data[i] = (data[i]-mus.at(i).toDouble())/sigmas.at(i).toDouble();

    //read weights file
    QStringList weightString;
    weightfile->open(QIODevice::ReadOnly);
    if(nin != QString(weightfile->readLine()).toInt())
        return QVector<double>(1,-1.0); //WRONG SIZE OF DATA VECTOR
    auto nhidden = QString(weightfile->readLine()).toInt();
    auto nout = QString(weightfile->readLine()).toInt();
    auto nsamples = 0;
    QVector<QVector<double> > w1(nhidden);
    QVector<double>           b1(nhidden);
    QVector<QVector<double> > w2(nout);
    QVector<double>           b2(nout);
    QVector<double> a(nout);
    QVector<double> z(nhidden);
    QVector<QVector<double> > y(nout);
    while(!weightfile->atEnd()){
        weightString = QString(weightfile->readLine()).split(',');
        nsamples++;
        //FOLLOWING IS mlpunpak
        for(int i=0;i<nhidden;i++){
            w1[i].clear();
            for(int j=0;j<nin;j++)
                w1[i] << weightString.takeFirst().toDouble();
        }
        for(int i=0;i<nhidden;i++)
            b1[i] = weightString.takeFirst().toDouble();
        for(int i=0;i<nout;i++){
            w2[i].clear();
            for(int j=0;j<nhidden;j++)
                w2[i] << weightString.takeFirst().toDouble();
        }
        for(int i=0;i<nout;i++)
            b2[i] = weightString.takeFirst().toDouble();
        //NOW WE RUN mlpfwd
        for(int i=0;i<nhidden;i++){
            z[i] = b1[i];
            for(int j=0;j<nin;j++)
                z[i] += data[j] * w1[i][j];
            z[i] = tanh(z[i]);
        }
        for(int i=0;i<nout;i++){
            a[i] = b2[i];
            for(int j=0;j<nhidden;j++)
                a[i] +=  z[j]   * w2[i][j];
            //mincut & maxcut???
            y[i] << 1 / (1 + exp(-a[i]));
        }
    }
    QVector<double> output(nout);
    for(int i=0;i<nout;i++){
        output[i] = 0.0;
        for(int j=0;j<nsamples;j++)
            output[i] += y[i][j];
        output[i] = output[i] / nsamples;
    }
    return output;
}

