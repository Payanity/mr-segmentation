#if 0
#include "mranalysis.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#define useDebug 0

#if useDebug
#include <QDebug>
#endif

QByteArray MrAnalysis::json()
{
	if (!analysisDone_)
		return QByteArray();
	QJsonObject jsonRoot;
	QJsonObject seedPointObject;
	QJsonObject featuresObject;
	QJsonArray segmentationObject;
	jsonRoot.insert("SeriesUID", image->seriesUID());
	seedPointObject.insert("X", xSeed);
	seedPointObject.insert("Y", ySeed);
	seedPointObject.insert("Z", zSeed);
	jsonRoot["SeedPoint"] = seedPointObject;
	int i = 0;
	for(QString feature : getFeatureNames())
	{
		featuresObject.insert(feature.remove(QChar(' ')), features.at(i));
		i++;
	}
	jsonRoot["Features"] = featuresObject;

	QVector<QVector<QPoint> > segmentation = getFilledRegion(QUANTX::XYPLANE);
	for (int i=0; i<segmentation.size(); i++){
		if (segmentation[i].size() > 0){
			QJsonObject segSliceObject;
			segSliceObject.insert("Slice", i);
			QJsonArray pointsArray;
			for (auto point : segmentation.at(i))
			{
				QJsonObject pointObject;
				pointObject.insert("X", point.x());
				pointObject.insert("Y", point.y());
				pointsArray.append(pointObject);
			}
			segSliceObject["Points"] = pointsArray;
			segmentationObject.append(segSliceObject);
		}
	}
	jsonRoot["Segmentation"] = segmentationObject;

	QJsonDocument doc = QJsonDocument(jsonRoot);

#if useDebug
#include <QFile>
	QFile file;
	//write out both indented and compact formats
	for (int j=0; j < 2; j++)
	{
		file.setFileName(QString("test%1.json").arg(j));
		file.open(QIODevice::WriteOnly);
		file.write(doc.toJson(static_cast<QJsonDocument::JsonFormat>(j)));
		file.close();
	}
	qDebug().noquote() << doc.toJson(); //Depending on size of segmentation and corresponding length of JSON output, this might fail to print out
#endif

	return doc.toJson();
}
#endif
