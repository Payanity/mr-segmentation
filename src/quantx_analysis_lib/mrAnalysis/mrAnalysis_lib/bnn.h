#ifndef MCMCBNN_H
#define MCMCBNN_H

#include <QVector>


//!
//! \addtogroup MRAnalysisModule
//! @{
//!
double bnnTest(QVector<double> data,int windowMode);
QVector<double> mcmcBnnTest(QVector<double> data);
//! @}
//!

#endif // MCMCBNN_H
