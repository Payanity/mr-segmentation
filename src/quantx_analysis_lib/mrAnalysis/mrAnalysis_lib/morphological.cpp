#include "mranalysis.h"
#include "morphological.h"
#include <QPoint>
#include <QVector>
#include <QVector3D>
#include <QBitArray>

#define useDebug 0
#if useDebug
#include <QDebug>
#endif

/*!
 * \brief Dilate a Polygon with specified radius (radius of a square). Be careful when using this function as it does not check for borders of the image
 * \param input A vector of points that represent the polygon.
 * \param radius The magnitude of the radius of the inputed polygon.
 * \param diagonals If the polygon has diagonals.
 * \param target A vector reprsenting the points of a new target polgon that has gone through dilation.
 * \return A vector of points representing a new, dilated polygon.
 */
QVector<QPoint>  dilatePolygon2D(const QVector<QPoint>&  input, int radius, bool diagonals, QVector<QPoint> target){
	//!diameter = 1 + ( 2 * radius )
	//!diagonals = true for 8-connected, false for 4-connected dilation (creates diamond instead of square)
	radius = abs(radius);
	for(auto i : input){
		for(int x=-radius;x<=radius;x++){
			for(int y=-radius;y<=radius;y++){
				if(!diagonals)
					if(abs(x) + abs(y) > radius)
						continue;
				if(!target.contains(i + QPoint(x,y)))
					target.append(i + QPoint(x,y));
			}
		}
	}
	return target;
}

/*!
 * \brief Dilates a Polygon in the z-direction, but NOT xy-direction.
 * \param input A vector of vectors of points that represent a 3D polygon (a polygon formed from layers of 2D polygons).
 * \param radiusXY The magnitude of the radius in the XY direction.
 * \param radiusZ The magnitude of the radius in the z-direction.
 * \param diagonals If the polygon has diagonals.
 * \param target A vector reprsenting the points of a new target polgon that has gone through dilation.
 * \return A vector of vectors of points representing a new, dilated polygon in the z-direction.
 */
QVector<QVector<QPoint> > dilatePolygon3D(const QVector<QVector<QPoint> >& input, int radiusXY, int radiusZ, bool diagonals = true, QVector<QVector<QPoint> > target = QVector<QVector<QPoint> >(0)){
	if(!(input.size() == target.size())){
		target.resize(input.size());
		for(auto & i : target)
			i.resize(0);
	}
	radiusXY = abs(radiusXY);
	radiusZ  = abs(radiusZ);
	for(int i=0;i<input.size();i++){
		if(input.at(i).empty())
			continue;
		for(int z=-radiusZ;z<=radiusZ;z++){
			if(((i+z) < 0) || ((i+z) > input.size()))
				continue;
			if(diagonals)
				target[i+z] = dilatePolygon2D(input.at(i),radiusXY,diagonals,target.at(i+z));
			else{ //diagonals == false
				if(abs(z) > radiusXY)
					continue;
				target[i+z] = dilatePolygon2D(input.at(i),radiusXY-abs(z),diagonals,target.at(i+z));
			}
		}
	}
	return target;
}

/*!
 * \brief Flood fill algorithm on binary data of a polygon.
 * \param first Seed point of flood fill algorithm.
 * \param mins x,y,z coordinates of origin of filledRegion.
 * \param inputRegion Polygon (vector of points) representing a slice of the fixed z-dimension.
 * \param filledRegion 3D vector holding coordinates where filled data is placed.
 * \param inside If inside region.
 */
void floodFill2D(QPoint first, const QPoint & mins, const QVector<QPoint>  & inputRegion, QVector<QVector<bool> > *filledRegion, bool inside)
{
	QList<QPoint> pointqueue;
	pointqueue.append(first);
	int maxx = filledRegion->first().size();
	int maxy = filledRegion->size();
	while(!pointqueue.isEmpty()){
		QPoint cur = pointqueue.takeFirst();
		int curx = cur.x();
		int cury = cur.y();
		if(curx < 0 || cury < 0)
			continue;
		if(curx >= maxx)
			continue;
		if(cury >= maxy)
			continue;
		if ((*filledRegion)[cury].at(curx))
			continue;
		//!check for being in the region
		if(inside){
			if(!inputRegion.contains(QPoint(curx+(mins.x()),cury+(mins.y()))))
				continue;
		}
		else{
			if(inputRegion.contains(QPoint(curx+(mins.x()),cury+(mins.y()))))
				continue;
		}

		(*filledRegion)[cury][curx] = true;

		pointqueue.append(QPoint(curx+1,cury));
		pointqueue.append(QPoint(curx-1,cury));
		pointqueue.append(QPoint(curx,cury+1));
		pointqueue.append(QPoint(curx,cury-1));
	}
    }

/*!
 * \brief Flood fill algorithm on binary data of a 3D polygon.
 * \param first Seed point of flood fill algorithm.
 * \param z The z-axis in the 3D polygon.
 * \param mins x,y,z coordinates of origin of filledRegion.
 * \param inputRegion The 3D polygon (vector of vectors of points) to flood fill.
 * \param filledRegion 3D vector holding coordinates where filled data is placed.
 * \param inside If inside region.
 */
void floodFillSeg(QPoint first, int z, const QVector<int> & mins, const QVector<QVector<QPoint> >  & inputRegion, QVector<QVector<QVector<bool> > > *filledRegion, const bool inside)
{
	auto maxx = filledRegion->first().first().size();
	auto maxy = filledRegion->first().size();
	auto maxz = filledRegion->size();
#if useDebug
	qDebug() << "create new BitArray";
#endif
	QVector<QVector<QBitArray> > allRegions(maxz,QVector<QBitArray>(maxy,QBitArray(maxx,false)));
	for(auto i = mins[0]; i < maxz + mins[0]; ++i)
		for(const auto & p : inputRegion.at(i))
			allRegions[i-mins[0]][p.y()-mins[1]].setBit(p.x()-mins[2], true);

	QVector<PointVector3D<int> > pointqueue;
	pointqueue.append(PointVector3D<int>(first.x(),first.y(),z));
#if useDebug
	qDebug() << "start processing pointqueue";
#endif
	while(!pointqueue.isEmpty()){
		auto cur = pointqueue.takeFirst();
		auto curx = cur.x();
		auto cury = cur.y();
		auto curz = cur.z();
		if(curx < 0 || cury < 0 || curz < 0)
			continue;
		if(curx >= maxx)
			continue;
		if(cury >= maxy)
			continue;
		if(curz >= maxz)
			continue;
		if ((*filledRegion)[curz][cury].at(curx))
			continue;
		//!check for being in the region
		if(inside){
			//if(!inputRegion.at(curz+(mins.at(0))).contains(QPoint(curx+(mins.at(2)),cury+(mins.at(1)))))
			if(!allRegions[curz][cury][curx])
				continue;
		}
		else{
			//if(inputRegion.at(curz+(mins.at(0))).contains(QPoint(curx+(mins.at(2)),cury+(mins.at(1)))))
			if(allRegions[curz][cury][curx])
				continue;
		}

		(*filledRegion)[curz][cury][curx] = true;

#if useDebug
		//qDebug() << "append new points in the queue";
#endif
		pointqueue.append(PointVector3D<int>(curx+1,cury,curz));
		pointqueue.append(PointVector3D<int>(curx-1,cury,curz));
		pointqueue.append(PointVector3D<int>(curx,cury+1,curz));
		pointqueue.append(PointVector3D<int>(curx,cury-1,curz));
		pointqueue.append(PointVector3D<int>(curx,cury,curz+1));
		pointqueue.append(PointVector3D<int>(curx,cury,curz-1));

	}
    }

/*!
 * \brief Uses floodFillSeg to flood fill inside the region.
 * \param first Seed point of flood fill algorithm.
 * \param z The z-axis in the 3D polygon.
 * \param mins x,y,z coordinates of origin of filledRegion.
 * \param inputRegion The 3D polygon (vector of vectors of points) to flood fill.
 * \param filledRegion 3D vector holding coordinates where filled data is placed.
 */
void floodFillInside(QPoint first, int z, const QVector<int> & mins, const QVector<QVector<QPoint> > & inputRegion, QVector<QVector<QVector<bool> > >* filledRegion)
{
	floodFillSeg(first,z,mins,inputRegion,filledRegion,true);
}

/*!
 * \brief Takes a polygon (vector of points) and a returns a vector where each item is a set of input points that are connected as a single region.
 * \param inputRegion The input points to be labeled as separate regions.
 * \return A vector where each item is a set of input points that are connected as a single region.
 */
QVector<QVector<QPoint> > labelRegions2D(const QVector<QPoint> &inputRegion)
{
	const auto region3D = labelRegions3D(QVector<QVector<QPoint> >(1,inputRegion));
	QVector<QVector<QPoint> > result;
	for(const auto & r : region3D)
		result << r.first();
	return result;
}

/*!
 * \brief Takes a 3D polygon (vector of vector of points) and a returns a vector where each item is a set of input polygons (vectors of points) that are connected as a single region.
 * \param inputRegion The input polygons (vectors of points) to be labeled as separate regions.
 * \return A vector where each item is a set of input polygons (vectors of points) that are connected as a single region.
 */
QVector<QVector<QVector<QPoint> > > labelRegions3D(QVector<QVector<QPoint> > inputRegion)
{
#if useDebug
	qDebug() << "in labelRegions3D";
#endif
	QVector<QVector<QVector<QPoint> > > output(0);

	int maxZ = inputRegion.size()-1;
	int minZ = 0;
	int curZ = 0;
	long totalPoints = 0;
	while(curZ < maxZ){
		if(!inputRegion.at(curZ).empty())
			break;
		curZ++;
	}
	minZ = curZ;
	while(curZ < maxZ){
		if(inputRegion.at(curZ).empty())
			break;
		totalPoints += inputRegion.at(curZ).size();
		curZ++;
	}
	if(maxZ == curZ)
		totalPoints += inputRegion.at(maxZ).size();
	maxZ = curZ;
	if(maxZ == minZ && inputRegion.at(minZ).isEmpty())
		return output;

	int minX = inputRegion.at(minZ).first().x();
	int maxX = minX;
	int minY = inputRegion.at(minZ).first().y();
	int maxY = minY;
	for(int z = minZ; z <= maxZ; z++){
		for(int i=0;i<inputRegion.at(z).size();i++){
			if(inputRegion[z].at(i).x() < minX)
				minX = inputRegion[z].at(i).x();
			if(inputRegion[z].at(i).x() > maxX)
				maxX = inputRegion[z].at(i).x();
			if(inputRegion[z].at(i).y() < minY)
				minY = inputRegion[z].at(i).y();
			if(inputRegion[z].at(i).y() > maxY)
				maxY = inputRegion[z].at(i).y();
		}
	}

	//!create box now
	QVector<QVector<QVector<bool> > > filledRegion(maxZ - minZ + 1, QVector<QVector<bool> >(maxY - minY + 1, QVector<bool>(maxX - minX + 1, false)));
	QVector<int> mins;
	mins << minZ << minY << minX;

	int curRegion = 0;
	for(int z = minZ; z <= maxZ; z++)
		while(!inputRegion[z].empty()){
			floodFillInside(QPoint(inputRegion[z].first().x() - minX, inputRegion[z].first().y() - minY), z - minZ, mins, inputRegion, &filledRegion);
			output.append(QVector<QVector<QPoint> >(inputRegion.size()));
			long numRegionPoints = 0;
			for(int z2 = 0; z2 < filledRegion.size(); z2++)
				for(int y2 = 0; y2 < filledRegion.first().size(); y2++)
					for(int x2 = 0; x2 < filledRegion.first().first().size(); x2++)
						if(filledRegion[z2][y2].at(x2)){
							output[curRegion][z2+minZ].append(QPoint(x2 + minX, y2 + minY));
							inputRegion[z2 + minZ].remove(inputRegion[z2 + minZ].indexOf(QPoint(x2 + minX, y2 + minY)));
							filledRegion[z2][y2][x2]=false;
							++numRegionPoints;
						}
			if(numRegionPoints > (totalPoints / 2)){
				//! This is the largest possible region, so stop scanning for more regions
#if useDebug
				qDebug() << "Leaving labelRegions3D early, found largest possible region.  numRegionPoints:" << numRegionPoints << " totalPoints:" << totalPoints;
#endif
				return output;
			}
			totalPoints -= numRegionPoints;
			curRegion++;
		}

#if useDebug
	qDebug() << "leaving labelRegions3D";
#endif
	return output;
}

/*!
 * \brief Uses floodFillSeg to flood fill outside the region.
 * \param first Seed point of flood fill algorithm.
 * \param z The z-axis in the 3D polygon.
 * \param mins x,y,z coordinates of origin of filledRegion.
 * \param inputRegion The 3D polygon (vector of vectors of points) to flood fill.
 * \param filledRegion 3D vector holding coordinates where filled data is placed.
 */
void floodFillOutside(QPoint first, int z, const QVector<int> & mins, const QVector<QVector<QPoint> > & inputRegion, QVector<QVector<QVector<bool> > >* filledRegion){
	floodFillSeg(first, z, mins, inputRegion, filledRegion, false);
}

/*!
 * \brief Fills the holes in a given polygon (vector of points).
 * \param inputRegion The polygon (vector of points) in which to fill the empty (x,y) points so the polgyon has no holes.
 * \return A polygon with every consecutive point in it (no gaps in it).
 */
QVector<QPoint> holeFill2D(QVector<QPoint> inputRegion)
{
	if(inputRegion.isEmpty())
		return inputRegion;

	int minX = inputRegion.first().x();
	int maxX = minX;
	int minY = inputRegion.first().y();
	int maxY = minY;
	for(auto i : inputRegion){
		if(i.x() < minX)
			minX = i.x();
		if(i.x() > maxX)
			maxX = i.x();
		if(i.y() < minY)
			minY = i.y();
		if(i.y() > maxY)
			maxY = i.y();
	}
	minX = minX - 1;
	maxX = maxX + 1;
	minY = minY - 1;
	maxY = maxY + 1;
	QPoint mins(minX,minY);

	//!create box now
	QVector<QVector<bool> > filledRegion(QVector<QVector<bool> >(maxY - minY + 1, QVector<bool>(maxX - minX + 1, false)));

	floodFill2D(QPoint(0,0), mins, inputRegion, &filledRegion, false);

	for(int y = 0; y < filledRegion.size(); y++)
		for(int x = 0; x < filledRegion[y].size(); x++)
			if(!filledRegion[y].at(x))
				if(!inputRegion.contains(QPoint(minX + x, minY + y)))
					inputRegion.append(QPoint(minX + x, minY + y));

	return inputRegion;
}

/*!
 * \brief Fills the holes in a given 3D polygon (vector of vectors of points).
 * \param inputRegion The 3D polygon (vector of vector of points) in which to fill the empty (x,y) points so the polgyon has no holes.
 * \return A 3D polygon with every consecutive point in it (no gaps in it).
 */
QVector<QVector<QPoint> > holeFill3D(QVector<QVector<QPoint> > inputRegion)
{
	int maxZ = inputRegion.size();
	int minZ = 0;
	int curZ = 0;
	while(curZ<maxZ){
		if(!inputRegion.at(curZ).empty())
			break;
		curZ++;
	}
	minZ = curZ;
	while(curZ<maxZ){
		if(inputRegion.at(curZ).empty())
			break;
		curZ++;
	}
	maxZ = curZ;

	int minX = inputRegion.at(minZ).first().x();
	int maxX = minX;
	int minY = inputRegion.at(minZ).first().y();
	int maxY = minY;
	for(int z=minZ;z<=maxZ;z++){
		for(int i=0;i<inputRegion.at(z).size();i++){
			if(inputRegion[z].at(i).x() < minX)
				minX = inputRegion[z].at(i).x();
			if(inputRegion[z].at(i).x() > maxX)
				maxX = inputRegion[z].at(i).x();
			if(inputRegion[z].at(i).y() < minY)
				minY = inputRegion[z].at(i).y();
			if(inputRegion[z].at(i).y() > maxY)
				maxY = inputRegion[z].at(i).y();
		}
	}
	minX = minX - 1;
	maxX = maxX + 1;
	minY = minY - 1;
	maxY = maxY + 1;
	minZ = minZ - 1;
	maxZ = maxZ + 1;
	QVector<int> mins;
	mins << minZ << minY << minX;

	//!create box now
	QVector<QVector<QVector<bool> > > filledRegion(maxZ - minZ + 1, QVector<QVector<bool> >(maxY - minY + 1, QVector<bool>(maxX - minX + 1, false)));

	floodFillOutside(QPoint(0,0), 0, mins, inputRegion, &filledRegion);

	for(int z=0;z<filledRegion.size();z++)
		for(int y=0;y<filledRegion.at(z).size();y++)
			for(int x=0;x<filledRegion[z][y].size();x++)
				if(!filledRegion[z][y].at(x))
					if(!inputRegion.at(minZ+z).contains(QPoint(minX+x,minY+y)))
						inputRegion[minZ+z].append(QPoint(minX+x,minY+y));

	return inputRegion;
}
