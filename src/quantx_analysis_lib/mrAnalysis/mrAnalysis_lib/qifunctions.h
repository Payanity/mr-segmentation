#ifndef QIFUNCTIONS_H
#define QIFUNCTIONS_H

#include "mranalysisdllcheck.h"

int QIMRANALYSIS_EXPORT roundUpToMultiple(int x, int multiple);
void QIMRANALYSIS_EXPORT qiSleep(int ms);

#define USE_DEPRECATED_QIFUNCTIONS 0
// The following functions are not necessary with modern C++
#if USE_DEPRECATED_QIFUNCTIONS

#ifndef _USE_MATH_DEFINES
    #define _USE_MATH_DEFINES // for C++
#endif
#include <utility>
#include <cmath>
#include <algorithm>

// Note that the following template functions aren't necessary, you can just use the lambda:
// auto qiMinMax = [](auto d){auto p = std::minmax_element(d.begin(), d.end()); return std::make_pair(*p.first, *p.second);};
// auto qiMaxWithLocation = [](auto d){auto p = std::max_element(d.begin(), d.end()); return std::make_pair(*p, std::distance(d.begin(), p));};
// auto qiMinWithLocation = [](auto d){auto p = std::min_element(d.begin(), d.end()); return std::make_pair(*p, std::distance(d.begin(), p));};

template <typename IS, typename IT>
inline auto extract_iterator_pair(std::pair<IS, IT> p)
{
    return std::make_pair(*p.first, *p.second);
}

template <typename T>
inline auto qi_minmax(const QVector<T> &v)
{
    return extract_iterator_pair(std::minmax_element(v.begin(), v.end()));
}

template <typename T>
inline auto qi_minmax(const QList<T> &v)
{
    return extract_iterator_pair(std::minmax_element(v.begin(), v.end()));
}

#include <QVector>
#include <QPointF>
#undef max
#undef min

double QIMRANALYSIS_EXPORT qiRound(double); // <cmath> provides round() as of C++11
QPointF QIMRANALYSIS_EXPORT max(QVector<double>); // <algorithm> provides std::max_element() as of C++17
QPointF QIMRANALYSIS_EXPORT min(QVector<double>); // <algorithm> provides std::min_element() as of C++17
QPoint QIMRANALYSIS_EXPORT max(QVector<int>); // <algorithm> provides std::max_element() as of C++17
QPoint QIMRANALYSIS_EXPORT min(QVector<int>); // <algorithm> provides std::min_element() as of C++17
int QIMRANALYSIS_EXPORT min(int,int);   //<algorithm>
int QIMRANALYSIS_EXPORT max(int,int);   //<algorithm>
#endif


#endif // QIFUNCTIONS_H
