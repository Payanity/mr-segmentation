#ifndef QUANTXLESIONDATABASE_H
#define QUANTXLESIONDATABASE_H

//#include "quantx_analysis_html_global.h"
//#include <Wt/WString.h>
//#include <memory>
#include <QStringList>
#include "model/lesion.h"
#include "model/lesiondetails.h"
#include "mranalysisdllcheck.h"
//#include "simplecrypt.h"
//#include "knowncase.h"

//class QuantXData;
//class QtObject;
//class AnalysisPanel;
struct KnownCase;
class MRLesionModel;
class MrAnalysis;
class QuantXLesionDatabasePrivate;
class QiMriImage;

//#define QI_FILE_PREFIX "C:/QuantX/"

//!
//! \brief This class handles communication with a database to add and retrieve mrAnalysis results to the database utitlizing Wt::Dbo to simplify database queries.
//!
//! \funcReqBegin
//! \funcReq53
//! \funcReq54
//! \funcReqEnd
//!
//! \todo Move to QI Sql Library.
//!
//! \ingroup MRAnalysisModule
//!
class QIMRANALYSIS_EXPORT QuantXLesionDatabase
{
public:
	enum class ConnectionType {
		Postgres,
		SQLite
	};

	QuantXLesionDatabase();
	QuantXLesionDatabase(const ConnectionType & conntype, const std::string & parameters);
	QuantXLesionDatabase(const QStringList & databaseSettings);
	QuantXLesionDatabase(const QuantXLesionDatabase &);
	~QuantXLesionDatabase();
	//QiSession session_;

	//QString seriesUID;

	//QStringList mraUUID;

	void setConnectionProperty(const std::string &name, const std::string &value);
	std::unique_ptr<Wt::Dbo::SqlConnection> connection() const;
	// addLesionToDatabase will return the uuid it generated
	QString addLesionToDatabase(QiMriImage *, const std::shared_ptr<MRLesionModel> &, QVector<const KnownCase *>, std::string = std::string());
	void removeLesionFromDatabase(const std::string & uuid);
	Wt::Dbo::ptr<quantx::mranalysis::Lesion> findLesionInDatabase(const std::string& uuid);
	Wt::Dbo::ptr<quantx::mranalysis::LesionDetails> findLesionDetailsInDatabase(const std::string& uuid);
	Wt::Dbo::collection<Wt::Dbo::ptr<quantx::mranalysis::Lesion> > findLesionInDatabaseBySeries(const std::string& seriesuid);
	QStringList dbSettings() const;
	Wt::Dbo::Session & session();

private:
	std::unique_ptr<QuantXLesionDatabasePrivate> d;
};

#endif // QUANTXLESIONDATABASE_H
