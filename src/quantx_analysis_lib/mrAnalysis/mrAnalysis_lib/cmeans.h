#ifndef CMEANS_H
#define CMEANS_H

#include "mranalysisdllcheck.h"

// I'm sure there's better/easier ways to perform a fuzzy c-means
// calculation, however, I want to recreate Weijie's format to
// make sure that we can replicate the data output from matlab

#include <QVector>
class MrAnalysis;
//!
//! \ingroup MRAnalysisModule
QIMRANALYSIS_EXPORT QVector<QVector<double> > cMeans(const QVector<QVector<double> > &, int nCluster, const QVector<double> &weights,double expo=2.0, int max_iter=100, double min_improve=0.00001, bool outputObjFn=false, int randSeed=5,int* progress = nullptr, QVector<unsigned int> nBins = QVector<uint>(1, 5), MrAnalysis * mrAnalysis = nullptr, QVector<QVector<double> > centerpoints = QVector<QVector<double> >());
#endif // CMEANS_H
