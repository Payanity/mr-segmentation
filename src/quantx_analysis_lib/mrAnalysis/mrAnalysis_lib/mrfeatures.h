#ifndef MRFEATURES_H
#define MRFEATURES_H

//#define _USE_MATH_DEFINES // for C++
#include "mranalysisdllcheck.h"
#include <cmath>
#include <QTextStream>
#include <QColor>
#include "glcmfeats.h"
#include "cmeans.h"

//!
//! \addtogroup MRAnalysisModule
//! @{
//!
enum NORMALIZATION_TYPE {
	NORMALIZATION_NONE                                  = 0x00000001,
	NORMALIZATION_PRECONTRAST_SUBTRACTION               = 0x00000002,
	NORMALIZATION_PRECONTRAST_SUBTRACTION_USING_MEAN    = 0x00000004,
	NORMALIZATION_PRECONTRAST_DIVISION                  = 0x00000008,
	NORMALIZATION_PRECONTRAST_DIVISION_USING_MEAN       = 0x00000010,
	NORMALIZATION_MINMAX_LINEAR                         = 0x00010000,
	NORMALIZATION_MINMAX_HISTOGRAM                      = 0x00020000
};

    //void analysisProgressCanceled(QProgressDialog *, QVector<QVector<double> > *);
    QVector<double> dynamicFeature(QVector<double> data, bool normalize, QVector<int> elapsedTime, int midTimePoint);
	QVector<double> shapeFeature3D(const QVector<QVector<QPoint> > &, double, double, double);
	QVector<QVector<QVector<quint16> > > makeBoundingBox(const QVector<int> & xData, const QVector<int> & yData, const QVector<int> & zData, const std::vector<std::vector<std::vector<quint16> > > *data = nullptr);
	QVector<QVector<QVector<double> > > isotropisize3D(QVector<QVector<QVector<quint16> > > *, double dx, double dy, double dz, int interpMethod = 0);
	QVector<double> interpolateVector(QVector<quint16> *data, const double & ratio, int method);
	QVector<double> linearInterpolateVector(QVector<quint16> *, const double &);
	QVector<double> monoCubicInterpolateVector(QVector<quint16> *, const double &);
	QVector<QVector<int> > isotrpisizeData(const QVector<int> &xData, const QVector<int> &yData, const QVector<int> &zData, double dx, double dy, double dz);
	QVector<QVector<double> > runGlcm3D(QVector<QVector<QPoint> > *region, const std::vector<std::vector<std::vector<std::vector<quint16> > > > & data, double dx, double dy, double dz, int firstTime = 0, int midTime = 2, bool morphology = true, bool localglcm=false, bool subtractionMorph=false, bool twoDmethod=false, int numLevels=32, int displacement = 1);
	//QVector<QVector<double> > runGlcmT2(QVector<QPolygon> *region, QList<QList<QList<QVector<quint16> > > > data, double dx, double dz,int height,bool morphology=true,bool localglcm=false,bool twoDmethod=false,int numLevels=32);
	QVector<double> varDynFeature(const QVector<QVector<double> > &, const NORMALIZATION_TYPE & normType, const QVector<int> &elapsedTime,  const int &midTime = 2);
	bool isNewPoint(QPoint,const QVector<QPoint>& );
	QVector<double> morphologyFeats(QVector<int> *, QVector<int> *, QVector<int> *, const QVector<QVector<QVector<double> > > *, double dmin);
	QIMRANALYSIS_EXPORT double findSurfaceArea(const QVector<QVector<QVector<double> > > & boundingBox);
	std::list<std::vector<std::array<bool, 3> > > findBoxes(const QVector<QVector<QVector<double> > > &boundingBox);
	double convertSVMToQiScore(double svm);
	double convertQiScoretoSVM(double qiScore);
//! @}
//!

	//int numPointsWithCommonDim(QList<QVector3D> & box, int commonDimensions);
	//int maxDimInCommon(QList<QVector3D> & box);

#endif // MRFEATURES_H
