#ifndef MRLESIONMODEL_H
#define MRLESIONMODEL_H

#include "mranalysisdllcheck.h"
#include "qimriimage.h"
#include <QPolygon>

//!
//! \class MRLesionModel
//! \brief Data for a segmented lesion
//!
//! \ingroup MRAnalysisModule
//!
class QIMRANALYSIS_EXPORT MRLesionModel
{
public:
	MRLesionModel(QiMriImage *);

	const QVector<double> & getFeatures() const { return features; }
	constexpr PointVector3D<int> getSeedPoint() const { return PointVector3D<int>(xSeed, ySeed, zSeed); }
	int precontrastIdx() const { return precontrastIdx_; }
	const QString & uuid() const { return uuid_; }
	const QString & mrn() const { return image->mrn(); }
	const QString & studyUID() const { return image->studyUID(); }
	const QString & accessionNumber() const { return image->accessionNumber(); }
	const QString & seriesUID() const { return image->seriesUID(); }
	const QVector<QVector<double> > & getRegionData() const { return regionData; }
	const QVector<double> getAvgUptake() const { return avgUptake; }
	const QVector<double> getMostUptaking() const { return mostUptaking; }
	const double & getPlotMaxValue() const { return plotMaxValue; }
	const QUANTX::SegmentationType & getSegmentationType() const { return segmentationType_; }
	const bool & segmentationDone() const {return segmentationDone_;}
	const bool & analysisDone() const {return analysisDone_;}
	const QVector<QTime> getTimestamps() const { return image->getTimestamps(); }
	const QTime getInjectionTime() const { return image->injectionTime(); }
	const QMap<double, QString> getTimeAndSeriesNums() const { return image->getTimeAndSeriesNums(); } //!< Return the time and Dicom series numbers for the DCEMR image associated with this lesion
	const QVector<QVector<QPolygon>>& getSegmentation() const { return segmentation; }
	const QUANTX::Orientation & orientation() const {return image->acquisitionOrientation();} //!< This holds the acquisition orientation of the dynamic MR image.

	void setUuid(const QString & u) { uuid_ = u; }

#if QUANTX_BUILD_WITH_TESTS
	friend class MrAnalysisTest;
	friend class QuantXDataTest;
#endif

//private:
	QiMriImage *image; //!< This holds a pointer to the dynamic MR image to be used for calculations.
	QVector<QVector<QPolygon> > segmentation; //!< This holds the outline of the ROI.
	QVector<double> features; //!< This holds the data from the analysis results.
	//QVector<double> bnnFeats;
	//QVector<double> histFeats;
	QVector<double> avgUptake; //!< This holds the data for the average time course curve throughout the ROI.
	QVector<double> mostUptaking; //!< This holds the data for the most uptaking cluster of data from the ROI.
	double plotMaxValue{}; //!< This holds the maximum y axis range (uptake) of the uptake plot.
	int xSeed {0}; //!< This holds the x component of the voxel index of the segmentation seed point.
	int ySeed {0}; //!< This holds the y component of the voxel index of the segmentation seed point.
	int zSeed {0}; //!< This holds the z component of the voxel index of the segmentation seed point.
	int precontrastIdx_; //!< This hold the index of the precontrast time point of the dynamic MR image. This will usually be zero, except for images that have multiple pre-contrast image volumes.
	QVector<QVector<double> > regionData; //!< This holds the kinetic data within the ROI.
	QUANTX::SegmentationType segmentationType_; //!< This holds the segmentation type for this MR Analysis (see SegmentationType data type in QuantXData)
	bool segmentationDone_ {false}; //!< This holds the binary value representing whether the segmentation has finished.
	bool analysisDone_ {false}; //!< This holds the binary value representing whether the analysis has finished.
	QString uuid_; //!< This holds a universally unique identifier (UUID) for the lesion.

	friend class MrAnalysis;
#ifdef QUANTX_BUILD_WITH_TESTS
	friend class QuantXGuiTest;
#endif
};

#endif // MRLESIONMODEL_H
