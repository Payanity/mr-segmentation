#include "dlib/svm_threaded.h"
#include <QList>
#include <QVector>
#include <QByteArray>
#include <QtAlgorithms>
#include <QFile>
#include <QDataStream>
#include <QPair>
#include <QFileDialog> //temporary

#define USEPROBABILISTICSVM 1

#define useDebug 0
#if useDebug
#include <QCryptographicHash>
#include <QDebug>
#endif

double testsvm(const QVector<double> &inputData, const QVector<QPair<double, double> >& localNormalizationFactors = (QVector<QPair<double, double> >()), QVector<int> featsToUse = QVector<int>() );

namespace dlib {

using sample_type = matrix<double, 0, 1>; //We can use matrix.set_size() later
using kernel_type = radial_basis_kernel<sample_type>;
//using kernel_type = sigmoid_kernel<sample_type>;
//using kernel_type = linear_kernel<sample_type>;

}

enum NormalizationLocality { LocalNormalization,
	                         GlobalNormalization,
	                         MixedNormalization };

using namespace dlib;

QVector<double> normalize(const QVector<double> &inputArray, const QVector<QPair<double, double> > &normalizationFactors)
{
	QVector<double> mins;
	QVector<double> factors;

	for(auto normalizationFactor : normalizationFactors){
		mins    << normalizationFactor.first;
		factors << normalizationFactor.second;
	}

	int nFeatures = inputArray.size();
	QVector<double> outputArray(nFeatures);

	for(int j=0; j < nFeatures; j++)
		outputArray[j] = (inputArray[j] - mins[j]) * factors[j];

	return outputArray;
}

double testsvm(const QVector<double> &inputData, const QVector<QPair<double, double> >& localNormalizationFactors, QVector<int> featsToUse)
{
	sample_type sample;

#if USEPROBABILISTICSVM
	using probabilistic_funct_type = probabilistic_decision_function<kernel_type>;
	using pfunct_type = normalized_function<probabilistic_funct_type>;

	pfunct_type learned_function;
#else
	using dec_funct_type = decision_function<kernel_type>;
	using funct_type = normalized_function<dec_funct_type>;

	funct_type learned_function;
#endif
	NormalizationLocality normLocality = GlobalNormalization;
	if(localNormalizationFactors.isEmpty())
		normLocality = GlobalNormalization;

	int nFeatures = inputData.size();

	QByteArray trained_function;
	QVector<int> vals;
	QVector<QPair<double,double> > globalNormalizationFactors;
	QVector<QPair<double,double> > normalizationFactors;

	QFile inFile(":/svm/saved_function");
	inFile.open(QIODevice::ReadOnly);
	QDataStream ds(&inFile);
	ds >> globalNormalizationFactors >> vals >> trained_function;
	inFile.close();

	if(normLocality == GlobalNormalization)
		normalizationFactors = globalNormalizationFactors;
	else{
		if(normLocality == MixedNormalization){
			QVector<int> globalNormalizationIndices;
			globalNormalizationIndices << 7 << 8 << 11; //Volume, Surface Area, Effective Radius
			for(int i = 0; i <= nFeatures; i++){
				if(globalNormalizationIndices.contains(i))
					normalizationFactors << globalNormalizationFactors.at(i);
				else
					normalizationFactors << localNormalizationFactors.at(i);
			}
		}
		else //if(normLocality == LocalNormalization)
			normalizationFactors = localNormalizationFactors;
	}

	QVector<double> normalizedData = normalize(inputData,normalizationFactors);

	std::stringstream in(std::string(trained_function.data(), static_cast<std::string::size_type>(trained_function.size())));
	deserialize(learned_function, in);
	//std::ifstream filein("saved_function.dat",std::ios::binary);
	//filein.ignore( ( globalNormalizationFactors.size() * 2 * sizeof(double) ) + ( vals.size() * sizeof(int) ) + 12 );
	//deserialize(learned_function, filein);

	if(featsToUse.empty())
		featsToUse << vals;
	sample.set_size(featsToUse.size());

#if useDebug
	QVector<double> featVals;
#endif

	for(int i = 0; i < featsToUse.size(); ++i){
#if useDebug
#   if 0
		QVector<double> testVals;
		testVals << 802.438 << 2326.39 << 1786.35 << -360.01 << 1934.12 << -1404.34 << -3535.91 << -2227.32 << -1436.8 << 3828.29 << 1022.36;
		featVals << testVals.at(i);
#   else
		featVals << normalizedData[featsToUse.at(i)];
#   endif
		sample(i) = featVals.at(i);
#else
		sample(i) = normalizedData[featsToUse.at(i)];
#endif
	}
#if useDebug
	std::stringstream s;
	serialize(learned_function, s);
	qDebug() << "Learned Function MD5:" << QCryptographicHash::hash(QByteArray( s.str().data(), static_cast<int> (s.str().size()) ),QCryptographicHash::Md5).toHex() << "Custom Feature List?" << !(vals == featsToUse);
	double firstRunVal = learned_function(sample);
	qDebug() << "Normalized Input Features:" << featVals << "SVM Output:" << firstRunVal;
#if 0
	int count = 0;
	int numRuns = 1000;
	for(int i = 0; i < numRuns; ++i)
		if(firstRunVal == learned_function(sample))
			count++;
	qDebug() << "SVM output reproduced" << count << "/" << numRuns << "times";
#endif //Testing block
#endif
	return learned_function(sample);

}

