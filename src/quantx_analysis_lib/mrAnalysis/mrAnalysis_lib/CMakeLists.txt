cmake_minimum_required(VERSION 3.14)
PROJECT(mranalysis)
find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Concurrent REQUIRED)
set(MRIV_ROOT ../../..)
set(CMAKE_DEBUG_POSTFIX _debug)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${MRIV_ROOT}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${MRIV_ROOT}/bin)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
#set(CMAKE_INCLUDE_CURRENT_DIR ON)

include_directories(${MRIV_ROOT}/src/qisql/qisql_lib
    ${MRIV_ROOT}/src/quantxdata/quantxdata_lib
    ${MRIV_ROOT}/include/jama
    ${MRIV_ROOT}/include/simplecrypt
    ${MRIV_ROOT}/${DLIB_DIR}
    ${WT_PATH}/include)

message("DLIB:" ${MRIV_ROOT}/${DLIB_DIR})
message("GDCM lib:" ${GDCM_PATH}/lib)
LINK_DIRECTORIES(${GDCM_PATH}/lib
    ${WT_PATH}/lib)

set (MRANALYSIS_HDRS
    mranalysis.h
    mranalysisdllcheck.h
    morphological.h
    cmeans.h
    glcmfeats.h
    qifunctions.h
    bnn.h
    mrfeatures.h
    binarycube.h
    debugtablemaker.h
    ${MRIV_ROOT}/src/mammoFeats/mammofeats_main/copytablewidget.h
    ${MRIV_ROOT}/include/simplecrypt/simplecrypt.h
    lesiondatabase.h
    mrlesionmodel.h)

set (MRANALYSIS_SRCS mranalysis.cpp
    morphological.cpp
    mrsegmentation.cpp
    cmeans.cpp
    mrfeatures.cpp
    glcmfeats.cpp
    qifunctions.cpp
    morphologyfeats.cpp
    bnn.cpp
    binarycube.cpp
    testsvm.cpp
    ../../mammoFeats/mammofeats_main/copytablewidget.cpp
    ../../quantxdata/quantxdata_lib/changeprevalence.cpp
    json.cpp
    ${MRIV_ROOT}/${DLIB_DIR}/dlib/test_for_odr_violations.cpp
    ${MRIV_ROOT}/include/simplecrypt/simplecrypt.cpp
    lesiondatabase.cpp
    mrlesionmodel.cpp)


set (MRANALYSIS_RCCS
    bnnfiles.qrc
    svmfiles.qrc)

if(QiMrAnalysisDll)
add_definitions(-DQIMRANALYSIS_DLL)
add_library(${PROJECT_NAME} SHARED ${MRANALYSIS_SRCS} ${MRANALYSIS_HDRS} ${MRANALYSIS_RCCS})
else()
add_library(${PROJECT_NAME} STATIC ${MRANALYSIS_SRCS} ${MRANALYSIS_HDRS} ${MRANALYSIS_RCCS})
endif()
target_link_libraries(${PROJECT_NAME} PUBLIC Qt5::Core Qt5::Widgets)
target_link_libraries(${PROJECT_NAME} PRIVATE qisql)
if (CMAKE_BUILD_TYPE MATCHES Debug)
    target_link_libraries(${PROJECT_NAME} PRIVATE wtd wtdbod wtdbosqlite3d wtdbopostgresd)
    TARGET_LINK_LIBRARIES(${PROJECT_NAME} PRIVATE gdcmCommon_debug)
else()
    target_link_libraries(${PROJECT_NAME} PRIVATE wt wtdbo wtdbosqlite3 wtdbopostgres)
    TARGET_LINK_LIBRARIES(${PROJECT_NAME} PRIVATE gdcmCommon)
endif()














