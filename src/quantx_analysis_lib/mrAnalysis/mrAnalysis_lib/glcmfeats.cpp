#include "glcmfeats.h"
#include "jama_eig.h"
#include "tnt_array2d_utils.h"
#include <QPointF>
#include <QBitArray>

#define useDebug 0
#if useDebug
#include <QDebug>
#include <QElapsedTimer>
#endif

//!
//! \brief Calculate the gray-level co-occurrence matrix (GLCM) on a volume.
//! \param xData
//! \param yData
//! \param zData
//! \param data
//! \param localglcm
//! \param twoDmethod
//! \param numLevels
//! \param displacement
//! \return
//!
//! \prePost{The (isotropisized) lesion volume is available.,Calculate and return the GLCM.}
//!
//! Compute the GLCM matrix p(i,j):
//! 1. The GLCM is calculated for a fixed number of gray-levels,
//! G, creating a matrix of size G×G, the voxel values must be first scaled into this range of voxel values,0 to (G-1).
//! The scaled voxel values, \f$V_s\f$, are calculated with the following equation:
//!
//! \f$ V_s = (\frac{G - 1}{H - L})(V - L) \f$ <br>
//! Where V is the original voxel value and H and L are the maximum and minimum voxel values, in the volume for which the GLCM is calculated.
//!
//! 2. The GLCM matrix, P, entry p(i,j) is the number of occurrences of voxel values i and j a distance d apart in the volume divided by the total number of voxels in the volume.
//! QuantX uses distances d of one, two, and three voxels yielding multiple GLCM matrices and multiple subsequent texture feature sets.
//! To calculate the GLCM-based texture features, these additional quantities are computed:
//!
//! \f$ p_x(i) = \displaystyle\sum_{j = 1}^{G} p(i,j) \f$ <br>
//! \f$ p_y(j) = \displaystyle\sum_{i = 1}^{G} p(i,j) \f$ <br>
//! \f$ p_{x + y}(k) = \displaystyle\sum_{i = 1}^{G}\displaystyle\sum_{j = 1}^{G} p(i,j), \quad k = i + j = 2, 3, \dots 2G \f$ <br>
//! \f$ p_{x - y}(k) = \displaystyle\sum_{i = 1}^{G}\displaystyle\sum_{j = 1}^{G} p(i,j), \quad k = |i - j| = 0, 1, 2, \dots G - 1 \f$
//!
//!
QVector<QVector<int> > glcmCalc(const QVector<int> & xData, const QVector<int> & yData, const QVector<int> & zData, const QVector<QVector<QVector<double> > > & data, bool localglcm, bool twoDmethod, int numLevels, int displacement)
{
	Q_UNUSED(localglcm)
#if useDebug
	QElapsedTimer time;
#endif

	//This function creates a gray level correlation matrix, then runs the features
	//you can remove the feature set if you want but I only need it for features
	//so I don't bother keeping the matrix

	auto width = data.size();
	auto height = data.last().size();
	auto depth = data.last().last().size();

	//int minx = min(*xData).x() - 1;      //shift mins left so we don't have to shift right later
	//int miny = min(*yData).x() - 1;
	//int minz = min(*zData).x() - 1;
	auto minx = 0;
	auto miny = 0;
	auto minz = 0; // Why was this originally fixed to zero?

	QVector<QVector<int> > offsets;
	if(twoDmethod){ //2-dimensional calculation
		offsets.resize(4);
		offsets[0] <<  0            <<  displacement << 0;
		offsets[1] << -displacement <<  displacement << 0;
		offsets[2] << -displacement <<  0            << 0;
		offsets[3] << -displacement << -displacement << 0;
	}
	else{           //3-dimensional calculation
		offsets.resize(13);
		int curoffset = 0;
		offsets[curoffset]         << 0            << 0              << displacement;
		curoffset++;
		for(int j=-1;j<2;j++){
			offsets[curoffset]     << 0            << displacement   << j*displacement;
			curoffset++;
		}
		for(int i=-1;i<2;i++)
			for(int j=-1;j<2;j++){
				offsets[curoffset] << displacement << i*displacement << j*displacement;
				curoffset++;
			}
	}

	//QList<double> allPixelValues;
	QVector<double> lesionPixelValues;
	auto levelImage = QVector<QVector<QVector<double> > > (width,QVector<QVector<double> >(height,QVector<double>(depth))); //data should be a isotropisized bounding box

	//bool inLesion;
#if useDebug
	qDebug() << "GLCM Data";
	qDebug() << "width" << width << ", height" << height << ", depth" << depth << ", minx" << minx << ", miny" << miny << ", minz" << minz << ", maxx" << max(xData).x() << ", maxy" << max(yData).x() << ", maxz" << max(zData).x();
	qDebug() << "Number of Lesion Voxels" << xData.size();
	qDebug() << QString("GLCM Calculate (10):  %1 seconds").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif
	QVector<QVector<QBitArray> > lesionMask(width,QVector<QBitArray>(height,QBitArray(depth,false)));
	for(int i = 0; i < xData.size(); ++i){
		lesionMask[xData[i] - minx][yData[i] - miny].setBit(zData[i] - minz, true);
		lesionPixelValues << data[xData[i] - minx][yData[i] - miny][zData[i] - minz];
	}
	/* This is an old way of doing things
	for(int i = 0; i < width; ++i)
		for(int j = 0; j < height; ++j)
			for(int k = 0; k < depth; ++k){
				//allPixelValues << (*data)[i][j][k];
				//inLesion = false;
				if(lesionMask[i][j].at(k))
					lesionPixelValues << (*data[i][j][k]);
				//for(int n = 0; n < xData->size(); n++){
				//    if(((*xData)[n] == i + minx) && ((*yData)[n] == j + miny) && ((*zData)[n] == k + minz)){
				//        lesionPixelValues << (*data)[i][j][k];
				//        //inLesion = true;
				//        break;
				//    }
				//}
			}
	*/
#if useDebug
	qDebug() << QString("GLCM Calculate (20):  %1 seconds").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif

	//std::sort(allPixelValues.begin(), allPixelValues.end() );
	std::sort(lesionPixelValues.begin(), lesionPixelValues.end() );
	auto totalPoints = lesionPixelValues.size();
	QVector<double> cutoffs(numLevels - 1);
	for(int n = 0; n < numLevels - 1; ++n)
		cutoffs[n] = lesionPixelValues[((n + 1) * totalPoints) / numLevels];
#if useDebug
	/*
	qDebug() << "Total Lesion Points Found:" << totalPoints;
	qDebug() << "Histogram Cutoff Points:" << cutoffs;
	double pixelValueSum = 0.0;
	for(const auto & lesionPixelValue : std::as_const(lesionPixelValues))
		pixelValueSum += lesionPixelValue;
	qDebug() << "Average Uptake =" << pixelValueSum / totalPoints;
	*/
	qDebug() << QString("GLCM Calculate (30):  %1 seconds").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif

#pragma omp parallel for
	for(int i = 0; i < width; i++)
		for(int j = 0; j < height; j++)
			for(int k = 0; k < depth; k++){
				//levelImage[i][j][k] = qiRound(static_cast<double>(numLevels-1) * cumSum[(levelImage[i][j][k])] / totalPoints);
				QVector<double>::iterator histLoc = cutoffs.begin();
				while(histLoc != cutoffs.end()){
					if(data.at(i).at(j).at(k) <= *histLoc)
						break;
					++histLoc;
				}
				levelImage[i][j][k] = histLoc - cutoffs.begin();
#if useDebug
				/*
				if(Q_UNLIKELY(levelImage.at(i).at(j).at(k) < 0))
					qDebug() << "levelImage less than zero at " << i << j << k;
				//    levelImage[i][j][k] = 0;
				if(Q_UNLIKELY(levelImage.at(i).at(j).at(k) >= numLevels))
					qDebug() << "levelImage greater than numLevels at " << i << j << k;
				//    levelImage[i][j][k] = numLevels - 1;
				*/
#endif
			}
#if useDebug
	qDebug() << "finished making levelImage, begin with offsets";
	qDebug() << QString("GLCM Calculate (40):  %1 seconds").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif

	//QVector<QVector<int> > corrMatrix(numLevels, QVector<int>(numLevels, 0) ); //just create one matrix and sum it all together
	QVector<QVector<int> > corrMatrix(numLevels);
#pragma omp parallel for
	for(int i = 0; i < numLevels; ++i)
		corrMatrix[i].resize(numLevels); //This forces QVector to allocate all of the data. According to documentation, this should also initialize the data to zero.

#if useDebug
	qDebug() << QString("GLCM Calculate (50):  %1 seconds").arg(time.restart() / 1000.0, 7, 'f', 3);
#endif

#pragma omp parallel for
	for(int i = 0; i < xData.size(); i++){
		auto x = xData.at(i);
		auto y = yData.at(i);
		auto z = zData.at(i);
//#pragma omp parallel for
		for(const auto & offset : std::as_const(offsets)){
			auto xo = x + offset.at(0);
			auto yo = y + offset.at(1);
			auto zo = z + offset.at(2);
			if( xo < 0 || xo >= width || yo < 0 || yo >= height || zo < 0 || zo >= depth)
				break;
			bool inLesion = lesionMask.at(xo).at(yo).at(zo);
			/*
			bool inLesion = false;
			for(int n = 0; n < xData->size(); n++){
				if( ( (*xData)[n] == xo ) && ( (*yData)[n] == yo ) && ( (*zData)[n] == zo ) ){
					inLesion = true;
					break;
				}
			}
			*/
			//if( (xo >= 0) && (yo >= 0) && (zo >= 0) && (xo < width) && (yo < height) && (zo < depth) ){
			if(inLesion){
				auto original = static_cast<int>(levelImage.at(x  - minx).at(y  - miny).at(z  - minz));
				auto adjacent = static_cast<int>(levelImage.at(xo - minx).at(yo - miny).at(zo - minz));
				auto & corrVector = const_cast<QVector<int> &>(corrMatrix.at(original));
//#pragma omp flush (corrMatrix)
//#pragma omp critical
#pragma omp atomic
				corrVector[adjacent]++;
			}
			/* Using this will double-count voxels already included in GLCM calculation
			if( (x - offset[0] >= 0) && (y - offset[1] >= 0) && (z - offset[2] >= 0) && (x - offset[0] < width) && (y - offset[1] < height) && (z - offset[2] < depth) ){
				int new2 = levelImage[x - offset[0]][y - offset[1]][z - offset[2]];
				corrMatrix[orig][new2] += 1;
			}
			*/
		}
	}

#if useDebug
	qDebug() << QString("GLCM Calculate (60):  %1 seconds").arg(time.restart() / 1000.0, 7, 'f', 3);
	qDebug() << "Finished creating GLCM";
#endif

	return corrMatrix;
	//return glcmFeats(&corrMatrix);

	//return glcm; //this return glcm, instead we should probably just get features and return those
}

//!
//! \brief Calculate the texture features using a GLCM.
//! \param corrMatrix
//! \return
//!
//! \prePost{The GLCM has been calculated for a volume.,Calculate and return the texture features.}
//!
//! The GLCM texture features are the following:
//!
//! \formula{Energy = \displaystyle\sum_{i = 1}^{G} \displaystyle\sum_{i = 1}^{G} p(i,j)^2}
//! \formula{Contrast = \displaystyle\sum_{k = 0}^{G - 1} k^2p_{x-y}(k)}
//! \formula{Correlation = \frac{\sum_{i=1}^{G}\sum_{j=1}^{G}(i \cdot j)p(i,j) - \mu_x\mu_y}{\sigma_x\sigma_y}}
//! Where \symbol{\mu_x} and \symbol{\mu_y} are the mean values of \symbol{p_x(i)} and \symbol{p_y(i)}, respectively
//! and \symbol{\sigma_x} and \symbol{\sigma_y} are the standard deviations of \symbol{p_x(i)} and \symbol{p_y(i)}, respectively. <br>
//! \formula{\text{Variance} = \displaystyle\sum_{i=1}^{G}(i - \mu_x)^2p_x(i)}
//! \formula{\text{Inverse Difference Moment} = \displaystyle\sum_{i=1}^{G}\displaystyle\sum_{j=1}^{G}\frac{1}{1+|i-j|}p(i,j)}
//! \formula{\text{Sum Average} = f_6 = \displaystyle\sum_{k=2}^{2G}kp_{x+y}(k)}
//! \formula{\text{Sum Variance} = \displaystyle\sum_{k=2}^{G}(k-f_6)^2p_{x+y}(k)}
//! \formula{\text{Sum Entropy} = -\displaystyle\sum_{k=2}^{2G}p_{x+y}(k)\log(p_{x+y}(k))}
//! \formula{\text{Entropy} = f_9 = -\displaystyle\sum_{i=1}^{G}\displaystyle\sum_{j=1}^{G}p(i,j)\log(p(i,j))}
//! \formula{\text{Difference Variance} = \displaystyle\sum_{k=0}^{G-1}(k-\mu_{x-y})^2p_{x-y}(k)}
//! Where \symbol{\mu_{x-y}} is the mean of \symbol{p_{x-y}(k)} <br>
//! \formula{\text{Difference Entropy} = -\displaystyle\sum_{k=0}^{G-1}p_{x-y}(k)\log(p_{x-y}(k))}
//! \formula{\text{Information Measure of Correlation 1(IMC1)} = \frac{f_9+\sum_{i=1}^{G}\sum_{j=1}^{G}p(i,j)\log(p_x(i)p_y(j))}{-\sum_{i=1}^{G}p_x(i)\log(p_x(i))}}
//! \formula{\text{IMC2} = \sqrt{1-e^{-2(-\sum_{i=1}^{G}\sum_{j-1}^{G}p_x(i)p_y(j)\log\big(p_x(i)p_y(j)\big)-f_9)}}}
//! \formula{\text{Maximum Correlation Coeff.} = \sqrt{\text{second largest eigenvalue of Q}}}
//! where \formula{Q(i,j) = \displaystyle\sum_{k=1}^{G}\frac{p(i,k)p(j,k)}{p_x(i)p_y(j)}}
//!
QVector<double> glcmFeats(const QVector<QVector<int> > & corrMatrix)
{
	//calculate feats
	auto numLevel = corrMatrix.size();
	QVector<double> feats;
	TNT::Array2D<double> glcm(numLevel,numLevel,0.0);

	//First, normalize the gorrelation matrix
	long sum = 0;
#pragma omp parallel for reduction(+: sum)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			sum += corrMatrix.at(i).at(j);
	// Return an empty array of features if the GLCM is empty
	if(sum == 0)
		return QVector<double>();
#if useDebug
	qDebug() << "sum:" << sum;
#endif

#pragma omp parallel for
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			glcm[i][j] = static_cast<double>(corrMatrix.at(i).at(j)) / sum;
#if useDebug
	qDebug() << "glcm normalized";
#endif

	//WE NOW HAVE NORMALIZED GLCM, PREPARE FOR FEATURES

	//Find Mean & Variance of Rows & columns
	double mr = 0.0;
	double mc = 0.0;
	double vr = 0.0;
	double vc = 0.0;
#pragma omp parallel for reduction(+: mr, mc)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++){
			mr += j * glcm[i][j];  //mean row of data
			mc += i * glcm[i][j];  //mean column of data
		}
#if useDebug
	qDebug() << "mr:" << mr << "mc:" << mc;
#endif
#pragma omp parallel for reduction(+: vr, vc)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++){
			vr += pow((mr-j),2) * glcm[i][j];
			vc += pow((mc-i),2) * glcm[i][j];
		}
#if useDebug
	qDebug() << "vr:" << vr << "vc:" << vc;
#endif
	//Standard Deviation
	auto sr = sqrt(vr);
	auto sc = sqrt(vc);
	//Create (r-c) and (r+c) matrices
	auto rplusc  = QVector<QVector<int> >(numLevel, QVector<int>(numLevel));
	auto rminusc = QVector<QVector<int> >(numLevel, QVector<int>(numLevel));
	//QVector<int> rpc(numLevel);
	//QVector<int> rmc(numLevel);
	for(auto i=0;i<numLevel;i++){
//#pragma omp parallel for //This crashes???
		for(auto j=0;j<numLevel;j++){
			rplusc [i][j] = i + j + 2; //+2 is to conform to shitty matlab vectors starting at index 1 instead of 0
			rminusc[i][j] = abs(i - j);
		}
		//rplusc.append(rpc);
		//rminusc.append(rmc);
	}
#if useDebug
	qDebug() << "rpc, rmc finished";
#endif

	//NOW CALCULATE FEATURES

	//Contrast
	auto contrast = 0.0;
#pragma omp parallel for reduction(+: contrast)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			contrast += glcm[i][j] * pow(static_cast<double>(i-j),2);
#if useDebug
	qDebug() << "contrast:" << contrast;
#endif

	//Correlation
	auto correlation = 0.0;
#pragma omp parallel for reduction(+: correlation)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			correlation += (i-mc) * (j-mr) * glcm[i][j];
	correlation = correlation / (sr * sc);
#if useDebug
	qDebug() << "correlation:" << correlation;
#endif

	//Energy (uniformity) (angular second moment)
	auto uniformity = 0.0;
#pragma omp parallel for reduction(+: uniformity)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			uniformity += pow(glcm[i][j],2);
#if useDebug
	qDebug() << "uniformity:" << uniformity;
#endif

	//Homogeneity (inverse difference moment)
	auto homogeneity = 0.0;
#pragma omp parallel for reduction(+: homogeneity)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			homogeneity += glcm[i][j] / (1 + pow(static_cast<double>(rminusc.at(i).at(j)),2));
#if useDebug
	qDebug() << "homogeneity:" << homogeneity;
#endif

	//Entropy
	auto entropy = 0.0;
#pragma omp parallel for reduction(+: entropy)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			if(Q_LIKELY(glcm[i][j] > 0.0))
				entropy += -(glcm[i][j] * log(glcm[i][j]));
#if useDebug
	qDebug() << "entropy:" << entropy;
#endif

	//Variance
	auto variance = 0.0;
#pragma omp parallel for reduction(+: variance)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			variance += pow((i-mc),2) * glcm[i][j];
#if useDebug
	qDebug() << "variance:" << variance;
#endif

	//Sum Average
	auto sumavg = 0.0;
#pragma omp parallel for reduction(+: sumavg)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			sumavg += rplusc[i][j] * glcm[i][j];
#if useDebug
	qDebug() << "sumavg:" << sumavg;
#endif

	//Sum Variance
	auto sumvar = 0.0;
#pragma omp parallel for reduction(+: sumvar)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			sumvar += pow((rplusc.at(i).at(j) - sumavg),2) * glcm[i][j];
#if useDebug
	qDebug() << "sumvar:" << sumvar;
#endif

	//Sum Entropy
	auto sument = 0.0;
	QVector<double> psum((numLevel * 2) + 1,0);
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			psum[(rplusc.at(i).at(j))] += glcm[i][j];
#pragma omp parallel for reduction(+: sument)
	for(auto i = 0; i < psum.size(); ++i)
		if(Q_LIKELY(psum.at(i) > 0.0))
			sument += -(psum.at(i) * log(static_cast<double>(psum.at(i))));
#if useDebug
	qDebug() << "sument:" << sument;
#endif

	//Diff Variance
	auto diffvar = 0.0;
	auto pdiff = QVector<double>(numLevel,0.0);
	auto ed = 0.0;
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			pdiff[rminusc[i][j]] += glcm[i][j];
#pragma omp parallel for reduction(+: ed)
	for(int i=0;i<numLevel;i++)
		ed += pdiff.at(i) * i;
#if useDebug
	qDebug() << "ed:" << ed;
#endif
#pragma omp parallel for reduction(+: diffvar)
	for(int i=0;i<numLevel;i++)
		diffvar += pow((i-ed),2) * pdiff.at(i);
#if useDebug
	qDebug() << "diffvar:" << diffvar;
#endif

	//Diff Entropy
	auto diffent = 0.0;
#pragma omp parallel for reduction(+: diffent)
	for(int i=0;i<numLevel;i++)
		if(Q_LIKELY(pdiff[i] > 0.0))
			diffent += -(pdiff.at(i) * log(pdiff.at(i)));
#if useDebug
	qDebug() << "diffent:" << diffent;
#endif

	//IMC1
	auto imc1 = 0.0;
	auto xsums = QVector<double>(numLevel,0.0);
	auto ysums = QVector<double>(numLevel,0.0);
	auto ysumXxsum = QVector<QVector<double> >(numLevel,xsums);
	for(int i=0;i<numLevel;i++)
//#pragma omp parallel for //This crashes???
		for(int j=0;j<numLevel;j++){
			ysums[i] += glcm[i][j];
			xsums[j] += glcm[i][j];
		}
#if useDebug
	qDebug() << "xsums, ysums finished";
#endif
#pragma omp parallel for
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++){
			ysumXxsum[i][j] = ysums.at(i) * xsums.at(j);
			//if(ysumXxsum[i][j] == 0.0)
			//    ysumXxsum[i][j] = 0.00000001; //get rid of zeroes
		}
#if useDebug
	qDebug() << "ysumXxsum finished";
#endif
	/*
	for(int i=0;i<numLevel;i++){              //now get rid of these zeroes (only division or log here on out)
		if(xsums.at(i) == 0.0)
			xsums[i] = 0.00000001;
		if(ysums.at(i) == 0.0)
			ysums[i] = 0.00000001;
	}
	*/
	auto hxy1 = 0.0;
#pragma omp parallel for reduction(+: hxy1)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			if(ysumXxsum.at(i).at(j) != 0.0)
				hxy1 += glcm[i][j] * log(ysumXxsum.at(i).at(j));
#if useDebug
	qDebug() << "hxy1:" << hxy1;
#endif
	auto hx = 0.0;    //Weijie made up variable name hx, horrible name IMO
#pragma omp parallel for reduction(+: hx)
	for(int i=0;i<numLevel;i++)
		if(ysums.at(i) != 0.0)
			hx += -(ysums.at(i) * log(ysums.at(i)));
#if useDebug
	qDebug() << "hx:" << hx;
#endif
	imc1 = (entropy + hxy1) / hx;

	//IMC2
	auto hxy2 = 0.0;
#pragma omp parallel for reduction(+: hxy2)
	for(int i=0;i<numLevel;i++)
		for(int j=0;j<numLevel;j++)
			if(ysumXxsum.at(i).at(j) != 0.0)
				hxy2 += -(ysumXxsum.at(i).at(j) * log(ysumXxsum.at(i).at(j)));
#if useDebug
	qDebug() << "hxy2:" << hxy2;
#endif
	auto imc2 = pow((1 - exp(-2*(hxy2 - entropy))),0.5);

	//Max CC
	auto maxcc = 0.0;
	/*
	TNT::Array2D<double> ymat(numLevel,numLevel);
	TNT::Array2D<double> xmat(numLevel,numLevel);
	for(int i=0;i<numLevel;i++)
		if(ysums[i] != 0.0)
			for(int j=0;j<numLevel;j++)
				if(xsums[j] != 0.0){
					ymat[i][j] = glcm[i][j] / ysums[i];
					xmat[i][j] = glcm[i][j] / xsums[j];
				}
	TNT::Array2D<double> qmat(TNT::matmult(xmat,ymat));
	*/

	auto qmat = TNT::Array2D<double>(numLevel,numLevel,0.0);
	for(int i=0; i<numLevel; ++i)
		if(ysums[i] != 0.0)
			for(int j=0; j<numLevel; ++j){
				double q = 0;
#pragma omp parallel for reduction(+: q)
				for(int k=0; k<numLevel; ++k)
					if(xsums[k] != 0.0)
						q += (glcm[i][k] * glcm[j][k]) / (ysums.at(i) * xsums.at(k));
				qmat[i][j] += q;
			}
#if useDebug
	qDebug() << "qmat finished";
#endif


#if useDebug
	QVector<double> qmatSums(numLevel,0.0);
	for(int i = 0; i < numLevel; ++i)
		for(int j = 0; j < numLevel; ++j)
			qmatSums[i] += qmat[i][j];
	qDebug() << "qmatSums:" << qmatSums;
#endif

	auto eig = JAMA::Eigenvalue<double>(qmat);
	auto eigens_real = TNT::Array1D<double>(numLevel);
	eig.getRealEigenvalues(eigens_real);

	/* This is sorted by magnitude? The real part of the second eigenvalue may be negative
	int eigvalnum;
	for(eigvalnum = 0; eigvalnum < numLevel - 2; eigvalnum++)
		if(eigens_real[eigvalnum] > 0.0)
			maxcc = sqrt(eigens_real[eigvalnum + 1]); //second-largest eigenvalue
	*/
	auto eigenvalues = QVector<double>(numLevel);
#pragma omp parallel for
	for(int i = 0; i < numLevel; ++i)
		eigenvalues[i] = eigens_real[i];
	/*
	TNT::Array1D<double> eigens_imag(numLevel);
	eig.getImagEigenvalues(eigens_imag);
	for(int i = 0; i < numLevel; ++i)
		eigenvalues << sqrt(pow(eigens_real[i],2) + pow(eigens_imag[i],2));
	*/
	std::sort(eigenvalues.begin(), eigenvalues.end());
	int eigvalnum = numLevel - 2;
	// We can get multiple copies of eigenvalues if not all of the gray levels are used
	while(qFuzzyCompare(eigenvalues[eigvalnum], 1.0) && eigvalnum > 0){
		eigvalnum--;
	}
	maxcc = sqrt(eigenvalues[eigvalnum]);

#if useDebug
	qDebug() << "Eigenvalues: " << eigenvalues;
	//qDebug() << "Selected Eigenvalue" << eigvalnum + 1 << "with value" << eigens[eigvalnum + 1] << "and square root" << sqrt(eigens[eigvalnum + 1]);
	qDebug() << "Selected Eigenvalue" << eigvalnum + 1 << "with value" << eigenvalues[eigvalnum] << "and square root" << sqrt(eigenvalues[eigvalnum]);
	//qDebug() << "Second Eigenvalue in complex form:" << eigens_real[eigvalnum + 1] << "+" << eigens_imag[eigvalnum + 1] << "i   -----   maxcc =" << maxcc;
#endif


	//Create Feature Vector and Return in alphabetical order
	feats.append(contrast);             //1
	feats.append(correlation);          //2
	feats.append(diffent);              //3
	feats.append(diffvar);              //4
	feats.append(uniformity); //energy  //5
	feats.append(entropy);              //6
	feats.append(homogeneity);          //7
	feats.append(imc1);                 //8
	feats.append(imc2);                 //9
	feats.append(maxcc);                //10
	feats.append(sumavg);               //11
	feats.append(sument);               //12
	feats.append(sumvar);               //13
	feats.append(variance);             //14



	return feats;
}

