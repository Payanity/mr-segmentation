#ifndef MRANALYSISDLLCHECK_H
#define MRANALYSISDLLCHECK_H

#include <QtGlobal>

#ifdef QIMRANALYSIS_DLL
#define QIMRANALYSIS_EXPORT  Q_DECL_EXPORT
#else
#define QIMRANALYSIS_EXPORT Q_DECL_IMPORT
#endif

#endif // MRANALYSISDLLCHECK_H
