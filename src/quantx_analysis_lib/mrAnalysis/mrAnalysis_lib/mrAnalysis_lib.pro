#-------------------------------------------------
#
# Project created by QtCreator 2012-06-08T12:03:06
#
#-------------------------------------------------
MRIV_ROOT = ../../..
include( $${MRIV_ROOT}/quantxconfig.pri )
include( $${MRIV_ROOT}/src/directoryconfig.pri )

#QT       += core
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets concurrent

TARGET = mranalysis
TEMPLATE = lib
DEPENDPATH += .
INCLUDEPATH += . \
               $${MRIV_ROOT}/src/qisql/qisql_lib \
               $${MRIV_ROOT}/src/quantxdata/quantxdata_lib \
               $${MRIV_ROOT}/include/simplecrypt \
               $${WT_PATH}/include \
               $${MRIV_ROOT}/include/jama \
               $${DLIB_DIR}

MOC_DIR           = moc
OBJECTS_DIR       = obj
DESTDIR           = $${MRIV_ROOT}/lib


    contains(CONFIG, QiMrAnalysisDll ) {
        CONFIG       += dll
        DEFINES      += QIMRANALYSIS_DLL
        DLLDESTDIR   = $${MRIV_ROOT}/bin
    }
    else {
        CONFIG += staticlib
    }

CONFIG(debug, debug|release) {
    LIBS += -L$${MRIV_ROOT}/lib -lqisql_debug \
            -L$${WT_PATH}/lib -lwtdbod -lwtdbosqlite3d -lwtdbopostgresd
}
CONFIG(release, debug|release) {
    LIBS += -L$${MRIV_ROOT}/lib -lqisql \
            -L$${WT_PATH}/lib -lwtdbo -lwtdbosqlite3 -lwtdbopostgres
}

LIBS += -L$${WT_PATH}/lib

win32 {
    LIBS += -lAdvapi32
}

SOURCES += mranalysis.cpp \
    morphological.cpp \
    mrsegmentation.cpp \
    cmeans.cpp \
    mrfeatures.cpp \
    glcmfeats.cpp \
    qifunctions.cpp \
    morphologyfeats.cpp \
    bnn.cpp \
    binarycube.cpp \
    testsvm.cpp \
    ../../mammoFeats/mammofeats_main/copytablewidget.cpp \
    ../../quantxdata/quantxdata_lib/changeprevalence.cpp \
    json.cpp \
    $${DLIB_DIR}/dlib/test_for_odr_violations.cpp \
    $${MRIV_ROOT}/include/simplecrypt/simplecrypt.cpp \
    lesiondatabase.cpp

HEADERS += mranalysis.h \
    model/lesiondetailsmr.h \
    model/lesionmr.h \
    mranalysisdllcheck.h \
    morphological.h \
    cmeans.h \
    glcmfeats.h \
    qifunctions.h \
    bnn.h \
    mrfeatures.h \
    binarycube.h \
    debugtablemaker.h \
    ../../mammoFeats/mammofeats_main/copytablewidget.h \
    $${MRIV_ROOT}/include/simplecrypt/simplecrypt.h \
    lesiondatabase.h


CONFIG(debug, debug|release) {
    TARGET = $$join(TARGET,,,_debug)

}

RESOURCES += \
    bnnfiles.qrc \
    svmfiles.qrc
















